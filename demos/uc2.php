<?php
function listErrors($objDef) {
    if(count($objDef->validationErrors)>0)
    {
        $i=1;
        foreach($objDef->validationErrors as $error)
        {
            echo "Error ${i}: Property:".$error->propertyName." Message:".$error->message."\n";
            $i+=1;
        }
    }
}

echo "UC2\n";

$LIBPATH=dirname(__FILE__)."/../";
$zonesBase = $LIBPATH."lib5/";
include_once($LIBPATH."lib5/AutoLoad.php");
include_once($LIBPATH."lib5/Consts.php");
include_once($LIBPATH."UtilLib/Utility.php");
include_once($LIBPATH."UtilLib/SpecUtils.php");
include_once($LIBPATH."UtilLib/SQLFuncs.php");
include_once($LIBPATH."UtilLib/EscapeSQL.php");
include("MyApp.php");

$app = new MyApp();
$app->start();

$objDef = new ObjDef("Person");
$objDef->setAttribute("table", "Person");
$objDef->setAttribute("title", "Person");
$objDef->setAttribute("description", "Person Lookup Table");
$objDef->setAttribute("idColumn", "personName");
$objDef->setAttribute("displayColumn", "personName");
$objDef->setAttribute("referencedBy", array("ResearchProjectPerson"));
$objDef->setAttribute("xmlTag", "Person");

$prop = $objDef->createProperty("personName", "string");
$prop->setAttribute("table", "Person");
$prop->setAttribute("column", "personName");
$prop->setAttribute("nullAllowed", false);
$prop->setAttribute("variableLength", 50);
$prop->setAttribute("description", "First and Last Name");
$prop->setAttribute("xmlTag", "personName");
$prop->setAttribute("columnType", "varchar(50)");
$prop->setAttribute("caption", "Name");
$prop->addCondition(new NotEmptyCondition());
$prop->addCondition(new StringMaxLengthCondition(50));
$objDef->addKey("personName");

// personName must not exist, in add mode

$pb = new ParamBuilder();
$pb->add("connectionName", "seqlims");
$pb->add("fkTable", "Person");
$pb->add("fkColumn", "personName");
$pb->add("exists", false);
$pb->add("errMsg", "FKLookupCondition : The value already exists.");
$prop->addCondition(new FKLookupCondition($pb->getParams()), array("add"));

$pb = new ParamBuilder();
$pb->add("connectionName", "seqlims");
$pb->add("fkTable", "ResearchProjectPerson");
$pb->add("fkColumn", "personId");
$pb->add("exists", false);
$pb->add("errMsg", "Because of a foreign key dependency in table ResearchProjectPerson the value can not be deleted.");
$prop->addCondition(new FKLookupCondition($pb->getParams()), array("delete"));

$prop->setAttribute("linkToReference", false);

$prop = $objDef->createProperty("addedOn", "datetime");
$prop->setAttribute("table", "Person");
$prop->setAttribute("column", "addedOn");
$prop->setAttribute("nullAllowed", false);
$prop->setAttribute("description", "(mm/dd/yyyy) Date this was added.");
$prop->setAttribute("xmlTag", "addedOn");
$prop->setAttribute("columnType", "datetime");
$prop->setAttribute("caption", "Added On");
$prop->setAttribute("defaultValue", "now()");
$prop->setAttribute("readOnly", true, array("add","edit"));
$prop->addCondition(new NotEmptyCondition());
$prop->setAttribute("dateFormat", "m/d/Y");
$prop->setAttribute("valueFormat", "Y-m-d G:i:s");
$prop->addCondition(new DateTypeCondition());
$prop->setAttribute("linkToReference", false);
$prop->setAttribute("handleAs", "date");

$prop = $objDef->createProperty("addedBy", "string");
$prop->setAttribute("table", "Person");
$prop->setAttribute("column", "addedBy");
$prop->setAttribute("nullAllowed", true);
$prop->setAttribute("variableLength", 20);
$prop->setAttribute("description", "User Id that added this.");
$prop->setAttribute("xmlTag", "addedBy");
$prop->setAttribute("columnType", "varchar(20)");
$prop->setAttribute("caption", "Added By");
$prop->setAttribute("defaultValue", "SESSION.userId");
$prop->setAttribute("readOnly", true, array("add","edit"));
$prop->addCondition(new StringMaxLengthCondition(20));


$pb = new ParamBuilder();
$pb->add("connectionName", "seqlims");
$pb->add("tableName", "ACU");
$pb->add("idColumnName", "user");
$pb->add("displayColumnName", "user");
$pb->add("addEmptyOption", true);
$prop->setConstraint(new SQLConstraint($pb->getParams()));


$prop->setAttribute("fkTable", "ACU");
$prop->setAttribute("fkColumn", "user");
$prop->setAttribute("fkIsAutoIncrement", false);
$prop->setAttribute("directReference",true);

$pb = new ParamBuilder();
$pb->add("connectionName", "seqlims");
$pb->add("fkTable", "ACU");
$pb->add("fkColumn", "user");
$pb->add("exists", true);
$prop->addCondition(new FKLookupCondition($pb->getParams()));

$prop->setAttribute("linkToReference", false);

$objDef->setAttribute("addOnChangeListener",array());

$app->specManager->addDef($objDef);

// find ObjDef Person
$objDef2 = $app->specManager->findDef("Person");

if (is_null($objDef2)) {
    echo "Error, Missing Person\n";
} else {
    echo "Success, Found Person\n";
}

//==============================================================================
$person = NewSpecObj('Person');
$person->personName = "Anthony Leotta";
$person->addedBy = "alex";
$objDef2->validate($person,"add");
listErrors($objDef2);


//==============================================================================
$person = NewSpecObj('Person');
$person->personName = "Tony Leotta";
$person->addedBy = 'unittest';
$objDef2->validate($person,"add");
listErrors($objDef2);




?>