<?php
class MyApp extends App {
    
    public $dbSeqLims;
            
    public function __construct() {                
        parent::__construct();               
        $this->machinename=strtolower(php_uname('n'));
        include_once("Config/".$this->machinename."/config.php");        
        $this->config = new Config();
        $this->dbPath = $this->config->dbPath;
        $this->configPath = $this->config->configPath;        
        $this->sitePath = $this->config->sitePath;        
    }    
    
    public function start() {
        parent::start();    
                
        $dbAdventureWorks=new Connection();
        $dbAdventureWorks->user=$this->dbSeqLims->user;
        $dbAdventureWorks->pass=$this->dbSeqLims->pass;
        $dbAdventureWorks->host=$this->dbSeqLims->host;
        $dbAdventureWorks->schema="adventureworks";
        try{
            $dbAdventureWorks->connect();
        } catch(Exception $e){
            // do nothing for now
        }
        $this->connectionMap->addConnection("adventureworks",$dbAdventureWorks);
        $this->connectionMap->addId("adventureworks","adventureworks");                 
    }
    
    public function appChecks() {        
        global $app;
        
        if($app->sessionManager->sessionObj->loggedIn==true){
            if ($this->acManager->userObj->hasLaboratories()===false) {                                                    
                $app->createRequestFromMenuItemId("NoLibrariesAssigned");
            }        
        }
    }
}
?>