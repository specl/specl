<?php

class DatabaseManager {

    public $hostname;
    public $user;
    public $pass;
    public $schema;
    public $dbConn;
    public $dbMan;
    
    public function __construct() {
        //$this->dbMan = new MySQLManager();
        
        $this->tableNames = array();
        $this->tables = array();
    }
    
    public function loadSchema() {        
        
        global $app;
        
        $this->dbConn = new Connection();
        $this->dbConn->user = $this->user;
        $this->dbConn->pass = $this->pass;
        $this->dbConn->host = $this->host;
        $this->dbConn->schema = $this->schema;            
        $this->dbConn->connect();
                
        // load tables        
        $this->loadTableNames();
        $this->loadTables();
    }
    
    public function loadTableNames() {        
        $sql = 'SHOW TABLES';        
        $results = $this->dbConn->select($sql);        
        $this->tableNames = $this->dbConn->fetchColumn($results);        
    }

    public function loadTables() {        
                
        foreach($this->tableNames as $tableName) {
                                 
            $columns = $this->listTableColumns($tableName);
            $this->tables[$tableName]['columns']=$columns;
            
            $constraints = $this->listTableConstraints($tableName);
            $this->tables[$tableName]['constraints']=$constraints;
            
            $indexes = $this->listTableIndexes($tableName);
            $this->tables[$tableName]['indexes']=$indexes;
            
            $relations = $this->listTableRelations($tableName);
            $this->tables[$tableName]['relations']=$relations;            
            
        }        
    }

    
    public function listTableColumns($table)
    {
        $sql = 'DESCRIBE ' . $this->dbConn->quoteIdentifier($table);
        $results = $this->dbConn->select($sql);        
        //$row = $this->dbConn->getRowAssoc($results);

        //$result = $this->conn->fetchAssoc($sql);

        $description = array();
        $columns = array();                      
         
        print $table."\n";
        
        while ($row = $this->dbConn->getRowAssoc($results)) {

            $row = array_change_key_case($row, CASE_LOWER);

           
            
            $decl = $this->dbConn->getPortableDeclaration($row);
            

            
            $values = isset($decl['values']) ? $decl['values'] : array();
            $row['default'] = $row['default'] == 'CURRENT_TIMESTAMP' ? null : $row['default'];

            $description = array(
                          'name'          => $row['field'],
                          'type'          => $decl['type'][0],
                          'alltypes'      => $decl['type'],
                          'ntype'         => $row['type'],
                          'length'        => $decl['length'],
                          'fixed'         => (bool) $decl['fixed'],
                          'unsigned'      => (bool) $decl['unsigned'],
                          'values'        => $values,
                          'primary'       => (strtolower($row['key']) == 'pri'),
                          'default'       => $row['default'],
                          'notnull'       => (bool) ($row['null'] != 'YES'),
                          'autoincrement' => (bool) (strpos($row['extra'], 'auto_increment') !== false),
                          );
            if (isset($decl['scale'])) {
                $description['scale'] = $decl['scale'];
            }
            
//            foreach($description as $key=>$value) {
//                if (is_array($value)) {
//                    print "\t".$key.":";
//                    
//                    $sep="";
//                    foreach($value as $v) {
//                        print $sep.$v;
//                        $sep=",";
//                    }
//                    print "\n";
//                       
//                } else {
//                    print "\t".$key.":".$value."\n";
//                }                    
//            }
            
            
            $columns[$row['field']] = $description;
//            print "\n";
        }

        return $columns;
    }
    
    /**
     * lists table constraints
     *
     * @param string $table     database table name
     * @return array
     */
    public function listTableConstraints($table)
    {
        $keyName = 'Key_name';
        $nonUnique = 'Non_unique';
//        if ($this->conn->getAttribute(Doctrine_Core::ATTR_FIELD_CASE) && ($this->conn->getAttribute(Doctrine_Core::ATTR_PORTABILITY) & Doctrine_Core::PORTABILITY_FIX_CASE)) {
//            if ($this->conn->getAttribute(Doctrine_Core::ATTR_FIELD_CASE) == CASE_LOWER) {
//                $keyName = strtolower($keyName);
//                $nonUnique = strtolower($nonUnique);
//            } else {
//                $keyName = strtoupper($keyName);
//                $nonUnique = strtoupper($nonUnique);
//            }
//        }

        $table = $this->dbConn->quoteIdentifier($table);
        $query = 'SHOW INDEX FROM ' . $table;
        $indexes = $this->dbConn->fetchAssoc($query);

        $result = array();
        foreach ($indexes as $indexData) {
            if ( ! $indexData[$nonUnique]) {
                if ($indexData[$keyName] !== 'PRIMARY') {
                    $index = $indexData[$keyName];
                    //print $index;
                    //$index = $this->dbConn->formatter->fixIndexName($indexData[$keyName]);
                } else {
                    $index = 'PRIMARY';
                }
                if ( ! empty($index)) {
                    $result[] = $index;
                }
            }
        }
        return $result;
    }

    public function listTableIndexes($table)
    {
        $keyName = 'Key_name';
        $nonUnique = 'Non_unique';
//        if ($this->conn->getAttribute(Doctrine_Core::ATTR_FIELD_CASE) && ($this->conn->getAttribute(Doctrine_Core::ATTR_PORTABILITY) & Doctrine_Core::PORTABILITY_FIX_CASE)) {
//            if ($this->conn->getAttribute(Doctrine_Core::ATTR_FIELD_CASE) == CASE_LOWER) {
//                $keyName = strtolower($keyName);
//                $nonUnique = strtolower($nonUnique);
//            } else {
//                $keyName = strtoupper($keyName);
//                $nonUnique = strtoupper($nonUnique);
//            }
//        }

        $table = $this->dbConn->quoteIdentifier($table, true);
        $query = 'SHOW INDEX FROM ' . $table;
        $indexes = $this->dbConn->fetchAssoc($query);


        $result = array();
        foreach ($indexes as $indexData) {
//            if ($indexData[$nonUnique] && ($index = $this->conn->formatter->fixIndexName($indexData[$keyName]))) {
//                $result[] = $index;
//            }
            $result[] = $indexData;
        }
        return $result;
    }
    
    
    public function listTableRelations($tableName)
    {
        $relations = array();
        $sql = "SELECT column_name, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME FROM information_schema.key_column_usage WHERE table_name = '" . $tableName . "' AND table_schema = '" . $this->dbConn->getDatabaseName() . "' and REFERENCED_COLUMN_NAME is not NULL";
        $results = $this->dbConn->fetchAssoc($sql);
        foreach ($results as $result)
        {
            $result = array_change_key_case($result, CASE_LOWER);
            $relations[] = array('table'   => $result['referenced_table_name'],
                                 'local'   => $result['column_name'],
                                 'foreign' => $result['referenced_column_name']);
        }
        return $relations;
    }
    
    public function dump() {          
        $tableNum=1;
        foreach($this->tableNames as $tableName) {
            
            echo $tableNum.'. '.$tableName."\n";            
            
            $columns = $this->tables[$tableName]['columns'];            
            foreach($columns as $column) {                
                echo "\t".print_r($column)."\n";            
            }
            
            $constraints = $this->tables[$tableName]['constraints'];
            foreach($constraints as $constraint) {                
                echo "\t".$constraint."\n";            
            }
            
            $indexes = $this->tables[$tableName]['indexes'];
            foreach($indexes as $index) {                
                echo "\t".print_r($index)."\n";            
            }

            $relations = $this->tables[$tableName]['relations'];
            foreach($relations as $relation) {                
                echo "\t".print_r($relation)."\n";            
            }
            
            $tableNum+=1;
        }
    }

}
?>