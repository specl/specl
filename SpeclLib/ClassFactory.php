<?php
/**
 * ClassFactory create PHP objects from data and spec defs.
 *
 * @author tony
 */
class ClassFactory {

    public $specMgr; // SpecManager
    
    public function  __construct($specMgr) {
        $this->specMgr = $specMgr;
    }
   
    public function _createObj($def, $data) {

        if (is_null($def)) {
            // this is a serious error
            return;
        }
        
        //$valid = $spec->validate($data);
        //if ($valid==false) {
        //    $log->error("ClassFactory","data not valid");
        //    return null;
        //}

        $constructorParams=array();
        foreach($def->constructorParameters as $key=>$propId)
        {
            if (array_key_exists($key, $data)) {
                $value = $data[$key];
            } else {
                $value = null;
            }

            $constructorParams[$propId] = $value;
        }

        $klass = $def->name.'Cls';
        
        if (autoloadSearch($klass)==false) {
            // create class
            $this->createClass($def);
        }
                
        #print $klass."\n";
        
        $obj = new $klass($constructorParams);
        //$obj = NewSpecObj($def->name);
                
        foreach ($def->getProperties() as $propertyDef) {
            
            if (array_key_exists($propertyDef->id, $data)) {
                
                if ($propertyDef->isScalar()) {

                    $valid=true;
                    $value = $data[$propertyDef->id];

                    if ($propertyDef->objectType == "string") {

                    } else if ($propertyDef->objectType == "numeric") {

                    } else if ($propertyDef->objectType == "boolean") {                        

                        $results = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                        
                        if (is_null($results)) {
                            $valid=false;
                        } else if ($results == true) {
                            $value=true;
                        } else if ($results==false) {
                            $value=false;
                        }
                        
                    } else if ($propertyDef->objectType == "date") {

                    } else if ($propertyDef->objectType == "datetime") {

                    }

                    if ($valid) {
                        if ($propertyDef->setter) {
                            $obj->{$propertyDef->setter}($value);
                        } else {
                            $obj->{$propertyDef->name} = $value;
                        }
                    }

                } else if ($propertyDef->isObject()) {

                    $objDef = $this->specMgr->findDef($propertyDef->objectTypeOrig);

                    $propObj = $this->_createObj($objDef, $data[$propertyDef->id]);

                    if ($propertyDef->setter) {
                        $obj->{$propertyDef->setter}($propObj);
                    } else {
                        $obj->{$propertyDef->name} = $propObj;
                    }

                } else if ($propertyDef->isCollection()) {

                    $collectionDef = $this->specMgr->findDef($propertyDef->objectTypeOrig);

                    $collectionObj = $this->_createCollection($collectionDef, $data[$propertyDef->id]);

                    //if ($collectionDef->setter) {
                    if ($propertyDef->setter) {
                        $obj->{$propertyDef->setter}($collectionObj);
                    } else {
                        $obj->{$propertyDef->name} = $collectionObj;
                    }
                }
            }
            else {
                //echo "{$def->id} {$propertyDef->id} missing type {$propertyDef->propType}\n";
            }
        }

        return $obj;        
    }

    public function _createCollection($def, $data) {

        $obj = array();

        foreach($data['data'] as $key=>$itemData) {

            $type =$itemData['__defType__'];
            $id =$itemData['__type__'];

            if ($type=='Object') {
                $itemDef = $this->specMgr->findDef($id);
                $itemObj = $this->_createObj($itemDef, $itemData);

                if ($def->isAssociative) {
                    $obj[$key] = $itemObj;
                } else {
                    $obj[] = $itemObj;
                }
            } else if ($type=='Collection') {

                $itemDef = $this->specMgr->findDef($id);
                $itemObj = $this->_createCollection($itemDef, $itemData);
                if ($def->isAssociative) {
                    $obj[$key] = $itemObj;
                } else {
                    $obj[] = $itemObj;
                }
            }
        }
        return $obj;
    }

    public function create($data, $className=null) {
        global $log;
        $obj = null;
       
        if (is_null($className)) {
            $className = $data['__type__'];
        } 

        $def = $this->specMgr->findDef($className);

        if ($def==null) {
            $log->error("ClassFactory","def for {$className} not found");
            return $obj;
        }

        if ($data==null) {
            
            $log->error("ClassFactory","data for {$className} was NULL");
            return $obj;
        }

        if ($def->isObject()) {
            $obj = $this->_createObj($def, $data);
        } else if ($def->isCollection()) {
            $obj = $this->_createCollection($def, $data);
        } 

        return $obj;
    }

    // given a SpecManager, an object return SimpleXML of that object
    // based on Spec Defs
    
    public function addComment($node, $msg) {
        $comment = $this->dom->createComment($msg);//create a comment
        $node->appendChild($comment);//Add some comment to the xml file
    }
    
    public function saveToXML($object) {
        global $log;
        $obj = null;

        if ($object==null) {
            $log->error("ClassFactory->save","data for {$className} was NULL");
            return "";
        }

        $className = get_class($object);
            
        $def = $this->specMgr->findDef($className);

        if ($def==null) {
            $log->error("ClassFactory","def for {$className} not found");
            return "";
        }
        
        $rootNode = null;        
        
        if ($def->isObject()) {
            $this->dom = new DOMDocument();
            $this->dom->formatOutput = true;
            $rootNode = $this->dom->createElement($def->name);
            //$rootNode = new SimpleXMLElement("<{$def->name}></{$def->name}>");
            $this->dom->appendChild($rootNode);//append the root node

            $this->_saveObj($rootNode, $def, $object);
        } else {
            die("saveToXML {$def->name} is not an ObjDef");
        }

        return $this->dom;
    }


    public function _saveScalar($parentNode, $propertyDef, $obj) {

        if ($propertyDef->getter) {
            $value = $obj->{$propertyDef->getter}();
        } else {
            if (isset($obj->{$propertyDef->name})) {
                $value = $obj->{$propertyDef->name};
            } else {
                $value = null;
            }
        }

        // outgoing type conversions
        if ($value) {
            if ($propertyDef->objectType == "string") {

            } else if ($propertyDef->objectType == "numeric") {

            } else if ($propertyDef->objectType == "boolean") {

                if ($value==true) {
                    $value = "true";
                } else {
                    $value = "false";
                }

            } else if ($propertyDef->objectType == "date") {

            }
            //$childNode = $parentNode->addChild($propertyDef->name,$value);
            $childNode=$this->dom->createElement($propertyDef->name,$value);
            $parentNode->appendChild($childNode);//append child element to the node
            
        } else {
            // add empty node?  This is make XML larger...
            $childNode = null; //$parentNode->addChild($propertyDef->name,"");
        }
            
        return $childNode;
    }

    public function _saveCollection($parentNode, $propertyDef, $obj) {

        if (isset($obj->{$propertyDef->name})==false) {
            return;
        }

        $collection = $obj->{$propertyDef->name};
        
        if (is_null($collection)) {
            return;
        }
         
        if (count($collection)==0) {
            return;
        }
        
        $collectionDef = $this->specMgr->findDef($propertyDef->objectTypeOrig);      
        //$collectionNode = $parentNode->addChild($propertyDef->name);
        $collectionNode=$this->dom->createElement($propertyDef->name);
        $parentNode->appendChild($collectionNode);
        //$this->addComment($parentNode, "propertyDef {$propertyDef->name}");

        foreach($collection as $key=>$itemData) {

            if (is_array($itemData)) {

                if (count($itemData)==0)
                    continue;
                
                // get childType of this collection $collectionDef
                if ($collectionDef->childIsCollection)  {

                    $collectionDef->collectionTag="GridRow";
                    //$nestedCollectionNode = $collectionNode->addChild($collectionDef->collectionTag);
                    $nestedCollectionNode=$this->dom->createElement($collectionDef->collectionTag);
                    $collectionNode->appendChild($nestedCollectionNode);

                } else {
                    $nestedCollectionNode = $collectionNode;
                }

                foreach($itemData as $childKey=>$childItemData) {

                    if (is_object($childItemData)) {
                        $className = get_class($childItemData);
                        $itemDef = $this->specMgr->findDef($className);
                        if (is_a($itemDef, "ObjDef")) {

                            //$objectNode = $nestedCollectionNode->addChild($itemDef->name);

                            $objectNode=$this->dom->createElement($itemDef->name);
                            $nestedCollectionNode->appendChild($objectNode);

                            $saveReferencesOnly = $propertyDef->getAttribute("saveReferencesOnly",false);     

                            if ($saveReferencesOnly==true) {
                                $this->_saveObjReference($objectNode, $itemDef, $childItemData);
                            } else {
                                $this->_saveObj($objectNode, $itemDef, $childItemData);
                            }   
                            
                        } else if (is_a($itemDef, "CollectionDef")) {
                            throw(new Exception("not done"));
                            //$this->_saveCollection($collectionNode,$itemDef, $itemData);
                        }
                    } else {
                        throw(new Exception("not done"));
                        //$this->_saveScalar($nestedCollectionNode, $childKey, $obj)
                        //$nestedCollectionNode->addChild($childKey,$childItemData);
                    }
                }
            } else {

                if (is_object($itemData)) {
                    
                    $className = get_class($itemData);
                    $itemDef = $this->specMgr->findDef($className);

                    if (is_a($itemDef, "ObjDef")) {
                        
                        //$objectNode = $collectionNode->addChild($itemDef->name);
                        $objectNode=$this->dom->createElement($itemDef->name);
                        $collectionNode->appendChild($objectNode);
                        
                        $saveReferencesOnly = $propertyDef->getAttribute("saveReferencesOnly",false);     
                         
                        if ($saveReferencesOnly==true) {
                            $this->_saveObjReference($objectNode, $itemDef, $itemData);                            
                        } else {
                            $this->_saveObj($objectNode, $itemDef, $itemData);
                        }                        

                    } else if (is_a($itemDef, "CollectionDef")) {
                        //$this->_saveCollection($collectionNode,$itemDef, $itemData);
                        throw(new Exception("not done"));
                    }
                } else {
                    $childNode=$this->dom->createElement($key,$itemData);                    
                    $collectionNode->appendChild($childNode);
                }

            }

//            if ($type=='Object') {
//                $itemDef = $spec->findDef($id);
//                $itemObj = $this->_createObj($spec, $itemDef, $itemData);
//
//                if ($def->isAssociative) {
//                    $obj[$key] = $itemObj;
//                } else {
//                    $obj[] = $itemObj;
//                }
//            } else if ($type=='Collection') {
//
//                $itemDef = $spec->findDef($id);
//                $itemObj = $this->_createCollection($spec, $itemDef, $itemData);
//                if ($def->isAssociative) {
//                    $obj[$key] = $itemObj;
//                } else {
//                    $obj[] = $itemObj;
//                }
//            }
        }
        //return $obj;
    }

    public function _saveObj($parentNode, $def, $obj) {
 
        if (isnull($def)) {
            die();
        }
        foreach ($def->getProperties() as $propertyDef) {
            if ($propertyDef->isScalar()) {

                $this->_saveScalar($parentNode, $propertyDef, $obj);

            } else if ($propertyDef->isObject()) {

                $objectNode=$this->dom->createElement($propertyDef->name);

                $objDef = $this->specMgr->findDef($propertyDef->objectTypeOrig);

                if (isnull($objDef)) {
                    die();
                }                
                $this->_saveObj($objectNode, $objDef, $obj);


                //$parentNode = $topNode->addChild($def->name);
                //throw(new Exception("not done"));
//                $objDef = $spec->findDef($propertyDef->id);
//
//                $propObj = $this->_createObj($spec, $objDef, $data[$propertyDef->id]);
//
//                if ($propertyDef->setter) {
//                    $obj->{$propertyDef->setter}($propObj);
//                } else {
//                    $obj->{$propertyDef->name} = $propObj;
//                }

            } else if ($propertyDef->isCollection()) {
               
                $this->_saveCollection($parentNode, $propertyDef, $obj);

//                if ($propertyDef->getter) {
//                    $obj->{$propertyDef->getter}($collectionObj);
//                } else {
//                    $obj->{$propertyDef->name} = $collectionObj;
//                }
            }
        }
    }
    
    public function _saveObjReference($parentNode, $def, $obj) {
 
        // get the key of the $obj and save those properties, a key 
        // could be made from multple properties
        
        $keyProperties = $obj->getPrimaryKeyProperties();
                
        foreach ($keyProperties as $propertyDef) {

            if ($propertyDef->isScalar()) {
                $this->_saveScalar($parentNode, $propertyDef, $obj);
            } else {
                // something is wrong, how can a primary key be an object or a collection?
            }                    

        }
    }
    
    
    public function genClass($objDef, $sb) {
        
        $sb->addLines(
"
<?php
/**
* Class {$objDef->name}
*
* @author ClassFactory.php genClass
*/
class {$objDef->name}Cls extends SpecCls {
");
        $sb->increaseIndentLevel();

        foreach($objDef->getProperties() as $propDef) {
            $sb->addLine("public \$".$propDef->name."; // ".$propDef->propType. " of type ". $propDef->objectTypeOrig);
        }
        $sb->addLine('',true);
         
        $sb->addLine("public function  __construct() {");
        
        $sb->increaseIndentLevel();

        $sb->addLine("parent::__construct('".$objDef->name."');");
        
        foreach($objDef->getProperties() as $propDef) {
            if ($propDef->propType == "collection") {
                $sb->addLine('$this->'.$propDef->name." = array(); ");
            } else {
                
                $defValue = $propDef->getAttribute("classDefaultValue", Null);
                if (is_null($defValue)) {
                    $sb->addLine('$this->'.$propDef->name." = null; ");
                } else {
                    $sb->addLine('$this->'.$propDef->name." = ".value2php($propDef,$defValue)."; ");
                }
            }
        }

        $sb->decreaseIndentLevel();
        $sb->addLine("}");

        $sb->decreaseIndentLevel();

        $sb->addLines("}
?>");                        
    }

    
    public function createClass($objDef) {
        
        if ($this->specMgr->autoGenClasses) {
            
            $newName = str_replace(":","_",$objDef->name);
            
            $classFileName = $this->specMgr->autoGenClassPath .$newName . "Cls.php";
            
            if(!file_exists($this->specMgr->autoGenClassPath)){
                mkdir($this->specMgr->autoGenClassPath);
            }
            
            // be silent my little code gen
            //echo "Class factory is auto-generating a missing class [$classFileName}\n";
            $sb = new StringBuilder();
            $this->genClass($objDef, $sb);
            file_put_contents($classFileName, $sb->getString());    
            chmod($classFileName,0777);
        }
    }
}
?>
