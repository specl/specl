<?php

/**
 * LookupTransformAction
 *
 * @author Tony
 */
class LookupTransformAction extends TransformAction {
    public $columnName; // column to save data 
    private $map;
    
    public function __construct($columnName, $map, $defaultValue=null) {        
        parent::__construct($defaultValue);        
        $this->columnName = $columnName;                        
        $this->map = $map;
    }  
    
    public function apply($columnName, &$row) {
        
        if (isset($row[$columnName])) {
            $value = $row[$columnName];
        } else {
            $value = null;
        }

        if (isnull($value)) {
            $row[$this->columnName] = $this->defaultValue;
        } else {        
            $mapped = $this->map->lookup($value);
            
            // If the value doesn't have a map, skip it and do nothing
            if(is_null($mapped)){
                // do nothing
            }
            // if the value maps to a null value set the column to null
            else if (strtolower($mapped)==="null") {
                $row[$this->columnName] = null;
            } else {
                $row[$this->columnName] = $mapped;
            }
        }
    }
    
    public function execute(&$row){
        $this->apply($this->columnName,$row);
    }
}


?>
