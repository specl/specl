<?php

/**
 * ColumnTransform
 *
 * @author Tony
 */
class ColumnTransform {
    public $columnName;
    public $actions;
    
    public function __construct($columnName) {
        $this->columnName = $columnName;        
        $this->actions = array();
    }
    
    public function transform(&$row) {      
        //if (isset($row[$this->columnName])) {
            foreach($this->actions as $action) {
                $action->apply($this->columnName, $row);
            }
        //}        
    }    
    
    public function addAction($action) {
        $this->actions[] = $action;
    }
}

?>
