<?php

class CopyColumnTransform {
    private $fromName;
    private $toName;            
    
    public function __construct($params){
        if(isset($params["fromName"])){
            $this->fromName=$params["fromName"];
        }
        
        if(isset($params["toName"])){
            $this->toName=$params["toName"];
        }
    }
    
    public function execute(&$row){
        $row[$this->toName]=$row[$this->fromName];
    }
    
}

?>