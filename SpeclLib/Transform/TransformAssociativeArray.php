<?php
/**
 *  This class stores and exectues a set of transformations based on the array key 
 */
class TransformAssociativeArray {
    protected $transformations;
    
    public function addTransformation($key,$transformation){
        $this->transformations[$key][]=$transformation;
    }
    
    // TODO: change the transform api to take row, params instead of hardcoding columnName
    public function execute(&$row){
        foreach($this->transformations as $key=>$transformations){
            foreach($transformations as $transformation){
                $transformation->apply($key,$row);
            }
        }
    }
}

?>