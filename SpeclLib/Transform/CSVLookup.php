<?php
/**
 * CSVLookupTransform
 *
 * @author Tony
 */
class CSVLookup extends Lookup {
    
    private $filename;
    
    public function __construct($filename) {
        parent::__construct();
        $this->filename = $filename;    
    }
    
    public function load() {

        $columnNames = array();         
        $this->setLoaded();                
        $csv_line = 0;
        $matrix = Array($csv_line => Array());
        
        $row = 0;
        ini_set('auto_detect_line_endings', true);
        $file=file_get_contents($this->filename);
        $csvReader=new CSVReader();
        $fileData=$csvReader->parse($file);       
        foreach($fileData as $data){            
            $num = count($data);
            for ($c=0; $c < $num; $c++) {
                //echo $row. ' ' .$c . ' ' . $data[$c] . "\n";                
                $matrix[$row][] = $data[$c];                 
            }
            $row++;
        }
                
        $rowNumber = 0;
        foreach($matrix as $row) {
            // key,map
            if ($rowNumber>0) {
                if (count($row)==2) {
                    $key = trim($row[0]);
                    $value = trim($row[1]);
                    $this->add($key, $value);
                } else {
                    // row does not have the correct number of columns. skip it.
                }
            }
            $rowNumber++;
        }
        
    }
 
    public function loadOld() {
        // load CSV file.
        $columnNames = array();
         
        $this->setLoaded();        
        $csv = new CSV();    
        
        try {
            $file = file_get_contents($this->filename, true);    
            $matrix = $csv->parse($file);            
        } catch (Exception $e) {
            $msg = "Caught exception: ".  $e->getMessage() . " Can't load CSV {$this->filename}\n";
            throw new Exception($msg);
        }

        $rowNumber = 0;
        foreach($matrix as $row) {
            // key,map
            if ($rowNumber>0) {
                if (count($row)==2) {
                    $key = trim($row[0]);
                    $value = trim($row[1]);
                    $this->add($key, $value);
                } else {
                    // row does not have the correct number of columns. skip it.
                }
            }
            $rowNumber++;
        }
    }       
}
?>
