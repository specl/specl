<?php

/**
 * Transform
 *
 * @author Tony
 */
class Transform {
    
    public $getName;
    public $setName;
    
    public function __construct($params) {   
                
        if (isset($params["getName"])) {
            $this->getName = $params["getName"];
        } else {
            $this->getName = null;
        }

        if (isset($params["setName"])) {
            $this->setName = $params["setName"];
        } else {
            $this->setName = null;
        }        
    }
}

?>
