<?php
/**
 * Description of DGURLTransform
 *
 * @author Tony
 */
class LinkTransform extends Transform {
    public $url;
    public $urlVars;
    public $caption;
    public $captionVars;
        
    public function __construct($params) {
        parent::__construct($params);
        $this->urlVars = array();
        $this->captionVars = array();
    }
    
    public function apply(&$objectData) {
                
        global $app;

        $sqlBuilder = new QueryBuilder();
        $sqlBuilder->setStatement($this->url);
        foreach($this->urlVars as $var) {     
            
            if (isset($objectData[$var])) {
                $value = $objectData[$var];
            } else {
                $value = null;
            }
        
            $sqlBuilder->createParam($var, "function", $value);
        }        
        $url = $sqlBuilder->bindParams();


        $sqlBuilder = new QueryBuilder();
        $sqlBuilder->setStatement($this->caption);
        foreach($this->captionVars as $var) {     
            
            if (isset($objectData[$var])) {
                $value = $objectData[$var];
            } else {
                $value = null;
            }
        
            $sqlBuilder->createParam($var, "function", $value);
        }        
        $caption = $sqlBuilder->bindParams();
        
        $objectData[$this->setName] = '<A HREF=\''.$url.'\'>'.$caption.'<A>';
    }            
}

?>
