<?php

class StringReplaceColumnTransform extends Transform {
    public $columnName;
    public $findString;
    public $replaceWith;
                
    public function __construct($params) {        
        if(isset($params["columnName"])){
            $this->columnName = $params["columnName"];
        }        
        if(isset($params["findString"])){
            $this->findString=$params["findString"];
        }
        if(isset($params["replaceWith"])){
            $this->replaceWith=$params["replaceWith"];
        }
       
    }
    public function execute(&$row){
        if(isset($row[$this->columnName])){
            $value=$row[$this->columnName];
            $row[$this->columnName]=  str_replace($this->findString, $this->replaceWith, $value);
        }
    }
    
    public function apply(&$row){
        $this->execute($row);
    }
}

?>