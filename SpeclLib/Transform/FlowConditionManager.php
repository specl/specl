<?php

/**
 * FlowConditionManager
 *
 * @author Tony
 */

class FlowConditionManager {
    
    public $conditions; // array of conditions
        
    public function __construct() {
        $this->conditions = array();
    }
    
    public function test($value) {

        $valid = true;
        foreach($this->conditions as $condition) {
            if($condition->validate($value)==false) {
                $valid = false;
                break;
            } else {
                continue;
            }
        }
        return $valid;
    }    
    
    public function addCondition($condition) {
        $this->conditions[] = $condition;
    }
    
}

?>
