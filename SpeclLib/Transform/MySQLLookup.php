<?php

/**
 * MySQLLookupTransform
 *
 * @author Tony
 */
class MySQLLookup {
    
    private $dbConn;
    private $statement;
    
    public function __construct($dbConn, $statement) {
        parent::__construct();
        $this->dbConn = $dbConn;
        $this->statement = $statement;    
    }
    
    public function lookup($key) {
        $query = new QueryBuilder();
        
        if ($this->caseSensitivity == false) {
            $key = strtolower($key);
        }
                        
        $query->setStatement($this->statement);
        $query->createParam("key", "string", $key);        
        $sql = $query->bindParams();

        $results =  $this->dbConn->select($sql);
        
        //print $sql."\n";
        
        if ($results!=0)
        {
            $row = $this->dbConn->getRowAssoc($results);

            $value = $row['value'];
            
            if (is_null($value)) {
                print("[$key} not found.\n");
            }

            return $value;
        } else {
            return $this->defaultValue;
        }        
    }
        
}

?>
