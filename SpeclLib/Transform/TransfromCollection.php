<?php
/**
 *  This class stores and exectues a set of transformations on a single row of data
 */
class TransfromCollection {
    protected $transformations=array();
    
    public function addTransformation($transformation){
        $this->transformations[]=$transformation;
    }
    
    public function execute(&$row){
        foreach($this->transformations as $transformation){
                $transformation->execute($row);            
        }
    }
}
?>