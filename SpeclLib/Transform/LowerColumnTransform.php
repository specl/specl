<?php

/**
 * LowerColumnTransform
 *
 * @author Tony
 */
class LowerColumnTransform {
        
    public $columnName; // column to save data 
                
    public function __construct($columnName) {        
        $this->columnName = $columnName;
    }
        
    public function apply($columnName, &$row) {        
        $value = $row[$columnName];    
        if (!isnull($value)) {
             $row[$this->columnName] = strtolower($value);
        }
    }
    
    public function execute(&$row){
        $this->apply($this->columnName,$row);
    }
}

?>
