<?php
/**
 * Description of SQLConstraintTransform
 *
 * @author Tony
 */
class SQLLookupTransform2 extends Transform  {
    public $connectionName;
    public $statement;
    public $fkTable;
    public $fkSearchColumn;
    public $fkReturnColumn;
    public $defaultValue;
    public $fkSearchColumnDataType;
    
    public $options;
    public $whereClause;
    
    // added by ALL on June 21, 2011
    public $includeInQuery; // true or false, added to prevent Constraints from being
    // used in the Query Buildering process fro data tables

    public function __construct($params)
    {        
        parent::__construct($params);
         
        if (isset($params["connectionName"])) {
            $this->connectionName = $params["connectionName"];
        } else {
            $this->connectionName = null;
        }

        if (isset($params["fkTable"])) {
            $this->fkTable = $params["fkTable"];
        } else {
            $this->fkTable = null;
        }        

        if (isset($params["fkSearchColumn"])) {
            $this->fkSearchColumn = $params["fkSearchColumn"];
        } else {
            $this->fkSearchColumn = null;
        }        

        if (isset($params["fkReturnColumn"])) {
            $this->fkReturnColumn = $params["fkReturnColumn"];
        } else {
            $this->fkReturnColumn = null;
        }        

        if (isset($params["fkSearchColumnDataType"])) {
            $this->fkSearchColumnDataType = $params["fkSearchColumnDataType"];
        } else {
            $this->fkSearchColumnDataType = 'string';
        }    
        
        if(isset($params["returnNullOnFailure"])){
            $this->returnNullOnFailure=$params["returnNullOnFailure"];
        }
        else{
            $this->returnNullOnFailure=false;
        }
        
        $this->options = array();
        $this->whereClause = null;
        $this->includeInQuery = true; // default is true so thing keep working as before        
    }

    public function apply(&$objectData) {
                
        global $app;

        if (is_null($this->getName)) {
            return;
        }
        
        if (is_null($this->setName)) {
            return;
        }
                
        if (is_array($objectData)) {
            if (isset($objectData[$this->getName])) {
                $value = $objectData[$this->getName];
            } else {
                $value = null;
            }
        } else {
            if (isset($objectData->{$this->getName})) {
                $value = $objectData->{$this->getName};
            } else {
                $value = null;
            }
        }
        
        if (isnull($value)) {
            if (is_array($objectData)) {
                $objectData[$this->setName] = $this->defaultValue;                
            } else {
                $objectData->{$this->setName} = $this->defaultValue;                
            }
        } else {        

            $lookup = SQLLookup($this->connectionName, $this->fkTable, $this->fkSearchColumn, $this->fkReturnColumn, $value);
            
            if($lookup===$value && $this->returnNullOnFailure){
                $lookup=null;
            }
            
            if (is_array($objectData)) {
                if (is_null($lookup)) {
                    $objectData[$this->setName] = null; //"MISSING";
                } else {
                    $objectData[$this->setName] = $lookup;
                }
            } else {
                if (is_null($lookup)) {
                    $objectData->{$this->setName} = null; //"MISSING";
                } else {
                    $objectData->{$this->setName} = $lookup;
                }                
            }
        }
    }
    
    public function execute(&$data){
        $this->apply($data);
    }
    
    public function generate()
    {
        global $app;
                
        $dbConn = $app->getConnection($this->connectionName);
        
        if (is_null($dbConn)) {
            // connection name not found or some other problem
            return;
        }
        
        $whereClause = "";
        
        if ($this->whereClause) {
            $whereClause .= "WHERE " . $this->whereClause;            
            // do a substitution
        }            
        
        $this->options = array();
        
        $this->statement = "select `{$this->idColumnName}`,`{$this->displayColumnName}` from `{$this->tableName}` {$whereClause} order by `{$this->displayColumnName}`;";                
        $results = $dbConn->select($this->statement);

        if ($results!=0)
        {
            if ($this->addEmptyOption == true) {
                $this->options["SNULL"] = "";
            }            
            while($row = $dbConn->getRowEnum($results)) {
                if (count($row)==1) {
                    $this->options[$row[0]] = $row[0];
                } else if (count($row)>1) {
                    $this->options[$row[0]] = $row[1];
                }                                        
            }
        }               
    }

}
?>
