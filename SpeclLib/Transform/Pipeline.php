<?php

/**
 * Pipeline
 *
 * @author Tony
 */
class Pipeline {
    
    private $dbConn;
    public $sql;
    private $transforms;
    public $data;
    
    public function __construct($dbConn) {        
        $this->dbConn = $dbConn;
        $this->transforms = array();
        $this->data = array();
    }
    
    public function addTransform($transform) { 
        $this->transforms[] = $transform;
    }
    
    public function execute() {
        
        $results = $this->dbConn->select($this->sql);
        if ($results==0) {
            $errMsg = "mysql_errno() MySQL error ".mysql_errno().": ".mysql_error()."\nWhen executing:\n{$this->sql}\n";             
            print "select failure {$errMsg}\n";                        
            return;
        } 
        
        $i=0;
        while($row = $this->dbConn->getRowAssoc($results)) {                       
            //print "{$i} {$row}\n";
//           foreach($row as $key=>$value) {
//               
//               if (is_null($value)) {
//                   $null = "True";
//               } else {
//                   $null = "False";
//               }
//               
//               print "{$i} ($key} {$value} ".$null."\n";  
//           }
           $this->applyTransforms($i,$row);
           $this->data[] = $row;
           $i++;
        }        
    }
    
    private function applyTransforms($rowNum,&$row) {
        
        //print_r($row);
        
        foreach($this->transforms as $transform) {                                    
            $transform->transform($row);            
        }

                
        //print "{$rowNum}\n";
        //print_r($row);
    }
    
    
    public function extract($columns, $columNames, $filename) {
        $fp = fopen($filename, 'w');

        fputcsv($fp, $columNames);
        
        foreach ($this->data as $row) {
            $fields = array();
            foreach($columns as $column) {
                if (isset ($row[$column])) {
                    $value = $row[$column];
                } else {
                    $value = NULL;
                }
                $fields[] = $value;
            }
            fputcsv($fp, $fields);
        }

        fclose($fp);        

    }
}
?>
