<?php

/**
 * SplitColumnTransform
 *
 * @author Tony
 */
// ginger: sddddddwZA
class SplitColumnTransform extends TransformAction {
        
    public $columnName; // column to save data 
    public $splitColumnName; // column to save data 
    public $extractor;
                
    public function __construct($columnName, $splitColumnName, $extractor, $defaultValue=null) {        
        parent::__construct($defaultValue);        
        $this->columnName = $columnName;                        
        $this->splitColumnName = $splitColumnName;                        
        $this->extractor = $extractor;
    }
    
    public function test($value) {

        $valid = true;
        foreach($this->conditions as $condition) {
            if($condition->validate($value)==false) {
                $valid = false;
                break;
            } else {
                continue;
            }
        }
        return $valid;
    }    
    
    public function apply($columnName, &$row) {
        
        $value = $row[$columnName];        
        
        if (!is_null($value)) {
            $extracted = $this->extractor->extract($value);

            if (isnull($extracted)) {
                $row[$this->splitColumnName] = $this->defaultValue;
            } else {        
                $row[$this->splitColumnName] = $extracted;
            }
        } else {
            $row[$this->splitColumnName] = $this->defaultValue;
        }
    }
    
    public function execute(&$row){
        $this->apply($this->columnName,&$row);
    }
}

?>
