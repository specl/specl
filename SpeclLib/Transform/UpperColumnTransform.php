<?php

/**
 * UpperColumnTransform
 *
 * @author Tony
 */
class UpperColumnTransform {
        
    public $columnName; // column to save data 
                
    public function __construct($columnName) {        
        $this->columnName = $columnName;
    }
        
    public function apply($columnName, &$row) {
        
        $value = $row[$this->columnName];
    
        if (!isnull($value)) {
             $row[$this->columnName] = strtoupper($value);
        }
    }
    
    public function execute(&$row){
        $this->apply($this->columnName,$row);
    }
}

?>
