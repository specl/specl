<?php
class Lookup {
    private $map;
    protected $caseSensitivity = false;
    protected $defaultValue = 0;
    private $_isLoaded;
        
    public function __construct() {
        $this->_isLoaded = false;        
        $this->map = array();
        $this->caseSensitivity = false;
        $this->defaultValue = null;
    }
    
    public function add($key, $value) {
        
        if ($this->caseSensitivity == false) {
            $key = strtolower($key);
        }
        
        $this->map[$key] = $value;
    }

    public function lookup($key) {
        
        if ($this->isLoaded()==false) {
            $this->load();
        }
        
        if ($this->caseSensitivity == false) {
            $key = strtolower($key);
        }
        
        if (array_key_exists($key, $this->map)) {
            return $this->map[$key];
        } else {
            return $this->defaultValue;
        }
    }
    
    public function setCaseSensitivity($value) {
        if (is_bool($value)) {
            $this->caseSensitivity = $value;
        }
    }

    public function setDefaultValue($value) {
        $this->defaultValue = $value;
    }
    
    public function load() {
        // override this if you want.
        $this->setLoaded();
    }
    
    public function isLoaded() {
        return $this->_isLoaded;
    }
    
    public function setLoaded() {
        $this->_isLoaded = true;        
    }
}
?>
