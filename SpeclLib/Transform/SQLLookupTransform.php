<?php
/**
 * Description of SQLLookupTransform
 *
 * @author tony
 */
class SQLLookupTransform extends Transform {
    
    public $connectionName;
    public $sql;
    public $defaultValue;
    
    public function __construct($params) {
        parent::__construct($params);
        
        if (isset($params["connectionName"])) {
            $connectionName = $params["connectionName"];
        } else {
            $connectionName = null;
        }

        if (isset($params["sql"])) {
            $sql = $params["sql"];
        } else {
            $sql = null;
        }

        if (isset($params["defaultValue"])) {
            $defaultValue = $params["defaultValue"];
        } else {
            $defaultValue = null;
        }
        
        if(isset($params["returnNullOnFailure"])){
            $this->returnNullOnFailure=$params["returnNullOnFailure"];
        }
        else{
            $this->returnNullOnFailure=false;
        }
        
        parent::__construct($params);

        $this->connectionName = $connectionName;
        $this->sql = $sql;        
        $this->defaultValue = $defaultValue;   
        
        
    }
    
        
    public function apply(&$objectData) {
                
        global $app;

        if (is_null($this->getName)) {
            return;
        }
        
        if (is_null($this->setName)) {
            return;
        }
        
        $app->setGlobalData("OBJ",$objectData);
         
        if (is_array($objectData)) {
            if (isset($objectData[$this->getName])) {
                $value = $objectData[$this->getName];
            } else {
                $value = null;
            }
        } else {
            if (isset($objectData->{$this->getName})) {
                $value = $objectData->{$this->getName};
            } else {
                $value = null;
            }            
        }
        
        if (isnull($value)) {
            if (is_array($objectData)) {
                $objectData[$this->getName] = $this->defaultValue;
            } else {
                $objectData->{$this->getName} = $this->defaultValue;
            }
        } else {        
    
            $sqlBuilder = new QueryBuilder();
            $dbConn = $app->getConnection($this->connectionName);

            if (is_null($dbConn)) {
                // connection name not found or some other problem
                return;
            }
                
            $sqlBuilder->setStatement($this->sql);
            $sqlBuilder->createParam("value", "string", $value);
            $sql = $sqlBuilder->bindParams();
            $sql = $app->fillInGlobals($sql);    
            $results = $dbConn->select($sql);

            if ($results!=0)
            {                
                //$row = $dbConn->getRowAssoc($results);
                try {
                    $row = $dbConn->getRowEnum($results);
                } catch(Exception $e) {
                    $errMsg = "Column missing from SQL results;" . $e->getMessage();
                    $log->error("SQLLookupTransform->apply",$errMsg);
                }
                if($row===false){
                    if($this->returnNullOnFailure){
                        $lookup=null;
                    }
                    else{
                        $lookup=$value;
                        $lookup=null;
                    }
                }
                else{
                    $lookup = $row[0];
                }
            } else {
                $lookup = null;
            }                     
            
            if (!isnull($lookup)) {            
                if (is_array($objectData)) {
                    if (is_null($lookup)) {
                        $objectData[$this->setName] = null; //"MISSING";
                    } else {
                        $objectData[$this->setName] = $lookup;
                    }
                } else {
                    if (is_null($lookup)) {
                        $objectData->{$this->setName} = null; //"MISSING";
                    } else {
                        $objectData->{$this->setName} = $lookup;
                    }                
                }
            }
        }
    }              
}
?>
