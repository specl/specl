<?php

/**
 * LowerTransform
 *
 * @author Tony
 */
class LowerTransform extends Transform
{				
	public function transform($in) {
		return strtolower($in);
	}
}
?>
