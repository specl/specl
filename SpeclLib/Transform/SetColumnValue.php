<?php

class SetColumnValue extends Transform {
    private $columnToSet;
    private $value;
    
    public function __construct($params){
        if(isset($params["columnToSet"])){
            $this->columnToSet=$params["columnToSet"];
        }
        
        if(isset($params["value"])){
            $this->value=$params["value"];
        }            
    }
    
    public function execute(&$row){
        $row[$this->columnToSet]=$this->value;
    }
    
    public function apply(&$row){
        $this->execute($row);
    }
    
}

?>