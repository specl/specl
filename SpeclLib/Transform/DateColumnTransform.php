<?php

/**
 * DateTransform
 *
 * @author Tony
 */
class DateColumnTransform {
        
    public $columnName; // column to save data 
                
    public function __construct($columnName) {        
        $this->columnName = $columnName;
    }
        
    public function apply($columnName, &$row) {        
        $value = $row[$columnName];    
                
        $ts = strtotime($value);

        if ($ts === false)
        {
            $row[$this->columnName] = date('Y-m-d H:i:s');
        }
    }
    
    public function execute(&$row){
        $this->apply($this->columnName,&$row);
    }
}

?>
