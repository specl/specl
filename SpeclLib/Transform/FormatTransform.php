<?php
/**
 * Description of FormatTransform
 *
 * @author tony
 */
class FormatTransform extends Transform {
    
    public $format;
    public $defaultValue;
    public $setName;
    public $params;
    
    public function __construct($params) {
        parent::__construct($params);
        
        if (isset($params["format"])) {
            $format = $params["format"];
        } else {
            $format = null;
        }

        if (isset($params["defaultValue"])) {
            $defaultValue = $params["defaultValue"];
        } else {
            $defaultValue = null;
        }

        if (isset($params["setName"])) {
            $setName = $params["setName"];
        } else {
            $setName = null;
        }
        
        if (isset($params["params"])) {
            $params = $params["params"];
        } else {
            $params = null;
        }
        
        
        parent::__construct($params);
        $this->format = $format;        
        $this->defaultValue = $defaultValue;   
        $this->setName = $setName;   
        $this->params = $params;
    }
            
    public function apply(&$objectData) {
                
        global $app;

        if (is_null($this->format)) {
            return;
        }
        
        if (is_null($this->setName)) {
            return;
        }
        
        $pattern = '%[0-9]+%';
        $numMatches = preg_match_all($pattern, $this->format, $matches,PREG_OFFSET_CAPTURE);
                
        $out = ""; // transformation output, which is created by this function 
        
        $keepCurrentIndex = 0;
        
        foreach($matches[0] as $match) {
            
            $paramIndex = $match[0];
            $position = $match[1] - 1;
                        
            $endOfParamIndex = strlen($paramIndex)+1;
            
            $keep = substr($this->format, $keepCurrentIndex, $position - $keepCurrentIndex);
            
            $keepCurrentIndex = $position + $endOfParamIndex + 1;
            
            $out .= $keep;
            
            if (isset($this->params[$paramIndex])) {
                $key = $this->params[$paramIndex];
                
                if(is_array($objectData)){
                    if (isset($objectData[$key])) {
                        $value = $objectData[$key];                    
                        $out .= $value;
                    } 
                }
                else{
                    if (isset($objectData->{$key})) {
                        $value = $objectData->{$key};                    
                        $out .= $value;
                    }  
                }                 
            } else {
                // error!!!! the $paramIndex is not found in the param array
            }           
        }

        $position = strlen($this->format);         
        $keep = substr($this->format, $keepCurrentIndex, $position - $keepCurrentIndex);            
        $out .= $keep;        
        
        
        if (is_array($objectData)) {
            if (is_null($out)) {
                $objectData[$this->setName] = null; //"MISSING";
            } else {
                $objectData[$this->setName] = $out;
            }
        } else {
            if (is_null($out)) {
                $objectData->{$this->setName} = null; //"MISSING";
            } else {
                $objectData->{$this->setName} = $out;
            }                
        }
            
    }          
    
}

?>
