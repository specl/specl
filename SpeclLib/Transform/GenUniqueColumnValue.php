<?php

/**
 * This Transformation keeps track of all the values it has seen for a particular column.
 * If it encounters a duplicate value a number is appended to the end of the value, starting
 * at the number 2. The number is incremented until a unique value is created.
 */
class GenUniqueColumnValue extends Transform {
    private $columnName;
    private $startingInteger;
    private $valuesSeen;
    private $caseInsensitive;
    private $lookup;
    
    public function __construct($params){
        if(isset($params["columnName"])){
            $this->columnName=$params["columnName"];
        }
        else{
            throw new Exception("The parameter 'columnName' is required");
        }
        
        if(isset($params["startingInteger"])){
            $this->startingInteger=$params["startingInteger"];
        }
        else{
            $this->startingInteger=2;
        }
        
        if(isset($params["caseInsensitive"])){
            $this->caseInsensitive=$params["caseInsensitive"];
        }
        else{
            $this->caseInsensitive=false;
        }
        
        if(isset($params["lookup"])){
            $this->lookup=$params["lookup"];
        }
        
        $this->valuesSeen=array();
    }
    
    public function execute(&$row){
        $value=$row[$this->columnName];
        if(!is_null($value)){
            $found=$this->checkIsFound($value);
            $this->valuesSeen[$value]=true;
            if($found){
                $intToAdd=$this->startingInteger;
                $newValue=$found." ".$intToAdd;
                
                $foundNew=$this->checkIsFound($newValue);
                // keep incrementing the integer at the end until a unique value is generated.
                while($foundNew){
                    $intToAdd+=1;
                    $newValue=$found." ".$intToAdd;
                    $foundNew=$this->checkIsFound($newValue);
                }  
                #$newValue=$found." ".$intToAdd;
                $this->valuesSeen[$newValue]=true;
                $row[$this->columnName]=$newValue;
            }
        }
    }
    
    public function checkIsFound($value){
        if(isset($this->valuesSeen[$value])){
            return $value;
        }else{
            $value=$this->lookup->lookupValue(array("key"=>$value,"enableCache"=>false));
            return $value;
        }
        
    }
}

?>
