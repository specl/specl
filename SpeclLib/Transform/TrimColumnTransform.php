<?php

/**
 * TRimCOlumnTransform
 *
 * @author Tony
 */
class TrimColumnTransform {
        
    public $columnName; // column to save data 
                
    public function __construct($columnName) {        
        $this->columnName = $columnName;
    }
        
    public function apply($columnName, &$row) {
        
        $value = $row[$columnName];
    
        if (!isnull($value)) {
             $row[$this->columnName] = trim($value);
        }
    }
    
    public function execute(&$row){
        $this->apply($this->columnName,$row);
    }
}

?>
