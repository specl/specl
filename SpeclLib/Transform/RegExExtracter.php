<?php

/**
 * RegExExtracter
 *
 * @author Tony
 */
class RegExExtracter {
    
    public $pattern;
    public $matches;
    public $returnAll;
    
    public function __construct($pattern,$returnAll=False) {
        $this->pattern = $pattern;
        $this->returnAll=$returnAll;
    }

    public function extract($value) {                
        preg_match_all($this->pattern, $value, $this->matches, PREG_PATTERN_ORDER);
        
        if (count($this->matches[0])==0) {
            // no match
            return null;
        } else {            
            if (count($this->matches[0])>1) {            
                // more than one match...
                if ($this->returnAll===false) {
                    return $this->matches[0][0];
                } else {
                    return $this->matches[0];
                }
            } else {
                return $this->matches[0][0];
            }
        }
    }
}

?>
