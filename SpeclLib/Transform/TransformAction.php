<?php

/**
 * TransformAction
 *
 * @author Tony
 */
class TransformAction extends FlowConditionManager {
    
    public $defaultValue;
    
    public function __construct($defaultValue=null) {        
        parent::__construct();        
    }
    
    public function apply($value) {
        
        if (isnull($value)) {
            return $this->defaultValue;
        }
                
        if ($this->flowCondition->test($value)) {
                        
        } else {
            return $this->defaultValue;
        }                
    }    
}

?>
