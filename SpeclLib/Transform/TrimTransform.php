<?php

/**
 * TrimTransform
 *
 * @author Tony
 */
class TrimTransform extends Transform
{				
	public function transform($in) {
		return trim($in);
	}
}


?>
