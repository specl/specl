<?php
class StringRegexReplaceColumnTransfrom {  
    protected $columnName;
    protected $regexPattern;
    protected $replaceWith;
                
    public function __construct($params) {        
        if(isset($params["columnName"])){
            $this->columnName = $params["columnName"];
        }        
        if(isset($params["findRegex"])){
            $this->findRegex=$params["findRegex"];
        }
        if(isset($params["replaceWith"])){
            $this->replaceWith=$params["replaceWith"];
        }
       
    }
    public function execute(&$row){
        if(isset($row[$this->columnName])){
            $value=$row[$this->columnName];
            $row[$this->columnName]=  preg_replace($this->findRegex, $this->replaceWith, $value);
        }
    }
    
}
?>