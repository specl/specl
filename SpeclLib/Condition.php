<?php

const LVL_ERROR = 0;
const LVL_WARNING = 1;
const LVL_INFO = 2;

class Condition
{    
    public $ruleName;  // same as the Class name
    public $description;      
    
    public $getValueFromPropertyId;
    public $useOriginalValue;
    public $errorLevel; // ERROR, WARNING, INFO
    public $userData;
    private $errorMsgs;                
    public $errorCode;        
    
    public $validated; // was validate called?  true or false
    public $valid;  // true or false
        
    public $customErrorMsg;
    public $useCustomErrorMsg;
    public $name; //user defined name of this rule
    public $enabled;
    
    public function __construct($params=array()) {
        
        $this->errorMsgs = array();
        
        if (isset($params["getValueFromPropertyId"])) {
            $this->getValueFromPropertyId = $params["getValueFromPropertyId"];
        } else {
            $this->getValueFromPropertyId = null;
        }

        if (isset($params["useOriginalValue"])) {
            $this->useOriginalValue = $params["useOriginalValue"];
        } else {
            $this->useOriginalValue = false;
        }
        
        if (isset($params["description"])) {
            $this->description = $params["description"];
        } else {
            $this->description = Null;
        }        
        
        if (isset($params["level"])) {
            // TODO : Implement levels
            // validate is this a correct level            
            $this->level = $params["level"];                        
        } else {
            $this->level = LVL_ERROR;
        }                
        
        if (isset($params["userData"])) {
            $this->userData = $params["userData"];
        } else {
            $this->userData = "Nothing";            
        }                  
        
        if (isset($params["errorMsg"])) {
            $this->errorMsgs[] = $params["errorMsg"];
        }     
        
        if (isset($params["errorCode"])) {
            $this->errorCode = $params["errorCode"];
        } else {
            $this->errorCode = "none";
        }           
         
        if (isset($params["customErrorMsg"])) {
            $this->customErrorMsg = $params["customErrorMsg"];
        } else {
            $this->customErrorMsg = Null;
        }    
        
        if (isset($params["useCustomErrorMsg"])) {
            $this->useCustomErrorMsg = ConvertToBool($params["useCustomErrorMsg"], false);
        } else {
            $this->useCustomErrorMsg = false;
        }    
        
        if (isset($params["name"])) {
            $this->name = $params["name"];
        } else {
            $this->name = uniqid();       
        }              
    
        $this->ruleName = Null;     
        
        $this->enabled = true;
        
    }

    public function validate($value, $objData=null)
    {
       throw new Exception("Function must be implemented in the subclass");
    }

    public function getRuleName() {
                
        if (isnull($this->ruleName)) {
            $this->ruleName = get_class($this);            
        }             
        
        return $this->ruleName;        
    }
    
    public function getErrorString($sep=Null)
    {        
        if (($this->useCustomErrorMsg === false)&&(!isset($this->customErrorMsg))) {
            $sep=" ";

            $msg="";
            if ($this->valid==false) {                    
                $this->getRuleName();
                //$msg = "(".$this->ruleName.")".$sep;
                foreach($this->errorMsgs as $errMsg) {
                    $msg .= $errMsg.$sep;
                }        
            }        
            return $msg;
        } else {
            return $this->customErrorMsg;            
        }
    }
    
    
    public function setErrorMsg($value)
    {
        $this->errorMsgs=array();
        $this->errorMsgs[] = $value;
    }

    public function clearErrorMsg($value)
    {
        $this->errorMsgs=array();        
    }
    
    /*
     *  When using this function you must remember to clear all of the error messages,
     *  every time property validation is called. This leads to trouble when calling
     *  validation of the same objDef multiple times, like in file upload. For now, 
     *  this function is not used anywhere.
     */
    public function addErrorMsg($value)
    {
        $this->errorMsgs[] = $value;
    }
        
    public function getValueFromPropertyId() {       
       return $this->getValueFromPropertyId;
    }
}
?>
