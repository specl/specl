<?php
global $specsTransformed; //global, so wrong.
$specsTransformed = array();

class ObjDef extends BaseDef {

    public $guid;
    public $name;
    public $properties;    
    public $propertiesOrdered;    
    public $primaryKey; //an array or Property Ids
    public $isScalar;
    public $requiredProperties;
    public $optionalProperties;
    public $validationErrors;
    public $ranValidation;
    public $constructorParameters;
    public $currentPath;    
    
    public $specDef; // a class that is used to define and transform a ObjDef
    public $filters;
    
    public $modes; // track modes
    public $states; // track states    
    public $actionMode;
    
    
    public function  __construct($params) {        
        
        $this->guid = uniqid();
        
        if (is_array($params)) {
            if (isset($params["id"])) {
                $id = $params["id"];
            }
        } else {
            $id = $params;
        }

        $id = str_replace(":", "_", $id);
                
        $this->name = $id;

        $id = strtolower($id);

        if (is_array($params)) {
            if (isset($params["domain"])) {
                $domain = $params["domain"];
            } else {
                $domain = "sys";
            }
        } else {
            $domain = "sys";  // the default domain is sys
        }
        
        parent::__construct($id, 'object', $domain);

        $this->properties = array();
        $this->propertiesOrdered=array();
        //$this->createProperty("__type","string");
        $this->requiredProperties=array();
        $this->optionalProperties=array();
        $this->primaryKey = array();
        $this->validationErrors=array();
        $this->isScalar = false;
        $this->constructorParameters=array();
        $this->ranValidation=false;
        $this->currentPath="";
        
        $this->specDef=null; // a specDef is not required.  A ObjDef can be created without
        //a specDef class, this makes it hard to transform it, so if you need to transform
        //a ObjDef you'll have to create a SpecDef anyway, and add it to the ObjDef....                
        $this->currentPath=new PathStack("/");
        $this->filters=new SQLWhere();
        
        $this->modes = Array();        
        $this->states = Array();        
        
        $this->states[$this->name]=Array();
        
        
        $this->modeAction = "";
        
    }
    
    public function addSubType($objDefId) {
        
    }
    
    // addPropertiesFromArray is a convenience function that adds properties
    // from an array of properties
    
    public function addPropertiesFromArray($properties)
    {
        foreach($properties as $property) {
            $propId = strtolower($property->id);
            $property->id = $propId;
            $this->addProperty($property);
        }
    }

    // addPropertiesFromArray is a convenience function that creates a new PropDef
    // and adds it as long as the propId does not alread exist
    
    public function createProperty($propId, $propType) {

        $propId = str_replace(":", "_", $propId); // XXX
        $propType = str_replace(":", "_", $propType); // XXX
        //
        //$propType = strtolower($propType);

        if (array_key_exists(strtolower($propId), $this->properties)==false) {
            $propObj = new PropDef(array('id'=>$propId,'propType'=>'object','objectType'=>$propType));
            $this->properties[strtolower($propId)] = $propObj;
            $this->propertiesOrdered[]=$propObj;            
            //$this->states[$this->name][$propId]=Array();            
            return $propObj;
        } else {
            //$log->warn("","createProperty propId:{$propId} propType:{$propType} failed, a property with the same propId already exists.");
            $propObj = $this->properties[strtolower($propId)];
            return $propObj;
        }
    }
    
    public function copyProperty($propId, $copyProp) {
    
        $propId = str_replace(":", "_", $propId); // XXX        

        if (array_key_exists(strtolower($propId), $this->properties)==false) {
            $propObj = new PropDef(array('id'=>$propId,'propType'=>'object','objectType'=>$copyProp->objectTypeOrig));
            
            foreach($copyProp->attributes as $mode=>$attributes) {
                foreach($attributes as $attr) {
                    $propObj->attributes[$mode][$attr->key]=$attr;
                }                
            }
        
            // copy all constraints
            $propObj->constraint=$copyProp->constraint;
            
            // copy all conditions
            foreach($copyProp->conditions as $mode=>$conditions) {
                foreach($conditions as $condition) {
                    $propObj->conditions[$mode][]=$condition;
                }                
            }            
            
            $this->properties[strtolower($propId)] = $propObj;
            $this->propertiesOrdered[]=$propObj;
            
            return $propObj;
        } else {
            //$log->warn("","createProperty propId:{$propId} propType:{$propType} failed, a property with the same propId already exists.");
            $propObj = $this->properties[strtolower($propId)];
            return $propObj;
        }
    }
    
    // Add all the properties, using objdef->id as in new id of property
    public function addAllPropsOfObjDef($objDef) {
        foreach($objDef->properties as $propDef) {
            $id = $objDef->id . '/' . $propDef->id;
            $clonedPropDef = clone $propDef;
            $clonedPropDef->id = $id;
            $this->addProperty($clonedPropDef);
        }
    }
   
    public function addProperty($propObj) {
        $key = strtolower($propObj->id);
        if (array_key_exists($key, $this->properties)==false) {
            $this->properties[$key] = $propObj;
            $this->propertiesOrdered[]=$propObj;
            return $propObj;
        }
    }
    
    public function removeOrderedProperty($key) {
        $index=0;
        foreach($this->propertiesOrdered as $propDef) {
            if ($key == $propDef->id) {
                unset($this->propertiesOrdered[$index]);      
            }
            $index++;
        }
    }
    
    public function removeProperty($propDef) {
        $key = strtolower($propDef->id);        
        $this->removeOrderedProperty($key);
                
        if (array_key_exists($key, $this->properties)==true) {
            unset($this->properties[$key]);            
        }
    }

    public function createCollection($propId, $propType) {
        //global $log;

        $propId = str_replace(":", "_", $propId); // XXX
        $propType = str_replace(":", "_", $propType); // XXX       

        if (array_key_exists(strtolower($propId), $this->properties)==false) {
            $propObj = new PropDef(array('id'=>$propId,'propType'=>'collection','objectType'=>$propType));

            $this->properties[strtolower($propId)] = $propObj;
            
            $this->propertiesOrdered[]=$propObj;
            
            return $propObj;
        } else {
            //$log->warn("","createCollection propId:{$propId} propType:{$propType} failed, a collection with the same propId already exists.");
            $propObj = $this->properties[$propId];
            return $propObj;
        }
    }

    public function addKey($propId) {

        $propId = strtolower($propId);

        if (array_key_exists($propId, $this->properties)==true) {

            $propObj = $this->properties[$propId];

             // just being scalar is enough... ALL 2011-03-07
            //if ($propObj->isScalar()) {
                if (array_key_exists($propId, $this->primaryKey)==false) {
                    $this->primaryKey[$propId] = $propId;
                }
            //}
        }
    }

    public function addPropDefToPrimaryKey($propDefs) {

        foreach($propDefs as $propdef) {
            $this->addKey($propdef->id);
        }
    }
    
    public function getPrimaryKey() {
        return $this->primaryKey;  // will return an array
    }
    
    public function hasPrimaryKey($propName){
        $propName=strtolower($propName);
        return array_key_exists($propName, $this->primaryKey);
    }

    // Returns an array of PropDefs that make up the Primary Key of
    // this ObjDef
    public function getPrimaryKeyProperties() {
       
        $props = array();
       
        $pKeys=array_keys($this->primaryKey);        
                  
        foreach($pKeys as $key) {
            $props[] = $this->getProperty($key);    
        }
        
        return $props;
    }
       
    public function getPrimaryKeyVarList() {
        
        $list = array();
        
        foreach($this->primaryKey as $propId) {
            
            $propDef = $this->getProperty($propId);
            $list[] = "$".$propDef->name;
        }
        
        $primaryKeysList = implode(",",$list);
        
        return $primaryKeysList;
    }
       
    public function isPartOfPrimaryKey($propId) {
        $propId = strtolower($propId);
        if (array_key_exists($propId, $this->primaryKey)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function isProperty($propId) {

        $propId = strtolower($propId);
        $propId = str_replace(":", "_", $propId); // XXX

        if (array_key_exists($propId, $this->properties)==true) {
            return true;
        } else {
            return false;
        }
    }

    public function isRequiredProperty($propId) {

        $propId = strtolower($propId);

        if (array_key_exists($propId, $this->requiredProperties)==true) {
            return true;
        } else {
            return false;
        }
    }

    public function getProperty($propId) {

        $propId = strtolower($propId);
        $propId = str_replace(":", "_", $propId); // XXX

        if (array_key_exists($propId, $this->properties)) {
            return $this->properties[$propId];
        } else {
            return null;
        }
    }
      
    public function setAttributeAllProperties($attributeKey, $value) {
        
        foreach($this->properties as $propDef) {
            $propDef->setAttribute($attributeKey, $value);            
        }
    }
  
    public function setPropertyAttribute($propertyId, $key, $value,$mode=null) {
         $prop = $this->getProperty($propertyId);
         
         if (!isnull($prop)) {
             $prop->setAttribute($key, $value, $mode);
         }
    }
    
    public function setIsScalar($value) {
        if (is_bool($value)) {
            $this->isScalar = $value;
        }
    }

    public function isScalar() {
        return $this->isScalar;
    }

    public function addRequiredProperty($propId)
    {
        global $log;

        if ($this->isProperty($propId))
        {
            $propId = strtolower($propId);
            
            if(array_key_exists($propId,$this->requiredProperties)==false)
            {
                $this->requiredProperties[$propId]=true;
            }
            else
            {
                $log->warning("ObjDef->addRequiredProperty","the required property {$propId} already exists.");
            }
        }
        else
        {
            $log->error("ObjDef->addRequiredProperty","{$propId} is not defined.");
            throw new Exception("Error: (ObjDef->addRequiredPropery) {$propId} is not defined.");
        }
    }

    public function addOptionalProperty($propId,$defaultValue=null) {
        global $log;
        
        if ($this->isProperty($propId)) {
            $propId = strtolower($propId);
            
            if(array_key_exists($propId,$this->optionalProperties)==false) {
                $this->optionalProperties[$propId]=$defaultValue;
            } else {
                $log->warning("ObjDef->addOptionalProperty","the optional property {$propId} already exists.");
            }
        } else {
            $log->error("ObjDef->addOptionalProperty","{$propId} is not defined.");
            throw new Exception("Error: (ObjDef->addOptionalProperty) {$propId} is not defined.");
        }
    }

    public function checkRequiredProperties($objData, $mode)
    {       
        // Check to make sure all of the required properties are in the data.
        $missingParameters=array();
        $lookup=new LookUpCondition($objData,true);        
        foreach($this->requiredProperties as $key=>$value) {
            if($lookup->validate($key)) {
                continue;
            } else {
                $validationError=new ValidationError($this->name,$key,"requiredProperty",$this->currentPath,"Required Property Missing",100,1);
                $this->validationErrors[]=$validationError;
            }
        }
    }

    public function checkValidProperties($objData) {

        foreach ($objData as $key=>$value) {
            if ($this->isProperty($key)==false) {
                $validationError=new ValidationError($this->name,$key,"isValidProperty",$this->currentPath,"Not A Valid Property {$key}",100,2);
                $this->validationErrors[]=$validationError;
            }
        }        
    }

    public function isValid()
    {
        global $log;
        
        if($this->ranValidation) {
            if(count($this->validationErrors)==0){
                return true;
            } else {
                return false;
            }
        } else {
           # TODO: Maybe this should be an error instead of a warning
            $log->warn("ObjDef->isValid","Is Valid Has been Called without running validation first.");
            return false;
        }
    }

    public function transformSpec($objData, $mode="any") {    
        global $app;
        global $specsTransformed;
        
        // this check is wrong, the specDef should always be not Null
        if (isset($this->specDef)) {
            $objDef=$this->specDef->transformSpec($objData, $mode);        
            
            foreach($objDef->properties as $propDef) {            
                if (($propDef->getAttribute("isActive",true)===True) && ($propDef->getAttribute("associationType", false)!==false) ) {
                    
                    $referenceTable=$propDef->getAttribute("referenceTable");
                                            
                    if (!array_key_exists($referenceTable, $specsTransformed)) {
                                                
                        $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                            
                        
                        $specsTransformed[$referenceTable] = true;
                        $objDef2 = $app->specManager->findDef($referenceTable);
                        
                        if (isset($objData[$propDef->name])) {
                            $objData2 = $objData[$propDef->name];
                            $objDef2->transformSpec($objData2,$mode);
                        }
                    } 
                }
            }
            
            return $objDef;
        } else {
            return $this;
        }
    }       
    
    public function transformValues($objData, $mode="any") {
        
        if (isset($this->specDef)) {
            $this->specDef->transformValues($objData, $mode);
        }
    }     
    
    public function onAdd($objData) {
        
        if (isset($this->specDef)) {
            $this->specDef->onAdd($objData);
        } else {
            //echo 'here';
        }
    } 

    public function onUpdate($oldData, $newData) {
        
        if (isset($this->specDef)) {
            $this->specDef->onUpdate($oldData, $newData);
        } 
    } 
    
    public function validate($objData, $mode="any", $isNested=false) {       
                
        $this->transformSpec($objData, $mode);            
        $this->clearValidationErrors();        
        $this->checkRequiredProperties($objData,$mode);
        //$this->checkValidProperties($objData);        
        if($isNested===false){            
            $this->currentPath=new PathStack("/");;
        }        
        $this->checkPropertyConditions($objData,$mode,$isNested);                
        $this->checkObjectConditions($objData,$mode); // Added 2011-03-09 by ALL          
        $this->ranValidation=true;
    }

    public function clearValidationErrors() {
        $this->validationErrors=array();
        $this->ranValidation=false;
        
    }

    public function addConstructorParam($propId) {        
        $key = strtolower($propId);

        if(array_key_exists($key,$this->constructorParameters)==false)
        {
            $this->constructorParameters[$key]=$propId;
        }
    }

    public function checkObjectConditions($objData, $mode="any")
    {
        $conditions = $this->getConditions($mode);
        
        // In edit mode only validate the properties that are different than then values in the
        // database, these are the only values that are inserted into the database during an update
        // the rest do not matter and validating them actually causes problems when checking for
        // duplicate values.
        if($mode==="edit"){
            $ObjDAL=NewDALObj($this->name);
            $oldObj=$ObjDAL->getOneUsingOriginalPK($objData);
            if ($oldObj) {
                $newObj=$ObjDAL->createFromData($objData,$nestedObjects=true);
                $difference=compareAToB($this, $oldObj, $newObj);
            } else {
                   $difference=Array();
            }
        }

        foreach($conditions as $condition) {
            if ($condition->enabled==True) {
                if($mode==="edit"){
                    // In edit mode, skip the conditions where there are no differences in any of the condition properties versus
                    // the original in the database. If there is a difference in one field, but not another,m the condition must be checked.
                    $propIds=$condition->getPropIds();
                    $propInDiff=false;
                    foreach($propIds as $key) {   
                        if(array_key_exists($key, $difference)){                
                            $propInDiff=true;
                        }
                    }
                    if($propInDiff===false){
                       continue;
                    }
                }
                if($condition->validate($this,$objData)==false) {
                    $conditionName=get_class($condition);
                    $errorPropIds = $condition->getPropIds();

                    foreach($errorPropIds as $key) {        

                        if (is_array($key)) {

                            foreach($key as $k) {
                                $error= new ValidationError($this->name,$k,$conditionName,$this->currentPath->getPath2(),$condition->getErrorString("<BR/>"),$condition->errorCode);                                        
                                $this->validationErrors[]=$error;                            
                            }

                        } else {
                            $error= new ValidationError($this->name,$key,$conditionName,$this->currentPath->getPath2(),$condition->getErrorString("<BR/>"),$condition->errorCode);                                        
                            $this->validationErrors[]=$error;
                        }
                    }
                } else {
                    continue;
                }
            }
        }
    }

    public function getValueFromObj($objData, $key, $defaultValue) {
        if (isset($objData[$key])) {
            $value = $objData[$key];
        } 
        else {
            $value = null;
        }       
        return $value;
    }
    
        
    public function collectionValidation($value,$propDef, $objData, $mode="any", $isNested=false) {
        global $app;
        
        $associationType = $propDef->getAttribute("associationType", false);
                
        if ($propDef->getAttribute("isActive",true)===False) {
            return;
        }
                        
        if ($associationType=="HasOne") {                    
            if ($propDef->getAttribute("isComposite",true)===False) {                
                $validateCollectionData = False;
            } else {
                $validateCollectionData = True;
            }
        } else if ($associationType=="HasMany") {                    
             $validateCollectionData = True;
        } else if ($associationType=="HasManyThrough" ) { 
            if($propDef->getAttribute("enableEditing",false) && $mode==="edit"){
                $validateCollectionData = True;
            }
            else{
                $validateCollectionData = False;
            }
        } else {
            $validateCollectionData = False;
        }
        
        // validate collection...
        // what object type is it?
        $collectionObjDef = $app->specManager->findDef($propDef->objectTypeOrig);             
        $childObjDef=$collectionObjDef->getFirstChildDef();

        $collectionData = null;
        if (isset($objData[$propDef->name])) {
                $collectionData = $objData[$propDef->name];
        }       
        
        if (isnull($collectionData)) {
            $validateCollectionData = False;
        }

        // deep collection validation
        // 
        $propDef->setMode($mode);
        $conditions = $propDef->getConditions();

        // Validate the collection itself

        foreach($conditions as $condition)
        {
            if ($condition->enabled) {                           
                $status = $condition->validate($collectionData);

                if ($status==false)
                {
                    if ($condition->level==0) {
                        // this is an error
                        $conditionName=get_class($condition);
                        $error= new ValidationError($this->name,$propDef->name,$conditionName,$this->currentPath->getPath2(),$condition->getErrorString("<BR/>"),$condition->errorCode, $value);
                        $this->validationErrors[]=$error;

                    } else if ($condition->level==1) {
                        // this is a warning.
                        // does this ObjDef have Warnings turned on?                        
                        $showWarnings = $this->getAttribute("showWarnings", false); // default is false

                        if ($showWarnings) {
                            $conditionName=get_class($condition);
                            $error= new ValidationError($this->name,$propDef->name,$conditionName,$this->currentPath->getPath2(),$condition->getErrorString("<BR/>"),$condition->errorCode, $value);
                            $error->errorLevel = 1; // a warning
                            $this->validationErrors[]=$error;                               
                        }
                    }                    
                }
            }
        }    


        // is it allowed to be null, in the current mode or state?
        // is it allowed to have any members, in the current mode or state?
        // how many members is it allowed to have, in the current mode or state?
        // 
        // can it have duplicate members? this is a condition called PropertyOfChildUnique
        // can it have the memebers that it has?
        // do any of the member conflict with each other? It it has one member does that preclude another?

        if ($validateCollectionData) {
            $i=0;
            $this->currentPath->add($propDef->name);
            foreach($collectionData as $itemData) {
                $this->currentPath->add($i);
                $childObjDef->setCurrentPath($this->currentPath);
                $childObjDef->validate($itemData,$mode,true);
                $this->currentPath->pop();

                $errors=$childObjDef->validationErrors;                    
                if (count($errors)>0) {                        
                    // add errors for this collection....
                    foreach($errors as $error) {                         
                        $this->validationErrors[]=$error;
                    }                         
                }
                $i++;
            }
            $this->currentPath->pop();            
        }
    }
    
    public function checkPropertyConditions($objData, $mode="any", $isNested=false)
    {
        global $app;
        
        // if the app does not have a session table global substitution does not work
        if(isset($app->sessionManager)){        
            $app->setGlobalData("OBJ",$objData);
            
            //commented out by Anthony Leotta on 1/25/2013 because we don't use this feature yet...
            //$app->setGlobalData("SESSION",$app->sessionManager->sessionObj);
        }
        
        // In edit mode only validate the properties that are different than then values in the
        // database, these are the only values that are inserted into the database during an update
        // the rest do not matter and validating them actually causes problems when checking for
        // duplicate values.
        if($mode==="edit"){
            $ObjDAL=NewDALObj($this->name);
            $oldObj=$ObjDAL->getOneUsingOriginalPK($objData);
            $newObj=$ObjDAL->createFromData($objData,$nestedObjects=true);
            $difference=compareAToB($this, $oldObj, $newObj);            
        }
        
        foreach($this->properties as $propDef) {
                        
            if ($propDef->isAttribute("ignore",true,$mode)) {
                continue;
            }
            
            // do not validate auto-id columns, since these are generated by the database
            // this is needed to add new nested collection values where the auto-id column is blank
            if($propDef->getAttribute("autoIncrement",false)===true && $isNested===true){
                continue;
            }
            
            // if the collection contains a property that references an autoId and there is no display transformation involved, 
            // do not validate the value.
            // this is needed for HasMany relationships where key that links to the parent should not be validated
            // This is wrong, the id and display column do not have to be the same, as a work around they are.
            if($propDef->getAttribute("fkIsAutoIncrement",false) && $propDef->getAttribute("directReference",false) && $isNested===true){
                continue;
            }
            
            
            // skip the scalar values that have not changed, still need to check nested data.
            if($mode==="edit" && !array_key_exists($propDef->name, $difference) && $this->isScalar()===true){                
                continue;                
            }
                    
            if ($propDef->isCollection() == false) {
                // This is required for add mode, to avoid duplicate key errors in nested collections
//                if ($isNested===true && $mode==="add") {
//                    if ($propDef->getAttribute("fkIsAutoIncrement", false)==true) {                    
//                        continue;
//                    }
//                }                
        
                $propDef->setMode($mode);
                $conditions = $propDef->getConditions();

                if (count($conditions)==0) {
                    // no conditions for this PropDef
                    continue;
                }
                
                $nullAllowed = true;
                // is this property allowed to be NULL?
                foreach($conditions as $condition)
                {
                    if ($condition->enabled) {
                        if (is_a($condition, "NotEmptyCondition")) {
                            $nullAllowed = false;
                        }
                    }
                }

                $value = $this->getValueFromObj($objData, $propDef->name, null);

                if (is_a($value, "EmptyValue")) {
                    $value = null;
                }
               
                $valueIsNull = false;
                                
                // first just check the null condition
                if ((($value==="") || is_null($value)) && ($nullAllowed == true)) {                    
                    $value=null;
                    continue;
                } else if($nullAllowed == false){
                    if (is_null($value) || $value==="") {
                        $notEmptyObj=new NotEmptyCondition();
                        $error= new ValidationError($this->name,$propDef->name,"NotEmptyCondition",$this->currentPath->getPath2(),$notEmptyObj->getErrorString("<BR/>"),$notEmptyObj->errorCode, $value);
                        $this->validationErrors[]=$error;
                        continue;
                    }
                }      
                
                // if this is edit mode  and if this is the primary key
                if ($mode=="edit") {
                    if ($this->isPartOfPrimaryKey($propDef->id)) {
                         $originalValueKey = "original-".$propDef->name;
                         
                         $originalValue = $this->getValueFromObj($objData, $originalValueKey, null);
                        
                        if ($originalValue==$value) {
                            // now need to validate...it has not changed
                            continue;
                        }
                    }
                }
                // it its not part of the primary key it gets checked...

                foreach($conditions as $condition)
                {
                    if ($condition->enabled) {
                        // what is the $originalValue is null or it was not retrieved, how do I check for this?
                        if ($condition->useOriginalValue==true) {
                            $status = $condition->validate($originalValue);
                        } else {
                            $status = $condition->validate($value);
                        }

                        if ($status==false)
                        {
                            if ($condition->level==0) {                            
                                // this is an error                            
                                $conditionName=get_class($condition);
                                $error= new ValidationError($this->name,
                                                            $propDef->name,
                                                            $conditionName,
                                                            $this->currentPath->getPath2(),
                                                            $condition->getErrorString("<BR/>"),
                                                            $condition->errorCode, 
                                                            $value);
                                $this->validationErrors[]=$error;

                            } else if ($condition->level==1) {
                                // this is a warning.
                                // does this ObjDef have Warnings turned on?                        
                                $showWarnings = $this->getAttribute("showWarnings", false); // default is false

                                if ($showWarnings) {
                                    $conditionName=get_class($condition);
                                    $error= new ValidationError($this->name,
                                                                $propDef->name,
                                                                $conditionName,
                                                                $this->currentPath->getPath2(),
                                                                $condition->getErrorString("<BR/>"),
                                                                $condition->errorCode,
                                                                $value);
                                    $error->errorLevel = 1; // a warning
                                    $this->validationErrors[]=$error;                               
                                }
                            }

                        }                        
                   }
                }    
            } else {             
                $this->collectionValidation($value,$propDef, $objData, $mode, $isNested);  
            }
        }        
    }

    public function setCurrentPath($pathStack) {
        $this->currentPath=$pathStack;
    }
    
    public function clearErrors(){
        $this->validationErrors=array();
    }
       
    // check all properperties and for each property with a NULL value, 
    // check if it has a default value.        
    
    public function addDefaultValues(&$objData, $mode) {        
        foreach($this->properties as $propDef) {
                        
            if ($propDef->isCollection() == false) {
            
                $propDef->setMode($mode); 
                if (isset($objData[$propDef->name])) {
                    $value = $objData[$propDef->name];
                } 
                else {
                    $value = null;
                }
                                
                if (is_a($value, "EmptyValue")) {
                    $value = null;
                }

                if (is_null($value) == True || $value=="") {
                    $defaultValue = $propDef->getDefaultValue($mode);
                    
                    if (is_null($defaultValue)==False || $defaultValue===false) {
                         $objData[$propDef->name] = $defaultValue;                       
                    }                        
                }
            }
        }
    }
 
    
    public function applyTransforms(&$objData, $mode="any")
    {
        foreach($this->properties as $propDef) {
                        
            if ($propDef->isAttribute("ignore",true, $mode)) {
                continue;
            }
                    
            if ($propDef->isCollection() == false) {
            
                $propDef->setMode($mode);
                $transforms = $propDef->getTransforms();

                foreach($transforms as $transform) {
                    $transform->apply($objData);                    
                }    
            }
        }
    }
 
    public function setOrder($propertyIds) {
        
        $this->propertiesOrdered=array();
        if (is_array($propertyIds)) {
            foreach($propertyIds as $propertyId) {
                if ($this->isProperty($propertyId)) {                                        
                    $propDef = $this->getProperty($propertyId);
                    $this->propertiesOrdered[]=$propDef;
                }
            }            
        }
    }
    
    public function getAssociations(){
        $associations=array();
        foreach($this->properties as $propDef){
            if($propDef->hasAttribute("associationType")){
                $associations[]=$propDef;
            }
        }
        
        return $associations;
    }
    
    /** gets properties from the object, this is need to get subsets of properties based
     *  on filters or specific attributes. For now, it just returns the properties array.
     * 
     */
    public function getProperties(){
        return $this->properties;
    }
    
    /**
     *  Given the name of an objDef and a list of properties, copy the propDef from the sourceObjDef
     *  to the current object.
     */
    public function addSubset($objDefName,$properties){
        global $app;
        $sourceObjDef=$app->specManager->findDef($objDefName);
        if($sourceObjDef===null){
            throw new Exception("The objDef '{$objDefName}' could not be found");
        }
        
        $unknownProperties=array();
        foreach($properties as $propertyName){
            if($sourceObjDef->isProperty($propertyName)){
                $propDef=$sourceObjDef->getProperty($propertyName);
                // serialize/unserialize supposedly does a deep clone of an object, testing
                // is required to see if this is true.
                $this->addProperty(unserialize(serialize($propDef)));
            }
            else{
                $unknownProperties[]=$propertyName;
            }
        }
        if(count($unknownProperties)>0){
           $unknownPropsString=implode(",",$unknownProperties);
           throw new Exception("The properties: '{$unknownPropsString}' do not exist in the objDef '{$objDefName}'");
        }
    }
}
?>