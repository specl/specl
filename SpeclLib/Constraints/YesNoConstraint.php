<?php
/**
 * Description of YesNoConstraint
 *
 * @author Tony
 */
class YesNoConstraint extends Constraint
{    
    public $options;

    public function __construct()
    {
        $this->options = array();
    }

    public function generate()
    {
        $this->options = array();

        $option = new Option();
        $option->key = 0;
        $option->value = "No";
        $this->options[$option->key] = $option;

        $option = new Option();
        $option->key = 1;
        $option->value = "Yes";
        $this->options[$option->key] = $option;

    }

    public function get()
    {
        return $this->options;
    }

    public function count()
    {
        return count($this->options);
    }

}
?>
