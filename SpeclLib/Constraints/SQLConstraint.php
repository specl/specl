<?php
class SQLConstraint extends Constraint
{    
    public $connectionName;
    public $statement;
    public $tableName;
    public $idColumnName;
    public $displayColumnName;
    public $orderByColumnName;
    public $addEmptyOption;            
    public $options;
    public $whereClause;
    public $whereStatement;
    public $numberOfRowsToGet;
    public $selectUniqueValues;
    public $manyToMany;
    public $extraWhereClause;
    
    // added by ALL on June 21, 2011
    public $includeInQuery; // true or false, added to prevent Constraints from being
    // used in the Query Buildering process fro data tables

    public function __construct($params)
    {        
       
                
        if (is_null($params)) {
            return;
        }
        
        if (isset($params["connectionName"])) {
            $this->connectionName = $params["connectionName"];
        } else {
            $this->connectionName = null;
        }
                    
        if (isset($params["tableName"])) {
            $this->tableName = $params["tableName"];
        } else {
            $this->tableName = null;
        }
        
        if(isset($params["tablesToJoin"])){
            $this->tablesToJoin=$params["tablesToJoin"];
        }else{
            $this->tablesToJoin=array();
        }
            
            
        if (isset($params["idColumnName"])) {
            $this->idColumnName = $params["idColumnName"];
        } else {
            $this->idColumnName = null;
        }
            
        if (isset($params["displayColumnName"])) {
            $this->displayColumnName = $params["displayColumnName"];
        } else {
            $this->displayColumnName = null;
        }

        if (isset($params["orderByColumnName"])) {
            $this->orderByColumnName = $params["orderByColumnName"];
        } else {
            $this->orderByColumnName = null;
        }
        
        if (isset($params["addEmptyOption"])) {
            $this->addEmptyOption = $params["addEmptyOption"];
        } else {
            $this->addEmptyOption = null;
        }

        if (isset($params["whereClause"])) {
            $this->whereClause = $params["whereClause"];
        } else {
            $this->whereClause = null;
        }
        
        if(isset($params["whereStatement"])){
            $this->whereStatement=$params["whereStatement"];
        }
        else{
            $this->whereStatement=new SQLWhere();
        }

        if (isset($params["includeInQuery"])) {
            $this->includeInQuery = $params["includeInQuery"];
        } else {
            $this->includeInQuery = true;
        }
        
        if(isset($params["selectUniqueValues"])){
            $this->selectUniqueValues=$params["selectUniqueValues"];
        } else{
            $this->selectUniqueValues=false;
        }        
        
        if(isset($params["manyToMany"])){
            $this->manyToMany=$params["manyToMany"];
        } else{
            $this->manyToMany=false;
        }
        
        if(isset($params["extraWhereClause"])){
            $this->extraWhereClause=$params["extraWhereClause"];
        } else{
            $this->extraWhereClause=Null;
        }        
                
        $this->numberOfRowsToGet=null;
        $this->options = array();        
        $this->optionsGenerated=false;
    }

    public function generate($mode="any")
    {
        global $app;
                
        $dbConn = $app->getConnection($this->connectionName);
        
        if (is_null($dbConn)) {
            // connection name not found or some other problem
            return;
        }
        
        $whereClause = "";
        
        if(count($this->whereStatement->clauses)>0){
            
            
            $filterClause=$this->whereStatement->generateWhereClause();
            
            if (!isnull($filterClause)) {
                if($this->whereClause){
                    if($filterClause!=""){
                        $filterClause.=" AND {$this->whereClause}";
                    }
                    else{
                        $filterClause=$this->whereClause;
                    }
                }
                $whereClause=" WHERE " . $app->fillInGlobals($filterClause);
            
                // do not return null display values, which show as a blank line is the dropdown or autocomplete
                $whereClause.=" AND {$this->displayColumnName} is not NULL";
            }
        }
        else if ($this->whereClause) {
            $whereClause = " WHERE " . $app->fillInGlobals($this->whereClause);  
            
            // do not return null display values, which show as a blank line is the dropdown or autocomplete
            $whereClause.=" AND {$this->displayColumnName} is not NULL";
        }
        else{
            // do not return null display values, which show as a blank line is the dropdown or autocomplete
            $whereClause=" WHERE {$this->displayColumnName} is not NULL";
        }
        
        // in add mode, if the table has an hideOnAdd column, filter the values the only where hideOnAdd is false.
        if($mode==="add"){
            $objDef=$app->specManager->findDef($this->tableName);
            if($objDef->isProperty("hideOnAdd")){
                if($whereClause!==""){
                    $whereClause.=" AND hideOnAdd=0";
                }else{
                    $whereClause=" WHERE hideOnAdd=0";
                }
            }
        }
        
        $tablesToJoin="";
        foreach($this->tablesToJoin as $tableName){
            $tablesToJoin.=",`{$tableName}`";
        }
        
        $this->options = array();
        
        $limit="";
        if(!is_null($this->numberOfRowsToGet)){
            $limit=" LIMIT {$this->numberOfRowsToGet}";
        }
        
        if($this->selectUniqueValues){
            $distinct="distinct";
        } else{
            $distinct="";
        }
        
        if (isnull($this->orderByColumnName)) {
            $this->orderByColumnName = $this->displayColumnName;
        }
        
        $this->statement = "select {$distinct} `{$this->tableName}`.`{$this->idColumnName}`,`{$this->tableName}`.`{$this->displayColumnName}` from `{$this->tableName}` {$tablesToJoin} {$whereClause} order by `{$this->orderByColumnName}` {$limit};";                
        $results = $dbConn->select($this->statement);

        if ($results!=0)
        {
            if ($this->addEmptyOption == true) {
                $this->options["SNULL"] = "";
            }            
            while($row = $dbConn->getRowEnum($results)) {
                if (count($row)==1) {
                    $this->options[$row[0]] = $row[0];
                } else if (count($row)>1) {
                    $this->options[$row[0]] = $row[1];
                }                                        
            }
        }
        $this->optionsGenerated=true;
    }

    
    public function lookup($key)
    {
        global $app;
                
        $dbConn = $app->getConnection($this->connectionName);
        
        if (is_null($dbConn)) {
            // connection name not found or some other problem
            return;
        }
          
        $constraintObjDef = $app->specManager->findDef($this->tableName);                                             
        $idPropDef = $constraintObjDef->getProperty($this->idColumnName);
        $displayPropDef = $constraintObjDef->getProperty($this->displayColumnName);
        
        $sqlBuilder = new SQLBuilder($dbConn,$app->specManager);
        
        $whereClause = " `{$this->tableName}`.`{$this->idColumnName}` = %key%";         
        $statement = "select `{$this->tableName}`.`{$this->displayColumnName}` from `{$this->tableName}` WHERE {$whereClause};";                
        
        $sqlBuilder->setStatement($statement);
        $sqlBuilder->createParam("key", $idPropDef->objectTypeOrig, $key);
                
        $sql = $sqlBuilder->bindParams();        
        
        $results = $dbConn->select($sql);
        
        if ($results!=0)
        {
            $row = $dbConn->getRowEnum($results);
            $value = $row[0];
        } else {
            $value = Null;
        }
        
        return $value;
    }
    
    // TODO: Refactor $tablesToJoin,$whereClause and distinct into seperate functions, this requires writing tests for the SQLConstraint class
    // before refactoring.
    public function count($mode="any"){
        global $app;
        $dbConn=$app->getConnection($this->connectionName);
        
        // refactor this
        $whereClause = "";
        if(count($this->whereStatement->clauses)>0){
            $filterClause=$this->whereStatement->generateWhereClause();
            
            if (!isnull($filterClause)) {
                if($this->whereClause){
                    if($filterClause!=""){
                        $filterClause.=" AND {$this->whereClause}";
                    }
                    else{
                        $filterClause=$this->whereClause;
                    }
                }
                $whereClause=" WHERE " . $app->fillInGlobals($filterClause);
            
                // do not return null display values, which show as a blank line is the dropdown or autocomplete
                $whereClause.=" AND {$this->displayColumnName} is not NULL";
            }
        }
        else if ($this->whereClause) {
            $whereClause = " WHERE " . $app->fillInGlobals($this->whereClause);  
            
            // do not return null display values, which show as a blank line is the dropdown or autocomplete
            $whereClause.=" AND {$this->displayColumnName} is not NULL";
        }
        else{
            // do not return null display values, which show as a blank line is the dropdown or autocomplete
            $whereClause=" WHERE {$this->displayColumnName} is not NULL";
        }
        
        // in add mode, if the table has an hideOnAdd column, filter the values the only where hideOnAdd is false.
        if($mode==="add"){
            $objDef=$app->specManager->findDef($this->tableName);
            if($objDef->isProperty("hideOnAdd")){
                if($whereClause!==""){
                    $whereClause.=" AND hideOnAdd=0";
                }else{
                    $whereClause=" WHERE hideOnAdd=0";
                }
            }
        }
        
        $tablesToJoin="";
        foreach($this->tablesToJoin as $tableName){
            $tablesToJoin.=",`{$tableName}`";
        }
        
        if($this->selectUniqueValues){
            $distinct="distinct";
        } else{
            $distinct="";
        }
        
        if (isnull($this->orderByColumnName)) {
            $this->orderByColumnName = $this->displayColumnName;
        }
        // end code to refactor.
        $countSQL="SELECT {$distinct} count(*) from `{$this->tableName}` {$tablesToJoin} {$whereClause} order by `{$this->orderByColumnName}`";
        $results=$dbConn->select($countSQL);
        $row=$dbConn->getRowEnum($results);
        return $row[0];
    }
    
    public function get()
    {
        return $this->options;
    }
}
?>
