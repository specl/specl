<?php
/**
 * Description of YesNoConstraint
 *
 * @author Tony
 */
class LookupConstraint extends Constraint
{    
    public $options;

    public function __construct($params)
    {
        if(isset($params["options"])){
            $this->options = $params["options"];
        }
        else{
            $this->options=array();
        }
    }

    public function generate() {

    }
    
    public function get()
    {
        return $this->options;
    }

    public function count()
    {
        return count($this->options);
    }

}
?>
