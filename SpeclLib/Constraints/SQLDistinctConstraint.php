<?php
class SQLDistinctConstraint extends Constraint
{    
    public $connectionName;
    public $statement;
    public $tableName;
    public $idColumnName;
    public $displayColumnName;
    public $addEmptyOption;            
    public $options;
    public $whereClause;
    
    // added by ALL on June 21, 2011
    public $includeInQuery; // true or false, added to prevent Constraints from being
    // used in the Query Buildering process fro data tables

    public function __construct($connectionName, $tableName,$idColumnName,$displayColumnName,$addEmptyOption)
    {        
        $this->connectionName = $connectionName;        
        $this->tableName = $tableName;
        $this->idColumnName = $idColumnName;
        $this->displayColumnName = $displayColumnName;
        $this->addEmptyOption = $addEmptyOption;                
        $this->options = array();
        $this->whereClause = null;
        $this->includeInQuery = true; // default is true so thing keep working as before
    }

    public function generate()
    {
        global $app;
                
        $dbConn = $app->getConnection($this->connectionName);
        
        if (is_null($dbConn)) {
            // connection name not found or some other problem
            return;
        }
        
        $whereClause = "";
        
        if ($this->whereClause) {
            $whereClause .= "WHERE " . $this->whereClause;            
            // do a substitution
        }            
        
        $this->options = array();        
        $this->statement = "select distinct(`{$this->idColumnName}`) from `{$this->tableName}` {$whereClause} order by `{$this->displayColumnName}`;";                
        $results = $dbConn->select($this->statement);

        if ($results!=0)
        {
            if ($this->addEmptyOption == true) {
                $this->options["SNULL"] = "";
            }            
            while($row = $dbConn->getRowEnum($results)) {
                if (count($row)==1) {
                    $this->options[$row[0]] = $row[0];
                } else if (count($row)>1) {
                    $this->options[$row[0]] = $row[1];
                }                                        
            }
        }               
    }

    public function get()
    {
        return $this->options;
    }

   
    
    public function count()
    {
        return count($this->options);
    }

}
?>
