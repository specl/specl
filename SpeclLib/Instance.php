<?php

// TODO: change instance to set and get class attributes instead of
// the properties array.

class Instance
{
    public $specManager;
    
    // any variable defined this way (as public) will get in the way of the __get magic method
    // in fact, any variable at all will get in the way, so I wrapped them in __ __ 
    // hopefully this works.  But I fear this is not good enough.
    public $__name__;
    public $__objDefId__;
    public $__objDef__;
    public $__properties__;
    public $scalarValidation;
    
    public function __construct($name,$__objDefId__, $specManager)
    {        
        $this->specManager = $specManager;
        
        $this->__name__=$name;  // what is this used for? ALL May 8, 2011
        $this->__objDefId__=$__objDefId__;
        $this->__properties__ = array();
          
        // Load spec using spec manager
        
        $this->specManager->loadSpecDef($__objDefId__);
        
        $objDef=$this->specManager->findDef($__objDefId__);
        
        if(!is_null($objDef)) {
            $this->__objDef__=$objDef;
            
            foreach($this->__objDef__->getProperties() as $propDef) {
                if($propDef->isCollection()) {
                     // Initialize all collection properties to an empty array
                    $this->__properties__["{$propDef->name}"]=array();
                } else {
                     // Initialize all non-collection properties to null
                    $this->__properties__["{$propDef->name}"]=new EmptyValue();
                }
            }
        } else {
            $msg="Failed To Initialize Instance: the Spec extends SpecDef {$__objDefId__} does not exist";
            $log->error("Instance->Construct",$msg);
            throw new Exception($msg);
        }         
        
        $this->scalarValidation = false;
    }
    
    public function setPropertyValue($name,$value)
    {        
        // get property spec from instance spec
        $propDef=$this->__objDef__->getProperty($name);
        
        if(!is_null($propDef))
        {
            // TODO: check value type        
            // load spec if the type of the property is a spec            
            if($propDef->objectType=="spec") {
                if(is_string($value)) {
                    $this->specManager->loadSpecDef($value);
                    $objDef=$this->specManager->findDef($value);
                    if(!is_null($objDef)) {
                        $this->__properties__[$name]=$objDef;
                    } else {
                         $msg="setPropertValue failed: The Spec extends SpecDef {$value} does not exist";
                         $log->error("",$msg);
                         throw new Exception($msg);  
                    }                    
                } else {
                    // And check for SpecObj here or in validate?                    
                    $this->setScalarValue($propDef,$value);
                }
            } else {
                $this->setScalarValue($propDef,$value);
            }
            // TODO: call setter function if available
        } else {
            $msg="setPropertValue failed: The property {$name} does not exist";
            $log->error("",$msg);
            throw new Exception($msg);  
        }
    }

    public function setScalarValue($propDef, $value) {
                
        if ($this->scalarValidation) {
            switch ($propDef->objectType) {
                case 'string':
                    break;
                case 'boolean':                                
                    $condition = new BooleanTypeCondition();                
                    $valid = $condition->validate($value);                
                    if (!$valid) {
                        echo "Instance->setScalarValue() {$this->__objDef__->id} {$propDef->name} {$value} not a boolean<br/>";
                    }
                    break;
                case 'date':
                    $condition = new DateTypeCondition();                
                    $valid = $condition->validate($value);                
                    if (!$valid) {
                        echo "Instance->setScalarValue() {$this->__objDef__->id} {$propDef->name} {$value} not a boolean<br/>";
                    } else {
                        $timestamp  = strtotime($value);

    //                    $year = date("Y", $timestamp);
    //                    $month = date("m", $timestamp);
    //                    $day = date("d", $timestamp);
    //                    $minute = date("i", $timestamp);
    //                    $second = date("s", $timestamp);
    //                    $hour = date("H", $timestamp);

                        $value = date('m/d/Y',$timestamp); 
                    }
                    break;

                case 'double':
                    $condition = new FloatTypeCondition();                
                    $valid = $condition->validate($value);

                    if (!$valid) {
                        echo "Instance->setScalarValue() {$this->__objDef__->id} {$propDef->name} {$value} not a double<br/>";
                    }
                    break;
                case 'integer':
                    $condition = new IntegerTypeCondition();                
                    $valid = $condition->validate($value);                
                    if (!$valid) {
                        echo "Instance->setScalarValue(){$this->__objDef__->id} {$propDef->name} {$value} not a integer<br/>";
                    }
                    break;
                default:
                    echo "Instance->setScalarValue() ERROR: Instance->Instance Unhandled type {$propDef->objectType} {$this->__objDef__->id} {$propDef->name} <br/>";
                    break;            
            }
        }
        
        
        if (is_string($value)) {
            if (is_blank($value)) {
                $value=null;
            }            
        }
        
        if (is_null($value)) {       
            $this->__properties__[$propDef->name]=new EmptyValue();
        } else {
            $this->__properties__[$propDef->name]=$value;        
        }
        //array_push($this->__properties__[$propDef->name], $value);
    }
    
    
    public function addValueToCollection($name,$value)
    {
        // get property spec from instance spec
        
        $propDef=$this->__objDef__->getProperty($name);
        
        if(!is_null($propDef))     
        {
            // check to make sure the type is "collection"
            
            if(!$propDef->isCollection())
            {
                $msg="addValueToCollection failed: The property {$name} is not a collection type.";
                $log->error("",$msg);
                throw new Exception($msg);
            }
            else
            {
                $colSpec=$this->specManager->findDef($propDef->name);

                if(is_a($value,"instance"))
                {
                    if($colSpec->isChildType($value->__objDefId__))
                    {
                         $this->__properties__[$name][]=$value;
                    }
                    else
                    {
                        $msg="addValueToCollection failed: The property {$value->__objDefId__} is not a valid child type of the property '{$name}'";
                        $log->error("",$msg);
                        throw new Exception($msg); 
                    }
                }
                else
                {
                    //TODO: Add checks for scalar types
                }
            }
        }
        else
        {
            $msg="addValueToCollection failed: The property {$name} does not exist";
            $log->error("",$msg);
            throw new Exception($msg);  
        }
    }
    
    public function getPropertyValue($name)
    {
        // check to make sure that name exists in the properties array
        
        if(array_key_exists($name, $this->__properties__))
        {
            $value = $this->__properties__[$name];
            
            if (is_a($value, "EmptyValue")) {
                return null;
            }
        }
        else
        {
            // raise error here instead of returning null?
            return null;
        }
        
    }
    
    public function hasProperty($name)
    {
        // check to make sure that name exists in the properties array
        
        if(array_key_exists($name, $this->__properties__))
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
    
    // add validate instance function?
    
    
    public function __get($name)
    {
        return $this->getPropertyValue($name);
    }
    
    /*
     * loadFromArray
     * 
     * (1) data array
     * 
     * TODO: handle nested structures.
     * 
     */
    public function loadFromArray($data) {
    
        foreach($this->__properties__ as $key=>$value) {        
            if (isset($data[$key])) {
                $val = $data[$key];                
                $this->setPropertyValue($key, $val);
            }
        }
    }
    
    
    /* 
     * convert to nested Array
     */
    public function validate() {
        $this->__objDef__->validate($this->__properties__,"any");
        
        return $this->__objDef__->isValid();
    }
    
    public function addDefaultValues() {
        $this->__objDef__->addDefaultValues($this->__properties__,"any");
    }
    
    
}

?>