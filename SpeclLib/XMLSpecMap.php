<?php

class XMLSpecMap {

    private $mapping;
    private $pathStack;
    
    public function __construct() {        
        $this->mapping = Array();        
        $this->pathStack = new PathStack(App::ZONESSEP);                  
    }
            
    public function addMap($path, $name) {        
        $path = strtolower($path);
        $name = strtolower($name);
        $this->mapping[$path] = $name;
    }  
    
    // given a $path, it returns either a ObjDef or a Objdef, PropDef tuple
    public function lookupPath($path) {
        
        $path = strtolower($path);
        if (array_key_exists($path, $this->mapping)) {
            return $this->mapping[$path];
        }
        return Null;
    }
    
    public function createMapFromObjDef($objDef) {   
        
        
        $this->recurse($objDef);
    }
    
    public function recurse($obj) {        
        global $app;
        
        if (is_a($obj, 'ObjDef')) {
            
            
            $path = $this->pathStack->getPath();
            
            if (is_blank($path)) {
                
                $xmlTag = $obj->getAttribute("xmlTag", Null);
                    
                if ($xmlTag) {
                    $this->addMap("/".$xmlTag, $obj->name);
                    $this->pathStack->add($xmlTag);
                } else {
                    $this->addMap("/".$obj->name, $obj->name);
                    $this->pathStack->add($obj->name);
                }
            } else {
                
            }
            
            foreach($obj->getProperties() as $propDef) {            
                if (is_a($propDef, 'CollectionDef')) {
                     
                } else {
                    $path = $this->pathStack->getPath();

                    $xmlTag = $propDef->getAttribute("xmlTag", Null);

                    if (is_null($xmlTag)) {
                        $xmlTag = $propDef->name;
                    }                    
                    
                    $this->addMap("/".$path.'/'.$xmlTag, $propDef->name);
                    
                    if($propDef->getAttribute("associationType")=="HasOne") {
                        
                        $referenceTable=$propDef->getAttribute("referenceTable");                                                            
                        $referenceObjDef=$app->specManager->findDef($referenceTable);                                                                                            
                    
                        $this->pathStack->add($xmlTag);
                        $this->recurse($referenceObjDef);
                        $this->pathStack->pop();
                    }                                                                  
                }
            }            
        } else if (is_a($obj, 'PropDef')) {
            
        }
    }    
}
?>
