<?php
/**
 * Description of SpecDef
 * Base class for all Spec Defs.  This idea is that this class can contain
 * validation and checking code and helpers to make the job of creating more
 * complex specs easier.
 * 
 * @author tony
 */
class SpecDef {
    
    public $objDef;
    public $objDefId;
    public $lineage;  // track the inheritance chain
    
    public function __construct($objDefId=null) {
        $this->lineage=Array();
        $this->objDefId = $objDefId;
        $this->lineage[] = $this->objDefId;
        $this->objDef = null;
    }

    public function extend($objDefId) {
        
        if ($objDefId==$this->objDefId) {
            // this is an error
            // TODO Raise
            return;
        } 
        
        if (count($this->lineage)>0) {
            // check for a duplicate $objDefId 
            // TODO
            $this->objDefId = $objDefId;
            $this->lineage[] = $objDefId;
        } else {
            $this->objDefId = $objDefId;
            $this->lineage[] = $objDefId;
        }
    }
    
    public function defSpec($specManager) {        
    }    
    
    public function transformSpec($objData, $mode="any") {
                
        
        if (is_subclass_of($objData, "SpecCls")) {
            $data = $objData->getAsArray();
        } else {
            $data = $objData;
        }              
        
        
        $columnCompositeLookups = array();
        
        foreach($this->objDef->getProperties() as $propDef) {            
            if ($propDef->getAttribute("compositeTrigger",false)) {                
                // get constraint of the propdef and use it to build up a list of choices
                // for the fieldValueToCompositeSpecMap                
                $constraint = $propDef->getConstraint();                
                if ($constraint) {
                     $constraint->generate($mode);
                     $columnCompositeLookups[$propDef->name]=$constraint->get();
                }                 
            }
        }
                            
        foreach($columnCompositeLookups as $fieldName=>$valueMap){
            $params=array(
                "fieldName"=>$fieldName,
                "valueToSpecMap"=>$valueMap
                );
            $mergeCompositeSpecComponent=new MergeCompositeSpec($params);
            $input=array("data"=>$objData,"objDef"=>$this->objDef);

            $this->objDef=$mergeCompositeSpecComponent->execute($input);            
        }
               
        return $this->objDef;           
        
    }
    
    public function transformValues($objData, $mode="any") {
        return $objData;
    }
    
    public function onAdd($objData) {         
    }
    
    public function onUpdate($objData) {         
    }
    
    public function onDelete($objData) {         
    }
    
    public function onView($objData) {         
    }    
}
?>
