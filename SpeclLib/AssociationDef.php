<?php
/**
 * Description of AssocDef
 *
 * @author Tony
 */

class AssociationLink {
    public $fromObjDefId;
    public $fromPropDefId;
    public $toObjDefId;
    public $toPropDefId;
}

class AssociationDef {
    
    public $assciationType;

    // def of the object linked to. The object link from is implied
    // by the associations parent objDef.
    public $objDefId;
    public $links;

    public function  __construct($objDefId) {
        //        $id = $assciationType.":".strtolower($objDefId);
        //        $this->id = $id;
        //        $this->assciationType = $assciationType;
        $this->objDefId = $objDefId;
        $this->links = array();
    }

    public function addLink($from,$to) {

        $link = new AssociationLink();
        $link->fromPropDefId = $from;
        $link->toPropDefId = $to;
        $this->links[] = $link;
    }
}
?>
