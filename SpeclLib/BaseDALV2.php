<?php
/*
 * This is a rewrite of the first baseDAL that hopes to accomplish the following:
 *      - change all operations to run from CompositeSpecDefs
 *      - incorporate all of the different database operations in a single layer, instead of hardcoded
 *      in webservices.
 *      - get rid of functionallity that is no longer used or is nearly indentical to another piece of functionallity.
 * 
 * BaseDALV2 acts as a wrapper layer it could potentially call functions and classes defined elsewhere.
 */
class BaseDALV2 {
    
    /**
     * The BaseDAL requires a connection to the database to execute the queries in the end.
     */
    public function __construct($params=array()){
        global $app;
        if(isset($params["connection"])){
            $this->connection=$params["connection"];
        }
        else{
            $this->connection=null;
        }    
        if(is_string($this->connection) || $this->connection===null){
            $this->dbConn=$app->getConnection($this->connection);
        }
        else{
            $this->dbConn=$this->connection;
        }
    }
    
    /*
     * This function executes a query based on the CompositeObjdef passed in and some parameters. Each object
     * is returned as a single row in a table. This function performs all joins which can be considered safe for
     * keeping a single object in a single row. This includes all parent and child data that is not a collection.
     *      
     * 
     * The order in which the objects are added matter for this function. The first object added is considered to be the main
     * table against which all joins are performed.
     *
     * Internally it uses a SelectStatementBuilder to build up an SQLStatement and return the appropriate data.
     * 
     * TODO: Select needs to be broken up into seperate functions and all the functions need to be seperated into
     * a seperate class or namespace.
     */
    public function select($spec,$params=array()){
        global $app;
        $selectBuilder=new SelectStatementBuilderV2();        
        
        // set the first object in the query to the table of the database.        
        $objects=$spec->getObjects();
        reset($objects);
        $firstKey=key($objects);
        $firstObject=$objects[$firstKey];
        $selectBuilder->setTable($firstObject->objDef->getAttribute("table"));        
        
        // each property is returned as {$propObj->objectName}_{$propObj->columnName} to ensure there are no duplicate property names,
        // unless an alias is set in the parameters of the property.
        foreach($spec->getProperties() as $propObj){            
            if(is_a($propObj,"CompositeProperty")){               
                if(isset($propObj->params["alias"]) && $propObj->params["alias"]!=="''"){
                    $colAlias=$propObj->params["alias"];
                }
                else{
                    $colAlias="{$propObj->objectName}_{$propObj->propertyName}";
                }
                $propDef=$propObj->propDef;
                $selectBuilder->addColumn($propDef->getAttribute("column"), $propDef->getAttribute("table"),$colAlias );
            }
            else if(is_a($propObj,"CountProperty")){
                $selectBuilder->addCountColumn();
            }
        }
        
        $this->addHasOneJoins($selectBuilder,$spec,$params);
        
        if(count($spec->getFilters())>0){
            foreach($spec->getFilters() as $filter){
                // TODO: add error checking to make sure that the object and property specified in the filter exist.
                if(is_a($filter,"CompositeValueFilter")
                   && (!isset($filter->params["collectionFilter"]) || $filter->params["collectionFilter"]===false)){
                    $objDef=$app->specManager->findDef($filter->objectName);
                    $propDef=$objDef->getProperty($filter->propertyName);
                    $tableName=$propDef->getAttribute("table");
                    $columnName=$propDef->getAttribute("column");
                    $selectBuilder->addValueFilter($tableName, $columnName, $filter->operation, $filter->value, $filter->conjunction);
                }
                if(is_a($filter,"CompositeGroupFilter")){
                    $filterGroup=new SQLFilterGroup();
                    $filterGroup->conjunction=$filter->conjunction;
                    foreach($filter->filters as $subFilter){
                        if(is_a($subFilter,"CompositeValueFilter")
                            && (!isset($subFilter->params["collectionFilter"]) || $subFilter->params["collectionFilter"]===false)){
                             $objDef=$app->specManager->findDef($subFilter->objectName);
                             $propDef=$objDef->getProperty($subFilter->propertyName);
                             $tableName=$propDef->getAttribute("table");
                             $columnName=$propDef->getAttribute("column");
                             $filterGroup->addValueFilter($tableName, $columnName, $subFilter->operation, $subFilter->value, $subFilter->conjunction);
                         }
                    }
                    $selectBuilder->addFilter($filterGroup);
                }
            }
            
//            foreach($spec->getFilters() as $filter){                
//                if(is_a($filter,"CompositeValueFilter")
//                   && isset($filter->params["collectionFilter"]) && $filter->params["collectionFilter"]===false){
//
//                    $selectBuilder->addValueFilter($tableName, $columnName, $filter->operation, $filter->value, $filter->conjunction);
//                }
//            }
        }
        
        if(count($spec->getAggregates())>0){
            foreach($spec->getAggregates() as $aggregate){
                $objDef=$app->specManager->findDef($aggregate->objectName);
                $propDef=$objDef->getProperty($aggregate->propertyName);
                $tableName=$propDef->getAttribute("table");
                $columnName=$propDef->getAttribute("column");
                $selectBuilder->addGroupBy($tableName,$columnName);
            }
        }
        
        // TODO: add error checking to make sure that the object and property specified in the sort exist
        if(isset($params["sort"])){
            foreach($params["sort"] as $sortCol){
                if(isset($sortCol["order"])){
                    $sortOrder=$sortCol["order"];
                }
                else{
                    $sortOrder="ASC";
                }
                $objDef=$app->specManager->findDef($sortCol["object"]);
                $propDef=$objDef->getProperty($sortCol["property"]);
                $tableName=$propDef->getAttribute("table");
                $columnName=$propDef->getAttribute("column");
                $selectBuilder->addSortColumn($tableName,$columnName,$sortOrder);
            }
        }
            
        
        if(isset($params["limit"])){
            $limit=$params["limit"];            
            if(isset($limit["numRows"])){
                $numRows=$limit["numRows"];                
            }            
            if(isset($limit["startingRow"])){
                $startingRow=$limit["startingRow"];
            }
            else{
                $startingRow=0;
            }
            $selectBuilder->addLimit($numRows,$startingRow);
        }
        
        
        /*
         * return the SQLSelectStatement builder built by the query. This is used if the user wants
         * to add custom SQL statements that are not handled by the select function, but want to take
         * advantage of the things that the select method does handle.
         */
        if(isset($params["returnSelectBuilder"]) && $params["returnSelectBuilder"]===true){
            return $selectBuilder;
        }
        $SQL=$selectBuilder->generateSQLSelectStatement();
        
        /*
         *  return the generated SQL, not the data. This needed to build subqueries for the
         *  collection filters.
         */
        if(isset($params["returnSQL"]) && $params["returnSQL"]===true){
            return $SQL;
        }
        $data=$this->dbConn->fetchAssoc($SQL);
        return $data;
    }
    
    /*
     * This function adds joins between two objects if the tables for the objects are connected only through
     * HasOne connections. This function has the following rules:
     *      - do not add duplicate join tables
     *      - if the columns are either side of the join can be null, then use a left outer join and not an inner join.
     *      - HasOne relationships are assumed to be downstream of the current object, meaning that the graph is following
     *      outgoing connections. An outgoing connection is a path the exists from the main table to the table to be joined.
     *      - A HasMany or HasManyThrough implies an inverse HasOne relationship, these HasOne relationships have already been
     *      added to the graph by the GraphBuilder.
     * 
     * This function requires that the app variabled associationGraphFile be set, which specifies the path of the file where
     * the serialized graph object containing all of the table connections is stored.
     */
    protected function AddHasOneJoins($selectBuilder,$spec,$params){
        global $app;
        $graph=unserialize(file_get_contents($app->associationGraphFile));
        $objectNames=array_keys($spec->getObjects());
        // Check to see if a path exists between the original table and any of the other objects being joined
        for($i=1;$i<count($spec->getObjects());$i++){
            // get the objDef name, to deal with case sensitivity.
            $objDef1=$app->specManager->findDef($objectNames[0]);
            $objDef2=$app->specManager->findDef($objectNames[$i]);                    
            $sourceTable=$objDef1->getAttribute("table");
            $targetTable=$objDef2->getAttribute("table");
            $path=$graph->getShortestPath($sourceTable,$targetTable,$edgeTypes=array("HasOne"));
            if($path===null){
                continue;
            }
            
            for($j=0;$j<count($path)-1;$j++){
                $edge=$graph->getEdge($path[$j],$path[$j+1]);
                $objDef1=$app->specManager->findDef($path[$j]);                
                $propDef1=$objDef1->getProperty($edge->data["sourceColumn"]);                
                $objDef2=$app->specManager->findDef($path[$j+1]);
                $propDef2=$objDef2->getProperty($edge->data["targetColumn"]);
                
                // if both columns on either side of the join cannot be null then add an inner join,
                // otherwise add and outer join,
                if($propDef1->getAttribute("nullAllowed")===false && $propDef2->getAttribute("nullAllowed")===false ){
                    $selectBuilder->addInnerJoinTable($objDef1->name,$objDef2->name,$propDef1->name,$propDef2->name);
                }
                else{
                    $selectBuilder->addOuterJoinTable($objDef1->name,$objDef2->name,$propDef1->name,$propDef2->name);
                }
            }
        }
    }
}
?>
