<?php
/**
 * Description of SpecCls
 *
 * @author Tony
 */
class SpecCls implements ArrayAccess{
    public $objDefId;
    public $objDef;
    
    public function __construct($objDefId) {
        $this->objDefId=$objDefId;
        $this->objDef = Null;
        $this->loadObjDef();       
        $this->initialize();       
    }
    
    public function loadObjDef() {
        if (isnull($this->objDef)==False) {
            return;
        }        
        global $app;        
        $this->objDef = $app->specManager->findDef($this->objDefId, true);         
        
        // Adding this check breaks the LIMS, because not all the calls to NewSpecObj,
        // refer to objDefs that actually exist.
//        if($this->objDef===null){
//            throw new Exception("The objDef '{$this->objDef}' does not exist");
//        }
    }
    
    public function initialize() {
        
        if (is_null($this->objDef)) {
            return;
        }
        
        foreach($this->objDef->getProperties() as $propDef) {        
            if ($propDef->isCollection()) {
                $this->{$propDef->name} = array();
            } else {
                $defaultValue = $propDef->getDefaultValue();
                $this->{$propDef->name} = $defaultValue;
            }
        }        
    }
    
    public function getObjDefId () {
        return $this->objDefId;
    }

    public function getObjDef () {
        return $this->objDef;
    }
        
    public function setObjDefId ($value) {
        $this->objDefId = $value;
    }

    public function setObjDef ($value) {
        $this->objDef = $value;
    }
    
    public function offsetSet($offset, $value) {
        $this->{$offset}=$value;                    
    }
    public function offsetExists($offset) {
        return isset($this->{$offset});
    }
    public function offsetUnset($offset) {
        unset($this->{$offset});
    }
    public function offsetGet($offset) {
        return isset($this->{$offset}) ? $this->{$offset} : null;
    } 
    
    // TODO: this is not nested, fix add nested recursion    
    public function loadFromArray($data) {
        
        if (is_array($data)===False) {
            return;
        }
        
        $this->loadObjDef();
        
        // does not handle nesting 
        // TODO: add support for nesting
        
        foreach($this->objDef->getProperties() as $propDef) {        
            if (isset($data[$propDef->name])) {
                $value = $data[$propDef->name];                
                $this->{$propDef->name} = $value;
            }
        }
    }    
        
    public function mergeFromArray($data) {
        
        if (is_array($data)===False) {
            return;
        }
        
        $this->loadObjDef();
        
        // does not handle nesting 
        // TODO: add support for nesting
        
        foreach($this->objDef->getProperties() as $propDef) {        
            if (isset($data[$propDef->id])) {
                $value = $data[$propDef->id];                                
                                                                
                // transform value from array data array to Propdef datatype                                               
                if (isset($this->{$propDef->name})==false) {
                    $this->{$propDef->name} = $value;
                } else if (isset($this->{$propDef->name})==true) {
                    
                    $oldValue  = $this->{$propDef->name};
                    
                    if (isnull($oldValue)) {
                        $this->{$propDef->name} = $value;    
                    }
                }               
            }
        }
    }    
            
    // TODO: this is not nested, fixed added nested recursion
    // TODO: what about default values?  This function could add them.
    public function getAsArray() {
        $data = array();
        $this->loadObjDef();
        
        foreach($this->objDef->getProperties() as $propDef) {        
            if (isset($this->{$propDef->name})) {
                $value = $this->{$propDef->name};                
                $data[$propDef->name] = $value;                                                
            } else {
                $data[$propDef->name] = null;
            }
        }         
        return $data;
    }
                   
    public function getAsString($indent=null) {
        
        $str=new StringBuilder();
            
        $this->loadObjDef();
        
        foreach($this->objDef->getProperties() as $propDef) {        
            if (isset($this->{$propDef->name})) {
                $str->addline($indent.$propDef->name.':'.$this->{$propDef->name});
            } else {
                $str->addline($indent.$propDef->name.':null');
            }
        }         
        return $str->getString();
    }
        
    public function addDefaultValues($mode="any") {
        $this->loadObjDef();
        $this->objDef->addDefaultValues($this,"any");
    }               
    
//    public function __get($name) {
//        // check spec                        
//        
//        if ($this->objDef->isProperty($name)) {
//            
//            $propDef = $this->objDef->getProperty($name);
//            // if the value is not set, check if it has a default value if it does, return that value
//            
//            $defaultValue = $propDef->getDefaultValue();
//            
//            // if no default value, return Null
//            
//            return $defaultValue;
//            
//        } else {
//            return Null;
//        }
//    }
    
    // json_encode($value) does not work anywork
    public function getArrayOfData() {
        global $app;
        
        $myArray = array();        
         foreach($this->objDef->getProperties() as $propDef) {
            if (isset($this->{$propDef->name})) {                
                if ($propDef->isScalar) {
                    $myArray[$propDef->name] = $this->{$propDef->name}; 
                    // some fields require data to be placed in a display column as well
                    // data for fields that do not exist are ignored, so it is safe to 
                    // add a display column for every property;                    
                    if (isset($this->{"display-".$propDef->name})) {
                        $myArray["display-".$propDef->name]=$this->{"display-".$propDef->name};                     
                    }                    
                } else if($propDef->isCollection()) {
                    $myArray[$propDef->name]=array();
                    
                    $collectionObjDef = $app->specManager->findDef($propDef->objectTypeOrig);
                    if ($collectionObjDef->isAssociative) {                                        
                        foreach($this->{$propDef->name} as $key=>$obj){
                            $myArray[$propDef->name][$key]=$obj->getArrayOfData();
                        }                         
                    } else {
                        foreach($this->{$propDef->name} as $obj){
                            $myArray[$propDef->name][]=$obj->getArrayOfData();
                        }                         
                    }
                } else {
                    $myArray[$propDef->name]=$this->{$propDef->name}->getArrayOfData();
                }                               
            }else{
                $myArray[$propDef->name] = Null;
            }
        }         
                
        return $myArray;        
    }
    
    

                                    
                                    
    public function json_encode() {        
        // transfer property keys and there values to an new array, 
        // call json_ecode, pass that back
        
        $json_array = array();
        
         foreach($this->objDef->getProperties() as $propDef) {        
            if (isset($this->{$propDef->name})) {
                $json_array[$propDef->name] = $this->{$propDef->name};
            } else {
                $json_array[$propDef->name] = Null;
            }
        }         
        $json = json_encode($json_array);
        
        return $json;
    }
    
}
?>
