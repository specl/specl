<?php
require_once("Condition.php");
require_once("Attribute.php");
/**
 * Description of BaseDef
 *
 * @author tony
 */
class BaseDef {
    public $id;
    public $defType;
    public $crossChecked;
    public $conditions;    
    public $constraint;
    public $attributes; // used to hold key/value pairs    
    public $description; // comments
    public $associations;    
    public $transforms;    
    public $mode;
    public $domain;     
    public $lineage;
    
    public function  __construct($id, $defType, $domain) {

        if (is_null($id)) {
            // TODO throw up
        }

        if (empty ($id)) {
            // TODO throw up
        }

        if (strlen(trim($id))) {
            // TODO throw up
        }
        
        $this->lineage = Array();
        $this->id = $id;
        $this->lineage[] = $id;
        
        $this->defType = $defType;
        $this->crossChecked = false;
        $this->conditions= array();
        $this->conditions["any"]=array();
        $this->attributes= array();
        $this->attributes["any"]=array();        
        $this->constraint = Null;
        //$this->constraint = array();
        //$this->constraint["any"]=array();        
        $this->associations=null;
        $this->mode='any';
        $this->domain=$domain;
        $this->transforms["any"]=array();        
        
        
    }

    public function getType() {
        return $this->defType;
    }

    public function isObject() {
        if ($this->defType == "object") {
            return true;
        } else {
            return false;
        }
    }

    public function isCollection() {
        if ($this->defType == "collection") {
            return true;
        } else {
            return false;
        }
    }

    public function addAttribute($key, $value,$mode=null) {
        $this->addAttribute($key, $value,$mode);
    }
            
    public function setAttribute($key, $value,$mode=null)
    {
        if(is_null($mode)) {
            $mode=$this->mode;
        }
        
        if (is_array($mode)) {         
            foreach($mode as $m) {
                $attr = new Attribute();
                $attr->key = $key;
                $attr->value = $value;
                $this->attributes[$m][$key]=$attr;                
            }
        } else {
            $attr = new Attribute();
            $attr->key = $key;
            $attr->value = $value;
            $this->attributes[$mode][$key]=$attr;
        }
    }

    // returns the $defaultVault if the key can't be found or the value is null
    public function getAttribute($key, $defaultValue=null, $mode=null) {
        
        if(is_null($mode)) {
            $mode=$this->mode;
        }        
        
        if(!isset($this->attributes[$mode])){
            $this->attributes[$mode]=array();
        }
        
        if (array_key_exists($key, $this->attributes[$mode])) {
            $attr = $this->attributes[$mode][$key];
            $value = $attr->value;
            
            if (is_null($value)) {
                return $defaultValue;
            }
            return $value;
        } 
        else {
            if(array_key_exists($key,$this->attributes['any'])) {
                $attr = $this->attributes['any'][$key];
                $value = $attr->value;
                if (is_null($value)) {
                    return $defaultValue;
                }                
                return $value;
            } else {
                return $defaultValue;
            }
        }   
    }

    public function removeAttribute($key,$mode=null) {
        if(is_null($mode)){
            $mode=$this->mode;
        }
        
        if(!isset($this->attributes[$mode])){
            $this->attributes[$mode]=array();
        }
        
        if (array_key_exists($key, $this->attributes[$mode])) {
            unset($this->attributes[$key]);
        }
    }

    public function hasAttribute($key, $mode=null) {
        
        if (is_null($mode)) {
            $mode=$this->mode;
        }

        if(!isset($this->attributes[$mode])){
            $this->attributes[$mode]=array();
        }


        if (array_key_exists($key, $this->attributes[$mode])) {
            return true;
        } else {
            if(array_key_exists($key,$this->attributes["any"])) {
                return true;
            }
        }
        
        return false;
    }
 
    public function isAttribute($key, $value, $mode=null) {
        if ($this->hasAttribute($key,$mode)) {
            
            $currVal = $this->getAttribute($key, null, $mode);
            
            if ($value===$currVal) {
                return true;
            } else {
                return false;
            }   
            
        } else {
            return false;
        }
    }    
    
    
    public function addCondition($conditionObj, $mode="any")
    {       
        if(is_a($conditionObj, "Condition"))
        {
            if (is_array($mode)) {
                foreach ($mode as $m) {
                    $this->conditions[$m][]=$conditionObj;
                }
            } else {
                $this->conditions[$mode][]=$conditionObj;
            }
        }
        else
        {
            throw new Exception("Trying to add condtion that is not a condition object");
        }
    }  
    

    
    
    public function hasCondition($conditionType, $mode="any")  { 
        
        $conditions = $this->getConditions($mode);
        
        foreach($conditions as $condition)
        {
            if (is_a($condition, $conditionType)) {
               return true;
            }
        }
        
        return false;
    }
    
    
    
    /* 
     * This return the first one that is found.  A better way needs to be designed.
     * TODO: create unique ids for conditions, create rules and enforce them
     * as to the number of specific types of condition that can be added to 
     * any one property. 
     */
    public function getCondition($conditionType, $mode="any")  { 
        
        $conditions = $this->getConditions($mode);
        
        foreach($conditions as $condition)
        {
            if (is_a($condition, $conditionType)) {
               return $condition;
            }
        }        
        return Null;
    }  

    
    public function getConditionByName($name, $mode="any")  { 
        
        $conditions = $this->getConditions($mode);
        
        foreach($conditions as $condition)
        {
            if ($condition->name == $name) {
               return $condition;
            }
        }        
        return Null;
    }  
    
    
    public function clearConstraint()
    {
        $this->constraint=Null;
    }
    
    public function setConstraint($obj)
    {
        if(is_a($obj, "Constraint"))
        {            
            $this->constraint=$obj;
        }   
        else
        {
            throw new Exception("Trying to set constraint to an object that is not a Constraint.");
        }
    }

    public function getConstraint()
    {
        return $this->constraint;
    }

    public function hasConstraint()
    {
        if (is_null($this->constraint)) {
            return False;
        } else {
            return True;
        }        
    }    
    
    public function addAttributesFromArray($attributes,$mode=null) {
        if(is_null($mode)) {
            $mode=$this->mode;
        }
        
        foreach($attributes as $attr) {
            //$this->setAttribute($attribute->key, $attribute->value);

            $this->attributes[$mode][$attr->key]=$attr;
        }
    }

    public function createAssociationTo($objDefId) {
         $associationObj = new AssociationDef($objDefId);
         $this->associations[] = $associationObj;
         return $associationObj;
    }
    
    
    public function addAssociation($associationObj) {
        $this->associations[]=$associationObj;
    }

    public function getAssociations() {
        return $this->associations;
    }

    public function hasAssociations() {
        if (count($this->associations)>0) {
            return true;
        } else {
            return false;
        }
    }    
    
    public function clearAssociation() {
        $this->associations=array();
    }
    
    public function setMode($mode)
    {
        $this->mode=$mode;
    }
    
    public function getConditions($mode=null)
    {
        if (is_null($mode)){
            $mode=$this->mode;
        } 
        
        if(isset($this->conditions["any"])){
            $conditions=$this->conditions["any"];
        } else {
            $conditions=array();
        }
        
        if ($mode != "any"){
            if(isset($this->conditions[$mode])){                                
                $conditions=array_merge($conditions,$this->conditions[$mode]);
            }
        }
        
        return $conditions;
    }
    
    public function clearConditions($mode=null){
        
        // clear all conditions
        if(is_null($mode))
        {
            $this->conditions= array();
            $this->conditions["any"]=array();
        }
        // clear the conditions only in the specified mode
        else{
            $this->conditions[$mode]=array();
        }  
    }

    public function addTransform($transformObj, $mode="any")
    {
        if (is_a($transformObj, "Transform") || (is_subclass_of($transformObj, "Transform")))
        {
            if (is_array($mode)) {
                foreach ($mode as $m) {
                    $this->transforms[$m][]=$transformObj;
                }
            } else {
                $this->transforms[$mode][]=$transformObj;
            }
        }
        else
        {
            throw new Exception("Trying to addTransform with non-Transform object");
        }
    }

    public function getTransforms($mode=null)
    {
        if(is_null($mode)){
            $mode=$this->mode;
        }
        
        $transforms=$this->transforms["any"];
        
        if($mode!="any"){
            if(isset($this->transforms[$mode])){
                $transforms=array_merge($transforms,$this->transforms[$mode]);
            }
        }
        
        return $transforms;
    }        
    
    
    public function extend($id) {
        $this->lineage[] = $id;
    }
}
?>
