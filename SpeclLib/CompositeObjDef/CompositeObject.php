<?php
/**
 * This class stores an objDef along with addition parameters passed into the addObject function. This
 * is needed so that the CompositeObjDef specific parameters are seperate from the objDef and do not
 * change its definition.
 */
class CompositeObject {
   public $objDef;
   public $params;
   public function __construct($objDef,$params){
       $this->objDef=$objDef;
       $this->params=$params;
   }
}

?>
