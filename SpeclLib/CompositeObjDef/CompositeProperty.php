<?php
/**
 * This class stores a propDef, the name of the object the propDef belongs to, the name of the property passed in and addition parameters passed 
 * into the addObject function. This is needed so that the CompositeObjDef specific parameters are seperate 
 * from the objDef and do notchange its definition.
 */
class CompositeProperty {
   public $objectName;
   public $propDef;
   public $propertyName;
   public $params;
   public function __construct($objectName,$propertyName,$propDef,$params){
       $this->objectName=$objectName;
       $this->propertyName=$propertyName;
       $this->propDef=$propDef;
       $this->params=$params;
   }
}

?>
