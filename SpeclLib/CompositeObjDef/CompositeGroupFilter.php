<?php
/*
 * A group of filters surrounded by ()
 * 
 */
class CompositeGroupFilter {
    public function __construct(){
        $this->filters=array();
    }
    
    public function addValueFilter($objectName,$propertyName,$operation,$value,$conjunction="AND"){
       $this->filters[]=new CompositeValueFilter($objectName,$propertyName,$operation,$value,$conjunction);
    }
}

?>