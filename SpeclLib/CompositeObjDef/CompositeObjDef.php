<?php
/* A CompositeObjDef is a spec made of one or more objDefs. Unlike normal objDefs, a composite objDef
 * does not have to correspond with a table in the database.
 * 
 * CompositeObjDefs are retrieved through the findDef method of the specManager, just like objDefs. They are
 * also defined in SpecDefs, just like ObjDefs.
 * 
 * Each CompositeObjDef must have a unique name and cannot have the same name as an existing objDef either.
 * 
 * 
 */
class CompositeObjDef {
   protected $objects;
   protected $properties;
   protected $filters;
   protected $aggregates;
   protected $transformations;
   public $name;
   
   public function __construct($params=array()){
       $this->objects=array();
       $this->properties=array();
       $this->filters=array();
       $this->aggregates=array();
       $this->transformations=array();
       if(isset($params["name"])){
           $this->name=$params["name"];
       }
       else{
           $this->name=null;
       }
   }
   
   /*
    * Add Object adds an existing object to the composite. The objDef being added must
    * already be defined. The same object cannot be added more than once.
    * 
    * TODO:
    *   - raise error when objDef does not exist
    *   - raise error when object has already been added to composite.
    */
   public function addObject($objectName,$params=null){
       global $app;
       
       $objDef=$app->specManager->findDef($objectName);
       
       if($objDef===null){
           throw new Exception("The objDef {$objectName} is not defined");
       }
       
       if(isset($this->objects[$objectName])){
           throw new Exception("The objDef {$objectName} has already been added to the CompositeObjDef");
       }
       
       $this->objects[$objectName]=new CompositeObject($objDef,$params);
       
   }
   
   /*
    * Add a property from an object to the composite. The object must already be added to the composite
    * and the property must exist in the object. A property can only be added once per object.
    */
   public function addProperty($objectName,$propertyName,$params=array()){
       if(!isset($this->objects[$objectName])){
           throw new Exception("The object {$objectName} has not been added yet to the composite spec");
       }
       $objDef=$this->objects[$objectName]->objDef;
       if(!$objDef->isProperty($propertyName)){
           throw new Exception("The property {$propertyName} does not exist in the objDef {$objectName}");
       }
       
       $propName=$objectName.".".$propertyName;
       if(isset($this->objects[$propName])){
           throw new Exception("The property {$propertyName} for the objDef {$objectName} has already been added to the CompositeObjDef");
       }
       $propDef=$objDef->getProperty($propertyName);
       
       $this->properties[$propName]=new CompositeProperty($objectName,$propertyName,$propDef,$params);
   }
   
   public function addCountProperty(){
       if(!isset($this->properties["count"])){
           $this->properties["count"]=new CountProperty();
       }
   }
   
   /*
    * Build a filter from an object, a column, an operator and a value
    * TODO: Error handling to make sure that "and" and "or" are the only values passed into conjunction
    */
   public function addValueFilter($objectName,$propertyName,$operation,$value,$conjunction="AND"){
       $this->filters[]=new CompositeValueFilter($objectName,$propertyName,$operation,$value,$conjunction);
   }
   
   public function addNamedValueFilter($filterName,$objectName,$propertyName,$operation,$value,$conjunction="AND"){
       if(!isset($this->filters[$filterName])){
            $this->filters[$filterName]=new CompositeValueFilter($objectName,$propertyName,$operation,$value,$conjunction,array("name"=>$filterName));
       }
   }
   
   public function addCollectionValueFilter($objectName,$propertyName,$operation,$value,$conjunction="AND"){
       $this->filters[]=new CompositeValueFilter($objectName,$propertyName,$operation,$value,$conjunction,array("collectionFilter"=>true));
   }
   
   public function addNamedCollectionValueFilter($filterName,$objectName,$propertyName,$operation,$value,$conjunction="AND"){
       if(!isset($this->filters[$filterName])){
            $this->filters[$filterName]=new CompositeValueFilter($objectName,$propertyName,$operation,$value,$conjunction,array("name"=>$filterName,"collectionFilter"=>true));
       }
   }
   
   public function addFilter($filterObj,$conjunction="AND"){
       $filterObj->conjunction=$conjunction;
       $this->filters[]=$filterObj;
   }
   
   public function addAggregate($objectName,$propertyName,$params=null){
       $this->aggregates[]=new CompositeAggregate($objectName,$propertyName,$params);
   }
   
   public function addTransform($transformation,$params=null){
       
   }   
   
   public function addConnection($obj1,$prop1,$obj2,$prop2,$params){
       
   }
   
   public function getObjects(){
       return $this->objects;
   }
   
   public function getProperties(){
       return $this->properties;
   }
   
   public function getFilters(){
       return $this->filters;
   }
   
   public function getAggregates(){
       return $this->aggregates;
   }
   
   public function removeAllProperties(){
       $this->properties=array();
   }
   
   /*
    *  create a copy with no references.
    * 
    */
   public function copy(){
       return unserialize(serialize($this));
   }
   
   public function getNamedFilter($filterName){
       if(isset($this->filters[$filterName])){
           return $this->filters[$filterName];
       }
       else{
           return null;
       }
   }
}

?>
