<?php

class CompositeValueFilter {
    public $objectName;
    public $propertyName;
    public $operation;
    public $value;
    
    public function __construct($objectName,$propertyName,$operation,$value,$conjunction="AND",$params=array()){
        $this->objectName=$objectName;
        $this->propertyName=$propertyName;
        $this->operation=$operation;
        $this->value=$value;
        $this->conjunction=$conjunction;
        $this->params=array();
    }
}

?>