<?php
/**
 * Description of Collection
 *
 * @author Tony
 */
class CollectionDef extends BaseDef {

    public $name;
    public $isAssociative;
    public $childTypes;
    public $keyId;
    public $setter;
    public $allowAllChildren;
    public $childIsCollection;
    public $collectionTag;


    public function  __construct($params, $isAssociative=false) {

        if (is_array($params)) {
            if (isset($params["id"])) {
                $id = $params["id"];
            }

            if (isset($params["isAssociative"])) {
                $isAssociative = $params["isAssociative"];
            } else {
                $isAssociative = false;
            }
        } else {
            $id = $params;
        }
       
        $id = str_replace(":", "_", $id);
         
        $this->name = $id;
        $id = strtolower($id);
       
        if (is_array($params)) {
            if (isset($params["domain"])) {
                $domain = $params["domain"];
            } else {
                $domain = "sys";
            }
        } else {
            $domain = "sys";  // the default domain is sys
        }
        
        
        parent::__construct($id, 'collection', $domain);
        
        if (is_bool($isAssociative)) {
            $this->isAssociative = $isAssociative;
        } else {
            throw new Exception("CollectionDef __construct() Warning: parameter isAssociative given '{$isAssociative}' must be of type boolean.");
        }

        $this->childTypes=array();

        $this->keyId = null;
        $this->setter=null;

        $this->allowAllChildren = false;

        $this->childIsCollection = false; // if true this collection, is a collection of another collection
    }

    public function setAllowAllChildren($value) {
        if (is_bool($value)) {
            $this->allowAllChildren = $value;
        }
    }
    
    public function addChildTypesFromPath($path) {
        
        $filesToProcess = array();
        if ($handle = opendir($path)) {
            while (false !== ($filename = readdir($handle))) {
                if (($filename!=".") && ($filename!="..")) {            

                    $path_parts = pathinfo($filename);
                    if (isset($path_parts['extension'])) {
                        $ext = $path_parts['extension'];
                        $basename =  $path_parts['basename'];
                        $basename = substr($basename, 0, -strlen($ext)-1);  

                        if ($ext=="php") {                            
                            $end = substr($basename,-4, 4);
                            
                            if (strcmp($end, "Spec")==0) {                                                                            
                                $this->addChildType(substr($basename,0,-4));   
                            }
                        }
                    }
                }
            }
            closedir($handle);
        }
        

    }            
            
    public function addChildTypesFromArray($childTypes) {
        foreach($childTypes as $objDef) {
            $this->addChildType($objDef->id);
        }
    }

    public function addChildType($type) {
        
        if (is_string($type)) {
            $type = str_replace(":","_",$type);
        
            if (array_key_exists(strtolower($type), $this->childTypes)==false) {
                $this->childTypes[strtolower($type)] = $type; // look it up and fill in the type at runtime
            }
        } else {
            
            if (array_key_exists($type->id, $this->childTypes)==false) {
                $this->childTypes[$type->id] = $type; // look it up and fill in the type at runtime
            }
            
        }
    }

    public function isChildType($type) {

        if ($this->allowAllChildren==true) {
            return true;
        } else {
            
            $type = str_replace(":","_",$type);
            
            if (array_key_exists(strtolower($type), $this->childTypes)==true) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getChildType($origType) {
        global $app;

        if ($origType=="parameterValue") {
            $origType="ParameterValue";
        }
        
        $type = strtolower($origType);
        $type = str_replace(":","_",$type);
        
        // cache the child type inside the collectionDef, so the childDef does not need to be loaded
        // again during a webservice call.
        if (array_key_exists($type, $this->childTypes)==true) {
            if(is_string($this->childTypes[$type])){
                $childDef=$app->specManager->findDef($origType);
                $this->childTypes[$type]=$childDef;
            }
            else{
                $childDef=$this->childTypes[$type];
            }            
            return $childDef;
        } else {
            return null;
        }
    }

    public function setKeyId($propId) {
        $propId = strtolower($propId);
        $this->keyId = $propId;
    }

    public function getKeyId() {
        return $this->keyId;
    }

    public function setSetter($setter) {
        $this->setter=$setter;
    }
    
    public function getFirstChildDef(){
        global $app;
        $childTypes = array();        
        // childTypes are stored as a key/value pair by name, must convert to array indexed by number
        foreach($this->childTypes as $childType) {
            $childTypes[] = $childType;
        }
        
        // cache the child type inside the collectionDef, so the childDef does not need to be loaded
        // again during a webservice call.
        if(is_string($childType)){
            $childDef=$app->specManager->findDef($childTypes[0]);
            $this->childTypes[$childType]=$childTypes[0];            
        }
        else{
            $childDef=$childType;
        }
        return $childDef;        
    }
}
?>
