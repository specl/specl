<?php

class WhenCondition extends Condition
{
    public $errMsg;
    public $when;
    public $check;

    public function __construct($when, $check)
    {
        parent::__construct();
        
        $this->when = $when;
        $this->check = $check;
        
        $this->errCode = get_class($this) ." ERR00303";
    }
        
    public function validate($object, $objData)
    {
        
    }

    public function getErrorMessage()
    {
        return "";
    }
}
?>
