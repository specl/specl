<?php
class EqualCondition extends Condition {
    
    public $compareValue;
    
    public function __construct($value) {
        parent::__construct();
        
        $this->compareValue=value;
    }

    public function validate($value) {
                
        $this->valueValidated=$value;
                
        if ($this->compareValue===$value) {
            return true;
        } else {
            return false;
        }
    }

    public function getErrorString($sep=Null)
    {        
        $this->setErrorMsg("[".$this->valueValidated."] does not equal the expected value of [".$this->compareValue."]");
        return parent::getErrorString($sep);
    }    
}
?>