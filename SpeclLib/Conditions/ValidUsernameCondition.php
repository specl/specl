<?php


function validate_alphanumeric_underscore($str) 
{
    return preg_match('/^[a-zA-Z0-9_]+$/',$str);
}


class ValidUsernameCondition extends Condition {

    public function __construct()
    {
        parent::__construct();
        $this->setErrorMsg("Not a valid username.");
    }

    public function validate($user)
    {
        $isValid = true;
        
        $isValid = validate_alphanumeric_underscore($user);
        
        $this->valid=$isValid;
        $this->validated=true;        
        return $isValid;
    }
}
?>
