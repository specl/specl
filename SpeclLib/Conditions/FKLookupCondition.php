<?php

/**
 * Description of FKLookup
 *
 * @author Tony
 */
class FKLookupCondition extends Condition {
    
    public $objDef;
    public $fkTable;
    public $fkColumn;   
    public $fkColumns;       
    public $exists;
    public $connectionName;
    public $allowNulls; // if true, Null valies will be allowed, menaing is 
                        // the value passed in is a NULL and exists is true,
                        // the return will be valid.
    public $whereClause;
    public $multiColumn;  // allows for a multicolumn unique index or similiar
    
    public function __construct($params)
    {
        if (isset($params["connectionName"])) {
            $connectionName = $params["connectionName"];
        }

        if (isset($params["fkTable"])) {
            $fkTable = $params["fkTable"];
        }
        
        if (isset($params["fkColumn"])) {
            $fkColumn = $params["fkColumn"];
        }
       
        if (isset($params["exists"])) {
            $exists = $params["exists"];
        }
        
        if (isset($params["errMsg"])) {
            $errMsg = $params["errMsg"];
        } else {
            $errMsg = null;
        }

        if (isset($params["allowNulls"])) {
            $allowNulls = $params["allowNulls"];
        } else {
            $allowNulls = true;
        }
        
        if (isset($params["whereClause"])) {
            $this->whereClause = $params["whereClause"];
        } else {
            $this->whereClause = null;
        }
        

        if(isset($params["whereStatement"])){
            $this->whereStatement=$params["whereStatement"];
        }
        else{
            $this->whereStatement=new SQLWhere();
        }
        
        
        if (isset($params["multiColumn"])) {           
            $var = $params["multiColumn"];            
            if (is_bool($var)) {
                $this->multiColumn = $var;
            } else {
                 $this->multiColumn = false;
            }                        
        } else {
            $this->multiColumn = false;
        }

        if (isset($params["objDef"])) {
            $this->objDef = $params["objDef"];
        } else {
            $this->objDef = null;
        }        
                
        parent::__construct($params);

        $this->connectionName = $connectionName;
        $this->fkTable = $fkTable;
        $this->fkColumn = $fkColumn;
        
        if (is_bool($exists)) {
            $this->exists=$exists;
        } else {
            $this->exists = true;
        }

        $this->setErrorMsg($errMsg);        
        $this->allowNulls = $allowNulls;        
    }
    
    
    public function objValidate($objData)
    {
        global $app;
        
        $objDef = $app->specManager->findDef($this->objDef);        
                
        $found = FkExistsMultiColumn($objDef, $this->connectionName, $this->fkTable, $this->fkColumn, $objData);
                    
        if ($this->exists == true) {
            if ($found==true) {
                return true;
            } else {
//                if ($this->errMsg==null) {
//                    
//                    foreach($this->fkColumn as $column) {
//                        //$this->errMsg =  "Value {$value} does not exist in the {$this->fkTable} table.";                
//                    }
//                }
                $this->errorCode = "obj_value_does_not_exist";
                return false;
            }
        } else {
            if ($found==true) {
//                if ($this->errMsg==null) {
//                    foreach($this->fkColumn as $column) {
//                        //$this->errMsg =  "Value {$value} already exists in the {$this->fkTable} table.";                    
//                    }
//                }
                $this->errorCode = "obj_value_exists";
                return false;
            } else {
                return true;
            }
        }        
        
    }
    
    public function propValidate($value)
    {
        global $app;

        
        if (is_null($value)) {
            if ($this->allowNulls == true) {
                return true;   
            } else {
                $this->setErrorMsg("[NULL] value not allowed");   
            }
        }
        
        $this->valueValidated=$value;
                 
        $whereClause = "";        
        if(count($this->whereStatement->clauses)>0){
            $filterClause=$this->whereStatement->generateWhereClause();
            
            if (!isnull($filterClause)) {
                if($this->whereClause){
                    if($filterClause!=""){
                        $filterClause.=" AND {$this->whereClause}";
                    }
                    else{
                        $filterClause=$this->whereClause;
                    }
                }
                $this->whereClause=  $app->fillInGlobals($filterClause);
            }
        }
        else if ($this->whereClause) {
            $this->whereClause =  $app->fillInGlobals($this->whereClause);    
        }
        
        if ($this->whereClause) {
            $found = FkExistsWhere($this->connectionName, $this->fkTable, $this->fkColumn, $value, $this->whereClause);
        }  else {
            $found = FkExists($this->connectionName, $this->fkTable, $this->fkColumn, $value);
        }             
    
        if ($this->exists == true) {
            if ($found==true) {
                return true;
            } else {                
                $this->setErrorMsg("[".$this->valueValidated."] does not exist in the table {$this->fkTable}");
                $this->errorCode = "prop_value_does_not_exist";
                return false;
            }
        } else {
            if ($found==true) {                
                $this->setErrorMsg("[".$this->valueValidated."] already exists in the table {$this->fkTable}");
                $this->errorCode = "prop_value_exists";
                return false;
            } else {
                return true;
            }
        }        
    }
    
    public function validate($value, $objData=Null)
    {
        if (is_null($value)) {
            if ($this->allowNulls == true) {
                return true;   
            } else {
                $this->setErrorMsg("[NULL] value not allowed");   
            }
        }
        
        if (is_object($value)) {
            return $this->objValidate($value);
        } else {
            return $this->propValidate($value);
        }
                        
    }
}
?>
