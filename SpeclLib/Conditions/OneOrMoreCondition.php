<?php

class OneOrMoreCondition extends Condition{
    public function __construct($params){        
        parent::__construct($params);
        $this->setErrorMsg("At least one value must be selected");
    }
    public function validate($value){
        if(count($value)>=1){
            return true;
        }
        else{
            return false;
        }
    }
}

?>