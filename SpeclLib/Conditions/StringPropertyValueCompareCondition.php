<?php
class StringPropertyValueCompareCondition extends Condition
{
    public $propNames;  // not Lowercased!
    public $property1;
    public $property2;
    public $operator;  // =, !=
    public $errMsg;

    public function __construct($property1,$property2,$operator)
    {
        parent::__construct();

        $this->property1=$property1;
        $this->property2=$property2;
        $this->operator=$operator;

        $this->propIds = array();

        if (!is_null($this->property1)) {
            $this->propNames[]=$this->property1->name;
        }

        if (!is_null($this->property2)) {
            $this->propNames[]=$this->property2->name;
        }
        //$this->errCode = get_class($this) ." ERR00303";
    }

    public function validate($object, $objData)
    {
        $this->errMsg = "";

        if (is_null($object)) {
            $this->errMsg .= "object was null ";
            return false;
        }

        if (is_null($this->property1)) {
            $this->errMsg .= "property value was null ";
            return false;
        }

        if (is_null($this->property2)) {
            $this->errMsg .= "property value was null ";
            return false;
        }

        $value1=null;
        $value2=null;

        if (isset($objData[$this->property1->id])) {
            $value1 = $objData[$this->property1->id];
        } else {
            $this->errMsg = "object missing the property 1 ";
        }
        
        if (isset($objData[$this->property2->id])) {
            $value2 = $objData[$this->property2->id];
        } else {
            $this->errMsg .= "object missing the property 2 ";
        }

        if ($value1==$value2) {
            return true;
        } else {
            return false;
        }
    }

    public function getErrorString($sep=Null)
    {
        $msg="The value of {$this->property1->name} must be the same as the value of {$this->property2->name}, {$this->errMsg}";
        $this->setErrorMsg($msg);
        return parent::getErrorString($sep);
    }

    // retrun an array of the two propIds
    public function getPropIds()
    {
        return $this->propNames;
    }
}
?>
