<?php

/**
 * Description of FKLookup
 *
 * @author Tony
 */
class UniqueCondition extends Condition {
    public $objDefId;
    public $propDef;   
    public $errMsg;
    public $connectionName;
    public $allowNulls; // if true, Null valies will be allowed, menaing is 
                        // the value passed in is a NULL and exists is true,
                        // the return will be valid.
        
    public function __construct($params)
    {
        if (isset($params["connectionName"])) {
            $connectionName = $params["connectionName"];
        } else {
            $connectionName = null;
        }

        if (isset($params["objDefId"])) {
            $objDefId = $params["objDefId"];
        }
        
        if (isset($params["propDefId"])) {
            $propDefId = $params["propDefId"];
        }
       
        if (isset($params["errMsg"])) {
            $errMsg = $params["errMsg"];
        } else {
            $errMsg = "FKLookupCondition invalid";
        }

        if (isset($params["allowNulls"])) {
            $allowNulls = $params["allowNulls"];
        } else {
            $allowNulls = true;
        }
                
        parent::__construct($params);

        //$this->connectionName = $connectionName;
        //$this->objDefId = $objDefId;
        //$this->propDefId = $propDefId;
        //$this->errMsg = $errMsg;
        //$this->allowNulls = $allowNulls;       
        
        //$objDef = $app->specManager->findDef($this->objDefId);                        
        //$dal = NewDALObj($objDef->name);
    }
    
    public function validate($objDef, $objData)
    {
        // if 
        
        
//        global $app;
//        
//        if (is_null($value)) {
//            if ($this->allowNulls == true) {
//                return true;   
//            }
//        }
//        
//        // for what primary keys does the value exist in the table?
//        
//        $primaryKeys = $dal->getPKforMatches($this->propDefId, $value);
//        
//        //select flowCellId from FlowCell where SerialNumber = 'FC-111-111-112';
//        //select flowCellId from FlowCell where SerialNumber = 'FC-111-111-112' and FLowCellId != 7;
//
//        
//        if ($dal->isUnique($this->propDefId, $value)) {
//            return true;
//        } else {
//            $this->errMsg =  "The value {$value} already exists.";  
//            return false;
//        }
        
             
//        if ($this->whereClause) {
//            $found = FkExistsWhere($this->connectionName, $this->fkTable, $this->fkColumn, $value, $this->whereClause);
//        }  else {
//            $found = FkExists($this->connectionName, $this->fkTable, $this->fkColumn, $value);
//        }         
//        
//        if ($this->exists == true) {
//            if ($found==true) {
//                return true;
//            } else {
//                if ($this->errMsg==null) {
//                    $this->errMsg =  "Value {$value} does not exist in the {$this->fkTable} table.";                
//                }
//                return false;
//            }
//        } else {
//            if ($found==true) {
//                if ($this->errMsg==null) {
//                    $this->errMsg =  "Value {$value} already exists in the {$this->fkTable} table.";
//                }
//                return false;
//            } else {
//                return true;
//            }
//        }
        
        return true;
    }

    public function getErrorMessage()
    {
        
        return $this->errMsg;
    }
}
?>
