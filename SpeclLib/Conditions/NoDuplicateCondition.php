<?php
class NoDuplicateCondition extends Condition {            
    public $table;
    public $column;
    public $connectionName;
    public $propNames;
    
    public function __construct($params)
    {             
        if (isset($params["connectionName"])) {
            $connectionName = $params["connectionName"];
        } else {
            $connectionName = null;
        }
        
        if (isset($params["table"])) {
            $table = $params["table"];
        } else {
            $table = null;
        }

        if (isset($params["column"])) {
            $column = $params["column"];
        } else {
            $column = null;
        }
          
        if (isset($params["errMsg"])) {
            $this->setErrorMsg($params["errMsg"]);
        }
        
        parent::__construct($params);
        $this->table = $table;
        $this->column = $column;   
        $this->connectionName = $connectionName;        
        $this->propNames = array();
        $this->propNames[]=$this->column;

    }
    
    public function validate($objDef,$objData)
    {        
        if (is_subclass_of($objData, "SpecCls")) {
            $data = $objData->getAsArray();
        } else {
            $data = $objData;
        }          
        
        if (isset($data[$this->column])) {
            $value = $data[$this->column];
        } else {
            $value = null;            
        }
                                                        
        $dal = NewDALObj($objDef->name);
        // does the $objData have a primary key?        
        $primaryKeySet = true;        
        foreach($objDef->primaryKey as $key) {
            $pkPropDef = $objDef->getProperty($key);
            if (!isset($data[$pkPropDef->name])) {
                $primaryKeySet = false;  // no composite primary keys wiuth null values allowed
            }
        }
        
        $found = false;
        if ($primaryKeySet===true) {            
            // try to get the row for the primary key, this could fail, because this could be a "add"
            $currObj = $dal->getOneUsingOriginalPK($data);
            
            if (!isnull($currObj)) {
                // CASE 1
                // the data already exists in the database                
                
                //CASE 1.A
                // the value could have stayed the same
                if ($data[$this->column] == $currObj->{$this->column}) {
                    $found = false;
                } else {
                    //CASE 1.B
                    // the value could have changed to another value that already exists 
                    
                    //CASE 1.C
                    // the value could have changed to a value that does not exist                                    
                    $found = FkExists($this->connectionName, $this->table, $this->column, $value);
                }
            } else {
                // CASE 2                
                // the row was not found in the database, this must be a add or a upload                
                $found = FkExists($this->connectionName, $this->table, $this->column, $value);
            }            
        } else {            
            // CASE 3            
            // primary key not set, check to see if value exists in another row.            
            $found = FkExists($this->connectionName, $this->table, $this->column, $value);
        }
                           
        if ($found==true) {
            if (empty($this->errorMsgs)) {
                $this->setErrorMsg("Value {$value} already exists.");
            }
            return false;
        } else {
            return true;
        }
        
        return true;
    }
    
    public function getPropIds()
    {
        return $this->propNames;
    }    
}
?>
