<?php
class PropertyOfChildUnique extends Condition
{
    public $propDefId;
    public $dupValue;
    public $ignoreIfEmpty;
    
    public function __construct($params)
    {
        if (isset($params["propDefId"])) {
            $this->propDefId = $params["propDefId"];
        } else {
            $this->propDefId = null;
        }
         
        if (isset($params["ignoreIfEmpty"])) {
            $this->ignoreIfEmpty = $params["ignoreIfEmpty"];
        } else {
            $this->ignoreIfEmpty = true;
        }        
        parent::__construct();
    }

    public function validate($value)
    {
        $tmp = array(); // array to hold values
        
        if (is_array($value)) {            
            foreach($value as $item) {                
                if (isset($item[$this->propDefId])) {
                    $itemValue = $item[$this->propDefId];

                    $itemValue = trim($itemValue); 
                    if ((strlen($itemValue)==0) &&  $this->ignoreIfEmpty) {
                        continue;
                    }
                                
                    if (isset($tmp[$itemValue])) {
                        // Error, value appears more than once
                        $this->dupValue= $itemValue;
                        return false;
                    } else {                        
                        $tmp[$itemValue]=true;
                    }
                } else if ($this->ignoreIfEmpty) {
                    return true;
                }
            }
        } else {
            // Error, value is not an array
            if (isnull($value)) {
                return true;
            } else {
                return true;
            }            
        }
        
        return true;
    }

    public function getErrorString($sep=Null)
    {
        if (is_null($this->dupValue)) {
            $value = "NULL";
        } else if (empty($this->dupValue)) {
            $value = "Empty String";
        } else {
            $value = "$this->dupValue";
        }                
        $msg = "{$this->propDefId} value [{$value}] appears in collection more than once.";        
        $this->setErrorMsg($msg);
        return parent::getErrorString($sep);
    }
}
?>
