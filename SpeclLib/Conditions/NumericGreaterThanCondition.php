<?php
class NumericGreaterThanCondition extends Condition
{
    public function __construct($min,$inclusive=true)
    {
        parent::__construct();
        $this->min=$min;
        $this->inclusive=$inclusive;        
    }

    public function validate($value)
    {
        // TODO: check if value is numeric
        if($this->inclusive)
        {
            if ($value >= $this->min)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
           if ($value > $this->min)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public function getErrorString($sep=Null)
    {
        if($this->inclusive)
        {
            $msg="Value must be greater than or equal to {$this->min} ";
        }
        else
        {
            $msg="Value must be greater than {$this->min} ";
        }
        $this->setErrorMsg($msg);
        return parent::getErrorString($sep);
    }
}
?>