<?php
// StringExactLength
class StringExactLengthCondition extends Condition {

    public function __construct($len)
    {
        parent::__construct();
        $this->len=$len;
    }

    public function validate($value)
    {
        $isValid=null;
        $len = strlen($value);
        if ($len==$this->len)
        {
            $isValid=true;
        }
        else
        {
            $isValid=false;
        }
        $this->valid=$isValid;
        $this->validated=true;        
        return $isValid;
    }

    public function getErrorString($sep=Null)
    {        
        $msg="String length must be exactly {$this->len} characters.";
        $this->setErrorMsg($msg);
        return parent::getErrorString($sep);
    }
}
?>
