<?php
class PropNotEmptyCondition extends Condition
{
    public $propNames;  // not Lowercased!
    public $prop;
    public $errMsg;

    public function __construct($prop)
    {
        parent::__construct();

        $this->prop=$prop;
        $this->propIds = array();

        if (!is_null($this->prop)) {
            $this->propNames[]=$this->prop->name;
        }
        
        $this->errCode = get_class($this) ." ERR00303";
    }

    public function validate($object, $objData)
    {
        $this->errMsg = "";

        if (is_null($object)) {
            $this->errMsg .= "object was null ";
            return false;
        }

        if (is_null($this->prop1)) {
            $this->errMsg .= "prop1 was null ";
            return false;
        }

        if (is_null($this->prop2)) {
            $this->errMsg .= "prop2 was null ";
            return false;
        }

        $value1=null;
        $value2=null;

        if (isset($objData[$this->prop1->id])) {
            $value1 = $objData[$this->prop1->id];
        } else {
            $this->errMsg = "object missing the property 1 ";
        }
        
        if (isset($objData[$this->prop2->id])) {
            $value2 = $objData[$this->prop2->id];
        } else {
            $this->errMsg .= "object missing the property 2 ";
        }

        if ($value1==$value2) {
            return true;
        } else {
            return false;
        }
    }

    public function getErrorMessage()
    {
        $msg="The value of {$this->prop1->name} must be the same as the value of {$this->prop2->name}, {$this->errMsg}";
        //$msg .= " ErrCode:{$this->errCode}";
        return $msg;
    }

    // retrun an array of the two propIds
    public function getPropIds()
    {
        return $this->propNames;
    }
}
?>
