<?php

/**
 * Description of FKLookup
 *
 * @author Tony
 */
class SQLConditionObj extends Condition {
    
    public $statement;
    public $errorColumns;
    public $connectionName;
    public $errorMsg;
    public $exists;
    
    public function __construct($params)
    {
        if (isset($params["connectionName"])) {
            $this->connectionName = $params["connectionName"];
        }

        if (isset($params["statement"])) {
            $this->statement = $params["statement"];
        }
        
        if (isset($params["errorColumns"])) {
            $this->errorColumns = $params["errorColumns"];
        }
       
        if(isset($params["errorMsg"])){
            $this->errorMsg=$params["errorMsg"];
        }
        
        if (isset($params["exists"])) {
            $this->exists = $params["exists"];
        }
        
        parent::__construct($params);
        
    }
    
    // Used by validation to get the field names where the errors should be displayed.
    public function getPropIds()
    {
        if(!empty($this->errorColumns)){
            return $this->errorColumns;
        }
        else{
            return Array();
        }
    }
    
    public function validate($objDef, $objData)
    {                        
        global $app;

        $app->setGlobalData("OBJ",$objData);
                
        
        $sql = $app->fillInGlobals($this->statement);
        
        $dbConn=$app->getConnection($this->connectionName);
        $results=$dbConn->select($sql);
        
        
        if ($results!=0)
        {
            $row = $dbConn->getRowAssoc($results);

            $success = $row['Success'];

            if ($success==0) {
                $foundRow=false;               
            } else {
                $foundRow=true;                
            }
        } else {
            $foundRow=false;                           
        }
    
        if (($this->exists===false)&&($foundRow===true)) {
            return false; // validation failed            
        } 
        
        if (($this->exists===false)&&($foundRow===false)) {
            return true; // validation passed         
        } 

        if (($this->exists===true)&&($foundRow===false)) {
            return false; // validation failed         
        } 
        
        if (($this->exists===true)&&($foundRow===true)) {
            return true; // validation passed         
        } 
        
        
    }
}
?>
