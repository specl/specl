<?php
class IntegerTypeCondition extends Condition
{   
    public function __construct()
    {
        parent::__construct();
        $this->setErrorMsg("Value must be an integer type");
    }

    public function validate($value)
    {
        if (preg_match('/^[-+]?([0-9]+)$/', $value)) { // -+NNNN
            return true;
        } else {
            return false;
        }
    }
}
?>