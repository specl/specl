<?php
// reguardless of type, check for not null
class NotEmptyCondition extends Condition {

    public function __construct()
    {
        parent::__construct();
        $this->setErrorMsg("Value must not be empty");
    }

    public function validate($value, $objData=Null)
    {
        if (is_null($value)) {
            return false;
        }
        
        if (is_string($value)) {
            if (is_blank($value)) {
                return false;
            } else {
                return true;
            }            
        } else {        
            return true;
        }
    }
}
?>
