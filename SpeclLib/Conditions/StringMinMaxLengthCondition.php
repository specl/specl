<?php
// String MaxLength
class StringMinMaxLengthCondition extends Condition {

    public function __construct($min,$max,$inclusive=true)
    {
        parent::__construct();        
        $this->min=$min;
        $this->max=$max;
        $this->inclusive=$inclusive;
    }

    public function validate($value)
    {
        $len = strlen($value);
        $isValid=null;        

        if ($this->inclusive)
        {
            if(($len >= $this->min) && ($len<=$this->max))
            {
                $isValid=true;
            }
            else
            {
                $isValid=false;
            }
        }
        else
        { 
           if(($len > $this->min) && ($len < $this->max))
            {
                $isValid=true;
            }
            else
            {
                $isValid=false;
            }
        }
        
        $this->valid=$isValid;
        $this->validated=true;        
        return $isValid;
    }

    public function getErrorString($sep=Null)
    {        
        $msg="String length must be between $this->min and $this->max ";

        if($this->inclusive)
        {
            $msg.="(inclusive)";
        }
        else
        {
            $msg.="(not inclusive)";
        }
        
        $this->setErrorMsg($msg);
        return parent::getErrorString($sep);
    }
}
?>
