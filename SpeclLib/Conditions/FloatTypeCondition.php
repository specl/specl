<?php
class FloatTypeCondition extends Condition
{
    
//        $msg="Value must be float type.<br/>";
//        $msg.="Allowed formats are:<br/>";
//        $msg.="-+NNNN<br/>";
//        $msg.="-+NNNN.NNNN<br/>";
//        $msg.="-+NNNN.NNNN+e-+NNNN<br/>";
//        $msg.="NaN or Inf<br/>";
        
    public function validate($value)
    {
        $this->valueValidated = $value;        
        
        $value = strtolower($value);
         
        // first check for a valid int
        // then float
        // then exponents
        // then Nan or Inf  shoudl these be lowercase check?
        if (preg_match('/^[-+]?([0-9]+)$/', $value)) { // -+NNNN
            return true;
        } else if (preg_match('/^[-+]?([0-9]*\.)[0-9]+$/', $value)) { // -+NNNN.NNNN
            return true;
        } else if (preg_match('/^[-+]?([0-9]*\.)[0-9]+e[-+][0-9]+$/', $value)) { // -+NNNN.NNNN+e-+NNNN
            return true;
        } else if ($value=='nan') {
            return true;
        } else if ($value=='inf') {
            return true;
        } else {
            return false;
        }
    }
    
    public function getErrorString($sep=Null)
    {
        $this->setErrorMsg("[".$this->valueValidated."] is not a valid float value");                 
        return parent::getErrorString($sep);               
    }
    
}
?>