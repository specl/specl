<?php
class StringMinLengthCondition extends Condition {

    public function __construct($length)
    {
        parent::__construct();        
        $this->length=$length;
    }

    public function validate($value)
    {
        $len = strlen($value);
        $isValid=null;

        if ($len>=$this->length)
        {
            $isValid=true;
        }
        else
        {
            $isValid=false;
        }
        
        $this->valid=$isValid;
        $this->validated=true;        
        return $isValid;
    }

    public function getErrorString($sep=Null)
    {
        $msg="String length must be at least {$this->length} characters.";
        $this->setErrorMsg($msg);
        return parent::getErrorString($sep);
    }
}
?>
