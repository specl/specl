<?php
class WhenEqualCondition extends Condition
{
    public $prop;
    public $value;
    public $errMsg;

    public function __construct($prop,$value)
    {
        parent::__construct();

        $this->prop=$prop;
        $this->value=$value;

        $this->errCode = get_class($this) ." ERR00303";
    }

    public function validate($object, $objData)
    {
        $this->errMsg = "";
        
        if (is_subclass_of($objData, "SpecCls")) {
            $data = $objData->getAsArray();
        } else {
            $data = $objData;
        }               
        
        if (is_null($object)) {
            $this->errMsg .= "object was null ";
            return true;
        }

        if (is_null($this->prop)) {
            $this->errMsg .= "prop was null ";
            return true;
        }

        if (is_null($this->value)) {
            $this->errMsg .= "value was null ";
            return false;
        }

        if (isset($data[$this->prop])) {
            $value = $data[$this->prop];
        } else {
            $value = null;
        }
        
        if ($value==$this->value) {
            return true;
        } else {
            return false;
        }
    }

    public function getErrorMessage()
    {
        $msg="The value of {$this->prop1->name} must be the same as the value of {$this->prop2->name}, {$this->errMsg}";
        //$msg .= " ErrCode:{$this->errCode}";
        return $msg;
    }

    // retrun an array of the two propIds
    public function getPropIds()
    {
        return $this->propNames;
    }
}
?>
