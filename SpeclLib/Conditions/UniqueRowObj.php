<?php
/**
 * @author Tony
 */
class UniqueRowObj extends Condition {
        
    public $columns;
    public $checkMemory;
    public $checkDB;
    public $uniqueKeys;
    public $ignoreNulls;
    public $propNames; 
    
    public function __construct($params)
    {               
        if (isset($params["columns"])) {
            $columns = $params["columns"];
        } else {
            $columns = null;
        }
        
        if (isset($params["propNames"])) {
            $propNames = $params["propNames"];
        } else {
            $propNames = null;
        }        
        
        if (isset($params["checkMemory"])) {
            $checkMemory = $params["checkMemory"];
        } else {
            $checkMemory = true;
        }
        
        if (isset($params["checkDB"])) {
            $checkDB = $params["checkDB"];
        } else {
            $checkDB = true;
        }

        if (isset($params["ignoreNulls"])) {
            $ignoreNulls = $params["ignoreNulls"];
        } else {
            $ignoreNulls = true;
        }
        
        $this->columns = $columns;
        $this->checkMemory = $checkMemory;
        $this->checkDB = $checkDB;
        $this->uniqueKeys = array();
        $this->setErrorMsg("dup value");
        $this->propNames = $propNames;                
        $this->ignoreNulls = $ignoreNulls;
        
        parent::__construct($params);
    }
    
     
    public function validateMemory($objDef, $objData)
    {
        $key = "";
        $sep = "";
        foreach($this->columns as $column) {                                    
            if (!isset($objData[$column]) && ($this->ignoreNulls==true)) {
                return true;
            }
            
            if (isnull($objData[$column])) {
                if ($this->ignoreNulls==false) {
                    $key .= '|';
                } else {
                    return true;
                }                                
            } else {
                $key .= $sep . $objData[$column];                
                if ($sep=="") $sep = "|";                            
            }
        }
        
        if (array_key_exists($key, $this->uniqueKeys)) {  
            $this->setErrorMsg("Value {$key} already exists.");            
            return false;
        } else {
            $this->uniqueKeys[$key]=true;
        }
        
        return true;
    }
    
    // TODO: Not Implemented
    public function validateDB($objDef, $objData)
    {        
        return true;
    }

    public function getPropIds()
    {
        return $this->propNames;
    }
    
    public function validate($objDef, $objData)
    {
        $valid = true;
                
        if ($this->checkMemory) {
            $validMemory = $this->validateMemory($objDef, $objData);
        } else {
            $validMemory = True;
        }  
        
        if ($this->checkDB) {
            $validDB = $this->validateMemory($objDef, $objData);
        } else {
            $validDB = True;
        }    
        
        $valid = $validMemory && $validDB;
        
        $this->valid=$valid;
        $this->validated=true;   
        return $valid;
    }    
}
?>
