<?php
class UserAccountUsernameExistsCondition extends Condition
{
    private $exists;
    
    public function __construct($exists)
    {
        parent::__construct();
        if (is_bool($exists)) {
            $this->exists=$exists;
        } else {
            $this->exists = true;
        }
    }

   public function validate($value)
   {
       global $app;
       $isValid=null;
       $found = $app->userManager->doesUsernameExist($value);       
       if ($this->exists == true) {
            if ($found==true) {
                $isValid=true;
            } else {
                $this->setErrorMsg("Username does not exist.");                
                $isValid=false;
            }
       } else {
            if ($found==true) {
                $this->setErrorMsg("Username already exists.");                
                $isValid=false;
            } else {
                $isValid=true;
            }
       }
       
       $this->valid=$isValid;
       $this->validated=true;   
       return $isValid;
   }
}
?>
