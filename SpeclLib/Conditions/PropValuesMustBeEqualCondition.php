<?php
class PropValuesMustBeEqualCondition extends Condition
{
    public $propNames;  // not Lowercased!
    public $prop1;
    public $prop2;
    public $errMsg;

    public function __construct($prop1,$prop2)
    {
        parent::__construct();

        $this->prop1=$prop1;
        $this->prop2=$prop2;

        $this->propIds = array();

        if (!is_null($this->prop1)) {
            $this->propNames[]=$this->prop1->name;
        }

        if (!is_null($this->prop2)) {
            $this->propNames[]=$this->prop2->name;
        }
        $this->errCode = get_class($this) ." ERR00303";
    }

    public function validate($object, $objData)
    {
        $this->errMsg = "";

        if (is_null($object)) {
            $this->errMsg .= "object was null ";
            return false;
        }

        if (is_null($this->prop1)) {
            $this->errMsg .= "prop1 was null ";
            return false;
        }

        if (is_null($this->prop2)) {
            $this->errMsg .= "prop2 was null ";
            return false;
        }

        $value1=null;
        $value2=null;

        if (isset($objData[$this->prop1->id])) {
            $value1 = $objData[$this->prop1->id];
        } else {
            $this->errMsg = "object missing the property 1 ";
        }
        
        if (isset($objData[$this->prop2->id])) {
            $value2 = $objData[$this->prop2->id];
        } else {
            $this->errMsg .= "object missing the property 2 ";
        }

        if ($value1==$value2) {
            return true;
        } else {
            return false;
        }
    }

    public function getErrorMessage()
    {
        $prop1Name=$this->prop1->getAttribute("caption",$this->prop1->name);
        $prop2Name=$this->prop2->getAttribute("caption",$this->prop2->name);
        $msg="The value of {$prop1Name} must be the same as the value of {$prop2Name}";
        return $msg;
    }

    // retrun an array of the two propIds
    public function getPropIds()
    {
        return $this->propNames;
    }
}
?>
