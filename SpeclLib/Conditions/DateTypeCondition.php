<?php
class DateTypeCondition extends Condition
{
    public $valueValidated;
    
    public function __construct()
    {
        parent::__construct();
    }

    //# date
    //dt = ANDConditionGroup('date')
    //dateRegex=RegexCondition('date')
    //# date formats...and there a lot of them...!!!
    //dateRegex.addRegex("^(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)-(0?[1-9]|[12][0-9]|3[01])-\d\d$",caseSensitive=False, pattern="MMM-dd-yy case insensitive")
    //dateRegex.addRegex("^(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)-(0?[1-9]|[12][0-9]|3[01])-(19|20)\d\d$",caseSensitive=False, pattern="MMM-dd-yyyy case insensitive")
    //dateRegex.addRegex("^(0?[1-9]|[12][0-9]|3[01])-(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)-\d\d$",caseSensitive=False, pattern="dd-MMM-yy case insensitive")
    //dateRegex.addRegex("^(0?[1-9]|[12][0-9]|3[01])-(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)-(19|20)\d\d$",caseSensitive=False, pattern="dd-MMM-yyyy case insensitive")
    //dateRegex.addRegex("(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])", pattern="YYYY MM DD or YYYY/MM/DD or YYYY-MM-DD")
    //dateRegex.addRegex("(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d", pattern="MM DD YYYY or MM/DD/YYYY or MM-DD-YYYY")
    //dateRegex.addRegex("(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d", pattern="DD MM YYYY or DD/MM/YYYY or DD-MM-YYYY")
    //dt.addCondition(dateRegex)
    //self.addConditionGroup(dt)
       
    public function validate($val, $objData=null)
    {       
        $this->valueValidated = $val;        
        
        if ($val=="now()") {
            return true;
        }
        
        if (strtotime($val)==false) {
            return false;
        } else {
            return true;
        }
    }

    public function getErrorString($sep=Null)
    {
        $this->setErrorMsg("[".$this->valueValidated."] is not a valid date value");                 
        return parent::getErrorString($sep);               
    }
}
?>