<?php
class IntRangeCondition extends Condition
{
    public function __construct($min,$max,$inclusive=true)
    {
        parent::__construct();
        $this->min=$min;
        $this->max=$max;
        $this->inclusive=$inclusive;
    }

    public function validate($value)
    {
        if($this->inclusive)
        {
            if(($value >= $this->min) && ($value<=$this->max)) {
                return true;
            } else {
                return false;
            }
        }
        else
        {
            if(($value > $this->min) && ($value < $this->max)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getErrorString($sep=Null)
    {
        $msg="Value must be between $this->min and $this->max ";
        if($this->inclusive) {
            $msg.="(inclusive)";
        } else {
            $msg.="(not inclusive)";
        }
        $this->setErrorMsg($msg);
        return parent::getErrorString($sep);
    }
}
?>
