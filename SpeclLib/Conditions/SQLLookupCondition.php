<?php

class SQLLookupCondition extends Condition {
    public function __construct($params){
        parent::__construct($params);
        if(isset($params["connectionName"])){
            $this->connectionName=$params["connectionName"];
        }else{
            $this->connectionName=NULL;
        }
        if(isset($params["SQL"])){
            $this->SQL=$params["SQL"];
        } else {
            $this->SQL=NULL;
        }
        
       if (isset($params["statement"])) {
            $this->statement = $params["statement"];
        }
        
        
        if(isset($params["exists"])){
            $this->exists=$params["exists"];            
        }else{
            $this->exists=true;
        }
    }
    
    public function validate($value){
        global $app;
        $sqlBuilder = new QueryBuilder();
        
        if (isnull($this->SQL)) {                        
            $this->SQL = $this->statement;
          
        } 
        $sqlBuilder->setStatement($this->SQL);
        $sqlBuilder->createParam("value","function",$value);             
        $sql = $sqlBuilder->bindParams();
        $dbConn=$app->getConnection($this->connectionName);
        $results=$dbConn->fetchData($sql);
        
        if(count($results)>0){
            $found=true;
        }else{
            $found=false;
        }
        
        if($this->exists){
            return $found;
        } else{
            return !$found;
        }
        
    }
    
    public function getErrorString($sep=Null)
    {
        $msg="SQLLookupCondition Failed ";
        $this->setErrorMsg($msg);
        return parent::getErrorString($sep);
    }
}

?>