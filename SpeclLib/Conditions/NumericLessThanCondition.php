<?php

class NumericLessThanCondition extends Condition
{
   public function __construct($max,$inclusive=true)
    {
        parent::__construct();
        $this->max=$max;
        $this->inclusive=$inclusive;
    }

    public function validate($value)
    {
        // TODO: check if value is numeric
        if($this->inclusive)
        {
            if ($value <= $this->max)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
           if ($value < $this->max)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public function getErrorString($sep=Null)
    {
        if($this->inclusive)
        {
            $msg="Value must be less than or equal to {$this->max} ";
        }
        else
        {
            $msg="Value must be less than {$this->max} ";
        }
        $this->setErrorMsg($msg);
        return parent::getErrorString($sep);
    }
}
?>