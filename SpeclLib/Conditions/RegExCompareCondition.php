<?php
// RegExCompare
class RegExCompareCondition extends Condition {

    public $pattern;
    public $friendlyPattern;
    public $equal;
    public $matches;
    
    public function __construct($pattern, $equal)
    {
        parent::__construct();

        $this->pattern = $pattern;
        $this->friendlyPattern = $pattern;
        $this->equal = $equal;
    }

    public function validate($value)
    {                
        $match = preg_match($this->pattern, $value, $this->matches);
        $isValid=null;
        if ($this->equal==true) {
           $isValid=$match;
        } else {
           $isValid=!$match;
        }        
        $this->valid=$isValid;
        $this->validated=true;        
        return $isValid;
    }

    public function getErrorString($sep=Null) {        
        if ($this->equal==true) {
            $subStr="must match";
        } else {
            $subStr="must not match";
        }
        $msg="Value {$subStr} the pattern of {$this->friendlyPattern}.";
        $this->setErrorMsg($msg);                
        return parent::getErrorString($sep);
    }
}
?>
