<?php
/**
 * This condition uses PHP's built in boolean validtion filter condition and considers 
 * a value to be a boolean for the following values:
 *      1,true, on, yes are considered true
 *      0,false,off,no,"" are considered false
 */
class BooleanTypeCondition extends Condition
{
    public function __construct()
    {
        parent::__construct();        
        $this->setErrorMsg("Value must be boolean type.");
    }
    
    public function validate($value, $objData=null)
    {
        if (is_bool($value)) {
            return true;
        }
        
        $value = (string)"{$value}";
        $this->valueValidated = $value;
                
        $filterVal = filter_var($value,FILTER_VALIDATE_BOOLEAN,array("flags" => FILTER_NULL_ON_FAILURE));
                
        //if(!is_null(filter_var($value,FILTER_VALIDATE_BOOLEAN,FILTER_NULL_ON_FAILURE)))
        if ($filterVal === null) {
            return false;
        } else {
            return true;
        }
    }
    
    public function getErrorString($sep=Null)
    {
        $this->setErrorMsg("[".$this->valueValidated."] is not a valid boolean value");                 
        return parent::getErrorString($sep);
    }
    
}
?>