<?php
class LookUpCondition extends Condition
{    
    public function __construct($lookupArray,$searchByKey=false)
    {
        parent::__construct();
        $this->errorMessage=false;
        $this->lookupArray=$lookupArray;
        $this->searchByKey=$searchByKey;
    }

    public function validate($value, $objData=NULL)
    {
        if($this->searchByKey)
        {
            if(array_key_exists($value,$this->lookupArray))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if(in_array($value,$this->lookupArray))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public function getErrorString($sep=NULL)
    {
        if($this->searchByKey)
        {
            $values=array_keys($this->lookupArray);
        }
        else
        {
            $values=array_values($this->lookupArray);
        }
        $valueString=implode(",",$values);
        $msg = "Value must be one of the following values: [$valueString]";
        $this->setErrorMsg($msg);
        return parent::getErrorString();        
    }
}
?>
