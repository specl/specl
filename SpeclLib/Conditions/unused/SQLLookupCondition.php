<?php
/**
 * Description of SQLLookup
 *
 * @author Tony
 */
class SQLLookupCondition extends Condition {

    private $exists;
    private $errMsg;
    private $statement;
    private $connectionName;    

    public function __construct($params)
    {
        if (isset($params["connectionName"])) {
            $connectionName = $params["connectionName"];
        }

        if (isset($params["statement"])) {
            $statement = $params["statement"];
        }
        
        if (isset($params["exists"])) {
            $exists = $params["exists"];
        }
        
        if (isset($params["errMsg"])) {
            $errMsg = $params["errMsg"];
        } else {
            $errMsg = "SQLLookupCondition invalid";
        }
                
        parent::__construct();

        $this->connectionName = $connectionName;
        $this->statement = $statement;
        $this->errMsg = $errMsg;        
        
        if (is_bool($exists)) {
            $this->exists=$exists;
        } else {
            $this->exists = true;
        }
    }

   public function validate($value)
   {
       global $app;

        
       #$app->sqlBuilder->setStatement($this->statement);
        //$this->sqlBuilder->createParam("valie", "string", $username);
        //$sql = $app->sqlBuilder->bindParams();
        $results = $app->dbConnSys->select($sql);

        if ($results!=0)
        {
            $row = $app->dbConnSys->getRowAssoc($results);

            $success = $row['Success'];

            if ($success==0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }


       $found = false;

       $this->errMsg = "";

       if ($this->exists == true) {
            if ($found==true) {
                return true;
            } else {
                $this->errMsg  = "Value does not exist.";
                //$this->errMsg .= get_class($this) ." ERR00315";
                return false;
            }
       } else {
            if ($found==true) {
                $this->errMsg  = "Value already exists.";
                //$this->errMsg .= get_class($this) ." ERR00316";
                return false;
            } else {
                return true;
            }
       }

   }

   public function getErrorMessage()
   {
       return $this->errMsg;
   }

}
?>
