<?php
/**
 * Description of SQLLookupUsingObjDef
 *
 * @author Tony
 */
class SQLLookupUsingObjDefCondition extends Condition {

    public $objDef;
    public $mustExist;
    public $errMsg;
    public $propNames;  // not Lowercased!

    public function __construct($params) {
        if (isset($params["mustExist"])) {
            $mustExist = $params["mustExist"];
        } else {
            $mustExist = false;
        }

        if (isset($params["errMsg"])) {
            $this->setErrorMsg($params["errMsg"]);            
        }
        parent::__construct(true);

        $this->mustExist = $mustExist;
        $this->errMsg = $errMsg;
    }

    // retrun an array of the two propIds
    public function getPropIds()
    {
        return $this->propNames;
    }


    public function validate($objDef, $objData)
    {
        global $app;

        foreach($objDef->primaryKey as $key) {
            $pkPropDef = $objDef->getProperty($key);
            $this->propNames[]=$pkPropDef->name;
        }

        $qs = $app->sqlBuilder->genGetByPK($objDef, $objData);

        //echo "{$qs->sql}<br/>";

        $results = $app->dbConnSys->select($qs->sql);

        $rowExists=false;

        if ($results!=0)
        {            
            $row = $app->dbConnSys->getRowAssoc($results);

            if ($row) {
                $rowExists= true;
            } else {
                $rowExists= false;
            }
        } else {
            $rowExists= false;
        }

        if ($this->mustExist==false) {
            if ($rowExists==false) {
                return true;
            } else {
                return false;
            }
        } else {
            if ($rowExists==false) {
                return false;
            } else {
                return true;
            }

        }
    }
}
?>
