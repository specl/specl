<?php

/**
 * Description of FKLookup
 *
 * @author Tony
 */
class FKLookupConditionObj extends Condition {
    
    public $objDef;
    public $fkTable;
    public $fkColumn;       
    public $exists;
    public $connectionName;
    public $allowNulls; // if true, Null valies will be allowed, menaing is 
                        // the value passed in is a NULL and exists is true,
                        // the return will be valid.
    public $whereClause;
    public $propNames;      
    public $multiColumn;  // allows for a multicolumn unique index or similiar
    
    public function __construct($params)
    {
        if (isset($params["connectionName"])) {
            $connectionName = $params["connectionName"];
        }

        if (isset($params["fkTable"])) {
            $fkTable = $params["fkTable"];
        }
        
        if (isset($params["fkColumn"])) {
            $fkColumn = $params["fkColumn"];
        }
       
        if (isset($params["exists"])) {
            $exists = $params["exists"];
        }
        
        if (isset($params["errorMsg"])) {
            $this->errorMsg = $params["errorMsg"];
        } else {
            $this->errorMsg = Null;
        }

        if (isset($params["allowNulls"])) {
            $allowNulls = $params["allowNulls"];
        } else {
            $allowNulls = true;
        }        

        if (isset($params["whereClause"])) {
            $this->whereClause = $params["whereClause"];
        } else {
            $this->whereClause = null;
        }

        if (isset($params["multiColumn"])) {           
            $var = $params["multiColumn"];            
            if (is_bool($var)) {
                $this->multiColumn = $var;
            } else {
                 $this->multiColumn = false;
            }                        
        } else {
            $this->multiColumn = false;
        }

        if (isset($params["objDef"])) {
            $this->objDef = $params["objDef"];
        } else {
            $this->objDef = null;
        }
        
        if(isset($params["errorDisplayColumns"])){
            $this->errorDisplayColumns=$params["errorDisplayColumns"];
        }
        else{
            $this->errorDisplayColumns=null;
        }
        
        parent::__construct($params);

        $this->connectionName = $connectionName;
        $this->fkTable = $fkTable;
        $this->fkColumn = $fkColumn;
        
        if (is_bool($exists)) {
            $this->exists=$exists;
        } else {
            $this->exists = true;
        }
        
        $this->allowNulls = $allowNulls;      
        $this->propNames=$this->fkColumn;
    }
    
    // Used by validation to get the field names where the errors should be displayed.
    public function getPropIds()
    {
        if(!empty($this->errorDisplayColumns)){
            return $this->errorDisplayColumns;
        }
        else{
            return $this->propNames;
        }
    }
    
    public function objValidate($objDef, $objData)
    {
        global $app;
                       
        $found = FkExistsMultiColumn($objDef, $this->connectionName, $this->fkTable, $this->fkColumn, $objData);
                    
        if ($this->exists == true) {
            if ($found==true) {
                return true;
            } else {                
                foreach($this->fkColumn as $column) {
                    //TODO ? $this->errMsg =  "Value {$value} does not exist in the {$this->fkTable} table.";                
                }
                return false;
            }
        } else {                        
            if ($found==true) {

                $values = "";
                $sep="";
                if(!empty($this->errorDisplayColumns)){
                    $errorColumns=$this->errorDisplayColumns;
                }
                else{
                    $errorColumns=$this->fkColumn;
                }
                foreach($errorColumns as $column) {
                    $value = $objData[$column];
                    $values .=  $sep.$value;                 
                    if ($sep=="") $sep=",";
                }           
                
                $this->valueValidated = $values;
                
                if(isset($this->errorMsg)){
                    $this->setErrorMsg($this->errorMsg);
                }
                else{
                    $this->setErrorMsg("[".$this->valueValidated."] already exist in the table {$this->fkTable}");                                                  
                }

                return false;
            } else {
                return true;
            }
        }        
        
    }
    
    public function propValidate($value)
    {
        $this->valueValidated = value;
        
        if (is_null($value)) {
            if ($this->allowNulls == true) {
                return true;   
            } else {
                $this->setErrorMsg("[NULL] value not allowed");   
            }
        }                
             
        if ($this->whereClause) {
            $found = FkExistsWhere($this->connectionName, $this->fkTable, $this->fkColumn, $value, $this->whereClause);
        }  else {
            $found = FkExists($this->connectionName, $this->fkTable, $this->fkColumn, $value);
        }         
    
    
        if ($this->exists == true) {
            if ($found==true) {
                return true;
            } else {
                if (empty($this->errorMsgs)) {
                    $this->setErrorMsg("[".$this->valueValidated."]  does not exist in the table {$this->fkTable}");
                }
                return false;
            }
        } else {
            if ($found==true) {
                if (empty($this->errorMsgs)) {
                    $this->setErrorMsg("[".$this->valueValidated."] already exists in the table {$this->fkTable}");                                                  
                }
                return false;
            } else {
                return true;
            }
        }        
    }
    
    public function validate($objDef, $objData=Null)
    {
        if (is_null($objData)) {
            if ($this->allowNulls == true) {
                return true;   
            } else {
                $this->setErrorMsg("[NULL] value not allowed");   
            }
        }
        
        if (is_object($objData) || is_array($objData)) {
            return $this->objValidate($objDef, $objData);
        } else {
            return $this->propValidate($objData);
        }
                        
    }
}
?>
