<?php
class StringMaxLengthCondition extends Condition {

    public $length;
    
    public function __construct($length)
    {   
        parent::__construct();        
        $this->length=$length;
    }

    public function validate($value, $objData=Null)
    {
        $isValid=null;
        
        $len = strlen($value);
        if ($len<=$this->length)
        {
            $isValid=true;
        }
        else
        {
            $this->errorCode = 1;
            $isValid=false;
        }
        
        $this->valid=$isValid;
        $this->validated=true;        
        return $isValid;
    }

    public function getErrorString($sep=Null)
    {        
        $msg="String length can not be greater than {$this->length} characters.";
        $this->setErrorMsg($msg);
        return parent::getErrorString($sep);
    }
}
?>
