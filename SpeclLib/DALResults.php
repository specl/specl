<?php
/**
 * Description of DALResults
 *
 * @author Tony
 */
class DALResults {
    
    public $success; // true or false
    public $operation; // select, update, insert, delete, stored procedure, create table, drop table
    public $errorType;
    public $errormessage;
    public $errorCode;    
    public $autoIncrementKey; // null unless opertaion = "insert", success is true and the table has auto increment primary key    
    
    public function __construct($success, $operation, $errorType, $errorMessage, $errorCode, $autoIncrementKey) {
        $this->success = $success;
        $this->operation = $operation;
        $this->errorType = $errorType;
        $this->errorMessage = $errorMessage;
        $this->errorCode = $errorCode;
        $this->autoIncrementKey = $autoIncrementKey;        
    }
    
    public function getMessage() {
        return  $this->operation.';'.$this->errorType.';'.$this->errorMessage;
    }
}
?>
