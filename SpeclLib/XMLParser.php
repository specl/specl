<?php
class XMLParser {

    private $data;
    private $pathStack;
    private $specStack;
    private $dataStack;
    private $dataSaver;
    private $attributes;
    private $errors;
    private $reader;
    private $spec;
    private $filename;
    private $xmlSpecMap;
    private $_indent;
    
    public function __construct() {        
        $this->reader = new XMLReader(); // The XMLReader extension is an XML Pull parser.
        $this->errors=array();
        $this->xmlSpecMap = Null;
        $this->reset();
    }

    public function parseFile($spec, $filename) {        
        $this->filename = $filename;        
        $this->reset();        
        $this->open($filename);
        $this->_indent="";
        $this->parse($spec);
        $this->close();
    }

    public function reset() {
        $this->fileOpened = false;
        $this->data = array();
        $this->pathStack = new PathStack(App::ZONESSEP);
        $this->specStack = new Stack();
        $this->dataStack = new Stack();
        $this->dataSaver = Array();
        
        $this->errors = array();
    }

    public function open($filename) {

        if (file_exists($filename)) {
            $this->reader->open($filename);
            $this->fileOpened = true;
        } else {
            throw new Exception("XML Parser Failed: The file: {$filename} does not exist");
        }
    }

    public function close() {
        if ($this->fileOpened == true) {
            $this->fileOpened = false;
            $this->reader->close();
        }
    }

    public function addCollection($tag,$parentSpec, $topSpec, $specInst) {
        //$typeName = str_replace(":","_",$tag);                        
        
        //$propDef = $parentSpec->getProperty($tag);
        $collectFromParentXMLNode = $parentSpec->getAttribute("collectFromParentXMLNode", false);        
        
        if ($parentSpec->isChildType($topSpec->name)) {

            if ($parentSpec->isAssociative) {
                
                //$propKey = $topSpec->getPrimaryKey();                
                $propKey = $parentSpec->getKeyId();                                

                if (isset($specInst->data[$propKey])) {
                    $keyValue = $specInst->data[$propKey];
                } else {
                    $keyPropDef = $topSpec->getProperty($propKey);                                
                    $propKey = $keyPropDef->getAttribute("xmlTag", Null);                
                    
                    if (!is_null($propKey)) {
                        $keyValue = $specInst->data[$propKey];
                    }
                }        
                                
//                $keyValue="";
//                $sep="";
//                foreach($propKey as $topSpecKey) {
//                    
//                    if (isset($specInst->data[$topSpecKey])) {
//                        $keyValue .= $sep.(string)$specInst->data[$topSpecKey];
//                        if ($sep=="") $sep=App::ZONESSEP;
//                    }
//                }
                                
                $parentInst = $this->dataStack->oneBackFromTop();
                $keyValue = str_replace(":","_",$keyValue); //XXX                               
                $parentInst->data['data'][$keyValue] = $specInst->data;
                
                 $this->specStack->pop();
                 $this->dataStack->pop();
                  
                if ($collectFromParentXMLNode) {
                    
                    $realParentInst = $this->dataStack->oneBackFromTop();
                    $realParentInst->data[$tag] = $parentInst->data;
                    
                    $this->specStack->pop();
                    $this->pathStack->pop();
                    $this->dataStack->pop();                
                }
                
                //$ownerInst = $this->dataStack->top();
                //$ownerInst['data'][$tag] = 
                //$ownerInst->data[$tag] = $specInst->data;                
                            
            } else {
                // non associative array
                $parentInst = $this->dataStack->oneBackFromTop();
                //$parentInst->data['data'][$tag] = $specInst->data;
                $parentInst->data['data'][] = $specInst->data;
                
                $this->specStack->pop();
                $this->dataStack->pop();                
            }

        } else {
            $log->info("END_ELEMENT isChildType","file:{$this->filename} {$tag} is NOT a child of CollectionDef {$parentSpec->id}");
            $this->specStack->pop();
            $this->pathStack->pop();
        }        
    }
    
    public function endElement($tag) {
        global $log;
                
        $tag = strtolower($tag);
        
        $topSpec = $this->specStack->top();
        $specInst = $this->dataStack->top();
        $parentInst = $this->dataStack->oneBackFromTop();

        if (is_a($topSpec, "PropDef")) {
            $this->specStack->pop();
             $this->pathStack->pop();
             //continue;
             return;
        }

        $parentSpec = $this->specStack->oneBackFromTop();
        $path = $this->pathStack->getPath();
        $this->pathStack->pop();
        $topTag = $this->pathStack->top();


        if (is_a($parentSpec, "ObjDef")) {
            // check that the tag is defined in the Object
            
            if (!is_null($this->xmlSpecMap)) {
                $propDefName = $this->xmlSpecMap->lookupPath("/".$path);
                
                if (!is_null($propDefName)) {
                    $tag = $propDefName;
                } else {
                    // something went wrong
                    
                }
            }             
            if ($parentSpec->isProperty($tag)) {
                $specInst = $this->dataStack->top();
                $parentInst = $this->dataStack->oneBackFromTop();
                
                $propDef = $parentSpec->getProperty($tag);
                $collectFromParentXMLNode = $propDef->getAttribute("collectFromParentXMLNode", false);
                
                if ($collectFromParentXMLNode===true) {                    
                    $collectionSpec = $this->spec->findDef($propDef->objectTypeOrig);                     
                    $this->addCollection($tag,$collectionSpec,$specInst);   
                    
                    // also add to the 
                    
                } else {
                    $tag = str_replace(":","_",$tag); //XXX                
                    $parentInst->data[$tag] = $specInst->data;
                    $this->specStack->pop();
                    $this->dataStack->pop();
                }
            } else {
                //$log->info("END_ELEMENT isProperty","file:{$this->filename} {$tag} is NOT a property of ObjDef {$parentSpec->id}");
                $this->specStack->pop();
                $this->pathStack->pop();
            }
        } else if (is_a($parentSpec, "CollectionDef")) {                        
            $this->addCollection($tag,$parentSpec,$topSpec,$specInst);
            
            
            
        } else {
             $specInst = $this->dataStack->top();

             if ($specInst) {    
                 
                $path = str_replace(":", "_", $path);
                 
                   
                if (!is_null($this->xmlSpecMap)) {
                    $name = $this->xmlSpecMap->lookupPath("/".$path);

                    if (!is_null($name)) {
                        $path = strtolower($name);
                    }
                } 
            
                $this->data[$path] = $specInst->data;
             }

             $this->specStack->pop();
             $this->pathStack->pop();
        }
    }

    public function startElement($tag, $isEmptyElement) {
        global $log;

        $origTag = $tag;
        
        $tag = strtolower($origTag);

        $path = $this->pathStack->add($tag);

        // get the spec at the top of the stack
        $topSpec = $this->specStack->top();

        $def=null;

        $collectFromParentXMLNode = false;
        
        if (is_a($topSpec, "ObjDef")) {
            
            if (!is_null($this->xmlSpecMap)) {
                $propDefName = $this->xmlSpecMap->lookupPath("/".$path);
                
                if (!is_null($propDefName)) {
                    $tag = $propDefName;
                }
            } 
            
            // check that the tag is defined in the Object
            if ($topSpec->isProperty($tag)) {

                $propDef = $topSpec->getProperty($tag);

                if ($propDef->crossChecked == False) {
                    $log->info("crossCheck", "propDef {$propDef->name} not crossChecked");
                }
                
                $collectFromParentXMLNode = $propDef->getAttribute("collectFromParentXMLNode", false);
                
                if ($propDef->isScalar()) {
                    $def = $propDef;
                } else {
                    $def = $this->spec->findDef($propDef->objectTypeOrig);
                }
                
            } else {
                //$log->info("Error isProperty", "file:{$this->filename} {$tag} is NOT a property of ObjDef {$topSpec->id}");
                ///echo "file:{$this->filename} '{$tag}' is NOT a property of ObjDef {$topSpec->id}\n";
                
                $this->errors[]=new ValidationError($topSpec->name, $tag,"InvalidProperty","Invalid Property Found",$topSpec->id,10,3, $tag);
                $def = new PropDef(array('id'=>'error','propType'=>'error','objectType'=>'error'));
            }
        } else if (is_a($topSpec, "CollectionDef")) {
            if ($tag == "Item") {
                $def = new PropDef(array('id'=>'Item','propType'=>'Item','objectType'=>'Item'));
            } else {
                if ($topSpec->isChildType($tag)) {
                   $def = $topSpec->getChildType($origTag);
                                                            
                    if (is_null($def)) {
                        $msg = "file:{$this->filename} {$origTag} is NOT a found";
                        $log->info("Error getChildType",$msg);
                        echo $msg;
                    }                    
                    
                    if ($def->isCollection()==true) {

                    } else {
                        if ($def->isScalar()) {
                            $def = new PropDef(array('id'=>'Item','propType'=>'Item','objectType'=>'Item'));
                        }
                    }
                } else {
                    $log->info("Error isChildType", "file:{$this->filename} {$tag} is NOT a childType of ObjDef {$topSpec->id}");
                    $this->errors[]=new ValidationError($topSpec->name,$tag,"InValidChildType","Invalid Child Type Found in Collection",$topSpec->id,10,4);
                    $def = new PropDef(array('id'=>'error','propType'=>'error','objectType'=>'error'));
                }
            }
        } else {
            // this is the first tag, look it up...
            
            if (!is_null($this->xmlSpecMap)) {
                $objDefName = $this->xmlSpecMap->lookupPath("/".$origTag);
                $def = $this->spec->findDef($objDefName);
            } 
            
            if (is_null($def)) {
                $def = $this->spec->findDef($origTag);
                if (is_null($def)) {
                    $log->info("findDef failed", "{$tag} not found");
                }
            }
        }

        if (is_null($def)==False) {
            $this->specStack->push($def);

            if (is_a($def, "PropDef")) {
                // push just the spec...
                $this->saveAttributes();
            } else {
                // its a CollectionDef or a ObjDef               
                //$def->getAttribute("xmlTag", "Lane");                
        
                if ($collectFromParentXMLNode) {
        
                    $this->pathStack->pop();  // get rid of the current tag, its wrong.
                    
                    $path = $this->pathStack->add($propDef->name);                    
                    
                    if (array_key_exists($path, $this->dataSaver)) {
                        $collectionSpecInst = $this->dataSaver[$path];
                        $this->dataStack->push($collectionSpecInst);
                        
                        $specInst = $this->dataStack->top();
                        $parentInst = $this->dataStack->oneBackFromTop();                        
                                                                
                        $tag = str_replace(":","_",$tag); //XXX                
                        $parentInst->data[$tag] = $collectionSpecInst->data;
                                
                    } else {                    
                        $collectionSpecInst = new SpecInstance($def, $path);
                        $this->dataStack->push($collectionSpecInst);
                        $this->dataSaver[$path] = $collectionSpecInst;
                    }
                    
                    $itemSpecKey = $propDef->getAttribute("referenceTable");
                    $itemSpec = $this->spec->findDef($itemSpecKey);
                          
                    $this->specStack->push($itemSpec);
                    
                    $path = $this->pathStack->add($itemSpec->name);                    
                    $specInst = new SpecInstance($itemSpec, $path);
                    $this->dataStack->push($specInst);                                        
                    
                    $attribute = $this->reader->moveToFirstAttribute();
                    while (true === $attribute) {
                        $attrKey = strtolower($this->reader->localName);
                        $attrValue = $this->reader->value;
                        $attribute = $this->reader->moveToNextAttribute();

                        if ($def->isCollection()) {
                            $attrKey = str_replace(":","_",$attrKey); // XXX
                            $specInst->data[$attrKey] = $attrValue;
                        } else {
                            if ($def->isProperty($attrKey)) {                            
                                $attrKey = str_replace(":","_",$attrKey); // XXX
                                $specInst->data[$attrKey] = $attrValue;
                                continue;
                            } else {
                                $log->info("XMLReader::ELEMENT isProperty","file:{$this->filename} {$attrKey} is NOT a property of ObjDef {$def->id}");
                            }
                        }
                    }                    
                } else {
                    $specInst = new SpecInstance($def, $path);
                    $this->dataStack->push($specInst);

                    $attribute = $this->reader->moveToFirstAttribute();
                    while (true === $attribute) {
                        $attrKey = strtolower($this->reader->localName);
                        $attrValue = $this->reader->value;
                        $attribute = $this->reader->moveToNextAttribute();

                        if ($def->isCollection()) {
                            $attrKey = str_replace(":","_",$attrKey); // XXX
                            $specInst->data[$attrKey] = $attrValue;
                        } else {
                            if ($def->isProperty($attrKey)) {                            
                                $attrKey = str_replace(":","_",$attrKey); // XXX
                                $specInst->data[$attrKey] = $attrValue;
                                continue;
                            } else {
                                $log->info("XMLReader::ELEMENT isProperty","file:{$this->filename} {$attrKey} is NOT a property of ObjDef {$def->id}");
                            }
                        }
                    }
                }
            }
        } else {
             $log->info("XMLParser def missing ".__LINE__, $tag);
        }

        if ($isEmptyElement) {
            $this->endElement($tag);
        }
    }

    public function indent() {
        for($i=0;$i<$this->_indent;$i++) {
            echo "\t";
        }        
    }
    
    public function parse($spec) {
        global $log;
        $this->spec= $spec;
        
        if ($this->fileOpened == false) {
            return NULL;
        }

        if($spec->isCrossChecked()==false)
        {
            throw new Exception("UnChecked Spec: A spec must be cross checked before parsing.");
        }

        while ($this->reader->read()) {

            switch ($this->reader->nodeType) {
                case XMLReader::END_ELEMENT:
                    $domNode = $this->reader->expand();
                    $tag = $this->reader->name;
                    //echo $this->indent()."{$tag}\n";
                    $this->endElement($tag);
                    $this->_indent-=1;
                    break;

                case XMLReader::ELEMENT:
                    $domNode = $this->reader->expand();
                    $tag = $this->reader->name;
                    $isEmptyElement = $this->reader->isEmptyElement;
                    //echo $this->indent()."{$tag} ".$isEmptyElement."\n";
                    $this->_indent+=1;
                    $this->startElement($tag, $isEmptyElement);
                    if ($isEmptyElement) {
                        $this->_indent-=1;
                    }
                    break;

                case XMLReader::TEXT:
                case XMLReader::CDATA:
                    $domNode = $this->reader->expand();
                    $tag = $this->reader->name;
                    $tag = strtolower($tag);
                    $value = $this->reader->value;
                    $topSpec = $this->specStack->top();
                    $topTag = $this->pathStack->top();

                    if (is_a($topSpec, "PropDef")) {                        
                        $propDef = $topSpec;
                        $parentSpec = $this->specStack->oneBackFromTop();
                        if (is_a($parentSpec, "ObjDef")) {
                            // check that the tag is defined in the Object
                            if ($parentSpec->isProperty($topTag)) {
                                $specInst = $this->dataStack->top();
                                
                                $topTag = str_replace(":","_",$topTag); // XXX
                                $specInst->data[$topTag] = $value;
                                continue;
                            } else {
                                $log->info("XMLReader::TEXT isProperty","file:{$this->filename} {$topTag} is NOT a property of ObjDef {$topSpec->id}");
                            }
                        } else if (is_a($parentSpec, "CollectionDef")) {

                             if ($parentSpec->isAssociative) {
                                $propKey="Id";
                                if ($this->hasAttribute($propKey)) {
                                    $keyValue = $this->getAttribute($propKey);
                                    $parentInst = $this->dataStack->top();
                                    
                                    $keyValue = str_replace(":","_",$keyValue); // XXX
                                    
                                    $parentInst->data['data'][$keyValue] = $value;
                                } else {
                                    // error! prop key missing!!!!
                                    $log->info("END_ELEMENT", "file:{$this->filename} ERROR propKey {$propKey} missing.");
                                }
                            } else {
                                $parentInst = $this->dataStack->top();
                                $parentInst->data['data'][] = $value;
                                
                            }
                        }
                    }
                    break;
                default:                   
                   break;
            }
        }
    }

    public function getData() {
        return $this->data;
    }
    
    public function getDataFromRoot() {
        return $this->data;            
    }
    
    public function getDataByKey($key)
    {
        //$key = str_replace(":","_",$key);        
        if (array_key_exists(strtolower($key), $this->data)) {
            $data = $this->data[strtolower($key)];
            return $data;
        } else {
            return Null;
        }
    }

    public function saveAttributes() {
        $this->attributes = array();

        $attribute = $this->reader->moveToFirstAttribute();
        while (true === $attribute) {
            $attrKey = $this->reader->localName;
            $attrValue = $this->reader->value;
            $attribute = $this->reader->moveToNextAttribute();
            $this->attributes[$attrKey]=$attrValue;
        }
    }

    public function hasAttribute($key) {
        if (array_key_exists($key, $this->attributes)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAttribute($key) {
        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        } else {
            return null;
        }
    }

    public function getStatus() {
        return true;
    }
    
    public function addSpecMap($xmlSpecMap) {
        $this->xmlSpecMap = $xmlSpecMap;
    }
}
?>
