<?php
require_once(dirname(__FILE__) ."/../UtilLib/GraphStructure.php");
require_once(dirname(__FILE__) ."/../SpeclLib/SpecManager.php");
require_once(dirname(__FILE__) ."/../lib5/ZonesInclude.php");
require_once(dirname(__FILE__) ."/../lib5/AutoLoad.php");
/*
 *  given one or more directories, read the Specs and build a graph based on their associations.
 * 
 */
class BuildGraphFromSpecs {
    public $directories;
    public $outputPath;
    private $specManager;
    private $graph;
    
    public function __construct($directories=array()){
        $this->directories=$directories;
    }
    
    public function addDirectory($dir){
        $this->directories[]=$dir;
    }
    
    /**
     *  Build Graph from all the specs in the specified directories.
     */
    public function build(){
        global $app;
        $this->graph=new GraphStructure();
        foreach($this->directories as $dir){
            $specFiles=glob($dir.'/*Spec.php');
            foreach($specFiles as $specFile){
                $fileSplit=preg_split("/(Base)?Spec.php$/i", basename($specFile));
                $specName=$fileSplit[0];
                if(!isset($this->graph->nodes[$specName])){
                    $objDef=$app->specManager->findDef($specName);
                    // could be a CollectionDef, do not add these
                    if(is_a($objDef, "ObjDef")){
                        $this->addObjDef($objDef);
                    }
                }
            }
        }
        
        return $this->graph;
        
    }
    
    /*
     *  adds objDef to graph
     *  add table name as the node name and add an edge for each association. To each
     *  edge add the column names that are being joined on.
     * 
     *  Each association type must be handled differently, because the required data for the edges
     *  is stored in different attributes depending on the association.
     * 
     *  Add an edge for each constraint in the objDef as well.
     */
    public function addObjDef($objDef){
        global $app;
        if(!isset($this->graph->nodes[$objDef->name])){
                $this->graph->addNode($objDef->name);

            // add associations as edge. Add the association type and the two columns used
            // to link the tables together.
            foreach($objDef->getAssociations() as $association){            
                // Based on the association type, add the associated tables to the graph and
                // and an edge between the current table and the table it references.                
                $associationType=$association->getAttribute('associationType');
                if($associationType==="HasOne"){
                    // add reference table to graph
                    $referenceTable=$association->getAttribute('referenceTable');
                    $refObjDef=$app->specManager->findDef($referenceTable);
                    $this->addObjDef($refObjDef);
                    // create edge between current table and reference table
                    $this->graph->addDirectedEdge($objDef->name,$refObjDef->name,array(
                       "targetColumn"=>$association->getAttribute('referenceColumn'),
                       "sourceColumn"=>$association->getAttribute('column'),
                       "associationType"=>$associationType,
                       "isComposite"=>$association->getAttribute('isComposite',false)
                    ));

                }
                /*
                 * In HasManyThrough relationships the column in the target and its connecting
                 * column in the intersection table are assumed to have to same name.                 
                 */
                else if($associationType==="HasManyThrough" || $associationType==="OneToManyThrough"){
                    // add through table to graph
                    $throughTable=$association->getAttribute('throughTable');
                    $throughObjDef=$app->specManager->findDef($throughTable);
                    $this->addObjDef($throughObjDef);
                    // create edge between current table and through table
                    $this->graph->addDirectedEdge($objDef->name,$throughObjDef->name,array(
                       "targetColumn"=>$association->getAttribute('throughColumnFrom'),
                       "sourceColumn"=>$association->getAttribute('column'),
                       "associationType"=>$associationType
                    ));
                    
                    // add reference table to graph
                    $referenceTable=$association->getAttribute('referenceTable');
                    $refObjDef=$app->specManager->findDef($referenceTable);
                    $this->addObjDef($refObjDef);
                    // create edge between the through table and reference table
                    $this->graph->addDirectedEdge($objDef->name,$refObjDef->name,array(
                       "targetColumn"=>$association->getAttribute('throughColumnTo'),
                       "sourceColumn"=>$association->getAttribute('throughColumnTo'),
                       "associationType"=>$associationType
                    ));
                    
                    // if association is oneToMany it implies an inverse HasOne association.
                    // However, must HasManyThrough relationships are ManyToMany, not OneToMany.
                    if($associationType==="OneToManyThrough"){
                        $this->graph->addDirectedEdge($throughObjDef->name,$objDef->name,array(
                            "targetColumn"=>$association->getAttribute('column'),
                            "sourceColumn"=>$association->getAttribute('throughColumnFrom'),
                            "associationType"=>"HasOne"
                        ));
                        
                        $this->graph->addDirectedEdge($refObjDef->name,$objDef->name,array(
                            "targetColumn"=>$association->getAttribute('throughColumnTo'),
                            "sourceColumn"=>$association->getAttribute('throughColumnTo'),
                            "associationType"=>"HasOne"
                        ));
                    }
                    

                }
                else if($associationType==="HasMany" || $associationType==="OneToMany"){
                    // add reference table to graph
                    $referenceTable=$association->getAttribute('referenceTable');
                    $refObjDef=$app->specManager->findDef($referenceTable);
                    $this->addObjDef($refObjDef);
                    // create edge between current table and reference table
                    $this->graph->addDirectedEdge($objDef->name,$refObjDef->name,array(
                       "targetColumn"=>$association->getAttribute('referenceColumn'),
                       "sourceColumn"=>$association->getAttribute('column'),
                       "associationType"=>$associationType
                    ));
                    
                    // if association is oneToMany it implies an inverse HasOne association.
                    if($associationType==="OneToMany"){
                        $this->graph->addDirectedEdge($refObjDef->name,$objDef->name,array(
                            "targetColumn"=>$association->getAttribute('column'),
                            "sourceColumn"=>$association->getAttribute('referenceColumn'),
                            "associationType"=>"HasOne"
                        ));
                    }
                }
                else{
                    throw new Exception("The associationType '{$associationType}' is not supported");
                }
            }
            
            foreach($objDef->getProperties() as $property){
                if($property->hasConstraint()){
                    $constraint=$property->getConstraint();
                    $tableName=$constraint->tableName;
                    $tableObjDef=$app->specManager->findDef($tableName);
                    $this->addObjDef($tableObjDef);
                    $this->graph->addDirectedEdge($objDef->name,$tableObjDef->name,array(
                        "targetColumn"=>$constraint->idColumnName,
                        "associationType"=>"constraint"
                    ));
                }
            }
        }
    }
    
    
}

?>