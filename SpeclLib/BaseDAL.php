<?php
/**
 * BaseDAL
 *
 * @author Tony
 */

class BaseDAL {
        
    public $connection;
    public $objDefId;  // The DAL is Spec model driven
    public $objDef;
    public $dalCls;
    public $objCls;
    public $stack;
    
    public function __construct($params=array()) {
        global $app;
        
        if(isset($params["objDefId"])){
            $this->objDefId=$params["objDefId"];        
            try {
                $this->objDef = $app->specManager->findDef($this->objDefId, true);  // this could be a call to a Specl Server.
            } catch (Exception $e) {
                $this->objDef = null;
            }                 
        }
        
        if(isset($params["connection"])){
            $this->connection=$params["connection"];
        }
        else{
            $this->connection=null;
        }
                        
        $this->dbConn=$app->getConnection($this->connection);
        $this->sqlBuilder=new SQLBuilder($this->dbConn,$app->specManager);
                
        $this->stack = new Stack();        
    }
    
    public function getOneByPk($keys, $recurse=true) {
        $obj = NewSpecObj($this->objDefId);
        
        $primaryKey = $this->objDef->getPrimaryKey();
        $pKeys=array_keys($primaryKey);        
                  
        foreach($pKeys as $key) {
            $propDef=$this->objDef->getProperty($key);                   
            $obj->{$propDef->name}= $keys[$propDef->name];
        }
        
        $retVal = $this->getByPK($obj, $recurse, False);
        
        // is primary key set to anythiong other than Null?
        if ($retVal===false) {
            return null;
        } else {
            return $obj;
        }
    }

        
    public function getOneByKeys($keys, $recurse=true) {
        $obj = NewSpecObj($this->objDefId);
        
        $primaryKey = $this->objDef->getPrimaryKey();
        $pKeys=array_keys($primaryKey);        
                  
        foreach($pKeys as $key) {
            $propDef=$this->objDef->getProperty($key);                   
            $obj->{$propDef->name}= $keys[$propDef->name];
        }
        
        $retVal = $this->getByPK($obj, $recurse, False);
        
        // is primary key set to anythiong other than Null?
        if ($retVal===false) {
            return null;
        } else {
            return $obj;
        }
    }
    
    
    public function getOneByPkDCC($keys, $recurse=true) {
        $obj = NewSpecObj($this->objDefId);
        
        $primaryKey = $this->objDef->getPrimaryKey();
        $pKeys=array_keys($primaryKey);        
        
        foreach($pKeys as $key) {
            $propDef=$this->objDef->getProperty($key);                   
            $obj->{$propDef->name}= $keys[$propDef->name];
        }
        
        $retVal = $this->getByPK($obj, $recurse, True);
        
        // is primary key set to anythiong other than Null?
        if ($retVal===false) {
            return null;
        } else {
            return $obj;
        }
    }
   
    // get one using the original primary key store in a hidden field, this is
    // used when getting data from edit forms, because one of the primary keys
    // may have been changed.
    public function getOneUsingOriginalPK($data) {
        
        $obj = NewSpecObj($this->objDefId);
        
        foreach($this->objDef->getPrimaryKey() as $propKeyName) {                 
            $propDef = $this->objDef->getProperty($propKeyName);

            $key = 'original-'.$propDef->name;
                 
            if (isset($data[$key])) {
                if ($data[$key]) {
                    $value = mysql2php($propDef,$data[$key]);
                    $obj->{$propDef->name} = $value;
                } else {
                    return null;   
                }
            } else {
                 return null;
                // if this is not set, a error should be generated.
            }
        }
        
        $this->getByPK($obj, true, False);
        
        // check all primary keys! 
        if (is_null($obj->{$propDef->name})) {
            return null;
        }
        else{
            return $obj;
        }
    }
    
    // given an array turn it into an object and get it.
    public function getOneUsingPK($data) {
        
        $obj = NewSpecObj($this->objDefId);
        
        foreach($this->objDef->getPrimaryKey() as $propKeyName) {                 
            $propDef = $this->objDef->getProperty($propKeyName);

            if (isset ($data[$propDef->name])) {
                $value = mysql2php($propDef,$data[$propDef->name]);
                $obj->{$propDef->name} = $value;
            } else {
                // if this is not set, a error should be generated.
            }
        }
        
        $this->getByPK($obj, true, False);
        
        // check all primary keys! 
        if (is_null($obj->{$propDef->name})) {
            return null;
        }
        else{
            return $obj;
        }
    }
    
    public function getOneByPkFromData($data) {
        
        $obj = NewSpecObj($this->objDefId);
        
        foreach($this->objDef->getPrimaryKey() as $propKeyName) {                 
            $propDef = $this->objDef->getProperty($propKeyName);

            if (isset ($data[$propDef->name])) {
                $obj->{$propDef->name} = $data[$propDef->name];
            } else {
                // if this is not set, a error should be generated.
            }
        }
        
        $this->getByPK($obj, true, False);
        
        // check all primary keys! 
        if (is_null($obj->{$propDef->name})) {
            return null;
        } else {
            return $obj;
        }
    }
           
    // convert data to a class
    public function createFromData($data,$nestedObjects=false) {
        
        $obj = NewSpecObj($this->objDefId);
 
        $i=0;
        foreach($this->objDef->getProperties() as $propDef) {            
            if (isset($data[$propDef->name]) || $nestedObjects=false) {
                if ($propDef->isScalar) {
                    $value = $data[$propDef->name];

                    if (is_blank($value)==true) {
                        $value=null;
                    }

                    $typedValue = mysql2php($propDef,$value);
                    $obj->{$propDef->name} = $typedValue;
                    $i+=1;
                } else if($propDef->isCollection()) {
                    $tableName=$propDef->getAttribute("referenceTable");                    
                    $tableDAL=NewDALObj($tableName);
                    $value=array();
                    foreach($data[$propDef->name] as $val){
                        $value[]=$tableDAL->createFromData($val);
                    }
                    $obj->{$propDef->name} = $value;
                    $i+=1;
                } else {                    
                     $tableName=$propDef->getAttribute("table");  
                     // HAS One relationship
                     if($tableName===null){
                         $tableName=$propDef->objectTypeOrig;
                     }
                     $tableDAL=NewDALObj($tableName);
                     $value=$tableDAL->createFromData($data[$propDef->name]);
                     $obj->{$propDef->name} = $value;
                     $i+=1;
                }
            } else {
                // TODO: should  a default value be used 
                // of is that the job of the constructor?
                // I think that the ObjDef has the defaults...
                // but the BaseDAL source code is generated from thr ObjDef
                // what is a human writes the BaseDAL and leaves of the defaults?                
            } 
        }
            
        return $obj;        
    }            
    
    public function createFromData2($objDef, $data, $nestedObjects=false) {
        global $app;
        
        $objDef=$objDef->transformSpec($data);                 
        $obj = NewSpecObj($objDef->name);
        
        foreach($objDef->getProperties() as $propDef) {            
            if($propDef->isScalar){
                if (isset($data[$propDef->name])) {
                    $value = $data[$propDef->name];
                    if (is_blank($value)==true) {
                        $value=null;
                    }
                    $typedValue = mysql2php($propDef,$value);
                    $obj->{$propDef->name} = $typedValue;                    
                }
            } else {           
                if (($propDef->getAttribute("isActive",true)===True) && (isset($data[$propDef->name]))) {
                    $associationType = $propDef->getAttribute("associationType", false);
                    if ($associationType=="HasOne") {                                            
                        // a HasOne association that is not composite should not be retrieved.                        
                        if ($propDef->getAttribute("isComposite",true)===True) {
                            $referenceTable=$propDef->getAttribute("referenceTable");
                            $referenceColumn=$propDef->getAttribute("referenceColumn");
                            $currentColumn = $propDef->getAttribute("column");
                            $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                            $currentColDef=$this->objDef->getProperty($currentColumn);                            
                            $referenceTableDAL=NewDALObj($referenceTableObjDef->name);     
                            $value=$referenceTableDAL->createFromData2($referenceTableObjDef, $data[$propDef->name]);
                            $obj->{$propDef->name} = $value;                             
                        } else {
                            $obj->{$propDef->name} = Null;                            
                        }
                    } else if ($associationType=="HasMany") {                        
                        $tableName=$propDef->getAttribute("referenceTable");                        
                        $tableDAL=NewDALObj($tableName);
                        $value=array();
                        foreach($data[$propDef->name] as $val){
                            $value[]=$tableDAL->createFromData($val);
                        }
                        $obj->{$propDef->name} = $value;                                                             
                    } else if ($associationType=="HasManyThrough") {                                                                      
                        $tableName=$propDef->getAttribute("throughTable");
                        $tableDAL=NewDALObj($tableName);                                                
                        $value=array();                                                
                        $intersectionObjDef=$app->specManager->findDef($tableName);                                                
                        foreach ($data[$propDef->name] as $val) {                                                                                                                                                             
                            //$tmp = $tableDAL->createFromData($val);
                            $interectionCls=NewSpecObj($tableName);                            
                            foreach($intersectionObjDef->getProperties() as $intersectionPropDef) {                            
                                $mapFrom = $intersectionPropDef->getAttribute("mapFrom", $intersectionPropDef->name);                            
                                if ($mapFrom == "<AUTOINCREMENT>") {                                                                
                                } else {                                                                             
                                    if (isset($val[$mapFrom])) {
                                        $interectionCls->{$intersectionPropDef->name} = $val[$mapFrom];                                    
                                    }
                                }                            
                            }                        
                            $value[]=$interectionCls;
                        }                        
                        $obj->{$propDef->name} = $value;                                                       
                    }                        
                }
            }            
        }
            
        return $obj;        
    }    
    
    
   
    
    
     public function bindQueryResultsToObjEnum($qs, $row, $obj) {

        $i=0;
        foreach($qs->selectedProperties as $property) {
            $value = mysql2php($property,$row[$i]);
            $obj->{$property->name} = $value;
            $i+=1;
        }
    }

    public function bindQueryResultsToObjAssoc($qs, $row, $obj, $dontConvertConstraint) {
        foreach($qs->selectedProperties as $property) {
            if (isset($row[$property->name])) {
                
                $value = $row[$property->name];

                if ($dontConvertConstraint==True) {
                    if ($property->hasConstraint()) {                                        
                        $newValue = $value;
                    } else {
                        $newValue = mysql2php($property,$value);
                    }
                } else {
                    $newValue = mysql2php($property,$value);
                }
                                                        
                $obj->{$property->name} = $newValue;
            } else {
                //echo "{$property->name} not in rs<br/>";
                $obj->{$property->name} = null;
            }
        }
    }

    public function getRecursive($row, $obj, $recurse, $dontConvertConstraint) {      
        global $app;
        
        foreach($this->objDef->getProperties() as $propDef){   
            if($propDef->getAttribute("associationType")=="HasOne")
            {                                              
                $addIt = True;

                $isComposite = $propDef->getAttribute("isComposite",false);
                if ($isComposite) {

                    $compositeTriggerColumn=$propDef->getAttribute("compositeTriggerColumn");
                    // get value
                    if (isset($obj->{$compositeTriggerColumn})) {
                        $compositeTriggerValue =  $obj->{$compositeTriggerColumn};
                        $compositeTriggerPropDef = $this->objDef->getProperty($compositeTriggerColumn);
                        $compositeTriggerConstraint = $compositeTriggerPropDef->getConstraint();
                        if ($compositeTriggerConstraint) {
                             $compositeTriggerConstraint->generate();
                             $columnCompositeLookups=$compositeTriggerConstraint->get();
                        } 
                        // Now get the Name of the ObjDef that the compositeTriggerColumn is set to...
                        // if any.  These are optional so it does not have to be set.
                        $compositeTriggerObjDefName = $columnCompositeLookups[$compositeTriggerValue];
                        // now compare
                        $referenceTable=$propDef->getAttribute("referenceTable");

                        if ($compositeTriggerObjDefName != $referenceTable) {
                            $addIt = False;
                        }
                    }
                }                        

                if ($addIt) {
                    $referenceTable=$propDef->getAttribute("referenceTable");
                    $referenceColumn=$propDef->getAttribute("referenceColumn");
                    $currentColumn = $propDef->getAttribute("column");
                    $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                    $currentColDef=$this->objDef->getProperty($currentColumn);

                    $whereStatement=new SQLWhere();
                    $whereStatement->addClause(new SQLComparison($referenceColumn,"=","%1%"));

                    $conn=$app->getConnection();

                    $SQLBuilder=new SQLBuilder($conn, $app->specManager);

                    $qs=$SQLBuilder->genGetByWhere($referenceTableObjDef, $whereStatement);
                    $statement=$qs->sql;

                    $QueryBuilder=new QueryBuilder();
                    $QueryBuilder->setStatement($statement);
                    if($obj->{$currentColumn}!==null){
                        $QueryBuilder->createParam("1",$currentColDef->objectType,$obj->{$currentColumn});
                        $SQL=$QueryBuilder->bindParams();

                        $result=$conn->select($SQL);

                        $tableDAL=NewDALObj($referenceTable);

                        $objDef=$app->specManager->findDef($referenceTable);
                        $primaryKeys=array_keys($objDef->getPrimaryKey());

                        $rowKey=array();

                        foreach($primaryKeys as $key){
                            $propDefTmp=$objDef->getProperty($key);
                            $value = mysql2php($propDefTmp,$row[$currentColumn]);
                            $rowKey[$propDefTmp->name]=$value;
                        }

                        // set path of child DAL to parent Stack Path                            
                        $this->stack->push($propDef->name);                                    
                        $tableDAL->stack = $this->stack;
                        $newRow=$tableDAL->getOneByPk($rowKey, true, $dontConvertConstraint);       
                        $this->stack->pop();

                        $obj->{$propDef->name}=$newRow;
                    }
                } 

            } else if($propDef->isCollection()){

                if ($propDef->getAttribute("associationType")=="HasManyThrough") {                                

                    // intersection ObjDef                            
                    // reference ObjDef                                                                                    
                    $currentColumn=$propDef->getAttribute("column");                                                                           
                    $intersectionTable=$propDef->getAttribute("throughTable");
                    $intersectionColumn=$propDef->getAttribute("throughColumnFrom");                                                            
                    $referenceTable=$propDef->getAttribute("referenceTable");                                                            
                    $referenceObjDef=$app->specManager->findDef($referenceTable);                                                                                            

                    $uniqueKey=$propDef->getAttribute("throughColumnTo");                                                        
                    $intersectionTableObjDef=$app->specManager->findDef($intersectionTable);                                                                                                                        
                    $intersectionTableColumnPropDef=$intersectionTableObjDef->getProperty($intersectionColumn);                            
                    $currentColDef=$this->objDef->getProperty($currentColumn);

                    // get all rows where intersectionColumn=$refValue

                    $whereStatement=new SQLWhere();
                    $whereStatement->addClause(new SQLComparison($intersectionColumn,"=","%1%"));
                    $conn=$app->getConnection();
                    $SQLBuilder=new SQLBuilder($conn, $app->specManager);
                    $qs=$SQLBuilder->genGetByWhere($intersectionTableObjDef, $whereStatement);
                    $statement=$qs->sql;

                    $QueryBuilder=new QueryBuilder();
                    $QueryBuilder->setStatement($statement);

                    // locate the primary key, nested in the obj using  the stack.             
                    if (count($this->stack->get())>0) {
                        $tmpObj = $obj;                                                            
                        foreach($this->stack->get() as $propDefName) {                                
                            if (isset($tmpObj->{$propDefName})) {
                                $tmpObj = $tmpObj->{$propDefName};
                            }
                        }                            
                    } else {
                        $tmpObj = $obj->{$currentColumn};
                    }

                    $QueryBuilder->createParam("1",$intersectionTableColumnPropDef->objectType,$tmpObj);                            
                    $SQL=$QueryBuilder->bindParams();
                    $result=$conn->select($SQL);

                    if (is_resource($result)) {       
                        $rowCount=mysql_num_rows($result);
                    } else {
                        $rowCount=0;
                    }
                    // if the query returns one or more results add an entry to
                    // the result set based on the propDef name;
                    if($rowCount>0){
                        // its the name of the collection, not the name of the Object type of the collection.
                        // besides, Specl collections can contain more than one ObjDef type anyway so this
                        // code won't even more in more complex cases.

                        // check to see if it exists and it ios an array?
                        $obj->{$propDef->name}=array();
                        $tableDAL=NewDALObj($referenceTable);
                        $intersectionDAL=NewDALObj($intersectionTableObjDef->name);

                        /* For each Row of the Results::
                         *      - get the value from the uniqueKey column
                         *      - query the DAL to get the row that corresponds to the key value
                         *      - add the row to the results
                         */
                        while($row2=$conn->getRowAssoc($result)) {

                            $intersectionObj=$intersectionDAL->createFromData($row2);

                            //$objDef=$app->specManager->findDef($tableName);
                            $primaryKeys=array_keys($referenceObjDef->getPrimaryKey());

                            $rowKey=array();                                    

                            $propDefTmp=$intersectionTableObjDef->getProperty($uniqueKey);
                            $value = mysql2php($propDefTmp,$row2[$propDefTmp->name]);
                            //$rowKey[$propDefTmp->name]=$value;

                            $primaryKeyPropDef = $referenceObjDef->getProperty($primaryKeys[0]);

                            $rowKey[$primaryKeyPropDef->name]=$value;

                            // set path of child DAL to parent Stack Path
                            $this->stack->push($primaryKeyPropDef->name);                                    
                            $tableDAL->stack = $this->stack;                                                                                                                                
                            $newRow=$tableDAL->getOneByPk($rowKey, true, $dontConvertConstraint);
                            $this->stack->pop();                                    
                            $intersectionObj->__data__ = $newRow;                                                                        
                            $obj->{$propDef->name}[]=$intersectionObj;                                        
                        }
                        // or Build join based on association data. (can be in the DAL)6
                    }
                } else if ($propDef->getAttribute("associationType")=="HasMany") {

                    $currentColumn=$propDef->getAttribute("column");

                    $referenceTable=$propDef->getAttribute("referenceTable");
                    $referenceColumn=$propDef->getAttribute("referenceColumn");                                
                    $uniqueKey=$propDef->getAttribute("associationUniqueKey");
                    $referenceTableObjDef=$app->specManager->findDef($referenceTable);                            
                    $currentColDef=$this->objDef->getProperty($currentColumn);

                    // get all rows where $referenceColumn=$refValue

                    $whereStatement=new SQLWhere();
                    $whereStatement->addClause(new SQLComparison($referenceColumn,"=","%1%"));
                    $conn=$app->getConnection();
                    $SQLBuilder=new SQLBuilder($conn, $app->specManager);
                    $qs=$SQLBuilder->genGetByWhere($referenceTableObjDef, $whereStatement);
                    $statement=$qs->sql;

                    $QueryBuilder=new QueryBuilder();
                    $QueryBuilder->setStatement($statement);

                    // locate the primary key, nested in the obj using  the stack.                            
                    if (count($this->stack->get())>0) {
                        $tmpObj = $obj;                                                            
                        foreach($this->stack->get() as $propDefName) {                                
                            if (isset($tmpObj->{$propDefName})) {
                                $tmpObj = $tmpObj->{$propDefName};
                            } 
                        }                            
                    } else {
                        $tmpObj = $obj->{$currentColumn};
                    }

                    //$QueryBuilder->createParam("1",$currentColDef->objectType,$obj->{$currentColumn});
                    $QueryBuilder->createParam("1",$currentColDef->objectType,$tmpObj);                            
                    $SQL=$QueryBuilder->bindParams();
                    $result=$conn->select($SQL);
                    $rowCount=mysql_num_rows($result);

                    // if the query returns one or more results add an entry to
                    // the result set based on the propDef name;
                    if($rowCount>0){
                        // its the name of the collection, not the name of the Object type of the collection.
                        // besides, Specl collections can contain more than one ObjDef type anyway so this
                        // code won't even more in more complex cases.

                        // check to see if it exists and it ios an array?
                        $obj->{$propDef->name}=array();

                        // get the tableName associated with the uniqueKey column and load the DAL

                        $uniqueColumn=$referenceTableObjDef->getProperty("{$uniqueKey}");

                        if($uniqueColumn->hasAttribute("fkTable")){
                            $tableName= $uniqueColumn->getAttribute("fkTable");
                        } else{
                            $tableName= $uniqueColumn->getAttribute("table");
                        }

                        $tableDAL=NewDALObj($tableName);

                        /* For each Row of the Results::
                         *      - get the value from the uniqueKey column
                         *      - query the DAL to get the row that corresponds to the key value
                         *      - add the row to the results
                         */
                        while($row2=$conn->getRowAssoc($result)){
                            $objDef=$app->specManager->findDef($tableName);
                            $primaryKeys=array_keys($objDef->getPrimaryKey());

                            $rowKey=array();                                    
                            if (get_class($tableDAL) == get_class($this)) {
                                $recursive = true;                                                                                                                       
                                // this works for a non-compostite key                                        
                                foreach($primaryKeys as $key){
                                    $propDefTmp=$objDef->getProperty($key);
                                    $value = mysql2php($propDefTmp,$row2[$propDefTmp->name]);
                                    $rowKey[$propDefTmp->name]=$value;
                                }                                                                                                                        

                            } else {
                                $recursive = false;                                        
                                foreach($primaryKeys as $key){
                                    $propDefTmp=$objDef->getProperty($key);
                                    $value = mysql2php($propDefTmp,$row2[$propDefTmp->name]);
                                    $rowKey[$propDefTmp->name]=$value;
                                }                                        
                            }

                            // set path of child DAL to parent Stack Path
                            //$this->stack->push($propDef->name);          
                            // get the attribute of the propdef of the column that this propdef's association is bound to...

                            $primaryKeyPropDef = $objDef->getProperty($primaryKeys[0]);
                            $this->stack->push($primaryKeyPropDef->name);                                    
                            $tableDAL->stack = $this->stack;                                                                                                                                
                            $newRow=$tableDAL->getOneByPk($rowKey, true, $dontConvertConstraint);
                            $this->stack->pop();

                            // merge in intersection row results...
                            foreach($row2 as $key=>$value) {
                                $propDefTmp=$referenceTableObjDef->getProperty($key);
                                if ($dontConvertConstraint==True) {
                                    if ($propDefTmp->hasConstraint()) {                                        
                                        $newValue = $value;
                                    } else {
                                        $newValue = mysql2php($propDefTmp,$value);
                                    }
                                } else {
                                        $newValue = mysql2php($propDefTmp,$value);
                                }
                                $newRow->{$key}=$newValue;
                            }                                    
                            $obj->{$propDef->name}[]=$newRow;    
                        }
                        // or Build join based on association data. (can be in the DAL)6
                    } 
                }   
            }   
        }                
    }
    // get the pobject , then get all its associations.
    // this is classic ORM
    // this function nests recursively based on the data. If an association has data associated
    // with it the function will continue to recurse, if the association does not have data
    // associated with it, the recursion stops.
    public function getByPK($obj, $recurse, $dontConvertConstraint) {        
        global $app;
                                 
        $qs = $this->sqlBuilder->genGetByPK($this->objDef, $obj);        

        if (is_null($this->dbConn)) {
            // connection name not found or some other problem
            return false;
        }
        
        $results = $this->dbConn->select($qs->sql);
        $row = $this->dbConn->getRowAssoc($results);

        if ($row!==false)
        {                                    
            $this->bindQueryResultsToObjAssoc($qs, $row, $obj, $dontConvertConstraint);

            if ($recurse==true) {
                $this->getRecursive($row, $obj, $recurse, $dontConvertConstraint);
            }
            
            return true;
        } else {
            return false;
        }
    }
               
    public function getByPK2($obj, $recurse, $dontConvertConstraint) {        
        global $app;
        
        $qs = $this->sqlBuilder->genGetByPK2($this->objDef, $obj);        

        if (is_null($this->dbConn)) {
            // connection name not found or some other problem
            return false;
        }
        
        $results = $this->dbConn->select($qs->sql);

        if ($results!=0)
        {
            $row = $this->dbConn->getRowAssoc($results);
            $this->bindQueryResultsToObjAssoc($qs, $row, $obj, $dontConvertConstraint);

            if ($recurse==true) {
                foreach($this->objDef->getProperties() as $propDef){
                    
                    if ($propDef->isCollection() || !$propDef->isScalar()){
                        
                        if($propDef->getAttribute("associationType")=="HasOne")
                        {      
                            //echo 'here';
                        } 
                        else if (($propDef->getAttribute("associationType")=="HasMany")||($propDef->getAttribute("associationType")=="HasManyThrough")) {                                 
                                                        
                            $currentColumn=$propDef->getAttribute("column");
                            
                            if ($propDef->getAttribute("associationType")=="HasMany") {                                
                                $referenceTable=$propDef->getAttribute("referenceTable");
                                $referenceColumn=$propDef->getAttribute("referenceColumn");                                
                                $uniqueKey=$propDef->getAttribute("associationUniqueKey");
                                $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                            } else {
                                $referenceTable=$propDef->getAttribute("throughTable");
                                $referenceColumn=$propDef->getAttribute("throughColumnFrom");                                
                                $uniqueKey=$propDef->getAttribute("throughColumnTo");
                                $referenceTableObjDef=$app->specManager->findDef($referenceTable);                                                                
                            }
                            
                            $currentColDef=$this->objDef->getProperty($currentColumn);

                            // get all rows where $referenceColumn=$refValue

                            $whereStatement=new SQLWhere();
                            $whereStatement->addClause(new SQLComparison($referenceColumn,"=","%1%"));

                            $conn=$app->getConnection();

                            $SQLBuilder=new SQLBuilder($conn, $app->specManager);

                            $qs=$SQLBuilder->genGetByWhere($referenceTableObjDef, $whereStatement);
                            $statement=$qs->sql;

                            $QueryBuilder=new QueryBuilder();
                            $QueryBuilder->setStatement($statement);
                            $QueryBuilder->createParam("1",$currentColDef->objectType,$obj->{$currentColumn});
                            $SQL=$QueryBuilder->bindParams();

                            $result=$conn->select($SQL);

                            $rowCount=mysql_num_rows($result);

                            // if the query returns one or more results add an entry to
                            // the result set based on the propDef name;
                            if($rowCount>0){
                                // its the name of the collection, not the name of the Object type of the collection.
                                // besides, Specl collections can contain more than one ObjDef type anyway so this
                                // code won't even more in more complex cases.

                                // check to see if it exists and it ios an array?
                                $obj->{$propDef->name}=array();

                                // get the tableName associated with the uniqueKey column and load the DAL

                                $uniqueColumn=$referenceTableObjDef->getProperty("{$uniqueKey}");

                                if($uniqueColumn->hasAttribute("fkTable")){
                                    $tableName= $uniqueColumn->getAttribute("fkTable");
                                } else{
                                    $tableName= $uniqueColumn->getAttribute("table");
                                }

                                $tableDAL=NewDALObj($tableName);

                                /* For each Row of the Results::
                                 *      - get the value from the uniqueKey column
                                 *      - query the DAL to get the row that corresponds to the key value
                                 *      - add the row to the results
                                 */

                                while($row2=$conn->getRowAssoc($result)){

                                    $objDef=$app->specManager->findDef($tableName);
                                    $primaryKeys=array_keys($objDef->getPrimaryKey());

                                    $rowKey=array();

                                    if (get_class($tableDAL) == get_class($this)) {
                                        $recursive = true;                                       
                                        
                                        $associationType = $propDef->getAttribute("associationType", false);
                                                                                            
                                        if ($propDef->getAttribute("associationType")=="HasMany") {                                
                                            $referenceTable=$propDef->getAttribute("referenceTable");
                                            $referenceColumn=$propDef->getAttribute("referenceColumn");                                
                                            $uniqueKey=$propDef->getAttribute("associationUniqueKey");
                                            $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                                        } else {
                                            $referenceTable=$propDef->getAttribute("throughTable");
                                            $referenceColumn=$propDef->getAttribute("throughColumnFrom");                                
                                            $uniqueKey=$propDef->getAttribute("throughColumnTo");
                                            $referenceTableObjDef=$app->specManager->findDef($referenceTable);                                                                
                                        }
                                                              
                            
        
                                        if ($uniqueKey) {
                                            // this works for a non-compostite key

                                            foreach($primaryKeys as $key){
                                                $propDefTmp=$objDef->getProperty($key);
                                                $value = mysql2php($propDefTmp,$row2[$uniqueKey]);
                                                $rowKey[$propDefTmp->name]=$value;
                                            }              
                                        } else {
                                            
                                        }
                                        
                                    } else {
                                        $recursive = false;
                                        
                                        foreach($primaryKeys as $key){
                                            $propDefTmp=$objDef->getProperty($key);
                                            $value = mysql2php($propDefTmp,$row2[$propDefTmp->name]);
                                            $rowKey[$propDefTmp->name]=$value;
                                        }                                        
                                    }                                                                                                   

                                    $newRow=$tableDAL->getOneByPk($rowKey, true, $dontConvertConstraint); //getOneByPk2
                                    
                                    // merge in intersection row results...
                                    foreach($row2 as $key=>$value) {

                                        $propDefTmp=$referenceTableObjDef->getProperty($key);

                                        if ($dontConvertConstraint==True) {
                                            if ($propDefTmp->hasConstraint()) {                                        
                                                $newValue = $value;
                                            } else {
                                                $newValue = mysql2php($propDefTmp,$value);
                                            }
                                        } else {
                                                $newValue = mysql2php($propDefTmp,$value);
                                        }

                                        $newRow->{$key}=$newValue;
                                    }

                                    //$results->{$propDef->objectTypeOrig}[]=$newRow;
                                    // get the ObjDef of the Collection
                                    
                                    $collectionObjDef = $app->specManager->findDef($propDef->objectTypeOrig);
                                    if ($collectionObjDef->isAssociative) {                                        
                                        //$keyPropDef = $objDef->getProperty($collectionObjDef->keyId);
                                        //$key = $newRow->{$keyPropDef->name};
                                        //$obj->{$propDef->name}[$key]=$newRow;                                                
                                        
                                        $obj->{$propDef->name}[]=$newRow;
                                        
                                    } else {
                                        $obj->{$propDef->name}[]=$newRow;
                                    }
                                }
                                // or Build join based on association data. (can be in the DAL)6
                            } 
                        }   
                    }   
                }
            }
        } else {
            return false;
        }
    }    

    public function getByKeys($obj, $keys, $recurse=true) {        
        global $app;
        
        $qs = $this->sqlBuilder->genGetByKeys($this->objDef, $keys);        

        if (is_null($this->dbConn)) {
            // connection name not found or some other problem
            return false;
        }
        
        $results = $this->dbConn->select($qs->sql);

        if ($results!=0)
        {
            $row = $this->dbConn->getRowAssoc($results);
            $this->bindQueryResultsToObjAssoc($qs, $row, $obj, False);

            if ($recurse==true) {
                foreach($this->objDef->getProperties() as $propDef){
                    
                    if ($propDef->isCollection() || !$propDef->isScalar()){
                        
                        $associationType = $propDef->getAttribute("associationType", false);
                         
                        if($associationType=="HasOne") {      
                            // TODO
                        } else if (($propDef->getAttribute("associationType")=="HasMany")||($propDef->getAttribute("associationType")=="HasManyThrough")) { 
                                                        
                            $currentColumn=$propDef->getAttribute("column");
                            
                            if ($propDef->getAttribute("associationType")=="HasMany") {                                
                                $referenceTable=$propDef->getAttribute("referenceTable");
                                $referenceColumn=$propDef->getAttribute("referenceColumn");                                
                                $uniqueKey=$propDef->getAttribute("associationUniqueKey");
                                $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                            } else {
                                $referenceTable=$propDef->getAttribute("throughTable");
                                $referenceColumn=$propDef->getAttribute("throughColumnFrom");                                
                                $uniqueKey=$propDef->getAttribute("throughColumnTo");
                                $referenceTableObjDef=$app->specManager->findDef($referenceTable);                                                                
                            }
                                                                                    
                            $currentColDef=$this->objDef->getProperty($currentColumn);

                            // get all rows where $referenceColumn=$refValue

                            $whereStatement=new SQLWhere();
                            $whereStatement->addClause(new SQLComparison($referenceColumn,"=","%1%"));

                            $conn=$app->getConnection();

                            $SQLBuilder=new SQLBuilder($conn, $app->specManager);

                            $qs=$SQLBuilder->genGetByWhere($referenceTableObjDef, $whereStatement);
                            $statement=$qs->sql;

                            $QueryBuilder=new QueryBuilder();
                            $QueryBuilder->setStatement($statement);
                            $QueryBuilder->createParam("1",$currentColDef->objectType,$obj->{$currentColumn});
                            $SQL=$QueryBuilder->bindParams();

                            $result=$conn->select($SQL);

                            $rowCount=mysql_num_rows($result);

                            // if the query returns one or more results add an entry to
                            // the result set based on the propDef name;
                            if($rowCount>0){
                                // its the name of the collection, not the name of the Object type of the collection.
                                // besides, Specl collections can contain more than one ObjDef type anyway so this
                                // code won't even more in more complex cases.

                                // check to see if it exists and it ios an array?
                                $obj->{$propDef->name}=array();

                                // get the tableName associated with the uniqueKey column and load the DAL

                                $uniqueColumn=$referenceTableObjDef->getProperty("{$uniqueKey}");

                                if($uniqueColumn->hasAttribute("fkTable")){
                                    $tableName= $uniqueColumn->getAttribute("fkTable");
                                } else{
                                    $tableName= $uniqueColumn->getAttribute("table");
                                }

                                $tableDAL=NewDALObj($tableName);

                                /* For each Row of the Results::
                                 *      - get the value from the uniqueKey column
                                 *      - query the DAL to get the row that corresponds to the key value
                                 *      - add the row to the results
                                 */

                                while($row2=$conn->getRowAssoc($result)){
                                    $objDef=$app->specManager->findDef($tableName);
                                    $primaryKeys=array_keys($objDef->getPrimaryKey());

                                    $rowKey=array();

                                    foreach($primaryKeys as $key){
                                        $propDefTmp=$objDef->getProperty($key);
                                        $value = mysql2php($propDefTmp,$row2[$propDefTmp->name]);
                                        $rowKey[$propDefTmp->name]=$value;
                                    }

                                    $newRow=$tableDAL->getOneByPk($rowKey, false);

                                    // merge in intersection row results...
                                    foreach($row2 as $key=>$value) {
                                        $propDefTmp=$referenceTableObjDef->getProperty($key);
                                        $newValue = mysql2php($propDefTmp,$value);
                                        $newRow->{$key}=$newValue;
                                    }

                                    //$results->{$propDef->objectTypeOrig}[]=$newRow;
                                    $obj->{$propDef->name}[]=$newRow;
                                }
                                // or Build join based on association data. (can be in the DAL)6
                            } 
                        }   
                    }   
                }
            }
            
            return true;
        } else {
            return false;
        }
    }
    
    public function insert($obj) {
        global $app;

        if ($this->objDef->getAttribute("hasCustomAutoIncrement",false)==true) {
            // this object has a custiom generated auto increment function            
            $this->customAutoIncrement($obj);
        }
        
        $sql = $this->sqlBuilder->genInsert($this->objDef,$obj);
        
        try{                        
             $status = $this->dbConn->insert($sql);

            if ($status > 0)  {
                $autoIncrementKey = $status;
                              
                // assign the value of the property in $obj to $autoIncrementKey
                // which is alway the primary key.

                // only set the keys of the primary key that are linked to an
                // autoincrement key of another table
                foreach($this->objDef->primaryKey as $objKey) {                    
                    $propDef=$this->objDef->getProperty($objKey);                                    
                    if ($propDef->isAttribute("autoIncrement",true)) {
                        $obj->{$propDef->name} = $autoIncrementKey;
                        break; // there can be only one.
                    }
                }        
            } else {
                $autoIncrementKey = null;
            }                
            $results = new DALResults(true, "insert", null,null,null, $autoIncrementKey);
            
        }catch(Exception $e){
            $message=$e->getMessage();            
            $results = new DALResults(false, "insert", "SQL failed",$message,null,null);            
        }
        
        return $results;        
    }
        
    public function insertTransaction($obj) {
        global $app;        
        $tranMan = new TransactionManager($this->dbConn);         
        
        //$obj = $this->createFromData($eventData,$nestedObjects=true);
        
        $tranOp = $tranMan->addInsert($this, $this->objDef, $obj);
                
        $this->recursiveInsertTransactional($this->objDef, $obj, $tranOp, $tranMan);                
        
        $results = $tranMan->execute();        
        
        // The idea here was to copy the new object, with a new autoid if the object,
        // had an autoid column. However, this is unneccessary because php objects
        // are pass by reference which means the original object passed in will have an
        // autoincrement id added to the object anyway. 
        // newObj is used in many places in the code, however, and cannot be remove right now.
        $this->newObj = $obj;
         
        return $results;        
    }
    
    public function addInsertToTransaction($obj,$transactionManager){
        $parentTranOp=$transactionManager->addInsert($this,$this->objDef,$obj);
        $this->recursiveInsertTransactional($this->objDef,$obj,$parentTranOp,$transactionManager);
        $this->newObj=$obj;
    }
    
    public function recursiveInsertTransactional($objDef, $obj, $parentTranOp, $tranMan) {                   
        global $app;                
        
        foreach ($objDef->getProperties() as $propDef) {
            
            $associationType = $propDef->getAttribute("associationType", false);
             
            if ($associationType=="HasOne") {                                                    
                $referenceTable=$propDef->getAttribute("referenceTable");
                $referenceColumn=$propDef->getAttribute("referenceColumn");                
                $referenceTableObjDef=$app->specManager->findDef($referenceTable);                                                
                $referenceTableDAL=NewDALObj($referenceTableObjDef->name);
                $referenceTableObjDef->addDefaultValues($referenceObj,"add"); // TODO think about different modes (other than add)
                
                if (isset($obj->{$propDef->name})) {
                    $addIt = true;
                    $referenceObj = $obj->{$propDef->name};

                    $tranOp = $tranMan->addInsert($referenceTableDAL, $referenceTableObjDef, $referenceObj);
                
                    // copy the primary keys of the parent into the child
                    // is the primary key of the parent an autoincrement?
                    foreach($objDef->primaryKey as $objKey) {                    

                        $pkPropDef=$objDef->getProperty($objKey);                                    

                        if ($pkPropDef->isAttribute("autoIncrement",true)) {                        
                            $copyAutoIncrement = new CopyAutoIncrement(array('parentTranOp'=>$parentTranOp, 'tranOp'=>$tranOp,'to'=>$referenceColumn));                                                                        
                            $tranOp->addTransformation($copyAutoIncrement);                        
                        } else {
                            $referenceObj->{$referenceColumn} = $obj->{$pkPropDef->name};
                        }
                    }                

                    $referenceTableDAL->recursiveInsertTransactional($referenceTableObjDef, $referenceObj, $tranOp, $tranMan);                                
                }
                
            } else if (($propDef->getAttribute("associationType")=="HasMany")||($propDef->getAttribute("associationType")=="HasManyThrough")) { 
                                                                
                $intersectionTableObjDef=$app->specManager->findDef($propDef->objectTypeOrig);                        

                if (isset($obj->{$propDef->name}))
                {
                    $childrenObj = $obj->{$propDef->name};
                    $childrenCount = count($childrenObj);

                    if ($childrenCount>0) {
                        $currentColumn=$propDef->getAttribute("column");
                        
                        if ($propDef->getAttribute("associationType")=="HasMany") {                                
                            $referenceTable=$propDef->getAttribute("referenceTable");
                            $referenceColumn=$propDef->getAttribute("referenceColumn");                                
                            $uniqueKey=$propDef->getAttribute("associationUniqueKey");
                            $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                        } else {
                            $referenceTable=$propDef->getAttribute("throughTable");
                            $referenceColumn=$propDef->getAttribute("throughColumnFrom");                                
                            $uniqueKey=$propDef->getAttribute("throughColumnTo");
                            $referenceTableObjDef=$app->specManager->findDef($referenceTable);                                                                
                        }

                        // are there any nested objects for this collection that 
                        // need to be inserted?

                        $referenceTableDAL=NewDALObj($referenceTableObjDef->name);     
                                                
                        foreach($childrenObj as $childObj) {                                    
                            $childObj->{$referenceColumn} = $obj->{$currentColumn};
                            
                            $tranOp = $tranMan->addInsert($referenceTableDAL, $referenceTableObjDef, $childObj);                                       
                            
                            // copy the primary keys of the parent into the child
                            // is the primary key of the parent an autoincrement?
                            foreach($objDef->primaryKey as $objKey) {                    

                                $pkPropDef=$objDef->getProperty($objKey);                                    

                                if ($pkPropDef->isAttribute("autoIncrement",true)) {                        
                                    $copyAutoIncrement = new CopyAutoIncrement(array('parentTranOp'=>$parentTranOp, 'tranOp'=>$tranOp,'to'=>$referenceColumn));                                                                        
                                    $tranOp->addTransformation($copyAutoIncrement);                        
                                } else {
                                    $childObj->{$referenceColumn} = $obj->{$pkPropDef->name};
                                }
                            }                                                       
                            
                            $referenceTableDAL->recursiveInsertTransactional($referenceTableObjDef, $childObj, $tranOp, $tranMan);                                          
                        }                                                             
                        //$referenceTableDAL->insertOneTransactional($referenceTableObjDef, $childObj, $tranMan);
                        
                    }
                }
            }
        }
    }        
    
    
    public function insertOne($obj) {
        global $app;
        
        //$trans = new MySQLTransaction($obj);
        
        // begin transaction
        
        // check to see if this is this part of a larger transaction?
        
        // if so, nest transationas are not allowed, and could cause deadlocks anyway
                        
        if ($this->objDef->getAttribute("hasCustomAutoIncrement",false)==true) {
            // this object has a custiom generated auto increment function            
            $this->customAutoIncrement($obj);
        }
        
        $sql = $this->sqlBuilder->genInsert($this->objDef,$obj);
        
        try{                        
            $status = $this->dbConn->insert($sql);

            if ($status > 0)  {
                
                $autoIncrementKey = $status;
                
                // assign the value of the property in $obj to $autoIncrementKey
                // which is alway the primary key.
                                
                // only set the keys of the primary key that are linked to an
                // autoincrement key of another table
                foreach($this->objDef->primaryKey as $objKey) {                    
                                        
                    $propDef=$this->objDef->getProperty($objKey);                                    
                    
                    if ($propDef->isAttribute("autoIncrement",true)) {
                        if (is_array($obj)==True) {
                            $obj[$propDef->name] = $autoIncrementKey;
                        } else {
                            $obj->{$propDef->name} = $autoIncrementKey;
                        }                                                
                    }
                }
                
                $this->newObj = $obj;
                                                    
            } else {
                $autoIncrementKey = null;
                
                $this->newObj = $obj;
            }
                
            $results = new DALResults(true, "insert", null,null,null, $autoIncrementKey);
      
            foreach ($this->objDef->getProperties() as $propDef) {
                
                $associationType = $propDef->getAttribute("associationType", false);
                        
                if ($associationType=="HasOne") {                         
                    $isComposite = $propDef->getAttribute("isComposite",false);
                    if ($isComposite) {                   
                        if (isset($obj->{$propDef->name})) {
                            $referenceTable=$propDef->getAttribute("referenceTable");
                            $referenceTableObjDef=$app->specManager->findDef($referenceTable);                            
                            $referenceTableDAL=NewDALObj($referenceTableObjDef->name);
                            $compObj = $obj->{$propDef->name};                        
                            $childInsertResults = $referenceTableDAL->insertOne($compObj);  //returns a DALResults                            
                        }
                    } else {
                    }                                                        
                } else if($propDef->isCollection()) {
                    if (($associationType=="HasMany")||($associationType=="HasManyThrough")) {   
                        
                        $intersectionTableObjDef=$app->specManager->findDef($propDef->objectTypeOrig);                        

                        // get the primary key of the $intersectionTableObjDef
                        
                        if (isset($obj[$propDef->name]))
                        {
                            $childrenObj = $obj[$propDef->name];                            
                            $childrenCount = count($childrenObj);
                            
                            if ($childrenCount>0) {                                
                                $currentColumn=$propDef->getAttribute("column");
                                
                                if ($propDef->getAttribute("associationType")=="HasMany") {                                
                                    $referenceTable=$propDef->getAttribute("referenceTable");
                                    $referenceColumn=$propDef->getAttribute("referenceColumn");                                
                                    $uniqueKey=$propDef->getAttribute("associationUniqueKey");
                                    $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                                } else {
                                    $referenceTable=$propDef->getAttribute("throughTable");
                                    $referenceColumn=$propDef->getAttribute("throughColumnFrom");                                
                                    $uniqueKey=$propDef->getAttribute("throughColumnTo");
                                    $referenceTableObjDef=$app->specManager->findDef($referenceTable);                                                                
                                }
                        
                                // are there any nested objects for this collection that 
                                // need to be inserted?
                                
                                $referenceTableDAL=NewDALObj($referenceTableObjDef->name);     
                                
                                foreach($childrenObj as $childObj){
                                    $childObj[$referenceColumn] = $obj[$currentColumn];                                                                         
                                    $childInsertResults = $referenceTableDAL->insertOne($childObj);  //DALResults
                                    // Errors here are not returned....
                                }
                            } else {
                                // the array was empty...nothing to insert.
                            }
                        }
                    }
                }
            }
            
            // end transaction            
            // is this the outer most transation?
            // commit transaction
            
        }catch(Exception $e){
            $message=$e->getMessage();            
            $results = new DALResults(false, "insert", "SQL failed",$message,null,null);            
            // roll back transaction ??            
            // do any applictaion specific cleanup due to a failed tranmsaction            
        }
        
        return $results;        
    }

    
    public function insertMany($objects) {
        global $app;
               
        $results=array();
        $i=0;
        foreach($objects as $obj){                
            $sql= $this->sqlBuilder->genInsert($this->objDef,$obj);            
            try{
                $insertId=$this->dbConn->insert($sql);
                $results[$i]=$insertId;
            }catch(Exception $e){
                $results[$i]=$e->getMessage();
            }
            $i++;
        }
        
        return $results;        
    }

    public function updateOneByPK($objOld, $objNew) {        
        
        $sql = $this->sqlBuilder->genUpdateByPK($this->objDef, $objOld, $objNew);        
        if (is_null($sql)) {
            // nothing to update...$objOld was the same as $objNew
            $results = new DALResults(true, "update", "no update required, old and new were the same",null,null, null);            
        } else {
            try{
                $sqlStatus=$this->dbConn->update($sql); // the status really doesn't matter 
                // because the update() throws and exception                
                $results = new DALResults(true, "update", null,null,null, null);                
            }catch(Exception $e){                
                $message=$e->getMessage();            
                $results = new DALResults(false, "update", "SQL failed",$message,null,null);                        
            }            
        }    
        return $results;    
    }
    
    public function updateTransaction($obj, $eventData=NULL) {
         
        if (isnull($eventData)) {
            $eventData = array();
            
            $primaryKey = $this->objDef->getPrimaryKey();
            $pKeys=array_keys($primaryKey);        
                  
            foreach($pKeys as $key) {
                $propDef=$this->objDef->getProperty($key);                                     
                $eventData['original-'.$propDef->name] = $obj->{$propDef->name};
                $eventData[$propDef->name] = $obj->{$propDef->name};                
            }
        }
        
        $tranMan = new TransactionManager($this->dbConn);         
        $this->updateOneTransactional($obj, $eventData, $tranMan);                
        $results = $tranMan->execute();        
        $this->newObj = $obj;
         
        return $results;
    }
    
    // updateOneTransactional
    // 
    //  is a row missing from the newObj that was in the oldObj?
    //  can the row be deleted, is there a foreign key constraint that 
    //  prevents it from being deleted?
    //  
    // insert
    //  is a row in the newObj that is not in the oldObj?
    //  does the new foreign key validate?  does the new row validate?
    //  Reasons for not validating:
    //      the foreign key may not exist
    //      the row could be a dup.
    //      the row may not be allowed by some condition
    //      
    //  errors need to be displayed to the user.
    //      
    //  Nasty issue...dups do not get detected until after the first one is 
    //  inserted
    //          
    // update
    //  how do you update an intersection table?  Is this allowed?
    //  does the current system evem support this?
    
    
    /*
     *  updateOneTransaction performs an update of the database in a transaction.
     *  This function takes an object with the new values and the original eventData
     *  gathered from the form. The original eventData is required because it contains
     *  the value of the key of the original object, even if the user changes the key
     *  in the form.
     * 
     *  updateOneTransaction does the following:
     *      - Finds all the columns that reference the object being changed, including associations
     *      - Updates all of the references with the new value
     *      - Update the orignal object, or create a new entry and remove the old one if needed
     *      - Recursively Update all the subobjects if a nested object is passed in to be updated.
     *
     *  see the comments in the code for more specifics about how it works.
     */
    private function updateOneTransactional($newObj, $eventData, $tranMan) {
        global $app;
        
        $queryBuilder=new QueryBuilder();
        $oldObj = $this->getOneUsingOriginalPK($eventData);
        $this->oldObj = $oldObj;      
        
        $references=$this->FindEditReferences($newObj, $eventData, $this->objDef);
       
        // if the object has references that need to be updated, update all the
        // references first, then changed the nested data. References include associations.
        if(count($references)!==0){
            
             // check to see if the current table has an autoincrement key.
            $autoCol=null;
            if($this->objDef->getAttribute("hasAutoIncrement",false)===true){
                // find the autoincrement column                    
                foreach($this->objDef->properties as $propDef){
                    if($propDef->getAttribute("autoIncrement",false)===true){
                        $autoCol=$propDef->name;
                        break;
                    }
                }
            }
            
            $updateReferences=true;
            
            // if the table has an autoincrement and the new value does not exist in the database,
            // just update the entry. All the columns will still reference the same autoid and do not need
            // to be updated.
            // Ideally this would be done before getting all the references, but I am not sure how.
            if($autoCol!==null){
                // All the references should point to the same autoId so it is only necessary to check the first reference
                $reference=$references[0];
                $refSpec=$app->specManager->findDef($reference["tableName"]);
                $colPropDef=$this->objDef->getProperty($reference["columnReferenced"]);  
                
                $statement="Select $autoCol as keyVal from {$this->objDef->getAttribute("table",$this->objDef->name)} where {$reference["columnReferenced"]}=%1%;";
                $queryBuilder->setStatement($statement);                     
                $queryBuilder->createParam("1", $colPropDef->objectType, $newObj->{$reference["columnReferenced"]});        
                $SQL = $queryBuilder->bindParams();                
                $result=$this->dbConn->fetchAssoc($SQL);
                if(count($result)===0){
                    $tranMan->addUpdate($this, $this->objDef, $oldObj, $newObj);
                    $updateReferences=false;
                }                  
            }
            
            if($updateReferences){
                // check to see if the new object exists in the database based on the primary key
                $objInDB=$this->getOneByPk($newObj,false);

                // disable foreignKeyConstraints and delete the old entry
                // this is needed because MySQL does not support deferring constraint checking till after
                // the transaction is completed.
                $tranMan->disableForeignKeyConstraint();
                $tranMan->addDelete($this, $this->objDef, $oldObj);
                $tranMan->enableForeignKeyConstraint();

                // create a new object in the database if, it does not already exist.
                // the object can already exist if you change the value to an existing value in the database.
                if(isnull($objInDB)){                               
                    $tranMan->addInsert($this, $this->objDef, $newObj);  
                }
                // if the new object does exist update it with the new values, if any
                else{
                    $tranMan->addUpdate($this, $this->objDef, $objInDB, $newObj);
                }

                // change all of the references of the old value to the new value.           
                foreach($references as $reference){                  
                    $refSpec=$app->specManager->findDef($reference["tableName"]);
                    $colPropDef=$this->objDef->getProperty($reference["columnReferenced"]);  
                    // if the current table has an autoincrement key lookup the autoincrement get the autoId of the lookup value and
                    // the value to substitute.
                    if($autoCol!==null){   
                        $statement="Select $autoCol as keyVal from {$this->objDef->getAttribute("table",$this->objDef->name)} where {$reference["columnReferenced"]}=%1%;";
                        $queryBuilder->setStatement($statement);                     
                        $queryBuilder->createParam("1", $colPropDef->objectType, $reference["value"]);        
                        $SQL = $queryBuilder->bindParams();                
                        $result=$this->dbConn->fetchAssoc($SQL);
                        $lookupValue=$result[0]["keyVal"];                    

                        $statement="Select $autoCol as keyVal from {$this->objDef->getAttribute("table",$this->objDef->name)} where {$reference["columnReferenced"]}=%1%;";
                        $queryBuilder->setStatement($statement);                     
                        $queryBuilder->createParam("1", $colPropDef->objectType, $newObj->{$reference["columnReferenced"]});        
                        $SQL = $queryBuilder->bindParams();                
                        $result=$this->dbConn->fetchAssoc($SQL);
                        $valueToSubstitute=$result[0]["keyVal"];
                    }
                    else{
                        $lookupValue=$reference["value"];
                        $valueToSubstitute=$newObj->{$reference["columnReferenced"]};
                    }

                    // change reference values by issuing an update command
                    $statement="Update {$refSpec->getAttribute("table",$refSpec->name)} set {$reference["columnName"]}=%1% where {$reference["columnName"]}=%2%";
                    $queryBuilder->setStatement($statement);                     
                    $queryBuilder->createParam("1", $colPropDef->objectType, $valueToSubstitute);                
                    $queryBuilder->createParam("2", $colPropDef->objectType, $lookupValue);        
                    $SQL=$queryBuilder->bindParams();
                    $tranMan->addSQLUpdate($this->dbConn,$SQL);
                }            
            }        
        }
        // if there are no references just update the object
        else{
            $tranMan->addUpdate($this, $this->objDef, $oldObj, $newObj);
        }
        
        foreach ($this->objDef->getProperties() as $propDef) {
            $associationType = $propDef->getAttribute("associationType", false);   
            // update nested data objects
            if ($associationType=="HasOne") {               
                // TODO, is primary key changes, it must be propogated down.                
                $referenceTable=$propDef->getAttribute("referenceTable");
                $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                $referenceColumn=$propDef->getAttribute("referenceColumn");
                $currentColumn=$propDef->getAttribute("column");
                $uniqueKey=$propDef->getAttribute("associationUniqueKey");
                $referenceTableDAL=NewDALObj($referenceTableObjDef->name);       
                                
                if (isset($oldObj->{$propDef->name})) {
                    $hasOneOldObj = $oldObj->{$propDef->name}; 
                } else {
                    $hasOneOldObj = Null;                
                }

                if (isset($newObj->{$propDef->name})) {
                    $hasOneNewObj = $newObj->{$propDef->name}; 
                } else {
                    $hasOneNewObj = Null;
                }
                
                if (!isnull($hasOneOldObj)&&!isnull($hasOneNewObj)) {                                    
                    $tranMan->addUpdate($referenceTableDAL, $referenceTableObjDef, $hasOneOldObj, $hasOneNewObj);                    
                } else if (!isnull($hasOneOldObj)&&isnull($hasOneNewObj)) {                    
                    // TODO                                    
                } else if (isnull($hasOneOldObj)&&!isnull($hasOneNewObj)) {                                    
                    // TODO                    
                } else if (isnull($hasOneOldObj)&&isnull($hasOneNewObj)) {                    
                    // TODO                    
                }                 
            }
            // add new nested collection objects, remove old ones, and update existing ones that have been changed
            else if (($associationType=="HasMany")||($associationType=="HasManyThrough")) {  
                                
                $currentColumn=$propDef->getAttribute("column");
                
                if ($propDef->getAttribute("associationType")=="HasMany") {                                
                    $referenceTable=$propDef->getAttribute("referenceTable");
                    $referenceColumn=$propDef->getAttribute("referenceColumn");                                
                    $uniqueKey=$propDef->getAttribute("associationUniqueKey");
                    $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                } else {
                    $referenceTable=$propDef->getAttribute("throughTable");
                    $referenceColumn=$propDef->getAttribute("throughColumnFrom");                                
                    $uniqueKey=$propDef->getAttribute("throughColumnTo");
                    $referenceTableObjDef=$app->specManager->findDef($referenceTable);                                                                
                }

                // are there any nested objects for this collection that 
                // need to be inserted?

                $referenceTableDAL=NewDALObj($referenceTableObjDef->name);       

                if (isset($newObj->{$propDef->name})) 
                {
                    $newChildrenObj = $newObj->{$propDef->name};                    
                    $newChildrenCount = count($newChildrenObj);
                    // propogate new reference value to all children that do not have
                    foreach($newChildrenObj as $childObj){                         
                        if ($childObj->{$referenceColumn} == null) { 
                            $childObj->{$referenceColumn} = $newObj->{$referenceColumn};                                
                        }                         
                    }
                } else {
                    $newChildrenCount = 0;
                }

                if (isset($oldObj->{$propDef->name})) 
                {
                    $oldChildrenObj = $oldObj->{$propDef->name};  
                    $oldChildrenCount=count($oldChildrenObj);
                    // propogate new reference value to all children
                    foreach($oldChildrenObj as $childObj){
                        $childObj->{$referenceColumn} = $newObj->{$referenceColumn}; 
                    }
                } else {
                    $oldChildrenObj = array();
                    $oldChildrenCount = 0;
                }

                if ($oldChildrenCount>0) {
                    // check for children that have been remove from the new object and delete them from the database
                    foreach($oldChildrenObj as $oldChildObj) {  
                        $exists = $this->findAinB($referenceTableObjDef,$oldChildObj,$newChildrenObj);
                        if ($exists==False) {
                            $tranMan->addDelete($referenceTableDAL, $referenceTableObjDef,$oldChildObj);
                        }
                    }                                
                }                                               

                if ($newChildrenCount>0) {
                    // check for children that have been added to the new object and insert them into the database
                    foreach($newChildrenObj as $newChildObj) {                                                                
                        $newClassObj = $newChildObj;  
                                              
                        // check to see in the newChild is in the oldChild list, if it is not add it to the database,
                        // if the newChild exists in the oldChild list, then update the entry in database.
                        $exists = $this->findAinB($referenceTableObjDef,$newClassObj,$oldChildrenObj);                            
                        if ($exists==False) {
                            $tranMan->addInsert($referenceTableDAL, $referenceTableObjDef,$newClassObj);
                        } 
                        else {                                
                            // get the $oldChildrenObj
                            foreach($oldChildrenObj as $oldChildObj) {   
                                if ($oldChildObj->{$uniqueKey}==$newChildObj->{$uniqueKey}) {
                                    $oldChildObjUpdate = $oldChildObj;
                                    break;
                                }                                    
                            }  
                            $tranMan->addUpdate($referenceTableDAL, $referenceTableObjDef, $oldChildObjUpdate, $newChildObj); 
                        }                            
                    }                            
                }                                        
            }
        }          
    }
    
    // update one row, no nesting, no transaction
    // this is a low overhead update for situation that do not require
    // complex nested operations.    
    public function updateOne($eventData) {
        $oldObj = $this->getOneUsingOriginalPK($eventData);        
        $newObj = $this->createFromData($eventData,$nestedObjects=true);                                
        return $this->update($oldObj, $newObj);            
    }
    
    public function update($oldObj, $newObj) {
        global $app;
                
        // build SQL
        $sql = $this->sqlBuilder->genUpdate($this->objDef, $oldObj, $newObj);
        
        if (is_null($sql)) {
            // nothing to update...$objOld was the same as $objNew            
            $results = new DALResults(true, "update", null,null,null,null);            
        } else {                        
            try {
                $status = $this->dbConn->update($sql);
                $results = new DALResults(true, "update", null,null,null,null);
            } catch(Exception $e) {
                $message=$e->getMessage();
                $results = new DALResults(false, "update", "updateOneByPK SQL failed",$message,null,null);
            }
        }        
        return $results;        
    }
    
    public function findAinB($referenceTableObjDef,$newChildObj,$oldChildrenObj) {
                
        foreach($oldChildrenObj as $oldChildObj) {
            $allKeysMatch=true;
            
            foreach($referenceTableObjDef->primaryKey as $key) {
                
                $propDef = $referenceTableObjDef->getProperty($key);
                
                $valueA = $newChildObj->{$propDef->name};
                $valueB = $oldChildObj->{$propDef->name};
                
                if ($valueA != $valueB) {
                    $allKeysMatch=false;
                    continue;
                }
            }
            
            if ($allKeysMatch==true) {
                return true;
            }
        }
        
        return false;
    }
    
    public function updateMany() {
        // TODO
    }
    
    public function delete($obj) {        
        
        $sql = $this->sqlBuilder->genDeleteByPK($this->objDef, $obj);
        
        if (is_null($sql)) {
            // hmm no SQL ?
            $results = new DALResults(false, "delete", "genDeleteByPK failed to create any SQL",null,null,null);            
        } else {
            try{
                $status=$this->dbConn->delete($sql);
                
                $results = new DALResults(true, "delete", null,null,null,null);
                
            }catch(Exception $e){
                
                $message=$e->getMessage();
                $results = new DALResults(false, "delete", "deleteOneByPK SQL failed",$message,null,null);
            }
            
        }        
        
        return $results;    
    }    
   
    
    public function deleteOneByPK($obj) {        
        
        // create a Transaction that performs cascading deletes for all subordinate tables
        
        
        $sql = $this->sqlBuilder->genDeleteByPK($this->objDef, $obj);
        
        if (is_null($sql)) {
            // hmm no SQL ?
            $results = new DALResults(false, "delete", "genDeleteByPK failed to create any SQL",null,null,null);            
        } else {
            try{
                $status=$this->dbConn->delete($sql);
                
                $results = new DALResults(true, "delete", null,null,null,null);
                
            }catch(Exception $e){
                
                $message=$e->getMessage();
                $results = new DALResults(false, "delete", "deleteOneByPK SQL failed",$message,null,null);
            }
        }        
        
        return $results;    
    }    
    
    public function deleteMany() {
        // TODO
    }

    public function getOne($sessionId) {
        // TODO
    }


    public function getMany() {
        // TODO
    }
    
    public function getAll() {                
                
        $rows=array();        
        $qs = $this->sqlBuilder->genSelectAll($this->objDef);                
        $results = $this->dbConn->select($qs->sql);
        
        if ($results!=0)
        {
            while($row = $this->dbConn->getRowAssoc($results)) {                            
                $objInstance = NewSpecObj($this->objDef->name);
                $this->bindQueryResultsToObjAssoc($qs, $row, $objInstance, False);    
                $rows[]=$objInstance;                
            }                    
        }    
        
        return $rows;
    }
    
    
    public function setObjDef($objDef){
        $this->objDef=$objDef;
    }
    
    
    public function deleteTransaction($eventData) {
         
        $tranMan = new TransactionManager($this->dbConn); 
        
        $this->deleteOneTransactional($eventData, $tranMan);
                
        $results = $tranMan->execute();
        
        return $results;
    }
    
    public function deleteOneTransactional($eventData, $tranMan) {
        global $app;
                
        $deleteObj = $this->getOneUsingPK($eventData); 
                                                        
        foreach ($this->objDef->getProperties() as $propDef) {
            
            $associationType = $propDef->getAttribute("associationType", false);                        
            if ($associationType=="HasOne") {               

                $referenceTable=$propDef->getAttribute("referenceTable");
                $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                $referenceColumn=$propDef->getAttribute("referenceColumn");
                $currentColumn=$propDef->getAttribute("column");
                $uniqueKey=$propDef->getAttribute("associationUniqueKey");
                $referenceTableDAL=NewDALObj($referenceTableObjDef->name);       
                                
                if (isset($deleteObj->{$propDef->name})) {
                    $hasOneObj = $deleteObj->{$propDef->name}; 
                } else {
                    $hasOneObj = Null;                }
                            
                if (!isnull($hasOneObj)) {  
                    $hasOneObjDAL=NewDALObj($referenceTable);
                    $hasOneObjDAL->deleteOneTransactional($hasOneObj, $tranMan);
                }                                                

            } else if (($associationType=="HasMany")||($associationType=="HasManyThrough")) {
                
                
                $currentColumn=$propDef->getAttribute("column");
                
                if ($propDef->getAttribute("associationType")=="HasMany") {                                
                    $referenceTable=$propDef->getAttribute("referenceTable");                                
                    $referenceColumn=$propDef->getAttribute("referenceColumn");                                
                    $uniqueKey=$propDef->getAttribute("associationUniqueKey");
                    $referenceTableObjDef=$app->specManager->findDef($referenceTable);
                } else {
                    $referenceTable=$propDef->getAttribute("throughTable");
                    $referenceColumn=$propDef->getAttribute("throughColumnFrom");                                
                    $uniqueKey=$propDef->getAttribute("throughColumnTo");
                    $referenceTableObjDef=$app->specManager->findDef($referenceTable);                                                                
                }
                
                
                

                // are there any nested objects for this collection that 
                // need to be inserted?
                $referenceTableDAL=NewDALObj($referenceTableObjDef->name);       
                if (isset($deleteObj->{$propDef->name})) 
                {
                    $deleteChildrenObj = $deleteObj->{$propDef->name};  
                    $deleteChildrenCount = count($deleteChildrenObj);

                    if ($deleteChildrenCount>0) {
                        foreach($deleteChildrenObj as $deleteChildObj) {                                   
                            $associationObj = NewSpecObj($referenceTableObjDef->name);
                            foreach($referenceTableObjDef->primaryKey as $pKey) {
                                $pKeyPropDef = $referenceTableObjDef->getProperty($pKey);
                                if (isset ($deleteObj->{$pKeyPropDef->name})) {
                                    $associationObj->{$pKeyPropDef->name} = $deleteObj->{$pKeyPropDef->name};
                                } else if (isset ($deleteChildObj->{$pKeyPropDef->name})) {
                                    $associationObj->{$pKeyPropDef->name} = $deleteChildObj->{$pKeyPropDef->name};
                                } else {
                                    echo __FILE__ . ":" . __LINE___ .":error";
                                }
                            }
                            $childObjDAL=NewDALObj($referenceTable);
                            $childObjDAL->deleteOneTransactional($associationObj, $tranMan);                           
                        }                                
                    }                                                                       
                }                     
            }
        }
        $tranMan->addDelete($this, $this->objDef, $deleteObj);
    }
    
    /*
     *  FindEditReferences is used to find all of the data referenced by the object whose
     *  values are being change. Only the properties whose value are different in the new object 
     *  compared to the old object are searched to find the references.
     * 
     *  If references are found they are returned as a list of tableName,columnName,columnReferenced,value,numberOfReferences
     *  rows, based on how many times a value is found in a particular column.
     * 
     *  Eventdata is required to get the key of the original object. The eventdata contains a field
     *  for the original key, even if the user changes the key in the edit form.
     */
    public function FindEditReferences($obj,$eventData,$objDef){
        global $app;
        // get oldObj        
        $oldObj = $this->getOneUsingOriginalPK($eventData);  
        
        // get the differences between the old and the new obj.
        $diff=compareAToB($objDef, $obj, $oldObj);
        
        $references=array();
        // if there are no differences don't check for references
        if(count($diff)===0){
            return $references;
        }
        
        // get all the tables that reference that current table from the Spec.
        $referenceTables=$objDef->getAttribute("referencedBy",array());        
        if(count($referenceTables)===0){
            return $references;
        }
        
        // find all the columns with refs based on the Spec and store them in a temporary array with the table name as the key and a list of arrays containing 
        // the columnName, the idColumn and the displayColumn.
        // A column has a reference if the property in the Spec has a constraint that references the current table and
        // the id or display column of the constraint is one of the properties that changed.
        $currentTable=$objDef->getAttribute("table",$objDef->name);        
        $diffColNames=array_keys($diff);
        $columnsWithRefs=array();
        foreach($referenceTables as $refTable){
            $refSpec=$app->specManager->findDef($refTable);
            foreach($refSpec->getProperties() as $propDef){
                // must also check if column is excluded from the query, which most likely means the column does not exist in the database.
                if($propDef->hasConstraint() && $propDef->getAttribute("excludeFromQuery",false)===false){
                    $constraint=$propDef->getConstraint();                    
                    if($constraint->tableName===$currentTable){
                        if(in_array($constraint->displayColumnName,$diffColNames)){
                            $columnsWithRefs[$refTable][]=array("columnName"=>$propDef->name,"displayColumn"=>$constraint->displayColumnName,"idColumn"=>$constraint->idColumnName);
                          
                        }
                        else if(in_array($constraint->idColumnName,$diffColNames)){ 
                            $columnsWithRefs[$refTable][]=array("columnName"=>$propDef->name,"displayColumn"=>$constraint->idColumnName,"idColumn"=>$constraint->idColumnName);                                
                        }
                        
                    }
                }
            }
        }
        
        // Check all of the columns that reference this table and see how many times that old value occurs in the column in the database.
        // If the value occurs one or more times add an entry to the reference array with the tableName,columnName,columnReferenced value and 
        // numberOfReferences.
        $queryBuilder=new QueryBuilder();
        foreach($columnsWithRefs as $tableName=>$columns){
            $tableDef=$app->specManager->findDef($tableName);
            foreach($columns as $column){
                // most properly convert the value to SQL based on the type in the spec.
                $propDef=$tableDef->getProperty($column["columnName"]);                
                $statement="Select count(*) as count from {$tableName} where {$column["columnName"]}=%1%;";
                $queryBuilder->setStatement($statement);                
                $queryBuilder->createParam("1", $propDef->objectType, $oldObj->{$column["idColumn"]});        
                $SQL = $queryBuilder->bindParams();                
                $result=$this->dbConn->fetchAssoc($SQL);
                $count=$result[0]["count"];
                if($count>0){
                    $references[]=array("tableName"=>$tableName,"columnName"=>$column["columnName"],"columnReferenced"=>$column["displayColumn"],
                                      "value"=>$oldObj->{$column["displayColumn"]},"numberOfReferences"=>$count);
                }                
            }
        }                
        return $references;
    }
    
    /*
     * FindDeleteReferences is used to find all of the data referenced by an object that is being
     * delete. This function differs from findEditReferences in that the function takes primarykey 
     * values as a key/value pair array, instead of an object. All of the properties of the object
     * are checked for references.
     * 
     * This function does not find references from associations, only if properties are used as
     * constraints by other tables. The references to associations is handled elsewhere.
     * 
     *  If references are found they are returned as a list of TableName,ColumnName,NumberOfReferences
     *  rows, based on how many times a value is found in a particular column.
     */
    public function FindDeleteReferences($keyValues,$objDef){
        
    }
    
    // Find an object based on a SQLWhere object
    public function findByFilter($whereStatement){
        $qs = new QueryDef();
        $term="";
        $columnsList="";
        $propIdsNotToInclude=$this->sqlBuilder->propsNotToInclude($this->objDef);
        
        foreach($this->objDef->getProperties() as $key=>$property) {                   
            if (!array_key_exists($property->name, $propIdsNotToInclude)) {
                $columnsList .= $term."`".$property->name."`";
                $qs->selectedProperties[] = $property;
                if ($term=="") $term=",";
            }
        }
                
        $sql = "select $columnsList from {$this->objDef->getAttribute("table",$this->objDef->name)}  where ".$whereStatement->generateWhereClause()." limit 1";
        $results = $this->dbConn->select($sql);
        $obj = Null;
        if ($results!=0)
        {
            $row = $this->dbConn->getRowAssoc($results);
            if ($row) {
                $obj = NewSpecObj($this->objDefId);        
                $this->bindQueryResultsToObjAssoc($qs, $row, $obj, $dontConvertConstraint=True);
                
                $this->getRecursive($row, $obj, $recurse=true, $dontConvertConstraint=True);                 
            }
        }         
        return $obj;
    }              
}
?>