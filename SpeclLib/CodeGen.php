<?php
/**
 * Description of CodeGen
 *
 * @author tony
 */
class CodeGen {
    
    public $filename;
    public $directory;
    public $fullfilename;
    
    public function  __construct() {

    }
      
    public function start() {
        ob_start();
    }

    public function save() {
        $str=ob_get_contents();
        ob_end_clean();
        file_put_contents($this->fullfilename,$str);
    }    
}
?>
