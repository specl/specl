<?php
class CollectionDefSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("CollectionDef");
                
        $prop = $objDef->createProperty("id", "string");
        $objDef->addConstructorParam("id");

        $prop = $objDef->createProperty("isAssociative", "boolean");
        $objDef->addConstructorParam("isAssociative");

        $prop = $objDef->createProperty("keyId", "string");
        $objDef->addConstructorParam("keyId");

        $prop = $objDef->createCollection("childTypes", "ChildTypes");  
        $prop->setAttribute("saveReferencesOnly", true);
          
        $prop->setSetter('addChildTypesFromArray');        

        $spec->addDef($objDef);
    }
}

?>