<?php
class PropDefsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("PropDefs", true);
        $colDef->addChildType("PropDef");       
        $colDef->setKeyId("id");
        $spec->addDef($colDef);        
    }
}
?>
