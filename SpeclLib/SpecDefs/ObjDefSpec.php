<?php
class ObjDefSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("ObjDef");

        $prop = $objDef->createProperty("name", "string");
                
        $prop = $objDef->createProperty("id", "string");
      
        $objDef->addConstructorParam("id");
        
        $prop = $objDef->createProperty("version", "string");
        $objDef->addConstructorParam("version");

        $prop = $objDef->createProperty("defType", "string");
        $prop = $objDef->createProperty("guid", "string");
        $prop = $objDef->createProperty("isScalar", "string");
        $prop = $objDef->createProperty("domain", "string");
                
        $prop = $objDef->createProperty("description", "string");
        
    
    
        $prop = $objDef->createCollection("properties", "PropDefs");        
        $prop->setSetter('addPropertiesFromArray');
        
        $prop = $objDef->createCollection("primaryKey", "PrimaryKey");
        $prop->setSetter('addPropDefToPrimaryKey');

        $prop = $objDef->createCollection("conditions", "Conditions");

//        requiredProperties
//        optionalProperties
//        validationErrors
//        ranValidation
//        constructorParameters
//        currentPath
//        states
//        specDef
//        filters
//        crossChecked
//        constraint
//        attributes        
//        associations  
//        transforms   
//        mode        
    
        $objDef->addKey("id");
        
        $spec->addDef($objDef);
    }
}

?>