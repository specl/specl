<?php
class ConditionsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Conditions", false);        
        
        $path = dirname(__FILE__) ."/Conditions/";
        
        $colDef->addChildTypesFromPath($path);
        
//        $colDef->addChildType("NotEmptyCondition");        
//        $colDef->addChildType("FloatTypeCondition");
//        $colDef->addChildType("IntegerTypeCondition");
//        $colDef->addChildType("DateTypeCondition");
//        $colDef->addChildType("SQLLookupUsingObjDefCondition");
//        $colDef->addChildType("FKLookupCondition");
        
        
        $colDef->setKeyId("id");
        $spec->addDef($colDef);        
    }
}
?>
