<?php
class ConstraintsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Constraints", false);
        
        $colDef->addChildType("YesNoConstraint");
        $colDef->addChildType("SQLConstraint");
        $colDef->addChildType("LookupConstraint");
        $colDef->setKeyId("id");
        $spec->addDef($colDef);        
    }
}
?>