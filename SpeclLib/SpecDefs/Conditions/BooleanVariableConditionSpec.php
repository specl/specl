<?php
class BooleanVariableConditionSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("BooleanVariableCondition");
        $prop = $objDef->createProperty("key", "string");
        $prop = $objDef->createProperty("value", "boolean");
        $spec->addDef($objDef);
    }
}
?>