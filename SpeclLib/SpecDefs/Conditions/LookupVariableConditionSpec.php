<?php
class LookupVariableConditionSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("LookupVariableCondition");
        $prop = $objDef->createProperty("key", "string");
        $prop = $objDef->createProperty("value", "string");
        $spec->addDef($objDef);
    }
}
?>