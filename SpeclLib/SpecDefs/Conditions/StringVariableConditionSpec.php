<?php
class StringVariableConditionSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("StringVariableCondition");
        $prop = $objDef->createProperty("key", "string");
        $prop = $objDef->createProperty("value", "string");
        $spec->addDef($objDef);
    }
}
?>