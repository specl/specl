<?php
class SQLLookupUsingObjDefConditionSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("SQLLookupUsingObjDefCondition");

        $prop = $objDef->createProperty("mustExist", "boolean");
        $prop = $objDef->createProperty("errMsg", "string");
        $prop = $objDef->createProperty("mode", "string");

        $objDef->addConstructorParam("mustExist");
        $objDef->addConstructorParam("errMsg");
        $objDef->addConstructorParam("mode");

        $spec->addDef($objDef);
    }
}

?>
