<?php
class FilterTextConditionSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("FilterTextCondition");
        $prop = $objDef->createProperty("operator", "string");
        $prop = $objDef->createProperty("characters", "string");
        $spec->addDef($objDef);
    }
}
?>