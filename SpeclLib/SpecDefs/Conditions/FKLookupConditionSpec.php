<?php
class FKLookupConditionSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("FKLookupCondition");

        $prop = $objDef->createProperty("objDef", "string");
        $prop = $objDef->createProperty("fkTable", "string");
        $prop = $objDef->createProperty("fkColumn", "string");
        $prop = $objDef->createProperty("exists", "boolean");
        $prop = $objDef->createProperty("errMsg", "string");        
        $prop = $objDef->createProperty("connectionName", "string");
        $prop = $objDef->createProperty("allowNulls", "boolean");
        $prop = $objDef->createProperty("multiColumn", "boolean");
            
        //$prop = $objDef->createProperty("propertyId", "string");
        
        $prop = $objDef->createProperty("whereClause", "SQLWhere");
        

//        public $propNames;

    
    
        $objDef->addConstructorParam("fkTable");
        $objDef->addConstructorParam("fkColumn");
        $objDef->addConstructorParam("exists");
        $objDef->addConstructorParam("errMsg");

        $spec->addDef($objDef);
    }
}

?>
