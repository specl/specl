<?php
class PropDefSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("PropDef");

        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("propType", "string");
        $prop = $objDef->createProperty("objectType", "string");
        $prop = $objDef->createProperty("link", "boolean");

        $prop = $objDef->createCollection("attributes", "attributes");
        $prop->setSetter('addAttributesFromArray');

        $prop = $objDef->createCollection("conditions", "conditions");
        $prop = $objDef->createCollection("constraints", "constraints");

        $objDef->addConstructorParam("id");
        $objDef->addConstructorParam("propType");
        $objDef->addConstructorParam("objectType");
        $objDef->addConstructorParam("link");

        $objDef->addKey("id");
        $spec->addDef($objDef);
    }
}

?>
