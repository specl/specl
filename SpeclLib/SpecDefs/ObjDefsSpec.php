<?php
/**
 * Description of ObjDefsSpec
 *
 * @author tony
 */

class ObjDefsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("ObjDefs", true);
        $colDef->addChildType("ObjDef");
        $colDef->setKeyId("id");
        $spec->addDef($colDef);
    }
}
?>
