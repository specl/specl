<?php

class ValidationError {

    public $ruleName;
    public $objDefName;
    public $propertyName;
    public $message;
    public $specName;
    public $value;  // the orginal value that was incorrect
    public $errorLevel;  // 0 error, 1 warning    
    public $errorCode;
    
    public function __construct($objDefName, $propertyName,$ruleName,$path, $msg, $errorCode, $value=null)
    {
        $this->objDefName = $objDefName;
        $this->propertyName=$propertyName;
        $this->path=$path;
        $this->fullPath=$path.App::ZONESSEP.$propertyName;
        $this->ruleName=$ruleName;
        $this->message=$msg;
        $this->value=$value;
        $this->errorLevel=0;        
        $this->errorCode=$errorCode;
    }

    public function getErrorMessage($lineSep=Null)
    {
        if (is_null($this->value)) {
            $this->value = 'NULL';
        }
     
        if ($this->errorLevel==0) {
            $type = "Error";
        } else if ($this->errorLevel==1) {
            $type = "Warning";
        }
        //$errorMessage="{$type}: Property:'{$this->propertyName}' Msg:{$this->message} Value:{$this->value}";       
        
        if (is_array($this->propertyName)) {
            $propName = "";
            $sep="";
            foreach($this->propertyName as $name) {
                $propName .= $sep.$name;
                if ($sep=="") $sep=",";
            }            
            $errorMessage="{$type}: ObjDef:{$this->objDefName} Properties:{$propName} : {$this->message}";                   
        } else {                        
            $errorMessage="{$type}: ObjDef:{$this->objDefName} Property:{$this->propertyName} : {$this->message}";                   
        }
                
        return $errorMessage;
    }
    
}
?>