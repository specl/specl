<?php
/**
 * SpecInstance is a nested self describing data structure.  Nodes can be either
 * ObjDefs of CollectionDefs.  A third type called "Unknown" was created to handle
 * errors. (04/26/2011:It is not used and not tested.)
 * 
 * Every node is dictionary.  Three keys, __path__, __type__ and __defType__,  
 * are added to every dictionary.    The values of __type__ and __defType___ refer to
 * SpecDefs.  A SpecManger object should have these Defs or they must be loaded
 * in order for the SpecInstance to make any sense.  Without the SpecDef that defines
 * a node, a SpecInstance isn't as powerful as it could be.  While not nessecarily 
 * a fatal error, SpecInstances and the SpecManger should always be in sync with
 * each other.
 * 
 *
 * @author tony
 */


class SpecInstance {

    public $def;
    public $path;
    public $data;

    public function __construct($def, $path) {
        $this->def = $def;
        $this->path = $path;
        $this->data = array();

        if (is_a($def, "ObjDef")) {
            $this->data['__type__'] = $def->name;
            $this->data['__defType__'] = "Object";
        } else if (is_a($def, "CollectionDef")) {
            $this->data['__type__'] = $def->name;
            $this->data['__defType__']= "Collection";
            $this->data['data'] = array();
        } else  {
            $this->data['__defType__'] = 'Unknown';
            $this->data['__type__'] = $def->id;
        }
        $this->data['__path__'] = $path;
    }
}

?>
