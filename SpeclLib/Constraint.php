<?php
class Constraint
{
   public function generate()
   {
       throw Exception("Function must be implemented in the subclass");
   }

   public function get()
   {
       throw Exception("Function must be implemented in the subclass");
   }

   public function count()
   {
       throw Exception("Function must be implemented in the subclass");
   }
}
?>
