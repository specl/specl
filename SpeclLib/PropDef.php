<?php
/**
 * Description of Property
 *
 * @author Tony
 */
class PropDef extends BaseDef {

    public $name;
    public $propType;
    public $isScalar;
    public $setter;
    public $getter;
    public $link;    
    
    public function  __construct($params) {

        if (isset($params["id"])) {
            $id = $params["id"];
        }

        if (isset($params["propType"])) {
            $propType = $params["propType"];
        } else {
            $propType = "unknown";
        }

        if (isset($params["link"])) {
            $link = $params["link"];
        } else {
            $link = false;
        }

        if (isset($params["objectType"])) {
            $objectType = $params["objectType"];
        } else if (isset($params["objectType"])) {
            $objectType = $params["objectType"];
        } else {
            $objectType = null;
        }

        if (is_array($params)) {
            if (isset($params["domain"])) {
                $domain = $params["domain"];
            } else {
                $domain = "sys";
            }

        } else {
            $domain = "sys";  // the default domain is sys
        }
        
        $this->name = $id;
        $id = strtolower($id);
        
        parent::__construct($id, get_class($this), $domain);
        
        $this->propType = strtolower(trim($propType));
        $this->objectType = strtolower($objectType);
        $this->objectTypeOrig = $objectType;
        $this->id = $id;
        $this->link = $link;
        //$this->conditions = array();
        $this->isScalar=false;
        $this->setter=null;
        $this->getter=null;
        
    }

    public function getPropType() {
        return $this->propType;
    }

    public function setIsScalar($value) {
                
        if (is_bool($value)) {
            $this->isScalar = $value;
        }
    }
    
    public function isScalar() {
        //if (!$this->isObject() && !$this->isCollection())  {
        if ($this->isScalar==True)  {
            return True;
        } else {
            return False;
        }
    }

    public function isObject() {
        if ($this->propType == "object") {
            return true;
        } else {
            return false;
        }
    }

    public function isCollection() {
        if ($this->propType == "collection") {
            return true;
        } else {
            return false;
        }
    }

    public function setSetter($setter) {
        $this->setter=$setter;
    }
    
    
    public function getDefaultValue($mode="any") {
        global $app;
    
        if ($this->isCollection()) {
            return array();
        }
                
        $defaultValue = Null;
        
        if($this->hasAttribute('defaultValue',$mode))
        {
            $defaultValue = $this->getAttribute('defaultValue',$mode);
            
            if (is_null($defaultValue)==False) {
                                
                if ($defaultValue === "SESSION.userId") {                                
                    $defaultValue = $app->getValue("SESSION.userId", null);
                } else if ($defaultValue==="now()") {
                    //$defaultValue = date("D M j G:i:s T Y");
                    $defaultValue = date("Y-m-d G:i:s");                
                } else if ($defaultValue==="USER.defaultLaboratory") {                
                    $defaultValue = $app->getValue("USER.defaultLaboratory", null);
                }
            }
        }
        
        return $defaultValue;
    }
    
    public function isReadOnly($mode="any") {
        
        if($this->hasAttribute("readOnly",$mode))
        {
            $value = $this->getAttribute("readOnly",$mode);
        } else {
            $value = False;
        }
        
        return $value;
    }
    
    // What are all the modes used by this property?
    public function getAllModesUsed() {    
         return array_keys($this->attributes);         
    }
    
    // What are all the attributes used by this property?
    public function getAllAttributesUsed() {
        $allAttributes= Array();
        foreach ($this->attributes as $key => $attributesArray) {
            $allAttributes = array_unique(array_merge($allAttributes, array_keys($attributesArray)));
        }
        return $allAttributes;
    }
    
    // What are all the conditions used by this property?
    
    // What are all the constraints used by this property?
    
    
    
}
?>
