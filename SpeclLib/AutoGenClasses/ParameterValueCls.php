<?php
/**
* Class ParameterValue
*
* @author ClassFactory.php genClass
*/
class ParameterValueCls extends SpecCls {
    public $key; // object of type string
    public $value; // object of type string
    
    public function  __construct() {
        parent::__construct('ParameterValue');
        $this->key = null; 
        $this->value = null; 
    }
}
?>
