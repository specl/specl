<?php
/**
* Class Article
*
* @author ClassFactory.php genClass
*/
class ArticleCls extends SpecCls {
    public $id; // object of type string
    public $title; // object of type string
    public $createdBy; // object of type string
    public $createdOn; // object of type string
    public $changedOn; // object of type string
    public $content; // object of type string
    
    public function  __construct() {
        parent::__construct('Article');
        $this->id = null; 
        $this->title = null; 
        $this->createdBy = null; 
        $this->createdOn = null; 
        $this->changedOn = null; 
        $this->content = null; 
    }
}
?>
