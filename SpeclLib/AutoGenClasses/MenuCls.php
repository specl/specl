<?php
/**
* Class Menu
*
* @author ClassFactory.php genClass
*/
class MenuCls extends SpecCls {
    public $menuId; // object of type string
    public $title; // object of type string
    public $description; // object of type string
    public $addedOn; // object of type datetime
    public $addedBy; // object of type string
    public $menuItems; // collection of type MenuItems
    
    public function  __construct() {
        parent::__construct('Menu');
        $this->menuId = null; 
        $this->title = null; 
        $this->description = null; 
        $this->addedOn = null; 
        $this->addedBy = null; 
        $this->menuItems = array(); 
    }
}
?>
