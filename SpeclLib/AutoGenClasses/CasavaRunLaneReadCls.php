<?php
/**
* Class CasavaRunLaneRead
*
* @author ClassFactory.php genClass
*/
class CasavaRunLaneReadCls extends SpecCls {
    public $casavaRunFlowCellReadId; // object of type integer
    public $laneId; // object of type integer
    public $readNumber; // object of type integer
    public $tileCount; // object of type double
    public $clustersRaw; // object of type double
    public $clustersRawSD; // object of type double
    public $clustersPF; // object of type double
    public $clustersPFSD; // object of type double
    public $prcPFClusters; // object of type double
    public $prcPFClustersSD; // object of type double
    public $phasing; // object of type double
    public $prephasing; // object of type double
    public $calledCyclesMin; // object of type double
    public $calledCyclesMax; // object of type double
    public $prcAlign; // object of type double
    public $prcAlignSD; // object of type double
    public $errRatePhiX; // object of type double
    public $errRatePhiXSD; // object of type double
    public $errRate35; // object of type double
    public $errRate35SD; // object of type double
    public $errRate75; // object of type double
    public $errRate75SD; // object of type double
    public $errRate100; // object of type double
    public $errRate100SD; // object of type double
    public $firstCycleIntPF; // object of type double
    public $firstCycleIntPFSD; // object of type double
    public $prcIntensityAfter20CyclesPF; // object of type double
    public $prcIntensityAfter20CyclesPFSD; // object of type double
    
    public function  __construct() {
        parent::__construct('CasavaRunLaneRead');
        $this->casavaRunFlowCellReadId = null; 
        $this->laneId = null; 
        $this->readNumber = null; 
        $this->tileCount = null; 
        $this->clustersRaw = null; 
        $this->clustersRawSD = null; 
        $this->clustersPF = null; 
        $this->clustersPFSD = null; 
        $this->prcPFClusters = null; 
        $this->prcPFClustersSD = null; 
        $this->phasing = null; 
        $this->prephasing = null; 
        $this->calledCyclesMin = null; 
        $this->calledCyclesMax = null; 
        $this->prcAlign = null; 
        $this->prcAlignSD = null; 
        $this->errRatePhiX = null; 
        $this->errRatePhiXSD = null; 
        $this->errRate35 = null; 
        $this->errRate35SD = null; 
        $this->errRate75 = null; 
        $this->errRate75SD = null; 
        $this->errRate100 = null; 
        $this->errRate100SD = null; 
        $this->firstCycleIntPF = null; 
        $this->firstCycleIntPFSD = null; 
        $this->prcIntensityAfter20CyclesPF = null; 
        $this->prcIntensityAfter20CyclesPFSD = null; 
    }
}
?>
