<?php
/**
* Class TableIndex
*
* @author ClassFactory.php genClass
*/
class TableIndexCls extends SpecCls {
    public $name; // object of type string
    public $isUnique; // object of type boolean
    public $columns; // collection of type TableColumns
    
    public function  __construct() {
        parent::__construct('TableIndex');
        $this->name = null; 
        $this->isUnique = null; 
        $this->columns = array(); 
    }
}
?>
