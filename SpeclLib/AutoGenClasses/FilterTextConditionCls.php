<?php
/**
* Class FilterTextCondition
*
* @author ClassFactory.php genClass
*/
class FilterTextConditionCls extends SpecCls {
    public $operator; // object of type string
    public $characters; // object of type string
    
    public function  __construct() {
        parent::__construct('FilterTextCondition');
        $this->operator = null; 
        $this->characters = null; 
    }
}
?>
