<?php
/**
* Class CasavaRunFlowCellRead
*
* @author ClassFactory.php genClass
*/
class CasavaRunFlowCellReadCls extends SpecCls {
    public $casavaRunFlowCellReadId; // object of type integer
    public $casavaRunId; // object of type integer
    public $readNumber; // object of type integer
    public $densityRatio; // object of type double
    public $lanes; // collection of type CasavaRunLaneReads
    
    public function  __construct() {
        parent::__construct('CasavaRunFlowCellRead');
        $this->casavaRunFlowCellReadId = null; 
        $this->casavaRunId = null; 
        $this->readNumber = null; 
        $this->densityRatio = null; 
        $this->lanes = array(); 
    }
}
?>
