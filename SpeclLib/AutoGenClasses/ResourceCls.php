<?php
/**
* Class Resource
*
* @author ClassFactory.php genClass
*/
class ResourceCls extends SpecCls {
    public $resourceId; // object of type string
    public $aco; // object of type string
    public $acu; // object of type string
    public $acr; // object of type string
    public $acf; // object of type string
    public $act; // object of type string
    
    public function  __construct() {
        parent::__construct('Resource');
        $this->resourceId = null; 
        $this->aco = null; 
        $this->acu = null; 
        $this->acr = null; 
        $this->acf = null; 
        $this->act = null; 
    }
}
?>
