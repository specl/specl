<?php
/**
* Class TablePrimaryKey
*
* @author ClassFactory.php genClass
*/
class TablePrimaryKeyCls extends SpecCls {
    public $columns; // collection of type TableColumns
    
    public function  __construct() {
        parent::__construct('TablePrimaryKey');
        $this->columns = array(); 
    }
}
?>
