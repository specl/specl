<?php
/**
* Class TableConstraint
*
* @author ClassFactory.php genClass
*/
class TableConstraintCls extends SpecCls {
    public $addConstraint; // object of type boolean
    public $addCondition; // object of type boolean
    public $enforced; // object of type boolean
    public $columnName; // object of type string
    public $referenceTable; // object of type string
    public $referenceColumn; // object of type string
    public $filter; // object of type string
    
    public function  __construct() {
        parent::__construct('TableConstraint');
        $this->addConstraint = true; 
        $this->addCondition = true; 
        $this->enforced = true; 
        $this->columnName = null; 
        $this->referenceTable = null; 
        $this->referenceColumn = null; 
        $this->filter = null; 
    }
}
?>
