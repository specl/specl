<?php
/**
* Class Connection
*
* @author ClassFactory.php genClass
*/
class ConnectionCls extends SpecCls {
    public $id; // object of type string
    public $user; // object of type string
    public $host; // object of type string
    public $port; // object of type numeric
    public $pass; // object of type string
    public $schema; // object of type string
    public $prefix; // object of type string
    
    public function  __construct() {
        parent::__construct('Connection');
        $this->id = null; 
        $this->user = null; 
        $this->host = null; 
        $this->port = null; 
        $this->pass = null; 
        $this->schema = null; 
        $this->prefix = null; 
    }
}
?>
