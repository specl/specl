<?php
/**
* Class TableColumn
*
* @author ClassFactory.php genClass
*/
class TableColumnCls extends SpecCls {
    public $name; // object of type string
    public $description; // object of type string
    public $friendlyName; // object of type string
    public $fieldComment; // object of type string
    public $columnType; // object of type string
    public $allowNulls; // object of type boolean
    public $isAutoIncrement; // object of type boolean
    public $isAutoGen; // object of type boolean
    public $size; // object of type integer
    public $isUnique; // object of type boolean
    public $defaultValue; // object of type string
    public $constraint; // object of type TableConstraint
    public $conditions; // collection of type Conditions
    public $isSigned; // object of type boolean
    public $referenceTable; // object of type string
    public $referenceColumn; // object of type string
    public $addHardConstraint; // object of type boolean
    public $isPrimaryKey; // object of type boolean
    public $clientValidate; // object of type boolean
    public $autoComplete; // object of type boolean
    public $autoCompleteSource; // object of type string
    public $userGrowable; // object of type boolean
    public $userGrowableACR; // object of type string
    public $isAutoKeyGeneration; // object of type boolean
    public $isCustomAutoIncrement; // object of type string
    public $startingAutoIncrement; // object of type integer
    public $readOnly; // object of type boolean
    public $addBlankChoice; // object of type boolean
    public $linkToReference; // object of type boolean
    public $transformColumn; // object of type boolean
    public $linkedToColumn; // object of type string
    public $actualColumn; // object of type string
    public $ignoredModes; // object of type string
    public $activeModes; // object of type string
    public $compositeTrigger; // object of type boolean
    public $acr; // object of type string
    public $handleAs; // object of type string
    public $xmlTag; // object of type string
    
    public function  __construct() {
        parent::__construct('TableColumn');
        $this->name = null; 
        $this->description = null; 
        $this->friendlyName = null; 
        $this->fieldComment = null; 
        $this->columnType = null; 
        $this->allowNulls = null; 
        $this->isAutoIncrement = null; 
        $this->isAutoGen = null; 
        $this->size = null; 
        $this->isUnique = null; 
        $this->defaultValue = null; 
        $this->constraint = null; 
        $this->conditions = array(); 
        $this->isSigned = null; 
        $this->referenceTable = null; 
        $this->referenceColumn = null; 
        $this->addHardConstraint = null; 
        $this->isPrimaryKey = null; 
        $this->clientValidate = null; 
        $this->autoComplete = null; 
        $this->autoCompleteSource = null; 
        $this->userGrowable = null; 
        $this->userGrowableACR = null; 
        $this->isAutoKeyGeneration = null; 
        $this->isCustomAutoIncrement = null; 
        $this->startingAutoIncrement = null; 
        $this->readOnly = null; 
        $this->addBlankChoice = null; 
        $this->linkToReference = null; 
        $this->transformColumn = null; 
        $this->linkedToColumn = null; 
        $this->actualColumn = null; 
        $this->ignoredModes = null; 
        $this->activeModes = null; 
        $this->compositeTrigger = false; 
        $this->acr = null; 
        $this->handleAs = null; 
        $this->xmlTag = null; 
    }
}
?>
