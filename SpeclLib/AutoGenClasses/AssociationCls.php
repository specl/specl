<?php
/**
* Class Association
*
* @author ClassFactory.php genClass
*/
class AssociationCls extends SpecCls {
    public $name; // object of type string
    public $associationType; // object of type string
    public $minAllowed; // object of type integer
    public $maxAllowed; // object of type integer
    public $displaySubForm; // object of type boolean
    public $column; // object of type string
    public $referenceTable; // object of type string
    public $referenceColumn; // object of type string
    public $associationUniqueKey; // object of type string
    public $caption; // object of type string
    public $description; // object of type string
    public $throughTable; // object of type string
    public $throughColumnFrom; // object of type string
    public $throughColumnTo; // object of type string
    public $selfReferencing; // object of type boolean
    public $cascadeDeleteAllowed; // object of type boolean
    public $xmlTag; // object of type string
    public $collectFromParentXMLNode; // object of type boolean
    public $acr; // object of type string
    public $isComposite; // object of type boolean
    public $compositeTriggerColumn; // object of type string
    
    public function  __construct() {
        parent::__construct('Association');
        $this->name = null; 
        $this->associationType = null; 
        $this->minAllowed = null; 
        $this->maxAllowed = null; 
        $this->displaySubForm = null; 
        $this->column = null; 
        $this->referenceTable = null; 
        $this->referenceColumn = null; 
        $this->associationUniqueKey = null; 
        $this->caption = null; 
        $this->description = null; 
        $this->throughTable = null; 
        $this->throughColumnFrom = null; 
        $this->throughColumnTo = null; 
        $this->selfReferencing = true; 
        $this->cascadeDeleteAllowed = true; 
        $this->xmlTag = null; 
        $this->collectFromParentXMLNode = false; 
        $this->acr = null; 
        $this->isComposite = null; 
        $this->compositeTriggerColumn = null; 
    }
}
?>
