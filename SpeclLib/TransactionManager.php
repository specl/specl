<?php
/**
 * Description of MySQLTransaction
 *
 * This object will be used for nested MySQL transactions.  It will hanbdle teh 
 * commit, rollback handling for nested inserts, updates and cascading deletes.
 * 
 * @author Tony
 */

class TransactionTransformation {
    
    public $parentTranOp; // pointer back to the TransactionOperation
    
    public function __construct($params) {       
        
        if(isset($params["parentTranOp"])){
            $this->parentTranOp=$params["parentTranOp"];
        }
        else{
            throw new Exception("the parameter 'parentTranOp' is requried");
        }        
    }

    public function execute() {          
    }
}

class CopyAutoIncrement extends TransactionTransformation {
    public $tranOp;
    public $to;
    
    public function __construct($params) {  
                        
        parent::__construct($params);
        
        if(isset($params["tranOp"])){
            $this->tranOp=$params["tranOp"];
        } else {
            throw new Exception("the parameter 'tranOp' is requried");
        }
        
        if(isset($params["to"])){
            $this->to=$params["to"];
        } else {
            throw new Exception("the parameter 'to' is requried");            
        }
    }        
          
    public function execute() {                     
        $this->tranOp->objData->{$this->to} = $this->parentTranOp->results->autoIncrementKey;                
    }        
}

class TransactionOperation {
    
    public $dal;
    public $objDef;
    public $objData;
    public $results; // for inserts this may store the AutoIncrement key, useful
    // when doing nested inserts that depend on a the AutoIncrement key of a parent
    // table
    public $transactionId;     
    public $transformations;  // an array of subclassed TransactionTransformation objects
    
    public function __construct($transactionId, $dal, $objDef, $objData) {                
        $this->dal = $dal;
        $this->objDef = $objDef;
        $this->objData = $objData;
        $this->results = null;
        $this->transactionId = $transactionId;
        $this->transformations = array();        
    }    
    
    public function execute( ) {                
    }
    
    public function getTransactionId() {
        return $this->transactionId;
    }
    
    public function addTransformation($transactionTransformation) {
         $this->transformations[] = $transactionTransformation;
    }
}

class InsertTransactionOperation extends TransactionOperation {
    
    public function __construct($transactionId, $dal, $objDef, $objData) {                
        parent::__construct($transactionId, $dal, $objDef, $objData);
    }    
    
    public function execute( ) {            
        
        foreach ($this->transformations as $instruction) {                
            $instruction->execute();
        }
        
        $this->results = $this->dal->insert($this->objData);        
        return $this->results;        
    }    
}

class DeleteTransactionOperation extends TransactionOperation {
    
    public function __construct($transactionId, $dal, $objDef, $objData) {                
        parent::__construct($transactionId, $dal, $objDef, $objData);
    }    
    
    public function execute( ) {
        $this->results = $this->dal->delete($this->objData);        
        return $this->results;           
    }    
}

class UpdateTransactionOperation extends TransactionOperation {
    
    public $oldObjData;
    public $newObjData;
        
    public function __construct($transactionId, $dal, $objDef, $oldObjData, $newObjData) {                
        parent::__construct($transactionId, $dal, $objDef, $oldObjData);
        $this->oldObjData = $oldObjData;
        $this->newObjData = $newObjData;
    }    
    
    public function execute( ) {
        $this->results = $this->dal->update($this->oldObjData, $this->newObjData);        
        return $this->results;
    }    
}

class InsertSQLTransactionOperation extends TransactionOperation {
    public $sql;
    public $dbConn;
    
    public function __construct($transactionId,$dbConn, $sql) {
        $this->transactionId=$transactionId;
        $this->dbConn = $dbConn;
        $this->sql = $sql;
    }    
    
    public function execute( ) {
        $this->results = $this->dbConn->insert($this->sql);        
        return $this->results;           
    }    
}

class UpdateSQLTransactionOperation extends TransactionOperation {
    
    public $sql;
    public $dbConn;
    
    public function __construct($transactionId,$dbConn, $sql) {
        $this->transactionId=$transactionId;
        $this->dbConn = $dbConn;
        $this->sql = $sql;
    }    
    
    public function execute( ) {
        
        try {
            $status = $this->dbConn->update($this->sql);                
            $results = new DALResults(true, "update", null,null,null, null);            
        } catch(Exception $e) {
            $message=$e->getMessage();            
            $results = new DALResults(false, "update", "SQL failed",$message,null,null);            
        }      
        
        return $results;           
    }    
}

class DeleteSQLTransactionOperation extends TransactionOperation {
    public $sql;
    public $dbConn;
    
    public function __construct($transactionId,$dbConn, $sql) {
        $this->transactionId=$transactionId;
        $this->dbConn = $dbConn;
        $this->sql = $sql;
    }    
    
    public function execute( ) {
        $this->results = $this->dbConn->delete($this->sql);        
        return $this->results;           
    }    
}

class ChangeForeignKeyConstraintSetting extends TransactionOperation{
    public $transMan;
    public $disable;
    
    public function __construct($transMan,$disable=true){
        $this->transMan=$transMan;
        $this->disable=$disable;
    }
    
    public function execute(){
        if($this->disable){
            $SQL="SET foreign_key_checks=0;";
            $this->transMan->foreignKeyConstraintDisabled=true;
        }
        else{
            $SQL="SET foreign_key_checks=1;";
            $this->transMan->foreignKeyConstraintDisabled=false;
        }   
        
        try {
            $this->transMan->db->update($SQL);              
            $results = new DALResults(true, "update", null,null,null, null);            
        } catch(Exception $e) {
            $message=$e->getMessage();            
            $results = new DALResults(false, "update", "SQL failed",$message,null,null);            
        } 
        return $results;   
    }
}

class TransactionManager {

    public $transactions; // children MySQLTransaction(s)
    public $db; // database connection class
    public $autoIncrement;
    // uniqueConstariantDisabled set to true we the unique constraint is disabled in the transaction
    // this is need because if the transaction fails and the uniqueConstraint is still disable, then
    // the uniqueConstraint must be reenabled in the database.
    public $foreignKeyConstraintDisabled;
    
    public function __construct($db) {        
        $this->db = $db;
        $this->transactions = array();
        $this->autoIncrement = array();
        $this->foreignKeyConstraintDisabled=false;
    }
    
    public function addInsert($dal, $objDef, $dataObj) {
        $transactionId = count($this->transactions);
        $op = new InsertTransactionOperation($transactionId, $dal, $objDef, $dataObj);
        $this->transactions[] = $op;
        
        return $op;
    }

    public function addUpdate($dal, $objDef, $oldDataObj, $newDataObj) {
        $transactionId = count($this->transactions);
        $op = new UpdateTransactionOperation($transactionId, $dal, $objDef,$oldDataObj, $newDataObj);
        $this->transactions[] = $op;        
        
        return $op;
    }
    
    public function addDelete($dal, $objDef, $dataObj) {
        $transactionId = count($this->transactions);
        $op = new DeleteTransactionOperation($transactionId, $dal, $objDef, $dataObj);
        $this->transactions[] = $op;        
        
        return $op;
    }
    
    public function addSQLInsert($dbConn, $sql) {
        $transactionId = count($this->transactions);
        $op = new InsertSQLTransactionOperation($transactionId, $dbConn, $sql);
        $this->transactions[] = $op;
        
        return $op;
    }

    public function addSQLUpdate($dbConn, $sql) {
        $transactionId = count($this->transactions);
        $op = new UpdateSQLTransactionOperation($transactionId, $dbConn, $sql);
        $this->transactions[] = $op;       
        
        return $op;
    }
    
    public function addSQLDelete($dbConn, $sql) {
        $transactionId = count($this->transactions);
        $op = new DeleteSQLTransactionOperation($transactionId, $dbConn, $sql);
        $this->transactions[] = $op;        
        
        return $op;
    }
    
    public function disableForeignKeyConstraint(){
        $op=new ChangeForeignKeyConstraintSetting($this);
        $this->transactions[]=$op;
        return $op;
    }
    
    public function enableForeignKeyConstraint(){
        $op=new ChangeForeignKeyConstraintSetting($this,false);
        $this->transactions[]=$op;
        return $op;
    }
    
    public function clearTransactions(){
        $this->transactions=array();
    }
    
    public function execute() {
        
        // first, is there any work to ?
       
        if (count($this->transactions) == 0) {
            $results = new DALResults(true, "transaction", null,null,null,null);            
            return $results; // just return.
        }        

        // begin the transaction 
        $this->db->begin();
        
        $transactionSuccess = true;
        foreach($this->transactions as $transaction) { 
          
            // $transaction (These are all subclassed from TransactionOperation)
                       
            $results = $transaction->execute();
            
            if ($results->success == false) {
                $transactionSuccess = false;
                //failure, roll back everything                
                break;
            }
        }
        
        // enable unique constraint setting if it has been disable by the transaction and
        // has not be reenable. This can be due to an error in the transaction or user error
        // forgetting to add a step to reenable the constraint.
        if($this->foreignKeyConstraintDisabled===true){
            $op=new ChangeForeignKeyConstraintSetting($this,false);
            $op->execute();
        }
        
        if ($transactionSuccess == true) {
            // commit transaction
            $this->db->commit();
            $results = new DALResults(true, "transaction", null,null,null,null);        
        } else {
            // rollback transaction   
            $this->db->rollback();
            
            $errorMessage="";
            foreach($this->transactions as $transaction) {
                if (!is_null($transaction->results)) {                    
                    if ($transaction->results->success == false) {
                        $errorMessage .= $transaction->results->errorMessage;
                        $errorMessage .= "<br/>";
                    }
                }
            }
            $results = new DALResults(false, "transaction", "transaction failed",$errorMessage,null,null);            
        }            
        return $results;
    }           
}
?>
