<?php
/*! \brief Manages SpecDefs
 * 
 * Finds and loads SpecDef definitions from XML, PHP or both and merges them into
 * a single SpecDef which is persisted in memory.
 */
class SpecManager {
    private $specDefs; //!< array of SpecDef
    private $objDefs;
    private $propDefs; //!< array of PropDef
    private $validationErrors;
    private $ranValidation;
    private $crossChecked;
    private $mode;   
    public $xmlParser; //!< a XMLParser
    private $cf;  //!< a ClassFactory        
    
    public $autoGenClasses; //!< boolean, default true, used by ClassFactory, check whether to generated a SpecCls class for a SpecDef
    public $autoGenClassPath; //!< string, default \AutoGenClasses\, path name where AutoGenClasses will be saved, used by ClassFactory, to save a SpecCls
    public $learnMode; //!< boolean, not used
    
    //! Constructor
    public function  __construct() {
        $this->autoGenClasses = true;
        $this->autoGenClassPath = dirname(__FILE__) ."/AutoGenClasses/";
        $this->learnMode = false;                
        $this->specDefs = array();
    }

    public function getSpecDefs() {
        return $this->specDefs;
    }

    public function getObjDefs() {
        return $this->objDefs;
    }
    
    public function setAutoGenClasses($value) {
        $this->autoGenClassPath = true;
    }

    public function autoGenClassPath($value) {
        $this->autoGenClassPath = true;
    }
    
    // comments go here
    public function init() {
        global $log;
        
        $this->xmlParser = new XMLParser();       
        $this->cf = new ClassFactory($this);
        
        $this->objDefs = array();
        $this->propDefs = array();

        $this->crossChecked=false;
        $this->clearValidationErrors();

        $objDef = new ObjDef("auto");
        $objDef->setIsScalar(true);
        $this->addDef($objDef);

        $objDef = new ObjDef("string");
        $objDef->setIsScalar(true);
        $this->addDef($objDef);

        $objDef = new ObjDef("date");
        $objDef->setIsScalar(true);
        $this->addDef($objDef);

        $objDef = new ObjDef("datetime");
        $objDef->setIsScalar(true);
        $this->addDef($objDef);

        
        $objDef = new ObjDef("boolean");
        $objDef->setIsScalar(true);
        $this->addDef($objDef);

        $objDef = new ObjDef("integer");
        $objDef->setIsScalar(true);
        $this->addDef($objDef);

        $objDef = new ObjDef("float");
        $objDef->setIsScalar(true);
        $this->addDef($objDef);

        $objDef = new ObjDef("double");  /* is this an alias for float ??*/
        $objDef->setIsScalar(true);
        $this->addDef($objDef);

        $objDef = new ObjDef("numeric");
        $objDef->setIsScalar(true);
        $this->addDef($objDef);

        $objDef = new ObjDef("dictionary");
        $objDef->setIsScalar(true);
        $this->addDef($objDef);

        $objDef= new ObjDef("spec");
        $this->addDef($objDef);

        $this->loadSpecDef("ObjDef");
        //$this->loadSpecDef("PropDef");
        $this->loadSpecDef("CollectionDef");
        
        //$this->loadSpecDef("Config"); //realy part of Zones
        //$this->loadSpecDef("Session"); //realy part of Zones
        //$this->loadSpecDef("NotEmptyCondition");

        try {
            $this->crossCheck();
        }  catch (Exception $exception) {
            $log->err("SpecManager->__construct",'crossCheck error ' . $exception->getMessage);
        }
    }

    public function loadSpecDef($specDefName) {
        $this->import($specDefName);
    }

    public function loadSpecDefExtra($specDefName) {
        $this->importExtra($specDefName);
    }

    public function registerSpec($name) {
        $def = $this->findDef($name);

        if (is_null($def)) {
            $this->import($name);
        } else {
            // name already registered
        }
    }
 
    public function import($defName, $type="ObjDef") {

        $loaded = false;
        if (array_key_exists(strtolower($defName), $this->objDefs)==false) {

            $loaded = $this->importFromPHP($defName);

            if ($loaded == false) {
                $loaded = $this->importFromXML($defName, $type);
            }

            if ($loaded) {
                $this->crossCheck();
            }
        }

        return $loaded;
    }
    
    public function saveSpecDef($defName, $def) {        
        if (array_key_exists($defName, $this->specDefs)===false) {
            $this->specDefs[$defName] = $def;        
        }
    }
    
    public function getSpecDef($defName) {        
        if (array_key_exists($defName, $this->specDefs)===true) {
            return $this->specDefs[$defName];        
        } else {
            return null;
        }
    }

    public function isLoaded($defName) {        
        if (array_key_exists($defName, $this->specDefs)===true) {
            return true;        
        } else {
            return false;
        }
    }    
    
    public function importFromPHP($defName) {

        $className = $defName . "Spec";

        if (autoloadSearch($className))
        {            
            $def = new $className();
            $def->defSpec($this);
            $this->crossCheck();            
            $this->saveSpecDef($defName, $def);            
            return true;
        } else {
            return false;
        }
    }

    public function importFromXML($defName, $type="ObjDef") {
        
        $filename = "./AppSpecs/".$defName."Spec.xml";
        
        if (file_exists ($filename)==True) {

            $tmp = $this->getObjectFromXML("./AppSpecs/".$defName."Spec.xml",$type);

            if ($tmp) {
                $this->addDef($tmp);
                $this->crossCheck();
                return true;
            } else {
                return false;
            }
        } else {
            return False;
        }
    }


    public function importExtra($defName) {

        $defName = $defName . "SpecExtra";

        if (array_key_exists(strtolower($defName), $this->objDefs)==false) {
            if (autoloadSearch($defName)) {
                $def = new $defName();
                $def->augment($this);
                $this->crossCheck();
            }
        }
    }

    public function addDef($defObj) {
        global $log;

        if (isnull($defObj)) {
            throw new Exception('addDef passed a NULL');
        }
        
        if (!isset($defObj->id)) {
            throw new Exception('addDef passed a NULL');
        }
        
        $key = strtolower($defObj->id);
        $key = str_replace(":","_",$key); // XXX

        if (array_key_exists($key, $this->objDefs)==false) {
            $this->objDefs[$key] = $defObj;
        } else {
            $log->warn("SpecDef","Warning: {$key} An ObjDef of the same id already exists.");
        }                
    }
    
    public function addCompositeDef($compositeDef){
        $key=strtolower($compositeDef->name);
        if($key===null){
            throw new Exception("Error: name has not been set for the CompositeObjDef");
        }
        if(array_key_exists($key, $this->objDefs)===false){
            $this->objDefs[$key]=$compositeDef;
        }
        else{
            throw new Exception("Error: The def {$this->compositeDef->name} is already defined");
        }
    }
   
    public function findDef($name, $loadIfMissing=true) {
        
        // remove the objDef from the cache to get a new copy of the object each time
        //unset($this->objDefs[$name]);
                
        $key = str_replace(":","_",$name); // XXX
        
        $key = strtolower($key);
        if (array_key_exists($key, $this->objDefs)) {
            return $this->objDefs[$key];             
        } else {
            if ($loadIfMissing) {
                if ($this->import($name))
                {
                    return $this->objDefs[$key];                    
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    public function addPropDef($propDef) {
        $key = strtolower($propDef->id);

        if (array_key_exists($key, $this->propDefs)==false) {
            $this->propDefs[$key] = $propDef;
        }
    }

    public function findPropDef($key) {
        $key = strtolower($key);
        if (array_key_exists($key, $this->propDefs)) {
            return $this->propDefs[$key];
        } else {
           return null;
        }
    }

    public function linkPropDefs($objDef) {
        global $log;

        foreach($objDef->getProperties() as $key=>$propDef) {

            if ($propDef->link == true) {

                $linkPropDef = $this->findPropDef($propDef->id);

                if ($linkPropDef) {
                    $objDef->removeProperty($propDef);

                    $objDef->addProperty($linkPropDef);
                } else {
                    // TODO: Handle this
                    // propDef could not be found, this is a serious error.
                    $msg="link for objDef {$objDef->id} propDef {$propDef->id} not found.";
                    $log->error("SpecManager->linkPropDefs",$msg);
                }
            }
        }
    }
    
    // This function determines whether an object is a scalar type or not, based on
    // the based types. It used to also preload all of the object and collections specs,
    // but that is not neccessary and slows things down.
    public function crossCheckObjDef($defObj) {
        if ($defObj->crossChecked == false) {
            $defObj->crossChecked = true;
            if (is_a($defObj, "ObjDef")) {
                foreach($defObj->getProperties() as $key=>$prop) {

//                    if ($prop->objectType==null) {
//                        $a = false;
//                    }

                    if ($prop->propType == "object") {
                        if (array_key_exists($prop->objectType, $this->objDefs)) {
                            $propDefObj = $this->objDefs[$prop->objectType];
                            if ($propDefObj->isScalar()==True) {
                                $prop->setIsScalar(true);
                            }
                        }
//                        else {
//                            // ok it was not found...is it string, boolean, date, numeric?
//                            //die("{$prop->objectType} not found");
//                            $this->import($prop->objectType);
//                        }
                    } 
//                    else if ($prop->propType == "collection") {
//                        if (array_key_exists($prop->objectType, $this->objDefs)==false) {
//                            $this->import($prop->objectTypeOrig, "CollectionDef");
//                        }
//                    }
                    
                    $prop->crossChecked = true;
                }
            } 
//            else if (is_a($defObj, "CollectionDef")) {
//                foreach($defObj->childTypes as $key=>$childType) {
//                    // Make sure the type exists and it is either a scalar or an object def
//                    if (is_string($childType))
//                    {
//                        if (array_key_exists($key, $this->objDefs)==false) {
//                            $this->import($childType);
//                        }
//
//                        if (array_key_exists($key, $this->objDefs)) {                             
//                            $defObj->childTypes[$key] = $this->objDefs[$key];
//                        } else {
//                            $msg="The Spec Definition for $childType does not exist";
//                            //$log->error("SpecManager->crossCheck",$msg);                            
//                            throw (new Exception($msg));
//                        }
//                    }
//                }
//            }
        }
    }
    

    public function crossCheck() {     
        foreach($this->objDefs as $defObj) { 
            // do not crosscheck CompositeObjDefs.
            if(is_a($defObj,"BaseDef")){
                $this->crossCheckObjDef($defObj);
            }
        }

        $this->crossChecked=true;
    }           
    
    // returns true if valid, or an array or errors if not
    public function validate($data, $mode="any") {

        $this->mode = $mode;
        
        $this->clearValidationErrors();

        $this->_validate($data);

        if ($this->isValid()) {
            return true;
        } else {
            return $this->validationErrors;
        }
    }

    private function _validate($data)
    {
        if($this->isCrossChecked()==false)
        {
            throw new Exception("UnChecked Spec: A spec must be cross checked before validation.");
        }

        //validate top level data
        // TODO: Fix code so you don't need recurse option.
        $this->validateData("toplevel",$data,$recurse=false);
        $this->validateChildren($data);
    }

    public function validateChildren($data)
    {
        foreach($data as $propId=>$propValue)
        {
            if(is_array($propValue) == true)
            {
                //$log->info("SpecDef->validateChildren","Validating property {$propId}");
                $results = $this->ValidateArray($propId,$propValue);
            }
        }

        $this->ranValidation=true;
    }

    public function validateData($propertyId,$propertyArray,$recurse=true)
    {
        global $log;
        # check for spec
        # TODO: Handle the errors that check for missing object defs and missing __type__.

        $spec=null;
        $type=null;

        # All data messages must have a type property
        if(array_key_exists("__type__",$propertyArray)==false) {
            if($propertyId=="data") {
                $this->_validate($propertyArray);
                return;
            } else {
                $msg="__type__ missing for {$propertyId}";
                $log->error("SpecDef->validateData",$msg);
                $validationError=new ValidationError("SPECL",$propertyId,"typedef",$propertyArray["__path__"],"Missing __type__ property.",10,1);
                $this->validationErrors[]=$validationError;
            }
        }
        else
        {
            $type=$propertyArray["__type__"];
            $spec=$this->findDef($type);

            if($spec==null) {
                $msg="SpecDef missing for type: {$type}";
                $log->error("SpecDef->validateData",$msg);
                $validationError=new ValidationError("SPECL",$type,"specdef",$propertyArray["__path__"],"Missing Spec Def",10,2);
            } else {

                if(is_a($spec,"ObjDef"))
                {
                    $propertyData=array();

                    //TODO validation on nested array data (if necessary)

                    foreach($propertyArray as $key=>$value)
                    {
                        if (($key == '__type__') || ($key == '__defType__') || ($key == '__path__') ) {
                            continue;
                        }

                        if(is_array($value)==false)
                        {
                            $propertyData[$key]=$value;
                        }
                        else
                        {
                            $propertyData[$key]="nested";
                            $this->validateData($key,$value);
                        }

                    }

                    $spec->setCurrentPath($propertyArray["__path__"]);
                    $spec->validate($propertyData, $this->mode);

                    if($spec->isValid()==false)
                    {
                        foreach($spec->validationErrors as $error)
                        {
                            $error->parentName=$propertyId;
                        }
                        $this->validationErrors=array_merge($this->validationErrors,$spec->validationErrors);
                        $spec->clearValidationErrors();
                    }
                }

                else if(is_a($spec,"CollectionDef"))
                {
                    // TODO: Add Validation for collections
                }

            }
        }
        if($recurse)
        {
            $this->validateChildren($propertyArray);
        }
    }

    public function isValid()
    {
        global $log;
        
        if($this->ranValidation) {
            if(count($this->validationErrors)==0) {
                return true;
            } else {
                return false;
            }
        }
        else {
            # TODO: Maybe this should be an error instead of a warning
            $log->warn("SpecDef->isValid","Is Valid Has been Called without running validation first.");
            return false;
        }
    }

    public function isCrossChecked()
    {
        if($this->crossChecked==true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function clearValidationErrors()
    {
        $this->validationErrors=array();
        $this->ranValidation=false;
    }

    public function clearCrossChecked()
    {
        $this->crossChecked=false;
    }
    
    public function getObjectFromXML($filename) {
        
        //TODO:Error checking, does file exist?

        //TODO:Error checking, add try catch block
        $this->xmlParser->parseFile($this, $filename);
        
        $data = $this->xmlParser->getDataFromRoot();
        
        $keys = array_keys($data);
        
        if(count($keys)>0){        
            $objectDefName = $keys[0];
        }else{
            throw new Exception("getObjectFromXML failed");
        }
        
        $data = $this->xmlParser->getDataByKey($objectDefName);
        
        //TODO:Error checking, add try catch block
        $obj = $this->cf->create($data, $objectDefName);

        //TODO:Error checking, check for null $obj

        return($obj);
    }

    
    public function saveToXML($object) {
        return $this->cf->saveToXML($object);
    }

    public function createClass($objDef) {
        return $this->cf->createClass($objDef);
    }

    
    public function getObjectFromJSON($json, $objectDefName) {

        $json = str_replace("'", '"', $json);

        $jsonObj = json_decode($json,$assoc=True);

        $jsonParser = new JsonParser();

        //TODO:Error checking, add try catch block
        $jsonParser->parse($this->specMgr, $jsonObj, $objectDefName);

        //TODO:Error checking, add try catch block
        $data = $jsonParser->getData();

        $obj = $this->createObjectFromData($data);

        return($obj);
    }

    public function createObjectFromData($data) {
        $obj = $this->cf->create($data);
        return $obj;
    }

    public function getDataFromForm($formObj) {

        $formParser = new FormParser();

        //TODO:Error checking, add try catch block
        $formParser->parseForm($this->specMgr, $formObj);

        //TODO:Error checking, add try catch block
        $data = $formParser->getData();

        return($data);
    }    
    
    public function traverseXML($node, $domain) {
        
        if ($node->nodeType == XML_TEXT_NODE) {
            return;
        }
                        
        $this->addIfMissingObjDef($node, $domain);

        if ($node->hasAttributes()==True) {
            foreach($node->attributes as $name => $attrNode) {
                $this->addIfMissingPropDefFromAttribute($node, $name);                                 
            }
        }
        
        if ($node->hasChildNodes()) {
            foreach ($node->childNodes as $childNode) {
                                
                if ($childNode->nodeType == XML_ELEMENT_NODE) {
              
                    if ($childNode->hasChildNodes()) {

                        if ($childNode->childNodes->length==1) {
                            if ($childNode->childNodes->item(0)->nodeType==XML_TEXT_NODE) {
                                $this->addIfMissingPropDef($node, $childNode, "string"); 
                            } else {
                                $this->addIfMissingObjDef($childNode, $domain);                         
                                $this->traverseXML($childNode, $domain);                                                                 
                                $this->addIfMissingPropDef($node, $childNode, $childNode->tagName);                                 
                            }
                        } else {                 
                            $this->addIfMissingObjDef($childNode, $domain);                         
                            $this->traverseXML($childNode, $domain);                            
                            $this->addIfMissingPropDef($node, $childNode, $childNode->tagName); 
                        }

                    } else {
                        $this->addIfMissingPropDef($node, $childNode, "string"); 
                    }                                                 
                    
                    
                } else  if ($childNode->nodeType == XML_TEXT_NODE) { 
                    //print $childNode->wholeText ."\n";
                } else {
                    print $childNode->nodeType ."\n";
                }
            }
        }        
    }
    
    
    public function addIfMissingPropDefFromAttribute($node, $name) {
 
        $objDef = $this->findDef($node->tagName);
        
        if (is_null($objDef)) {
            return;
        }
        
        if ($objDef->isCollection()) {
            return;
        }
       
        if ($objDef->isProperty($name)==False) {                
            $prop = $objDef->createProperty($name, "string");
        } 
    }
    
    public function addIfMissingPropDef($node, $childNode, $type, $isCollection=False) {
        
        $objDef = $this->findDef($node->tagName);
        
        if (is_null($objDef)) {
            return;
        }
        
        if ($objDef->isCollection()) {
            return;
        }
        
        $typeObjDef = $this->findDef($type);
        
        
        if ($typeObjDef) {
            
            if ($typeObjDef->isCollection()) {
                $prop = $objDef->createCollection($childNode->tagName, $type);
            } else {
                $prop = $objDef->createProperty($childNode->tagName, $type);                
            }
        } else {        
            if ($objDef->isProperty($childNode->tagName)==False) {                
                $prop = $objDef->createProperty($childNode->tagName, $type);
            } 
        }
    }
    
    
    public function addIfMissingObjDef($node, $domain) {
        
        $def = $this->findDef($node->tagName);

        if (is_null($def)) {
            $this->addObjDef($node->tagName, $domain);
        }       
    }
    
    public function addObjDef($name, $domain) {
        
        $name = str_replace(":","_",$name); // XXX
        
        $pb = new ParamBuilder();
        $pb->add("id", $name);
        $pb->add("domain", $domain);

        $objDef = new ObjDef($pb->getParams());
                    
        $this->addDef($objDef);    
    }
    
    public function learnSpecFromXML($filename, $domain) {
        
        //$xml = new SimpleXMLElement($filename);
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->Load($filename);        
        $root = $dom->documentElement;               
        $this->traverseXML($root, $domain);
    }
       
    // save all the ObjDefs of a specific domain.
    public function genSpecDefs($pathToSave, $domain) {                
        foreach($this->objDefs as $objDef) {
            
            if ($objDef->domain == $domain) {                            
                $filename = $pathToSave.$objDef->name."BaseSpec.php";                                            
                $text = $this->saveBaseSpecObjDef($objDef);   
                file_put_contents($filename, $text);    
                
                $filename = $pathToSave.$objDef->name."Spec.php";                                            
                $text = $this->saveObjDef($objDef);   
                file_put_contents($filename, $text);    
                
            }
        }
    }
    
    public function saveObjDef($objDef) {
        $sb = new StringBuilder();
        
        if ($objDef->isCollection()==True) {
            $this->saveCollection($sb, $objDef);
        } else {
            $this->saveObj($sb, $objDef);
        }        
        return $sb->getString();
    }
    
    
    public function saveBaseSpecObjDef($objDef) {
        $sb = new StringBuilder();
        
        if ($objDef->isCollection()==True) {
            $this->saveBaseSpecCollection($sb, $objDef);
        } else {
            $this->saveBaseSpecObj($sb, $objDef);
        }        
        return $sb->getString();
    }
    
    public function saveBaseSpecObj($sb, $objDef) {   
        $sb->addLine("<?php");
        $sb->addLine("class ".$objDef->name."BaseSpec extends SpecDef {");
        
        $sb->increaseIndentLevel();
        $sb->addLine('',true);   
        
        foreach($objDef->getProperties() as $propDef) {
            $sb->addLine("//{$propDef->name}  {$propDef->propType}");    
        }
        $sb->addLine('',true);   
        
        $sb->addLine('public function __construct() {');
        $sb->increaseIndentLevel();
        $sb->addLine('parent::__construct("'.$objDef->name.'");');
        $sb->decreaseIndentLevel();
        $sb->addLine('}');
        $sb->addLine('');        
                
        $sb->addLine('public function defSpec($spec) {');
        $sb->increaseIndentLevel();
        $sb->addLine('$objDef = new ObjDef($this->objDefId);');
        
        $isBlob = false;
        
        foreach($objDef->attributes as $mode=>$mode_attributes) {                        
            foreach($mode_attributes as $attribute) {            
                 $sb->addLine('$objDef->setAttribute'."(\"{$attribute->key}\", \"{$attribute->value}\", \"{$mode}\");");
            }            
        }
        $sb->addLine('');  

        foreach($objDef->getProperties() as $propDef) {
            
            if ($propDef->isCollection()) {
                $sb->addLine('$prop = $objDef->createCollection("'.$propDef->name.'", "'.$propDef->objectType.'");');
            }
            else {
                $sb->addLine('$prop = $objDef->createProperty("'.$propDef->name.'", "'.$propDef->objectType.'");');
            }
            
            foreach($propDef->attributes as $mode=>$mode_attributes) {                        
                foreach($mode_attributes as $attribute) {            
                     $sb->addLine('$objDef->setAttribute'."(\"{$attribute->key}\", \"{$attribute->value}\", \"{$mode}\");");
                }            
            }
            $sb->addLine('');  
            
            foreach($propDef->conditions as $mode=>$mode_conditions) {
                foreach($mode_conditions as $condition) {            
                    print $condition;
                }
            }
            
            
//        $prop->addCondition(new NotEmptyCondition());
//        $prop->addCondition(new StringMaxLengthCondition(32));
//        $pb = new ParamBuilder();
//        $pb->add("connectionName", "seqlims");
//        $pb->add("fkTable", "ACROF");
//        $pb->add("fkColumn", "acf");
//        $pb->add("exists", false);
//        $pb->add("errMsg", "Because of a foreign key dependency in table ACROF the value can not be changed or deleted.");
//        $prop->addCondition(new FKLookupCondition($pb->getParams()), array("edit","delete"));
//        $objDef->addKey("acf");
        
            
        }
        $sb->addLine('');  
        $sb->addLine('$this->objDef = $objDef;');
        $sb->addLine('$spec->addDef($objDef);');
        
        
        $sb->decreaseIndentLevel();
        $sb->addLine("}");

        $sb->decreaseIndentLevel();

        $sb->addLine("}");       
        $sb->addLine("?>");        
        
        return $sb->getString();
    }    
        
    public function saveBaseSpecCollection($sb, $objDef) {   
        

        if ($objDef->isAssociative) {
            $isAssociative = "true";
        } else {
            $isAssociative = "false";
        }
        
        $sb->addLine("<?php");
        $sb->addLine("class ".$objDef->name."BaseSpec extends SpecDef {");
        
        $sb->increaseIndentLevel();
        $sb->addLine('',true);   
        
        
        $sb->addLine('public function __construct() {');
        $sb->increaseIndentLevel();
        $sb->addLine('parent::__construct("'.$objDef->name.'");');
        $sb->decreaseIndentLevel();
        $sb->addLine('}');
        $sb->addLine('');        
                
        $sb->addLine('public function defSpec($spec) {');
        $sb->increaseIndentLevel();
        
        $sb->addLine('$colDef = new CollectionDef("'.$objDef->name.'", '.$isAssociative.');');
        
        foreach($objDef->childTypes as $childType) {        
            $sb->addLine('$colDef->addChildType("'.$childType->name.'");');
        }
        
        if ($objDef->isAssociative) {
            $sb->addLine('$colDef->setKeyId("'.$objDef->id.'");');
        }
        
        $sb->addLine('$spec->addDef($colDef);');
        
        $sb->decreaseIndentLevel();
        $sb->addLine("}");

        $sb->decreaseIndentLevel();

        $sb->addLine("}");       
        $sb->addLine("?>");        
        
        return $sb->getString();
    }        
    
    
    
    public function saveObj($sb, $objDef) {   
        $sb->addLine("<?php");
        $sb->addLine("class ".$objDef->name."Spec extends ".$objDef->name."BaseSpec {");
        
        $sb->increaseIndentLevel();
        $sb->addLine('',true);   
                
        $sb->addLine('public function __construct() {');
        $sb->increaseIndentLevel();
        $sb->addLine('parent::__construct();');
        $sb->decreaseIndentLevel();
        $sb->addLine('}');
        $sb->addLine('');        
                
        $sb->decreaseIndentLevel();

        $sb->addLine("}");       
        $sb->addLine("?>");        
        
        return $sb->getString();
    }    
        
    public function saveCollection($sb, $objDef) {   
        
        if ($objDef->isAssociative) {
            $isAssociative = "true";
        } else {
            $isAssociative = "false";
        }
        
        $sb->addLine("<?php");
        $sb->addLine("class ".$objDef->name."Spec extends ".$objDef->name."BaseSpec {");
        $sb->increaseIndentLevel();
        $sb->addLine('',true);   
        
        $sb->addLine('public function __construct() {');
        $sb->increaseIndentLevel();
        $sb->addLine('parent::__construct();');
        $sb->decreaseIndentLevel();
        $sb->addLine('}');
        $sb->addLine('');        

        $sb->decreaseIndentLevel();

        $sb->addLine("}");       
        $sb->addLine("?>");        
        
        return $sb->getString();
    }           
}
?>