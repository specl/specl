<?php
function CatchAllException($exception) {
    global $log;

    $errorMsg = 'Error on line '.$exception->getLine().' in '.$exception->getFile(). ' '.$exception->getMessage();
    $log->info("CatchAllException", $errorMsg);
}

/**
 * Description of CatchAll
 *
 * @author Tony
 */
class CatchAll {

    public function __construct() {
        set_exception_handler('CatchAllException');
    }

}
?>
