<?php
/**
 * Description of CallFunctionAction
 *
 * @author Tony
 */
class CallFunctionAction {

    public $method;
    public $onSuccess;
    public $onFailure;
    public $onRetryLimitExceeded;

    public function  __construct() {
        $this->method = null;
        $this->onSuccess = null;
        $this->onFailure = null;
        $this->onRetryLimitExceeded = null;
    }

    public function call($formObj) {
        
        if ($this->method) {

            if ($this->method->class) {
                $cls = new $this->method->class();
            }
            else {
                return false;
            }

            try {
                $result = $cls->{$this->method->function}($formObj);
                return $result;
            } catch (Exception $e) {
                 echo 'Caught exception: ',  $e->getMessage(), "\n";
                 return false;
            }

        }
        return false;
    }

}
?>
