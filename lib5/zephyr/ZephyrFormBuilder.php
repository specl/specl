<?php
namespace ZephyrFormBuilder;
/**
 *  The module contains functions needed to build zephyr forms based on a
 *  spec. It does not actually build the form for you, that should be done seperately,
 *  because there are many different ways to build a form.
 */

function shouldRenderField($propDef,$mode){ 
    if($propDef->getAttribute("visible",false,$mode)){
        return false;
    }
    if($propDef->getAttribute("ignore",false,$mode)){
        return false;
    }
    return true;
}

function isFieldHidden($propDef,$mode){
    if($mode==="add" && $propDef->getAttribute("autoIncrement",false,$mode)){        
        return true;        
    }
    if($propDef->getAttribute("hidden",false,$mode)){
        return true;
    }          
    
    return false;
}

/*
 * A field is readOnly under the following conditions:
 *      - the readOnly attribute is set to true
 *      - the mode is view.
 */
function isFieldReadOnly($propDef,$mode){
    if($mode==="view"){
        return true;
    }
    if($propDef->getAttribute("readOnly",false,$mode)){
        return true;
    }
    if($mode!=="add" && $propDef->getAttribute("autoIncrement",false,$mode)){
        return true;
    }
    return false;
}


function isFieldDropDown($propDef,$mode){
    if(!$propDef->hasConstraint()){
        return false;
    }
    $constraint=$propDef->getConstraint();
    $constraint->generate($mode);
    $numoptions=$constraint->count();
    if($numoptions>20){
        return false;
    }
    return true;
}

function isFieldAutoComplete($propDef,$mode){
    if(!$propDef->hasConstraint()){
        return false;
    }
    $constraint=$propDef->getConstraint();
    $constraint->generate($mode);
    $numoptions=$constraint->count();
    if($numoptions<=20){
        return false;
    }
    return true;
}

function isFieldTextEntry($propDef,$mode){
    if(in_array($propDef->objectType, array("auto","integer","double","float"))){
        return true;
    }
    if($propDef->objectType==="string" && !isFieldTextArea($propDef,$mode)){
        return true;
    }
    return false;
}

function isFieldTextArea($propDef,$mode){
    if($propDef->objectType==="string" && 
        ($propDef->getAttribute("columnType",null,$mode)==="longtext") || $propDef->getAttribute("isBlob",false,$mode)
      ){
      return true;  
    }
    return false;
}

function isCheckBoxField($propDef,$mode){
    if($propDef->objectType==="boolean"){
        return true;
    }    
    return false;
}

function isDateField($propDef,$mode){
    if($propDef->objectType==="date"){
        return true;
    }
    return false;
}

function isDateTimeField($propDef,$mode){
    if($propDef->objectType==="datetime"){
        return true;
    }
    else{
        return false;
    }
}

function getFieldType($propDef,$mode){
    if(isFieldHidden($propDef, $mode)){
        return "Hidden";
    }    
    else if(isFieldTextArea($propDef, $mode)){
        return "TextArea";
    }   
    else if(isFieldReadOnly($propDef, $mode)){
        return "Text";
    } 
    else if(isFieldAutoComplete($propDef, $mode)){
        return "AutoComplete";
    }
    else if(isFieldDropDown($propDef, $mode)){
        return "DropDown";
    }
    else if(isCheckBoxField($propDef,$mode)){
        return "CheckBox";
    }
    else if(isDateField($propDef,$mode)){
        return "DatePicker";
    }
    else if(isDateTimeField($propDef,$mode)){
        return "DateTime";
    }
    else if(isFieldTextEntry($propDef, $mode)){
        return "Text";
    }
    else{
        return "Unknown";
    }
}

function getFieldSize($propDef,$mode){
    global $app;
    
    if(in_array($propDef->objectType,array("integer","double","float")) && !isFieldAutoComplete($propDef, $mode)){
        return 8;
    }
    if((isDateField($propDef,$mode)||isDateTimeField($propDef,$mode)) && isFieldReadOnly($propDef, $mode)){
        return 18;
    }    
    
    if($propDef->hasConstraint()){
        $constraint=$propDef->getConstraint();
        $displayObjDef=$app->specManager->findDef($constraint->tableName);
        $displayPropDef=$displayObjDef->getProperty($constraint->displayColumnName); 
    }
    else{
        $displayPropDef=$propDef;
    }
    
    if($displayPropDef->hasAttribute("displayLength")){
        $size=$displayPropDef->getAttribute("displayLength",$mode);
    }
    else{
        $size=$displayPropDef->getAttribute("variableLength",80,$mode);
    }
    $maxDisplaySize=70;
    if($size>$maxDisplaySize){
        $size=$maxDisplaySize;
    }    
    return $size;
}

function getDropDownChoices($propDef,$mode){
    $constraint=$propDef->getConstraint();
    $constraint->generate();
    $choices=$constraint->get();
    
    // sort choices by key
    if ($propDef->getAttribute("sortLookup",true,$mode)) {
        uasort($choices,"strnatcasecmp");
    }
    // lookups returned from the constraint map from real value to display value, drop downs map the
    // other way around, so you must flip the values.
    $choices=array_flip($choices);
    return $choices;
}

// Takes a spec object that contains a reference to a spec.
function convertBooleanValuesToString($obj){                
    foreach($obj->getObjDef()->getProperties() as $propDef){
         if($propDef->objectType=="boolean"){
            if (isset ($obj->{$propDef->name})) {
                $value=$obj->{$propDef->name};
                $trueValues=array("true","1",true,1);
                if (in_array($value, $trueValues,$strict=True)) {
                        $obj->{$propDef->name}="true";
                } else {                        
                    $obj->{$propDef->name}="false";
                }    
            }
        }
    }
}

// Takes a spec object that contains a reference to a spec.
function convertIntegerValuesToString($obj){
    foreach($obj->getObjDef()->getProperties() as $propDef){
         if($propDef->objectType=="integer"){
            $obj->{$propDef->name}=strval($obj->{$propDef->name});
        }
    }
}

// Takes a spec object that contains a reference to a spec.
function convertDateTimeValuesToString($obj,$mode){
    foreach($obj->getObjDef()->getProperties() as $propDef){
         if($propDef->objectType=="datetime" && 
            $propDef->getAttribute("readOnly",false,$mode)===false) {                 
            $value=trim($obj->{$propDef->name});                
            // javascript Date strings must have a "/" seperators instead of "-"

            // TODO: I did this because the InputEx DateTime field fails for a null value horrible                
            if (empty($value)) {
                $value = date('Y-m-d h:i:s');                    
            }
            $value=str_replace("-","/",$value);
            $obj->{$propDef->name}=$value;
        }
    }
}

function performConstraintDisplayValueSubstitution($formFields,$formData,$mode){
    $objDef=$formData->getObjDef();
    foreach($formFields as $formField){
        if(preg_match("/^original-/", $formField["name"]))
        {
           $propToGet=preg_replace("/^original-/", "", $formField["name"]);
        }
        else{
           $propToGet=$formField["name"];
        }
        $propDef=$objDef->getProperty($propToGet);
        $propName=$propDef->name;
       
        // If the field is hidden do not substitute the display value no matter what, this field
        // is never seen by the user, so substitution is not needed.
        if($formField["type"]==="Hidden"){
            continue;
        }
        
        // Substitute the display value for AutoComplete, DropDown and readOnly fields that are not hidden.
        if($formField["type"]==="AutoComplete" || isFieldReadOnly($propDef, $mode)){            
            if (isset($formData->{$propName})) {
                $constraint=$propDef->getConstraint();
                if(!is_null($constraint)){
                    $constraint->generate($mode);
                    $lookup=$constraint->get();                            
                    // If the default value is a key in the lookup
                    // transform the key value into a string value
                    if(isset($lookup[$formData->{$propName}]))
                    {
                        $value=$lookup[$formData->{$propName}];
                        if($value==false){
                            $formData->{$propName}=null;
                        }
                        else{
                            $formData->{$propName}=$value;
                        }
                    }
                    // If the default value is a lookup value, set the field without any transformation.
                    else if(in_array($formData->{$propName}, $lookup, $strict=true)){
                        $formData->{$propName}=$formData->{$propName};
                    }                            
                    else{
                        $formData->{$propName}="";
                    }
                }
            }
        }
    }
}

/*
 *  Collections returned from the database have two problems:
 *          - HasManyThrough collections also return the intersection table data, which needs to be removed
 *          - Collections are infinetly nested, we only want one level of nesteing
 */
function transformCollectionData($formData,$mode){
    $objDef=$formData->getObjDef();
    foreach($objDef->getProperties() as $propDef){
        if($propDef->isCollection() && isset($formData->{$propDef->name})){
            $associationType=$propDef->getAttribute("associationType",false);            
            if($associationType==="HasManyThrough"){
                $intersectionObjs=$formData->{$propDef->name};
                $newSubObjs=array();
                foreach($intersectionObjs as $intersectionObj){
                    $subObj=$intersectionObj->__data__;
                    removeNestedData($subObj);
                    $newSubObjs[]=$subObj;
                }
                $formData->{$propDef->name}=$newSubObjs;
            }
            else if ($associationType==="HasMany"){
                $subObjs=$formData->{$propDef->name};
                $newSubObjs=array();
                foreach($subObjs as $subObj){
                    removeNestedData($subObj);
                    $newSubObjs[]=$subObj;
                }
                $formData->{$propDef->name}=$newSubObjs;
            }           
            
        }
    }
}

function removeNestedData($objData){
    $objDef=$objData->getObjDef();
    foreach($objDef->getProperties() as $propDef){
        if($propDef->isCollection()){
            $objData->{$propDef->name}=null;
        }
    }
}

// This function adds a popup panel for each autocomplete element on the
// form and adds them to the page. This function is here because
// we have had several different page types that require adding this panels
// and we had to maintain the code between all the different pages, which was bad.
function buildAutoCompleteSelectPopups($formFields,$objDef,$page){
    global $app;
    
    // add constraints based on page parameters
    $whereStatementBuilder=new \WhereStatementBuilder();
    $whereStatementBuilder->processPageParameters($page->page);
    
    foreach($formFields as $field){
        if($field["type"]!=="AutoComplete"){
            continue;
        }
        $propDef=$objDef->getProperty($field["name"]);
        $constraint=$propDef->getConstraint();
        $panelObjDef=$app->specManager->findDef($constraint->tableName);
        $page->fireCallback("transformSpec",$panelObjDef);
        
        // want to build filters based on the panelObjDef, not the pageObjDef
        $whereStatementBuilder->processParameters(array("objDef"=>$panelObjDef));        
        $whereStatement=$whereStatementBuilder->buildWhereStatement(); 
        
        // if the constraint has a filter add it to the whereStatement
        if(!is_null($constraint->whereClause)){
            $SQL=$app->fillInGlobals($constraint->whereClause);
            $whereStatement->addClause(new \SQLWhereString($SQL));
        }
        else if(isset($constraint->whereStatement)){
            foreach($constraint->whereStatement->clauses as $whereClause){
                $whereStatement->addClause($whereClause);
            }
        }
        
        // if the field has a constraint, the constraint table has a hideOnAdd property and the current mode is add,
        // then add a hideOnAdd constraint.
        if($propDef->hasConstraint() && $page->mode=="add"){
            $constraint=$propDef->getConstraint();
            $tableDef=$app->specManager->findDef($constraint->tableName);
            if($tableDef->isProperty("hideOnAdd")){
                $whereClause=new \SQLComparison("{$constraint->tableName}.hideOnAdd","=","0");
                $whereStatement->addClause($whereClause);
            }
        }
        
        $hideColumns = array('addedOn', 'addedBy', 'hideOnAdd');
        $checkprops = $panelObjDef->getProperties();
        foreach($checkprops as $checkProp){
            if ($checkProp->hasAttribute("hideInPopUp")){                                        
                $hideColumns[] = $checkProp->name;
            }
        }

        $gridPanelName="{$propDef->name}AutoCompletePopupPanel";
        $panelParams=array(
            "id"=>"{$propDef->name}_AutoCompletePopupPanel",
            "parentObj"=>$page,
            "pageObj"=>$page,
            "objDef"=>$panelObjDef,
            "title"=>"Select {$field["label"]}",
            "selectButtonText"=>"Select {$propDef->getAttribute("caption",$propDef->name)}",  
            "mode"=>$page->mode,
            "whereStatement"=>$whereStatement,
            "hideColumns"=>$hideColumns,
            "selectOneMode"=>true,
            "enableClearAll"=>false,
            "initialLoad"=>false, 
            "onSelectCallback"=>"{$field["name"]}_autocomplete_select",
            "getNestedData"=>false
        );

        $page->{$gridPanelName}= new \DataGridSelectPopupComponent($panelParams);
        $page->addComponent("Content",$page->{$gridPanelName});    
    }    
}

// each autocomplete popup must have a seperate callback because the mapping between the value
// in the grid and the field names are different.
// There are two mappings the must take place:
//      1) map from the display column name of the propDef corresponding to the column in the grid.
//         to the key in the datagrid, they are not the same
//      2) the data from the grid must be mapped to the field the corresponds to the propDef name.
// Again, this is here, because we have used this function before for multiple pages
function renderAutoCompletePopupSelectCallbacks($formFields,$objDef,$page){        
    foreach($formFields as $field){
        if($field["type"]!=="AutoComplete"){
            continue;
        }        
        $gridPanelName="{$field["name"]}AutoCompletePopupPanel";
        $propDef=$objDef->getProperty($field["name"]);
        $constraint=$propDef->getConstraint();
        $propNameToColumnKeyMap=$page->{$gridPanelName}->grid->getPropNameToKeyMap();
        $gridColumnToGet=$propNameToColumnKeyMap[$constraint->displayColumnName];
        
        $page->jsInlineHead->addJS(
"
var {$field["name"]}_autocomplete_select = function(e){
    var record={$field["name"]}_AutoCompletePopupPanel_Grid.getSelectedRows();    ;
    // map from display column name in propDef to Grid Column key.
    var selectedData=record['{$gridColumnToGet}'];
    
    var field={$page->form->id}_formControl.controls['{$field["name"]}'];
    field.setValue(selectedData);
    {$page->{$gridPanelName}->id}.hide();
}
"              
);
    }
}

function buildAddSelectPopups($objDef,$page){
    global $app;
    
    // add constraints based on page parameters
    $whereStatementBuilder=new \WhereStatementBuilder();
    $whereStatementBuilder->processPageParameters($page->page);
    
    foreach($objDef->getProperties() as $propDef){
        if($propDef->isCollection() && !$propDef->getAttribute("hidden",false,$page->mode) && $propDef->getAttribute("visible",true,$page->mode)){
            // no popup panel for embeddedCollections
            if($propDef->getAttribute("embeddedCollection",false)){
                continue;
            }
            if ($propDef->getAttribute("popupEdit",true,$page->mode)) {
                  if ($propDef->getAttribute("associationType")=="HasMany") {                         
                    $intersectionObjDef=$app->specManager->findDef($propDef->getAttribute("referenceTable"));
                    $associationKey=$propDef->getAttribute("associationUniqueKey");
                    $associationColDef=$intersectionObjDef->getProperty($associationKey);
                    $refTableName=$associationColDef->getAttribute("fkTable",$intersectionObjDef->name);                          
                  } else if ($propDef->getAttribute("associationType")=="HasManyThrough") {                                                
                    $intersectionObjDef=$app->specManager->findDef($propDef->getAttribute("throughTable"));
                    $associationKey=$propDef->getAttribute("throughColumnFrom");
                    $associationColDef=$intersectionObjDef->getProperty($associationKey);
                    $refTableName=$propDef->getAttribute("referenceTable");
                  }

                $refObjDef=$app->specManager->findDef($refTableName);
                $page->fireCallback("transformSpec", $refObjDef);
               
                // want to build filters based on the panelObjDef, not the pageObjDef
                $whereStatementBuilder->processParameters(array("objDef"=>$refObjDef));
                $whereStatement=$whereStatementBuilder->buildWhereStatement();
               
                $gridPanelName="{$propDef->name}_AddPopupPanel";
                $panelParams=array(
                    "id"=>"$gridPanelName",
                    "parentObj"=>$page,
                    "pageObj"=>$page,
                    "objDef"=>$refObjDef,
                    "title"=>"Add {$refObjDef->name}",
                    "selectButtonText"=>"Add Selected Values",
                    "mode"=>$page->mode,
                    "initialLoad"=>false,  
                    "whereStatement"=>$whereStatement,
                    "onSelectCallback"=>"",
                    "getNestedData"=>false
                );
                $page->{$gridPanelName}= new \DataGridSelectPopupComponent($panelParams);
                $page->addComponent("Content",$page->{$gridPanelName});      
            }
        }
    }
}       

function addSuccessPanel($page){        
        $params=new \ParamBuilder();
        $params->add("id","{$page->id}_SuccessPanel");
        $params->add("parentObj",$page);
        $params->add("pageObj",$page->pageObj);
        $params->add("title","Success");
        $page->successPanel=new \PopupComponent($params->getParams());
        
        $params=new \ParamBuilder();
        $params->add("id","{$page->id}_SuccessMessage");
        $params->add("innerHTML","Submit Successful");
        $messageDiv=new \DivComponent($params->getParams());
        $page->successPanel->addComponent($messageDiv);        
        $page->addComponent("Content",$page->successPanel);                
}
        

?>
