/* 
 * Basic CheckBox Control
 */

Zephyr.CheckBoxControl=function(options){
    Zephyr.BaseControl.call(this,options);
    if(options.size!==undefined){
        this.size=options.size;
    }
}

// inherit methods from parent.
Zephyr.CheckBoxControl.prototype=new Zephyr.BaseControl({});

// override parent methods
Zephyr.CheckBoxControl.prototype.render=function(){
    this.el=document.createElement("input");
    this.el.setAttribute("type", "checkbox");
    this.el.setAttribute("id", this.name);
    this.el.setAttribute("size", this.size);
    
    if(this.el.checked===true){
        this.el.setAttribute("checked","checked");
    }
    this.el.setAttribute("value", this.value); 
    this.el.setAttribute("readonly","readonly");
    return this.el;    
}

Zephyr.CheckBoxControl.prototype.setValue=function(value){
    if ((value==='1')||(value===1)||(value===true)) {
        this.el.checked=true;
    } else {
        this.el.checked=false;
    }    
}

Zephyr.CheckBoxControl.prototype.getValue=function(value){
    if (this.el.checked) {
        return '1';
    } else {
        return '0';
    }    
}