Zephyr.LinkControl=function(options){
    this.collectInput=false;
    if(options.text!==undefined){
        this.text=options.text;
    }
    
    if(options.url!==undefined){
        this.url=options.url;
    }
    
    if(options.cssClass===undefined){
        this.cssClass="ZephyrLink";
    }
}

Zephyr.LinkControl.prototype=new Zephyr.BaseControl({});

Zephyr.LinkControl.prototype.render=function(){
    this.el=document.createElement('a')
    this.el.setAttribute("href",this.url);
    this.el.setAttribute("class",this.cssClass);
    if(this.text!==undefined){
        this.el.innerHTML=this.text;
    }
    else{
        this.el.innerHTML=this.url;
    }
    return this.el;
}

Zephyr.LinkControl.prototype.setValue=function(url){
    this.el.setAttribute("href",url);
}

Zephyr.LinkControl.prototype.getValue=function(url){
    return this.el.getAttribute("href",url);
}


