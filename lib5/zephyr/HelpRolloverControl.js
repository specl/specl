/* 
 * This control display a help icon and displays the text when the user hovers over the icon
 */

Zephyr.HelpRolloverControl=function(options){
    Zephyr.BaseControl.call(this,options);
    if(options.popupText!==undefined){
        this.popupText=options.popupText;
    }
    
    if(options.type===undefined){
        this.type="HelpRollOver";
    }
    
    this.collectInput=false;
    
    if(options.cssClass===undefined){
        this.cssClass="HelpRollover"
    }
    this.el=null;
    this.imageEl=null;
    this.textEl=null;
}

// inherit methods from parent.
Zephyr.HelpRolloverControl.prototype=new Zephyr.BaseControl({});;

// override parent methods
Zephyr.HelpRolloverControl.prototype.render=function(){
    this.el=document.createElement("span");
    this.el.setAttribute("id", this.name);
    this.el.setAttribute("class",this.cssClass);
    this.imageEl=document.createElement("img")
    this.imageEl.setAttribute("src","help.jpg");
    this.el.appendChild(this.imageEl);
    
    this.tooltip = new YAHOO.widget.Tooltip("myTooltip", { 
    context: this.imageEl, 
    text: this.popupText,
    xyoffset:[0,0]});
    return this.el;    
}

Zephyr.HelpRolloverControl.prototype.setValue=function(value){
    this.el.value=value;        
}

Zephyr.HelpRolloverControl.prototype.getValue=function(){
    return this.el.value;
}


