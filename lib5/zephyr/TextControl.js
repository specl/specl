/* 
 * Basic Text Entry control
 */

Zephyr.TextControl=function(options){
    Zephyr.BaseControl.call(this,options);
    if(options.size!==undefined){
        this.size=options.size;
    }
    
    if(options.maxSize!==undefined){
        this.maxSize=options.maxSize;
    }
    
    if(options.value===undefined){
        this.value="";
    }
}

// inherit methods from parent.
Zephyr.TextControl.prototype=new Zephyr.BaseControl({});

// override parent methods
Zephyr.TextControl.prototype.render=function(){
    this.el=document.createElement("input");
    this.el.setAttribute("type", "text");
    this.el.setAttribute("id", this.name);
    this.el.setAttribute("size", this.size);
    this.el.setAttribute("maxlength", this.maxSize);
    this.el.setAttribute("value", this.value)
    this.el.setAttribute("class",this.cssClass);
    if(this.readonly===true){
        this.el.setAttribute('readonly',this.readonly);    
    }
    return this.el;    
}


Zephyr.TextControl.prototype.setValue=function(value){
    this.el.value=value;        
}

Zephyr.TextControl.prototype.getValue=function(){            
    return this.el.value;
}
