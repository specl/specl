Zephyr.FormControl=function(options){
    this.controls={};
    this.layout={};
    this.numspaces=0;
    if(options.name!==undefined){
        this.name=options.name
    }
    else{
        this.elementId=null;
    }
    
    this.el=null;
    
    // only stores the error fields, used to find all the error fields and clear them
    this.errorFields={};
    
    // stores all controls for rendering
    this.controls={};
}

Zephyr.FormControl.prototype.addControl=function(control){
    this.controls[control.name]=control;
}

// Build the form DOM element
Zephyr.FormControl.prototype.build=function(){
    var key,controlEl;   
    
    // TODO: Create a randomId in the elementId is not defined    
   
    this.el=document.createElement("div");
    this.el.setAttribute("class","ZephyrForm");
    this.el.setAttribute("id",this.name);
    
    for(key in this.controls){        
        if(this.controls[key].type!=="SpaceControl"){
            controlEl=this.controls[key].render();
            this.el.appendChild(controlEl);
        }
        else{
            this.el.appendChild(this.controls[key].el);
        }
    }
}

Zephyr.FormControl.prototype.addNewLine=function(){
    var spaceControl={"type":"SpaceControl","collectInput":false}
    this.numspaces++;
    var spaceEl=document.createElement("div");
    var spaceElId=this.name+"_"+this.numspaces;
    spaceEl.setAttribute("id",spaceElId);
    spaceEl.setAttribute("class","ZephyrSpace");
    spaceControl.el=spaceEl;
    this.controls[spaceElId]=spaceControl;
}

// Add the DOM element to the browser DOM.
Zephyr.FormControl.prototype.render=function(){
    var browserEl;
    
    browserEl=YAHOO.util.Dom.get(this.name);
    browserEl.parentNode.replaceChild(this.el,browserEl);    
}

Zephyr.FormControl.prototype.initEvents=function(){
    var key,control;
    for(key in this.controls){
        control=this.controls[key];
        if(control.initEvents!==undefined){
            control.initEvents();
        }
    }
}

Zephyr.FormControl.prototype.getData=function(){
    var key,control;
    var data={};
    for(key in this.controls){
        control=this.controls[key];
        if(control.collectInput===true){
            if(control["getValue"]!==undefined){
                data[control.name]=control.getValue();
            }
            else if(control["getData"]!==undefined){
                data[control.name]=control.getData();
            }
        }
    }
    return data;
}

Zephyr.FormControl.prototype.displayFieldError=function(fieldName,errorMsg){    
    if(!fieldName.match(/.*-collectionError/)){
        var control=this.controls[fieldName];    
        var errorField;
        if(control["displayError"]!==undefined){
            if(control.type==="Collection"){
                control.displayError(errorMsg);
            }
            else{
                control.displayError(errorMsg[fieldName]);
            }
        }
        errorField=this.errorFields[control.name+"_error"];
        // handles the form error message field, that has no control
        if(typeof(errorField)==="undefined"){
            errorField=this.errorFields[control.name];
        }
        if(typeof(errorMsg)==='string' && typeof(errorField)!=="undefined"){
            errorField.el.innerHTML=errorMsg;
            errorField.setCssClass("ZephyrFormErrorMessage"); 
        }
    }else{
        var split=fieldName.split("-");
        var controlName=split[0];
        var control=this.controls[controlName];
        control.showErrorOutline();
        errorField=this.errorFields[control.name+"_error"];
        errorField.el.innerHTML=errorMsg;
        errorField.setCssClass("ZephyrFormErrorMessage");
    }
    
    
}

Zephyr.FormControl.prototype.clearFieldErrors=function(){
    var key,control,errorField;
    for(key in this.controls){
        control=this.controls[key];
        if(control.clearError!==undefined){
            control.clearError();
        }
        if(control.clearErrors!==undefined){
            control.clearErrors();
        }
        errorField=this.errorFields[control.name+"_error"];
        // handles the form error message field, that has no control
        if(typeof(errorField)==="undefined"){
            errorField=this.errorFields[control.name];
        }
        if(typeof(errorField)!=="undefined"){
            errorField.innerHTML="";
            errorField.setCssClass("hidden");
        }
    }
}

Zephyr.FormControl.prototype.addErrorField=function(control){
    this.addControl(control);
    this.errorFields[control.name]=control;
}


 