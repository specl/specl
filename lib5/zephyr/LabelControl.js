/* 
 * Basic Label Control
 */

Zephyr.LabelControl=function(options){
    Zephyr.BaseControl.call(this,options);
    if(options.size!==undefined){
        this.size=options.size;
    }
    
    if(options.type===undefined){
        this.type="label";
    }
    
    this.collectInput=false;
    
    if(options.cssClass===undefined){
        this.cssClass="ZephyrLabel"
    }
}

// inherit methods from parent.
Zephyr.LabelControl.prototype=new Zephyr.BaseControl({});;

// override parent methods
Zephyr.LabelControl.prototype.render=function(){
    this.el=document.createElement("input");
    this.el.setAttribute("type", "text");
    this.el.setAttribute("id", this.name);
    this.el.setAttribute("size", this.size);
    this.el.setAttribute("value", this.value); 
    this.el.setAttribute("readonly","readonly");
    this.el.setAttribute("class",this.cssClass);
    return this.el;    
}

Zephyr.LabelControl.prototype.setValue=function(value){
    this.el.value=value;        
}

Zephyr.LabelControl.prototype.getValue=function(){
    return this.el.value;
}