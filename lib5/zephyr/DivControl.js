/* 
 * Basic Div Control
 */

Zephyr.DivControl=function(options){
    Zephyr.BaseControl.call(this,options);
    if(options.size!==undefined){
        this.size=options.size;
    }
    
    if(options.type===undefined){
        this.type="div";
    }
    
    if(options.value===undefined){
        this.value="";
    }
    
    this.collectInput=false;
}

// inherit methods from parent.
Zephyr.DivControl.prototype=new Zephyr.BaseControl({});;

// override parent methods
Zephyr.DivControl.prototype.render=function(){
    this.el=document.createElement("div");
    this.el.setAttribute("class", this.cssClass);
    this.el.innerHTML=this.value;    
    return this.el;    
}

Zephyr.DivControl.prototype.setValue=function(value){
    this.el.innerHTML=value;        
}

Zephyr.DivControl.prototype.getValue=function(){
    return this.el.innerHTML;
}/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Zephyr.DivControl.prototype.setCssClass=function(className){
    this.el.setAttribute("class", className);
}

