/* 
 * Basic Span Control
 */

Zephyr.SpanControl=function(options){
    Zephyr.BaseControl.call(this,options);
    if(options.size!==undefined){
        this.size=options.size;
    }
    
    if(options.type===undefined){
        this.type="span";
    }
    
    this.collectInput=false;
}

// inherit methods from parent.
Zephyr.SpanControl.prototype=new Zephyr.BaseControl({});;

// override parent methods
Zephyr.SpanControl.prototype.render=function(){
    this.el=document.createElement("span");
    this.el.setAttribute("class", this.cssClass);
    this.el.innerHTML=this.value;    
    return this.el;    
}

Zephyr.SpanControl.prototype.setValue=function(value){
    this.el.innerHTML=value;        
}

Zephyr.SpanControl.prototype.getValue=function(){
    return this.el.innerHTML;
}