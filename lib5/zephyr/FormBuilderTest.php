<?php
require_once("ZephyrFormBuilder.php");

require_once("../ZonesInclude.php");

$classPaths[]=dirname(__FILE__) ."/../../seqlims5/Custom/Spec/";
$classPaths[]=dirname(__FILE__) ."/../../seqlims5/Custom/DAL/";
$classPaths[]=dirname(__FILE__) ."/../../seqlims5/Custom/Cls/";
$classPaths[]=dirname(__FILE__) ."/../../seqlims5/CodeGen/";
$classPaths[]=dirname(__FILE__) ."/../../seqlims5/";
$app = new LIMSApp();
$app->start();
$app->setValue("SESSION.userId", "marks"); 
$app->setValue("USER.defaultLaboratory", "wigler"); 

$formObjDef=$app->specManager->findDef("SeqSample");

$types=array();
$sizes=array();

$mode="add";
foreach($formObjDef->properties as $propDef){
    if($propDef->isScalar() && ZephyrFormBuilder\shouldRenderField($propDef, $mode)){
        $types[$propDef->name]=ZephyrFormBuilder\getFieldType($propDef, $mode);
        $sizes[$propDef->name]=ZephyrFormBuilder\getFieldSize($propDef, $mode);
    }
}

print_r($types);
print_r($sizes);



?>
