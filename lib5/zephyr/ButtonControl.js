/* 
 * Basic Button control
 */

Zephyr.ButtonControl=function(options){
    Zephyr.BaseControl.call(this,options);
    if(options.type!==undefined){
        this.type=options.type;
    }
    else{
        this.type="button";
    }
    
    if(options.text!==undefined){
        this.text=options.text
    }
    else{
        this.text="button";
    }
    
    this.collectInput=false;
    
    if(options.maxSize!==undefined){
        this.maxSize=options.maxSize;
    }
}

// inherit methods from parent.
Zephyr.ButtonControl.prototype=new Zephyr.BaseControl({});

// override parent methods
Zephyr.ButtonControl.prototype.render=function(){
    this.el=document.createElement("input");
    this.el.setAttribute("type", this.type);
    this.el.setAttribute("id", this.name);
    this.el.setAttribute("value",this.text);
    return this.el;    
}


Zephyr.ButtonControl.prototype.setValue=function(value){
    this.el.value=value;        
}

Zephyr.ButtonControl.prototype.getValue=function(){
    return this.el.value;
}


// The components do not need to send events themselves, because javascript bubbles up all events.
/*
 *  When a button is clicked it sends a buttonClicked event to the central 
 *  dispatcher along with the elementName of the button that was clicked 
 *  
 *  The obj variable is to pass along user defined variables, it does not have
 *  to be defined.
 */
//Zephyr.ButtonControl.prototype.initEvents=function(obj){
//    var that=this;
//    var onClicked=function(e){
//        var eventMsg={
//            event:'buttonClicked',
//            elementName:that.name,
//            obj:obj
//        }
//        if(DispatchObj!==undefined){
//            DispatchObj.dispatch(eventMsg)
//        }
//    }
//    YAHOO.util.Event.addListener(this.el,'click',onClicked);
//}
