/* 
 * Basic DropDown control
 * 
 * The drop down control takes a javascript object in the choices parameter
 * where the key represents the text value of the choice and the value
 * represents the real value of the choice
 */

Zephyr.DropDownControl=function(options){
    Zephyr.BaseControl.call(this,options);
    if(options.choices!==undefined){
        this.choices=options.choices;
    }
    else{
        this.choices={};
    }    
}

// inherit methods from parent.
Zephyr.DropDownControl.prototype=new Zephyr.BaseControl({});

// override parent methods
Zephyr.DropDownControl.prototype.render=function(){
    this.el=document.createElement("select");    
    this.el.setAttribute("id", this.name);    
    var key,value,choiceEl;
    for(key in this.choices){
        value=this.choices[key];
        choiceEl=document.createElement("option");
        choiceEl.setAttribute("value",value);
        choiceEl.innerHTML=key;
        this.el.appendChild(choiceEl);        
    }    
    this.el.value=this.value;
    return this.el;    
}

Zephyr.DropDownControl.prototype.setValue=function(value){
    this.el.value=value;        
}

Zephyr.DropDownControl.prototype.getValue=function(){
    return this.el.value;
}