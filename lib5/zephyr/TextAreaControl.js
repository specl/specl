Zephyr.TextAreaControl=function(options){
    Zephyr.BaseControl.call(this,options);
    if(options.size!==undefined){
        this.size=options.size;
    }
    
    if(options.maxSize!==undefined){
        this.maxSize=options.maxSize;
    }
    
    if(options.value===undefined){
        this.value="";
    }
}

// inherit methods from parent.
Zephyr.TextAreaControl.prototype=new Zephyr.BaseControl({});

// override parent methods
Zephyr.TextAreaControl.prototype.render=function(){
    this.el=document.createElement("textarea");
    this.el.setAttribute("type", "hidden");
    this.el.setAttribute("id", this.name);
    this.el.setAttribute("size", this.size);
    this.el.setAttribute("maxlength", this.maxSize);
    this.el.setAttribute("class",this.cssClass);
    this.el.value=this.value;
    if(this.readonly===true){
        this.el.setAttribute('readonly',this.readonly);    
    }
    return this.el;    
}


Zephyr.TextAreaControl.prototype.setValue=function(value){
    this.el.value=value;        
}

Zephyr.TextAreaControl.prototype.getValue=function(){            
    return this.el.value;
}






