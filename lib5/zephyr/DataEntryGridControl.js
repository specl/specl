Zephyr.DataEntryGridControl=function(options){
    Zephyr.BaseControl.call(this,options);
    
    if(options.cssClass===undefined){
        this.cssClass="ZephyrDataEntryGrid";
    }
    if(options.fields){
        this.fields=options.fields;
    }
    
    if(options.value){
        this.initValues=options.value;
    }
    
    if(options.defaultValues){
        this.defaultValues=options.defaultValues;
    }
    else{
        this.defaultValues={};
    }
    
    // store all the controls of the row in an array of objects
    this.controls=[];
    
    /*
     *  An autoincrement index is required give each row a unique internal id. This is needed for the
     *  remove row button, as the program needs to know which element to remove. The autoindex is incremented
     *  every time a new row is added, but it is not decremented when a row is removed.
     */
    this._autoIndex=0;
}

Zephyr.DataEntryGridControl.prototype=new Zephyr.BaseControl({});

Zephyr.DataEntryGridControl.prototype.render=function(){    
    this.tableEl=document.createElement("table");
    this.tableEl.setAttribute("id",this.name+"_table");
    this.tableEl.setAttribute("class",this.cssClass);
    this.renderHeader();
    this.renderBody();
    for(var i=0;i<this.initValues.length;i++){
        this.addNewRow();
        this.setRowData(i,this.initValues[i]);
    }
    this.setRowData(this.initValues);   
    this.renderFooter();
    return this.tableEl;
}

Zephyr.DataEntryGridControl.prototype.renderHeader=function(){
    var thead = document.createElement("thead");       
    thead.setAttribute("id", this.name+'_thead');    
    this.tableEl.appendChild(thead); 
    var tr = document.createElement("tr");
    thead.appendChild(tr);   
    for (var i=0; i<this.fields.length; i++) {        
        var field = this.fields[i];             
        var th = document.createElement("th");
        if(field["type"]==="Hidden"){
            th.style.display="none";
        }
        tr.appendChild(th);    
        var text = document.createTextNode(field.label);                 
        th.appendChild(text);            
    }
}

Zephyr.DataEntryGridControl.prototype.renderBody=function(){
    this.body=document.createElement("tbody")
    this.body.setAttribute("id",this.name+"_tbody");
    this.tableEl.appendChild(this.body);
}

Zephyr.DataEntryGridControl.prototype.addRowControl=function(control){
    this.rowControls.push(control);
}

// This function assumes that all zephyr classes are labeled as "{$type}Control" and
// are defined in the Zephyr namespace.
Zephyr.DataEntryGridControl.prototype.addNewRow=function(){
    var tr=document.createElement("tr");
    var rowId=this.genRowId();
    tr.setAttribute("id",rowId);
    var rowControls={};
    for(var i=0;i<this.fields.length;i++){
        var field=this.fields[i];
        var td= document.createElement("td");
        var className=field["type"]+"Control";
        var constructor=Zephyr[className];
        var control=new constructor(field);
        control.rowId=rowId;
        var el=control.render();
        td.appendChild(el);
        if(field["type"]==="Hidden"){
            td.style.display="none";
        }     
        tr.appendChild(td);              
        rowControls[field["name"]]=control;        
        //create an error div for each element
        var errDiv =  document.createElement("div"); 
        errDiv.setAttribute("id", rowId +'_'+control.name+"_error");
        errDiv.setAttribute("class","ZephyrGridErrorMessage");
        td.appendChild(errDiv); 
    }
    this.controls.push(rowControls);
    var key;
    rowControls=this.controls[this.controls.length-1];
    for(key in rowControls){
        if(this.defaultValues[key]!==undefined){
            control=rowControls[key];        
            control.setValue(this.defaultValues[key]);
        }
    }
    this.body.appendChild(tr);
}

/*
 * This function creates a unique row name based on the id of the DataEntryGrid and the autoincrement id.
 * The autoincrement is incremented by 1 everytime this function is called.
 */
Zephyr.DataEntryGridControl.prototype.genRowId=function(){
    var id=this.name + "_row_" + this._autoIndex;
    this._autoIndex+=1;
    return id;
}

Zephyr.DataEntryGridControl.prototype.setRowData=function(index,rowData){
    var rowControls,key,control;
    rowControls=this.controls[index];
    for(key in rowControls){
        if(rowData[key]!==undefined){
            control=rowControls[key];        
            control.setValue(rowData[key]);
        }
    }    
}

Zephyr.DataEntryGridControl.prototype.getData=function(){
    var i,key,rowControls,control,rowData;
    var data=[];
    for(i=0;i<this.controls.length;i++){
        rowControls=this.controls[i];
        rowData={};
        for(key in rowControls){
            control=rowControls[key];
            rowData[key]=control.getValue();
        }
        data.push(rowData);
    }
    return data;
}

Zephyr.DataEntryGridControl.prototype.clearError=function() {        
    
    var i,rowControls,control,fieldName,errorDivId,errorDiv;
    for(i=0;i<this.controls.length;i++){
        rowControls=this.controls[i]
        for(fieldName in rowControls){
            control=rowControls[fieldName];    
            if(control.clearError!==undefined){
                control.clearError();
            }
            errorDivId=control.rowId +'_'+control.name+"_error"
            errorDiv=YAHOO.util.Dom.get(errorDivId);
            errorDiv.innerHTML="";
        }
    } 
    YAHOO.util.Dom.removeClass(this.tableEl,"ZephyrErrorOutlineField");
}

Zephyr.DataEntryGridControl.prototype.displayError=function(errorMessages) {        
    
    var i,rowErrors,fieldName,fieldError,control,errorDivId,errorDiv,rowControls;
    for(i=0;i<errorMessages.length;i++){
        rowErrors=errorMessages[i];
        rowControls=this.controls[i];
        for(fieldName in rowErrors){
            fieldError=rowErrors[fieldName];            
            control=rowControls[fieldName];
            if(control!==undefined){
                control.displayError(fieldError);
                errorDivId=control.rowId +'_'+control.name+"_error"
                errorDiv=YAHOO.util.Dom.get(errorDivId);
                errorDiv.innerHTML=fieldError;
            }
        }
    }   
    
    if(errorMessages.length>=1){
        this.showErrorOutline();        
    }
}

Zephyr.DataEntryGridControl.prototype.showErrorOutline=function(){
     YAHOO.util.Dom.addClass(this.tableEl,"ZephyrErrorOutlineField");
}

Zephyr.DataEntryGridControl.prototype.renderFooter=function(){
    var tfoot = document.createElement("tfoot");        
    var id=this.name+'_tfoot';    
    tfoot.setAttribute("id", id); 
    var tr = document.createElement("tr");
    tfoot.appendChild(tr);        
    var td = document.createElement("td");
    td.setAttribute("colspan",this.fields.length);
    tr.appendChild(td);   
    this.tableEl.appendChild(tfoot);
}

Zephyr.DataEntryGridControl.prototype.removeRow=function(rowEl){
   var name=rowEl.getAttribute("id");
   var split=name.split('_');
   var index=parseInt(split[2])
   this.body.removeChild(rowEl);
   this.controls.splice(index, 1);  
}
