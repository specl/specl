/* BaseControl is the class through which all Zepthyr gui controls inherit.
 * It defines a common set of attributes and functions accross all controls.
 * 
 * TODO: Implement parameter checking and definition in js
 * TODO: Implement consistent way of retrieving options for all js classes.
 */

Zephyr.BaseControl=function(options){
    if(options.name!==undefined){
        this.name=options.name;
    }
    
    if(options.type!==undefined){
        this.type=options.type;
    }
    
    if(options.value!==undefined){
        this.value=options.value;
    }
    
    if(options.visible!==undefined){
        this.visible=options.visible;
    }
    
    if(options.readonly!==undefined){
        this.readonly=options.readonly;
    }
    else{
        this.readonly=false;
    }

    if(options.cssClass!==undefined){
        this.cssClass=options.cssClass;
    }
    else{
        this.cssClass="ZephyrControl";
    }
    
    if(options.collectInput!==undefined){
        this.collectInput=options.collectInput;
    }
    else{
        this.collectInput=true;
    }
    
    if(options.data){
        this.data=options.data;
    }
    else{
        this.data={};
    }
    
    // store reference to rendered html element
    this.el=null;
};

/*
 * This function renders the HTML of the control. This should be implemented
 * in every child class. The function must return a javascript dom object.
 */
Zephyr.BaseControl.prototype.render=function(){
    throw{
        name:"NotImplemented",
        message:"The function render must be implemented in all Zephry gui controls"
    }
};

// This is the default for non-compound elements
Zephyr.BaseControl.prototype.displayError=function(errorMsg){
    YAHOO.util.Dom.addClass(this.el,"ZephyrErrorOutlineField");
}

// This is the default for non-compound elements
Zephyr.BaseControl.prototype.clearError=function(){
    YAHOO.util.Dom.removeClass(this.el,"ZephyrErrorOutlineField");   
}




