/*
 *  Central Dispatcher is an object that coordinates the dispatching of events and actions for
 *  the entire webpage.
 *  
 *  Central Dispatcher has two main functions:
 *      - To respond to events and generate actions based on a set of rules
 *      - Execute actions
 *  
 *  Both events and actions are trigger by sending the Dispatcher a message, in the form
 *  of a javascript object. The message can be in response to a client side event
 *  or an ajax event. 
 *  
 *  WARNING: Any error messages throw are not diplayed in the firebug console if they are thrown
 *           from inside a callback function. This class needs a better way of handling errors.
 */

// Central Dispatcher constructor
var CentralDispatcher=function(){
    this.events={};
    this.actions={};
}

/*
 * dispatch is the main execution method of the Central Dispatcher, it takes a message to
 * dispatch as its arguement.
 * 
 * The message is validated before being executed.
 * 
 * If the message is an action and it is valid, then the action is executed.
 * 
 * If the message is an event and it is valid, then each rule for that event
 * is executed and translated into an action message.
 */
CentralDispatcher.prototype.dispatch=function(message){
    try{
        this.validateMessage(message);     
    }
    catch(e){
        // an alert is the only cross browser compatable way of displaying an error message
        // even inside of callbacks where throwing errors does not produce a message.
        alert("errorName:"+e.name + "\n" + "lineNumber:" + e.lineNumber + "\n" + e.message);
    }
    if(message["action"]!==undefined){
        var runFunc=this.actions[message["action"]]["actionFunction"];
        runFunc(message);
    }
    // if the message is an event iterate through all the rule functions and execute them
    else{
        var ruleName,runFunc;
        var rules=this.events[message.event].rules;
        for(ruleName in rules){
            runFunc=rules[ruleName];
            runFunc(message);
        }
    }    
}

/*
 * Validate the message.
 * 
 * At the moment, when a invalid message is passed to the dispatcher an exception is thrown,
 * this may change in the future, when we design a better way of handling errors.
 * 
 * If the error message is valid do nothing and return true.
 */
CentralDispatcher.prototype.validateMessage=function(message){
    var checkResults;
    checkResults=this.checkGenericMessageRules(message);    
    // do nothing if an event is not defined in the central dispatcher.
    if(checkResults.skip===true){
        return true;
    }
    if(checkResults.valid===true){
        checkResults=this.checkMessageParameters(message);
        if(checkResults.valid===true){
            return true;
        }
        else{            
            throw{
                name:"ParameterMessageValidation",
                message:checkResults.message
            }
        }        
    }
    else{    
        throw{
            name:"GenericMessageValidation",
            message:checkResults.message
        }
    }
}

/*
 * checkGenericMessageRules checks the rules that apply to all messages passed into the Central Dispatcher.
 * 
 * Current Rules:
 *      - the message must contain an event or action property, but cannot contain both.
 *      - the action or event must be defined in the CentralDispatcher
 * 
 * If validation rules an error message object is returned, else a success message is returned.
 */
CentralDispatcher.prototype.checkGenericMessageRules=function(message){
    var eventDefined=false;
    var actionDefined=false;
    
    if(message["event"]!==undefined && message["event"]!=""){
        eventDefined=true;
    }
    
    if(message["action"]!==undefined && message["action"]!=""){
        actionDefined=true;
    }
    
    if(eventDefined===false && actionDefined===false){
        return {
            "valid":false,
            "message":"The property 'event' or 'action' must be defined in a message passed to the dispatcher"
        };
    }
    else if (eventDefined===true && actionDefined===true){
        return {
            "valid":false,
            "message":"The properties 'event' and 'action' cannot be defined at the same time"
        };
    }
    
    // An action must be defined in the event dispatcher.  
    if(actionDefined===true){
        if(this.actions[message["action"]]===undefined){
            return{
                "valid":false,
                "message":"The action " + message["action"] + " is not defined in the dispatcher."
            }
        }        
    }
    // if the event is not defined in the central dispatcher do nothing.
    // this means that an event has been fired and there are no rules or actions associated with it.
    else if (eventDefined===true)
    {        
        if(this.events[message["event"]]===undefined){
            return{
                "valid":false,
                "message":"The event " + message["event"] + " is not defined in the dispatcher.",
                "skip":true
            }
        }
    }    
    
    // the only way to get here is if all of the conditions have passed
    return {
        "valid":true
    };

}

/*
 * checkMessageParameters validates the parameters of the action or event specified in the message. 
 * 
 * If validation rules an error message object is returned, else a success message is returned.
 * 
 * This function validates the functions requiredParameters checks the parameter rules if there are any
 */
CentralDispatcher.prototype.checkMessageParameters=function(message){
    var def;
    if(message["action"]!==undefined){
        def=this.actions[message["action"]];
    }
    else{
        def=this.events[message["event"]];
    }    
    
    // check for required parameters
    var requiredParameters=def["requiredParameters"];    
    if(requiredParameters!==undefined){
        var checkRequired=this.checkRequiredParams(requiredParameters, message);
        if(checkRequired.valid===false){
            return checkRequired;
        }
    }

    // check custom rules
    var parameterRules=def["parameterRules"];
    if(parameterRules!==undefined){
        var rulesResult=parameterRules(message);
        if(rulesResult.valid!==true){
            return rulesResult;
        }
    }
    
    // if we made it here then all the checks have passed.
    return {"valid":true};
}

/*
 *  addAction takes an object defining the action as its parameter and adds it to the CentralDispatcher.
 *  
 *  The action object requires the following properties:
 *      - actionName: The name of the action as a string. The name must not be already used by the CentralDispatcher
 *      - actionFunction: A javascript function to execute
 *  
 *  Other options for the actionDef are:
 *      - requiredParameters: An array of strings specifiying the required parameter names
 *      - parameterRules: A javascript function to be executed to validate the message parameters before executing the action.
 *  
 *  This function will typically be called by code generated from the server when defining the page.
 */
CentralDispatcher.prototype.addAction=function(actionDef){
    // check required parameters
    var requiredParameters=["actionName","actionFunction"];
    var checkRequired=this.checkRequiredParams(requiredParameters, actionDef);
    if(checkRequired.valid!==true){
        throw{
            name:"MissingRequiredParameters",
            message:checkRequired.message
        }
    }    
    
    // check that the action name is not already defined in the dispatcher    
    // if the action is already defined do nothing.
    if(this.actions[actionDef["actionName"]]!==undefined){        
        return;
//        throw{
//            name:'ActionAlreadyDefined',
//            message:"The action " + actionDef["actionName"] + " is already defined in the dispatcher"                
//        }
    }
    
    // if the actionName does not exist add it to the dispatcher
    this.actions[actionDef["actionName"]]=actionDef;
}

/**
 * addEvent takes an object defining an event as its parameter and adds it to the CentralDispatcher. If the
 * event already exists, then the rules of the event passed in are added to the current event, if there are
 * any new rules. Trying to add a rule that already exists to an event will raise an error.
 * 
 * The event object requires the following properties:
 *   - eventName: The name of the even as a string
 *   - rules: An object of javascript functions that take an event message as an input and return an 
 *            action message as output.
 *  
 *  Other options for the eventDef are:
 *      - requiredParameters: An array of strings specifiying the required parameter names
 *      - parameterRules: A javascript function to be executed to validate the message parameters before executing the action.
 *      
 *  This function will typically be called by code generated from the server when defining the page.
 */
CentralDispatcher.prototype.addEvent=function(eventDef){
    // check required parameters
    var requiredParameters=["eventName","rules"];
    var checkRequired=this.checkRequiredParams(requiredParameters, eventDef);
    if(checkRequired.valid!==true){
        throw{
            name:"MissingRequiredParameters",
            message:checkRequired.message
        }
    }
    
    // check to see if the event exists in the Dispatcher
    if(this.events[eventDef["eventName"]]!==undefined){
        // if the event does exist add the rules of the event passed in to the existing event,
        // if their are duplicate rules raise an error
        var currentEvent=this.events[eventDef["eventName"]];
        var ruleName
        for(ruleName in eventDef.rules){
            if(currentEvent[ruleName]===undefined){
                currentEvent[ruleName]=eventDef.rules[ruleName];
            }
            else if (ruleName===undefined){
                continue;
            }
            else{
                throw{
                    name:"DuplicateRule",
                    message:"The Rule " +ruleName+ " already exists for the event " +currentEvent.eventName
                }
            }
        }
    }
    // if the event does not just add it to the dispatcher.
    else{
        this.events[eventDef["eventName"]]=eventDef;                    
    }
    
};

/**
 *  This function takes a list of requiredParameters and an object and checks
 *  to make sure that the parameters exist.
 */
CentralDispatcher.prototype.checkRequiredParams=function(requiredParams,obj){
    var missingParams=[];
    var paramName;   
    for(var i=0; i<requiredParams.length;i++){
        paramName=requiredParams[i];
        if(obj[paramName]===undefined){
            missingParams.push(paramName);
        }
    }
    if(missingParams.length>0){
        var errorMsg="The following required parameters are missing:" + missingParams.join(',');
        return{
            "valid":false,
            "message":errorMsg
        }
    }
    else{
        return{
            "valid":true
        }
    }
}

