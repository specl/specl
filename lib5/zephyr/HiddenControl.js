Zephyr.HiddenControl=function(options){
    Zephyr.BaseControl.call(this,options);
    if(options.size!==undefined){
        this.size=options.size;
    }
    
    if(options.maxSize!==undefined){
        this.maxSize=options.maxSize;
    }
    
    if(options.value===undefined){
        this.value="";
    }
}

// inherit methods from parent.
Zephyr.HiddenControl.prototype=new Zephyr.BaseControl({});

// override parent methods
Zephyr.HiddenControl.prototype.render=function(){
    this.el=document.createElement("input");
    this.el.setAttribute("type", "hidden");
    this.el.setAttribute("id", this.name);
    this.el.setAttribute("size", this.size);
    this.el.setAttribute("maxlength", this.maxSize);
    this.el.setAttribute("value", this.value)
    this.el.setAttribute("class",this.cssClass);
    return this.el;    
}


Zephyr.HiddenControl.prototype.setValue=function(value){
    this.el.value=value;        
}

Zephyr.HiddenControl.prototype.getValue=function(){            
    return this.el.value;
}



