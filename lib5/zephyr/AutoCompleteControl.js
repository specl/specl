// An autocomplete control is a combination of a text control, a YUI datasource and a YUI autocomplete
Zephyr.AutoCompleteControl=function(options){
    Zephyr.BaseControl.call(this,options);
    this.textControl=new Zephyr.TextControl(options);
    this.textControl.cssClass="ZephyrAutoCompleteText";
    this.textControl.name=options.name+"_textControl";
    
    
    if(options.url!==undefined){
        this.url=options.url;
    }
    else{
        this.url="";
    }
    
    if(options.objDef!==undefined){
        this.objDef=options.objDef
    }else{
        this.objDef="";
    }
    
    
    if(options.datasource!==undefined){
        this.datasource=options.datasource;
    }
    else{
        this.datasource=new YAHOO.util.DataSource(options.url,{
                    responseType:YAHOO.util.XHRDataSource.TYPE_JSON,
                    responseSchema:{resultsList:'data', fields:['value']},
                    maxCacheEntries:500
                });
    }
    
    if(options.autoCompleteOpts!==undefined){
        this.autoCompleteOpts=options.autoCompleteOpts
    }
    else{
        this.autoCompleteOpts={
            "forceSelection":false,
            "typeAhead":false,
            "minQueryLength":1,
            "maxResultsDisplayed":20,
            "generateRequest":function(sQuery) { return "&fieldId=" + options.name + "&query=" + sQuery +"&objDef="+options.objDef ; },
            "formatResult":function(oResultItem, sQuery) 
            {                           
                return oResultItem[0] ;
            }
        }
    }
}

Zephyr.AutoCompleteControl.prototype=new Zephyr.BaseControl({});

Zephyr.AutoCompleteControl.prototype.render=function(){
    this.el=document.createElement("span");
    this.el.setAttribute("id",this.name);
    this.textEl=this.textControl.render();
    // YUI autocomplete requires a container element in which to display to results
    this.containerEl=document.createElement("span")
    this.containerEl.setAttribute("id",this.name+"_resultsContainer");
    this.el.appendChild(this.textEl);
    this.el.appendChild(this.containerEl);  
    
    if(this.readonly===false){
        // YUI components only work when the HTML element has already been rendered to the browser, so
        // you must wait until the html elements have been rendered before initializing the YUI component
        YAHOO.util.Event.onAvailable(this.textEl.id,this.buildAutoComplete,this,true);    
    }
    
    return this.el;
}

Zephyr.AutoCompleteControl.prototype.buildAutoComplete=function(){
   this.autocompleteObj=new YAHOO.widget.AutoComplete(this.textEl,this.containerEl,this.datasource,this.autoCompleteOpts);           
}

Zephyr.AutoCompleteControl.prototype.setValue=function(value){
    this.textEl.value=value;        
}

Zephyr.AutoCompleteControl.prototype.getValue=function(){            
    return this.textEl.value;
}

// This is the default for non-compound elements
Zephyr.AutoCompleteControl.prototype.displayError=function(errorMsg){   
    YAHOO.util.Dom.addClass(this.textEl,"ZephyrErrorOutlineField");
}

// This is the default for non-compound elements
Zephyr.AutoCompleteControl.prototype.clearError=function(){
    YAHOO.util.Dom.removeClass(this.textEl,"ZephyrErrorOutlineField");
}
