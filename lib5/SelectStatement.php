<?php

// A subquery can return a scalar (a single value), a single row, a single column, or a table
// Subqueries can be found anywhere these value types can be substituted.


// A SQL statement can be a subquery, a SQL function, or a SQL store procedure.

// Anywhere $column is found it can be a column name or a SQL statement that returns a column

// Anywhere $table is found it can be a table name or a SQL statement that returns a table

// Anywhere $value is found it can be a scalar value or a SQL statement that returns a scalar value

// Whether a substatement has parenthesis or not is dependent on the location in the SQL

// In a spec all of the columns, tables and values would be strings.
// To validate these statements you would have to execute each substatement in SQL

// Column and Tablenames that are not SQL must be validated and escaped properly.

// SQL could be a spec object or a SQL string.



class SQLSelectStatement
{
    public function __construct()
    {
        $this->columns=array();
        $this->tables=array();
        $this->joins=array();
        $this->whereStatements=array();
        $this->groupByStatement=array();
        $this->limitStatement=null;
        $this->orderStatement=null;
        $this->selectDistinct=false;
    } 
}

class SQLJoin
{
    public function __construct($table1,$table2, $column1,$column2,
            $joinType="inner",$joinDirection="left")
    {
        $this->table1=$table1;
        $this->table2=$table2;
        $this->column1=$column1;
        $this->column2=$column2;
        $this->joinType=$joinType;
        $this->joinDirection=$joinDirection;
    }

}

class SQLLimit
{
    public function __construct($numberOfRows,$offset=null)
    {
        $this->numberOfRows=$numberOfRows;
        $this->offset=$offset;
    }
}

class SQLGroupBy
{
    public function __construct($columns)
    {
        $this->columnNames=$columns;
    }
}


class SQLOrderBy
{
    public function __construct($columns,$direction="Asc")
    {
        $this->columns=$columns;
        $this->direction=$direction;
    }
}

class SQLScalar
{
    public function __construct($value,$isSQL=false)
    {
        $this->value=$value;
        $this->isSQL=$isSQL;
    }
}


?>