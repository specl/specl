<?php

class ComponentViewer {

    public $theModel;
    public $theController;
    public $outerDivClass;
    public $outerDivId;

    public function __construct($theController) {
        $this->theModel = Null;
        $this->theController = $theController;
        $this->outerDivClass = Null;
        $this->outerDivId = Null;
    }

    public function render() {
        $this->recoverProperties();
        $this->renderStart();
        $this->renderEnd();
    }

    public function displayHeader() {
        echo "<!-- ComponentViewer->renderHeader for {$this->theController->id} -->\n";
    }

    protected function recoverProperties() {
        //$reg = Registry::instance();
        //$reg->getClassProperties($this);
    }

    protected function renderStart() {
        if ($this->outerDivClass) {
            echo '<div class="' . $this->outerDivClass . '">';
        } else if ($this->outerDivId) {
            echo '<div id="' . $this->outerDivId . '">';
        }
    }

    protected function renderEnd() {
        if (($this->outerDivClass) || ($this->outerDivId)) {
            echo '</div>';
        }
    }

}

?>