<?php
/**
 * Description of IncludeManager
 *
 * @author tony
 */
class InlineJSManager {    
    private $js;
    
    public function __construct() {
        $this->js = array();
    }
    
    /*
     * Add JS to the beginning of the array so it is rendered first.
     */
    public function prependJS($js){
        array_unshift($this->js, $js);
    }
        
    public function addJS($js) {
        $this->js[] = $js;
    }
    
    public function render($strbld) {
        foreach($this->js as $js) {
            $strbld->addline("
<script>{$js}</script>");
        }
    }
}
?>
