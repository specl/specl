<?php
/**
 * Description of MetaTagsManager
 *
 * @author tony
 */
class MetaTagsManager {
        
    public $author;
    public $description;
    public $keywords;
    public $keywordStr;
    public $generator;
    public $revised;
    public $copyright;
    public $expires;
    public $refresh;
    public $robots;
    public $googlebot;
    
    public function __construct() {        
        $this->author = null;
        $this->description = null;
        $this->keywords = array();
        $this->keywordStr = null;
        $this->generator = null;
        $this->revised = null;
        $this->copyright = null;
        $this->expires = null;
        $this->refresh = null;
        $this->robots = null;
        $this->googlebot = null;        
    }
    
    public function setAuthor($value) {
        $this->author = $value;
    }
    
    public function addKeyword($keyword) {
        $keyword = strtolower($keyword);                
        $key = array_search($keyword, $this->keywords);
        if ($key == false) {
            $this->keywords[] = $keyword;
        }
    }
    
    public function render($strbld) {        
        
        $strbld->addLine("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">");

    
        if ($this->author) {
            $strbld->addLine("<meta name=\"author\" content=\"{$this->author}\"/>");
        }
        
        if ($this->description) {
            $strbld->addLine("<meta name=\"description\" content=\"{$this->description}\"/>");
        }
        
        $this->keywordStr = implode(",", $this->keywords);
                
        if ($this->keywordStr) {
            $strbld->addLine("<meta name=\"keywords\" content=\"{$this->keywordStr}\"/>");
        }
        
        if ($this->generator) {
            $strbld->addLine("<meta name=\"generator\" content=\"{$this->generator}\"/>");
        }
        
        if ($this->revised) {
            $strbld->addLine("<meta name=\"revised\" content=\"{$this->revised}\"/>");
        }
    }
    
}

?>
