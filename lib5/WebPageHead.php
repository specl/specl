<?php
/**
 * Description of PageHead
 *
 * @author tony
 */
class WebPageHead {

    public $metaTags;
    public $cssIncludes;
    public $jsIncludes;
    public $cssInline;
    public $jsInline;
    
    public function __construct() {
    
        $this->metaTags = new MetaTagsManager();       
        $this->cssIncludes = new IncludeManager("CSS");
        $this->jsIncludes = new IncludeManager("JS");
        $this->cssInline = new InlineCSSManager();
        $this->jsInline = new InlineJSManager();
    }    
    
    public function render($strbld) {
        
        $this->renderStart($strbld);                        
        
        $this->metaTags->render($strbld);
        $this->cssIncludes->render($strbld);
        $this->jsIncludes->render($strbld);
        $this->cssInline->render($strbld);
        $this->jsInline->render($strbld);                
                        
        $this->renderEnd($strbld);
        
        $strbld->decreaseIndentLevel();
    }    
    
    
    public function renderStart($strbld) {

        $strbld->addLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
        $strbld->addLine("<html>");
        $strbld->increaseIndentLevel();
        $strbld->addLine("<head>");
        $strbld->increaseIndentLevel();
    }
    
    public function renderEnd($strbld) {
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</head>");   
        $strbld->decreaseIndentLevel();        
    }    
}

?>
