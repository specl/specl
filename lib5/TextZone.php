<?php

class TextZone extends Component {
    
    public $title=null;
    public $text=null;
    
    public function __construct($params) 
    {
        parent::__construct($params);
        
        if(isset($params["title"])){
            $this->title=$params["title"];
        }
        
        if(isset($params["text"])){
            $this->text=$params["text"];
        }
    }
            
    public function render($strbld=null)
    {
        
        $strbld->addLine("<table class=\"layoutTable\"><tr><td>");
        
        $strbld->addLine("<div class=\"textBox\">");
            
        if (!is_null($this->title)) {
            $strbld->addLine("<h2>{$this->title}</h2>");
        }
        
        if (!is_null($this->text)) {
            $strbld->addLine("<p>{$this->text}</p>");
        }
        
        $strbld->addLine("</div>");
        $strbld->addLine("</tr></td></table>");
    }
}

?>
