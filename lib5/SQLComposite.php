<?php

/** @ObjDefAnn(objDefId="SQLComposite")  
 */
class SQLComposite extends SQLWhereClause{
    public function __construct(){
        $this->clauses=array();
    }
        
    public function addClause($SQLWhereClause,$conjunction="AND") {
        
        if(is_a($SQLWhereClause,"SQLWhereClause")){
            $SQLWhereClause->conjunction=$conjunction;
            $this->clauses[]=$SQLWhereClause;
        }
    }
    
    public function generateSQLClause(){
       
        $SQL="(";
        
        foreach($this->clauses as $clause){
            if($SQL!="("){
                $SQL.=" {$clause->conjunction} ";
            }
            $SQL.=$clause->generateSQLClause();
        }
        $SQL.=")";
        
        return $SQL;
    }
}



?>
