<?php
class FormComponent extends Component {
    
    public function __construct($id, $parentObj) {
        parent::__construct($id, $parentObj);
        $this->hidden=false;        
    }
        
    public function render($pageObj) {        
        $this->pageObj = $pageObj;        
        echo "FormComponent->render()<br/>";
    }
        
    public function hideForm() {
        global $app;
        // TODO: handle session variables
        $sessionVar=$this->id.App::ZONESSEP."hidden";
        $app->sessionStore->store($sessionVar,true);
    }
    
    public function validateData() {        
        
    }    
}
?>
