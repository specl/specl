<?php

class CRUDGridComponent extends DataGridComponent {
    
    public $enableEdit;
    public $enableDelete;
    public $enableAdd;
    public $enableAddInGrid;
    public $enableView;
    public $objDef;
    public $onEditAction;
    public $onDeleteAction;
    public $addButtonCallback;
    public $addLinkCallback;
    public $editButtonCallback;
    
    public function __construct($params){
        parent::__construct($params);
        
        if(isset($params["enableEdit"])){
            $this->enableEdit=$params["enableEdit"];
        }
        else{
            $this->enableEdit=false;
        }
        
        if(isset($params["enableDelete"])){
            $this->enableDelete=$params["enableDelete"];
        }
        else{
            $this->enableDelete=false;
        }
        
        if(isset($params["enableAdd"])){
            $this->enableAdd=$params["enableAdd"];
        }
        else{
            $this->enableAdd=false;
        }

        if(isset($params["enableAddInGrid"])){
            $this->enableAddInGrid=$params["enableAddInGrid"];
        }
        else{
            $this->enableAddInGrid=false;
        }
        
        if(isset($params["enableView"])){
            $this->enableView=$params["enableView"];
        }
        else{
            $this->enableView=false;
        }
        
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        }
        else{
            throw new Exception("the parameter 'objDef' is required");
        }

        if(isset($params["onEditAction"])){
            $this->onEditAction=$params["onEditAction"];
        }
        else{
            $this->onEditAction="getRow2";
        }
        
        if(isset($params["onDeleteAction"])){
            $this->onDeleteAction=$params["onDeleteAction"];
        }
        else{
            $this->onDeleteAction="deleteRow";
        }
        
        if(isset($params["addButtonCallback"])){
            $this->addButtonCallback=$params["addButtonCallback"];
        }
        else{
            $this->addButtonCallback="{$this->id}_onAddButton";
        }

        if(isset($params["addLinkCallback"])){
            $this->addLinkCallback=$params["addLinkCallback"];
        }
        else{
            $this->addLinkCallback="{$this->id}_onAddLink";
        }

        
        if(isset($params["editButtonCallback"])){
            $this->editButtonCallback=$params["editButtonCallback"];
        }
        else{
            $this->editButtonCallback="{$this->id}_onEdit";
        }
        
        if(isset($params["deleteButtonCallback"])){
            $this->deleteButtonCallback=$params["deleteButtonCallback"];
        }
        else{
            $this->deleteButtonCallback="{$this->id}_onDelete";
        }
        
        if(isset($params["viewButtonCallback"])){
            $this->viewButtonCallback=$params["viewButtonCallback"];
        }
        else{
            $this->viewButtonCallback="{$this->id}_onView";
        }
        
        $this->addIncludes();
        $this->buildSubComponents();
    }
    
    public function addIncludes(){
        $this->pageObj->addInputExIncludes();
        $this->pageObj->addDataTableIncludes();
        //$this->addInputExDataTableIncludes(); 
        $this->pageObj->addCustomDataTableIncludes();
    }
    
    
    public function buildSubComponents(){
        parent::buildSubComponents();
        
        if($this->enableEdit){
          //  $this->buildEditComponents();
        }
        
        if($this->enableAdd){
            $params=new ParamBuilder();
            $params->add("id","{$this->id}_addButtonRedirect");                        
            if($this->objDef->hasAttribute("title")){
                $title= $this->objDef->getAttribute("title");
            } else{
                $title= $this->objDef->name;
            }                                
            $params->add("text","Add {$title}");                        
            $params->add("clickCallback",$this->addButtonCallback);
            $this->addButton=new ButtonComponent($params->getParams());
            $this->addComponent($this->addButton);
           // $this->buildAddComponents();
        }
                
        if($this->enableDelete){
            $this->buildDeleteComponents();
        }
    }
    
    public function buildEditComponents(){
        $fb=new InputExFormBuilder();
        $this->editForm=$fb->build("{$this->id}_EditForm","edit",$this->objDef,$this->pageObj);
        $this->editForm->pageObj=$this->pageObj;
        $this->editForm->formDef=$this->objDef;
        $this->editForm->registerCallback("onFormEditValidCallback", $this, "onFormEditValidCallback", null);
        $this->editForm->cancelCallback="{$this->id}_onEditCancel";
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_EditPanel");
        $params->add("parentObj",$this);
        $params->add("pageObj",$this->pageObj);
        $params->add("title","Edit Row");
        $this->editPanel=new PopupComponent($params->getParams());
        
        $this->editPanel->addComponent($this->editForm);
        
        $this->addComponent($this->editPanel);
    }
    
    public function buildAddComponents(){
        $fb=new InputExFormBuilder();
        $this->addForm=$fb->build("{$this->id}_AddForm","add",$this->objDef,$this->pageObj);
        $this->addForm->pageObj=$this->pageObj;
        $this->addForm->formDef=$this->objDef;
        $this->addForm->registerCallback("onFormAddValidCallback", $this, "onFormAddValidCallback", null);
        $this->addForm->cancelCallback="{$this->id}_onAddCancel";
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_AddPanel");
        $params->add("parentObj",$this);
        $params->add("pageObj",$this->pageObj);
        $params->add("title","Add Row");
        $this->addPanel=new PopupComponent($params->getParams());
        
        $this->addPanel->addComponent($this->addForm);
        
        $this->addComponent($this->addPanel);
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_addButton");
        $params->add("text","Add");
        $params->add("clickCallback",$this->addButtonCallback);
        
        $this->addButton=new ButtonComponent($params->getParams());
        $this->addComponent($this->addButton);
    }
            
    public function buildDeleteComponents(){
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_DeletePanel");
        $params->add("parentObj",$this);
        $params->add("pageObj",$this->pageObj);
        $params->add("title","Delete Results");
        $this->deletePanel=new PopupComponent($params->getParams());
        
        $this->addComponent($this->deletePanel);
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_DeleteMessage");
        $messageDiv=new DivComponent($params->getParams());
        $this->deletePanel->addComponent($messageDiv);
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_DeleteOkButton");
        $params->add("text","OK");
        $params->add("clickCallback","{$this->id}_onDeleteOk");
        $okButton=new ButtonComponent($params->getParams());
        $this->deletePanel->addComponent($okButton);
        
    }
    
    public function renderDataGridConfig($strbld){
        parent::renderDataGridConfig($strbld);
        
        if($this->enableAddInGrid){
            $strbld->addLine("{$this->id}_Config.showAddColumn=true");
        }
        
        if($this->enableEdit){
            $strbld->addLine("{$this->id}_Config.showEditColumn=true");
        }
        
        if($this->enableDelete){
            $strbld->addLine("{$this->id}_Config.showDeleteColumn=true");
        }
        
        if($this->enableView){
            $strbld->addLine("{$this->id}_Config.showViewColumn=true");
        }
    }
    
    public function render($strbld){
        $this->renderHTML($strbld);
        $this->renderCSS($strbld);
        $this->renderJavascript($strbld);
    }
    
    public function renderHTML($strbld){
        
        // render add button on top of datagrid
        if($this->enableAdd){
            $this->addButton->renderHTML($strbld);
          //  $this->addPanel->renderHTML($strbld);
        }
        
        parent::renderHTML($strbld);
                               
        if($this->enableEdit){
          //  $this->editPanel->renderHTML($strbld);
        }
        
        if($this->enableDelete){
            $this->deletePanel->renderHTML($strbld);
        }
        
    }
    
    public function renderCSS($strbld){
        parent::renderCSS($strbld);
        if($this->enableAdd){
         //   $this->addPanel->renderCSS($strbld);
        }
        
        if($this->enableEdit){
         //   $this->editPanel->renderCSS($strbld);
        }
        
        if($this->enableDelete){
            $this->deletePanel->renderCSS($strbld);
        }
        
    }
    
    public function renderJavascript($strbld){
        parent::renderJavascript($strbld);
        
        if($this->enableAdd){
            $this->addButton->renderJavascript($strbld);
        }
        
        if($this->enableAddInGrid){
            //$this->renderAddCancelCallback($strbld);
            //$this->addPanel->renderJavascript($strbld);
            //$this->renderAddCallback($strbld);
            //$this->addButton->renderJavascript($strbld);
            $this->renderAddLinkCallback($strbld);
            //$this->renderAddSuccessCallback($strbld);
        }

        if($this->enableEdit){
            //$this->renderEditCancelCallback($strbld);
            //$this->editPanel->renderJavascript($strbld);
            $this->renderEditCallback2($strbld);
            //$this->renderEditSuccessCallback($strbld);
        }
        if($this->enableDelete){
            $this->renderDeleteCallback($strbld);
            $this->renderDeleteCSS($strbld);
            $this->deletePanel->renderJavascript($strbld);
            $this->renderDeleteOkCallback($strbld);
        }
        
        if($this->enableView){
            $this->renderViewCallback($strbld);
        }
    }
    
    public function renderAddCallback($strbld){
        $strbld->addLines(
"
<script>
   {$this->id}_onAdd=function(e){
        {$this->addPanel->id}.center();
        {$this->addPanel->id}.show();
   }
</script>
");
    }
    
    public function renderEditCallback($strbld){
        $jsonKeys=json_encode($this->keys);
        $strbld->addLines(
"
<script>
    YAHOO.util.Event.addListener(window,'load',function(){
        {$this->id}.subscribe('initEvent',function(){
        
            {$this->id}_onEdit=function(type,args){
                
                var rowIndex=this.getTrIndex(args[0])

                var record = this.getRecord(rowIndex);
                
                var keys={$jsonKeys};
                var keyValues={};
                var key, origKey;

                for(key in keys)
                {
                   origKey=key + '_original';
                   keyValues[key]=record.getData(origKey);
                }
            
                var keysVal=YAHOO.lang.JSON.stringify(keyValues);
                var ajaxURL='index.php?pageId={$this->pageObj->id}&controllerId={$this->dataSourceId}&actionName={$this->onEditAction}';
                ajaxURL+='&keys=' + keysVal;
                
                var onSuccess=function(response){
                    var results=YAHOO.lang.JSON.parse(response.responseText);
                    var row = results.data;
                    
                    {$this->id}.selectRow(record);
                    // populate the hidden fields.

                    var newkey;

                    for(key in this.keys)
                    {
                        newkey = 'original-' + key;
                        row[newkey] = row[key];
                    }

                    {$this->editForm->id}.setValue(row);
                    {$this->editPanel->id}.center();
                    {$this->editPanel->id}.show();
                }
                
                var onFailure=function(response){alert('fail'); return false;};
                    
                var callback={
                    success:onSuccess,
                    failure:onFailure
                }
                
               YAHOO.util.Connect.asyncRequest('GET',ajaxURL,callback);
            }
                   
            {$this->id}.editClicked.subscribe({$this->editButtonCallback},scope=this);
        });
    });
</script>
");
    }
    

    
    public function renderAddLinkCallback($strbld){        
        $strbld->addLines(
"
<script>
    YAHOO.util.Event.addListener(window,'load',function(){
        {$this->id}.subscribe('initEvent',function(){
            {$this->id}.addClicked.subscribe({$this->addLinkCallback},scope=this);
        });
    });
</script>
");
    }
    
     public function renderEditCallback2($strbld){
        $jsonKeys=json_encode($this->keys);
        $strbld->addLines(
"
<script>
    YAHOO.util.Event.addListener(window,'load',function(){
        {$this->id}.subscribe('initEvent',function(){
            {$this->id}.editClicked.subscribe({$this->editButtonCallback},scope=this);
        });
    });
</script>
");
    }
    
    public function renderViewCallback($strbld){
        $jsonKeys=json_encode($this->keys);
        $strbld->addLines(
"
<script>
    YAHOO.util.Event.addListener(window,'load',function(){
        {$this->id}.subscribe('initEvent',function(){
            {$this->id}.viewClicked.subscribe({$this->viewButtonCallback},scope=this);
        });
    });
</script>
");
    }
    
    public function renderAddCancelCallback($strbld){
        $strbld->addLines(
"
<script>
    {$this->id}_onAddCancel=function(type,args){
        {$this->addForm->id}.clear();
        {$this->addForm->id}.clearErrorMessages();
        {$this->addPanel->id}.hide();
        return false;
    }
        
    YAHOO.util.Event.addListener(window,'load',function(){   
        YAHOO.util.Event.addListener({$this->addPanel->id}.close,'click',{$this->id}_onAddCancel);
    });
</script>
");
    }
    
    public function renderEditCancelCallback($strbld){
        $strbld->addLines(
"
<script>
    {$this->id}_onEditCancel=function(type,args){
        {$this->editForm->id}.clear();
        {$this->editForm->id}.clearErrorMessages();
        {$this->id}.unselectAllRows();
        {$this->editPanel->id}.hide();
        
        
        return false;
    }
     
    YAHOO.util.Event.addListener(window,'load',function(){  
        YAHOO.util.Event.addListener({$this->editPanel->id}.close,'click',{$this->id}_onEditCancel);
    });
</script>
");
    }
    
    
    public function renderEditSuccessCallback($strbld){
        $strbld->addLines(
"
<script>
    YAHOO.util.Event.addListener(window,'load',function(){  
        {$this->editForm->id}.submitSuccess.subscribe(function(e){
            {$this->id}.unselectAllRows();
            {$this->id}.refreshData({$this->id}.whereStatement);
            {$this->editPanel->id}.hide();
        });
    });
</script>
");
    }
    
    public function renderAddSuccessCallback($strbld){
        $strbld->addLines(
"
<script>
    YAHOO.util.Event.addListener(window,'load',function(){  
        {$this->addForm->id}.submitSuccess.subscribe(function(e){
            {$this->id}.refreshData({$this->id}.whereStatement);
            {$this->addPanel->id}.hide();
            {$this->addForm->id}.clear();
            {$this->addForm->id}.clearErrorMessages();
        });
    });
</script>
");
    }
    
    public function renderDeleteCallback($strbld){
         $jsonKeys=json_encode($this->keys);
         $strbld->addLines(
"
<script>
    YAHOO.util.Event.addListener(window,'load',function(){
        {$this->id}.subscribe('initEvent',function(){
            {$this->id}_onDelete=function(type,args){
                var rowIndex=this.getTrIndex(args[0])
                var record = this.getRecord(rowIndex);
                {$this->id}.selectRow(record);
                
                if(confirm('Delete the Selected Row?')){
                
                    var rowIndex=this.getTrIndex(args[0])
                    var record = this.getRecord(rowIndex);
                    var keys={$jsonKeys};
                    var keyValues={};
                    var key;

                    for(key in keys)
                    {
                       origKey=key + '_original';
                       keyValues[key]=record.getData(origKey);
                    }

                    var keysVal=YAHOO.lang.JSON.stringify(keyValues);
                    var ajaxURL='index.php?pageId={$this->pageObj->id}&controllerId={$this->dataSourceId}&actionName={$this->onDeleteAction}&buildPage=false';
                    ajaxURL+='&keys=' + keysVal;

                    var onSuccess=function(response){
                        var results=YAHOO.lang.JSON.parse(response.responseText);                       
                    
                        // check status of message.
                        var messageType;
                        var validMessage;
                        
                        var messageDiv=YAHOO.util.Dom.get('{$this->id}_DeleteMessage');
                        if(!YAHOO.lang.isUndefined(results.messageType)) {
                            messageType=results.messageType;                           
                            validMessage = true;
                    
                            if (messageType=='failure') {
                                var errors=results.errorMessages;
                                var msg='';
                                var key;
                                for(key in errors)
                                {
                                    msg += errors[key];                                    
                                }  
        
                                messageDiv.innerHTML=msg;
                                {$this->deletePanel->id}.center();
                                {$this->deletePanel->id}.show();        
       
                            } else if (messageType == 'success') { 
                                messageDiv.innerHTML='Row Deleted Successfully';
                            }
                        } else {
                            validMessage = false;
                            // this is not a valid message...something could be 
                            // wrong with server side...
                        }
                    
                        {$this->id}.unselectAllRows();
                        {$this->id}.refreshData({$this->id}.whereStatement);                    
                    }

                    var onFailure=function(response) {
                        alert('fail'); return false;
                    };

                    var callback={
                        success:onSuccess,
                        failure:onFailure
                    }

                   YAHOO.util.Connect.asyncRequest('GET',ajaxURL,callback);
                    
                    
                } else {
                    {$this->id}.unselectRow(record);
                    return;
                }
            };
        {$this->id}.deleteClicked.subscribe({$this->deleteButtonCallback});
        });
     });
</script>
");
    }
    
    public function renderDeleteCSS($strbld){
        $strbld->addLines(
"
<style>
                
    #{$this->deletePanel->id} .bd {
        text-align:center;
        max-width: 25em;
    }
    #{$this->id}_DeleteOkButton {
        margin-top: 1em;
        
    }
</style>
");
    }
    
    public function renderDeleteOkCallback($strbld){
              $strbld->addLines(
"
<script>
    {$this->id}_onDeleteOk=function(type,args){
        var messageDiv=YAHOO.util.Dom.get('{$this->id}_DeleteMessage');
        messageDiv.innerHTML='';
        {$this->deletePanel->id}.hide();
        return false;
    }
     
    YAHOO.util.Event.addListener(window,'load',function(){  
        YAHOO.util.Event.addListener({$this->deletePanel->id}.close,'click',{$this->id}_onDeleteOk);
    });
</script>
");
    }
    
    
    public function onFormAddValidCallback($eventData,$userData) {            
                
        $tableName=$this->objDef->getAttribute("table", $this->objDef->name);
        
        $dal = NewDALObj($tableName);
 
        $results = $dal->insertTransaction($eventData);

        if ($results->success == true) {
            $this->addForm->sendSuccess(null,null);          
        } else {
            $this->addForm->sendError($results->getMessage());          
        }
     }
     
     
     public function onFormEditValidCallback($eventData,$userData) {            
                      
        $tableName=$this->objDef->getAttribute("table", $this->objDef->name);
                
        $dal = NewDALObj($tableName);
        $results = $dal->updateTransaction($eventData);        

        if ($results->success == true) {
            $this->editForm->sendSuccess(null,null);          
        } else {
            $this->editForm->sendError($results->getMessage());          
        }
     }
}
?>