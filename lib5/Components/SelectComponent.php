<?php

class SelectComponent extends Component {
    
    public $choices;
    
    public function __construct($params){
        parent::__construct($params);
        $this->choices=array();
    }
    
    public function addChoice($label,$value=null){
        if(is_null($value)){
            $value=$label;
        }
        $this->choices[]=array("label"=>$label,"value"=>$value);
    }
    
    public function render($strbld){
        $this->renderHTML($strbld);
    }
    
    public function renderHTML($strbld){
        $strbld->addLine("<select id='{$this->id}'>");
        $strbld->increaseIndentLevel();
        foreach($this->choices as $choice){
            $label=$choice["label"];
            $value=$choice["value"];
            
            $strbld->addLine("<option value='{$value}'>{$label}</option>");
        }
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</select>");
    }
}

?>