<?php
// this class is not used.
class GridPopupComponent extends PopupComponent {
    
    public $objDef;
    public $pageObj;
    public $rowsPerPage;
    public $selectRowsCallback;
    public $gridSearchComponent;
    public $selectRowsButton;
    public $checkButtonGroup;
    
    public function __construct ($params){
        parent::__construct($params);
        
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        }
        else{
            throw new Exception("ObjDef is a required Parameter");
        }
        
        if(isset($params["pageObj"])){
            $this->pageObj=$params["pageObj"];
        }
        else{
            throw new Exception("pageObj is a required Parameter");
        }
        
        if(isset($params["rowsPerPage"])){
            $this->rowsPerPage=$params["rowsPerPage"];
        }
        else{
            $this->rowsPerPage=20;
        }
        
        if(isset($params["selectRowsCallback"])){
            $this->selectRowsCallback=$params["selectRowsCallback"];
        }
        else{
            throw new Exception("The parameter 'selectRowsCallback' is required");
        }
        
        $this->initializeSubComponents();
    }
    
    public function initializeSubComponents(){
        $this->initializeGridSearchComponent();
        $this->initializeSelectRowsButton();
        $this->initializeCheckButtons();        
    }
    
    public function initializeGridSearchComponent(){
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_SearchGrid");
        $params->add("objDef",$this->objDef);
        $params->add("parentObj",$this);
        $params->add("pageObj",$this->pageObj);
        $params->add("rowsPerPage",$this->rowsPerPage);
        $this->gridSearchComponent=new GridSearchComponent($params->getParams());
        $this->addComponent($this->gridSearchComponent);
    }
    
    public function initializeSelectRowsButton(){
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_select");
        $params->add("text","Select Rows");
        $params->add("clickCallback",$this->selectRowsCallback);
        $this->selectRowsButton=new ButtonComponent($params->getParams());
        $this->addComponent($this->selectRowsButton);
    }
    
    public function initializeCheckButtons(){
        $params=new ParamBuilder();
        $params->add("id", "{$this->id}_checkButtons");
        $this->checkButtonGroup=new ButtonGroupComponent($params->getParams());
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_selectAll");
        $params->add("text","Select All");
        $params->add("clickCallback","{$this->gridSearchComponent->id}_Grid.checkAll");
        $params->add("callbackScope","{$this->gridSearchComponent->id}_Grid");
        $selectAllButton=new ButtonComponent($params->getParams());
        $this->checkButtonGroup->addButton($selectAllButton);
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_clearAll");
        $params->add("text","Clear All");
        $params->add("clickCallback","{$this->gridSearchComponent->id}_Grid.unCheckAll");
        $params->add("callbackScope","{$this->gridSearchComponent->id}_Grid");
        $clearAllButton=new ButtonComponent($params->getParams());
        $this->checkButtonGroup->addButton($clearAllButton);
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_cancel");
        $params->add("text","Cancel");
        $params->add("clickCallback","{$this->id}_cancel");
        $clearAllButton=new ButtonComponent($params->getParams());
        $this->checkButtonGroup->addButton($clearAllButton);
        
        $this->addComponent($this->checkButtonGroup);
    }
    
   
    
    public function renderCSS($strbld){
        parent::renderCSS($strbld);
        $strbld->addLines(
"
<style>
    .yui-panel .bd button{
        margin-top: 0.5em;
    }
</style>
");
    }
    
    public function renderJavascript($strbld){
        parent::renderJavascript($strbld);
        $strbld->addLines(
"
<script>
    {$this->id}_cancel=function(e){
        {$this->gridSearchComponent->id}_Search.setValue({{$this->gridSearchComponent->id}_Search_search:[{}]});
        {$this->gridSearchComponent->id}_Search.onSubmit(e);
        {$this->id}.hide();
    }  
</script>
");
    }
}

?>