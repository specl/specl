<?php

// TODO: add parameters to set the height and width
class PopupComponent extends Component {
    
    public $title;
    public $resizable;
    public $autosize;
    public $height;
    public $width;
    
    public function __construct($params){
        parent::__construct($params);
        if(isset($params["title"])){
            $this->title=$params["title"];
        }
        else{
            $this->title="";
        }
        
        if(isset($params["resizeable"])){
            $this->resizable=$params["resizeable"];
        }
        else{
            $this->resizable=true;
        }
        
        if(isset($params["modal"])){
            $this->modal=$params["modal"];
        }
        else{
            $this->modal=true;
        }
        
    }
    
    public function render($strbld){
        $this->renderCSS($strbld);
        $this->renderHTML($strbld);
        $this->renderJavascript($strbld);
       
    }
    
    public function renderHTML($strbld){
        $strbld->addLine("<div id='{$this->id}'>");
        $strbld->increaseIndentLevel();
        if(trim($this->title)!=""){
            $this->renderHTMLHeader($strbld);
        }
        $this->renderHTMLBody($strbld);
        $this->renderHTMLFooter($strbld);
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</div>");
    }
    
    public function renderHTMLHeader($strbld){
       $strbld->addLine("<div class='hd'> {$this->title}</div>");
    }
    
    public function renderHTMLBody($strbld){
        $strbld->addLine("<div class='bd'>");
        $strbld->increaseIndentLevel();
        foreach($this->components as $childComponent){
            $childComponent->renderHTML($strbld);
        }
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</div>");
    }
    
    public function renderHTMLFooter($strbld){
       // $strbld->addLine("<div class='ft'></div>");
    }
    
    public function renderJavascript($strbld){

        foreach($this->components as $childComponent){
            $childComponent->renderJavascript($strbld);
        }
        
        if($this->modal){
            $modal="true";
        }
        else{
            $modal="false";
        }

        $strbld->addLine("<script type='text/javascript'>");
        $strbld->increaseIndentLevel();
        $strbld->addLine("var {$this->id}=new YAHOO.widget.Panel('{$this->id}',{visible:false,close:true,draggable:true,modal:{$modal}});");
        $strbld->addLine("{$this->id}.render();");
        $this->renderSetPanelSize($strbld);
        
        
        if($this->resizable){
            $this->renderResizeJavascript($strbld);
        }
        
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</script>");
    }
    
    public function renderResizeJavascript($strbld){
        
        $strbld->addLines(
"
    var {$this->id}_resize=new YAHOO.util.Resize({$this->id}.id,{
        handles:['br'],
        status:false
    });
    
    {$this->id}_resize.on('resize',function(args){
        var panelHeight=args.height;
        var panelWidth=args.width;
        {$this->id}.cfg.setProperty('height',panelHeight + 'px');
        {$this->id}.cfg.setProperty('width',panelWidth + 'px');
        },this.dialog,true);
");
        
        
    }
    
    // sets the panel that popups up to have a height equal to the viewport and
    // a width 70% of the viewport
    public function renderSetPanelSize($strbld){
    $strbld->addLines(
"
{$this->id}.beforeShowEvent.subscribe(function(e){
    
    if({$this->id}.fitsInViewport()===false){
        var viewportHeight=YAHOO.util.Dom.getViewportHeight();
        var viewportWidth=YAHOO.util.Dom.getViewportWidth();

        var dialogHeight=(viewportHeight-100) + 'px';
        var dialogWidth=(viewportWidth * .70) + 'px';
        {$this->id}.cfg.setProperty('height',dialogHeight);
        {$this->id}.cfg.setProperty('width',dialogWidth);
    }
    
    });
");
    }
    
    
    public function renderCSS($strbld){
        $strbld->addLines(
"
<style>
    .yui-panel .bd {
        overflow:scroll;
        padding:10px;
    }
</style>
");     
        
        if($this->resizable){
            $strbld->addLines(
"
<style>

    .yui-panel .yui-resize-handle-br {
        right:0;
        bottom:0;
        height: 8px;
        width: 8px;
        position:absolute; 
    }
</style>
");
        }
        
    }
    
    
}


?>