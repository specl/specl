<?php

class DirectoryComponent extends Component {

    public $functions;
    
    public function __construct($params)
    {
        parent::__construct($params);
        $this->addAction("ls","ls");
        $this->addAction("getusers","getUsers");
        $this->addAction("getobjdef","getObjDef");
        
        
        $this->functions = array();
    }

    public function addFunction($name) {
        $this->functions[$name] = $name;
    }
    
    public function ls()
    {                
        $status=true;
        
        $response = array();

        $response['message']='list';
        $response['functions']=array();
        
        foreach($this->functions as $key=>$value) {
            $response['functions'][]=$value;
        }
        
        if ($status) {
            if ($this->hasCallback("onLSSuccess")) {
                $this->fireCallback("onLSSuccess",$response);
            } 
        } else {
            if ($this->hasCallback("onLSFailure")) {
                $this->fireCallback("onLSFailure",$response);
            }                 
        }   
    }       
    
    public function getObjDef()
    { 
        global $app;
 
 
        $response = array();

        $response['message']='getobjdef';
       
        
        if (isset($app->request->parameters['objDefId'])) {
            $objDefId = $app->request->parameters['objDefId'];
        } else { 
            $objDefId = null;
        }        
        
        $response['objDefId']=$objDefId;
         
        if ($objDefId) {
            $status=true;            
            $objDef = $app->specManager->findDef($objDefId, true);
            
            $response['objDef'] = $objDef;
            
        } else {
            $status=false;
        }
    
        
       $response['status'] = $status;
        
        if ($status) {
            if ($this->hasCallback("onGetObjDefSuccess")) {
                $this->fireCallback("onGetObjDefSuccess",$response);
            } 
        } else {
            if ($this->hasCallback("onGetObjDefFailure")) {
                $this->fireCallback("onGetObjDefFailure",$response);
            }                 
        }          
    }    
    
    
    public function getUsers()
    {                
        global $app;
        $status=true;
        
        $response = array();

        $response['message']='getusers';
        $response['users']=array();
        
        
        $sqlBuilder = new QueryBuilder();

        $dbConn = $app->getConnection();

        if (is_null($dbConn)) {
            // connection name not found or some other problem
            return;
        }

        $sql = "select user from `ACU`;";
        $sqlBuilder->setStatement($sql);        
        $sql = $sqlBuilder->bindParams();

        $results = $dbConn->select($sql);

        if ($results!=0)
        {
            $status=true;                                        
            while($row = $dbConn->getRowAssoc($results)) {
                $response['users'][] = $row['user'];
            }
        } else {
            $status=false;            
        }

        $response['status'] = $status;
        
        if ($status) {
            if ($this->hasCallback("onGetUsersSuccess")) {
                $this->fireCallback("onGetUsersSuccess",$response);
            } 
        } else {
            if ($this->hasCallback("onGetUsersFailure")) {
                $this->fireCallback("onGetUsersFailure",$response);
            }                 
        }   
    }           
}
?>