<?php

/**
 *  DataGridSelectPopupComponent2 uses the new Button Classes to send events to the Central Dispatcher
 *  instead of using hardcoded javascript callback functions.
 * 
 */
class DataGridSelectPopupComponent2 extends PopupComponent {
    
    public $whereStatement;
    public $objDef;
    public $dataSource;
    public $searchObj;
    public $rowsPerPage;
    public $grid;
    public $selectButtonText;
    public $formId;
    public $fieldName;
    public $onSelectCallback;
    public $mode;
    public $selectOneMode;
    
    public function __construct($params){
        parent::__construct($params);
        
        if(isset($params["whereStatement"])){
            $this->whereStatement=$params["whereStatement"];
        }
        else{
            $this->whereStatement=null;
        }
        
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        }
        else{
            throw (new Exception("the 'objDef' parameter is required"));
        }
        
        if(isset($params["pageObj"])){
            $this->pageObj=$params["pageObj"];
        }
        else{
            throw (new Exception("the 'pageObj' parameter is required"));
        }
        
        if(isset($params["rowsPerPage"])){
            $this->rowsPerPage=$params["rowsPerPage"];
        }
        else{
            $this->rowsPerPage=10;
        }
        
        if(isset($params["selectButtonText"])){
            $this->selectButtonText=$params["selectButtonText"];
        }
        else{
            $this->selectButtonText="Select Rows";
        }
        
        if(isset($params["selectOneMode"])){
            $this->selectOneMode=$params["selectOneMode"];
        }
        else{
            $this->selectOneMode=false;
        }
        
        if(isset($params["enableClearAll"])){
            $this->enableClearAll=$params["enableClearAll"];
        }
        else{
            $this->enableClearAll=true;
        }
        
        if(isset($params["initialLoad"])){
            $this->initialLoad=$params["initialLoad"];
        }
        else{
            $this->initialLoad=true;
        }
        
        $this->addIncludes();
        $this->buildSubComponents();
    }
    
    public function addIncludes(){
        $this->pageObj->addDataTableIncludes();
        $this->pageObj->addCustomDataTableIncludes();
        $this->pageObj->addInputExIncludes();
        $this->pageObj->addSearchFieldIncludes();
        
    }
    
    public function buildSubComponents(){
        $this->buildDataSource();
        $this->buildSearchGrid();
        $this->buildButtons();
    }
    
    public function buildDataSource(){
        
        $params= new ParamBuilder();
        $params->add("id","{$this->id}_DataSource");
        $params->add("pageObj",$this->pageObj);
        $params->add("objDef",$this->objDef);
        
        if(!is_null($this->whereStatement)){
            $params->add("whereStatement",$this->whereStatement);
        }
        
        $this->dataSource=new MySQLObjDefSource($params->getParams());
        $this->addComponent($this->dataSource);
    }
    
    public function buildSearchGrid(){
        
        // add search filter at the top of the datagrid.
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_Search");
        $params->add("parentObj",$this);
        $params->add("searchButtonName","Filter Grid");
        $params->add("clearButtonName","Clear Filter");
        $params->add("pageObj",$this->pageObj);
        $this->searchObj=new SearchComponent($params->getParams());
        $this->searchObj->addFieldsFromObjDef($this->objDef);
        $this->searchObj->objDef=$this->objDef;
        $this->searchObj->dataTableName="{$this->id}_Grid";
        $this->searchObj->build();      
        $this->addComponent($this->searchObj);
        
        // build datagrid
        
        $params = new ParamBuilder();
        $params->add("id", "{$this->id}_Grid");
        $params->add("parentObj",$this);
        $params->add("pageObj",$this->pageObj);
        $params->add("dataSourceId",$this->dataSource->id);
        $params->add("pagingEnabled",true);
        $params->add("rowsPerPage",$this->rowsPerPage);
        $params->add("dynamicData",true);
        $params->add("enableSelectRows",true);
        $params->add("selectOneMode",$this->selectOneMode);
        $params->add("enableDragDrop",false);
        $params->add("initialLoad", $this->initialLoad);
        $params->add("objDef",$this->objDef);
        
        $this->grid=new DataGridComponent($params->getParams());
        $this->grid->buildFromObjDef($this->objDef);
        $this->addComponent($this->grid);
    }
    
    public function buildButtons(){
        $selectButton=new ButtonComponent2(array(
            "id"=>"{$this->id}_select",
            "text"=>$this->selectButtonText
        ));
        $this->addComponent($selectButton);
        
        $buttonGroup=new ButtonGroupComponent(array(
            "id"=>"{$this->id}_Grid_Buttons"
        ));
        
        if($this->enableClearAll){
            $params=new ParamBuilder();
            $params->add("id","{$this->id}_clearAll");
            $params->add("text","Clear All");
            $clearAllButton=new ButtonComponent2(array(
                "id"=>"{$this->id}_clearAll",
                "text"=>"Clear All"
            ));
            $buttonGroup->addButton($clearAllButton);
        }
        
        $cancelButton=new ButtonComponent2((array(
            "id"=>"{$this->id}_cancel",
            "text"=>"Cancel"
        )));  
        $buttonGroup->addButton($cancelButton);
        $this->addComponent($buttonGroup);
    }
    
    public function renderJavascript($strbld){
        $this->renderAddActions($strbld);
        parent::renderJavascript($strbld);
    }
    
    // TODO: Add check in PHP to see if actions have been registers, and do not render
    //       the code if they have already been registered. This check should not be in javascript,
    //       but it is form now, until a beeter solution is done.
    // TODO: Add this function to the base popup class.
    public function renderCallbackFunctions($strbld){
        $this->renderAddActionShowPopup($strbld);
        $this->renderAddActionHidePopup($strbld);
    }

    /**
     * 
     *  Renders the javascript to add the components actions to the events. Before
     *  the action is added there needs to be a check to see if the action exists or
     *  an error will be raised when trying to add the action
     */
    public function renderAddActions($strbld){
        $this->renderAddActionShowPopup($strbld);
        $this->renderAddActionHidePopup($strbld);
    }
    
    
    public function renderAddActionShowPopup($strbld){
        $strbld->addLines(
"
<script>
DispatchObj.addAction({    
    actionName:'showPopup',
    requiredParameters:['elementName'],
    parameterRules:function(message){
     if(window[message['elementName']]===undefined){
            return {
                valid:false,
                message:'The element ' + message['elementName'] + ' is not defined in the global namespace'
            }
        }                    
        return{
            valid:true
        }
    },
    actionFunction:function(message){
        var popupEl=window[message['elementName']];
        popupEl.center();
        popupEl.show();
        popupEl.center();
    }    
});
</script>
");
    }
    
    public function renderAddActionHidePopup($strbld){
        $strbld->addLines(
"
<script>
DispatchObj.addAction({
    actionName:'hidePopup',
    requiredParameters:['elementName'],
    parameterRules:function(message){
        if(window[message['elementName']]===undefined){
            return {
                valid:false,
                message:'The element ' + message['elementName'] + ' is not defined in the global namespace'
            };
        }                    
        return{
            valid:true
        };
    },
    actionFunction:function(message){
        var popupEl=window[message['elementName']];
        popupEl.hide();
    }
});
</script>
");        
    }
    
    public function renderSelectCallback($strbld){
        $keyToPropNameMap=$this->grid->getKeyToPropNameMap();
        $jsonKeyMap=json_encode($keyToPropNameMap);
        
        $strbld->addLines(
"
<script>
   var {$this->id}_getRows_default=function(e){
    
        var records={$this->grid->id}.getSelectedRows();
        var key,key2;
        var recordsArray=[];
        var keyToPropMap={$jsonKeyMap};
        
        for(key in records){
            var newRecord={};
            var oldRecord=records[key];
            for(key2 in keyToPropMap){
                var propName=keyToPropMap[key2];
                var displayName='display-'+propName;
                newRecord[propName]=oldRecord[key2];
                newRecord[displayName]=oldRecord[key2];
            }
            recordsArray.push(newRecord);
        }
        var field={$this->formId}.getFieldByName('{$this->fieldName}');
        field.setValue(recordsArray);
        {$this->id}.hide();
        
    } 
</script>
");
    }
    
    public function renderCancelCallback($strbld){
        $strbld->addLines(
"
<script>
    var {$this->id}_cancel=function(e){
        {$this->id}.hide();
    }
</script>
");
    }
    
    public function renderCSS($strbld){
        parent::renderCSS($strbld);
        $strbld->addLines(
"
<style>
    .yui-panel .bd button{
        margin-top: 0.5em;
    }
</style>
");
    }
}

?>