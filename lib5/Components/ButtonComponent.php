<?php

class ButtonComponent extends Component {
    public $text;
    public $clickCallback;
    public $callbackScope;
    public $type;
    
    public function __construct($params){
        
        parent::__construct($params);
        
        if(isset($params["text"])){
            $this->text=$params["text"];
        }
        else{
            throw new Exception("the text field is required");
        }
        
        if(isset($params["clickCallback"])){
            $this->clickCallback=$params["clickCallback"];
        }
        else{
            $this->clickCallback=null;
        }
        
        if(isset($params["callbackScope"])){
            $this->callbackScope=$params["callbackScope"];
        }
        else{
            $this->callbackScope=null;
        }
        
        if(isset($params["type"])){
            $this->type=$params["type"];
        }
        else{
            $this->type="button";
        }
    }
    
    public function render($strbld){
        $this->renderHTML($strbld);
        $this->renderJavascript($strbld);
    }
    
    public function renderHTML($strbld){
        $strbld->addLine("<button type='{$this->type}' id='{$this->id}'>{$this->text}</button>");
        
    }
    
    public function renderJavascript($strbld){
        if(!is_null($this->clickCallback)){
            $strbld->addLine("<script>");
            $strbld->increaseIndentLevel();
            $strbld->addLine("YAHOO.util.Event.addListener(window,'load',function(){");
            $strbld->increaseIndentLevel();
            if(is_null($this->callbackScope)){
               $strbld->addLine("YAHOO.util.Event.addListener('{$this->id}','click',{$this->clickCallback});"); 
            }
            else{
                $strbld->addLine("YAHOO.util.Event.addListener('{$this->id}','click',{$this->clickCallback},{$this->callbackScope},true);");
            }
            
            $strbld->decreaseIndentLevel();
            $strbld->addLine("});");
            $strbld->decreaseIndentLevel();
            $strbld->addLine("</script>");
        }
    }
    
    
}

?>