<?php

class ListComponent extends Component{
    
    public $title;
    public $height;
    public $width;
    
    public function __construct($params)
    {
        parent::__construct($params);
        
        if(isset($params["title"])){
            $this->title=$params["title"];
        }
        
        if(isset($params["height"])){
            $this->height=$params["height"];
        }
        else{
            $this->height=240;
        }
        
        if(isset($params["width"])){
            $this->width=$params["width"];
        }
        else{
            $this->width=200;
        }        
        $this->pageObj->addListComponentIncludes();        
    }
    
    public function render($strbld) { 
        $this->renderCSS($strbld);
        $this->renderHTML($strbld);
        $this->renderJavascript($strbld);
    }    
    
    public function renderHTML($strbld)
    {
        $strbld->addLine("<div id={$this->id} class='draglist'></div>");

    }
    
    public function renderJavascript($strbld)
    {
        $strbld->addLines("
<script type='text/javascript'>
    {$this->id}=new CustomWidgets.ListComponent('{$this->id}','{$this->title}',true);

</script>
");
        
    }    
        
    public function renderCSS($strbld)
    {
        $strbld->addLines("
<style type='text/css'>

div.workarea { padding:10px; float:left }

ol.draglist { 
    position: relative;
    width: {$this->width}px; 
    height:{$this->height}px;
    background: #f7f7f7;
    border: 1px solid gray;
    list-style: none;
    margin-left:0.4em;
    padding:0;
    
}

ol.draglist li {
    margin: 1px;
    cursor: move;
    zoom: 1;
}

ol.draglist_alt { 
    position: relative;
    width: {$this->width}px; 
    list-style: none;
    margin:0;
    padding:0;
    /*
       The bottom padding provides the cushion that makes the empty 
       list targetable.  Alternatively, we could leave the padding 
       off by default, adding it when we detect that the list is empty.
    */
    padding-bottom:20px;
}

ol.draglist_alt li {
    margin: 1px;
    cursor: move; 
}


li.list1 {
    background-color: #D1E6EC;
    border:1px solid #7EA6B2;
}

li.list2 {
    background-color: #D8D4E2;
    border:1px solid #6B4C86;
}

#user_actions { float: right; }

</style>
");
    }
    
}

?>