<?php
/**
 *  A ConfirmationPopup is a dialog box that allows the user to choose whether they
 *  want to proceed with a procedure or not. There are two buttons, corresponding to
 *  Yes or No. Each button can have a callback associated with it.
 *  
 *  The parameters for a ConfrimationPopup are as follows:
 *      - yesText: The text of the yes button, the default value is 'Yes'
 *      - noText: The text of the no button, the default value is 'No'
 *      - yesCallback: The callback function association with the yes button, by default this just
 *                     closes the dialog box and does nothing. (This default will most likely be
 *                     changed in the future.).
 *      - noCallback: The callback function associated with the no button, by default this just
 *                    closes the dialog box and does nothing.
 * 
 *  
 */
class ConfirmationPopup extends PopupComponent {
    public function __construct($params){
         parent::__construct($params);
         
         if(isset($params["yesText"])){
             $this->yesText=$params["yesText"];
         }
         else{
             $this->yesText="Yes";
         }
         
         if(isset($params["noText"])){
             $this->noText=$params["noText"];
         }
         else{
             $this->noText="No";
         }
         
         if(isset($params["noCallback"])){
             $this->noCallback=$params["noCallback"];
         }
         else{
             $this->noCallback="{$this->id}.hide()";
         } 
         
         if(isset($params["yesCallback"])){
             $this->yesCallback=$params["yesCallback"];
         }
         else{
             $this->yesCallback="{$this->id}.hide()";
         } 
    }
    
    public function build(){
        $this->buildBody();
        $this->buildButtons();
    }
    
    /**
     *  A dialog popup has an empty div as its body where text can be inserted by javascript.
     */
    private function buildBody(){
       $div=new DivComponent(array("id"=>"{$this->id}_body"));
       $this->addComponent($div);
    }
    
    /**
     *  Build the Yes and No buttons.
     */
    private function buildButtons(){
        // add all the buttons to a button group so they are rendered on the same line in the same div.
        $buttonGroup=new ButtonGroupComponent(array("id"=>"{$this->id}_buttonGroup"));
        $yesButton=new ButtonComponent(array(
           "id"=>"{$this->id}_yesButton",
           "text"=>$this->yesText,
           "clickCallback"=>$this->yesCallback
        ));
        $buttonGroup->addButton($yesButton);
        
        $noButton=new ButtonComponent(array(
           "id"=>"{$this->id}_noButton",
           "text"=>$this->noText,
           "clickCallback"=>$this->noCallback
        ));
        $buttonGroup->addButton($noButton);
        
        $this->addComponent($buttonGroup);
    }
}

?>