<?php

class DivComponent extends Component {
    public $class;
    
    public function __construct($params){
        parent::__construct($params);
        
        if(isset($params["class"])){
            $this->class=$params["class"];
        }
        else{
            $this->class="";
        }
        
        if(isset($params["innerHTML"])){
            $this->innerHTML=$params["innerHTML"];
        }
        else{
            $this->innerHTML="";
        }
    }
    
    public function render($strbld){
        $this->renderHTML($strbld);
        $this->renderJavascript($strbld);
    }
    
    public function renderHTML($strbld){
        $strbld->addLine("<div id='{$this->id}' class='{$this->class}'>{$this->innerHTML}</div>");
    }
    
    public function renderJavascript($strbld){
        
    }
}

?>