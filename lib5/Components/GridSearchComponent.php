<?php

class GridSearchComponent extends Component {
    
    public $objDef;
    public $pageObj;
    public $gridComponent;
    public $searchComponent;
    public $dataSourceComponent;
    public $rowsPerPage;
    
    
    public function __construct($params){
        parent::__construct($params);
        
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        }
        else{
            throw new Exception("ObjDef is a required Parameter");
        }
        
        if(isset($params["pageObj"])){
            $this->objDef=$params["objDef"];
        }
        else{
            throw new Exception("pageObj is a required Parameter");
        }
        
        if(isset($params["rowsPerPage"])){
            $this->rowsPerPage=$params["rowsPerPage"];
        }
        else{
            $this->rowsPerPage=20;
        }
        
        $this->addIncludes();
        $this->initializeSubComponents();
    }
    
    public function addIncludes(){
        $this->pageObj->addDataTableIncludes();
        $this->pageObj->addInputExDataTableIncludes(); 
        $this->pageObj->addCustomDataTableIncludes();
        $this->pageObj->addSearchFieldIncludes();
    }
    
    public function initializeSubComponents(){
        $this->initializeDataSourceComponent();
        $this->initializeSearchComponent();
        $this->initializeGridComponent();  
    }
    
    public function initializeDataSourceComponent(){
        $params= new ParamBuilder();
        $params->add("id","{$this->id}_DataSource");
        $params->add("pageObj",$this->pageObj);
        $params->add("objDef",$this->objDef);
        $this->dataSourceComponent=new MySQLObjDefSource($params->getParams());
        $this->addComponent($this->dataSourceComponent);
    }
    
    public function initializeGridComponent(){
        $params = new ParamBuilder();
        $params->add("id", "{$this->id}_Grid");
        $params->add("parentObj",$this);
        $params->add("pageObj",$this->pageObj);
        $params->add("dataSourceId",$this->dataSourceComponent->id);
        $params->add("pagingEnabled",true);
        $params->add("rowsPerPage",$this->rowsPerPage);
        $params->add("dynamicData",true);
        $params->add("enableSelectRows",true);
        $params->add("enableDragDrop",false);
        $this->gridComponent=new DataGridComponent($params->getParams());
        $this->gridComponent->buildFromObjDef($this->objDef);
        $this->addComponent($this->gridComponent);
    }
    
    public function initializeSearchComponent(){
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_Search");
        $params->add("parentObj",$this);
        $params->add("searchButtonName","Filter Grid");
        $params->add("clearButtonName","Clear Filter");
        $this->searchObj=new SearchComponent($params->getParams());
        $this->searchObj->pageObj=$this->pageObj;
        $this->searchObj->addFieldsFromObjDef($this->objDef);
        $this->searchObj->dataTableName="{$this->id}_Grid";
        $this->searchObj->build();
        $this->addComponent($this->searchObj);
    }
    
    public function render($strbld){
        $this->renderHTML($strbld);
        $this->renderJavascript($strbld);
    }
    
    public function renderHTML($strbld){
        foreach($this->components as $component){
            $component->renderHTML($strbld);
        }
    }
    
    public function renderJavascript($strbld){
        foreach($this->components as $component){
            $component->renderJavascript($strbld);
        }
    }
}

?>