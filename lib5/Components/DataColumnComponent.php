<?php

class DataColumnComponent extends Component {
    
    public $label;
    public $key;
    public $columnAlias;
    public $resizeable;
    public $sortable;
    public $hidden;
    public $field;
    public $transforms;
    public $width;
    
    public function __construct($params)
    {
        parent::__construct($params);
        
        if(isset($params["label"]) && !is_null($params["label"]))
        {
            $this->label=$params["label"];
        }
        else
        {
            $this->label=null;
        }
        
        if(isset($params["key"]) && !is_null($params["key"]))
        {
            $this->key=$params["key"];
        }
        else
        {
            throw new Exception("the parameter 'key' is required");
        }
        
        if(isset($params["resizeable"]) && !is_null($params["resizable"]))
        {
            $this->resizeable=$params["resizeable"];
        }
        else
        {
            $this->resizeable=true;
        }
        
        if(isset($params["sortable"]) && !is_null($params["sortable"]))
        {
            $this->sortable=$params["sortable"];
        }
        else
        {
            $this->sortable=true;
        }
        
        if(isset($params["hidden"]) && !is_null($params["hidden"]))
        {
            $this->hidden=$params["hidden"];
        }
        else
        {
            $this->hidden=false;
        }
        
        if(isset($params["field"]) && !is_null($params["field"]))
        {
            $this->field=$params["field"];
        }
        else
        {
            $this->field=null;
        } 
        
        if(isset($params["propDefName"])){
            $this->propDefName=$params["propDefName"];
        }
        else{
            $this->propDefName=null;
        }
        
        if(isset($params["columnAlias"])){
            $this->columnAlias=$params["columnAlias"];
        }
        else{
            $this->columnAlias=null;
        }
        
        if(isset($params["width"])){
            $this->width=$params["width"];
        }
        else{
            $this->width=null;
        }
        
        if(isset($params["colType"])){
            $this->colType=$params["colType"];
        }
        else{
            $this->colType=null;
        }
        
        $this->transfoms = null;
    }
    
    public function addTransform($transform) {
        if (is_null($this->transforms)) {
            $this->transforms = array();
        }
        $this->transforms[] = $transform;        
    }
    
    public function render($strbld=null)
    {
        $colParams=array();

        $colParams["key"]=$this->key;

        if(!is_null($this->label))
        {
            $colParams["label"]=$this->label;
        }
        else if (!is_null($this->propDefName)){
            $colParams["label"]=$this->propDefName;
        }
        
        if(!is_null($this->field)){
            $colParams["field"]=$this->field;
        }
        
        if(!is_null($this->width)){
            $colParams["width"]=$this->width;
        }
        
        if(!is_null($this->colType)){
            $colParams["colType"]=$this->colType;
        }
        
        if(isset($this->url)){
            $colParams["url"]=$this->url;
        }
        
        if(!empty($this->valuesToSubstitute)){
            $colParams["valuesToSubstitute"]=$this->valuesToSubstitute;
        }

        $colParams["resizeable"]=$this->resizeable;
        $colParams["sortable"]=$this->sortable;
        $colParams["hidden"]=$this->hidden;
        $colParams["columnAlias"]=$this->columnAlias;
        
        $jsonOutput=json_encode($colParams);
        if(is_null($strbld))
        {
            return $jsonOutput;
        }
        else
        {
            $strbld->addLine($jsonOutput);
        }
       
    }
    
}

?>