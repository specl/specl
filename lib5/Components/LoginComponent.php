<?php

class LoginComponent extends Component {

    public function __construct($params)
    {
        parent::__construct($params);
        $this->addAction("login","login");
    }
        
    public function login()
    {
        global $app;
        $status = null;
        
        // parameter checking, validation etc.
        
        if (isset($app->request->parameters['user'])) {
            $user = $app->request->parameters['user'];
        } else { 
            $user = null;
        }        
                
        $response = array();

        $response['message']='login';
        $response['user']=$user;        

        $response['errorMessages']=array();
        
        if ($user=="tony") {      
            $status = true;
            
        } else {
            $status = false;            
            $response['errorMessages']['user']=array();
            $response['errorMessages']['user'][] = 'That user is stupid.';

            $response['errorMessages']['password']=array();
            $response['errorMessages']['password'][] = 'That password is dumb.';
        }
        $response['status']=$status;
        
        
        if ($status) {
            if ($this->hasCallback("onLoginSuccess")) {
                $this->fireCallback("onLoginSuccess",$response);
            } 
        } else {
            if ($this->hasCallback("onLoginFailure")) {
                $this->fireCallback("onLoginFailure",$response);
            }                 
        }   
    }       
}
?>