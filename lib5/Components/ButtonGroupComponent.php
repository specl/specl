<?php

class ButtonGroupComponent extends Component {
    public $buttons;
    public $singleLine;
    
    public function __construct($params){
        parent::__construct($params);
        $this->buttons=array();
        
        if(isset($params["singleLine"])){
            $this->singleLine=$params["singleLine"];
        }
        else{
            $this->singleLine=true;
        }
    }
    
    public function addButton($buttonComponent){
        if(is_a($buttonComponent,"ButtonComponent") || is_a($buttonComponent,"ButtonComponent2")){
            $this->buttons[]=$buttonComponent;
        }
    }
    
    public function render($strbld){
        $this->renderHTML($strbld);
        $this->renderJavascript($strbld);
    }
    
    public function renderHTML($strbld){
        $strbld->addLine("<div id={$this->id}>");
        $strbld->increaseIndentLevel();
        
        foreach($this->buttons as $button){
            if($this->singleLine){
                $strbld->addLine("<span>");
            }
            else{
                $strbld->addLine("<div>");
            }
            $strbld->increaseIndentLevel();
            $button->renderHTML($strbld);
            $strbld->decreaseIndentLevel();
            
            if($this->singleLine){
                $strbld->addLine("</span>");
            }
            else{
                $strbld->addLine("</div>");
            }
        }
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</div>");
    }
    
    public function renderJavascript($strbld){
        foreach($this->buttons as $button){
            if(is_a($button,"ButtonComponent")){
                if(!is_null($button->clickCallback)){
                    $button->renderJavascript($strbld);
                }
            }
            else{
                $button->renderJavascript($strbld);
            }
        }
    }
    
}

?>