<?php

class DataGridInputExComponent extends DataGridComponent {
    
    public $showEditRow;
    public $showDeleteRow;
    public $showInsertRow;
    public $showHideColumn;
    public $showCheckBoxColumn;
    public $addForm;
    public $editForm;
    
    public function __construct($params)
    {
        parent::__construct($params);
        
         if(isset($params["showEditRow"]) && !is_null($params["showEditRow"]))
        {
            $this->showEditRow=$params["showEditRow"];
        }
        else
        {
            $this->showEditRow=false;
        }
        
        if(isset($params["showInsertRow"]) && !is_null($params["showInsertRow"]))
        {
            $this->showInsertRow=$params["showInsertRow"];
        }
        else
        {
            $this->showInsertRow=false;
        }
        
        if(isset($params["showDeleteRow"]) && !is_null($params["showDeleteRow"]))
        {
            $this->showDeleteRow=$params["showDeleteRow"];
        }
        else
        {
            $this->showDeleteRow=false;
        }
        
        if(isset($params["showHideColumn"]) && !is_null($params["showHideColumn"]))
        {
            $this->showHideColumn=$params["showHideColumn"];
        }
        else
        {
            $this->showHideColumn=false;
        }
        
        if(isset($params["showCheckBoxColumn"]) && !is_null($params["showCheckBoxColumn"]))
        {
            $this->showCheckBoxColumn=$params["showCheckBoxColumn"];
        }
        else
        {
            $this->showCheckBoxColumn=false;
        }
        
        if(isset($params["addForm"]) && is_a($params["addForm"],"InputExForm"))
        {
            $this->addForm=$params["addForm"];
        }
        else
        {
            $this->addForm=null;
        }
        
        if(isset($params["editForm"]) && is_a($params["editForm"],"InputExForm"))
        {
            $this->editForm=$params["editForm"];
        }
        else
        {
            $this->editForm=null;
        }
    }
    
    public function render($strbld) {        
        $this->renderCSS($strbld);
        $this->renderHTML($strbld);
        $this->renderJavascript($strbld);
    }
    
    public function renderCreateDataTableJavascript($strbld)
    {
     
       $strbld->addLine("var {$this->id};");
       $strbld->addLine("var create{$this->id}=function(){");
       $strbld->increaseIndentLevel();
       
       $this->renderDialogConfig($strbld);
       $this->renderDataGridConfig($strbld);
       $this->renderColumnDefs($strbld);
       
       
       $strbld->addLine("{$this->id}=new inputEx.widget.CustomDataTable({");
       $strbld->increaseIndentLevel();
       
       $keys=json_encode($this->keys);
       
       
       $configs=array();
       $configs[]="parentEl:'{$this->id}'";
       $configs[]="columnDefs:{$this->id}_ColumnDefs";
       $configs[]="datasource:{$this->dataSourceId}";
       $configs[]="datasourceId:'$this->dataSourceId'";
       $configs[]="datatableOpts:{$this->id}_Config";
       $configs[]="panelConfig:{$this->id}_panelConfig";
       $configs[]="keys:{$keys}";
       
       if ($this->addForm) {
        $configs[]="addFormId:'{$this->addForm->id}'";
       }
       
       if ($this->editForm) {
        $configs[]="editFormId:'{$this->editForm->id}'";
       }
       
       $baseURL="index.php?resourceId={$this->pageObj->id}"; 
       $configs[]="baseURL:'$baseURL'";
       
       if(!is_null($this->addForm))
       {
           $tempbld=new StringBuilder();
           $this->addForm->renderFields($tempbld,"addFields");
           $configs[]=$tempbld->getString();
       }
       
       if(!is_null($this->editForm))
       {
           $tempbld=new StringBuilder();
           $this->editForm->renderFields($tempbld,"editFields");
           $configs[]=$tempbld->getString();
       }
       
       if($this->showHideColumn)
       {
           $configs[]="showHideColumnsDlg:true";
       }
       
       if($this->showEditRow===false)
       {
           $configs[]="allowModify:false";
       }
       
       if($this->showDeleteRow===false)
       {
           $configs[]="allowDelete:false";
       }
       
       if($this->showInsertRow===false)
       {
           $configs[]="allowInsert:false";
       }
       
       if($this->showCheckBoxColumn)
       {
           $configs[]="showCheckBoxColumn:true";
       }
       else
       {
           $configs[]="showCheckBoxColumn:false";
       }
       
       
       $strbld->addList($configs);
       
       $strbld->decreaseIndentLevel();
       
       $strbld->addLine("});");
       
       $strbld->decreaseIndentLevel();
       
       $strbld->addLine("};");
       
       $strbld->addLine("YAHOO.util.Event.addListener(window,'load',create{$this->id});");
        
    }
    
    public function renderDialogConfig($strbld)
    {
        
        $strbld->addLines(
"


var {$this->id}_panelConfig={
                            constraintoviewport: false, 
                            underlay:'shadow', 
                            close:true, 
                            fixedcenter: false,
                            visible:true, 
                            draggable:true,
                            modal: true
                            }


");
    }
    
    public function renderCSS($strbld)
    {
        $strbld->addLines(
"
<style>

.yui-panel .bd
{
    overflow:auto;
    background-color:fff;
    padding:10px;

}

.yui-panel .yui-resize-handle-br {
    right:0;
    bottom:0;
    height: 8px;
    width: 8px;
    position:absolute; 
}


</style>
");
    }
    
}

?>