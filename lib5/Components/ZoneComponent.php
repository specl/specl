<?php
class ZoneComponent extends Component {

	public $cssClass;  	// class=""
	public $cssId;		// id = ""
	public $cssStyle;	// inline style
	public $startWrapper;
	public $endWrapper;
    
    public function  __construct($id,$parentObj) {
        
        $params= new ParamBuilder();
        $params->add("id",$id);
        $params->add("parentObj",$parentObj);
        parent::__construct($params->getParams());
		$this->cssClass = Null;
		$this->cssId = Null;
		$this->cssStyle = Null;
		$this->startWrapper = Null;
		$this->endWrapper = Null;
    }
    
    public function render($strbld) {
        //$strbld->addline("<!-- Zone {$this->id} Start -->");
        
        foreach($this->components as $component)
        {
                $component->render($strbld);      
        }
            
        //$strbld->addline("<!-- Zone {$this->id} End -->");
    }
    
    public function process()
    {
        foreach($this->components as $component)
        {
            $this->component->process();
        }
    }

    public function renderStart($controllerObj)
	{
		if ($this->startWrapper)
		{
			echo $this->startWrapper;
		}
	}

	public function renderEnd($controllerObj)
	{
		if ($this->startWrapper)
		{
			if ($this->endWrapper)
			{
				echo $this->endWrapper;
			}
			else
			{
				echo "</div>";
			}
		}
	}
    
}
?>