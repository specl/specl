<?php
/*
 *  ButtonComponent2 uses the Central Dipsatcher to raise click events rather than hardcoded
 *  javascript callbacks .
 * 
 *  In order to function this component requires that a Central Dispatcher be defined on the page.
 */

class ButtonComponent2 extends Component {
    public $text;
    
    public function __construct($params){        
        parent::__construct($params);        
        if(isset($params["text"])){
            $this->text=$params["text"];
        }
        else{
            throw new Exception("the text field is required");
        }        
    }
    
    public function render($strbld){
        $this->renderHTML($strbld);
        $this->renderJavascript($strbld);
    }
    
    public function renderHTML($strbld){
        $strbld->addLine("<button type='button' id='{$this->id}'>{$this->text}</button>");        
    }
    
    public function renderJavascript($strbld){
      $strbld->addLines(
"
<script>
// {$this->id} click callback
if(DispatchObj!==undefined){
    {$this->id}_onClick=function(){
        var eventMsg={
            event:'buttonClicked',
            elementName:'{$this->id}'
        }
        DispatchObj.dispatch(eventMsg);
    }
    YAHOO.util.Event.addListener('{$this->id}','click',{$this->id}_onClick);
}
</script>
");
    }
}

?>