<?php
/*
 * This class builds a SearchComponent from a CompositeObjDef. The rendering code is the same
 * as the CompositeSearchTest, but the search processing code is completly different. There were
 * too many changes to inherit from the standard search component, so a new class was created and the
 * rendering code was copied from SearchComponent.php, this could change in the future if we decide
 * to update the gui elements.
 * 
 * CompositeSearchComponent inherits from InputExForm because the search javascript component is
 * an InputExForm with a custom inputex component, so the CompositeSearchComponent rendering uses
 * some of the InputExForm rendering.
 */
class CompositeSearchComponent extends InputExForm {
    
    public $searchFields;
    public $dataTableName;
    public $searchButtonName;
    public $clearButtonName;
    public $objDef;
    
    
    public function __construct($params){
        
        if(!isset($params["id"]))
        { 
           throw new Exception("The parameter 'id' is required");
        }
        
        if(!isset($params["parentObj"]))
        {
            throw new Exception("The parameter 'parentObj' is required");
        }
        
        parent::__construct($params["id"],$params["parentObj"]);
        
        if(isset($params["searchButtonName"])){
            $this->searchButtonName=$params["searchButtonName"];
        }
        else{
            $this->searchButtonName="Search";
        }
        
        if(isset($params["clearButtonName"])){
            $this->clearButtonName=$params["clearButtonName"];
        }
        else{
            $this->clearButtonName="Clear Search";
        }
        
        if(isset($params["dropdownTermLimit"])){
            $this->dropdownTermLimit=$params["dropdownTermLimit"];
        }
        else{
            $this->dropdownTermLimit=20;
        }
        
        if(isset($params["compositeDef"])){
            $this->compositeDef=$params["compositeDef"];
        }
        else{
            $this->compositeDef=null;
        }
        
        if(isset($params["pageObj"])){
            $this->pageObj=$params["pageObj"];
        }
        else{
            throw (new Exception("The parameter 'pageObj' is required"));
        }
        
        if(isset($params["dataTableName"])){
            $this->dataTableName=$params["dataTableName"];
        }
        else{
            $this->dataTableName=null;
        }
        
        $this->submitAction="buildSearchQuery";
        $this->addAction("webservice","buildSearchQuery");
    }
    
    public function build($request=null){
        
        $pb = new ParamBuilder();
        $pb->add("id", "{$this->id}_search");
        $searchField = new InputExSearchField($pb->getParams());
        
        // The fields need to be sorted by there label name, not by the field name, since that is what appears to the user
        $searchLabels=array();
        foreach($this->searchFields as $key=>$field){
            $searchLabels[]=$field->fieldLabel;
        }
        usort($searchLabels,"strnatcasecmp");
        $orderedSearchFields=array();
        foreach($searchLabels as $label){
            foreach($this->searchFields as $key=>$field){
                if($field->fieldLabel===$label){
                    $orderedSearchFields[]=$field;
                    break;
                }
            }
        }
        foreach($orderedSearchFields as $key=>$field){
            $searchField->addField($field);
        }
        
        $this->addField($searchField);
    }
    
    public function renderHTML($strbld) {
        $this->renderTitleHTML($strbld);
        parent::renderHTML($strbld);
        $this->renderCloseDivWrapper($strbld);
    }
    
    public function renderTitleHTML($strbld){
        $strbld->addLine("<div> <button id='{$this->id}_expandCollapse' class='searchToggle' onclick='{$this->id}_ShowHideSearch()'>Hide Search</button></div>");        
        $strbld->addLine("<div id='{$this->id}_wrapper'>");
        $strbld->increaseIndentLevel();
    }
    
    public function renderCloseDivWrapper($strbld){
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</div>");
    }
    
    public function renderJavascript($strbld){
        $this->renderShowHideJavascript($strbld);
        parent::renderJavascript($strbld);
        $this->renderSetInitialValue($strbld);
        $this->renderFilterDeletedCallback($strbld);
    }
    
    /*
     * This function renders a button above the search component to show/hide the search component.
     */
    public function renderShowHideJavascript($strbld){
        $strbld->addLines(
"
<script>
    var {$this->id}_ShowHideSearch=function(){
        var wrapperEl = document.getElementById('{$this->id}_wrapper');
        var toggleEl= document.getElementById('{$this->id}_expandCollapse');        
        if(wrapperEl.style.display === '' || wrapperEl.style.display === 'block') {
            wrapperEl.style.display = 'none';
            toggleEl.innerHTML = 'Show Search';
        }
        else {
            wrapperEl.style.display = 'block';
            toggleEl.innerHTML = 'Hide Search';
        }
    }
</script>
"
);
    }
    
    /*
     * Every time the user removes a filter from the search component the grid needs to be updated
     * to reflect those changes. This requires resubmitting the search filter everytime the user
     * clicks on the remove button.
     */
    public function renderFilterDeletedCallback($strbld){
        $strbld->addLines(
"
<script type='text/javascript'>
    YAHOO.util.Event.addListener(window,'load',function(){
        var searchListField={$this->id}.getFieldByName('{$this->id}_search');
        searchListField.filterDeleted.subscribe(function(){
            {$this->id}.onSubmit();
        });
    });
</script>
");

    }
    
    /*
     * If there is a search filter saved as a user variable then set the saved search to the initial
     * value of the search component. This is required for search persistence.
     */
    public function renderSetInitialValue($strbld)
    {
        global $app;
        
        $search=$app->userStorage->retrieve("{$this->pageObj->id}_{$this->id}_search",false);
        if(is_null($search)){
            $search="{'{$this->id}_search':[]}";
        }else{
            $whereStatement=$app->userStorage->retrieve("{$this->pageObj->id}_{$this->dataTableName}_where",false);
            if(!is_null($whereStatement)){
                $app->sessionStorage->store("{$this->pageObj->id}_{$this->dataTableName}_where",$whereStatement);
            }
        }
          
        $strbld->addLine("<script type='text/javascript'>");
        $strbld->increaseIndentLevel();
        $strbld->addLine("YAHOO.util.Event.addListener(window,'load',function(){");
        $strbld->increaseIndentLevel();
        $strbld->addLine("{$this->id}.setValue({$search},false)");
        $strbld->decreaseIndentLevel();
        $strbld->addLine("});");
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</script>");
    }
    
    public function renderAjaxConfig($strbld){
         $strbld->addLines(
"
ajax: {
    method:'POST',
    uri:'index.php?pageId={$this->pageObj->id}&resourceId={$this->pageObj->page->resourceId}&controllerId={$this->id}&actionName={$this->submitAction}&mode={$this->formMode}&buildPage=false',
    callback: {
        success:function(response)
        {
            {$this->id}.clearErrorMessages()
            var results=YAHOO.lang.JSON.parse(response.responseText);

            if(!YAHOO.lang.isUndefined(results.errorMessages))
            {
               var errors=results.errorMessages;
               {$this->id}.showErrorMessages(errors);
            } 
            else if (results.message == 'redirect') {                 
                 window.location.href = results.parameters.location;
            }
            else{
               // reset the paginator page to 1. This is required to change the recordOffset to 0 so the query will work properly.
               {$this->dataTableName}.get('paginator').setPage(1, true);            
               {$this->dataTableName}.refreshData('{$this->pageObj->id}_{$this->dataTableName}_where');
               
            }
            
        },
        failure: function(response){return false}
    }
");
    }
    
    public function renderFormButtons($strbld){
               $strbld->addLines(
"
buttons:[{type:'submit',value:'{$this->searchButtonName}'},
         {type:'submit',value:'{$this->clearButtonName}',
            onClick:function()
                {
                    {$this->id}.setValue({{$this->id}_search:[]});   
                }
            }
        ],");
    }
    
    public function addSearchField($field){
        if(is_a($field,"SearchField")){
            $this->searchFields[$field->fieldName]=$field;
        }
        else{
            throw new Exception("must add a object of type SearchField");
        }
    }
    
    /*
     * The function Processes the request sent by the user when they click on the search button:
     *      - store the JSON of the search in a user variable so it can be reloaded the next time the
     *      user loads the page if search persistence is enabled.
     *      - process the search data and build Compoiste Filter Objects
     *      - save the composite filter objects in session storage and userStorage.
     *          - after the function returns success the datagrid is refreshed using the filter objects stored
     *          in the session variable.
     *          - the filter objects are also stored as a user variable for persistence.
     */
    public function buildSearchQuery($request){
        
        global $app;
        
        $formDataJSON=$app->activeSession->getVarNoStrip("POST","value");
                
        $app->userStorage->store("{$this->pageObj->id}_{$this->id}_search",$formDataJSON);
        
        $formData=json_decode($formDataJSON,$associative=true);        
        $searchData=$formData["{$this->id}_search"];
        
        $whereStatement=$this->buildCompositeFilterObjs($searchData);
        
        $app->sessionStorage->store("{$this->pageObj->id}_{$this->dataTableName}_where",$whereStatement);
        $app->userStorage->store("{$this->pageObj->id}_{$this->dataTableName}_where",$whereStatement);
        
        $results=array();
        $results["Success"]=true;
        
        echo json_encode($results);
        
    }
    
    /*
     *  Build Composite Filter Objects from searchData.
     *  
     *  SearchData is an array of rows of data each describing a single filter, the format of each filter is as follows:
     *      [$columnName,$operator,$value1,$value2]
     *  
     *  $columnName has the format of $objectName_$propertyName
     * 
     *  There are four different types of operators, each type requires slightly different processing:
     *      - comparisonOperator: convert the search operation to the equivalent filter operation based on the
     *      comparisonOperatorLookup
     *      - likeOperator: Depending on the operation, a % needs to be added in the appropriate place to the 
     *      string being searched
     *      - betweenOperator: A between operator has two values and requires creating to filter conditions
     *      - nullOperator: null operators have no values, the value is simply null.
     */
    public function buildCompositeFilterObjs($searchData){
        /*
         * These operator maps serve two purposes:
         *     - divide the search operators into different categories that are processed differently
         *     - map from the operator name in the search to the Filter Operation.
         */
        $comparisonOperatorLookup=array(
            "equals"=>"=",
            "not equals"=>"!=",
            "lessthan"=>"<",
            "lessthanorequal"=>"<=",
            "greaterthan"=>">",
            "greaterthanorequal"=>">="
        );
        
        $likeOperatorLookup=array(
            "contains"=>"like",
            "starts with"=>"like",
            "ends with"=>"like"
        );
        
        $betweenOperatorLookup=array(
          "between"  => "between",
          "not between"=>"not between"
            
        );
        
        $nullOperatorLookup=array(
            "is blank"=>"is null",
            "is not blank"=>"is not null"
        );
        
        $compositeFilters=array();        
        foreach($searchData as $searchFilter){
            // make the operator lookup case insensitive.
            $operator=strtolower($searchFilter[1]);
            
            // get the object name and property name from the columnName
            $columnName=$searchFilter[0];
            $split=explode("_", $columnName);
            $objectName=$split[0];
            $propertyName=$split[1];
            
            if(array_key_exists($operator, $comparisonOperatorLookup)){
                $value=$searchFilter[2];
                // A comparison operator must have a value set in the filter, otherwise ignore the filter
                if($value===null || trim($value)===""){
                    continue;
                }          
                $value=trim($value);
                $value=$this->applyValueTransforms($value,$columnName);
                $compositeFilters[]=new CompositeValueFilter($objectName, $propertyName, $comparisonOperatorLookup[$operator], $value);
            }
            else if(array_key_exists($operator,$likeOperatorLookup)){
                $value=$searchFilter[2];
                // A like operator must have a value set in the filter, otherwise ignore the filter
                if($value===null || trim($value)===""){
                    continue;
                }          
                $value=trim($value);
                $value=$this->applyValueTransforms($value,$columnName);
                
                // add the % in the appropriate place to the string
                if($operator=="contains"){
                    $value="%".$value."%";
                }
                else if($operator=="starts with"){
                    $value=$value."%";
                }
                else if ($operator=="ends with"){
                    $value="%".$value;
                }
                else{
                    throw new Exception("'{$operator}' is an unhandled like operator");
                }
                $compositeFilters[]=new CompositeValueFilter($objectName, $propertyName, "like", $value);
            }
            else if(array_key_exists($operator,$betweenOperatorLookup)){
                $value1=$searchFilter[2];
                $value2=$searchFilter[3];
                // A between operator must two values set in the filter, otherwise ignore the filter
                if($value1===null || trim($value1)===""){
                    continue;
                } 
                if($value2===null || trim($value2)===""){
                    continue;
                }
                $value1=$this->applyValueTransforms($value1,$columnName);
                $value2=$this->applyValueTransforms($value2,$columnName);
                
                // between is the same as (>= min AND <=max) where value1=min and value2=max
                if($operator=="between"){
                    $group=new CompositeGroupFilter();
                    $group->addValueFilter($objectName, $propertyName, ">=", $value1);
                    $group->addValueFilter($objectName, $propertyName, "<=", $value2);
                    $compositeFilters[]=$group;
                }
                // not between is the same as (< min or > max) where value1=min and value2=max
                else if($operator=="not between"){
                    $group=new CompositeGroupFilter();
                    $group->addValueFilter($objectName, $propertyName, "<", $value1);
                    $group->addValueFilter($objectName, $propertyName, ">", $value2,"OR");
                    $compositeFilters[]=$group;
                }
            }
            else if(array_key_exists($operator, $nullOperatorLookup)){
                if($operator==="is blank"){
                    $compositeFilters[]=new CompositeValueFilter($objectName, $propertyName, "=", null);
                }
                else if($operator==="is not blank"){
                    $compositeFilters[]=new CompositeValueFilter($objectName, $propertyName, "!=", null);
                }
            }
        }
        return $compositeFilters;
    }
    
    /*
     * This function takes a compositeObjDef and adds search fields to the search component based on the compositeObjDef passed in.
     * 
     * Here are the rules:
     *      - the types of the inputEx fields that make up the search and the objDef types are not always the same and must
     *      be mapped.
     *      for standard CompositeProperty objects the rules are as follows:
     *           - all search fields are named $objectName_$propertyName to avoid any ambiguity if there are muliple objects with the same field
     *           - all search fields have a label associated with them that is take from the caption of the propDef defining the property
     *           - constrainted fields can be either a dropdown or a autocomplete based on the number of choices. If the number of choices is
     *           below the dropdownTermLimit parameter, then the field is a select dropdown, otherwise it is an autocomplete field.
     *           - the handleAs property can be used so that the value is treated by the search component as a different type, other
     *           than the type specified in the propDef. This is primarily used to treat datetime columns as date fields.
     *           - when a field has a constraint the following happens:
     *              - all the constraint values are retrieved and stored in memory
     *              - "SNULL" returned by the constraint is converted to a "null" value in the dropdown
     *              - the choices are sorted, in natural sort order, based on the choice labels
     *              - the number of choices are counted and whether the field is a dropdown or an autocomplete is determined.     * 
     *              - the choicetype is set based on the type of the propDef of the display column
     *           - if the field is a select drop down the following happens:
     *               - the choices are all sent to as a parameter to the javascript search component.
     *           - if the field is an autocomplete field the following happens:     *              
     *               - an url where the autocomplete field is to get its values from is generated
     *               - an canBeNull field parameter is set based on the propDef
     */
    public function buildFieldsFromCompositeObjdef($compositeObjDef){
        global $app;
        
        $propToFieldTypeMap=array(
            "string"=>"string",
            "float"=>"numeric",
            "integer"=>"numeric",
            "date"=>"date",
            "datetime"=>"datetime",
            "boolean"=>"boolean",
            "double"=>"numeric");
        
        foreach($compositeObjDef->getProperties() as $propObj){
            $fieldParams=new ParamBuilder();
            if(is_a($propObj,"CompositeProperty")){
                $fieldParams->add("fieldName","{$propObj->objectName}_{$propObj->propertyName}");
                $propDef=$propObj->propDef;
                
                $label = $propDef->getAttribute("caption",$propDef->name);
                $fieldParams->add("fieldLabel", $label);
                
                if($propDef->hasAttribute("handleAs")){
                    $objectType=$propDef->getAttribute("handleAs");
                }
                else{
                    $objectType=$propDef->objectType;
                }
                
                if(is_null($propDef->getConstraint())){
                    $fieldParams->add("fieldType",$propToFieldTypeMap[$objectType]); 
                }
                else{
                     $constraint = $propDef->getConstraint();                     

                     // getting all the values and returning it in memory is wrong for autocomplete values. This
                     // can be fixed be adding a getCount method to constraints. This will be fixed after unit tests
                     // are written and after the SQLConstraint classes are upgraded to use the new BaseDAL.
                     $constraint->generate();
                     $values=$constraint->get();
                     
                     $choices=array();                     
                     foreach($values as $value=>$name){
                         if($value=="SNULL"){
                             $choices[""]="null";
                         }
                         else{
                             $choices[$name]=$value; 
                         }   
                     }
                    
                     //sort choices by label
                     uksort($choices,"strnatcasecmp");                     
                     
                     /*
                      * Set the choice type based on the type of the display column
                     */
                     $constraintTable=$constraint->tableName;
                     $displayColumn=$constraint->displayColumnName;
                     $displayObjDef=$app->specManager->findDef($constraintTable);
                     $displayPropDef=$displayObjDef->getProperty($displayColumn);
                     $objectType=$displayPropDef->objectType;
                     $fieldParams->add("choiceType",$propToFieldTypeMap[$objectType]);
                     $fieldParams->add("idType",$propToFieldTypeMap[$propDef->objectType]);
                     
                     if(count($choices)<=$this->dropdownTermLimit){
                        $fieldParams->add("fieldType","choice");                        
                        $fieldParams->add("fieldChoices",$choices);
                     }
                     
                     else{
                         $fieldParams->add("fieldType","autocomplete");
                         $fieldParams->add("url","index.php?pageId={$this->pageObj->id}&resourceId={$this->pageObj->page->resourceId}&controllerId={$this->id}&actionName=autoComplete&buildPage=false");
                         
                         /*
                          * For autocomplete fields the nullAllowed property determines whether or not to add the "is blank" and 
                          * "is not blank" operators to the search field
                          */
                         if($propDef->getAttribute("nullAllowed", false)){
                             $fieldParams->add("canBeNull",true);
                         }
                         else{
                             $fieldParams->add("canBeNull",false);
                         }
                    }
                }
            }
            
            if(is_a($propObj,"CountProperty")){
                $fieldParams->add("fieldName", "count");
                $fieldParams->add("fieldLabel","Count");
                $fieldParams->add("fieldType","integer");
            }
            
            $searchField = new SearchField($fieldParams->getParams());
            $this->addSearchField($searchField);
        }       
    }
    
    /*
     * Apply transformations of the value passed in by the user and the fieldName. The following transforms are applied:
     *      - if the user enters a string in an integer field quotes are added around the string, otherwise
     *      the value will not be converted properly to SQL and an error will occur.
     *      - if the field in an autocomplete, the display name value needs to be converted to its id.
     */
    protected function applyValueTransforms($value,$fieldName){
        global $app;
        /*
         * If the searchFields have not been added yet, don't apply any transformations. This primiarly
         * happens during unit testing and doesn't typically occur in real code.
         */
        if(!empty($this->searchFields)){
            $field=$this->searchFields[$fieldName];
            if($field->fieldType=="autocomplete"){
                $oldValue=$value;
                $split=explode("_",$fieldName);
                $objName=$split[0];
                $propName=$split[1];
                $objDef=$app->specManager->findDef($objName);
                $propDef=$objDef->getProperty($propName);
                $constraint=$propDef->getConstraint();
                $constraint->generate();
                $lookup=$constraint->get();
                $value=array_search($value,$lookup);
                
                /*
                 * If the value is not found in the lookup keep the old value
                 * in the future this should given a validation error.
                 */
                if($value===false){
                    $value=$oldValue;
                    $fieldType=$field->idType;
                }
                else{
                    $fieldType=$field->choiceType;
                }
            }
            else if($field->fieldType=="choice"){
                $fieldType=$field->choiceType;
            }
            else{
                $fieldType=$field->fieldType;
            }
            
            if($fieldType=="numeric"){
                if(!is_numeric($value)){
                    $value="'$value'";
                }
            }
        }
        return $value;
    }
}

?>