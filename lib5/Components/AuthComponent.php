<?php

class AuthComponent extends Component {

    public $mode;
    
    public function __construct($params)
    {
        parent::__construct($params);
        
        $this->addAction("page","showLoginForm","showLoginForm",array("success","failure"));
        
        $this->addAction("page","showRegisterForm","showRegisterForm",array("success","failure"));        
        $this->addAction("page","showRegisterSuccessForm","showRegisterSuccessForm",array("success","failure"));     
        
        $this->addAction("page","showRemindMeForm","showRemindMeForm",array("success","failure"));     
        $this->addAction("page","showReminderSentForm","showReminderSentForm",array("success","failure"));     

        $this->addAction("page","ChangePassword","changePassword",array("success","failure"));     
        $this->addAction("page","ChangePasswordSuccess","changePasswordSuccess",array("success","failure"));     
        
        $this->addAction("page","logout","logout",array("success","failure"));                
        
        $this->mode = "login";
    }

    public function build($request) {
        if (($request->actionName == "showLoginForm") || ($request->controllerId == "LoginForm")) {
            $this->buildLoginForm();
        } else if (($request->actionName == "showRegisterForm")|| ($request->controllerId == "RegisterForm")) { 
            $this->buildRegisterForm();            
        } else if (($request->actionName == "showRemindMeForm") || ($request->controllerId == "RemindMeForm")) { 
            $this->buildRemindMeForm();            
        } else if (($request->actionName == "ChangePassword") || ($request->controllerId == "ChangePasswordForm")) { 
            $this->buildChangePasswordForm();            
        } else if ($request->actionName == "showReminderSentForm") { 
            $this->buildReminderSentForm();            
        } else if ($request->actionName == "showRegisterSuccessForm") { 
            $this->buildRegisterSuccessForm();            
        } else if ($request->actionName == "ChangePasswordSuccess") { 
            $this->buildChangePasswordSuccess();            
        }    
    }
        
    public function process($request) {
        if ($request->actionName == "logout") {
            $this->logout();
        }
    }

    public function buildRemindMeForm() {
        global $app;
        
        $this->mode = "remindme";        
        $this->pageObj->addInputExIncludes();
        $this->formObjDef = $app->specManager->findDef("RemindMe", true); 
        
        $fb= new InputExFormBuilder();
        
        $this->form = $fb->build($this->formObjDef->name."Form","add",$this->formObjDef,$this->pageObj);
        $this->form->pageObj=$this->pageObj;
        $this->form->parentObj=$this;
        $this->form->formDef=$this->formObjDef;
        $this->form->visible=true;       
        $this->form->registerCallback("onFormAddValidCallback", $this, "onRemindMe", null);                
        
        $this->form->formTitle = "Email Password Reminder";
         
        $this->addComponent($this->form);  
    }
    
    public function buildReminderSentForm() {
        global $app;
        
        $pb = new ParamBuilder();
        $pb->add("id","z1");
        $pb->add("pageObj",$this);        
        $z1 = new TextZone($pb->getParams()); 
                      
        $z1->title = "Reminder Sent";
        $z1->text = "A reminder has been sent to your email address.";
        
        $this->addComponent($z1);   
    }
            
    public function buildRegisterSuccessForm() {
        global $app;
        
        $pb = new ParamBuilder();
        $pb->add("id","z1");
        $pb->add("pageObj",$this);        
        $z1 = new TextZone($pb->getParams()); 
                      
        $z1->title = "Register Success";
        $z1->text = "Your login information has been sent to your email address.";
        
        $this->addComponent($z1);   
    }
    
    
    public function buildLoginForm() {
        global $app;
        
        $this->mode = "login";        
        $this->pageObj->addInputExIncludes();
        $this->formObjDef = $app->specManager->findDef("Login", true); 
        
        $fb= new InputExFormBuilder();
        
        $this->form = $fb->build($this->formObjDef->name."Form","add",$this->formObjDef,$this->pageObj);
        $this->form->pageObj=$this->pageObj;
        $this->form->parentObj=$this;
        $this->form->formDef=$this->formObjDef;
        $this->form->visible=true;               
        $this->form->formCSS="LoginForm";       
        $this->form->registerCallback("onFormAddValidCallback", $this, "onLoginValid", null);                
        
        $this->form->formTitle = "Login";
        
        $this->addComponent($this->form);          
    }

    public function logout() {
        global $app;
        
        if ($app->sessionManager->state()=="LoggedIn") {
            $app->sessionManager->setStateLoggedOut(); 
            $app->setRedirect();
        }            
    }
    
    public function onLoginValid($formdata) {       
        global $app;
                                
        $status = $app->acManager->login($formdata['user'],$formdata['password']);        
        
        if ($status) {
            $app->sessionManager->setStateLoggedIn($formdata['user']);                        
            $this->form->sendSuccess("Login Successful", "index.php?menuItemId=Home");            
        } else {
            $error=new ValidationError('login','password',"Error",null,"login failed: password invalid",1000,1,$formdata['user']);
            $this->form->addFieldError($error);            
            $this->form->sendError('Error: Login attempt failed.');            
        }                
    }
    
    public function onRegisterFormValid($eventData,$userData) {       
        global $app;
                                        
        // turn the array data from the form into an Instance.
        $acuDef = $app->specManager->findDef("ACU", true); 
        $acuObj = NewSpecObj('ACU');
        $acuObj->loadFromArray($eventData);
        
        $acuObj->addDefaultValues();
        
        $acrDAL = NewDALObj('ACR');
            
        $acrObj = $acrDAL->getOneByPkFromData(array('acr'=>'user'));
        if (is_null($acrObj)==False) {
            $acuObj->ACUR[] = $acrObj;
        }                                            

        $acrObj = $acrDAL->getOneByPkFromData(array('acr'=>'tech'));
        if (is_null($acrObj)==False) {
            $acuObj->ACUR[] = $acrObj;
        }                                            
        
        $acuObj->active = 1;
        
        $results = $app->acManager->register($acuObj);        
        
        if ($results->success===True) {            
            $app->acManager->registerNotification($acuObj);
            $this->form->sendSuccess("Register Success", "index.php?menuItemId=RegisterSuccess");
        } else {
            $error=new ValidationError('login','user',"Error",null,"Registration failed",1000,1,null);
            $this->form->addFieldError($error);            
            $this->form->sendError('Error: Register attempt failed.');            
        }                
    }
        
    public function onRemindMe($formdata) {       
        global $app;
                                
        $acuObj = $app->acManager->getUserByEmail($formdata['email']);        
        
        if ($acuObj) {           
            $output=array();
            
            $status = $app->acManager->passwordReminderNotification($acuObj);
                                
            if ($status) {
                $this->form->sendSuccess("Reminder Sent", "index.php?menuItemId=ReminderSent");
            } else {
                $error=new ValidationError('login','user',"Error",null,"Incorrect",1000,3,$formdata['user']);
                $this->form->addFieldError($error);            
                $this->form->sendError('Error: Remind attempt failed.');            
            }            
        } else {
            $error=new ValidationError('login','user',"Error",null,"Incorrect",1000,2,$formdata['user']);
            $this->form->addFieldError($error);            
            $this->form->sendError('Error: Remind attempt failed.'); 
        }
    }
    
    
    public function buildRegisterForm() {
        global $app;
               
        $pb = new ParamBuilder();
        $pb->add("id","z1");
        $pb->add("pageObj",$this);        
        $z1 = new TextZone($pb->getParams()); 
                      
        $z1->title = "Register";
        $this->addComponent($z1);                 
        $this->mode = "register";
       
        $this->pageObj->addInputExIncludes();
        $this->formObjDef = $app->specManager->findDef("Register", true); 
        
        $fb= new InputExFormBuilder();
        
        $this->form = $fb->build($this->formObjDef->name."Form","add",$this->formObjDef,$this->pageObj);
        
        $this->form->pageObj=$this->pageObj;       
        $this->form->parentObj=$this->pageObj; 
        $this->form->formDef=$this->formObjDef;
        $this->form->visible=true;       
        $this->form->registerCallback("onFormAddValidCallback", $this, "onRegisterFormValid", null);                
        
        $this->form->formTitle = "New User Regsiter";
         
        $this->addComponent($this->form);         
    }            
    
    public function buildChangePasswordForm() {
        global $app;
                               
        $this->mode = "ChangePassword";
       
        $this->pageObj->addInputExIncludes();
        $this->formObjDef = $app->specManager->findDef("ChangePassword", true); 
        
        $fb= new InputExFormBuilder();
        
        $this->form = $fb->build($this->formObjDef->name."Form","add",$this->formObjDef,$this->pageObj);
        $this->form->pageObj=$this->pageObj;
        $this->form->parentObj=$this;
        $this->form->formDef=$this->formObjDef;
        $this->form->visible=true;       
        $this->form->registerCallback("onFormAddValidCallback", $this, "onChangePassword", null);                
        
        $this->form->formTitle = "Change Password";
         
        $this->addComponent($this->form);         
    }
    
    public function onChangePassword($eventData,$userData) {       
        global $app;
                                                                                
        $results = $app->acManager->changePassword($eventData['password']);        
        
        if ($results->success===True) {            
            $this->form->sendSuccess("Change Password Success", "index.php?menuItemId=ChangePasswordSuccess");
        } else {
            $error=new ValidationError('login','user',"Error",null,"Change Password failed",1000,4, null);
            $this->form->addFieldError($error);            
            $this->form->sendError('Error: Password change attempt failed.');            
        }                
    }
    
    public function buildChangePasswordSuccess() {
        global $app;
        
        $pb = new ParamBuilder();
        $pb->add("id","z1");
        $pb->add("pageObj",$this);        
        $z1 = new TextZone($pb->getParams()); 
                      
        $z1->title = "Change Password Success";
        $z1->text = "Your password has been changed.";
        
        $this->addComponent($z1);           
    }    
}
?>