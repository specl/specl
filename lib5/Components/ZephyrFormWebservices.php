<?php

class ZephyrFormWebservices extends Component {
    public function __construct($params){
        parent::__construct($params);
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        } else {
            $this->objDef = null;        
        }
        if(isset($params["mode"])){
            $this->mode=$params["mode"];
        } else {
            $this->mode = null;        
        }
        $this->fields=array();
        $this->addAction("webservice","autoComplete");
        $this->addAction("webservice","validateData");
    }
    
    /**
     *  This function is the same as the autoCompleteDefault function in the InputExForm, if
     *  a seperate autoComplete function is needed just create another function and have the url
     *  point to that function.
     */
    public function autoComplete(){
        global $app;
        $fieldId=$app->activeSession->getVarNoStrip("GET",'fieldId');               
        $query=$app->activeSession->getVarNoStrip("GET",'query');
        $objDefName=$app->activeSession->getVarNoStrip("GET",'objDef');
        $results=array();
        $results["data"]=array();
        if($objDefName===""){
            $objDef=$this->objDef;
        }else{
            $objDef=$app->specManager->findDef($objDefName);
        }
        $propDef=$objDef->getProperty($fieldId);
        $constraint=$propDef->getConstraint(); 
        // only display unique constraint values, this is necessary if the same value is defined
        // for multiple labs and you are typing in an autocomplete field where the values are
        // not constrained by lab.
        $constraint->selectUniqueValues=true;
        $constraint->whereStatement->addClause(new SQLComparison("{$constraint->displayColumnName}","like","'{$query}%'"));        
        $constraint->numberOfRowsToGet=20;
        $constraint->addEmptyOption=false;
        $constraint->generate($this->mode);
        $results=$constraint->get();
        
        $data=array();
        $data["data"]=array();
        foreach($results as $value=>$key){
            $data["data"][]=array("value"=>$key);
        }
        
        $json_out=json_encode($data);
        echo $json_out;  
    }
    
    public function validateData(){
        global $app;
        $data=$app->activeSession->getVarNoStrip("POST",'value');               
        $formData=json_decode($data,$associative=true);                       
        $this->decodeFormData($formData);   
        // the validation validates all of the data in the object and collections in the
        // form only have partial data, so you must get the rest of the data from the DB.
        $formData=$this->getCollectionDataFromDB($formData);
        $this->objDef->applyTransforms($formData,$this->mode);
        $this->objDef->validate($formData,$this->mode);
        
        $errors=$this->objDef->validationErrors;
        //
        if(count($errors)===0){
            if ($this->mode=="add") {
                if ($this->hasCallback("onFormAddValidCallback")) {
                    $this->fireCallback("onFormAddValidCallback",$formData);
                } 
                else{
                    echo json_encode(array("success"=>false,"messageType"=>"error","errorMsg"=>"No Callback Functions have been set on the form"));
                }
            } else if ($this->mode=="edit" || $this->mode=="upload") {
                if ($this->hasCallback("onFormEditValidCallback")) {
                    $this->fireCallback("onFormEditValidCallback",$formData);
                }                 
                else{
                    echo json_encode(array("success"=>false,"messageType"=>"error","errorMsg"=>"No Callback Functions have been set on the form"));
                }
            }
            else{
                echo json_encode(array("success"=>false,"messageType"=>"error","errorMsg"=>"The mode {$this->add} is not handled by the form"));
            }
            
        }
        else{
            $fieldErrors=array();
            foreach($errors as $error){
                // any nested errors will not return a propDef from the baseObjDef
                // you would need to get the correct objDef based on the path, this
                // is not needed right now, so we just check to see if the propDef is
                // null.
                $propDef=$this->objDef->getProperty($error->propertyName);
                // Errors for the entire collection must be stored in a seperate variable than,
                // the nested errors inside the collection, other wise returning the errors won't
                // work if there is an error in both.
                if($propDef!==null && $propDef->isCollection()){
                    $path=$error->fullPath."-collectionError";
                }else{
                    $path=$error->fullPath;
                }
                array_set($fieldErrors,$path,$error->message);  
            }
            $fieldErrors["form_error"]="This form contains errors the need to be fixed";
            echo json_encode(array("success"=>false,"messageType"=>"error","fieldErrors"=>$fieldErrors));
        }
    }
    
    public function decodeFormData(&$formData) {
        foreach($this->fields as $field) {
            if(!isset($formData[$field["name"]])){
                continue;
            }
            // Replace SNULL string values with real null values
            if ($field["type"] == "DropDown") {
                if (isset($formData[$field["name"]])) {
                    $value = $formData[$field["name"]];
                    if ($value=="SNULL") {
                        $formData[$field["name"]] = NULL;
                    }
                }
            }
            // Substitute the display value with the real value for autocomplete fields
            else if($field["type"]=="AutoComplete"){
               $propDef=$this->objDef->getProperty($field["name"]);
               $constraint=$propDef->getConstraint();
               $constraint->generate($this->mode);
               $lookup=$constraint->get();
                              
               $value=array_search($formData[$field["name"]],$lookup);
               
               if ($value=="SNULL" || is_null($formData[$field["name"]]) || trim($formData[$field["name"]])=="") {;
                        $formData[$field["name"]] = NULL;
               }
               else if($value===false){
                   // if the lookup value does not exist do not do transform the value and leave it as it is.
                   // TODO: This should raise some sort of lookup validation error.
               }else{
                   $formData[$field["name"]]=$value;
               }
            } 
            // DateTime is represented as UTC in javascript, but the database time is in EST,
            // must convert between time zones.
            else if ($field["type"]=="datetime") {
                if (isset($formData[$field["name"]])) {
                    $value = $formData[$field["name"]];                

                    $UTC = new DateTimeZone("UTC");
                    $newTZ = new DateTimeZone("America/New_York");
                    $date = new DateTime( $value, $UTC );
                    $date->setTimezone( $newTZ );
                    $value=$date->format('Y-m-d G:i:s');    
                    
                    $formData[$field["name"]] = $value;
                }                
            }
            
            else if($field["type"]==="Collection"){
                $newCollectionData=array();
                foreach($formData[$field["name"]] as $colField){
                    $this->decodeFormData($colField);
                    $newCollectionData[]=$colField;
                }
                $formData[$field["name"]]=$newCollectionData;
            }
        }
        
        // apply
        
        // After performing transformations, convert from js values to php values
        foreach($this->objDef->getProperties() as $propDef) {            
            if ($propDef->hasAttribute("ignore",true,$this->mode)) {
                continue;
            }
            // Some of the propDefs may not be in the form.  Check for them
            // first.  This happens because some propDefs are not in the form.            
            if (isset($formData[$propDef->name])) {
                $input = $formData[$propDef->name];                    
                $value = javascript2php($propDef,$input);
                $formData[$propDef->name] = $value;
            }
        }                                       
    }
    
    // Get the current data from the DB and update it with the new data from the form.
    public function getCollectionDataFromDB($formData){
        global $app;
        foreach($this->objDef->getProperties() as $propDef){
            if($propDef->isCollection() && !empty($formData[$propDef->name])){    
                $collectionDef=$app->specManager->findDef($propDef->objectTypeOrig);
                $childDef=$collectionDef->getFirstChildDef();
                $childDAL=NewDALObj($childDef->name);
                $newFormData=array();
                foreach($formData[$propDef->name] as $formEntry){
                    
                    if($this->mode==="edit" && $propDef->getAttribute("enableEditing",false)){
                        $origObj=$childDAL->getOneUsingOriginalPK($formEntry);
                    }else{
                        $origObj=$childDAL->getOneByPk($formEntry);
                    }
                    // the object may not exist in the database if the collection is embedded and
                    // you add a new entry.
                    if($origObj!==null){
                        $newEntry=$origObj->getAsArray();
                        $newEntry=array_merge($newEntry,$formEntry);
                    }else{
                        $newEntry=$formEntry;
                    }
                    
                    $newFormData[]=$newEntry;
                }
                $formData[$propDef->name]=$newFormData;
            }
        }
        return $formData;
    }
}

?>