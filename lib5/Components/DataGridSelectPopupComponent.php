<?php

class DataGridSelectPopupComponent extends PopupComponent {
    
    public $whereStatement;
    public $displayColumns;
    public $hideColumns;
    public $objDef;
    public $dataSource;
    public $searchObj;
    public $rowsPerPage;
    public $grid;
    public $selectButtonText;
    public $formId;
    public $fieldName;
    public $onSelectCallback;
    public $mode;
    public $selectOneMode;
    
    public function __construct($params){
        parent::__construct($params);
        
        if(isset($params["mode"])){
            $this->mode=$params["mode"];
        }
        else{
            $this->mode=null;
        }
        
        if(isset($params["whereStatement"])){
            $this->whereStatement=$params["whereStatement"];
        }
        else{
            $this->whereStatement=null;
        }
        
        if(isset($params["displayColumns"])){
            $this->displayColumns=$params["displayColumns"];
        }
        else{
            $this->displayColumns=null;
        }
        
        
        if(isset($params["hideColumns"])){
            $this->hideColumns=$params["hideColumns"];
        }
        else{
            $this->hideColumns=null;
        }
        
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        }
        else{
            throw (new Exception("the 'objDef' parameter is required"));
        }
        
        if(isset($params["pageObj"])){
            $this->pageObj=$params["pageObj"];
        }
        else{
            throw (new Exception("the 'pageObj' parameter is required"));
        }
        
        if(isset($params["rowsPerPage"])){
            $this->rowsPerPage=$params["rowsPerPage"];
        }
        else{
            $this->rowsPerPage=10;
        }
        
        if(isset($params["selectButtonText"])){
            $this->selectButtonText=$params["selectButtonText"];
        }
        else{
            $this->selectButtonText="Select Rows";
        }
        
        // only needed for default select callback
        if(isset($params["formId"])){
            $this->formId=$params["formId"];
        }
        else{
            $this->formId="{$this->id}_form";
        }
        
        // only needed for default select callback
        if(isset($params["fieldName"])){
            $this->fieldName=$params["fieldName"];
        }
        else{
            $this->fieldName="{$this->id}_field";
        }
        
        if(isset($params["onSelectCallback"])){
            $this->onSelectCallback=$params["onSelectCallback"];
        }
        else{
            $this->onSelectCallback="{$this->id}_getRows_default";
        }
        
        if(isset($params["selectOneMode"])){
            $this->selectOneMode=$params["selectOneMode"];
        }
        else{
            $this->selectOneMode=false;
        }
        
        if(isset($params["enableClearAll"])){
            $this->enableClearAll=$params["enableClearAll"];
        }
        else{
            $this->enableClearAll=true;
        }
        
        if(isset($params["initialLoad"])){
            $this->initialLoad=$params["initialLoad"];
        }
        else{
            $this->initialLoad=true;
        }
        
        if(isset($params["getNestedData"])){
            $this->getNestedData=$params["getNestedData"];
        }else{
            $this->getNestedData=true;
        }
        
        if(isset($params["initialSortColumn"])){
            $this->initialSortColumn=$params["initialSortColumn"];
        }
        else{
            $this->initialSortColumn=null;
        }
        
        $this->addIncludes();
        $this->buildSubComponents();
    }
    
    public function addIncludes(){
        $this->pageObj->addDataTableIncludes();
        $this->pageObj->addCustomDataTableIncludes();
        $this->pageObj->addSearchFieldIncludes();
    }
    
    public function buildSubComponents(){
        $this->buildDataSource();
        $this->buildSearchGrid();
        $this->buildButtons();
    }
    
    public function buildDataSource(){
        
        $params= new ParamBuilder();
        $params->add("id","{$this->id}_DataSource");
        $params->add("pageObj",$this->pageObj);
        $params->add("objDef",$this->objDef);
        
        $params->add("hideProps",$this->hideColumns);
        
        if (is_null($this->mode)===False) {
            $params->add("mode",$this->mode);
        }
        
        if(!is_null($this->whereStatement)){
            $params->add("whereStatement",$this->whereStatement);
        }
        
        // needed for the old form page.
        if($this->getNestedData){
            $params->add("buildForm",true);
        }else{
            $params->add("buildForm",false);
        }
        
        $this->dataSource=new MySQLObjDefSource($params->getParams());
        
        if(!$this->initialSortColumn!==null){
            $this->dataSource->sortCol=$this->dataSource->selectBuilder->getColumn($this->initialSortColumn);
        }
        $this->addComponent($this->dataSource);
    }
    
    public function buildSearchGrid(){
        
        // add search filter at the top of the datagrid.
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_Search");
        $params->add("parentObj",$this);
        $params->add("searchButtonName","Filter Grid");
        $params->add("clearButtonName","Clear Filter");
        $params->add("pageObj",$this->pageObj);
        
        $this->searchObj=new SearchComponent($params->getParams());
        
        
        $this->searchObj->addFieldsFromObjDef($this->objDef);
        
        
        $this->searchObj->objDef=$this->objDef;
        $this->searchObj->dataTableName="{$this->id}_Grid";
        $this->searchObj->build();      
        $this->addComponent($this->searchObj);
        
        // build datagrid
        
        $params = new ParamBuilder();
        $params->add("id", "{$this->id}_Grid");
        $params->add("parentObj",$this);
        $params->add("pageObj",$this->pageObj);
        $params->add("dataSourceId",$this->dataSource->id);
        $params->add("pagingEnabled",true);
        $params->add("rowsPerPage",$this->rowsPerPage);
        $params->add("dynamicData",true);
        $params->add("enableSelectRows",true);
        $params->add("selectOneMode",$this->selectOneMode);
        $params->add("enableDragDrop",false);
        $params->add("initialLoad", $this->initialLoad);
        $params->add("objDef",$this->objDef);        
        $params->add("hiddenColumns",$this->hideColumns);
        $params->add("initialSortColumn",$this->initialSortColumn);
        
        
        $this->grid=new DataGridComponent($params->getParams());
        $this->grid->buildFromObjDef($this->objDef);
        $this->addComponent($this->grid);
    }
    
    public function buildButtons(){
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_select");
        $params->add("text",$this->selectButtonText);
        // if onSelectCallback is blank, the event will simply be propogate up the page
        if($this->onSelectCallback!==""){
            $params->add("clickCallback",$this->onSelectCallback);
        }
        $selectButton=new ButtonComponent($params->getParams());
        $this->addComponent($selectButton);
        
        
        $params=new ParamBuilder();
        $params->add("id", "{$this->id}_Grid_Buttons");
        $buttonGroup=new ButtonGroupComponent($params->getParams());
        
        if($this->enableClearAll){
            $params=new ParamBuilder();
            $params->add("id","{$this->id}_clearAll");
            $params->add("text","Clear All");
            $params->add("clickCallback","{$this->grid->id}.unCheckAll");
            $params->add("callbackScope","{$this->grid->id}");
            $clearAllButton=new ButtonComponent($params->getParams());
            $buttonGroup->addButton($clearAllButton);
        }
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_cancel");
        $params->add("text","Cancel");
        $params->add("clickCallback","{$this->id}_cancel");
        $cancelButton=new ButtonComponent($params->getParams());
        $buttonGroup->addButton($cancelButton);
        
        $this->addComponent($buttonGroup);
    }
    
    public function renderJavascript($strbld){
        $this->renderCallbackFunctions($strbld);
        parent::renderJavascript($strbld);
         $strbld->addLines(
        "
        <script>
            // grids are rendered after page load so all the grid events must be added after load as well in order to ensure the grid exists.
            YAHOO.util.Event.addListener(window,'load',function(){
                // After the data is loaded and the grid is rendered the size of the popup changes and you must recheck that it fits in
                // the viewport, if not the panel must be resized.
                {$this->grid->id}.subscribe('renderEvent',function(e){
                    if($this->id.cfg.getProperty('visible')===true){
                        if({$this->id}.fitsInViewport()===false){
                            var viewportHeight=YAHOO.util.Dom.getViewportHeight();
                            var viewportWidth=YAHOO.util.Dom.getViewportWidth();
                            var dialogHeight=(viewportHeight-100) + 'px';
                            var dialogWidth=(viewportWidth * .70) + 'px';
                            {$this->id}.cfg.setProperty('height',dialogHeight);
                            {$this->id}.cfg.setProperty('width',dialogWidth);
                            {$this->id}.center();
                        }
                    }
                });
            });
        </script>
        ");
    }
    
    public function renderCallbackFunctions($strbld){
        if($this->onSelectCallback==="{$this->id}_getRows_default"){
            $this->renderSelectCallback($strbld);
        }
        $this->renderCancelCallback($strbld);
    }
    
    public function renderSelectCallback($strbld){
        $keyToPropNameMap=$this->grid->getKeyToPropNameMap();
        $jsonKeyMap=json_encode($keyToPropNameMap);
        
        $strbld->addLines(
"
<script>
   var {$this->id}_getRows_default=function(e){
    
        var records={$this->grid->id}.getSelectedRows();
        var key,key2;
        var recordsArray=[];
        var keyToPropMap={$jsonKeyMap};
        
        for(key in records){
            var newRecord={};
            var oldRecord=records[key];
            for(key2 in keyToPropMap){
                var propName=keyToPropMap[key2];
                var displayName='display-'+propName;
                newRecord[propName]=oldRecord[key2];
                newRecord[displayName]=oldRecord[key2];
            }
            recordsArray.push(newRecord);
        }
        var field={$this->formId}.getFieldByName('{$this->fieldName}');
        field.setValue(recordsArray);
        {$this->id}.hide();
        
    } 
</script>
");
    }
    
    public function renderCancelCallback($strbld){
        $strbld->addLines(
"
<script>
    var {$this->id}_cancel=function(e){
        {$this->id}.hide();
    }
</script>
");
    }
    
    public function renderCSS($strbld){
        parent::renderCSS($strbld);
        $strbld->addLines(
"
<style>
    .yui-panel .bd button{
        margin-top: 0.5em;
    }
</style>
");
    }
}

?>