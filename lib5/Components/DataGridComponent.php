<?php
class DataGridComponent extends Component {
    
    public $pagingEnabled;
    public $dynamicData;
    public $rowsPerPage;
    public $columns;
    public $keys;
    public $showHideColumns;
    public $enableSelectRows;
    public $numberOfRowsPerPageIntervals;
    public $hiddenColumns;  // an array of column names
    public $columnOrder; // array of column names and the order they should be displayed in the data grid
    
    // yui tables require a datasource, they can be in memory datasources.
    public $dataSourceName;
    public $draggableColumns;
    public $navPosition;
    
    public $enableDragDrop;
    public $highlightRow;
    public $columnsByName;
    public $initialLoad;
    public $bodyId;
    public $scrollToTopOnPageChange;
    public $topPaginatorName;
    public $persistState;
    public $enableExportCSV; 
    public $selectOneMode;
    
    public function __construct($params) {
        parent::__construct($params);
        
        $this->addAction("webservice","saveSettings");
         $this->addAction("webservice","resetState");
        
        $this->columns=array();
        $this->columnByName=array();
        
        if(isset($params["pagingEnabled"]) && !is_null($params["pagingEnabled"]))
        {
            $this->pagingEnabled=$params["pagingEnabled"];
        }
        else
        {
            $this->pagingEnabled=true;
        }
        
        if(isset($params["dynamicData"]) && !is_null($params["dynamicData"]))
        {
            $this->dynamicData=$params["dynamicData"];
        }
        else
        {
            $this->dynamicData=true;
        }
        
        if(isset($params["rowsPerPage"]) && !is_null($params["rowsPerPage"]))
        {
            $this->rowsPerPage=$params["rowsPerPage"];
        }
        else
        {
            $this->rowsPerPage=20;
        }
        
        if(isset($params["dataSourceId"]) && !is_null($params["dataSourceId"]))
        {
            $this->dataSourceId=$params["dataSourceId"];
        }
        else
        {
            throw new Exception("The parameter 'dataSourceId' is required");
        }
        
        if(isset($params["draggableColumns"]) && !is_null($params["draggableColumns"]))
        {
            $this->draggableColumns=$params["draggableColumns"];
        }
        else
        {
            $this->draggableColumns=true;
        }
        
        if(isset($params["columns"]) && !is_null($params["columns"]))
        {
            $columns=$params["columns"];
            if(is_array($columns))
            {
                foreach($columns as $column)
                {
                    $this->addColumn($column);
                }
            }
            else
            {
                $this->addColumn($column);
            }
        }
        
        if(isset($params["showHideColumns"]) && !is_null($params["showHideColumns"]))
        {
            $this->showHideColumns=$params["showHideColumns"];
        }
        else
        {
            $this->showHideColumns=true;
        }
        
        if(isset($params["enableSelectRows"]) && !is_null($params["enableSelectRows"]))
        {
            $this->enableSelectRows=$params["enableSelectRows"];
        }
        else
        {
            $this->enableSelectRows=false;
        }
        
        if(isset($params["enableDragDrop"]) && !is_null($params["enableDragDrop"]))
        {
            $this->enableDragDrop=$params["enableDragDrop"];
        }
        else
        {
            $this->enableDragDrop=false;
        }
        
        if(isset($params["numberOfRowsPerPageIntervals"]) && !is_null($params["numberOfRowsPerPageIntervals"]))
        {
            $this->numberOfRowsPerPageIntervals=$params["numberOfRowsPerPageIntervals"];
        }
        else
        {
            $this->numberOfRowsPerPageIntervals=5;
        }
        
        if(isset($params["navPosition"])){
            $this->navPosition=strtolower($params["navPosition"]);
        }
        else{
            $this->navPosition='both';
        }
        
        if(isset($params["highlightRow"])){
            $this->highlightRow=$params["highlightRow"];
        }
        else{
            $this->highlightRow=true;
        }
        
        if(isset($params["hiddenColumns"])){
            $this->hiddenColumns=$params["hiddenColumns"];
        }
        else{
            $this->hiddenColumns=array();
        }

        if(isset($params["columnOrder"])){
            $this->columnOrder=$params["columnOrder"];
        }
        else{
            $this->columnOrder=array();
        }
        
        if(isset($params["initialLoad"])){
            $this->initialLoad=$params["initialLoad"];
        }
        else{
            $this->initialLoad=true;
        }
        
        if(isset($params["bodyId"])){
            $this->bodyId=$params["bodyId"];
        }
        else{
            $this->bodyId="yahoo-com";
        }
        
        if(isset($params["scrollToTopOnPageChange"])){
            $this->scrollToTopOnPageChange=$params["scrollToTopOnPageChange"];
        }
        else{
            $this->scrollToTopOnPageChange=false;
        }
        
        if(isset($params["persistState"])){
            $this->persistState=$params["persistState"];
        }
        else{
            $this->persistState=false;
        }
        
        if(isset($params["enableExportCSV"])){
            $this->enableExportCSV=$params["enableExportCSV"];
        }
        else{
            $this->enableExportCSV=false;
        }
        
        if(isset($params["exportCSVCallback"])){
            $this->exportCSVCallback=$params["exportCSVCallback"];
        }
        else{
            $this->exportCSVCallback="{$this->id}_onExportCSV";
        }
        
        if(isset($params["initialSortColumn"])){
            $this->initialSortColumn=$params["initialSortColumn"];
        }
        
        if(isset($params["initialSortDir"])){
            $this->initialSortDir=$params["initialSortDir"];
        }
        else{
            $this->initialSortDir="asc";
        }
        
        $this->keys=array();
        
        if(get_class($this)=="DataGridComponent"){
            $this->buildSubComponents();
        }
        
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        } else {
            $this->objDef = null;        
        }
        
        if(isset($params["compositeDef"])){
            $this->compositeDef=$params["compoisteDef"];
        }
        else{
            $this->compositeDef=null;
        }
        
        if(isset($params["selectOneMode"])){
            $this->selectOneMode=$params["selectOneMode"];
        }
        else{
            $this->selectOneMode=false;
        }
        
        if(isset($params["showAllColumns"])){
            $this->showAllColumns=$params["showAllColumns"];
        }
        else{
            $this->showAllColumns=false;
        }
        
        /*
         * When mapSortKeysForRequest is true, the sortColumn is mapped from its key to its propDefName
         * before sending the request. This is needed for objDefs, but not needed for composite defs.
         */
        if(isset($params["mapSortKeysForRequest"])){
            $this->mapSortKeysForRequest=$params["mapSortKeysForRequest"];
        }
        else{
            $this->mapSortKeysForRequest=true;
        }
    }
    
    public function buildSubComponents(){
        if($this->enableExportCSV){
            $params=new ParamBuilder();
            $params->add("id","{$this->id}_exportAsCSVButton");
            $params->add("text","Export Data as CSV");
            $params->add("clickCallback",$this->exportCSVCallback);
            $params->add("newline",true);
            $this->exportCSVButton=new ButtonComponent($params->getParams());
            $this->addComponent($this->exportCSVButton);
        }        
    }
    
    public function clearColumnOrder() {
        $this->columnOrder=array();
    }
    
    public function setFirstColumn($columnName) {
        $this->columnOrder[]=$columnName;
    }
    
    public function setNextColumn($columnName) {
        $this->columnOrder[]=$columnName;
    }
        
    public function addColumn($column)
    {
        if(is_a($column, "DataColumnComponent"))
        {
            $this->columns[$column->key]=$column;
            $this->columnsByName[$column->propDefName]=$column;
        } 
    }
    
    # Adds new button column not associated with any existing data
    public function addButtonColumn($id, $key, $caption){
        $params=array(
          "id"=>$id,
          "key"=>$key,
          "label"=>$caption   
        );
        $column=new ButtonColumnComponent($params);
        $this->columnsByName[$id]=$column;
        $this->columns[$key]=$column;
    }
    
    # Takes an existing column, with existing data and overlays a link
    public function addLinkToColumn($columnName,$url,$valuesToSubstitute=array()){
        if(!isset($this->columnsByName[$columnName])){
            throw new Exception("The column '{$columnName}' does not exist (addLinkToColumn)");
        }
        $column=$this->columnsByName[$columnName];
        $column->url=$url;
        $column->colType="link";
        $column->valuesToSubstitute=$valuesToSubstitute;
    }

    public function getColumn($columnName)
    {
        if (array_key_exists($columnName, $this->columnsByName)) {
            $column = $this->columnsByName[$columnName];
            return $column;
        } else {
            return Null;
        }
    }
    
    public function addHiddenColumn($columnName)
    {
        $this->hiddenColumns[]=$columnName;
    }

    
    public function render($strbld) {        
        $this->renderHTML($strbld);
        $this->renderJavascript($strbld);
        $this->renderCSS($strbld);
    }
    
    public function renderHTML($strbld)
    {
        if($this->pagingEnabled && ($this->navPosition=='both' || $this->navPosition=='top')){
            $this->renderPaginatorTop($strbld);
        }
        
        if($this->persistState){
            $this->renderResetButton($strbld);
        }
        
        // this is the div for the datagrid
        $strbld->addLine("<div id='{$this->id}'></div>");
        
        if($this->pagingEnabled && ($this->navPosition=='both' || $this->navPosition=='bottom'))
        {
            $this->renderPaginatorBottom($strbld);
        }
        
       
        
        if($this->enableExportCSV){
            $this->exportCSVButton->renderHTML($strbld);
        }
        
    }
    
    public function renderPaginatorTop($strbld){
        $strbld->addLine("<div id='{$this->id}_paginator_top'></div>");
        $this->topPaginatorName="{$this->id}_paginator_top";
    }
    
    public function renderPaginatorBottom($strbld)
    {
        $strbld->addLine("<div id='{$this->id}_paginator_bottom'></div>");
    }
    
    public function renderResetButton($strbld){
        $strbld->addLine("<button id='{$this->id}_resetButton' type='button' onClick='{$this->id}.resetState()'>Reset Grid Settings</button>");
    }
    
    public function renderJavascript($strbld)
    {
        $strbld->addLine("<script type='text/javascript'>");
       
        $this->renderRequestBuilder($strbld);
        
        
        $this->renderCreateDataTableJavascript($strbld);
        
        
        $this->renderSendStateToServer($strbld);
        $this->renderResetStateJavascript($strbld);
        
        $strbld->addLine("</script>");
        
        if($this->enableExportCSV){
            $this->exportCSVButton->renderJavascript($strbld);
            $this->renderExportCSVCallback($strbld);
        }
    }
    
    
    public function renderCreateDataTableJavascript($strbld)
    {
       global $app;
        
       $strbld->addLine("var {$this->id};");
       $strbld->addLine("var create{$this->id}=function(){");
       $strbld->increaseIndentLevel();
       
       if($this->persistState){
       
           $state=$app->userStorage->retrieve("{$this->pageObj->id}_{$this->id}_state",false);

           if(is_null($state)){
               $this->renderColumnDefs($strbld);
           }else{
               $state=json_decode($state, $assoc=true);
               $columnDefs=$state["columnState"];

               # certain columns that do not contain data, like the edit,view, checkbox, and view columns
               # are hardcoded to rendered everytime the datagrid is rendered, to avoid duplicating this columns they
               # must be removed from the column persistence data before rendering the data grid. The
               # columns to remove is specified in the javascript in the CustomDataTable2.js
               if(isset($state["columnIndexesToRemove"])){
                   foreach($state["columnIndexesToRemove"] as $index){
                       unset($columnDefs[$index]);
                   }
               }
               
               # columns link buttons and links have special formatters associated with them that are
               # determined based on the column type, the types of the columns must be preserved as well
               # to ensure that the correct formatters are applied to these columns.
               if(isset($state["columnTypes"])){
                    foreach($state["columnTypes"] as $colName=>$data){
                        $columnDefs[$data["index"]]["colType"]=$data["colType"];
                    }
               }
               
               # check to make sure that all the columns being persisted exist in the grid definition, if not remove them.
               # unless the column is a button column, which is not defined in the objdef.
               $colIndexesToRemove=array();               
               foreach($columnDefs as $index=>$colDef){                    
                    if(!isset($this->columns[$colDef["key"]])){
                        $colIndexesToRemove[]=$index;
                    }
               }
               foreach($colIndexesToRemove as $index){
                   unset($columnDefs[$index]);
               }
               
               # check to make sure that all the columns in the grid def are in the persistence data, if not
               # add the columns to the end of the grid def.                             
               $persistColNames=array();
               foreach($columnDefs as $colDef){
                   $persistColNames[]=$colDef["key"];
               }
               $defColNames=array_keys($this->columns);
               $colToAdd=array_diff($defColNames,$persistColNames);
               
               foreach($colToAdd as $colName){
                    $columnDefs[]=$this->columns[$colName];                   
               }               
               
               # columns are stored in a standard array indexed by numbers, after removing columns
               # the array must be reindex to reflect the fact that entries were removed.
               $columnDefs=array_values($columnDefs);
               $jsonColumnDefs=json_encode($columnDefs);
               $strbld->addLine("var {$this->id}_ColumnDefs={$jsonColumnDefs};");
           }
       } else {
           $this->renderColumnDefs($strbld);
       }
              
       $this->renderDataGridConfig($strbld);
       
       $strbld->addLine("{$this->id}=new CustomWidgets.DataTable('{$this->id}',{$this->id}_ColumnDefs,{$this->dataSourceId},{$this->id}_Config);");
       
       if($this->enableDragDrop){
           $this->renderDragDrop($strbld);
       }
       
       if($this->highlightRow){
           $this->renderHighlightRow($strbld);
       }
       
       $strbld->decreaseIndentLevel();

       $strbld->addLine("}");
       $strbld->addLine("YAHOO.util.Event.addListener(window,'load',create{$this->id});");
        
    }
    
    public function renderColumnDefs($strbld)
    {
        $strbld->addLine("var {$this->id}_ColumnDefs=[");

        $colDefs=array();
        
        # only add the display and id columns initially and any summary columns defined, by default for
        # tables defined by an ObjDef
        if(isset($this->objDef)){
            $columnsToShow=array();
            $columnsToShow[]=$this->objDef->getAttribute("idColumn");
            $columnsToShow[]=$this->objDef->getAttribute("displayColumn");
            $columnsToShow=array_unique(array_merge($columnsToShow,array_keys($this->objDef->getPrimaryKey())));
            $columnsToShow=array_unique(array_merge($columnsToShow,$this->objDef->getAttribute("summaryColumns",array())));
        }

        foreach($this->columns as $column)
        {
            if(isset($this->objDef)){
                if(!$this->showAllColumns && !in_array($column->propDefName, $columnsToShow)){
                    $column->hidden=true;
                }
            }
            $colString=$column->render();
            $colDefs[]=$colString;
        }
        
        $strbld->addList($colDefs);

        $strbld->decreaseIndentLevel();

        $strbld->addLine("];");
    }
    
    public function renderDataGridConfig($strbld)
    {
        global $app;
        
        $keys=json_encode($this->keys);
        if($this->pagingEnabled)
        {
             $strbld->addLine("var {$this->id}_templateString='{FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink} &nbsp {RowsPerPageDropdown} <BR/> {CurrentPageReport} ';");
        }
        
        $strbld->addLine("var {$this->id}_Config={");
        $strbld->increaseIndentLevel();
        $configList=array();
        
        /*
         * Edit and delete redirects both require the key of the object to edit or delete. The keys
         * variable stores what the name of the key columns so that the javascript callbacks know what
         * data to get from the row that was clicked on.
         */
        if(count($this->keys)>0){
            $configList[]="keys:{$keys}";
        }
        
        $jsonPropNameToKeyMap=json_encode($this->getPropNameToKeyMap());
        $configList[]="propNameToKeyMap:{$jsonPropNameToKeyMap}";
        
        $jsonKeyToPropNameMap=json_encode($this->getKeyToPropNameMap());
        $configList[]="keyToPropNameMap:{$jsonKeyToPropNameMap}";
        
        if($this->draggableColumns)
        {
           $configList[]="draggableColumns:true"; 
        }
        
        if($this->showHideColumns){
            $configList[]="showHideColumnsDlg:true";
        }
        
        if($this->enableSelectRows){
            $configList[]="enableSelectRows:true";
        }
        
        if($this->selectOneMode){
            $configList[]="selectOneMode:true";
        }
        
        if($this->dynamicData)
        {
            $configList[]="dynamicData:true";
            $configList[]="generateRequest:{$this->id}_generateRequest";
        }
        
        // restore UI sort state if persistance is enabled
        if($this->persistState){
            $state=$app->userStorage->retrieve("{$this->pageObj->id}_{$this->id}_state",false);
            if(!is_null($state)){
                $state=json_decode($state, $assoc=true);
                if(isset($state["sortState"])){
                    $sortedData=$state["sortState"];
                    // must check to see if the persisted sort column exists in the grid,
                    // this may not be the case if the spec has changed.
                    if(isset($this->columns[$sortedData["key"]])){
                        $jsonSortedBy=json_encode($state["sortState"]);
                        $configList[]="sortedBy:$jsonSortedBy";
                    }
                }                
            }
        }
        
        if(isset($this->initialSortColumn)){
            $sortConfig=array();
            $sortConfig["key"]=$this->getColumnFieldByName($this->initialSortColumn);
            if($this->initialSortDir==="asc"){
                $sortConfig["dir"]="YAHOO.widget.DataTable.CLASS_ASC";
            }
            else{
                $sortConfig["dir"]="YAHOO.widget.DataTable.CLASS_DESC";
            }
            $configList[]="sortedBy:{key:'{$sortConfig["key"]}',dir:{$sortConfig['dir']}}";
        }
        
        if($this->pagingEnabled)
        {
            
            $rowsPerPageValue="[";
            $pageInterval=$this->rowsPerPage;
            $firstValue=true;
            
            if($this->scrollToTopOnPageChange){
                $configList[]="scrollToTopOnPageChange:true";
                $configList[]="topPaginatorName:'{$this->topPaginatorName}'";
            }
            
            $configList[]="bodyId: '{$this->bodyId}'";
            
            // If state persistence is enabled add the sortedBy information to the intial request, this requires a conversion.
            if($this->persistState){
                $state=$app->userStorage->retrieve("{$this->pageObj->id}_{$this->id}_state",false);                
                // if the state is not null you must still check the for the appropriate state variables
                // this is required when upgrading the code to include additional information and the user's
                // state information is out of date. This will be corrected automatically that next time the user
                // makes a change to the state of the database.
                if(!is_null($state)){                    
                    $state=json_decode($state, $assoc=true);                    
                       if(isset($state["paginatorState"])){
                        $startingRow=$state["paginatorState"]["recordOffset"];
                        $rowsPerPage=$state["paginatorState"]["rowsPerPage"];
                    }
                    else{
                        $startingRow=0;
                        $rowsPerPage=$this->rowsPerPage;
                    }
                    $initialRequest="initialRequest:'&numberOfRows={$rowsPerPage}&startingRow={$startingRow}&whereStatement={$this->pageObj->id}_{$this->id}_where";                                        
                    if(isset($state["sortState"])){                               
                        $sortedData=$state["sortState"];
                        // must check to see if the persisted sort column exists in the grid,
                        // this may not be the case if the spec has changed.
                        if(isset($this->columns[$sortedData["key"]])){
                            // convert column key to column name
                            $keyToColumnMap=$this->getKeyToPropNameMap();
                            $sortColumn=$keyToColumnMap[$state["sortState"]["key"]];

                            // convert YUI sort values to regular sort values
                            $sortValueMap=array("yui-dt-asc"=>"asc","yui-dt-desc"=>"desc");
                            $sortDir=$sortValueMap[$state["sortState"]["dir"]];                          
                            $initialRequest.="&sortBy={$sortColumn}&sortDir={$sortDir}";
                        }
                    }               
                    // close initial Request string
                    $initialRequest.="'";
                    $configList[]=$initialRequest;
                 
                }
                else{
                    $configList[]="initialRequest:'&numberOfRows={$this->rowsPerPage}&startingRow=0&whereStatement={$this->pageObj->id}_{$this->id}_where'";
                }
                
            }
            else{
                $configList[]="initialRequest:'&numberOfRows={$this->rowsPerPage}&startingRow=0&whereStatement={$this->pageObj->id}_{$this->id}_where'";
            }
            
            if($this->initialLoad===false){
                $configList[]="initialLoad:false";
            }
            for($i=0;$i<$this->numberOfRowsPerPageIntervals;$i++){
                if($firstValue){
                    $rowsPerPageValue.="$pageInterval";
                    $firstValue=false;
                }
                else{
                    $rowsPerPageValue.=",";
                    $rowsPerPageValue.="$pageInterval";
                }
                $pageInterval+=$this->rowsPerPage;
            }
            
            $rowsPerPageValue.="]";
            
            $rowsPerPage=$this->rowsPerPage;
            $pageNumber=1;
            if($this->persistState){
                $state=$app->userStorage->retrieve("{$this->pageObj->id}_{$this->id}_state",false);
                if(!is_null($state)){
                    $state=json_decode($state, $assoc=true);
                    if(isset($state["paginatorState"])){  
                        $pageNumber=$state["paginatorState"]["page"];
                        $rowsPerPage=$state["paginatorState"]["rowsPerPage"];
                    }                    
                }
            }
            
            $paginatorConfig=trim(
"
paginator:new YAHOO.widget.Paginator({
    initialPage:{$pageNumber},
    rowsPerPage:{$rowsPerPage},
    template: {$this->id}_templateString,
    containers:['{$this->id}_paginator_top','{$this->id}_paginator_bottom'],
    pageReportTemplate:'<b>displaying {startRecord} - {endRecord} of {totalRecords} results</b>',
    pageLinks:5,
    // this is a temporary value till the data comes back.
    totalRecords: YAHOO.widget.Paginator.VALUE_UNLIMITED,
    rowsPerPageOptions:$rowsPerPageValue,
    alwaysVisible: true
    })
");

            $configList[]=$paginatorConfig;
            
        }
        $strbld->addList($configList);
        $strbld->decreaseIndentLevel();
        $strbld->addLine("}");
    }
    
    public function renderRequestBuilder($strbld)
    {
        $keyToNameMap=$this->getKeyToPropNameMap();
        $keyToNameMapJSON=json_encode($keyToNameMap);
        $strbld->addLines(
"
{$this->id}_generateRequest=function(oState,oSelf){
     // Set defaults (same as in YUIDatatable source)
    oState = oState || {pagination:null, sortedBy:null};
    var sortKey = encodeURIComponent((oState.sortedBy) ? oState.sortedBy.key : null);
    var dir = (oState.sortedBy && oState.sortedBy.dir === YAHOO.widget.DataTable.CLASS_DESC) ? 'desc' : 'asc';
    var startIndex = (oState.pagination) ? oState.pagination.recordOffset : 0;
    var results = (oState.pagination) ? oState.pagination.rowsPerPage : null;    
    var keyToNameMap={$keyToNameMapJSON};

    // Build Request in format that fits in with framework
    
    var request='';");
    if($this->mapSortKeysForRequest){
        $strbld->addLine("
    if(!YAHOO.lang.isNull(oState.sortedBy)){
        request+='&sortBy=' + keyToNameMap[sortKey] +'&sortDir=' + dir;
    }    
");
    }
    else{
        $strbld->addLine("
    if(!YAHOO.lang.isNull(oState.sortedBy)){
        request+='&sortBy=' + sortKey +'&sortDir=' + dir;
    }    
"); 
    }
      $strbld->addLines(
"
    if(!YAHOO.lang.isNull(results)){
        request+='&startingRow=' + startIndex + '&numberOfRows=' + results;
    }

    request+='&whereStatement=' + '{$this->pageObj->id}_{$this->id}_where';
    return request;
}");
    }
    
    public function renderSendStateToServer($strbld){
        $strbld->addLines(
"
YAHOO.util.Event.addListener(window,'load',function(){
    {$this->id}.sendStateToServer=function(state){
        var stateString=encodeURIComponent(YAHOO.lang.JSON.stringify(state));

        var URL ='index.php?pageId={$this->pageObj->id}&resourceId={$this->pageObj->page->resourceId}&controllerId={$this->id}&actionName=saveSettings&buildPage=false';
        var postString='state=' + stateString;

        var onSuccess=function(response){return true;}
        var onFailure=function(response){return false;}
        var callback={success:onSuccess,failure:onFailure};

        YAHOO.util.Connect.asyncRequest('POST',URL,callback,postString);
    }
});
");
    }
    
    public function renderResetStateJavascript($strbld){
        $strbld->addLines(
"
YAHOO.util.Event.addListener(window,'load',function(){
    {$this->id}.resetState=function(state){
        var stateString=YAHOO.lang.JSON.stringify(state);

        var URL ='index.php?pageId={$this->pageObj->id}&resourceId={$this->pageObj->page->resourceId}&controllerId={$this->id}&actionName=resetState&buildPage=false';     

        var onSuccess=function(response){
            // Reseting the state of the datagrid requires a page refresh
            window.location.reload()            
            
            return true;
        }
        var onFailure=function(response){return false;}
        var callback={success:onSuccess,failure:onFailure};

        YAHOO.util.Connect.asyncRequest('GET',URL,callback);
    }
});
");
    }
    
    public function renderExportCSVCallback($strbld){
        $strbld->addLines(
"
<script>
    {$this->id}_onExportCSV=function(){
        // the state must be saved to the server before exporting. This is needed when the grid is in
        // its initial state and the user has 
        window.location.href='index.php?pageId={$this->pageObj->id}&controllerId={$this->dataSourceId}&actionName=saveDataAsCSV&whereStatement={$this->pageObj->id}_{$this->id}_where&gridName={$this->id}&buildPage=false';
    }
</script>
");
    }
    
    public function renderCSS($strbld){
        if($this->enableExportCSV){
            $strbld->addLines(
"<style>
    #{$this->exportCSVButton->id}{
        display:block;
        margin-bottom: .5em;
    }
</style>
");
        }
    }
    
    public function buildFromObjDef($objDef){  
        $this->objDef=$objDef;
        
        if(count($objDef->propertiesOrdered)>0){
            $props=$objDef->propertiesOrdered;
        }
        else{
            $props=$objDef->getProperties();
        }
        
        foreach($props as $propDef)
        {
            if($this->includePropDef($propDef)===false){
                continue;
            }
            $column=$this->createColumnFromPropDef($propDef);            
            $this->addColumn($column);            
        }
        
        $this->setKeysFromObjDef($objDef);
    }
    
    /*
     * This function does not seem to be used and can most likely be removed.
     */
    public function applyUpdates()
    {
         foreach($this->hiddenColumns as $columnName) {             
            if (array_key_exists($columnName, $this->columns)) {
                $column  =  $this->columns[$columnName];
                $column->hidden = true;
            }
         }
         
         $columnsReorder = array();
         foreach($this->columnOrder as $columnName) {  
            
            if (array_key_exists($columnName, $this->columns)) {                
                $column  =  $this->columns[$columnName];                
                $columnsReorder[$column->key] = $column;                
                unset($this->columns[$columnName]);
            }             
         }
         // add remaining columsn to the end         
         foreach($this->columns as $column) {  
            $columnsReorder[$column->key] = $column;                
         }         
         $this->columns = $columnsReorder;
    }
        
    public function buildFromSelectBuilder($selectBuilder)
    {        
        foreach($selectBuilder->columns as $sqlColumn)
        {          
            $column=$this->createColumnFromSQLColumn($sqlColumn);
            $this->addColumn($column);
        }
    }
    
    /*
     * This function takes a CompositeObjDef and builds a grid. By default, all of the
     * properties specified in the compoisteObjDef are added to the grid and no other columns.
     * The order of the columns is the same as the order in which the properties where added to
     * the CompositeObjDef.
     */
    public function buildFromCompositeObjDef($compositeObjDef){
        global $app;
        $this->compositeObjDef=$compositeObjDef;
        
        foreach($compositeObjDef->getProperties() as $property){
            $colParms=new ParamBuilder();
            if(is_a($property,"CompositeProperty")){
                $propDef=$property->propDef;
                $columnName="{$property->objectName}_{$property->propertyName}";
                $colParms->add("id","{$this->id}/$columnName");
                $colParms->add("key",$columnName);
                $colParms->add("field",$columnName);
                $colParms->add("propDefName",$propDef->name);
                $colParms->add("colType",$propDef->objectType);
                
                if(isset($property->params["alias"])){
                    $colParms->add("label",$property->params["alias"]);
                }
                else if($propDef->hasAttribute("caption"))
                {
                    $colParms->add("label",$propDef->getAttribute("caption")); 
                }
                $column=new DataColumnComponent($colParms->getParams());
                $this->addColumn($column);
            }
            else if(is_a($property,"CountProperty")){
                $columnName="count";
                $colParms->add("id","{$this->id}/$columnName");
                $colParms->add("key",$columnName);
                $colParms->add("field",$columnName);
                $column=new DataColumnComponent($colParms->getParams());
                $this->addColumn($column);
            }
            
        }
    }
    
    public function setKeysFromObjDef($objDef){
        
        foreach($objDef->primaryKey as $key=>$value)
        {
            $propDef=$objDef->getProperty($key);
            $this->keys[$propDef->name]=$this->getColumnFieldByName($propDef->name);
        }
    }
    
    public function addKey($columnName){
        $this->keys[$columnName]=$this->getColumnFieldByName($columnName);
    }
    
    public function getColumnFieldByName($colName){
        
        if (isset($this->columnsByName[$colName])) {
            $column=$this->columnsByName[$colName];
            return $column->field;
        }
        else {
            return null;
        }
    }
    
    public function getKeyToPropNameMap(){
        $map=array();
        foreach($this->columns as $column){
            $map[$column->key]=$column->propDefName;
        }
        return $map;
    }
    
    public function getPropNameToKeyMap(){
        $map=array();
        foreach($this->columns as $column){
            if (!isnull($column->columnAlias)) {
                $map[$column->columnAlias]=$column->key;
            } else {
                if (!isnull($column->propDefName)) {
                    $map[$column->propDefName]=$column->key;
                }
            }
        }
        return $map;
    }
    
    
    public function renderDragDrop($strbld){
        $strbld->addLines(
"
{$this->id}.subscribe('renderEvent',CustomWidgets.onDataTableRender);
{$this->id}.subscribe('rowAddEvent',CustomWidgets.onDataTableAdd);
");
    }
    
    public function renderHighlightRow($strbld){
        $strbld->addLines(
"
{$this->id}.subscribe('rowMouseoverEvent',{$this->id}.onEventHighlightRow);
{$this->id}.subscribe('rowMouseoutEvent',{$this->id}.onEventUnhighlightRow);
");
    }
    
    public function saveSettings($request){
        global $app;
        $state=$app->activeSession->getVarNoStrip('POST','state');
        
        $app->userStorage->store("{$this->pageObj->id}_{$this->id}_state",$state);
        
        $results=array("success"=>true);        
        return $results;
    }
    
    public function resetState($request){
        global $app;
        $app->userStorage->store("{$this->pageObj->id}_{$this->id}_state",null);
        $results=array("success"=>true);        
        return $results;
    }
    
    # build the column key from a propDef
    public function buildKeyFromPropDef($propDef){
        if($propDef->hasConstraint()===false){
            $table=$propDef->getAttribute("table",Null);       
            if (isnull($table)) {
                $columnName="{$propDef->name}";   
            } else {
                $columnName="{$table}_{$propDef->name}";   
            }
        }
        else{
            $constraint=$propDef->getConstraint();
            $table= $constraint->tableName;
            $columnName="{$table}_{$propDef->name}";   
        }
        return $columnName;
    }
    
    # This function takes a propDef and determines wheter to include it in the datagrid or not
    # based on a set rules definined in the function.
    public function includePropDef($propDef){        
        if ($propDef->isAttribute("excludeFromQuery",true) && $propDef->getAttribute("isDerived",false)==false) {
            return false;
        }
        else if($propDef->getAttribute("visible",true)===false){
            return false;
        }
        // collections and objects cannot be shown in a normal data grid
        else if($propDef->isCollection()==true || $propDef->isScalar()==false){
            return false;
        }
        else{
            return true;
        }
    }    
    
    public function createColumnFromPropDef($propDef){
        global $app;
        $colParms=new ParamBuilder();
        $propDefName=$propDef->name;
        $columnName=$this->buildKeyFromPropDef($propDef);            
        $colParms->add("id","{$this->id}/$columnName");
        $colParms->add("key",$columnName);
        $colParms->add("field",$columnName);
        $colParms->add("propDefName",$propDefName);
        $colParms->add("colType",$propDef->objectType);

        if($propDef->isAttribute("hidden",true) || in_array($propDef->name, $this->hiddenColumns)==true){
            $colParms->add("hidden",true);
        }
        
        if($propDef->hasAttribute("caption"))
        {
           $colParms->add("label",$propDef->getAttribute("caption")); 
        }

        if($propDef->objectType=="datetime"){
            $colParms->add("width",120);
        }

        $gridParams = $propDef->getAttribute("gridParams");
        if (!isnull($gridParams)) {
            if (isset($gridParams['width'])) {
                $colParms->add("width",$gridParams['width']);    
            }
            if (isset($gridParams['visible'])) {
                $colParms->add("hidden",!$gridParams['visible']);    
            }                
        }

        if($propDef->getAttribute("isDerived",false)===true){
            $colParms->add("sortable",false);
        }
        
        $column= new DataColumnComponent($colParms->getParams());
        
        return $column;
    }
    
    public function createColumnFromSQLColumn($sqlColumn){      
        global $app;
        
        $objDef=$this->objDef;
        $propDef = null;            
        if (!isnull($objDef)) {
                $propDef=$objDef->getProperty($sqlColumn->columnName);
            if (is_null($propDef)){
                $propDef=$objDef->getProperty($sqlColumn->columnAlias);
            }
        }

        if(isset($sqlColumn->columnAlias)){
            $name = "{$sqlColumn->tableRef}_{$sqlColumn->columnAlias}";
        }
        else{
            $name = "{$sqlColumn->tableRef}_{$sqlColumn->columnName}";
        }

        $colParms=new ParamBuilder();

        $colParms->add("id","{$sqlColumn->tableAlias}/{$name}");
        $colParms->add("key",$name);
        $colParms->add("field",$name);
        if($sqlColumn->label===null && $sqlColumn->columnAlias!==null){
            $colParms->add("label",$sqlColumn->columnAlias);
        }
        else{
            $colParms->add("label",$sqlColumn->label);
        }

        if(in_array($sqlColumn->columnName,$this->hiddenColumns)){
            $colParms->add("hidden",true);
        }

        if (!is_null($propDef)){                

            $colParms->add("propDefName",$propDef->name);

            if($propDef->isAttribute("hidden",true)){
                $colParms->add("hidden",true);
            }

            if($propDef->objectType=="datetime"){
                $colParms->add("width",120);
            }

            $gridParams = $propDef->getAttribute("gridParams");
            if (!isnull($gridParams)) {
                if (isset($gridParams['width'])) {
                    $colParms->add("width",$gridParams['width']);    
                }
                if (isset($gridParams['visible'])) {
                    $colParms->add("hidden",!$gridParams['visible']);    
                }                
            }
        }
        else{
            if(!$sqlColumn->isSQL){
                $objDef=$app->specManager->findDef($sqlColumn->tableRef);
                // The table may not exists in the database because it is created by a join
                if($objDef!==null){
                    $propDef=$objDef->getProperty($sqlColumn->columnName);
                    
                    if (isnull($propDef)) {
                        $colParms->add("propDefName",Null);
                    } else {
                        $colParms->add("propDefName",$propDef->name);
                        $colParms->add("columnAlias",$sqlColumn->columnAlias);
                    }
                } else {
                    echo __FILE__.":".__LINE__.":Error";
                }
            }
        }
        $column= new DataColumnComponent($colParms->getParams());
        return $column;
    }
}

?>