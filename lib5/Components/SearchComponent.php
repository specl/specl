<?php

class SearchComponent extends InputExForm {
    
    public $fields; // array of SearchField
    public $dataTableName;
    public $searchButtonName;
    public $clearButtonName;
    public $objDef;
    
    
    public function __construct($params){
        
        if(!isset($params["id"]))
        { 
           throw new Exception("The parameter 'id' is required");
        }
        
        if(!isset($params["parentObj"]))
        {
            throw new Exception("The parameter 'parentObj' is required");
        }
        
        parent::__construct($params["id"],$params["parentObj"]);
        
        if(isset($params["searchButtonName"])){
            $this->searchButtonName=$params["searchButtonName"];
        }
        else{
            $this->searchButtonName="Search";
        }
        
        if(isset($params["clearButtonName"])){
            $this->clearButtonName=$params["clearButtonName"];
        }
        else{
            $this->clearButtonName="Clear Search";
        }
        
        if(isset($params["dropdownTermLimit"])){
            $this->dropdownTermLimit=$params["dropdownTermLimit"];
        }
        else{
            $this->dropdownTermLimit=20;
        }
        
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        }
        else{
            $this->objDef=null;
        }
        
        if(isset($params["pageObj"])){
            $this->pageObj=$params["pageObj"];
        }
        else{
            throw (new Exception("The parameter 'pageObj' is required"));
        }
        
        if(isset($params["dataTableName"])){
            $this->dataTableName=$params["dataTableName"];
        }
        else{
            $this->dataTableName=null;
        }
        
        $this->submitAction="buildSearchQuery";
        $this->addAction("webservice","buildSearchQuery");
    }
    
    public function build($request=null){
        
        $pb = new ParamBuilder();
        $pb->add("id", "{$this->id}_search");
        $searchField = new InputExSearchField($pb->getParams());
        
        // The fields need to be sorted by there label name, not by the field name, since that is what appears to the user
        $searchLabels=array();
        foreach($this->searchFields as $key=>$field){
            $searchLabels[]=$field->fieldLabel;
        }
        usort($searchLabels,"strnatcasecmp");
        $orderedSearchFields=array();
        foreach($searchLabels as $label){
            foreach($this->searchFields as $key=>$field){
                if($field->fieldLabel===$label){
                    $orderedSearchFields[]=$field;
                    break;
                }
            }
        }
        foreach($orderedSearchFields as $key=>$field){
            $searchField->addField($field);
        }
        
        $this->addField($searchField);
    }
    
    public function renderHTML($strbld) {
        $this->renderTitleHTML($strbld);
        parent::renderHTML($strbld);
        $this->renderCloseDivWrapper($strbld);
    }
    
    public function renderTitleHTML($strbld){
        $strbld->addLine("<div> <button id='{$this->id}_expandCollapse' class='searchToggle' onclick='{$this->id}_ShowHideSearch()'>Hide Search</button></div>");        
        $strbld->addLine("<div id='{$this->id}_wrapper'>");
        $strbld->increaseIndentLevel();
    }
    
    public function renderCloseDivWrapper($strbld){
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</div>");
    }
    
    public function renderJavascript($strbld){
        $this->renderShowHideJavascript($strbld);
        parent::renderJavascript($strbld);
        $this->renderSetInitialValue($strbld);
        $this->renderFilterDeletedCallback($strbld);
    }
    
    public function renderShowHideJavascript($strbld){
        $strbld->addLines(
"
<script>
    var {$this->id}_ShowHideSearch=function(){
        var wrapperEl = document.getElementById('{$this->id}_wrapper');
        var toggleEl= document.getElementById('{$this->id}_expandCollapse');        
        if(wrapperEl.style.display === '' || wrapperEl.style.display === 'block') {
            wrapperEl.style.display = 'none';
            toggleEl.innerHTML = 'Show Search';
        }
        else {
            wrapperEl.style.display = 'block';
            toggleEl.innerHTML = 'Hide Search';
        }
    }
</script>
"
);
    }
    
    // when a filter is removed resubmit the form, like the user clicking on filter grid.
    public function renderFilterDeletedCallback($strbld){
        $strbld->addLines(
"
<script type='text/javascript'>
    YAHOO.util.Event.addListener(window,'load',function(){
        var searchListField={$this->id}.getFieldByName('{$this->id}_search');
        searchListField.filterDeleted.subscribe(function(){
            {$this->id}.onSubmit();
        });
    });
</script>
");

    }
    
    
    public function renderSetInitialValue($strbld)
    {
        global $app;
        
        $search=$app->userStorage->retrieve("{$this->pageObj->id}_{$this->id}_search",false);
        if(is_null($search)){
            $search="{'{$this->id}_search':[]}";
        }else{
            $whereStatement=$app->userStorage->retrieve("{$this->pageObj->id}_{$this->dataTableName}_where",false);
            if(!is_null($whereStatement)){
                $app->sessionStorage->store("{$this->pageObj->id}_{$this->dataTableName}_where",$whereStatement);
            }
        }
          
        $strbld->addLine("<script type='text/javascript'>");
        $strbld->increaseIndentLevel();
        $strbld->addLine("YAHOO.util.Event.addListener(window,'load',function(){");
        $strbld->increaseIndentLevel();
        $strbld->addLine("{$this->id}.setValue({$search},false)");
        $strbld->decreaseIndentLevel();
        $strbld->addLine("});");
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</script>");
    }
    
    public function renderAjaxConfig($strbld){
         $strbld->addLines(
"
ajax: {
    method:'POST',
    uri:'index.php?pageId={$this->pageObj->id}&resourceId={$this->pageObj->page->resourceId}&controllerId={$this->id}&actionName={$this->submitAction}&mode={$this->formMode}&buildPage=false',
    callback: {
        success:function(response)
        {
            {$this->id}.clearErrorMessages()
            var results=YAHOO.lang.JSON.parse(response.responseText);

            if(!YAHOO.lang.isUndefined(results.errorMessages))
            {
               var errors=results.errorMessages;
               {$this->id}.showErrorMessages(errors);
            } 
            else if (results.message == 'redirect') {                 
                 window.location.href = results.parameters.location;
            }
            else{
               // reset the paginator page to 1. This is required to change the recordOffset to 0 so the query will work properly.
               {$this->dataTableName}.get('paginator').setPage(1, true);            
               {$this->dataTableName}.refreshData('{$this->pageObj->id}_{$this->dataTableName}_where');
               
            }
            
        },
        failure: function(response){return false}
    }
");
    }
    
    public function renderFormButtons($strbld){
               $strbld->addLines(
"
buttons:[{type:'submit',value:'{$this->searchButtonName}'},
         {type:'submit',value:'{$this->clearButtonName}',
            onClick:function()
                {
                    {$this->id}.setValue({{$this->id}_search:[]});   
                }
            }
        ],");
    }
    
    public function addSearchField($field){
        if(is_a($field,"SearchField")){
            $this->searchFields[$field->fieldName]=$field;
        }
        else{
            throw new Exception("must add a object of type SearchField");
        }
    }
    
    /** 
     *  Create SearchFields from a ObjDef.
     *      
     *  The following is done for all properties:
     *      - the value of "caption" attribute in the propDef is set to 'fieldLabel'.
     *      - propDefs with the attribute 'excludeFromQuery' set to true are not added to the search component.
     *      - propDef types are mapped to field types based on $fieldTypeMap.
     *      - if a propDef has a 'table' attribute the fieldName is transformed to "$tableName.$fieldName".
     *  
     *  The mapped type is place in the "fieldType" variable if the field has no constraints.
     *      
     *  Properties with constraints are handled as follows:     
     *      - the fieldType is set to choice if the number of choices is <=$this->dropdownTermLimit.
     *      - the fieldType is set to autocomplete if the number of choices is >$this->dropdownTermLimit
     *      - the mapped type from the propDef is set to the choiceType attribute in the field
     *      - the choices are order according to the label based on a natural sort, case insensitive method in the case of dropdowns.
     *      - the null, empty value is place first
     *      - the choices attribute is set to a "$label=>$value" associative array if the fieldType is choice.
     *      - the choices attribute is set to an array of labels if the fieldType is autocomplete.
     * 
     */
    public function addFieldsFromObjDef($objDef){
        
        // mapping of objDefType to fieldType
        $fieldTypeMap=array(
            "string"=>"string",
            "float"=>"numeric",
            "integer"=>"numeric",
            "date"=>"date",
            "datetime"=>"datetime",
            "boolean"=>"boolean",
            "double"=>"numeric");
        
        foreach($objDef->getProperties() as $propDef){
            
            // TODO: add sub queires
            if (($propDef->isCollection()==false) && ($propDef->isScalar)) {
                                
                if($propDef->isAttribute("hideInPopUp",true)){
                    continue;
                }
                
                if($propDef->isAttribute("excludeFromQuery",true)){
                    continue;
                }
                
                if($propDef->getAttribute("visible",true)===false){
                    continue;
                }
                
                // Add the table name to the column to avoid unambiguious where clauses                
                if($propDef->hasAttribute("table")){
                    $tableName=$propDef->getAttribute("table");
                    $columnName="{$tableName}.{$propDef->name}";
                }
                else{
                    $columnName=$propDef->name;
                }
                $params = new ParamBuilder();
                $params->add("fieldName", $columnName);
                $label = $propDef->getAttribute("caption",$propDef->name);
                $params->add("fieldLabel", $label);
                
                if($propDef->hasAttribute("handleAs")){
                    $objectType=$propDef->getAttribute("handleAs");
                }
                else{
                    $objectType=$propDef->objectType;
                }
                
                
                if(is_null($propDef->getConstraint())){
                    $params->add("fieldType",$fieldTypeMap[$objectType]); 
                }
                else{
                     $constraint = $propDef->getConstraint();
                     $choices=$constraint->get();
                     $sqlConstraint=clone $constraint;
                     $count=$sqlConstraint->count();
                     
                     if($count<=$this->dropdownTermLimit){
                        $sqlConstraint->generate();
                        $values=$sqlConstraint->get();                     
                        $choices=array();

                        foreach($values as $value=>$name){
                            if($value=="SNULL"){
                                $choices[""]="null";
                            }
                            else{
                                $choices[$name]=$value; 
                            }   
                        }

                        //sort choices by label
                        uksort($choices,"strnatcasecmp");
                        $params->add("fieldType","choice");
                        $params->add("choiceType",$fieldTypeMap[$objectType]); 
                        $params->add("fieldChoices",$choices);
                     }
                     
                     else{
                         $params->add("fieldType","autocomplete");
                         $params->add("choiceType",$fieldTypeMap[$objectType]); 
                         $params->add("url","index.php?pageId={$this->pageObj->id}&resourceId={$this->pageObj->page->resourceId}&controllerId={$this->id}&actionName=autoComplete&buildPage=false");
                         
                         if($sqlConstraint->addEmptyOption){
                             $params->add("canBeNull",true);
                         }
                         else{
                              $params->add("canBeNull",false);
                         }
                     }
                }
                
                $searchField = new SearchField($params->getParams());
                $this->addSearchField($searchField);
            }
        }
    }
    
    public function buildSearchQuery($request){
        
        global $app;
        
        $formDataJSON=$app->activeSession->getVarNoStrip("POST","value");
                
        $app->userStorage->store("{$this->pageObj->id}_{$this->id}_search",$formDataJSON);
        
        $formData=json_decode($formDataJSON,$associative=true);        
        $searchData=$formData["{$this->id}_search"];
        
        $whereStatement=$this->buildWhereStatement($searchData);
        
        $app->sessionStorage->store("{$this->pageObj->id}_{$this->dataTableName}_where",$whereStatement);
        $app->userStorage->store("{$this->pageObj->id}_{$this->dataTableName}_where",$whereStatement);
        
        $results=array();
        $results["Success"]=true;
        
        echo json_encode($results);
        
    }
    
    /**
     *  Takes an array of searchData in the form: [$columnName,$operator,$value1,$value2]
     *  and builds a where statement based on the operator. The number of values passed
     *  in depends on the operator type.
     * 
     *  There are four types of operators:
     *      - standard comparison operator
     *      - like operators
     *      - between operators
     *      - null operators
     * 
     *  The search operator values that go into each type are determined by a dictionary
     *  for each type that contains a mapping from searchOperators to SQL operators. All operator
     *  names in these maps are all in lowercase.
     * 
     *  The columnName, the operator (after mapping) and the values are used to create a SQLWhereClause class
     *  which is used by other functions to generate SQL statements.
     * 
     *  All values initially come in as strings, they are trimmed and then translated into SQL
     *  by the convertValueToSQL function. Fields where any of the values are blank are skipped.
     * 
     *  The transformation of values are mostly handled by the convertValueTOSQL function except for the following:
     *      - 'contains': $value->'%$value%' 
     *      - 'starts with': $value->'$value%'
     *      - 'ends with': $value->'%$value'
     */
    public function buildWhereStatement($searchData){
        
        $comparisonOperatorLookup=array(
            "equals"=>"=",
            "not equals"=>"!=",
            "lessthan"=>"<",
            "lessthanorequal"=>"<=",
            "greaterthan"=>">",
            "greaterthanorequal"=>">="
        );
        
        $likeOperatorLookup=array(
            "contains"=>"like",
            "starts with"=>"like",
            "ends with"=>"like"
        );
        
        $betweenOperatorLookup=array(
          "between"  => "between",
          "not between"=>"not between"
            
        );
        
        $nullOperatorLookup=array(
            "is blank"=>"is null",
            "is not blank"=>"is not null"
        );
        
        $whereStatement=new SQLWhere();
        
        foreach($searchData as $searchParam){
            
            if(isset($searchParam[2])){
                $value=$searchParam[2];
                if(is_null($value) || trim($value)==""){
                    continue;
                }
            }
            
            // check to make sure both values with the between operator have been filled out
            
            if(isset($searchParam[3])){
                $value2=$searchParam[3];
                if(is_null($value2) || trim($value2)==""){
                    continue;
                }
            }
            
            $propertyName=$searchParam[0];
            $columnName=$searchParam[0];
            
            $operator=strtolower($searchParam[1]);
            
            $columnName=$this->convertColumnName($columnName);
            
            if(array_key_exists($operator,$comparisonOperatorLookup)){
                if(!isset($searchParam[2])){
                    continue;
                }
                $value=$this->convertValueToSQL($propertyName,$searchParam[2]);
                $SQLOperator=$comparisonOperatorLookup[$operator];               
                $clause=new SQLComparison($columnName,$SQLOperator,$value);
                $whereStatement->addClause($clause);
            }
            else if(array_key_exists($operator,$likeOperatorLookup)){
                $SQLoperator="like";
                
                if($operator=="contains"){
                    $value="'%".$searchParam[2]."%'";
                }
                else if($operator=="starts with"){
                    $value="'".$searchParam[2]."%'";
                }
                else if ($operator=="ends with"){
                    $value="'%".$searchParam[2]."'";
                }
                else{
                    throw new Exception("'{$operator}' is an unhandled like operator");
                }
                
                $clause=new SQLComparison($columnName,$SQLoperator,$value);
                $whereStatement->addClause($clause);
                
            }
            else if(array_key_exists($operator,$betweenOperatorLookup)){
                if(!isset($searchParam[2]) || !isset($searchParam[3])){
                    continue;
                }
                $min=$this->convertValueToSQL($propertyName,$searchParam[2]);
                $max=$this->convertValueToSQL($propertyName,$searchParam[3]);
                
                if($operator=="between"){
                    $clause=new SQLBetween($columnName,$min,$max);
                }
                else if($operator=="not between"){
                    $clause=new SQLNotBetween($columnName,$min,$max);
                }
                $whereStatement->addClause($clause);
            }
            else if($operator=="is blank"){
                $clause=new SQLIsNull($columnName);
                $whereStatement->addClause($clause);
            }
            else if($operator=="is not blank"){
                $clause=new SQLIsNotNull($columnName);
                $whereStatement->addClause($clause);
            }
            else{
                throw new Exception("'{$operator}' is a unhandled operator");
            }
            
        }
        
        return $whereStatement;   
    }
    /*
     *  Converts values taken from search fields in SQL values.
     *  The following transformation happen based on the field type:
     *      - string: $value -> '$value'
     *      - numeric: If the passed in value is numeric nothing happens otherwise treat the value like a string type.
     *      - boolean: "true" -> 1, "false" -> 0
     *      - date: $value -> cast('$value' as date)
     *      - datetime: convert UTC time to eastern standard time and cast as datetime
     *      - autocomplete: substitute value based on the constraint,then convert the value based on choice type. (constraints are maps from realValue=>displayValue)
     *                      If the value is not found in the constaint than continue the conversion without performing the
     *                      substitution and treat the value as a string.
     *          
     *      - choice: Convert the value based on the choice type
     * 
     *      - other: no conversion takes place
     * 
     *  Errors:
     *      - Throws an exception when an autocomplete field does not have a propDef defined.
     */
    public function convertValueToSQL($propName,$value){
        $field=$this->searchFields[$propName];
        
        $valueType=$field->fieldType;
        
        if($valueType=="choice"){
            $valueType=$field->choiceType;
        }
        
        // perform lookup of autocomplete values.
        if($valueType=="autocomplete"){
            
            $valueType=$field->choiceType;
            
            if(strstr($propName, ".")){
                $propNameArray=explode(".",$propName);
                $propName=$propNameArray[count($propNameArray)-1];
            }
            
            $propDef=$this->objDef->getProperty($propName);
            
            $oldValue=$value;
            if(!is_null($propDef)){
                $constraint=$propDef->getConstraint();
                $constraint->generate();
                $lookup=$constraint->get();
                $value=array_search($value,$lookup);
            }
            else{
                throw new Exception("'{$propName}' does not have a propDef");
            }
            
            if($value===false){
                $value=$oldValue;
                $valueType="string";
            }

        }
        
        if($valueType=="string"){
            $value="'$value'";
        }
        else if($valueType=="boolean"){
            if($value=="true"){
                $value=1;
            }
            else if ($value=="false"){
                $value=0;
            }
        }
        
        if($valueType=="numeric"){
            // if the user wrongly enters a non numeric type
            if(!is_numeric($value)){
                $value="'$value'";
            }
        }
        
        else if($valueType=="date"){
            $value="cast('$value' as date)";
        }
        else if($valueType=="datetime"){
            $UTC = new DateTimeZone("UTC");
            $newTZ = new DateTimeZone("America/New_York");
            $date = new DateTime( $value, $UTC );
            $date->setTimezone( $newTZ );
            $value=$date->format('Y-m-d G:i:s');

            $value="cast('$value' as datetime)";
        }
        return $value;
    }
    
    public function convertColumnName($columnName){
        if(isset($this->objDef)){
            $colNameList=explode(".",$columnName);
            $propName=$colNameList[count($colNameList)-1];
            $propDef=$this->objDef->getProperty($propName);
            $handleAs=$propDef->getAttribute("handleAs",null);

            if($handleAs){
                switch ($handleAs){
                    case "date":
                        $columnName="Date($columnName)";
                        break;
                    default:
                        throw new Exception("Handle As '{$handleAs} has not be implemented");                    
                }
            }

            return $columnName;
        }
        else{
            $field=$this->searchFields[$columnName];
            if(!is_null($field->handleAs)){
                switch ($field->handleAs){
                    case "date":
                        $columnName="Date($columnName)";
                        break;
                    default:
                        throw new Exception("Handle As '{$handleAs} has not be implemented");                    
                }
                return $columnName;
            }else
            {
                return $columnName;
            }
        }
    }
    
}

?>