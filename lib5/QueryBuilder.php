<?php
class QueryBuilder
{
    public $params; // contains the params that will be bound to values
    public $statement;
        
    public function __construct()
    {
        $this->params = array();  // array of BindParam classes

        $this->delim = "%"; // the delimiter.  This clashes with SQL like delimiter,
                            // but the params array solve this issue, by only
                            // replacing exact matches.

        $this->variableRegex="/\%([a-z]|[A-Z]|[_])+\%/";  // kept for historical value
        
        $this->statement = "";
    }

    public function setStatement($statement,$resetParams=true)
    {
        //global $log;
        //$log->info("setStatement",$statement);

        if($resetParams)
        {
            $this->params = array(); // clear the params when the statement is set
        }
        
        $this->statement=$statement;
    }

    public function resetParams()
    {
        $this->params=array();
    }

     
    public function addParam($param) {
        // no error checking, for now
        $this->params[$param->key]= $param;
    }

    public function createParam($key, $dataType, $value=null) {
        $param = new BindParameter();
        $param->key = $key; // the original mixed case name, not the lower cased one
        $param->dataType = $dataType;
        $param->value = $value;

        $this->params[$param->key]= $param;
    }

    public function setParamValue($key, $value) {

        if (array_key_exists($key, $this->params)) {
            $this->params[$key]->value = $value;
        }
        else
        {
            throw new Exception("setParamValue: The parameter:{$key} does not exist");
        }
    }
    
    public function convertToSQL($name, $type, $value) {
        
    }
    
    /*
     * This function should be a seperate utility function to convert PHP values to SQL values.
     * This method is kept in this class to avoid breaking any of the existing code.
     */
    public function convertParam($key, $paramValue,$paramType) {
        global $log;
        
        $subValue=null;

        if (is_blank($paramValue))  {
            $paramValue=null;
        }                

        if ($paramType=="string") {
            if (is_null($paramValue)) {
                $subValue="NULL";
            } else if (is_a($paramValue, "EmptyValue")) {
                $subValue="NULL";
            } else {
                $value = mres($paramValue);
                //echo "value={$value}<br/>";
                $subValue="'{$value}'";
            }
        } else if ($paramType=="function") {
            $subValue=$paramValue;
        } else if (($paramType=="date") || ($paramType=="datetime")) {
            if (is_null($paramValue)) {
                $subValue="NULL";
            } else {                    
                if ($paramValue=="now()") {
                    $subValue="now()";
                } else {                        
                    if (is_array($paramValue)) {
                        //print 'here';                            
                    } else {
                        $value = mysql_real_escape_string($paramValue);
                        //echo "value={$value}<br/>";
                        if($paramType=="date")
                        {
                            // make sure the date is in the right format.
                            $value=date('Y-m-d',strtotime($value));
                            $subValue="'{$value}'";
                        }
                        else if ($paramType=="datetime"){
                            // make sure the datetime is in the right format
                            $value=date('Y-m-d G:i:s',strtotime($value));
                            $subValue="'{$value}'";
                        }
                    }
                }
            }
        } else if ($paramType=="integer") {
            if (is_null($paramValue)) {
               $subValue="NULL";
            } else {
               $value= filter_var($paramValue, FILTER_SANITIZE_NUMBER_INT);
               $subValue="{$value}";
            }
        } else if ($paramType=="boolean") {
            if (is_null($paramValue)) {
                $subValue="NULL";
            } else {
                $value= filter_var($paramValue, FILTER_VALIDATE_BOOLEAN);
                if ($value)
                    $value = 1;
                else
                    $value = 0;
                $subValue="{$value}";
            }
        } else if ($paramType=="float") {
            if (is_null($paramValue)) {
                $subValue="NULL";
            } else {
                $value= filter_var($paramValue, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
                $subValue="{$value}";
            }
        } else if ($paramType=="double") {
            if (is_null($paramValue)) {
                $subValue="NULL";
            } else {
                $value= filter_var($paramValue, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
                $subValue="{$paramValue}";
            }
        } else if ($paramType=="null") {
            $subValue="NULL";
        } else if($paramType=="column") {
             $value = mysql_real_escape_string($paramValue);
             $subValue="`{$value}`";
        } else if($paramType=="SQLColumn") {
             $subValue="`" . $paramValue->tableAlias . "`.`" . $paramValue->columnName ."`";
        } else {
            $msg="{$paramType} is Invalid Parameter Type";
            $log->error("QueryBuilder->bindParams",$msg);
            throw(new Exception($msg));
        }
        return $subValue;                    
    }
    
    public function bindParams()
    {
        // make copy of statement before any substitutions have been made
        $newString = $this->statement;

        foreach($this->params as $key=>$param) {
            $paramValue=$param->value;
            $paramType=$param->dataType;

            $lookfor = $this->delim . $key . $this->delim;

            // position is not the same after substituion occurs so we must use the newstring to get the index
            // this could be a problem if a subtituted value matches a variable name.

            $start = strpos($newString, $lookfor);

            if($start===false)
            {
                throw new Exception("bindParams: The variable '$lookfor' was not found in the statement.");
            }
        
            $subValue = $this->convertParam($key, $paramValue,$paramType);
            
            $newString=substr_replace($newString, $subValue, $start, strlen($lookfor));
        }

        return $newString;
    }

    public function getParameterStrings()
    {
        $parameterStrings=array();
        preg_match_all($this->variableRegex,$this->statement,$parameterStrings);
        $this->parameterStrings=$parameterStrings[0];
    }
}
?>
