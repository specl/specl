<?php
/**
 * Description of Execution
 *
 * @author Tony
 */

class Execution {

    private $taskQueue;

    public function  __construct() {
        $this->taskQueue = new SplQueue();
    }

    public function addTask($taskObj) {
        $this->taskQueue->push($taskObj);
    }

    public function execute() {

        while($this->taskQueue->count()>0) {
            $task = $this->taskQueue->pop();
            $results = $task->execute();
            
            if ($results==true) {
                echo "task [$task->name} success\n";
            } else {
                echo "task [$task->name} failed";
            }

        }
    }
}
?>
