<?php
/** @ObjDefAnn(objDefId="SQLWhere")       
 ** @AuthorAnn("Steven Marks")
 ** @AuthorAnn("Anthony Leotta")
 ** @CommentAnn("We are not rewriting SQL here, so only a subset of where clauses
are supported. In SQL a where clause can be any set of expressions
that evaluate to true or false.

All where clauses contain a single operator.

the SQLWhereClause classes must be used by validate the proper
variable values are being passed to raise sensible errors for
the user.
") 
 *       
 */

// TODO: SQLIn SQLNotIn

class SQLWhere {
    
    /** @PropDefAnn(propId="Clauses",propType="collection",objectType="SQLWhereClause")       
     ** @DescriptionAnn("List of Where Clauses");
     */
    public $clauses;    
        
    /** @PropDefAnn(propId="Tables",propType="collection",objectType="string")          
     ** @DescriptionAnn("List of tables");
     */
    public $tables;
    
    public function __construct() {
        $this->clauses = array();
        $this->tables = array();        
    }

    // $rightHS and $leftHS can be one of:
    //  SQLColumn
    //  scalar value
    //  a function
    //  a nested SQL query (sub where)
    //  a list of values  ('rob','bob','job')
    //  
    // operator can be:
    //  =
    //  !=
    //  >=
    //  <=
    //  in
    //  like
    //  IS NULL, then the $rightHS is not used.
    //  IS NOT NULL, then the $rightHS is not used.
    //  between min AND max, then $rightHS has to be an array of exactly two value
    
    public function addClause($SQLWhereClause,$conjunction="AND") {
        
        
        // is this already added???
        
        
        if(is_a($SQLWhereClause,"SQLWhereClause")){
            $SQLWhereClause->conjunction=$conjunction;
            $this->clauses[]=$SQLWhereClause;
        }
    }
    
    public function addTable($tableName) {        
       $this->tables[]=$tableName;
    }
        
    public function generateTables(){
        $SQL="";
        
        $sep="";
        foreach($this->tables as $table){
            $SQL.=$sep . " {$table} ";
            if ($sep=="") { $sep = ","; }        
        }
        
        return $SQL;
    }
    
    public function generateWhereClause(){
        $SQL="";
        
        foreach($this->clauses as $clause){
  
            $whereClause = $clause->generateSQLClause();
                    
            if (!isnull($whereClause)) {
                
                if($SQL!=""){
                    $SQL.=" {$clause->conjunction} ";
                }                
                
                $SQL.=$clause->generateSQLClause()."\n";
            }
        }
        
        return $SQL;
    }
    
}

?>
