(function() {
    
    CustomWidgets.myDTDargs={};
    
    CustomWidgets.DragDropRow=function(id,datatable, sGroup, config){
        this.datatable=datatable;
            CustomWidgets.DragDropRow.superclass.constructor.call(this,id, sGroup, config);
        }
        
    YAHOO.lang.extend(CustomWidgets.DragDropRow,YAHOO.util.DDProxy,{
        srcEl:null,
        proxyEl:null,
        
        startDrag:function(){
            this.proxyEl = this.getDragEl()
            this.srcEl = this.getEl();
            
            this.datatable.selectRow(this.srcEl);
            // Make the proxy look like the source element
           // YAHOO.util.Dom.setStyle(srcEl, "visibility", "hidden");
            this.proxyEl.innerHTML = "<table><tbody>"+this.srcEl.innerHTML+"</tbody></table>";
        },
        
        onDragDrop: function(e, id) {
            var destDD = YAHOO.util.DragDropMgr.getDDById(id);
            // Only if dropping on a valid target
            if(destDD && destDD.isTarget && this.srcEl) {
                    var srcIndex = this.datatable.getRecordIndex(this.srcEl),
                    destEl = YAHOO.util.Dom.get(id),
                    destIndex =this.datatable.getRecordIndex(destEl),
                    srcData = this.datatable.getRecord(this.srcEl).getData();

                // Cleanup existing Drag instance
                CustomWidgets.myDTDargs[this.srcEl.id].unreg();
                delete CustomWidgets.myDTDargs[this.srcEl.id];

                // Move the row to its new position
                this.datatable.deleteRow(srcIndex);
                this.datatable.addRow(srcData, destIndex);
                YAHOO.util.DragDropMgr.refreshCache();
                
                this.srcEl = null;       
            }
            
        },
        
        onDragOver:function(e,id){
            this.datatable.highlightRow(id);
        },
        
        onDragOut: function(e,id){
            this.datatable.unhighlightRow(id)
        },
        
        onDragEnd: function(e,id){
            this.datatable.unselectRow(this.srcEl);
        }   
    });
    
    CustomWidgets.onDataTableRender=function(e){
        var i, id,
            allRows = this.getTbodyEl().rows;

        for(i=0; i<allRows.length; i++) {
            id = allRows[i].id;
            // Clean up any existing Drag instances
            if (CustomWidgets.myDTDargs[id]) {
                 CustomWidgets.myDTDargs[id].unreg();
                 delete CustomWidgets.myDTDargs[id];
            }
            // Create a Drag instance for each row
            CustomWidgets.myDTDargs[id] = new CustomWidgets.DragDropRow(id,this);
        }
    };
    
    CustomWidgets.onDataTableAdd=function(e){
        var id = e.record.getId();
        CustomWidgets.myDTDargs[id] = new CustomWidgets.DragDropRow(id,this);
    };
    
       
})()