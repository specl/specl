(function(){
    
    var operator= function(key,value,inputNumber){
        this.key=key;
        this.value=value;
        this.inputNumber=YAHOO.lang.isUndefined(inputNumber)? 1 : inputNumber;
    }
    
    inputEx.SearchField=function(options){
        
        // a key value pair containing all of the operations, must be unique based
        // on key displayed.
        this.operators={}
        this.buildDefaultOperators();
        
        // contains a map of the types to all of their possible operations
        this.typeOperations={}
        this.buildDefaultTypes();
        
        // maps data types to inputex field types;
        this.typeInputs={};
        this.buildDefaultInputTypes();
        
        
        this.searchFields={};
        
        inputEx.SearchField.superclass.constructor.call(this,options);
        
        
        
    };
    
    YAHOO.lang.extend(inputEx.SearchField,inputEx.CombineField,{
        
        initEvents:function(){
            inputEx.SearchField.superclass.initEvents.call(this);
            
            this.inputs[0].updatedEvt.subscribe(this.onSelectFieldChange,this,true);
            this.inputs[1].updatedEvt.subscribe(this.onOperationFieldChange,this,true);
        },
        
        addOperator:function(key,value,inputNumber){
            
            var newOp= new operator(key,value,inputNumber);
            
            this.operators[key]=newOp;
            
        },
        
        addType: function(typeName)
        {
            this.typeOperations[typeName]=[];
        },
        
        addOperatorToType: function(typeName,operatorName)
        {
            var operator=this.operators[operatorName];
            this.typeOperations[typeName].push(operator);
        },
        
        
        buildDefaultOperators: function()
        {
            this.addOperator("Equals","=");
            this.addOperator("Not Equals","!=");
            this.addOperator("Contains","Contains");
            this.addOperator("Starts With","StartsWith");
            this.addOperator("Ends With","EndsWith");
            
            this.addOperator("LessThan","<");
            this.addOperator("LessThanOrEqual","<=");
            this.addOperator("GreaterThan",">");
            this.addOperator("GreaterThanOrEqual",">=");
            
            this.addOperator("Between","Between",2);
            this.addOperator("is Blank","is Blank",0);
            this.addOperator("is not Blank","is not Blank",0);
        },
        
        buildDefaultTypes: function()
        {
            this.addType("string");
            this.addOperatorToType("string","Equals");
            this.addOperatorToType("string","Not Equals");
            this.addOperatorToType("string","Contains");
            this.addOperatorToType("string","Starts With");
            this.addOperatorToType("string","Ends With");
            this.addOperatorToType("string","is Blank");
            this.addOperatorToType("string","is not Blank");
            
            this.addType("numeric");
            this.addOperatorToType("numeric","Equals");
            this.addOperatorToType("numeric","Not Equals");
            this.addOperatorToType("numeric","LessThan");
            this.addOperatorToType("numeric","LessThanOrEqual");
            this.addOperatorToType("numeric","GreaterThan");
            this.addOperatorToType("numeric","GreaterThanOrEqual");
            this.addOperatorToType("numeric","Between");
            this.addOperatorToType("numeric","is Blank");
            this.addOperatorToType("numeric","is not Blank");
            
            
            this.addType("date");
            this.addOperatorToType("date","Equals");
            this.addOperatorToType("date","Not Equals");
            this.addOperatorToType("date","LessThan");
            this.addOperatorToType("date","LessThanOrEqual");
            this.addOperatorToType("date","GreaterThan");
            this.addOperatorToType("date","GreaterThanOrEqual");
            this.addOperatorToType("date","Between");
            this.addOperatorToType("date","is Blank");
            this.addOperatorToType("date","is not Blank");
            
            this.addType("datetime");
            this.addOperatorToType("datetime","Equals");
            this.addOperatorToType("datetime","Not Equals");
            this.addOperatorToType("datetime","LessThan");
            this.addOperatorToType("datetime","LessThanOrEqual");
            this.addOperatorToType("datetime","GreaterThan");
            this.addOperatorToType("datetime","GreaterThanOrEqual");
            this.addOperatorToType("datetime","Between");
            this.addOperatorToType("datetime","is Blank");
            this.addOperatorToType("datetime","is not Blank");


            this.addType("boolean");
            this.addOperatorToType("boolean","Equals");
            this.addOperatorToType("boolean","Not Equals");
            this.addOperatorToType("boolean","is Blank");
            this.addOperatorToType("boolean","is not Blank");

        },
        
        buildDefaultInputTypes: function(){
          
          this.setTypeInput("string",{type:"string"});
          this.setTypeInput("numeric",{type:"string"});
          this.setTypeInput("date",{type:"datepicker",valueFormat:"Y-m-d"});
          this.setTypeInput("datetime",{type:"datetime",valueFormat:"Y-m-d"}); // G:i:s
          this.setTypeInput("boolean",{type:'select',choices:["true","false"]});          
          
        },
        
        // builds the key select field inputEx config.
        buildKeyFieldConfig:function(){
          
            var choices=[],key;
            
            for(key in this.searchFields){
                var fld = this.searchFields[key];
                choices.push({'value':key,'label':fld.label})
            }
            
            return{type:'select',choices:choices};
                
        },
        
        buildOperationFieldConfig: function(fieldName){
            
            var fieldType=this.searchFields[fieldName].type;
            
            var operators= this.typeOperations[fieldType];
            
            var choices=[],key,operator;
            
            
            for(var i=0;i<operators.length;i++){
                operator= operators[i];
                
                choices.push({'value':operator.key, 'label':operator.key});
            }
            
            return {type:'select',choices:choices};
           
        },
        
        buildChoiceType: function(type, choices) {
            
            this.addType(type);
            this.addOperatorToType(type,"Equals");
            this.addOperatorToType(type,"Not Equals");
            
            var canBeNull=false;
            
            var newChoices=[];
            
            for(var i=0;i<choices.length;i++){
                if(choices[i].value!="null"){
                    newChoices.push(choices[i])
                }
                else{
                    canBeNull=true;
                }
            }
            
            if(canBeNull){
                this.addOperatorToType(type,"is Blank");
                this.addOperatorToType(type,"is not Blank");
            }

            this.setTypeInput(type,{'type':'select','choices':newChoices});     
                
        },
        
        buildAutoCompleteType:function(type,url,canBeNull,name){
            this.addType(type);
            this.addOperatorToType(type,"Equals");
            this.addOperatorToType(type,"Not Equals");
            var name=name.split(".");
            name=name[name.length-1];
            if(canBeNull){
                this.addOperatorToType(type,"is Blank");
                this.addOperatorToType(type,"is not Blank");
            }
            
            var datasource=new YAHOO.util.DataSource(url,{
                responseType:YAHOO.util.XHRDataSource.TYPE_JSON,
                responseSchema:{resultsList:'data', fields:['value']},
                maxCacheEntries:500
            });
            
            var autoConfig={"forceSelection":true,
                            "typeAhead":false,
                            "minQueryLength":1,
                            "maxResultsDisplayed":20,
                            "generateRequest":function(sQuery) { return "&fieldId=" + name + "&query=" +sQuery;},
                            "formatResult":function(oResultItem, sQuery){
                                return oResultItem[0];
                            }
            }
            
            var fieldConfig={'type':'autocomplete',
                             'datasource':datasource,
                             'autoComp': autoConfig
                            }
            
            this.setTypeInput(type,fieldConfig);
        },
        
        setOptions:function(options){
            
            var i;
            var choices;
            
            // check the option to render conjunction drop down
            
            // create a drop down field with a choice of AND or OR
            
            // add the field to the field list
            
            // iterate through each search column based on the searchFields passed in.
            for(i=0;i<options.searchFields.length;i++){                                
                var name=options.searchFields[i].name;
                var label=options.searchFields[i].label;
                var type=options.searchFields[i].type;
                
                // build the drop down and autocomplete fields for each search column on the fly
                if(type=="choice"){
                    choices=options.searchFields[i].choices;
                    type=name+"_choice";
                    this.buildChoiceType(type, choices);
                    this.searchFields[name]={"name":name,"label":label,"type":type};
                }
                else if(type=="autocomplete"){
                    var url=options.searchFields[i].url;
                    var canBeNull=options.searchFields[i].canBeNull;
                    type=name+"_autocomplete";
                    this.buildAutoCompleteType(type,url,canBeNull,name);
                    this.searchFields[name]={"name":name,"label":label,"type":type};
                }
                else {
                    this.searchFields[name]={"name":name,"label":label,"type":type};
                } 
            }                
            
            // build drop down list of column names
            var selectKeyField=this.buildKeyFieldConfig()
            
            // build drop down for the operations based on the field type
            var selectOperationField=this.buildOperationFieldConfig(options.searchFields[0].name);
            
            // combine the two fields into a list
            var fieldList=[selectKeyField,selectOperationField];
            
            // each autocomplete and drop down menu need a seperate type for each column, because the choices or url
            // is different for each column.
            var fieldType=options.searchFields[0].type;
            var fieldName;
            
            // save the selected field info for reference later
            this.currentField=options.searchFields[0];
            
            if (fieldType=="choice") {
                fieldName=options.searchFields[0].name;                
                fieldType=fieldName+"_choice";    
            }
            else if(fieldType=="autocomplete"){
                fieldName=options.searchFields[0].name;
                fieldType=fieldName+"_autocomplete";
            }
            
            // based on the operator, get the input type and build the correct input field
            var operators=this.typeOperations[fieldType];
            var inputFieldConfig=this.typeInputs[fieldType];
            var operator=operators[0];
            
            // save the current operator in a class variable for use later
            this.currentOperator=operator;
            
            // add the input to the field list
            for(i=0;i<operator.inputNumber;i++){
                fieldList.push(inputFieldConfig);
            }

            // pass the array of fields into the CombineField class, which handles the basic rendering of the fields
            var newOptions={
                fields:fieldList
            };
            
            YAHOO.lang.augmentObject(newOptions,options)
            
            inputEx.SearchField.superclass.setOptions.call(this,newOptions);
        },
        
        
        // changes operators and input when field name is chosen.
        onSelectFieldChange:function(e,params){
            var key=params[0];
            var i;  
            
            
            var currentField=this.searchFields[key];
            var oldField=this.currentField
            this.currentField=currentField;
            
            // compare the type of the old field and new field, if the type is the same don't change anything
            if(oldField.type===currentField.type){
                return;
            }
            
            // if the types are different, change the operation and input fields
            
            // first remove the operation and input fields, this must be done first
            // because inputEx appends new fields to the end of this.inputs as they are rendered
            // which means the fields will appear in the order in which they are added.
            // This must be done first because input ex appends new fields to the 
            // end of this.inputs when they are rendered

            // check option to see if conjuction field is rendered, if so keep the first two fields
            // else only remove the first field.
            for(i=1;i<this.inputs.length;i++){
                var currentField=this.inputs[i];
                currentField.destroy();
            } 
            
            // all of the 1, in reference to the node position to add to next, needs to the changed to 2 if
            // a conjunction field is present.
            this.inputs.splice(1,this.inputs.length);
            var next=this.divEl.childNodes[1];
            
            // add operation select field to DOM and add listener
            var fieldConfig=this.buildOperationFieldConfig(key)
            var operatorField= this.renderField(fieldConfig);
            var operatorFieldEl=operatorField.getEl();
            YAHOO.util.Dom.setStyle(operatorFieldEl, 'display', 'inline-block');
            this.divEl.insertBefore(operatorFieldEl, next);
            this.inputs[1].updatedEvt.subscribe(this.onOperationFieldChange,this,true);
            
            
            // add input boxes            
            
            // get operator
            var operatorName=fieldConfig.choices[0].label;
            var operator=this.operators[operatorName];
            this.currentOperator=operator;
            
            // get input field configs
            var fieldType= this.searchFields[key].type;
            var inputFieldConfig=this.typeInputs[fieldType]
            var lastInput,inputField,inputFieldEl;
            
            for(i=0;i<operator.inputNumber;i++){
                lastInput=this.inputs[this.inputs.length-1];
                next = this.divEl.childNodes[inputEx.indexOf(lastInput.getEl(), this.divEl.childNodes)+1];
                inputField = this.renderField(inputFieldConfig);
                inputFieldEl = inputField.getEl();
                YAHOO.util.Dom.setStyle(inputFieldEl, 'display', 'inline-block');
                this.divEl.insertBefore(inputFieldEl, next);
                
            }
        
        },
        
        setTypeInput:function(typeName,fieldConfig){
            this.typeInputs[typeName]=fieldConfig;
        },
        
        
        // change input box based on operator chosen
        onOperationFieldChange:function(e,params){
            var operatorName=params[0];
            var i,currentField;
            var currentOperator=this.operators[operatorName];
            var oldOperator=this.currentOperator;
            this.currentOperator=currentOperator;
            
            // Changing the operator does not change the type of the field, the only difference is the number of input fields.
            // If the number of input fields of the old field are the same as the new field don't change anything.            
            if(oldOperator.inputNumber===currentOperator.inputNumber){
                return;
            }
            
            
            // If the number of input boxes is different remove the existing input box and add new ones, if required.
            
            
            // All references to 2, the position where to add the next node, need to be changed to 3
            // if a conjunctionNode exists.
            
            // remove all input fields first, 
            // This must be done first because input ex appends new fields to the 
            // end of this.inputs when they are rendered
            for(i=2;i<this.inputs.length;i++){
                currentField=this.inputs[i];
                currentField.destroy();
            } 
            this.inputs.splice(2,this.inputs.length);
            
            
            var key=this.inputs[0].getValue();
            var fieldType= this.searchFields[key].type;
            var inputFieldConfig=this.typeInputs[fieldType];
            var lastInput,inputField,inputFieldEl,next;            
            for(i=0;i<currentOperator.inputNumber;i++){
                lastInput=this.inputs[this.inputs.length-1];
                next = this.divEl.childNodes[inputEx.indexOf(lastInput.getEl(), this.divEl.childNodes)+1];
                inputField = this.renderField(inputFieldConfig);
                inputFieldEl = inputField.getEl();
                YAHOO.util.Dom.setStyle(inputFieldEl, 'display', 'inline-block');
                this.divEl.insertBefore(inputFieldEl, next);
                
            }    
        },
        
        setValue: function(values) {
            if(!values) {
            return;
            }
            
            // set values needs to be changed to check for the presence of a conjunction, and set the value if it exists.
            
            this.inputs[0].setValue(values[0],false);
            this.onSelectFieldChange(null,[values[0]]);
            this.inputs[1].setValue(values[1],false);
            this.onOperationFieldChange(null,[values[1]]);
            var fieldType= this.searchFields[values[0]].type;
            
            var i, n=this.inputs.length;
            for (i = 2 ; i < n ; i++) {
              if(fieldType=="data"){
                  // parse a date in yyyy-mm-dd format
                  var parts = input.match(/(\d+)/g);
                  // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
                  values[i]= new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
              }                
              else if(fieldType=="datetime"){
                  values[i]=new Date(values[i]);
              }
              this.inputs[i].setValue(values[i], false);
            }
        }
    });
    
    inputEx.registerType("search",inputEx.SearchField,{});
    
})();