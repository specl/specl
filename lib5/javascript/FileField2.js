(function(){

    inputEx.FileField2=function(options){
        inputEx.FileField2.superclass.constructor.call(this,options);
    };
    
    YAHOO.lang.extend(inputEx.FileField2,inputEx.FileField,{
       
       getValue:function(){
           return this.el.value;
       }
       
    });
    
    inputEx.registerType("file2", inputEx.FileField2,{});
})();