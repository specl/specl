(function(){
    
    inputEx.ButtonField=function(options){
        inputEx.ButtonField.superclass.constructor.call(this,options);
        
    }
    
    YAHOO.lang.extend(inputEx.ButtonField,inputEx.Field,{
        
        setOptions:function(options){
            inputEx.ButtonField.superclass.setOptions.call(this,options);
            this.options.buttonText=options.buttonText;
            this.callbackFunction=options.callbackFunction;
        },
        
        renderComponent:function(){
            this.el=document.createElement("button");
            this.el.innerHTML=this.options.buttonText;
            this.el.setAttribute("type","button");
            
            this.fieldContainer.appendChild(this.el);
        },
        
        initEvents:function(){
            if(this.callbackFunction){
                YAHOO.util.Event.addListener(this.el,"click",this.callbackFunction)
            }
        }
        
    });
    
    inputEx.registerType("button",inputEx.ButtonField);
    
})();