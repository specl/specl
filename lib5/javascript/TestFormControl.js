var formControl;
var PageDispatcher=function(){

};
    
PageDispatcher.prototype=new CentralDispatcher()

var submitData=function(message){
    if(message.elementName==="SubmitButton"){
        var data=formControl.getData();
        var JSONData=JSON.stringify(data,null,4)
        alert("Submit Clicked Data Recieved:\n" + JSONData);
    }        
}

var cancel=function(message){
    if(message.elementName==="CancelButton"){
        alert("Cancel Clicked");
    }
}

PageDispatcher.prototype.initEvents=function(){
    this.addEvent({
        eventName:'buttonClicked',
        requiredParameters:['elementName'],
        rules:{
            "submit":submitData,
            "cancel":cancel
        }
    })
}

var DispatchObj=new PageDispatcher();
DispatchObj.initEvents();

(function(){
    YAHOO.util.Event.onDOMReady(function(){
        formControl=new Zephyr.FormControl({"name":"TestForm"});
        var textLabel=new Zephyr.LabelControl({"name":"TextLabel1","value":"Test Label","size":"10"});
        formControl.addControl(textLabel);
        var textEntry=new Zephyr.TextControl({"name":"TextControl1","value":"this is a test"});
        formControl.addControl(textEntry);   
        formControl.addNewLine();
        var textLabel2=new Zephyr.LabelControl({"name":"TextLabel12","value":"Test Label2","size":"10"});
        formControl.addControl(textLabel2);
        var textEntry2=new Zephyr.TextControl({"name":"TextControl2","value":"testing"});
        formControl.addControl(textEntry2);
        formControl.addNewLine();        
        var textLabel3=new Zephyr.LabelControl({"name":"AutoCompleteLabel1","value":"AutoComplete1","size":"10"});
        formControl.addControl(textLabel3);        
        var autoCompleteEntry=new Zephyr.AutoCompleteControl({"name":"AutoCompleteTest","value":"","url":"index.php"});
        formControl.addControl(autoCompleteEntry);
        formControl.addNewLine();
        var SubmitButton=new Zephyr.ButtonControl({"name":"SubmitButton","text":"Submit"});
        formControl.addControl(SubmitButton);
        var CancelButton=new Zephyr.ButtonControl({"name":"CancelButton","text":"Cancel"});
        formControl.addControl(CancelButton);
        formControl.addNewLine();
        
        
        formControl.build();
        formControl.render();
        formControl.initEvents();
    });    
})();


