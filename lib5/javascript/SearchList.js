(function(){
    
    var lang = YAHOO.lang, Dom = YAHOO.util.Dom, Event = YAHOO.util.Event;

    inputEx.SearchList=function(options){
        inputEx.SearchList.superclass.constructor.call(this,options);
    };
    
    YAHOO.lang.extend(inputEx.SearchList,inputEx.ListField,{        
        
        filterDeleted: new YAHOO.util.CustomEvent('filterDeleted', this),
        
        onDelete:function(e){
             inputEx.SearchList.superclass.onDelete.call(this,e);
             this.filterDeleted.fire()
        }
            
        
    
    
    });
    
    
    
    inputEx.registerType("SearchList", inputEx.SearchList,{});
})();

