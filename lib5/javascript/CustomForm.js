(function(){
    var util = YAHOO.util, lang = YAHOO.lang
    inputEx.CustomForm=function(options){
        this.name=options.name;
        inputEx.CustomForm.superclass.constructor.call(this,options);
    }
    
    YAHOO.lang.extend(inputEx.CustomForm,inputEx.Form,{
        clearErrorMessages:function(){
            var invalidNodes=YAHOO.util.Selector.query('.inputEx-fieldWrapper.invalid',this.name);
            var i=0;
            for(i;i<invalidNodes.length;i++)
            {
                YAHOO.util.Dom.removeClass(invalidNodes[i],'invalid');
                var selector='#' + invalidNodes[i].id + ' .error-message';
                var msgNode=YAHOO.util.Selector.query(selector,this.name,true);
                msgNode.parentNode.removeChild(msgNode);
            }
            
            var invalidNodes=YAHOO.util.Selector.query('.inputEx-CombineField.invalid',this.name);
            var i=0;
            for(i;i<invalidNodes.length;i++)
            {
                YAHOO.util.Dom.removeClass(invalidNodes[i],'invalid');
                var selector='#' + invalidNodes[i].id + ' .error-message';
                var msgNode=YAHOO.util.Selector.query(selector,this.name,true);
                msgNode.parentNode.removeChild(msgNode);
            }
            
            this.onClearErrors.fire();
        },
        
        showFieldErrorMessage:function(fieldName, errorMsg,formObj){
            if(typeof(formObj)==='undefined'){
                formObj=this;
            }
            var field=formObj.getFieldByName(fieldName);
            if(field!==null){
                // subform
                if(typeof(errorMsg)=='object'){
                    // subfield is a list of forms
                    if(typeof(field.subFields)!='undefined'){
                        this.showListErrorMessages(errorMsg,field,formObj);
                    }
                    else{
                        this.showErrorMessages(errorMsg,field);
                    }
                }
                else{      
                    YAHOO.util.Dom.addClass(field.divEl,'invalid');
                    var msgEl = inputEx.cn('div', {className: 'error-message'});
                    try{
                        var divElements = field.divEl.getElementsByTagName('div');
                        field.divEl.insertBefore(msgEl, divElements[(divElements.length-1>=0)?divElements.length-1:0]); //insertBefore the clear:both div
                    }catch(e){alert(e);}
                    msgEl.innerHTML=errorMsg;
                }
            }
        },
        
        showErrorMessages:function(errors,formObj){
            var fieldName;
            if(typeof(formObj)==='undefined'){
                formObj=this;
            }
            for(fieldName in errors)
            {
                this.showFieldErrorMessage(fieldName, errors[fieldName],formObj);
            } 
            
            this.onShowErrors.fire();
        },
        
        showListErrorMessages:function(errors,listField,formObj){
            var listIndex;
            for(listIndex in errors){
                formObj.showErrorMessages(errors[listIndex],listField.subFields[listIndex]);
            }
        },
        
        showFieldWarningMessage:function(fieldName, errorMsg){
            var field=this.getFieldByName(fieldName);
            YAHOO.util.Dom.addClass(field.divEl,'invalid-warning');
            var msgEl = inputEx.cn('div', {className: 'warning-message'});
              try{
                 var divElements = field.divEl.getElementsByTagName('div');
                 field.divEl.insertBefore(msgEl, divElements[(divElements.length-1>=0)?divElements.length-1:0]); //insertBefore the clear:both div
              }catch(e){alert(e);}
            msgEl.innerHTML=errorMsg;
        },
        
        showWarningMessages:function(errors){
            var fieldName;   
            for(fieldName in errors)
            {
                this.showFieldWarningMessage(fieldName, errors[fieldName]);
            }   
        },
        
        submitSuccess: new YAHOO.util.CustomEvent('submitSuccess',this),
        
        onShowErrors: new YAHOO.util.CustomEvent('onShowErrors',this),
        
        onClearErrors: new YAHOO.util.CustomEvent('onClearErrors',this),
        
        updateForm:function(data){
            var formCommands=data["formCommands"];
            var i=0;
            for(i;i<formCommands.length;i++){
                var command=formCommands[i];
                var commandName=command["command"];
                if(commandName==="removeField"){
                    var field=this.getFieldByName(command["fieldName"]);
                    if(field){
                        field.destroy();
                    }
                }
                else if(commandName==="addField"){
                    if(command["fieldParams"]){
                        this.addField(command["fieldParams"]);
                    }
                }
            }
            // always make sure that the field that displays the form error is last.
            var errorField=this.getFieldByName('formMessage'); 
            var errorFieldOptions=errorField.options;
            errorFieldOptions.type="uneditable";
            errorField.destroy();
            this.addField(errorFieldOptions);
        },
        
        waitDialog: new YAHOO.widget.Panel('wait',  
                                { width:'240px', 
                                fixedcenter:true, 
                                close:false, 
                                draggable:false, 
                                zindex:4,
                                modal:true,
                                visible:false
                                } 
                            ),
        
       /**
        * Send the form value in JSON through an ajax request
        */
        asyncRequest: function() {
            
            this.waitDialog.setHeader('Processing, please wait...');        
            this.waitDialog.render(document.body);
            this.waitDialog.show(document.body);

            if(this.options.ajax.showMask) { this.showMask(); }

                var formValue = this.getValue();

                // options.ajax.uri and options.ajax.method can also be functions that return a the uri/method depending of the value of the form
                var uri = lang.isFunction(this.options.ajax.uri) ? this.options.ajax.uri(formValue) : this.options.ajax.uri;
                var method = lang.isFunction(this.options.ajax.method) ? this.options.ajax.method(formValue) : this.options.ajax.method;

                var postData = null;

                // Classic application/x-www-form-urlencoded (like html forms)
                if(this.options.ajax.contentType == "application/x-www-form-urlencoded" && method != "PUT") {
                    var params = [];
                    for(var key in formValue) {
                        if(formValue.hasOwnProperty(key)) {
                            var pName = (this.options.ajax.wrapObject ? this.options.ajax.wrapObject+'[' : '')+key+(this.options.ajax.wrapObject ? ']' : '');
                            params.push( pName+"="+window.encodeURIComponent(formValue[key]));
                        }
                    }
                    postData = params.join('&');
                }
                // The only other contentType available is "application/json"
                else {
                    YAHOO.util.Connect.initHeader("Content-Type" , "application/json" , false);

                    // method PUT don't send as x-www-form-urlencoded but in JSON
                    if(method == "PUT") {
                        var formVal = this.getValue();
                        var p;
                        if(this.options.ajax.wrapObject) {
                            p = {};
                            p[this.options.ajax.wrapObject] = formVal;
                        }
                        else {
                            p = formVal;
                        }
                        postData = lang.JSON.stringify(p);
                    }
                    else {
                        // We keep this case for backward compatibility, but should not be used
                        // Used when we send in JSON in POST or GET
                        postData = "value="+window.encodeURIComponent(lang.JSON.stringify(this.getValue()));
                    }
                }

            util.Connect.asyncRequest( method, uri, {
                success: function(o) {
                    this.waitDialog.hide();
                    if(this.options.ajax.showMask) { this.hideMask(); }
                    if( lang.isFunction(this.options.ajax.callback.success) ) {
                    this.options.ajax.callback.success.call(this.options.ajax.callback.scope,o);
                    }
                },

                failure: function(o) {
                    this.waitDialog.hide();
                    if(this.options.ajax.showMask) { this.hideMask(); }
                    if( lang.isFunction(this.options.ajax.callback.failure) ) {
                    this.options.ajax.callback.failure.call(this.options.ajax.callback.scope,o);
                    }
                },

                scope:this
            }, postData);
        },
     
        /*
         *  When a value changes, if a global dispatcher is defined, send an fieldChanged event to the global dispatcher.
         */
        onChange: function(eventName, args) {                 
           if(DispatchObj!==undefined){
                var fieldValue = args[0];
                var fieldInstance = args[1];

                var eventMsg={
                  event:"fieldChanged",
                  formName:this.options.name,
                  fieldName:fieldInstance.options.name,
                  newValue:fieldValue
                };
                DispatchObj.dispatch(eventMsg)
           }         
           inputEx.CustomForm.superclass.onChange(this,eventName,args);              
        }
    });
    
})();