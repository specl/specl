(function(){
   
   inputEx.AutoComplete2=function(options){
      inputEx.AutoComplete2.superclass.constructor.call(this,options);
   }
   
   YAHOO.lang.extend(inputEx.AutoComplete2,inputEx.AutoComplete,{
        setOptions: function(options){
            if(options.url!==undefined){
                options.datasource=new YAHOO.util.DataSource(options.url,{
                    responseType:YAHOO.util.XHRDataSource.TYPE_JSON,
                    responseSchema:{resultsList:'data', fields:['value']},
                    maxCacheEntries:500
                });
                
                options.autoComp={
                    "forceSelection":true,
                    "typeAhead":false,
                    "minQueryLength":1,
                    "maxResultsDisplayed":20,
                    "generateRequest":function(sQuery) { return "&fieldId=" + options.name + "&query=" + sQuery ; },
                    "formatResult":function(oResultItem, sQuery) 
                    {                           
                        return oResultItem[0] ;
                    }
                }
            }
            inputEx.AutoComplete2.superclass.setOptions.call(this,options);
        }
   
   });
   
   inputEx.registerType("autocomplete2",inputEx.AutoComplete2,{});
   
})();