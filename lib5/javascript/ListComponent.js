(function() {
    var Dom = YAHOO.util.Dom;
    var Event = YAHOO.util.Event;
    var DDM = YAHOO.util.DragDropMgr;
    
    // define ListComponent
    CustomWidgets.ListComponent=function(parentEl,title,enableDragDrop){
        this.parentEl=Dom.get(parentEl);
        this.titleEl=document.createElement("H2");
        this.titleEl.innerHTML=title;
        this.parentEl.appendChild(this.titleEl);
        this.listEl=document.createElement("ol");
        this.listEl.className=this.parentEl.className;
        this.listEl.id=this.parentEl.id + "_ol"
        this.parentEl.appendChild(this.listEl);
        this.enableDragDrop=enableDragDrop;
        this.numberOfElements=0;
        this.listData={};
       
        if(this.enableDragDrop){
            new YAHOO.util.DDTarget(this.listEl);
        }
    }
    
    CustomWidgets.ListComponent.prototype={
        
        addListElement:function(text,dataObject,className){
            var listElement=document.createElement("li");
            listElement.innerHTML=text;
            listElement.className=className;
            
            this.numberOfElements+=1;
            listElement.id=this.listEl.id + "li" + this.numberOfElements;
            this.listEl.appendChild(listElement);
            
            this.listData[text]=dataObject;
            
            if(this.enableDragDrop){
                new CustomWidgets.DDList(listElement.id);
            }
        },
        
        clearList:function(){
            this.numberOfElements=0;
            this.listData={};
            while (this.listEl.hasChildNodes()) {
                this.listEl.removeChild(this.listEl.lastChild);
            }
        },
        
        removeElementsByClass:function(className){
          var selectorString="."+className;
          var nodesToRemove=YAHOO.util.Selector.query(selectorString,this.listEl);
          
          for(var i=0;i<nodesToRemove.length;i++){
              this.listEl.removeChild(nodesToRemove[i]);
          }
        },
        
        
        getListLength:function(){
          return this.listEl.childNodes.length;  
        },

        getListItems:function(){
            
            var parseList = function(ul, title) {
                var items = ul.getElementsByTagName("li");
                var out = title + ": ";
                
                var sep="";
                    
                for (var i=0;i<items.length;i=i+1) {
                    out += sep + items[i].innerHTML;
                    if (sep=="") { 
                        sep = "|";
                    }
                }
                return out;
            };

        
            return parseList(this.listEl, "Lanes");  
        },
        
        getListData:function(){
            var listElements=this.listEl.getElementsByTagName("li");
            var orderedData=[];
            
            for(var i=0;i<listElements.length;i++){
                orderedData.push(this.listData[listElements[i].innerHTML]);
            }
            
            return orderedData;
        }

    }
    
})();