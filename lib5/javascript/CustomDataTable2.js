(function(){
    

    
    CustomWidgets.DataTable=function(elContainer , aColumnDefs , oDataSource , oConfigs){
        
        this.listenToColumns=[];
        this._aColumnDefs=aColumnDefs;
        this._addAdditionalColumns(oConfigs);        
        this._applyFormatting(aColumnDefs);
        this.columnsWithEvents=[];
        this._addColumnEvents(aColumnDefs);
        
        //this.initialStateSaved=false;
        this.checkedRows={};
        this.whereStatement=null;
        CustomWidgets.DataTable.superclass.constructor.call(this,elContainer , this._aColumnDefs , oDataSource , oConfigs)
        this.subscribe("initEvent",this.initEvents,this,true);
        
        
    }
    
    YAHOO.lang.extend(CustomWidgets.DataTable,YAHOO.widget.DataTable,{
        
        initEvents:function(){
            if(this._showHideColumnsDlg){
                YAHOO.util.Event.addListener(this._showHideDlgNode,'click',this.displayShowHideDlg,this,true);
            }
            
            if(this._enableSelectRows){
                this.subscribe("checkboxClickEvent",this.onCheckBoxClick,scope=this);
            }

            if(this._showAddColumn){
                this.addClicked=new YAHOO.util.CustomEvent("addClicked",this);
            }

            if(this._showEditColumn){
                this.editClicked=new YAHOO.util.CustomEvent("editClicked",this);
            }
            
            if(this._showDeleteColumn){
                this.deleteClicked=new YAHOO.util.CustomEvent("deleteClicked",this);
            }
            
            if(this._showViewColumn){
                this.viewClicked=new YAHOO.util.CustomEvent("viewClicked",this);
            }
            
            if(this._showAddColumn || this._showEditColumn || this._showDeleteColumn || this._showViewColumn){
                this.subscribe("cellClickEvent",this.onCellClick,scope=this)                
            }
            
            this.subscribe("renderEvent",this.adjustView,scope=this);
            
            // save column state on the server when a change occurs
            this.subscribe("columnShowEvent",this.saveState,scope=this);
            this.subscribe("columnHideEvent",this.saveState,scope=this);
            this.subscribe("columnResizeEvent",this.saveState,scope=this);
            this.subscribe("columnReorderEvent",this.saveState,scope=this);
            this.subscribe("sortedByChange",this.saveState,scope=this);
            this.subscribe("paginatorChange",this.saveState,scope=this);
                        
        },
        
        _addColumnEvents:function (aColumnDefs){
            var i,column,eventName;
            for(i=0;i<aColumnDefs.length;i++){
                column=aColumnDefs[i];
                if(column.colType==="button"){
                    eventName=column.key + "Clicked";
                    this[eventName]=new YAHOO.util.CustomEvent(eventName,this);
                    this.columnsWithEvents.push(column.key);
                }
            }
        },
        
        initAttributes:function(oConfigs){
            oConfigs=oConfigs||{};
            
            CustomWidgets.DataTable.superclass.initAttributes.call(this,oConfigs);
            
            this.setAttributeConfig('enableSelectRows',{
                value:false,
                validator:YAHOO.lang.isBoolean,
                method:function(value){
                    this._enableSelectRows=value;
                } 
            })

            this.setAttributeConfig('showAddColumn',{
                value:false,
                validator:YAHOO.lang.isBoolean,
                method:function(value){
                    this._showAddColumn=value;
                }  
            })

            this.setAttributeConfig('showEditColumn',{
                value:false,
                validator:YAHOO.lang.isBoolean,
                method:function(value){
                    this._showEditColumn=value;
                }  
            })
            
            
            this.setAttributeConfig('showDeleteColumn',{
                value:false,
                validator:YAHOO.lang.isBoolean,
                method:function(value){
                    this._showDeleteColumn=value;
                }   
            })
            
            this.setAttributeConfig('showViewColumn',{
                value:false,
                validator:YAHOO.lang.isBoolean,
                method:function(value){
                    this._showViewColumn=value;
                }   
            })
            
            
            this.setAttributeConfig('showHideColumnsDlg',{
                value:false,
                validator:YAHOO.lang.isBoolean,
                method:function(value){
                    this._showHideColumnsDlg=value;
                    this._showHideRendered=false;
                }                
            })
            
            this.setAttributeConfig('showHideTitle',{
                value:"Show/Hide Columns",
                validator:YAHOO.lang.isString,
                method:function(value){
                    this._showHideTitle=value;
                }
            })
            
            this.setAttributeConfig("showHideCSS",{
                value:"customDataTable-showHide",
                validator:YAHOO.lang.isString,
                method:function(value){
                    this._showHideCSS=value;
                }
            })
            
            this.setAttributeConfig("keys",{
                value:{},
                validator:YAHOO.lang.isObject,
                method:function(value){
                    this.keys=value;
                }
            })
            
            this.setAttributeConfig("propNameToKeyMap",{
                value:{},
                validator:YAHOO.lang.isObject,
                method:function(value){
                    this.propNameToKeyMap=value;
                }
            })
            
            this.setAttributeConfig("keyToPropNameMap",{
                value:{},
                validator:YAHOO.lang.isObject,
                method:function(value){
                    this.keyToPropNameMap=value;
                }
            })
            
            this.setAttributeConfig("bodyId",{
                value:null,
                validator:YAHOO.lang.isString,
                method:function(value){
                    this.bodyId=value;
                }
            })
            
            this.setAttributeConfig("scrollToTopOnPageChange",{
                value:false,
                validator:YAHOO.lang.isBoolean,
                method:function(value){
                    this.scrollToTopOnPageChange=value;
                }
            })
            
            this.setAttributeConfig("topPaginatorName",{
                value:false,
                validator:YAHOO.lang.isString,
                method:function(value){
                    this.topPaginatorName=value;
                }
            })
            
            this.setAttributeConfig("selectOneMode",{
                value:false,
                validator:YAHOO.lang.isBoolean,
                method:function(value){
                    this.selectOneMode=value;
                }
            })
            
        },
        
        render:function(){
            if(this._showHideColumnsDlg && this._showHideRendered===false){
                
                this._renderShowHideColumnsDlg();
                this._showHideRendered=true;
            }
            
            CustomWidgets.DataTable.superclass.render.call(this);
            
            // saving the state after rendering is no longer needed. Not sure why it was here in the first place.
//            // after rendering the grid save the initial state. This only needs to be done once.//            
//            if(!this.initialStateSaved){
//                this.saveState();                
//                this.initialStateSaved=true;
//            }
        },
        
        
        
        _renderShowHideColumnsDlg:function(){
            var parentEl=this.getContainerEl();
            this._showHideDlgNode=document.createElement("a");
            this._showHideDlgNode.href="#";
            this._showHideDlgNode.innerHTML="Show/Hide Columns";
            parentEl.parentNode.insertBefore(this._showHideDlgNode,parentEl);           
       
            
            this.showHideDlg=new YAHOO.widget.Dialog(YAHOO.util.Dom.generateId(), {
              width: "30em",
		        visible: false,
		        modal: true,
		        buttons: [ 
				      { text:"Close",  handler: function(e) { this.hide(); } }
              ],
              fixedcenter: true,
              constrainToViewport: true
            });
           
           this.showHideDlg.beforeShowEvent.subscribe(function(e){   
               // here "this" is the dialog box object
                var viewportHeight=YAHOO.util.Dom.getViewportHeight();
                var dialogHeight=(viewportHeight-100) + 'px';
                this.cfg.setProperty('height',dialogHeight);
               
           });
           7
            this.showHideDlg.beforeShowEvent.subscribe(function(e){   
               // here "this" is the dialog box object
                var viewportHeight=YAHOO.util.Dom.getViewportHeight();
                var dialogHeight=(viewportHeight-100) + 'px';
                this.cfg.setProperty('height',dialogHeight);
               
           });           
            
            YAHOO.util.Dom.addClass(this.showHideDlg.element.firstChild,'customDataTable-columnsDlg');
            
            this.showHideDlg.bodyId=YAHOO.util.Dom.generateId();
            this.showHideDlg.setHeader("Select the columns to show or hide");
            this.showHideDlg.setBody("<div id='"+this.showHideDlg.bodyId+"'></div>");
            this.showHideDlg.render(document.body);
        },
        
        
        displayShowHideDlg:function(e){
          YAHOO.util.Event.stopEvent(e);
          
      
          if(!this.noNewCols) {

              var that = this;
              var handleButtonClick = function(e, oSelf) {
                  var sKey = this.get("name");
                  if(this.get("value") === "Hide") {
                      // Hides a Column
                      that.hideColumn(sKey);
                  }
                  else {
                      // Shows a Column
                      that.showColumn(sKey);
                  }
              };

               // Populate Dialog
               // Using a template to create elements for the SimpleDialog
               var allColumns = this.getColumnSet().keys;
               var elPicker = YAHOO.util.Dom.get(this.showHideDlg.bodyId);

               var elTemplateCol = document.createElement("div");
               YAHOO.util.Dom.addClass(elTemplateCol, "dt-dlg-pickercol");
               var elTemplateKey = elTemplateCol.appendChild(document.createElement("span"));
               YAHOO.util.Dom.addClass(elTemplateKey, "dt-dlg-pickerkey");
               var elTemplateBtns = elTemplateCol.appendChild(document.createElement("span"));
               YAHOO.util.Dom.addClass(elTemplateBtns, "dt-dlg-pickerbtns");
               var onclickObj = {fn:handleButtonClick, obj:this, scope:false };

               // Create one section in the SimpleDialog for each Column
               var elColumn, elKey, elButton, oButtonGrp;
               // need to create new objects and sort on those, because sorting the existing column objects causes bugs
               // this sorts the hide/show dialog by column label name
               var colNames=[];
               for (var i=0;i<allColumns.length;i++){
                   colNames.push({label:allColumns[i].label,key:allColumns[i].getKey(),hidden:allColumns[i].hidden});
               }
               
               colNames.sort(function(a,b){
                   var keyA=a.label;
                   var keyB=b.label;                   
                   if(keyA < keyB) return -1;
                   if(keyA > keyB) return 1;
                   return 0;
               });
               for(var i=0,l=colNames.length;i<l;i++) {
                   var oColumn = colNames[i];

                   // Use the template
                   elColumn = elTemplateCol.cloneNode(true);

                   // Write the Column key
                   elKey = elColumn.firstChild;
                   elKey.innerHTML = (oColumn.label && oColumn.label !== "") ? oColumn.label : oColumn.getLabel();

                   if(oColumn.key!=="addColumn" && oColumn.key!=="editColumn" && oColumn.key!=="checkBoxColumn" &&  oColumn.key!=="deleteColumn" && oColumn.key!=="viewColumn") {

                      // Create a ButtonGroup
                      oButtonGrp = new YAHOO.widget.ButtonGroup({ 
                                      id: "buttongrp"+i, 
                                      name: oColumn.key, 
                                      container: elKey.nextSibling
                      });
                      oButtonGrp.addButtons([
                          { label: "Show", value: "Show", checked: ((!oColumn.hidden)), onclick: onclickObj},
                          { label: "Hide", value: "Hide", checked: ((oColumn.hidden)), onclick: onclickObj}
                      ]);

                      elPicker.appendChild(elColumn);

                   }
               }
               this.noNewCols = true;
            }
            this.showHideDlg.show();
        },
        
        _addAdditionalColumns:function(oConfigs){
            var newColumnDefs=[];
            if(oConfigs.enableSelectRows){
                newColumnDefs=newColumnDefs.concat([{
                      key:'checkBoxColumn',
                      label:'Select',
                      formatter:function(el, oRecord, oColumn, oData) {

                        var recKey=this.getRecordKey(oRecord);
                        if(YAHOO.lang.isUndefined(this.checkedRows[recKey]) || this.checkedRows[recKey]===false){
                            checked="";
                        }
                        else{
                            checked="checked='checked'";
                        }

                        el.innerHTML = "<input type='checkbox'" + checked +" />";
                      }
                }]);
            }
            
            if(oConfigs.showAddColumn){
                newColumnDefs=newColumnDefs.concat([{
                      key:'addColumn',
                      label:'Add',
                      formatter:function(elCell) {
                           elCell.innerHTML = "add";
                           elCell.style.cursor = 'pointer';
                      }
                }]);
            }

            if(oConfigs.showEditColumn){
                newColumnDefs=newColumnDefs.concat([{
                      key:'editColumn',
                      label:'Edit',
                      formatter:function(elCell) {
                           elCell.innerHTML = "edit";
                           elCell.style.cursor = 'pointer';
                      }
                }]);
            }
            
            if(oConfigs.showDeleteColumn){
                newColumnDefs=newColumnDefs.concat([{
                      key:'deleteColumn',
                      label:'Delete',
                      formatter:function(elCell) {
                           elCell.innerHTML = "delete";
                           elCell.style.cursor = 'pointer';
                      }
                }]);
            }
            
            if(oConfigs.showViewColumn){
                newColumnDefs=newColumnDefs.concat([{
                      key:'viewColumn',
                      label:'View',
                      formatter:function(elCell) {
                           elCell.innerHTML = "view";
                           elCell.style.cursor = 'pointer';
                      }
                }]);
            }
            
            
            newColumnDefs=newColumnDefs.concat(this._aColumnDefs);
            
            this._aColumnDefs=newColumnDefs;
        },
        
        _applyFormatting:function(){
            var i;
            var column;
            var newColumnDef;
            var aColumnDefs=this._aColumnDefs;
            for(i=0;i<aColumnDefs.length;i++){
                column=aColumnDefs[i];
                if(column.colType==="button"){
                    newColumnDef={
                        key:column.key,
                        label:column.label,
                        formatter:function(elCell, oRecord, oColumn, oData) {
                           elCell.innerHTML = "<button >" + oColumn.label + "</button>";
                           elCell.style.cursor = 'pointer';
                        },
                        sortable:false,
                        colType:column.colType,
                        resizeable:true,
                        width:column.width
                    }
                    aColumnDefs[i]=newColumnDef;
                }
                
                if(column.colType==="link"){
                    newColumnDef={
                        key:column.key,
                        label:column.label,
                        formatter:function(elCell,oRecord,oColumn,oData){
                            if(oData){
                                var substitutedUrl=oColumn.url;
                                var i,pattern,re,colname
                                if(oColumn.valuesToSubstitute){
                                    for(i=0;i<oColumn.valuesToSubstitute.length;i++){
                                        colname=oColumn.valuesToSubstitute[i];
                                        pattern = "%" + colname + "%"; 
                                        re = new RegExp(pattern, "g");                                 
                                        substitutedUrl = substitutedUrl.replace(re, oRecord.getData(colname));
                                    }
                                }
                                elCell.innerHTML="<a href='" +substitutedUrl+"'>" + oData + "</a>";
                            }
                            else{
                                elCell.innerHTML="";
                            }
                            elCell.style.cursor = 'pointer';
                        },
                        sortable:false,
                        resizeable:true,
                        colType:column.colType,
                        url:column.url,
                        valuesToSubstitute:column.valuesToSubstitute ,
                        width:column.width
                }
                aColumnDefs[i]=newColumnDef;              
            }
            
            if(column.colType==="boolean"){
                aColumnDefs[i].formatter=function(elCell,oRecord,oColumn,oData){
                    if(oData==="1" || oData===1){
                        elCell.innerHTML="true";
                    }
                    else if (oData==="0" || oData===0){
                        elCell.innerHTML="false"
                    }
                }
            }
        }
            this._aColumnDefs=aColumnDefs;
        },
        
        getRecordKey:function(record){
            var key;
            var values=[];

            for(key in this.keys){
               values.push(record.getData(this.keys[key]))
            }

            var recKey=values.join("_");
            return recKey
        },
        
        getSelectedRows:function(){
            var selectedRows={};
            var key;
            if(!this.selectOneMode){
                for(key in this.checkedRows){
                    if(this.checkedRows[key]){
                        selectedRows[key]=this.checkedRows[key].getData();
                    }
                }
            }
            else{
                for(key in this.checkedRows){
                    if(this.checkedRows[key]){
                        selectedRows=this.checkedRows[key].getData();
                    }
                }
            }
            return selectedRows;
        },
        
        onCheckBoxClick: function(oArgs){
            var elCheckbox =oArgs.target;
            var record= this.getRecord(elCheckbox);

            var key=this.getRecordKey(record);

            if(elCheckbox.checked === true){
               if(this.selectOneMode){
                   this.checkedRows={};
               }
               this.checkedRows[key]=record;
               
               if(this.selectOneMode){                   
                   this.render();
               }
            }
            else{
               this.checkedRows[key]=false;
            } 
        },
        
        onCellClick:function(ev,args){
            var target = YAHOO.util.Event.getTarget(ev);
            var column = this.getColumn(target);

            if(column.key=='addColumn'){
                this.addClicked.fire(target);
                return;
            }

            if(column.key=='editColumn'){
                this.editClicked.fire(target);
                return;
            }
            
            if(column.key=='deleteColumn'){
                this.deleteClicked.fire(target);
                return;
            }
            
            if(column.key=='viewColumn'){
                this.viewClicked.fire(target);
                return;
            }
            
            var i,colName,eventName;
            
            for(i=0;i<this.columnsWithEvents.length;i++){
                colName=this.columnsWithEvents[i];
                if(column.key===colName){
                    eventName=column.key + "Clicked";
                    this[eventName].fire(target);
                    return;
                }
            }
            
        },

        checkAll: function(){
            // generate url
            var datasource=this.getDataSource();
            var oState=this.getState();
            
            var url=datasource.liveData
            url+="&numberOfRows="+oState.totalRecords;
               
            if(!YAHOO.lang.isUndefined(this.whereStatement) && !YAHOO.lang.isNull(this.whereStatement)){
                url+='&whereStatement=' + this.whereStatement;
            }
            
            // define callback to parse JSON response and update checkRows hash accordingly
            
            var onSuccess=function(response){
                var results=YAHOO.lang.JSON.parse(response.responseText);
                var data=results.data;
                
                for(var i=0;i<data.length;i++){
                    var record=new YAHOO.widget.Record(results.data[i]);
                    var key=this.getRecordKey(record);
                    this.checkedRows[key]=record;
                }
                this.render();     
            }
            
            var onFailure=function(response){return false;};
            
            var callback={success:onSuccess,
                          failure:onFailure,
                          scope: this
                         }
            
            
            // execute request using standard ajax call
            YAHOO.util.Connect.asyncRequest('GET',url,callback);
            
            
        },
        
        checkAllOnPage:function(){
            
            var records=this.getRecordSet();
            
            for(var i=0;i<records.getLength();i++){
                var record=this.getRecord(i);
                var key=this.getRecordKey(record);
                this.checkedRows[key]=record;
            }
            
            this.render();
        },
        
        unCheckAllOnPage:function(){
            
            var records=this.getRecordSet();
            
            for(var i=0;i<records.getLength();i++){
                var record=this.getRecord(i);
                var key=this.getRecordKey(record);
                this.checkedRows[key]=false;
            }
            
            this.render();
        },

        unCheckAll: function(){
           this.checkedRows={};
           this.render();
           
        },
       
        
        handleDataReturnPayload:function(oRequest, oResponse, oPayload) {
            oPayload.totalRecords = oResponse.meta.totalRecords;
            return oPayload;
        },
        
        refreshData: function(whereStatement){
        
            if(whereStatement){
               this.whereStatement=whereStatement;
            }
            else{
               this.whereStatement=null;
            }
            var oState=this.getState();

            var oCallback={
                success:this.onDataReturnSetRows,
                failure:this.onDataReturnSetRows,
                argument: oState,
                scope:this
            }

           var request=this.get("generateRequest")(oState,this);

           this.getDataSource().sendRequest(request,oCallback);
        },
        
        adjustView: function(){
            if(!YAHOO.lang.isNull(this.bodyId)){
               //  Get Scroll Position

                var ScrollBarPosition= YAHOO.util.Dom.getDocumentScrollTop() + YAHOO.util.Dom.getViewportHeight();;

                var body=YAHOO.util.Dom.getRegion(this.bodyId);

                // if the ScrollBar position is past to bottom of the body, scroll
                // the bottom of the body into view. This happens when the size of
                // the table decreases.
                
                if(ScrollBarPosition > body.bottom){
                    var bodyEl=YAHOO.util.Dom.get(this.bodyId)
                    bodyEl.scrollIntoView(false);
                } 
            }
        },
        
        onPaginatorChangeRequest: function(oPaginatorState){
            CustomWidgets.DataTable.superclass.onPaginatorChangeRequest.call(this,oPaginatorState);
             
            if(this.scrollToTopOnPageChange){
                var region=YAHOO.util.Dom.getRegion(this.topPaginatorName);
                window.scrollTo(region.left,region.top);
            }
        },
        
        saveState: function(){
            var columnState=this.getColumnSet().getDefinitions();
            
            var dataTableState=this.getState();
            
            var sortState=dataTableState.sortedBy;
            var paginatorState=dataTableState.pagination;
            // if paginator state is defined
            if(paginatorState){
                delete paginatorState.paginator;
            }
            
            var columnsToRemove=["addColumn","editColumn","deleteColumn","checkBoxColumn","viewColumn"];
            //columnsToRemove.push.apply(columnsToRemove,this.columnsWithEvents);
            
            var columnIndexesToRemove=[];
            for(var i=0;i<columnsToRemove.length;i++){
                var columnName=columnsToRemove[i];
                var column = this.getColumn(columnName);
                if(!YAHOO.lang.isNull(column)){
                    var columnIndex = column.getKeyIndex();
                    columnIndexesToRemove.push(columnIndex);
                }
            }
            
            var columnTypes={};
            for(var j=0;j<this.columnsWithEvents.length;j++){
                var columnName=this.columnsWithEvents[j];
                var columnObj=this.getColumn(columnName);
                columnTypes[columnName]={"colType":columnObj.colType,"index":columnObj.getKeyIndex()};
                               
            }
            
            var state={"columnState":columnState,"paginatorState":paginatorState,"sortState":sortState,
                        "columnIndexesToRemove":columnIndexesToRemove,"columnTypes":columnTypes};
            
            this.sendStateToServer(state);
        },
        
        // this function is generated by PHP.
        sendStateToServer: function(state){
            
        }
        
        
    })
    
})();
