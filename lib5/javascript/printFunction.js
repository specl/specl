(function(){

    printElement=function(element)
    {
        var iframe= document.createElement("iframe");
        document.body.appendChild(iframe);
        var newElement=element.cloneNode(true);
        iframe.contentWindow.document.body.appendChild(newElement);
        iframe.contentWindow.focus();
        iframe.contentWindow.print();
        document.body.removeChild(iframe);
        return false;
    }
    
    printFormData=function(data){
        var iframe= document.createElement("iframe");
        document.body.appendChild(iframe);
        iframe.contentWindow.document.write(data);
        iframe.contentWindow.focus();
        iframe.contentWindow.print();
        document.body.removeChild(iframe);
        return false;
    }
    
})();