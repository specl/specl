(function(){
    
    var lang = YAHOO.lang, Dom = YAHOO.util.Dom, Event = YAHOO.util.Event;

    inputEx.ListField2=function(options){
        inputEx.ListField2.superclass.constructor.call(this,options);
    };
    
    YAHOO.lang.extend(inputEx.ListField2,inputEx.ListField,{
    
    setOptions: function(options){
         inputEx.ListField2.superclass.setOptions.call(this, options);
         
         this.options.disableAddRemove = YAHOO.lang.isUndefined(options.disableAddRemove) ? false : options.disableAddRemove;
         
         this.options.collapsible = lang.isUndefined(options.collapsible) ? false : options.collapsible;
         this.options.collapsed = lang.isUndefined(options.collapsed) ? false : options.collapsed;         

    },
    
    clear: function(sendUpdatedEvt) {
        this.setValue(YAHOO.lang.isUndefined(this.options.value) ? [] : this.options.value, sendUpdatedEvt);
    },
    
    /**
    * Default render of the dom element. Create a divEl that wraps the field.
    */
	render: function() {
	
	   // Create a DIV element to wrap the editing el and the image
	   this.divEl = inputEx.cn('div', {className: 'inputEx-fieldWrapper'});
	   if(this.options.id) {
	      this.divEl.id = this.options.id;
	   }
	   if(this.options.required) {
	      Dom.addClass(this.divEl, "inputEx-required");
	   }
       
       this.fieldContainer = inputEx.cn('div', {className: this.options.className}); // for wrapping the field and description
	   
	   // Label element
	   if (YAHOO.lang.isString(this.options.label)) {
	      
          if(this.options.collapsible) {
            this.labelDiv = inputEx.cn('div', {id: this.divEl.id+'-label', className: 'inputEx-Group-legend', 'for': this.divEl.id+'-field'});
            this.collapseImg = inputEx.cn('div', {className: 'inputEx-Group-collapseImg'}, null, ' ');
            this.labelDiv.appendChild(this.collapseImg);            
            this.labelEl = inputEx.cn('label', null, null, this.options.label === "" ? "&nbsp;" : this.options.label);
            this.labelDiv.appendChild(this.labelEl);
            this.divEl.appendChild(this.labelDiv);
            
            YAHOO.util.Dom.addClass(this.fieldContainer,'inputEx-Expanded');
            YAHOO.util.Dom.addClass(this.labelDiv,'inputEx-Expanded');
          }
          else{
                this.labelDiv = inputEx.cn('div', {id: this.divEl.id+'-label', className: 'inputEx-label', 'for': this.divEl.id+'-field'});
                this.labelEl = inputEx.cn('label', null, null, this.options.label === "" ? "&nbsp;" : this.options.label);
                this.labelDiv.appendChild(this.labelEl);
                this.divEl.appendChild(this.labelDiv);
          }
	    
      }
      
      // Render the component directly
      this.renderComponent();
      
      // Description
      if(this.options.description) {
         this.fieldContainer.appendChild(inputEx.cn('div', {id: this.divEl.id+'-desc', className: 'inputEx-description'}, null, this.options.description));
      }
      
      this.divEl.appendChild(this.fieldContainer);
      
	   // Insert a float breaker
	   this.divEl.appendChild( inputEx.cn('div',null, {clear: 'both'}," ") );
       
        if(!lang.isUndefined(this.options.value)) {
            this.setValue(this.options.value,false);
        }
	
	},
    
    renderComponent: function(){
        // Add element button
	   if(this.options.useButtons && !this.options.disableAddRemove) {
	      this.addButton = inputEx.cn('img', {src: inputEx.spacerUrl, className: 'inputEx-ListField-addButton'});
	      this.fieldContainer.appendChild(this.addButton);
        }      

       // List label
       this.fieldContainer.appendChild( inputEx.cn('span', null, {marginLeft: "4px"}, this.options.listLabel) );

	   // Div element to contain the children
	   this.childContainer = inputEx.cn('div', {className: 'inputEx-ListField-childContainer'});
	   this.fieldContainer.appendChild(this.childContainer);
	   
	   // Add link
	   if(!this.options.useButtons && !this.options.disableAddRemove) {
	      this.addButton = inputEx.cn('a', {className: 'inputEx-List-link'}, null, this.options.listAddLabel);
	      this.fieldContainer.appendChild(this.addButton);
      }
      
      // Collapsed at creation ?
  	   if(this.options.collapsed) {
  	      this.toggleCollapse();
  	   }
      
      YAHOO.util.Dom.addClass(this.labelDiv,'inputEx-ListField');
    },
    
    renderSubField: function(value) {
	      
	   // Div that wraps the deleteButton + the subField
	   var newDiv = inputEx.cn('div'), delButton;
	      
	   // Delete button
	   if(this.options.useButtons && !this.options.disableAddRemove) {
	      delButton = inputEx.cn('img', {src: inputEx.spacerUrl, className: 'inputEx-ListField-delButton'});
	      Event.addListener( delButton, 'click', this.onDelete, this, true);
	      newDiv.appendChild( delButton );
      }
	      
	   // Instantiate the new subField
	   var opts = YAHOO.lang.merge({}, this.options.elementType);
	   
	   // Retro-compatibility with deprecated inputParams Object : TODO -> remove
      if(YAHOO.lang.isObject(opts.inputParams) && !YAHOO.lang.isUndefined(value)) {
         opts.inputParams.value = value;
         
      // New prefered way to set options of a field
      } else if (!YAHOO.lang.isUndefined(value)) {
         opts.value = value;
      }
	   
	   var el = inputEx(opts,this);
	   
	   var subFieldEl = el.getEl();
       YAHOO.util.Dom.addClass(subFieldEl, 'inputEx-ListField-subFieldEl');
	   newDiv.appendChild( subFieldEl );
	   
	   // Subscribe the onChange event to resend it 
	   el.updatedEvt.subscribe(this.onChange, this, true);
	
	   // Arrows to order:
	   if(this.options.sortable ) {
	      var arrowUp = inputEx.cn('div', {className: 'inputEx-ListField-Arrow inputEx-ListField-ArrowUp'});
	      YAHOO.util.Event.addListener(arrowUp, 'click', this.onArrowUp, this, true);
	      var arrowDown = inputEx.cn('div', {className: 'inputEx-ListField-Arrow inputEx-ListField-ArrowDown'});
	      YAHOO.util.Event.addListener(arrowDown, 'click', this.onArrowDown, this, true);
	      newDiv.appendChild( arrowUp );
	      newDiv.appendChild( arrowDown );
	   }
	   
	   // Delete link
	   if(!this.options.useButtons && !this.options.disableAddRemove) {
	      delButton = inputEx.cn('a', {className: 'inputEx-List-link'}, null, this.options.listRemoveLabel);
	      YAHOO.util.Event.addListener( delButton, 'click', this.onDelete, this, true);
	      newDiv.appendChild( delButton );
      }
	
	   // Line breaker
	   newDiv.appendChild( inputEx.cn('div', null, {clear: "both"}) );
	
	   this.childContainer.appendChild(newDiv);
	      
	   return el;
	},
    
    initEvents: function(){
        if(!this.options.disableAddRemove){
            inputEx.ListField2.superclass.initEvents.call(this);
        }
        
        
        // Add a listener for the 'collapsible' option
        if(this.options.collapsible) {
            Event.addListener(this.labelDiv, "click", this.toggleCollapse, this, true);
        }
        
        this.updatedEvt.subscribe(this.onUpdate,this);
    },
    
    /**
    * Toggle the collapse state
    */
   toggleCollapse: function() {
      // only toggle the collapsed state if there are sub-elements
        if(Dom.hasClass(this.fieldContainer, 'inputEx-Expanded')) {
            Dom.replaceClass(this.fieldContainer, 'inputEx-Expanded', 'inputEx-Collapsed');
            Dom.replaceClass(this.labelDiv, 'inputEx-Expanded', 'inputEx-Collapsed');
        }
        else {
            Dom.replaceClass(this.fieldContainer, 'inputEx-Collapsed','inputEx-Expanded');
            Dom.replaceClass(this.labelDiv, 'inputEx-Collapsed', 'inputEx-Expanded');
        }
      
    },
    
    onUpdate:function(e){
        if(this.subFields.length>0){
            Dom.replaceClass(this.labelDiv,'inputEx-label', 'inputEx-Group-legend');
            Dom.addClass(this.collapseImg,'inputEx-Group-collapseImg');
            
        }
        else{
            Dom.replaceClass(this.labelDiv, 'inputEx-Group-legend', 'inputEx-label');
            Dom.removeClass(this.collapseImg,'inputEx-Group-collapseImg');
            
            // if there are no elements in the field return it to the collapsed state.
            Dom.replaceClass(this.fieldContainer, 'inputEx-Expanded', 'inputEx-Collapsed');
            Dom.replaceClass(this.labelDiv, 'inputEx-Expanded', 'inputEx-Collapsed');
        }
            
    },
    
    /**
	 * Add a new element to the list and fire updated event
	 * @param {Event} e The original click event
	 */
	onAddButton: function(e) {
	   Event.stopEvent(e);
	   
	   // Prevent adding a new field if already at maxItems
	   if( lang.isNumber(this.options.maxItems) && this.subFields.length >= this.options.maxItems ) {
	      return;
	   }
	   
	   // Add a field with no value: 
	   var subFieldEl = this.addElement();
	   
	   // Focus on this field
	   subFieldEl.focus();
       
       // on add expand the field if it is collapsed.
       Dom.replaceClass(this.fieldContainer, 'inputEx-Collapsed','inputEx-Expanded');
       Dom.replaceClass(this.labelDiv, 'inputEx-Collapsed', 'inputEx-Expanded');    
	   
	   // Fire updated !
	   this.fireUpdatedEvt();
	},

    validate:function(){
        return true;
    },
            
    setClassFromState:function(){

    }
    });
    
    
    
    inputEx.registerType("list2", inputEx.ListField2,{});
})();