/*
 *  A DataEntryGrid stores a copy of the column and row definitions in javascript and then
 *  renders them in html. 
 *  
 *  Anytime the properties of a column change in must be updated in the javascript data and then rerendered.
 *  
 *  Anytime a row is added or removed the row data must be updated in the javascript as well.
 *   
 */
var DataEntryGrid=function(options){
    this.events={};
    this.actions={};  
    this.navButtonObjs=[];
    this.id=options.data.id;
    /*
     *  An autoincrement index is required give each row a unique internal id. This is needed for the
     *  remove row button, as the program needs to know which element to remove. The autoindex is incremented
     *  every time a new row is added, but it is not decremented when a row is removed.
     */
    this._autoIndex=0;
    
    this.setColumnDef(options.data.columns);
    this.setData(options.data.rows);
    
    this.gridObj = options.data;        
    var rootDiv=YAHOO.util.Dom.get(this.gridObj.id);            
    this.render(rootDiv);
        
    this.addRowNumbers();
    
    this.initEvents();
}

DataEntryGrid.prototype.render=function(rootDiv) {    
    this.renderTable(rootDiv);
}


DataEntryGrid.prototype.renderTable=function(rootDiv) {
    
    var form = document.createElement("form");
    rootDiv.appendChild(form);   
    
    var table = document.createElement("table");
    var id=this.id+'_table';    
    table.setAttribute("id", id);
    form.appendChild(table);    
    
    this.renderHeader(table);  
    this.renderRows(table);  
    this.renderFooter(table);                  
}

DataEntryGrid.prototype.renderHeader=function(table) {   
    
    var thead = document.createElement("thead");        
    var id=this.id+'_thead';    
    thead.setAttribute("id", id);
    
    table.appendChild(thead);    
        
    var tr = document.createElement("tr");
    thead.appendChild(tr);        
        
    for (var i=0; i<this.colData.length; i++) {
        var control = this.colData[i].data;        
    
        var th = document.createElement("th");
        tr.appendChild(th);    
        
        var text = document.createTextNode(control.caption);                 
        th.appendChild(text);            
    }
}

DataEntryGrid.prototype.renderRows=function(table) {
        
    var tbody = document.createElement("tbody");            
    table.appendChild(tbody);
    var id=this.id+'_tbody';    
    tbody.setAttribute("id", id);
        
    for (var i=0; i<this.rowData.length; i++) {        
        var rowData = this.rowData[i].data;          
        //var rowData = this.rowData[i];          
        rowData.data.rowNum = i + 1;
        var tr = this.renderRow(tbody, i, rowData);                      
    }      
}

DataEntryGrid.prototype.addRowNumbers=function() {
    
    for (var i=0; i<this.rowData.length; i++) {        
        var row = this.rowData[i].data;            
        var id = row.id+'_rowNum';  // hard code the name of the columna
        // i don't like this but I haven't thought of a way around it yet.                   
        var input=YAHOO.util.Dom.get(id);              
        input.setAttribute('value',i+1)
    }    
}

DataEntryGrid.prototype.renderRow=function(tbody, rowNum, rowData) {
        
    var tr = document.createElement("tr");       
    tr.setAttribute("id", rowData.id);
    
    if (rowNum%2 == 0) {
        tr.setAttribute("class", "odd");
    } 
    tbody.appendChild(tr);  

    for (var j=0; j<this.colData.length; j++) {
        var classname = this.colData[j].classname;
        var control = this.colData[j].data;
        
        var td = document.createElement("td");
        
        tr.appendChild(td);
        
        if (rowData.data[control.id]===undefined) {
            value = "";
        } else {
            value = rowData.data[control.id];
        }
        
        
        // Add the BaseProperties shared from all controls
        var params={
           "name": rowData.id+'_'+control.id,
           "size": control.size, 
           "value":value
        }
        var el,obj;
        if (classname=="TextControl") {
            params.maxSize=control.maxSize;
            obj=new Zephyr.TextControl(params);
            el=obj.render();
            td.appendChild(el);            
        } else if (classname=="LabelControl") {
            obj=new Zephyr.LabelControl(params);
            el=obj.render();                                        
            td.appendChild(el);
            
        } else if (classname=="CheckBoxControl") {
            obj=new Zephyr.CheckBoxControl(params);
            el=obj.render();                                        
            td.appendChild(el);
        } else if (classname=="DropDownControl") {
            params.choices=control.choices;
            obj=new Zephyr.DropDownControl(params);
            el=obj.render();
            td.appendChild(el);
        } else if (classname=="ListControl") {
            
        } else if (classname=="AutoCompleteControl") {
            
        } else if (classname=="ImageControl") {
        
        } else if (classname=="ButtonControl") {            
            obj=new Zephyr.ButtonControl({
                name:rowData.id + '_' + control.id ,
                text: control.caption
            });
            el=obj.render();
            obj.initEvents({"rowId":tr.id, "rowNumber":rowNum,"action":"removeRow"});  
            td.appendChild(el);             
        } else {
            if (rowData.data[control.id]===undefined) {
            }
        }
        rowData.controls[control.id] = obj; 
        
        // initialize control events if it has any        
              
        
        var errDiv =  document.createElement("div"); 
        errDiv.setAttribute("id", rowData.id+'_'+control.id+"_error");
        td.appendChild(errDiv); 
        
    }
    
    return tr;
}

DataEntryGrid.prototype.renderFooter=function(table) {        
        
    var tfoot = document.createElement("tfoot");        
    var id=this.id+'_tfoot';    
    tfoot.setAttribute("id", id);    
    table.appendChild(tfoot);    
        
    var tr = document.createElement("tr");
    tfoot.appendChild(tr);        

    var td = document.createElement("td");
    td.setAttribute("colspan",this.gridObj.columns.length);
    tr.appendChild(td);            
        
    var navButtons = this.gridObj.navButtons;
    var buttonObj,el;
    
    for (var i=0; i<navButtons.length; i++) {        
        var control = navButtons[i].data;                  
        control.domId = this.id + '_' + control.id;  
        buttonObj=new Zephyr.ButtonControl({
            "type":"button",
            "text":control.caption,
            "name":control.domId
        })
        el=buttonObj.render();
        this.navButtonObjs.push(buttonObj);                              
        td.appendChild(el);
    }    
}

/**
 *  This function takes a list of column option objects and stores it in the datagrid.
 *  This is used when initializing a new data grid.
 */
DataEntryGrid.prototype.setColumnDef=function(colDefs){
    this.colData=colDefs;
}

/*
 * This function takes a list of RowData objects and stores it in javascript.
 * This is used when initializing a new data grid.
 * 
 * Each row in the grid is given a unique id based on the DataEntryGrid id and the
 * autoincrement index.
 */
DataEntryGrid.prototype.setData=function(rowData){
    //var newId;
    //for(var i=0;i<rowData.length;i++){
    //   newId=this.genRowId();
    //   rowData[i].data.id=newId;       
    //}
    //this.rowData=rowData;    
    this.rowData=[];
    var id;
    var row;
    for(id in rowData) {
        row = rowData[id];
        this.addRow2(id, row);
    }
}

/*
 * This function gets the row data from the javascript class.
 */
DataEntryGrid.prototype.getData=function(){
    
    // move data from GUI to RowData   
    for (var i=0; i<this.rowData.length; i++) {        
        var rowData = this.rowData[i].data;          
        for (var j=0; j<this.colData.length; j++) {
            //var classname = this.colData[j].classname;
            var control = this.colData[j].data;               
            var controlObj = this.rowData[i].data.controls[control.id];                                                         
            var value = controlObj.getValue();              
            this.rowData[i].data.data[control.id] = value;
         }                           
    }
    
    return this.rowData;
}

/*
 *  This functions gets the data from the grid and formats the data into
 *  an appropriate structure for JSON. *  
 *  It does the following:
 *      - Remove the control data
 *      - Put the row data into a key/value pair object of rowname/data
 */
DataEntryGrid.prototype.getJSONData=function(){
    var origRowData=this.getData();
    var jsonRowData={};
    var oldRow,rowData,rowName;    
    
    for(var i=0; i<origRowData.length;i++){
        oldRow=origRowData[i];
        rowName=oldRow.data.id
        rowData=oldRow.data.data;
        jsonRowData[rowName]=rowData;
    }    
    return jsonRowData;    
}




/*
 * This function gets the column data from the javascript class
 */
DataEntryGrid.prototype.getColumnDefs=function(){
    return this.colData;
}

/*
 * This function creates a unique row name based on the id of the DataEntryGrid and the autoincrement id.
 * The autoincrement is incremented by 1 everytime this function is called.
 */
DataEntryGrid.prototype.genRowId=function(){
    var id=this.id + "_row_" + this._autoIndex;
    this._autoIndex+=1;
    return id;
}
 
// Do not call this more than once.
DataEntryGrid.prototype.initEvents=function() {           
    for (var i=0; i<this.gridObj.navButtons.length; i++) {                        
        var control = this.gridObj.navButtons[i].data;                    
        this.navButtonObjs[i].initEvents({"grid":this.gridObj, "control":control});        
    }
}

DataEntryGrid.prototype.addEmptyRowData=function() {    
    var row = {};        
    row.data = {};       
    row.classname = "RowData";
    row.data.id = this.genRowId();        
    row.id = row.data.id;    
    
    // could init value to defaults!    
    row.data.data = {};
    row.data.controls = {};   // to store the controls, so we can use their get and set
    
    row.data.data.rowNum = 0;
          
    this.rowData.push(row);     
    return row;
}


DataEntryGrid.prototype.addRow2=function(id, rowData) {    
    var row = {};        
    row.data = {};       
    row.classname = "RowData";
    row.data.id = id;          
    row.id = row.data.id;    
    
    // could init value to defaults!    
    row.data.data = rowData;
    row.data.controls = {};   // to store the controls, so we can use their get and set
    
    row.data.data.rowNum = 0;
          
    this.rowData.push(row);     
    return row;
}



DataEntryGrid.prototype.addNewRow=function(obj) {          
    var rowData = this.addEmptyRowData();                       
    var tbodyId=this.id+'_tbody';            
    var tbody=YAHOO.util.Dom.get(tbodyId);         
    this.renderRow(tbody, this.rowData.length-1, rowData.data);          
    this.addRowNumbers();
}

/*
 * Takes a standard javascript object with the row data and adds a row.
 */
DataEntryGrid.prototype.addRow=function(rowData){    
    var tbodyId=this.id+'_tbody';            
    var tbody=YAHOO.util.Dom.get(tbodyId);         
    
    var row={};
    row.data={};
    row.classname = "RowData";
    row.data.id = this.genRowId();        
    row.id = row.data.id;  
    row.data.data = rowData;  
    row.data.controls = {};   // to store the controls, so we can use their get and set
    
    this.rowData.push(row);    
    this.renderRow(tbody, this.rowData.length-1, row.data);        
    
    var fillData={}
    fillData[row.data.id]=rowData;
    this.fillInRowData(fillData)
    this.addRowNumbers();
}

DataEntryGrid.prototype.removeRow=function(obj) {        
    
    var rowId = obj.rowId;
    var rowNumber = obj.rowNumber;

    var id=this.id+'_tbody';            
    var tbody=YAHOO.util.Dom.get(id);              
    var tr=YAHOO.util.Dom.get(rowId);              
    tbody.removeChild(tr);                   
        
    var index;
    for (var i=0; i<this.rowData.length; i++) {        
        var rowData = this.rowData[i].data;          
        
        if (rowData.id == obj.rowId) {
            // we have a winner
            index = i;
        }
    }      
    
    this.rowData.splice(index, 1);            
    this.addRowNumbers();
}

DataEntryGrid.prototype.clearErrors=function() {        
    
    var rowData;
    var control;
    var errDivId;
    var errDiv;
    var errText;
    var parentDiv;
    var inputId;
    var inputEl;
    
    for (var i=0; i<this.rowData.length; i++) {        
        rowData = this.rowData[i].data;          

        for (var j=0; j<this.colData.length; j++) {
            control = this.colData[j].data;              
            errDivId = rowData.id+'_'+control.id+"_error";
            errDiv = YAHOO.util.Dom.get(errDivId);             
            
//            if (errDiv.childNodes.length > 0) {
//                errText = errDiv.firstChild;
//                errText.nodeValue="";
//            }            
            errDiv.innerHTML = '';

            parentDiv = errDiv.parentNode;
            YAHOO.util.Dom.removeClass(parentDiv, 'error');       
            
            inputId = rowData.id+'_'+control.id;
            inputEl=YAHOO.util.Dom.get(inputId);                        
            YAHOO.util.Dom.removeClass(inputEl, 'error'); 
         }
    }  
}


DataEntryGrid.prototype.showErrorMessages=function(errorMessages) {        
    
    var rowKey;
    var fieldKey;
    var rowError;
    var fieldError;
    var errDivId;
    var errDiv;
    var errText;
    var parentDiv;
    
    var inputId;
    var inputEl;
    
    var addButton;
    var addButtonEl;
    
    for(rowKey in errorMessages) {
        rowError = errorMessages[rowKey];
        
        for(fieldKey in rowError) {            
            fieldError = rowError[fieldKey];

            errDivId = rowKey+'_'+fieldKey+"_error";            
            errDiv=YAHOO.util.Dom.get(errDivId);

            if (errDiv !== null) {
                inputId = rowKey+'_'+fieldKey; 
                inputEl=YAHOO.util.Dom.get(inputId);
                errDiv.innerHTML = fieldError;
    //            if (errDiv.childNodes.length > 0) {
    //                errText = errDiv.firstChild;
    //                //errText.nodeValue=fieldError;
    //                errText.innerHTML = errText;
    //                
    //            } else {
    //                errText=document.createTextNode(fieldError)
    //                errText.innerHTML = errText;
    //                errDiv.appendChild(errText);
    //            }
                YAHOO.util.Dom.addClass(errDiv, 'error');             
                //YAHOO.util.Dom.removeClass(element, className);            
                parentDiv = errDiv.parentNode;            
                YAHOO.util.Dom.addClass(parentDiv, 'error');             
                YAHOO.util.Dom.addClass(inputEl, 'error'); 

                //addButton=new Zephyr.ButtonControl({
                //    name:rowKey+'_'+fieldKey+'_add',
                //    text:'Add Sample'
                //});
                //addButtonEl=addButton.render();
                //parentDiv.appendChild(addButtonEl);                          
            }
        }
    }        
}

DataEntryGrid.prototype.fillInRowData=function(dataRows) {        
    
    var rowKey;
    var rowData;
    
    var fieldKey;    
    var fieldValue;
        
    var inputId;
    var inputEl;
        
    var theCurrentRow;
    
    for(rowKey in dataRows) {
        rowData = dataRows[rowKey];
                
        for (var j=0; j<this.rowData.length; j++) {            
            var myRow = this.rowData[j];
            
            if (myRow.data.id==rowKey) {
                theCurrentRow = myRow;   
                break;
            }
        }
        
        var colKey;
        for(var i=0;i<this.colData.length;i++){
            if(this.colData[i].classname!=="ButtonControl" && this.colData[i].classname!=="LabelControl"){
                colKey=this.colData[i].data.id;
                fieldValue=rowData[colKey];
                if(fieldValue===undefined){
                    fieldValue="";
                }
                inputId = rowKey+'_'+colKey; 
                inputEl=YAHOO.util.Dom.get(inputId);

                if (inputEl!==null && inputEl!==undefined) {                  
                    var controlObj = theCurrentRow.data.controls[colKey];                
                    controlObj.setValue(fieldValue);
                }
            }
        }
    }        
}

/*EOF*/