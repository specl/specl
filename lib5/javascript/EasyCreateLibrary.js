var PageDispatcher=function(options){
    this.pageId=options.pageId;
    this.formId=options.formId;
    this.dataEntryGridId=options.dataEntryGridId;
    this.createButtonId=options.createButtonId;
    this.selectPopupId=options.selectPopupId;
    this.selectPopupGridId=options.selectPopupGridId;  
    this.selectSampleButtonId=options.selectSampleButtonId;
}

PageDispatcher.prototype=new CentralDispatcher();

var submitLibraryData = function(message) {
    if(message.elementName===DispatchObj.createButtonId){
        var postObj = {};
        var form=window[DispatchObj.formId];
        var dataEntryGrid=window[DispatchObj.dataEntryGridId]
        postObj.illuminaLibrary = form.getValue();
        postObj.samples = dataEntryGrid.getJSONData();
        var postStr=YAHOO.lang.JSON.stringify(postObj);        
        var ajaxURL='index.php?pageId='+DispatchObj.pageId+'&actionName=submitLibraryData&buildPage=false';

        var onSuccess=function(response){                    
            form.clearErrorMessages();              
            dataEntryGrid.clearErrors();

            var results=YAHOO.lang.JSON.parse(response.responseText);
            if(!YAHOO.lang.isUndefined(results.errors)) {
                var errors=results.errors;
                if(!YAHOO.lang.isUndefined(errors.form)) {                           
                    form.showErrorMessages(errors.form);
                }                       
                if(!YAHOO.lang.isUndefined(errors.sample)) {                           
                    dataEntryGrid.showErrorMessages(errors.sample);
                }
            } else if (results.message == 'redirect') {
                window.location.href = results.parameters.location;
            } else if(results.message == 'success'){
            //XXX.submitSuccess.fire();
            } else if(results.message == 'debug'){
            alert('the webservive returns : debug');
            }
        }
        var onFailure=function(response){alert('fail'); return false;};

        var callback={
            success:onSuccess,
            failure:onFailure
        }

        YAHOO.util.Connect.asyncRequest('POST',ajaxURL,callback,"value="+postStr);
    }    
}

var openSampleSearch = function(message){
    if(message.elementName===DispatchObj.selectSampleButtonId){
        var actionMsg={
            action:'showPopup',
            elementName: DispatchObj.selectPopupId                            
        };
        DispatchObj.dispatch(actionMsg);
    }
}

var removeRow=function(message){     
    if(message.obj!==undefined && message.obj.action==="removeRow"){
        var dataEntryGrid=window[DispatchObj.dataEntryGridId]
        dataEntryGrid.removeRow(message.obj);
    }
}



var addSample=function(message){
    var dataEntryGrid=window[DispatchObj.dataEntryGridId]
    if(message.elementName===dataEntryGrid.id+"_addSampleButton"){        
        dataEntryGrid.addNewRow();
    }
}

var createMissingSamples=function(message){    
    var dataEntryGrid=window[DispatchObj.dataEntryGridId];
    if(message.elementName===dataEntryGrid.id+"_createMissingSamples"){
        var postObj = {};        
        postObj.samples = dataEntryGrid.getJSONData();
        var postStr=YAHOO.lang.JSON.stringify(postObj);
        var ajaxURL='index.php?pageId='+DispatchObj.pageId+'&actionName=addSamples&buildPage=false';
        var onSuccess=function(response){                                    
            var results=YAHOO.lang.JSON.parse(response.responseText);
            if(!YAHOO.lang.isUndefined(results.errors)) {            
                if(!YAHOO.lang.isUndefined(results.rows)) { 
                    dataEntryGrid.clearErrors();
                    dataEntryGrid.fillInRowData(results.rows);
                    dataEntryGrid.showErrorMessages(results.errors);
                }        
            } else if(results.message == 'success'){ 
                dataEntryGrid.clearErrors();
            }
        }
        var onFailure=function(response){
            alert('fail'); 
            return false;
        };

        var callback={
            success:onSuccess,
            failure:onFailure
        }

        YAHOO.util.Connect.asyncRequest('POST',ajaxURL,callback,'value='+postStr);
    }
}

var lookupSamples=function(message){
    var dataEntryGrid=window[DispatchObj.dataEntryGridId];
    if(message.elementName===dataEntryGrid.id+"_lookupSamplesButton"){        
        var postObj = {};        
        postObj.samples = dataEntryGrid.getJSONData();
        var postStr=YAHOO.lang.JSON.stringify(postObj);
        var ajaxURL='index.php?pageId='+DispatchObj.pageId+'&actionName=lookupSamples&buildPage=false';        
        var onSuccess=function(response){                                    
            var results=YAHOO.lang.JSON.parse(response.responseText);
            if(!YAHOO.lang.isUndefined(results.errors)) {            
                if(!YAHOO.lang.isUndefined(results.rows)) {   
                    dataEntryGrid.clearErrors();
                    dataEntryGrid.fillInRowData(results.rows);
                    dataEntryGrid.showErrorMessages(results.errors);
                }        
            } else if(results.message == 'success'){ 
                dataEntryGrid.fillInRowData(results.rows);
                dataEntryGrid.clearErrors();
            }
        }
        var onFailure=function(response){
            alert('fail'); 
            return false;
        };

        var callback={
            success:onSuccess,
            failure:onFailure
        }

        YAHOO.util.Connect.asyncRequest('POST',ajaxURL,callback,'value='+postStr);    
    }
}

var updateSamples=function(message){
    var dataEntryGrid=window[DispatchObj.dataEntryGridId];
    if(message.elementName===dataEntryGrid.id+"_updateSamplesButton"){
        var postObj = {};        
        postObj.samples = dataEntryGrid.getJSONData();
        var postStr=YAHOO.lang.JSON.stringify(postObj);
        var ajaxURL='index.php?pageId='+DispatchObj.pageId+'&actionName=updateSamples&buildPage=false';        
        var onSuccess=function(response){                                    
            var results=YAHOO.lang.JSON.parse(response.responseText);
            if(!YAHOO.lang.isUndefined(results.errors)) {            
                if(!YAHOO.lang.isUndefined(results.rows)) { 
                    dataEntryGrid.clearErrors();
                    dataEntryGrid.fillInRowData(results.rows);
                    dataEntryGrid.showErrorMessages(results.errors);
                }        
            } else if(results.message == 'success'){  
                dataEntryGrid.clearErrors();
            }
        }
        var onFailure=function(response){
            alert('fail'); 
            return false;
        };

        var callback={
            success:onSuccess,
            failure:onFailure
        }
        YAHOO.util.Connect.asyncRequest('POST',ajaxURL,callback,'value='+postStr);   
    }
}
            

PageDispatcher.prototype.initEvents=function(){
    this.addEvent({
        eventName:'buttonClicked',
        requiredParameters:['elementName'],    
        rules:{
            'addSample':addSample,
            'createMissingSamples':createMissingSamples,
            'lookupSamples':lookupSamples,
            'updateSamples':updateSamples,
            'removeRow':removeRow,
            'submitLibraryData' : submitLibraryData,               
            'openSampleSearch': openSampleSearch,            
            'closeSampleSearch':function(message){
                // close when the cancel button is pressed
                if(message.elementName===DispatchObj.selectPopupId+'_cancel'){
                    var actionMsg={
                        action:'hidePopup',
                        elementName: DispatchObj.selectPopupId                            
                    };
                    DispatchObj.dispatch(actionMsg);
                }
            },

            'selectSampleData':function(message){
                // when the select sample button is pressed get the data and add it to the list
                if(message.elementName===DispatchObj.selectPopupId+'_select'){
                    var selectPopupGridObj=window[DispatchObj.selectPopupGridId];
                    var dataEntryGrid=window[DispatchObj.dataEntryGridId];
                    var data=selectPopupGridObj.getSelectedRows();                
                    var key,oldKey,oldRow,newRow,newKey;
                    // convert from datagrid control data format to dataentry row data format.
                    for(key in data){                    
                        oldRow=data[key];
                        newRow={};
                        for(oldKey in oldRow){                        
                            newKey=oldKey.split('_')[1];
                            newRow[newKey]=oldRow[oldKey];
                        }
                        dataEntryGrid.addRow(newRow);
                    }            
                    var eventMsg={
                        event:'buttonClicked',
                        elementName:dataEntryGrid.id+"_lookupSamplesButton"
                    }
                    DispatchObj.dispatch(eventMsg)                                           
                    
                    var actionMsg={
                        action:'hidePopup',
                        elementName: DispatchObj.selectPopupId                            
                    };
                    selectPopupGridObj.unCheckAll();
                    DispatchObj.dispatch(actionMsg);
                }                    
            }
        }
    });
}
