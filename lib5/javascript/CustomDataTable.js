(function() {
    
    msgs = inputEx.messages;
    
    inputEx.widget.CustomDataTable = function (options){
        inputEx.widget.CustomDataTable.superclass.constructor.call(this,options)
        this.checkedRows={};
        this.checkedAll=false;
        this.formMode=null;
        this.activeFormId=null;
        this.dragDropRows=false;
        
    }
    
    YAHOO.lang.extend(inputEx.widget.CustomDataTable,inputEx.widget.DataTable,{
        
        setOptions:function(options)
        {
            inputEx.widget.CustomDataTable.superclass.setOptions.call(this,options);
            this.baseURL=options.baseURL;
            this.keys=options.keys;
            this.addFields=options.addFields;
            this.editFields=options.editFields;
            this.addFormId=options.addFormId;
            this.editFormId=options.editFormId;
            this.showCheckBoxColumn=options.showCheckBoxColumn;
            this.datasourceId=options.datasourceId;
            //this.dragDropRows=options.dragDropRows;
        },
        
        clearErrorMessages:function()
        {
            var form=this.dialog.getForm();
            var formNode= YAHOO.util.Dom.get(form.parentEl);
            
            var invalidNodes=YAHOO.util.Selector.query('.inputEx-fieldWrapper.invalid',formNode);

            var i=0;

            for(i;i<invalidNodes.length;i++)
            {
                YAHOO.util.Dom.removeClass(invalidNodes[i],'invalid');

                var selector='#' + invalidNodes[i].id + ' .error-message';

                var msgNode=YAHOO.util.Selector.query(selector,form.parentEl,true);
                msgNode.parentNode.removeChild(msgNode);
            }   
        },
        
        showErrorMessages:function(errors)
        {
            var key;

            var form=this.dialog.getForm();
            for(key in errors)
            {
                var field=form.getFieldByName(key);
                YAHOO.util.Dom.addClass(field.divEl,'invalid');
                
                
                var msgEl = inputEx.cn('div', {className: 'error-message'});
                  try{
                     var divElements = field.divEl.getElementsByTagName('div');
                     field.divEl.insertBefore(msgEl, divElements[(divElements.length-1>=0)?divElements.length-1:0]); //insertBefore the clear:both div
                  }catch(e){alert(e);}

                msgEl.innerHTML=errors[key];
            }  
        },
        
        onDialogSave: function()
        {
            var newvalues,record;
            
            var values=this.dialog.getValue();
            var json_values=YAHOO.lang.JSON.stringify(values);
            var postData='value=' + json_values;
            var ajaxURL=this.baseURL+"&actionName=validateData";
            ajaxURL+="&mode=" + this.formMode;
            ajaxURL+="&controllerId=" + this.activeFormId;

            var onSuccess=function(response){

            this.clearErrorMessages();
            var results=YAHOO.lang.JSON.parse(response.responseText);
            
            this.recordIndex=this.selectedRecord;
            this.selectedRecord=this.datatable.getTrEl(this.selectedRecord);

            if(!YAHOO.lang.isUndefined(results.errorMessages))
            {
               var errors=results.errorMessages;
               this.showErrorMessages(errors);
            }
            else
            {
                this.refreshData();
               // Update the record
                if(!this.insertNewRecord){

                    // Fire the modify event
                    this.itemModifiedEvt.fire(record);

               }
                // Adding new record
                else{
                    
                    this.itemAddedEvt.fire(record);
                }
                this.dialog.hide()
            }
        }
    
        var onFailure=function(response){
            return false
        };


        var callback={success:onSuccess,
                      failure:onFailure,
                      scope:this};

        YAHOO.util.Connect.asyncRequest('POST',ajaxURL,callback,postData);
    },
    
    /**
	 * Set the column definitions, create them if none from the fields, adds the modify and delete buttons
     * to left of the data grid
	 */
	setColumnDefs: function() {
        
        
        
        var columndefs=[];
        
        if(this.showCheckBoxColumn ) {
           columndefs = columndefs.concat([{
              key:'checkBoxColumn',
              label:'Select Rows',
              formatter:function(el, oRecord, oColumn, oData) {
                
                var recKey=this.getRecordKey(oRecord);
                if(YAHOO.lang.isUndefined(this.checkedRows[recKey]) || this.checkedRows===false){
                    checked="";
                }
                else{
                    checked="checked='checked'";
                }
                
                el.innerHTML = "<input type='checkbox'" + checked +
                        " class='" + this.CLASS_CHECKBOX + "' />";
              }
              
         }]);
        }
        
        
        // Adding modify column if we use form editing and if allowModify is true
        if(this.options.allowModify ) {
           columndefs = columndefs.concat([{
              key:'modify',
              label:'Edit Row',
              formatter:function(elCell) {
               elCell.innerHTML = msgs.modifyText;
               elCell.style.cursor = 'pointer';
            }
         }]);
        }

        // Adding delete column
        if(this.options.allowDelete) {
         columndefs = columndefs.concat([{
            key:'delete',
            label:'Delete Row',
            formatter:function(elCell) {
               elCell.innerHTML = msgs.deleteText;
               elCell.style.cursor = 'pointer';
            }
         }]);
        }
        
		columndefs = columndefs.concat(this.options.columnDefs || this.fieldsToColumndefs(this.options.fields));

		return columndefs;
	},
    
    /**
    * Render the dialog for row edition
    */
   renderModifyDialog: function() {
      
     var that = this;
      
     this.dialog = new inputEx.widget.Dialog({
				id: this.options.dialogId,
				inputExDef: {
				        type: 'form',
			            fields: this.editFields,
			            buttons: [
			               {type: 'submit', value: msgs.saveText, onClick: function() { that.onDialogSave(); return false; /* prevent form submit */} },
			               {type: 'link', value: msgs.cancelText, onClick: function() { that.onDialogCancel(); } }
			            ]
				      },
				title: "Edit Row",
				panelConfig: this.options.panelConfig
		});
        
        
		// Add a listener on the closing button and hook it to onDialogCancel()
		YAHOO.util.Event.addListener(that.dialog.close,"click",function(){
			that.onDialogCancel();
		},that);
		
   },
   
   /**
    * Render the dialog for row edition
    */
   renderInsertDialog: function() {
      
     var that = this;
     this.activeFormId=this.addFormId;
     
     this.dialog = new inputEx.widget.Dialog({
				id: this.options.dialogId,
				inputExDef: {
				         type: 'form',
			            fields: this.addFields,
			            buttons: [
			               {type: 'submit', value: msgs.saveText, onClick: function() { that.onDialogSave(); return false; /* prevent form submit */} },
			               {type: 'link', value: msgs.cancelText, onClick: function() { that.onDialogCancel(); } }
			            ]
				      },
				title: 'Add New Row',
				panelConfig: this.options.panelConfig
		});
	
        
		// Add a listener on the closing button and hook it to onDialogCancel()
		YAHOO.util.Event.addListener(that.dialog.close,"click",function(){
			that.onDialogCancel();
		},that);   
		
   },
    
    addResizeToDialog:function()
    {
        this.resize = new YAHOO.util.Resize(this.dialog.id,{
            handles:["br"],
            status:false
        });
        
        this.resize.on('resize', function(args) {
            var panelHeight = args.height;
            this.cfg.setProperty("height", panelHeight + "px");
            }, this.dialog, true)
    },
    
    onClickModify:function(rowIndex){
      
        this.renderModifyDialog();
        this.formMode='edit';
        this.activeFormId=this.editFormId;
       
        // NOT Inserting new record
		this.insertNewRecord = false;
		
		// Set the selected Record
		this.selectedRecord = rowIndex;
		
		// Get the selected Record
		var record = this.datatable.getRecord(this.selectedRecord);
        
        var keyValues={};
        var key;
        
        for(key in this.keys)
        {
            keyValues[key]=record.getData(key);
        }
        
        var keysVal=YAHOO.lang.JSON.stringify(keyValues);
        
        var ajaxURL=this.baseURL+"&actionName=getRow&keys="+keysVal;
        ajaxURL+="&controllerId="+this.datasourceId;
        
        var onSuccess=function(response)
        {
            var results=YAHOO.lang.JSON.parse(response.responseText);
            var row = results.data;

            // populate the hidden fields.
            
            var newkey;
            
            for(key in this.keys)
            {
                newkey = "original-" + key;
                row[newkey] = row[key];
            }

            this.dialog.setValue(row);
            
            this.dialog.show();
        }

        var onFailure=function(response)
        {
            alert("fail");
            this.dialog.show();
        }

        var callback={
                    success:onSuccess,
                    failure:onFailure,
                    scope:this
                    };
        
        this.dialog.whenFormAvailable({
            fn:function(){
                this.dialog.setFooter("");
                this.setDialogHeight();
                this.addResizeToDialog();
                
                YAHOO.util.Connect.asyncRequest('GET',ajaxURL,callback);
            },
            scope:this
        });
           
    },
    
    /**
    * Insert button event handler
    */
   onInsertButton: function(e) {
        this.formMode='add';
        this.renderInsertDialog();
      
		// Inserting new record
		this.insertNewRecord = true;
		
		this.dialog.whenFormAvailable({
			fn: function() {
              
                this.dialog.setFooter("");
                this.setDialogHeight();
                this.addResizeToDialog();
                this.dialog.getForm().clear();
                this.dialog.show();	
			},
			scope: this
		});
		
   },
   
   onCheckBoxClick: function(oArgs){
       var elCheckbox =oArgs.target;
       var record= this.getRecord(elCheckbox);
       
       var key=this.getRecordKey(record);
       
       if(elCheckbox.checked === true){
           this.checkedRows[key]=record;
       }
       else{
           this.checkedRows[key]=false;
       }    
   },
   
   renderDatatable:function(){
       inputEx.widget.CustomDataTable.superclass.renderDatatable.call(this);
       
       
       // these properties must be set in the datatable in order for the custom formatter to work
       this.datatable.keys=this.keys;
       this.datatable.getRecordKey=this.getRecordKey
       this.datatable.checkedRows=this.checkedRows;
       this.datatable.checkAll=this.checkAll;
        
        
       if(this.showCheckBoxColumn){
            this.datatable.subscribe("checkboxClickEvent",this.onCheckBoxClick,scope=this)
       }
       
       if(this.dragDropRows){
           this.datatable.DTDTargets={};
           
           var onRowSelect= function(ev) {
               
               var dataTable=this;
               
               var Dom = YAHOO.util.Dom,
               Event = YAHOO.util.Event,
               DDM = YAHOO.util.DragDropMgr
               
               
                var par = this.getTrEl(Event.getTarget(ev)),
                    srcData,
                    srcIndex,
                    tmpIndex = null,
                    ddRow = new YAHOO.util.DDProxy(par.id),
                    srcEl,
                    proxyEl;

                ddRow.handleMouseDown(ev.event);


                    /**
                    * Once we start dragging a row, we make the proxyEl look like the src Element. We get also cache all the data related to the
                    * @return void
                    * @static
                    * @method startDrag
                    */
                    ddRow.startDrag = function () {
                        proxyEl  = this.getDragEl();
                        srcEl = this.getEl();
                        srcData = dataTable.getRecord(srcEl).getData();
                        srcIndex = srcEl.sectionRowIndex;
                        // Make the proxy look like the source element
                        Dom.setStyle(srcEl, "visibility", "hidden");
                        proxyEl.innerHTML = "<table><tbody>"+srcEl.innerHTML+"</tbody></table>";
                    };

                    /**
                    * Once we end dragging a row, we swap the proxy with the real element.
                    * @param x : The x Coordinate
                    * @param y : The y Coordinate
                    * @return void
                    * @static
                    * @method endDrag
                    */
                    ddRow.endDrag = function(x,y) {
                        Dom.setStyle(proxyEl, "visibility", "hidden");
                        Dom.setStyle(srcEl, "visibility", "");
                    };


                    /**
                    * This is the function that does the trick of swapping one row with another.
                    * @param e : The drag event
                    * @param id : The id of the row being dragged
                    * @return void
                    * @static
                    * @method onDragOver
                    */
                   
                   ddRow.onDragOver=function(e,id){
                        var destDD = YAHOO.util.DragDropMgr.getDDById(id);
                        // Only if dropping on a valid target
                        if(destDD && destDD.isTarget && this.srcEl) {
                            var	srcEl = this.srcEl,
                                srcIndex = srcEl.sectionRowIndex,
                                destEl = Dom.get(id),
                                destIndex = destEl.sectionRowIndex,
                                srcData = dataTable.getRecord(srcEl).getData();

                            this.srcEl = null;

                            // Cleanup existing Drag instance
                            dataTable.DTDTargets[srcEl.id].unreg();
                            delete myDTDrags[srcEl.id];

                            // Move the row to its new position
                            dataTable.deleteRow(srcIndex);
                            dataTable.addRow(srcData, destIndex);
                            YAHOO.util.DragDropMgr.refreshCache();
                        }
                    };
            };
                   
                 
            
            this.datatable.subscribe('cellMousedownEvent', onRowSelect);
            
             //////////////////////////////////////////////////////////////////////////////
        // Create DDTarget instances when DataTable is initialized
        //////////////////////////////////////////////////////////////////////////////
        this.datatable.subscribe("initEvent", function() {

            var i, id,
                allRows = this.getTbodyEl().rows;


            for(i=0; i<allRows.length; i++) {
                id = allRows[i].id;
                // Clean up any existing Drag instances
                if (this.DTDTargets[id]) {
                     this.DTDTargets[id].unreg();
                     delete this.DTDTargets[id];
                }
                // Create a Drag instance for each row
                this.DTDTargets[id] = new YAHOO.util.DDTarget(id);
            }
        });

        //////////////////////////////////////////////////////////////////////////////
        // Create DDTarget instances when new row is added
        //////////////////////////////////////////////////////////////////////////////
        this.datatable.subscribe("rowAddEvent",function(e){
            var id = e.record.getId();

            this.DTDTargets[id] = new YAHOO.util.DDTarget(id);
        });
           
       }
   },
   
   getRecordKey:function(record){
        var key;
        var values=[];

        for(key in this.keys){
           values.push(record.getData(key))
        }

        var recKey=values.join("_");
        return recKey
    },
    
    getSelectedRows:function(){
        var selectedRows={};
        var key;
        for(key in this.checkedRows){
            if(this.checkedRows){
                selectedRows[key]=this.checkedRows[key].getData();
            }
        }
        return selectedRows;
    },
    
    setDialogHeight: function(){
        var viewportHeight=YAHOO.util.Dom.getViewportHeight();
        
        if(this.dialog.fitsInViewport()===false){
            var dialogHeight=(viewportHeight-100) + 'px';
            this.dialog.cfg.setProperty("height",dialogHeight);   
        }
        else{
            this.dialog.cfg.setProperty("height","");
        }
            
    },
    
    refreshData: function(whereStatement){
        
        if(whereStatement){
            this.datatable.whereStatement=whereStatement;
        }
        else{
            this.datatable.whereStatement=null;
        }
        var oState=this.datatable.getState();
        
        var oCallback={
            success: this.datatable.onDataReturnSetRows,
            failure: this.datatable.onDataReturnSetRows,
            argument: oState,
            scope: this.datatable
        }
        
        var request=this.datatable.get("generateRequest")(oState,this.datatable);
        
        this.datatable.getDataSource().sendRequest(request,oCallback);
    }
  
    });
    
    msgs.saveText = "Save";
    msgs.cancelText = "Cancel";
    msgs.deleteText = "delete";
    msgs.modifyText = "edit";
    msgs.insertItemText = "Add Row";
    msgs.addButtonText = "Add";
    msgs.loadingText = "Loading...";
    msgs.emptyDataText = "No records found.";
    msgs.errorDataText = "Data error.";
    msgs.confirmDeletion = "Are you sure?";

    msgs.tableOptions = "Show/Hide Columns";
    msgs.showColumnButton = "Show";
    msgs.hideColumnButton = "Hide";
    msgs.columnDialogTitle = "Choose which columns you would like to see";
    msgs.columnDialogCloseButton = "Close";
})();