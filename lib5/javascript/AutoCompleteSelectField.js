(function(){
   
   inputEx.AutoCompleteSelectField=function(options){
      inputEx.AutoCompleteSelectField.superclass.constructor.call(this,options);
   }
   
   YAHOO.lang.extend(inputEx.AutoCompleteSelectField,inputEx.CombineField,{
       
        // buildSelect field and buildTheAutoComplete field for the first option.
        setOptions: function(options){
            this.fieldList=YAHOO.lang.isUndefined(options.fieldList) ? [] : options.fieldList;
            var selectFieldConfig=this.buildSelectFieldConfig(options.fieldList);
            
            this.autoComp=options.autoComp;
            
            var firstField=this.fieldList[0];
            var autoCompleteConfig=this.buildAutoCompleteFieldConfig(firstField.fieldName,firstField.datasource);
            
            var fieldList=[autoCompleteConfig,selectFieldConfig];
            
            var newOptions={
                fields:fieldList
            };
            
            YAHOO.lang.augmentObject(newOptions,options);
            
            inputEx.AutoCompleteSelectField.superclass.setOptions.call(this,newOptions);
            
        },
        
        initEvents:function(){
            inputEx.AutoCompleteSelectField.superclass.initEvents.call(this);
            this.inputs[1].updatedEvt.subscribe(this.onSelectFieldChange,this,true);
        },
        
        // build inputEx Select field config
        buildSelectFieldConfig: function (fieldList){
            var choices=[];
            for(var i=0; i<fieldList.length;i++){
                choices.push({'label':fieldList[i].fieldName,'value':[fieldList[i].fieldName,fieldList[i].datasource]});
            }
            
            return{type:'select',choices:choices};
        },
        
        
        // build inputEx AutoComplete field Config.
        buildAutoCompleteFieldConfig: function (fieldName,datasource){
            var fieldConfig={}
            fieldConfig.type="autocomplete";
            fieldConfig.datasource=datasource;
            fieldConfig.fieldName=fieldName;
            
            var newAutocomp={};
            newAutocomp.generateRequest=function(sQuery){
                    return "&fieldId=" + fieldName + "&query=" + sQuery ;
            }
            YAHOO.lang.augmentObject(newAutocomp,this.autoComp);
            fieldConfig.autoComp=newAutocomp;
            
            return fieldConfig;
            
        },
        
        onSelectFieldChange: function(e,params){
            var value=params[0];
            var fieldName=value[0];
            var datasource=value[1];
            
            
            var oldAutoField=this.inputs[0];
            var numChildren=this.divEl.childNodes.length;
            var selectBoxEl=this.divEl.childNodes[numChildren-2]
            oldAutoField.destroy();
            this.inputs.shift()
            
            var fieldConfig=this.buildAutoCompleteFieldConfig(fieldName,datasource);
            var newAutoField=this.renderField(fieldConfig);
            
            var tmp=this.inputs[0];
            
            this.inputs[0]=this.inputs[1];
            this.inputs[1]=tmp;
            
            var newAutoFieldEl=newAutoField.getEl();
            
            YAHOO.util.Dom.setStyle(newAutoFieldEl, 'display', 'inline-block');
            this.divEl.insertBefore(newAutoFieldEl, selectBoxEl);
            
            
        }
   
   });
   
   inputEx.registerType("AutoCompleteSelect",inputEx.AutoCompleteSelectField,{});
   
})();