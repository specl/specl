<?php 

require_once("SQLWhere.php");
require_once("Start2.php");

autoloadAddPath("../seqlims3/");
autoloadAddPath("../seqlims3/WebPages/");
autoloadAddPath("../seqlims3/WebServices/");
autoloadAddPath("../seqlims3/Controllers/", ADDTOP);
autoloadAddPath("../seqlims3/Viewers/", ADDTOP);
autoloadAddPath("../seqlims3/CodeGen/", ADDTOP);
autoloadAddPath("../seqlims3/Specs/", ADDTOP);

$app=new LIMSApp();

$whereStatement=new SQLWhere();

$whereStatement->addClause(new SQLComparison("sampleName","like","%22%"));
$whereStatement->addClause(new SQLBetween("concentration","1","10"));
$whereStatement->addClause(new SQLIsNull(("well")));

$app->specManager->loadSpecDef("Sample");

$objDef=$app->specManager->findDef("Sample");

$SQLBuilder=new SQLBuilder($app->getConnection(), $app->specManager);

$qs=$SQLBuilder->genGetByWhere($objDef, $whereStatement);

echo $qs->sql;

?>