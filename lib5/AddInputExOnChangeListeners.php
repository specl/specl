<?php

class AddInputExOnChangeListeners extends WorkFlowComponent{
    public $url;
    
    public function __construct($params){
        if($params["url"]){
            $this->url=$params["url"];
        }
    }
    
    public function execute($input){
        $objDef=$input["objDef"];
        $strbld=$input["strbld"];
        $formName=$input["formName"];
        
        if (isnull($objDef)) {
            return $strbld;
        }
        if(count($objDef->getAttribute("addOnChangeListener",array()))==0){
            return $strbld;
        }
        else{
            foreach($objDef->getAttribute("addOnChangeListener") as $fieldName){
                $strbld->addLines(
"
    <script>
        //on load store the orginal value of the form in a field variable.
        YAHOO.util.Event.addListener(window,'load',function(){
            var field={$formName}.getFieldByName('{$fieldName}');
            field.originalValue=field.getValue();
        });                        
        // attach the onChange listener to {$fieldName}
        var field={$formName}.getFieldByName('{$fieldName}');
        field.updatedEvt.subscribe(function(e,params){
            var newVal=params[0];
            var oldValue=field.originalValue;
            field.originalValue=newVal;            
            var url='{$this->url}';
            var onSuccess=function(response){
                var data=YAHOO.lang.JSON.parse(response.responseText);
                {$formName}.updateForm(data);
            };            
            var onFailure=function(){alert('failed to connect to server')};
            
            var postData={'oldValue':oldValue,'newValue':newVal,'fieldName':'{$fieldName}'};
            var postString='data='+YAHOO.lang.JSON.stringify(postData);
            var callback={success:onSuccess,failure:onFailure}; 
            YAHOO.util.Connect.asyncRequest('POST',url,callback,postString);
        });
    </script>
");
            }
        }
        
        return $strbld;
    }
    
    
}

?>