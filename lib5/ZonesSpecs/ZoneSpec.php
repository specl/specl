<?php
class ZoneSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("Zone");

        $objDef->addConstructorParam("id");

        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createCollection("zones","Zones");
        $prop = $objDef->createCollection("controllers","Controllers");

        
        $objDef->addRequiredProperty("id");
        $objDef->addKey("id");

        $spec->addDef($objDef);
    }
}
?>
