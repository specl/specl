<?php
class OptionSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("Option");
        $prop = $objDef->createProperty("key", "string");
        $prop = $objDef->createProperty("value", "string");
        $objDef->addRequiredProperty("key");
        $objDef->addRequiredProperty("value");
        
        $objDef->addKey("key");
        $spec->addDef($objDef);
    }
}

?>
