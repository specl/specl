<?php

class DataGridColumnsSpec extends SpecDef {
    public function defSpec($spec){
        $colDef=new CollectionDef("DataGridColumns",false);
        $colDef->addChildType("DataGridColumn");
        $spec->addDef($colDef);
    }
}
?>
