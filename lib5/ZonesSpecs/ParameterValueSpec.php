<?php
class ParameterValueSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("ParameterValue");
        $prop = $objDef->createProperty("key", "string");
        $prop = $objDef->createProperty("value", "string");
        
        $objDef->addRequiredProperty("key");
        $objDef->addKey("key");

        $objDef->addRequiredProperty("value");
        
        
        $spec->addDef($objDef);
    }
}
?>
