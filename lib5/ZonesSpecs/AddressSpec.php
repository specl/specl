<?php
class AddressSpec extends SpecDef {

    
    public function defSpec($spec) {
        $objDef = new ObjDef("Address");
        $prop = $objDef->createProperty("ownOrRent", "boolean");
        $prop = $objDef->createProperty("street1", "string");
        $prop = $objDef->createProperty("street2", "string");
        $prop = $objDef->createProperty("aptNo", "string");
        $prop = $objDef->createProperty("city", "string");
        $prop = $objDef->createProperty("county", "string");
        $prop = $objDef->createProperty("state", "string");
        $prop = $objDef->createProperty("zip", "string");
        $prop = $objDef->createProperty("country", "string");
        $spec->addDef($objDef);
    }
}

?>