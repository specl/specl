<?php

class PageSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("Page");

        $prop = $objDef->createProperty("id", "string");
        $prop->setSetter('setId');

        $prop = $objDef->createProperty("title", "string");
        $prop = $objDef->createProperty("description", "string");
        $prop = $objDef->createProperty("helpId", "string");
        $prop = $objDef->createProperty("path", "string");
        $prop->setSetter('setPath');

        $prop = $objDef->createProperty("showInState", "string");

        $prop = $objDef->createProperty("showWhenLoggedIn", "boolean");
        $prop = $objDef->createProperty("showInNav", "boolean");
        $prop = $objDef->createProperty("allowDirectAccess", "boolean");


        $prop = $objDef->createCollection("pageControllers","PageControllers");

        $prop = $objDef->createCollection("zones","Zones");
        $prop = $objDef->createCollection("roles","Roles");
        $prop = $objDef->createCollection("parameters","Parameters");

        $prop = $objDef->createCollection("showInNavWhen","ShowInNavWhen");

        $prop = $objDef->createProperty("pageController", "string");
        $prop = $objDef->createProperty("pageActions", "string");
        $prop = $objDef->createProperty("includeDojo", "boolean");
        $prop = $objDef->createProperty("resourceId", "string");
        $prop = $objDef->createProperty("controllerId", "string");
        $prop = $objDef->createProperty("actionName", "string");
        $prop = $objDef->createProperty("aco", "string");
        $prop = $objDef->createProperty("acu", "string");
        $prop = $objDef->createProperty("acr", "string");
        $prop = $objDef->createProperty("acf", "string");
        $prop = $objDef->createProperty("act", "string");
        $prop = $objDef->createProperty("showInSessionState", "string");
        
        // Added so each page can have a link to a help topic        
        $prop = $objDef->createProperty("helpTopicId", "string");        
        $prop = $objDef->createCollection("parameterValues","ParameterValues");
    
        
        $objDef->addConstructorParam("id");
        $objDef->addRequiredProperty("id");
        $objDef->addKey("id");

        $spec->addDef($objDef);
    }
}

?>
