<?php
class ConnectionSpec extends SpecDef {

    public function defSpec($spec) {
        $connectionDef = new ObjDef("Connection");
        $prop = $connectionDef->createProperty("id", "string");
        $prop = $connectionDef->createProperty("user", "string");
        $prop = $connectionDef->createProperty("host", "string");
        $prop = $connectionDef->createProperty("port", "numeric");
        $prop = $connectionDef->createProperty("pass", "string");
        $prop = $connectionDef->createProperty("schema", "string");
        $prop = $connectionDef->createProperty("prefix", "string");

        $connectionDef->addRequiredProperty("id");
        $connectionDef->addRequiredProperty("user");
        $connectionDef->addRequiredProperty("pass");
        $connectionDef->addRequiredProperty("host");
        $connectionDef->addRequiredProperty("schema");
        $connectionDef->addOptionalProperty("port", 3306);
        $connectionDef->addKey("id");

        $spec->addDef($connectionDef);
    }
}

?>
