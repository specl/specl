<?php
class RegisterMessageSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("RegisterMessage");

        // RegisterMessage/username
        $prop = $objDef->createProperty("username", "string");
        $prop->addCondition(new NotEmptyCondition());
        $prop->addCondition(new StringMinLength(3));
        $prop->addCondition(new StringMaxLengthCondition(20));
        $prop->addCondition(new UserAccountUsernameExists(false));  // username must not exist

        $prop = $objDef->createProperty("password", "string");
        $prop->addCondition(new NotEmptyCondition());
        $prop->addCondition(new StringMinLength(3));
        $prop->addCondition(new StringMaxLengthCondition(20));

        $prop = $objDef->createProperty("verifyPassword", "string");
        $prop->addCondition(new NotEmptyCondition());
        $prop->addCondition(new StringMinLength(3));
        $prop->addCondition(new StringMaxLengthCondition(20));

        $prop = $objDef->createProperty("email", "string");
        $prop->addCondition(new NotEmptyCondition());
        $prop->addCondition(new StringMaxLengthCondition(128));
        $prop->addCondition(new ValidEmail());
        $prop->addCondition(new UserAccountEmailExists(false));  // email must not exist


        $prop = $objDef->createProperty("verifyEmail", "string");
        $prop->addCondition(new NotEmptyCondition());
        $prop->addCondition(new StringMaxLengthCondition(128));

        
        // Define object level conditions

        $objDef->addRequiredProperty("username");
        $objDef->addRequiredProperty("password");
        $objDef->addRequiredProperty("verifyPassword");
        $objDef->addRequiredProperty("email");
        $objDef->addRequiredProperty("verifyEmail");

                
        $objDef->addCondition(
                 new PropValuesMustBeEqual($objDef->getProperty("password"),
                                           $objDef->getProperty("verifyPassword"))
                             );

        $objDef->addCondition(
                 new PropValuesMustBeEqual($objDef->getProperty("email"),
                                           $objDef->getProperty("verifyEmail"))
                             );

        $spec->addDef($objDef);


    }
}
?>
