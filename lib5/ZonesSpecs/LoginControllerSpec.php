<?php
class LoginControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("LoginController");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
