<?php
class RoleSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("Role");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addRequiredProperty("id");
        $objDef->addKey("id");
        $spec->addDef($objDef);
    }
}

?>
