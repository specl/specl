<?php
/**
 * Description of CallFunctionActionSpec
 *
 * @author tony
 */
class CallFunctionActionSpec extends SpecDef {
   public function defSpec($spec) {
        $objDef = new ObjDef("CallFunctionAction");
        $prop = $objDef->createProperty("method", "Method");

        $prop = $objDef->createProperty("onFailure", "OnFailure");
        $prop = $objDef->createProperty("onSuccess", "OnSuccess");
        $prop = $objDef->createProperty("onRetryLimitExceeded", "OnRetryLimitExceeded");

        $objDef->addRequiredProperty("method");

        $spec->addDef($objDef);
    }
}
?>
