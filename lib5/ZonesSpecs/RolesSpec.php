<?php
class RolesSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Roles", true);
        $colDef->addChildType("Role");
        $spec->addDef($colDef);
    }
}
?>
