<?php
class DocumentSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("Document");
        $prop = $objDef->createProperty("id", "auto");
        $prop = $objDef->createProperty("title", "string");
        $prop = $objDef->createProperty("source", "string");
        $prop = $objDef->createProperty("content", "string");
        $objDef->addKey("Id");        
        $spec->addDef($objDef);
    }
}
?>
