<?php
class AttributesSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef(array("id"=>"Attributes", "isAssociative"=>true));
        $colDef->addChildType("Attribute");       
        $spec->addDef($colDef);
    }
}
?>
