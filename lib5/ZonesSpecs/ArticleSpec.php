<?php

class ArticleSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("Article");

        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("title", "string");
        $prop = $objDef->createProperty("createdBy", "string");
        $prop = $objDef->createProperty("createdOn", "string");
        $prop = $objDef->createProperty("changedOn", "string");
        $prop = $objDef->createProperty("content", "string");
    
        $objDef->addConstructorParam("id");
        $objDef->addRequiredProperty("id");
        $objDef->addKey("id");
        
        $spec->addDef($objDef);
    }
}

?>
