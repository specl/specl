<?php
class ControllersSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Controllers", false);

        // this is silly, this list of allowable child types
        // could be done at runtime.

        //$colDef->setAllowAllChildren(true);

        $colDef->addChildType("Controller");
        $colDef->addChildType("FormController");
        $colDef->addChildType("DocumentController");
        $colDef->addChildType("InspectusController");
        $colDef->addChildType("DataGridController");
        $colDef->addChildType("DataGridController2");
        $colDef->addChildType("LoginController");
        $colDef->addChildType("RegisterController");
        $colDef->addChildType("LoginReminderController");
        $colDef->addChildType("ChangePasswordController");
        $colDef->addChildType("LogoutController");
        $colDef->addChildType("SearchFormController");
        $colDef->addChildType("SearchFormControllerTony");
        $colDef->addChildType("LabUsersController");
        $colDef->addChildType("UserSamplesController");
        $colDef->addChildType("IncludeController");
        $colDef->addChildType("ModelViewerController");
        $colDef->addChildType("WorkflowController");
        $colDef->addChildType("FileUploadController");


        $spec->addDef($colDef);
    }    
}
?>
