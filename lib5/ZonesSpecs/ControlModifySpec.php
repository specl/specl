<?php
/**
 * Description of ControlModifySpec
 *
 * @author tony
 */
class ControlModifySpec extends SpecDef {
   public function defSpec($spec) {
        $objDef = new ObjDef("ControlModify");

        $prop = $objDef->createProperty("id", "string");

        $prop = $objDef->createCollection("keyValuePairs", "KeyValuePairs");


        $objDef->addRequiredProperty("id");

        //$prop = $objDef->createProperty("caption", "string");
        //$prop = $objDef->createProperty("visible", "boolean");

        $spec->addDef($objDef);
    }
}
?>
