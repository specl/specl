<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoadPageActionSpec
 *
 * @author tony
 */
class LoadPageActionSpec extends SpecDef {
   public function defSpec($spec) {
        $objDef = new ObjDef("LoadPageAction");

        $prop = $objDef->createProperty("pageId", "string");

        $spec->addDef($objDef);
    }
}
?>
