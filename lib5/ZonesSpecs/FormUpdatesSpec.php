<?php
/**
 * Description of FormUpdatesSpec
 *
 * @author tony
 */
class FormUpdatesSpec extends SpecDef {

    public function defSpec($spec) {
        $colDef = new CollectionDef("FormUpdates", false);
        $colDef->addChildType("ControlModify");

        $spec->addDef($colDef);
    }
}
?>
