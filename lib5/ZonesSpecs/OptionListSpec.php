<?php
class OptionListSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("OptionList", true);
        $colDef->addChildType("Option");
        $colDef->setKeyId("key");
        $spec->addDef($colDef);
    }
}
?>
