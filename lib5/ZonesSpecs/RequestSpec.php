<?php
class RequestSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("Request");
        $prop = $objDef->createProperty("pageId", "string");
        $prop = $objDef->createProperty("controllerId", "string");
        $prop = $objDef->createProperty("requestType", "string");
        $prop = $objDef->createProperty("actionId", "string");

        $prop = $objDef->createProperty("parameters", "dictionary");

        $spec->addDef($objDef);
    }
}

?>