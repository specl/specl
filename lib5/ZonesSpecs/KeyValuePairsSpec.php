<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of KeyValuePairsSpec
 *
 * @author tony
 */

class KeyValuePairsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("KeyValuePairs", true);
        $colDef->addChildType("KeyValue");
        $colDef->setKeyId("key");
        $spec->addDef($colDef);
    }
}
?>
