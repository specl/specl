<?php
class ParameterValuesSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("ParameterValues", true); //it is associative
        $colDef->addChildType("ParameterValue");
        $colDef->setKeyId("key");
        $spec->addDef($colDef);
    }
}
?>
