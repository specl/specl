<?php
class ZonesSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Zones", true);
        $colDef->addChildType("Zone");
        $colDef->setKeyId("id");
        $spec->addDef($colDef);
    }
}
?>
