<?php
class ControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("Controller");
        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("title", "string");
        $prop = $objDef->createProperty("caption", "string");
        $objDef->addKey("Id");
        $objDef->addRequiredProperty("Id");
        $spec->addDef($objDef);
    }
}
?>
