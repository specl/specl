<?php
/**
 * Description of LoadPageActionSpec
 *
 * @author tony
 */
class GenerateFormActionSpec extends SpecDef {
   public function defSpec($spec) {
        $objDef = new ObjDef("GenerateFormAction");

        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("objDefId", "string");

        $prop = $objDef->createCollection("formFields", "FormFields");

        $spec->addDef($objDef);
    }
}
?>
