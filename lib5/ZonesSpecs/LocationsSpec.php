<?php
class LocationsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Locations", false);
        $colDef->addChildType("Location");
        $spec->addDef($colDef);
    }
}
?>
