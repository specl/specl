<?php
class ChildTypesSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef(array("id"=>"ChildTypes", "isAssociative"=>false));
        $colDef->addChildType("ObjDef");
        $spec->addDef($colDef);
    }
}
?>