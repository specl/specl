<?php

class DataGrid2Spec
{
    public function defSpec($spec)
    {
        $objDef= new ObjDef("DataGrid2");
        
        $prop=$objDef->createProperty("objDefId","string");
        $prop=$objDef->createProperty("auto","string");
        $prop=$objDef->createProperty("source","string");
        $prop=$objDef->createProperty("fileName","string");

        
        
        $prop=$objDef->createCollection("dataGridColumns", "DataGridColumns");
        $prop=$objDef->createProperty("tableName","string");
        $prop=$objDef->createProperty("pagingEnabled","boolean");
        $prop=$objDef->createProperty("rowsPerPage","numeric");
        $prop=$objDef->createProperty("ajaxQueryController","string");
        $prop=$objDef->createProperty("showAddColumn","boolean");
        $prop=$objDef->createProperty("showEditColumn","boolean");
        $prop=$objDef->createProperty("showDeleteSelected","boolean");
        $prop=$objDef->createProperty("showHideColumnsLink","boolean");
        $prop=$objDef->createProperty("showAddRow","boolean");

        

        $objDef->createProperty("id","string");
        $objDef->addKey('id');
        
        $objDef->addOptionalProperty("dataGridColumns",null);
        $objDef->addOptionalProperty("tableName",null);
        $objDef->addRequiredProperty("id");
        $objDef->addOptionalProperty("pagingEnabled",true);
        $objDef->addOptionalProperty("rowsPerPage",10);
        $objDef->addOptionalProperty("showAddColumn",false);
        $objDef->addOptionalProperty("showEditColumn",false);
        $objDef->addOptionalProperty("showDeleteSelected",false);
        $objDef->addOptionalProperty("showHideColumnsLink",false);
        $objDef->addOptionalProperty("showAddRow",false);
        
        
        $spec->addDef($objDef);
       
    }
}
?>
