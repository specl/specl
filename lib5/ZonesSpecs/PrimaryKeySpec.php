<?php
class PrimaryKeySpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("PrimaryKey", false);
        $colDef->addChildType("PropDef");
        $spec->addDef($colDef);
    }
}
?>
