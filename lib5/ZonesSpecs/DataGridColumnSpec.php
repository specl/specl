<?php
class DataGridColumnSpec extends SpecDef {
    public function defSpec($spec)
    {
        $objDef=new ObjDef("DataGridColumn");
        $objDef->createProperty("caption","string");
        $objDef->createProperty("visible","boolean");
        $objDef->createProperty("allowFilter","boolean");
        $objDef->createProperty("propDefId","string");
        //$objDef->createProperty("type","string");
        //$objDef->createProperty("label","string");
        $spec->addDef($objDef);

    }
}
?>
