<?php
class LocationSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("Location");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addRequiredProperty("id");
        $objDef->addKey("id");
        $spec->addDef($objDef);
    }
}

?>
