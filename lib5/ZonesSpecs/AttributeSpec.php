<?php
class AttributeSpec extends SpecDef {

    public function defSpec($spec) {
        $def = new ObjDef("Attribute");
        $prop = $def->createProperty("key", "string");
        $prop = $def->createProperty("value", "string");
        $def->addKey("key");        
        $spec->addDef($def);
    }
}

?>
