<?php
class AddressesSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef(array("id"=>"Address", "isAssociative"=>false));
        $colDef->addChildType("Address");
        $spec->addDef($colDef);
    }
}
?>