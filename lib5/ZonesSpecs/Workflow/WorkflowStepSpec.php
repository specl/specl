<?php
/**
 * Description of WorkflowStepSpec
 *
 * @author tony
 */
class WorkflowStepSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("WorkflowStep");
        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("description", "string");
        $prop = $objDef->createCollection("conditions", "WorkflowStepConditions");
        $prop = $objDef->createCollection("actions", "Actions");
        $prop = $objDef->createProperty("createFormFromObjDef", "string");
        $objDef->addKey("id");
        $spec->addDef($objDef);
    }
}
?>
