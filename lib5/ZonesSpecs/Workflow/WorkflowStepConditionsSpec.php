<?php
class WorkflowStepConditionsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("WorkflowStepConditions", false);
        $colDef->addChildType("IsNullCondition");
        $colDef->addChildType("StringVariableCondition");
        $colDef->addChildType("BooleanVariableCondition");
        $colDef->addChildType("LookupVariableCondition");

        $spec->addDef($colDef);
    }
}
?>
