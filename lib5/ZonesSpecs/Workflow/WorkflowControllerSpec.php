<?php


class WorkflowControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("WorkflowController");
        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("workflowId", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
