<?php
class WorkflowSequenceSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("WorkflowSequence", false);
        $colDef->addChildType("WorkflowStep");
        $spec->addDef($colDef);
    }
}
?>
