<?php
/**
 * Description of WorkflowSpec
 *
 * @author Tony
 */
class WorkflowSpec extends SpecDef {
    public function defSpec($spec) {

//        $def = new FormFieldSpec();
//        $def->defSpec($spec);
//
//        $def = new FormFieldsSpec();
//        $def->defSpec($spec);
//
//        $def = new GenerateFormActionSpec();
//        $def->defSpec($spec);

        $def = new EventHandlerSpec();
        $def->defSpec($spec);

        $def = new PropDefsSpec();
        $def->defSpec($spec);

        $def = new FormsSpec();
        $def->defSpec($spec);

        $def = new ObjDefsSpec();
        $def->defSpec($spec);

        $def = new WorkflowStepSpec(); // add this BEFORE defining the collection
                                       // or else the associative arrays do not work.
        $def->defSpec($spec);

        $def = new WorkflowStepsSpec();
        $def->defSpec($spec);

        $def = new WorkflowSequenceSpec();
        $def->defSpec($spec);

        $def = new WorkflowExecutionSpec();
        $def->defSpec($spec);


        $objDef = new ObjDef("Workflow");

        $prop = $objDef->createProperty("id", "string");        
        $prop = $objDef->createProperty("version", "string");        
        $prop = $objDef->createProperty("createdBy", "string");
        $prop = $objDef->createProperty("createdDate", "string");
        $prop = $objDef->createProperty("description", "string");
        $prop = $objDef->createProperty("objDefId", "string");

        $prop = $objDef->createProperty("startingStepId", "string");

        $objDef->createCollection("forms", "Forms");
        $objDef->createCollection("propDefs", "PropDefs");
        $objDef->createCollection("objDefs", "ObjDefs");
        $objDef->createCollection("workflowSteps", "WorkflowSteps");

        $prop = $objDef->createProperty("onBeginWorkflow", "EventHandler");
        $prop = $objDef->createProperty("onCancelWorkflow", "EventHandler");
        $prop = $objDef->createProperty("onCompleteWorkflow", "EventHandler");
        //$prop = $objDef->createProperty("workflowExecution", "WorkflowExecution");



        $spec->addDef($objDef);
    }
}
?>
