<?php
/**
 * Description of WorkflowStepsSpec
 *
 * @author tony
 */
class WorkflowStepsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("WorkflowSteps", true);
        $colDef->addChildType("WorkflowStep");


        $colDef->setKeyId("id");
        $spec->addDef($colDef);
    }
}
?>
