<?php
class RowsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Rows", false);
        $colDef->addChildType("Row");
        $spec->addDef($colDef);
    }
}
?>
