<?php
class TextAreaElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("TextAreaElement");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
