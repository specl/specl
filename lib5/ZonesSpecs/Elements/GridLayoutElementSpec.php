<?php
class GridLayoutElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("GridLayoutElement");
        $prop = $objDef->createProperty("id", "string");
        
        $prop = $objDef->createProperty("columnCount", "numeric");
        $prop = $objDef->createProperty("border", "numeric");

        $prop = $objDef->createCollection("gridRows","GridRows");        

        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
