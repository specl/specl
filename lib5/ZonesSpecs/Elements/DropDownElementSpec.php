<?php
class DropDownElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("DropDownElement");
        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createCollection("options", "OptionList");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
