<?php
class CalendarElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("CalendarElement");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
