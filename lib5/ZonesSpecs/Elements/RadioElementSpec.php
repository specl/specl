<?php
class RadioElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("RadioElement");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
