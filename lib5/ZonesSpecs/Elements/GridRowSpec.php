<?php
class GridRowSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("GridRow", false);
        $colDef->addChildType("GridRowSkip");
        $colDef->addChildType("GridRowStartBunch");
        $colDef->addChildType("GridRowEndBunch");
        $colDef->addChildType("LayoutElement");
        $colDef->addChildType("GridLayoutElement");
        $colDef->addChildType("LabelElement");
        $colDef->addChildType("TextElement");
        $colDef->addChildType("ButtonElement");
        $colDef->addChildType("SubmitButtonElement");
        $colDef->addChildType("CancelButtonElement");
        $colDef->addChildType("ResetButtonElement");
        $colDef->addChildType("DateElement");
        $colDef->addChildType("CalendarElement");
        $colDef->addChildType("ImageElement");
        $colDef->addChildType("ListElement");
        $colDef->addChildType("DropDownElement");
        $colDef->addChildType("TextAreaElement");
        $colDef->addChildType("RadioElement");
        $colDef->addChildType("RadioGroupElement");
        $colDef->addChildType("CheckBoxElement");
        $colDef->addChildType("CheckBoxGroupElement");
        $colDef->addChildType("ImageIndicatorElement");
        $spec->addDef($colDef);
    }
}
?>