<?php
class ImageIndicatorElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("ImageIndicatorElement");

        $prop = $objDef->createProperty("id", "string");
        
        $prop = $objDef->createProperty("filename", "string");

        $prop = $objDef->createProperty("visible", "boolean");

        $prop = $objDef->createProperty("indicatorFor", "string");

        $prop = $objDef->createProperty("caption", "string");

        $prop = $objDef->createCollection("messages","StringList");

        $objDef->addKey("id");

        $objDef->addRequiredProperty("id");

        $spec->addDef($objDef);
    }
}
?>
