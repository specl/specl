<?php


class FormElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("FormElement");
        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("title", "string");
        $prop = $objDef->createProperty("caption", "string");
        $prop = $objDef->createProperty("objDefId", "string");

        $prop = $objDef->createCollection("elements","FormElements");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>