<?php
class DateElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("DateElement");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
