<?php
class FormFieldsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef(array("id"=>"FormFields", "isAssociative"=>false));
        $colDef->addChildType("FormField");
        $spec->addDef($colDef);
    }
}
?>