<?php
class SubmitButtonElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("SubmitButtonElement");
        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("caption", "string");
        $prop = $objDef->createProperty("buttonName", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
