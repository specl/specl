<?php
class CheckBoxGroupElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("CheckBoxGroupElement");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
