<?php
class LabelElementSpec extends SpecDef {
    public function defSpec($spec) {


        $objDef = new ObjDef("LabelElement");

        //$objDef->addMixin("FormElementSpec");


        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("caption", "string");
        $prop = $objDef->createProperty("labelFor", "string");     
        $prop = $objDef->createProperty("visible", "boolean");

        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
