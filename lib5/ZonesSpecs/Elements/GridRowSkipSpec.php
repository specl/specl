<?php
class GridRowSkipSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("GridRowSkip");

        $prop = $objDef->createProperty("columns", "numeric");
        $objDef->addRequiredProperty("columns");
        
        $spec->addDef($objDef);
    }
}
?>