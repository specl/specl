<?php
class ImageElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("ImageElement");
        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("filename", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
