<?php
class GridRowsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("GridRows", false);
        $colDef->addChildType("GridRow");

        $colDef->childIsCollection=true;
        $colDef->collectionTag="GridRow";

        $spec->addDef($colDef);
    }
}
?>
