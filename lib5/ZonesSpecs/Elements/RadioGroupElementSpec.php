<?php
class RadioGroupElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("RadioGroupElement");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
