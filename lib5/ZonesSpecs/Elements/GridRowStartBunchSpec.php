<?php
class GridRowStartBunchSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("GridRowStartBunch");

        $prop = $objDef->createProperty("columns", "numeric");
        $prop = $objDef->createProperty("align", "string");

        //TODO: add the constraints for align CENTER, LEFT, RIGHT, JUSTIFY
        
        $objDef->addRequiredProperty("columns");
        
        $spec->addDef($objDef);
    }
}
?>