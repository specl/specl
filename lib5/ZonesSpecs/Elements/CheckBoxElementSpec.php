<?php
class CheckBoxElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("CheckBoxElement");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
