<?php
class ButtonElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("ButtonElement");
        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("caption", "string");
        
        $prop = $objDef->createProperty("buttonType", "string");
        $prop->setSetter('setButtonType');


        $prop = $objDef->createProperty("buttonName", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
