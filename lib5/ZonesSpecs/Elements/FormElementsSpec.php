<?php
class FormElementsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("FormElements", true);
        $colDef->addChildType("LayoutElement");
        $colDef->addChildType("GridLayoutElement");
        $colDef->addChildType("LabelElement");
        $colDef->addChildType("TextElement");
        $colDef->addChildType("ButtonElement");
        $colDef->addChildType("SubmitButtonElement");
        $colDef->addChildType("CancelButtonElement");
        $colDef->addChildType("ResetButtonElement");
        $colDef->addChildType("DateElement");
        $colDef->addChildType("CalendarElement");
        $colDef->addChildType("ImageElement");
        $colDef->addChildType("ListElement");
        $colDef->addChildType("DropDownElement");
        $colDef->addChildType("TextAreaElement");
        $colDef->addChildType("RadioElement");
        $colDef->addChildType("RadioGroupElement");
        $colDef->addChildType("CheckBoxElement");
        $colDef->addChildType("CheckBoxGroupElement");
        $colDef->addChildType("ImageIndicatorElement");
        $spec->addDef($colDef);
    }
}
?>
