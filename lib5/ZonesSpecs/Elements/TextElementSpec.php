<?php
class TextElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("TextElement");
        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("value", "string");
        $prop = $objDef->createProperty("displayWidth", "numeric");
        $prop = $objDef->createProperty("propDefId", "string");
        $prop = $objDef->createProperty("errorIndicator", "string");

        $prop = $objDef->createProperty("trimInput", "boolean");

//        $prop = $objDef->createProperty("removeTags", "boolean");
//        $prop->setAttribute('comment', "Strip HTML and PHP tags from a input");


        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
