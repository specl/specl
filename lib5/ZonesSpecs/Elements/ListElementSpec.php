<?php
class ListElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("ListElement");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
