<?php
class LayoutElementSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("LayoutElement");
        $prop = $objDef->createProperty("id", "string");
        
        $prop = $objDef->createProperty("rowCount", "numeric");

        $prop = $objDef->createProperty("columnCount", "numeric");

        $prop = $objDef->createCollection("rows","Rows");

        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
