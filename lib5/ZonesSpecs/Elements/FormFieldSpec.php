<?php


class FormFieldSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("FormField");
        $prop = $objDef->createProperty("propDefId", "string");
        $objDef->addKey("propDefId");
        $objDef->addRequiredProperty("propDefId");
        $spec->addDef($objDef);
    }
}
?>