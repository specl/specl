<?php
class ParametersSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Parameters", false);
        $colDef->addChildType("Parameter");
        $spec->addDef($colDef);
    }
}
?>
