<?php
class ConfigSpec extends SpecDef {
    public function defSpec($spec) {
        $def = new CollectionDef("Config", true);
        $def->addChildType("Site");
        $def->addChildType("Pages");
        $def->addChildType("Connections");
        $spec->addDef($def);
    }
}
?>
