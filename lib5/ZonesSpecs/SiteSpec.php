<?php
class SiteSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("Site");
        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("title", "string");
        $prop = $objDef->createProperty("tagLine", "string");
        $prop = $objDef->createProperty("description", "string");
        $prop = $objDef->createProperty("dbPath", "string");
        $prop = $objDef->createProperty("sessionDBPath", "string");
        $prop = $objDef->createProperty("homePage", "string");

        $prop = $objDef->createProperty("templateName", "string");
        $prop = $objDef->createProperty("templatePath", "string");
        $prop = $objDef->createProperty("siteName", "string");
        $prop = $objDef->createProperty("webAddress", "string");
        $prop = $objDef->createProperty("adminEmail", "string");
    
        $prop = $objDef->createProperty("defaultController", "string");
        $prop = $objDef->createProperty("defaultResourceId", "string");
        
        $prop = $objDef->createProperty("Connection", "Connection");
        

        $prop = $objDef->createCollection("PageControllers", "PageControllers");

        $objDef->addRequiredProperty("id");
       
        $objDef->addKey("id");
        
        $spec->addDef($objDef);
    }
}

?>
