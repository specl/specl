<?php
class UserAccountSpec extends SpecDef {
    public function defSpec($spec) {

        $objDef = new ObjDef("UserAccount");
        $objDef->setAttribute('comment', "UserAccount");

        $prop = $objDef->createProperty("userAccountId", "auto");
            $prop->setAttribute('nullAllowed', false);
            $prop->setAttribute('comment', "unique auto id generated on insert");

        $prop = $objDef->createProperty("email", "string");
            $prop->setAttribute('nullAllowed', false);
            $prop->setAttribute('variableLength', 128);
            $prop->setAttribute('comment', 'email address');

        $prop = $objDef->createProperty("username", "string");
            $prop->setAttribute('nullAllowed', false);
            $prop->setAttribute('variableLength', 20);
            $prop->setAttribute('comment', 'unique username');

        $prop = $objDef->createProperty("password", "string");
            $prop->setAttribute('nullAllowed', false);
            $prop->setAttribute('variableLength', 20);
            $prop->setAttribute('comment', 'password stored as plain text');

        $prop = $objDef->createProperty("dateCreated", "date");
            $prop->setAttribute('nullAllowed', false);
            $prop->setAttribute('comment', 'The date this user was created');

        $prop = $objDef->createProperty("active", "boolean");
            $prop->setAttribute('nullAllowed', true);
            $prop->setAttribute('defaultValue', 0);
            $prop->setAttribute('comment', '1 if this username is active, 0 when it is not');

        $objDef->addKey("userAccountId");



        $spec->addDef($objDef);
    }
}
?>
