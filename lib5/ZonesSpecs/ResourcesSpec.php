<?php
class ResourcesSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Resources", true);
        $colDef->addChildType("Resource");
        $colDef->setKeyId("resourceId");
        $spec->addDef($colDef);
    }
}
?>
