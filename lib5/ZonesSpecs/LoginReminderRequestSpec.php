<?php
class LoginReminderRequestSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("LoginReminder");

        $prop = $objDef->createProperty("email", "string");
        $objDef->addRequiredProperty("email");

        $spec->addDef($objDef);
    }
}
?>
