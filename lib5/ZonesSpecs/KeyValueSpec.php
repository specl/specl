<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of KeyValueSpec
 *
 * @author tony
 */
class KeyValueSpec extends SpecDef {

    public function defSpec($spec) {
        $objDef = new ObjDef("KeyValue");
        $prop = $objDef->createProperty("key", "string");
        $prop = $objDef->createProperty("value", "string");
        $objDef->addRequiredProperty("key");
        $objDef->addRequiredProperty("value");

        $objDef->addKey("key");
        $spec->addDef($objDef);
    }
}
?>
