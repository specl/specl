<?php
class ColumnsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Columns", false);
        $colDef->addChildType("Column");
        $spec->addDef($colDef);
    }
}
?>
