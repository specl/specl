<?php
class PagesSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Pages", true);
        $colDef->addChildType("Page");
        $colDef->setKeyId("id");
        $spec->addDef($colDef);
    }
}
?>
