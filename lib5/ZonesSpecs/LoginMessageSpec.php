<?php
class LoginMessageSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("LoginMessage");

        $prop = $objDef->createProperty("username", "string");
        $prop->addCondition(new NotEmptyCondition());

        $prop = $objDef->createProperty("password", "string");
        $prop->addCondition(new NotEmptyCondition());

        // for debugging I added a address collection.
        //$prop = $objDef->createCollection("addresses","Addresses");

        $objDef->addRequiredProperty("username");        
        $objDef->addRequiredProperty("password");

        $spec->addDef($objDef);
    }
}
?>
