<?php
class DocumentControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("DocumentController");

        $objDef->addConstructorParam("id");

        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("documentId", "string");

        $prop = $objDef->createProperty("document", "Document");
        $prop->setSetter('setDocument');


        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");

        $spec->addDef($objDef);
    }
}
?>
