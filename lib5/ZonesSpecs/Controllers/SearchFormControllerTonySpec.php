<?php
class SearchFormControllerTonySpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("SearchFormControllerTony");
        $prop = $objDef->createProperty("id", "string");

        $prop = $objDef->createProperty("objDefId", "string");
        $prop = $objDef->createCollection("dataGridColumns","DataGridColumns");

        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
