<?php
class FormControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("FormController");

        $objDef->addConstructorParam("id");

        $prop = $objDef->createProperty("id", "string");

        $prop = $objDef->createProperty("form", "Form");
        $prop->setSetter('setForm');

        $prop = $objDef->createProperty("auto", "boolean");
        $prop = $objDef->createProperty("objDefId", "string");
        $prop = $objDef->createProperty("title", "string");
        $prop = $objDef->createProperty("visible", "boolean");
        $prop = $objDef->createProperty("mode", "string");
        $prop = $objDef->createProperty("ajaxSubmit","boolean");
        $prop = $objDef->createProperty("hideOnCancel","boolean");
        $prop = $objDef->createProperty("visibleClass","string");
        $prop = $objDef->createProperty("loadFromXML","boolean");
        
        $prop = $objDef->createProperty("formDefId","string");
        $prop = $objDef->createProperty("loadFormDefFromXML","boolean");
        
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
