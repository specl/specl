<?php
class ModelViewerControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("ModelViewerController");

        $objDef->addConstructorParam("id");

        $prop = $objDef->createProperty("id", "string");

        $prop = $objDef->createProperty("objDefId", "string");
        $prop = $objDef->createProperty("fileName", "string");

        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
