<?php

class DataGridController2Spec
{
    public function defSpec($spec)
    {
        $objDef=new ObjDef("DataGridController2");
        $prop=$objDef->createProperty("DataGrid","DataGrid2");
        $prop->setSetter("setDataGrid");
        
        $prop=$objDef->createProperty("EditController","string");
        $prop=$objDef->createProperty("AddController","string");
        $prop=$objDef->createProperty("ViewController","string");
        $prop=$objDef->createProperty("DeleteController","string");
        $prop=$objDef->createProperty("queryName","string");
        
        $prop=$objDef->createProperty("objDefId","string");
        $prop=$objDef->createProperty("auto","boolean");

        $prop=$objDef->createProperty("id","string");
        $objDef->addConstructorParam("id");
        $objDef->addRequiredProperty("id");
        $objDef->addKey("id");

        $spec->addDef($objDef);

    }
}
?>
