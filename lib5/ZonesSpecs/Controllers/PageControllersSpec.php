<?php
class PageControllersSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("PageControllers", false);
        $colDef->addChildType("AuthController");
        //$colDef->addChildType("SiteNavController");

        $spec->addDef($colDef);
    }
}
?>
