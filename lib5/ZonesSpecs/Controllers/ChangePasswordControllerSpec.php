<?php
class ChangePasswordControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("ChangePasswordController");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
