<?php
class RegisterControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("RegisterController");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
