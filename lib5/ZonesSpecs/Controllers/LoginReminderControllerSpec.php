<?php
class LoginReminderControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("LoginReminderController");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
