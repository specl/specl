<?php
class FileUploadControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("FileUploadController");

        $objDef->addConstructorParam("id");

        $prop = $objDef->createProperty("id", "string");
        
        $prop= $objDef->createProperty("validationSpec","string");

        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
