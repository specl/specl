<?php
class SiteNavControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("SiteNavController");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
