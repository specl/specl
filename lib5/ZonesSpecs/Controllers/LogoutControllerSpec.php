<?php
class LogoutControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("LogoutController");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
