<?php
class InspectusControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("InspectusController");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
