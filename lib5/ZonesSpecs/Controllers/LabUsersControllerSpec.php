<?php
class LabUsersControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("LabUsersController");

        $objDef->addConstructorParam("id");
        $prop = $objDef->createProperty("id", "string");
       
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
