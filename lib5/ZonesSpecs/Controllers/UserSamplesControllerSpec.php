<?php
class UserSamplesControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("UserSamplesController");

        $objDef->addConstructorParam("id");
        $prop = $objDef->createProperty("id", "string");
       
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
