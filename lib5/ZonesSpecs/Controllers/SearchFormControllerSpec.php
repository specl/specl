<?php
class SearchFormControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("SearchFormController");
        $prop = $objDef->createProperty("id", "string");

        $prop = $objDef->createProperty("objDefId", "string");
        $prop = $objDef->createCollection("dataGridColumns","DataGridColumns");

        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
