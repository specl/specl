<?php
class IncludeControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("IncludeController");

        $objDef->addConstructorParam("id");

        $prop = $objDef->createProperty("id", "string");
        $prop = $objDef->createProperty("filename", "string");

        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");

        $spec->addDef($objDef);
    }
}
?>
