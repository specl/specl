<?php
class AuthControllerSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("AuthController");
        $prop = $objDef->createProperty("id", "string");
        $objDef->addKey("id");
        $objDef->addRequiredProperty("id");
        $spec->addDef($objDef);
    }
}
?>
