<?php
class ConnectionsSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("Connections", true);
        $colDef->addChildType("Connection");
        $colDef->setKeyId("id");
        $spec->addDef($colDef);
    }
}
?>
