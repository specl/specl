<?php
class ShowInNavWhenSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("ShowInNavWhen", false);
        $colDef->addChildType("IsNullCondition");
        $colDef->addChildType("StringVariableCondition");
        $colDef->addChildType("BooleanVariableCondition");
        $colDef->addChildType("LookupVariableCondition");

        $spec->addDef($colDef);
    }
}
?>
