<?php
class ParameterSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("Parameter");
        $prop = $objDef->createProperty("key", "string");
        $prop = $objDef->createProperty("required", "boolean");
        $prop = $objDef->createCollection("locations","Locations");
        $prop = $objDef->createCollection("conditions","Conditions");
        $objDef->addRequiredProperty("key");
        $objDef->addKey("key");
        $spec->addDef($objDef);
    }
}
?>
