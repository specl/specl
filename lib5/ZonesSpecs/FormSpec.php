<?php

include_once 'KeyValueSpec.php';
include_once 'KeyValuePairsSpec.php';
include_once 'ControlModifySpec.php';
include_once 'FormUpdatesSpec.php';
include_once 'UpdateFormActionSpec.php';
include_once 'LoadFormActionSpec.php';
include_once 'LoadPageActionSpec.php';
include_once 'RedisplayFormActionSpec.php';
include_once 'OnRetryLimitExceededSpec.php';
include_once 'OnFailureSpec.php';
include_once 'OnSuccessSpec.php';
include_once 'MethodSpec.php';
include_once 'CallFunctionActionSpec.php';
include_once 'ActionsSpec.php';
include_once 'OnFormValidSpec.php';

class FormSpec extends SpecDef {
    public function defSpec($spec) {

        $def = new MethodSpec();
        $def->defSpec($spec);


        $def = new OnFormValidSpec();
        $def->defSpec($spec);

        $def = new ActionsSpec();
        $def->defSpec($spec);

        $def = new ControlModifySpec();
        $def->defSpec($spec);

        $def = new FormUpdatesSpec();
        $def->defSpec($spec);

        $def = new UpdateFormActionSpec();
        $def->defSpec($spec);

        $def = new RedisplayFormActionSpec();
        $def->defSpec($spec);

        $def = new CallFunctionActionSpec();
        $def->defSpec($spec);

        $def = new LoadPageActionSpec();
        $def->defSpec($spec);

        $def = new OnFailureSpec();
        $def->defSpec($spec);

        $def = new OnSuccessSpec();
        $def->defSpec($spec);


        $def = new OnRetryLimitExceededSpec();
        $def->defSpec($spec);

        $def = new KeyValueSpec();
        $def->defSpec($spec);

        $def = new KeyValuePairsSpec();
        $def->defSpec($spec);

        //----------------------------------------------------------------------
        //----------------------------------------------------------------------

        $objDef = new ObjDef("Form");
        $prop = $objDef->createProperty("id", "auto");
        $prop = $objDef->createProperty("title", "string");
        $prop = $objDef->createProperty("objDefId", "string");
        $prop = $objDef->createProperty("auto", "boolean");
        $prop = $objDef->createCollection("elements","FormElements");

        $prop = $objDef->createProperty("maxRetry", "integer");
        $prop = $objDef->createProperty("retryTimeout", "integer");
        $prop = $objDef->createProperty("onFormValid", "OnFormValid");

        $objDef->addKey("Id");
        $spec->addDef($objDef);
    }
}
?>
