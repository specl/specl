<?php
class StringListSpec extends SpecDef {
    public function defSpec($spec) {
        $colDef = new CollectionDef("StringList", false);
        $colDef->addChildType("string");
        $spec->addDef($colDef);
    }
}
?>
