<?php

class ResourceSpec extends SpecDef {
    public function defSpec($spec) {
        $objDef = new ObjDef("Resource");

        $prop = $objDef->createProperty("resourceId", "string");

        $prop = $objDef->createProperty("aco", "string");
        $prop = $objDef->createProperty("acu", "string");
        $prop = $objDef->createProperty("acr", "string");
        $prop = $objDef->createProperty("acf", "string");
        $prop = $objDef->createProperty("act", "string");       
            
        $objDef->addConstructorParam("resourceId");
        $objDef->addRequiredProperty("resourceId");
        $objDef->addKey("resourceId");

        $spec->addDef($objDef);
    }
}

?>
