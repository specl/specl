<?php
/**
 * Description of ActionsSpec
 *
 * @author tony
 */
class ActionsSpec extends SpecDef {
    
    
    public function defSpec($spec) {
        $colDef = new CollectionDef("Actions", false);
        $colDef->addChildType("UpdateFormAction");
        $colDef->addChildType("RedisplayFormAction");
        $colDef->addChildType("CallFunctionAction");
        $colDef->addChildType("LoadPageAction");
        $colDef->addChildType("LoadFormAction");
        $colDef->addChildType("GenerateFormAction");

        $spec->addDef($colDef);
    }
}
?>
