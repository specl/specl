<?php
/**
 * Description of Zone
 *
 * @author Tony
 */
class Zone {

    public $id;
    public $controllers;

	public $cssClass;  	// class=""
	public $cssId;		// id = ""
	public $cssStyle;	// inline style
	public $startWrapper;
	public $endWrapper;

    public $zones;

    
    public function  __construct($params) {

        if (isset($params["id"])) {
            $this->id = $params["id"];
        } else {
            $this->id = null;
        }

        $this->controllers = array();

		$this->cssClass = Null;
		$this->cssId = Null;
		$this->cssStyle = Null;
		$this->startWrapper = Null;
		$this->endWrapper = Null;

        $this->zones = array();
    }
    
    public function render($strbld) {
        //$strbld->addline("<!-- Zone {$this->id} Start -->");
        
        foreach($this->controllers as $controllerObj)
        {
            //if ($controllerObj->shouldRender() == true) {
                //$zoneObj->renderStart($obj);
                $controllerObj->render($strbld);
                //$zoneObj->renderEnd($obj);
            //}
        }
            
        //$strbld->addline("<!-- Zone {$this->id} End -->");
    }

    public function renderStart($controllerObj)
	{
		if ($this->startWrapper)
		{
			echo $this->startWrapper;
		}
	}

	public function renderEnd($controllerObj)
	{
		if ($this->startWrapper)
		{
			if ($this->endWrapper)
			{
				echo $this->endWrapper;
			}
			else
			{
				echo "</div>";
			}
		}
	}	

	public function addController($controllerObj)
	{
		// TODO : check for duplicate controller ids
		if (array_key_exists($controllerObj->id, $this->controllers)==False)
		{
            $this->$controllers[$controllerObj->id] = $this->controllers;
		}
        else{
            throw new Execption("A controller with the name {$controllerObj->id} already exists in zone {$this->id}");
        }
	}

}
?>
