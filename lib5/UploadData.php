<?php
class UploadDataException extends Exception{}
/*
 *  The UploadData class manages the upload and insertion of data into the database.
 * 
 *  UploadData takes the following parameters:
 *      - objDefName (required): the name of the objDef associated with the data you are inserting
 *      - mode (default="upload"): the mode parameter to pass to the objDef
 *      - commitInterval (default=1000): perform a database commit after every X number of objects are inserted.
 *      - connection (optional): name of the connection you wish to use. If no connection is passed in the App's default connection is used.
 *      - indentifierColumnName (optional): name of the column to used to identify the column in error reporting, if no column name is
 *                                          is specified then the row number is used as an indentifier.
 *
 *  UploadData has the following methods:
 *      - insertData: Takes an SpecCls object or an associative array of data and adds the appropriate insert statements to a transaction, if
 *                    the data passes validation.The data is commited to the database based on the commitInterval parameter or if the commitData
 *                    function is called.
 *      
 *      - commitData: Commits the data in the current transaction to the database. This function is typically called at the end of the upload to 
 *                    commit any data that still remains in the transaction.
 * 
 *      - getRowIndentifier: This is a helper function to retrieve the row indentifier based on the indentifierColumnName parameter.
 *                           If the indentifierColumnName parameter is not set, the rowcount is used as an indentifier. If indentifierColumnName
 *                           is set and the column does not exist in the row, raise an error.
 * 
 *  UploadData can raise the following errors:
 *      - ObjDef not found: This means that component could not find the ObjDef associated with the objDefName that was passed in * 
 *      - Validation Error: This means that the data that was passed in failed validation and was not inserted into the database * 
 *      - Insertion Error: This means that the transaction failed due to some sort of SQL error. This should typically not happen
 *                         because the data is validated before being inserted into the database. If this error occurs then there
 *                         is most likely a bug in the SQL generation code or the ObjDef is missing a validation rule that should be there.
 *      
 *      - Indentifier Column missing: The indentifier column specified is missing from the data passed in.
 * 
 *  TODO: Errors should emit events rather than raise exceptions or echo. We need to figure out how we plan to deal with this.
 *  TODO: Add parameters to control whether or not to add defaults or apply transforms from SPECs.
 */
class UploadData {
    private $mode;
    private $objDefName;
    private $commitInterval;
    private $transactionManager;
    private $numObjectsInserted;
    private $indentifierColumnName;
    private $rowCount;
    
    public function __construct($params){
        global $app;
        
        if(isset($params["mode"])){
            $this->mode=$params["mode"];
        }
        else{
            $this->mode="upload";
        }
        
        if(isset($params["objDefName"])){
            $this->objDefName=$params["objDefName"];
        }
        else{
            throw new Exception("The parameter 'objDefName' is required");
        }
        
        if(isset($params["commitInterval"])){
            $this->commitInterval=$params["commitInterval"];
        }
        else{
            $this->commitInterval=1000;
        }
        
        if(isset($params["indentifierColumnName"])){
            $this->indentifierColumnName=$params["indentifierColumnName"];
        }
        
        $dbConn=$app->getConnection();
        $this->transactionManager=new TransactionManager($dbConn);
        $this->numObjectsInserted=0;
        $this->rowCount=0;
        
    }
    
    /*
     *  InsertData: 
     *      Required Input Parameters:
     *          - data: The data to insert, which can be either a SpecCls object or an associative array.
     *      WorkFlow:
     *          - load the objDef associated with the objDefName parameter, if the ObjDef cannot be found raise an error
     *          - create the appropriate DAL object based on the objDefName
     *          - set the mode of the objDef based on the mode parameter
     *          - add default values to the data based on the objDef and the mode
     *          - if the data is an array, transform it into a SpecCls obj
     *          - apply data transformations based on the objDef and the mode
     *          - validate the data, raise an error if the data fails validation
     *          - add insert to transaction
     *          - if the insert occurs at the commitInterval, commit the data to the database.
     *          
     */
    public function insertData($input){
        global $app;
        
        if(!isset($input["data"])){
            throw new Exception("The input parameter 'data' is required");
        }
        else{
            $data=$input["data"];
        }
        
        $objDef=$app->specManager->findDef($this->objDefName);
        if(is_null($objDef)){
            throw new Exception("The objDef for '{$this->objDefName}' could not be found");
        }
        
        $this->rowCount+=1;        
        $DAL=NewDALObj($this->objDefName);
        $objDef->setMode($this->mode);  
        $objDef->addDefaultValues($data,$this->mode);
        if(is_array($data)){
            $data=$DAL->createFromData2($DAL->objDef,$data,false);
        }
        $objDef->applyTransforms($data,$this->mode);
        $objDef->validate($data,$this->mode);
        
        // TODO: changeto emit an error event, so the errors can be further processed.
        if(count($objDef->validationErrors)>0){
            $columnIndentifier=$this->getRowIndentifier($data);            
            $errMsg="";            
            $sep="";
            foreach($objDef->validationErrors as $error){
                $errMsg.="{$sep}Validation Failed for row '{$columnIndentifier}' with error message: {$error->getErrorMessage()};";
                if($sep===""){$sep="\n";};
            }
            throw new UploadDataException($errMsg);
        }
        
        $DAL->addInsertToTransaction($data, $this->transactionManager);
        $this->numObjectsInserted+=1;
        if($this->numObjectsInserted % $this->commitInterval===0){
            $this->commit();
        }
       
    }
    
    
    
    /*
     *  UpdateData: 
     *      Required Input Parameters:
     *          - data: The data to update, which can be either a SpecCls object or an associative array.
     *      WorkFlow:
     *          - load the objDef associated with the objDefName parameter, if the ObjDef cannot be found raise an error
     *          - create the appropriate DAL object based on the objDefName
     *          - set the mode of the objDef based on the mode parameter
     *          - add default values to the data based on the objDef and the mode
     *          - if the data is an array, transform it into a SpecCls obj
     *          - apply data transformations based on the objDef and the mode
     *          - validate the data, raise an error if the data fails validation
     *          - add insert to transaction
     *          - if the insert occurs at the commitInterval, commit the data to the database.
     *          
     */    
    public function updateData($input){
        global $app;
        
        if(!isset($input["data"])){
            throw new Exception("The input parameter 'data' is required");
        }
        else{
            $data=$input["data"];
        }
        
        $objDef=$app->specManager->findDef($this->objDefName);
        if(is_null($objDef)){
            throw new Exception("The objDef for '{$this->objDefName}' could not be found");
        }
        
        $this->rowCount+=1;        
        $DAL=NewDALObj($this->objDefName);
        $objDef->setMode($this->mode);  
        //$objDef->addDefaultValues($data,$this->mode);
        //if(is_array($data)){
        //    $updateObj=$DAL->createFromData2($DAL->objDef,$data,false);
        //}
        
        $originalObj = $DAL->getOneByPkFromData($data);
        $updateObj = $DAL->getOneByPkFromData($data);
                
        if ($originalObj) {                                        
             
              foreach($objDef->getProperties() as $propDef) {
               
                if (($propDef->isCollection() == true)  || !$propDef->isScalar()) {
                    continue;
                }

                if (isset($data[$propDef->name])) {
                    $updateObj->{$propDef->name}=$data[$propDef->name];
                }
            }
                        
            $DAL->updateOneByPK($originalObj, $updateObj);   
                                
        } else {
            
            // the object was not found this is a non fatal error. 
            
        }
       
    }
    
    
    public function commit(){
        // TODO: change to emit an error event, so the errors can be further processed.
        $results=$this->transactionManager->execute();
        $this->transactionManager->clearTransactions();
        if(!$results->success){
                throw new UploadDataException("Insertion failed with error: {$results->errorMessage}");            
        }        
    }
    
    public function getRowIndentifier($data){
        if(isset($this->indentifierColumnName)){
            if(!isset($data[$this->indentifierColumnName])){
                throw new UploadDataException("The Indentifier Column '{$this->indentifierColumnName}' is missing from row '{$this->rowCount}'.");
            }
            else{
                return $data[$this->indentifierColumnName];
            }
        }
        else{
            return $this->rowCount;
        }
    }
    
}

?>