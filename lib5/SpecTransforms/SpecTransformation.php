<?php

class SpecTransformation {
    
    /*    
     *  transformSpec takes an objDef and some data(optionally) and trasnforms the
     *  objDef. An objDef is an object, which in PHP is a reference, so the functiohn
     *  modifies the objDef directly and does not return an objDef.
     */
    public function transformSpec($objDef,$data=null){
        throw new Exception("This function must be implemented in the child class");
    }
    
    // apply transformation recusively to nested Specs
    // all Specs are references so they only need to be transformed once, no matter where they are the nested heirarchy.  
    public function applyTransformRecursively($objDef,$data){
        global $app;
        $objDef->visited=true;
        foreach($objDef->getProperties() as $propDef){
            if($propDef->isCollection() ){     
                $collectionDef=$app->specManager->findDef($propDef->objectTypeOrig);
                foreach($collectionDef->childTypes as $childType){
                    if(is_string($childType)){
                        $childObjDef=$collectionDef->getChildType($childType);
                    }else{
                        $childObjDef=$childType;
                    }
                    
                    if(!isset($childObjDef->visited) || $childObjDef->visited!=true){
                        $this->transformSpec($childObjDef,$data);                      
                    }
                }                
            }
            else if(is_a($propDef, "ObjDef")){
                if(!isset($propDef->visited) || $propDef->visited!=true){
                    $this->transformSpec($propDef,$data);                      
                }                
            }
        }
        $objDef->visited=false;
    }
}

?>