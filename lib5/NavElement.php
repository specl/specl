<?php
/**
 * @author Tony
 */
class NavElement {
        
    public $id;
    public $title;
    public $parent;
    
    public $path; // path seperated by forward slashes
    public $pathArray; // array version of the path
    
    public $allowAccess;
    public $allowDirectAccess;        
    
    public $rolesReq; // roles required to access this NavElement
    public $parameters; // parameters for this NavElement
    public $showInNav;
    public $showInNavWhen;
    public $showInState;
    public $showWhenLoggedIn;    

    public $children;    
    
    public function  __construct() {
        $this->id = null;
        $this->title = null;
        $this->parent = null;

        $this->path = null;
        $this->pathArray = array();

        $this->allowAccess = true;
        $this->allowDirectAccess = true;

        $this->rolesReq = array();
        $this->parameters = array();
        $this->showInNav = true;
        $this->showInNavWhen = null;
        $this->showInState = null;
        $this->showWhenLoggedIn = null;
    
        $this->children = array();
    }

    public function setId($value) {
        $this->id = sanitize($value, PARANOID);
        if (is_null($this->title)) {
            $this->title = $this->id;
        }
    }

    public function setPath($path) {
        //global $log;
        $errors=array();

        // set path, root, parent and pathArray

        // 	strip any white space
        $path = trim($path);

        // strip leading and trailing \ if any
        if ($path[0] == '/')
        {
            $path = substr($path, 1);
        }

        if ($path[strlen($path)-1] == '/')
        {
            $path = substr($path, 0, strlen($path)-1);
        }

        $pathArray = array();

        $parts = explode('/' , $path);

        if (count($parts)==1)
        {
            $pageId = $parts[0];
            $pageParent = '/';
            $pageRoot = '/';
        }
        else
        {
            $pageId = $parts[count($parts)-1];
            $pageParent = $parts[0];
            $pageRoot = $parts[0];
        }

        foreach($parts as $part) {
            $id = sanitize($part, PARANOID);

            if (array_key_exists($id,$pathArray))
            {
                // error duplicate page id in path
                $this->errors[] = "Error duplicate page id \"$id\" in path<br/>";
                return Null;
            } else {
                $pathArray[]=$id;
            }
        }

        $this->path = $path;
		$this->root = $pageRoot;
		$this->parent = $pageParent;
		$this->pathArray = $pathArray;

    }

//    public function isVisible() {
//        global $app;
//
//        if ($this->showInNav==false)
//            return false;
//
//        if ($this->allowDirectAccess==false)
//            return false;
//
//        if (count($this->showInNavWhen)==0) {
//            return true;
//        }
//
//        foreach($this->showInNavWhen as $condition) {
//
//            if (is_a($condition, "BooleanVariableCondition")) {
//                $value = $app->getValue($condition->key);
//                if ($condition->value != $value) {
//                    return false;
//                }
//            } else if (is_a($condition, "LookupVariableCondition")) {
//                $lookupArray = $app->getValue($condition->key);
//                if ($lookupArray) {
//                    if (array_key_exists($condition->value,$lookupArray)==false) {
//                        return false;
//                    }
//                } else {
//                    return false;
//                }
//            } else if (is_a($condition, "StringVariableCondition")) {
//                $value = $app->getValue($condition->key);
//
//                if ($condition->value != $value) {
//                    return false;
//                }
//            }
//        }
//        return true;
//    }    
}
?>

