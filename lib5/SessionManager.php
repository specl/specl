<?php
/**
 * Description of SessionManager
 *
 * @author tony
 */
class SessionManager {

    public $sessionObj;
    
    public function  __construct() {        
        $this->sessionDAL = NewDALObj('Session');
    }

    public function start() {
        global $log;
        global $app;
          
        $result = session_start();
        //If a session fails to start, then FALSE is returned.
        if ($result) {
            $sessionId = session_id();
        } else {
            $sessionId = null;
        }

        $createNewSessionId = false;

        if (!is_null($sessionId)) {
            
            $this->sessionObj = $this->sessionDAL->getSession($sessionId);
                    
            if (is_null($this->sessionObj)) {
                $createNewSessionId = true;
            }
            else{

                if (is_null($this->sessionObj->sessionId)) {
                    $createNewSessionId = true;
                } else {
                    // is session still active?
                    if ($this->sessionObj->active===false) {
                        $createNewSessionId = true;
                    }

                    // perform time-out check
                    // TODO:implement this

                    // perform demanding session
                    // TODO:implement this

                    // is session logged in?
    //                if ($this->sessionObj->loggedIn===false) {
    //                    $createNewSessionId = true;
    //                }     
                }

                if (is_null($this->sessionObj->user)) {
                    $this->sessionObj->user='anonymous';
                }
            }

        } else {
            $createNewSessionId = true;
        }

        if ($createNewSessionId) {

            /*CLOSE PREVIOUS SESSION*/
            session_unset();
            session_destroy();

            /*NOW GENERATING LINK FOR SESSION DATA */
            session_start();
            session_regenerate_id();
            $sessionId = session_id();
            
            $this->sessionObj = $this->sessionDAL->createSession($sessionId, 
                                                                 $app->activeSession->remoteAddr, 
                                                                 $app->activeSession->userAgent);            
        } else {
            $this->sessionDAL->touchSession($this->sessionObj,$app->activeSession->remoteAddr);
        }        
    }

    public function state() {
            
        if ($this->sessionObj->loggedIn==true) {
            return "LoggedIn";
        } else {
            return "LoggedOut";
        }        
    }
    
    public function setStateLoggedIn($user) {
        // does user exist in acu?
        // is user active?
        $results = $this->sessionDAL->setStateLoggedIn($this->sessionObj->sessionId, $user);        
        $this->sessionObj->loggedIn = true;    
    }

    public function setStateLoggedOut() {
        $results = $this->sessionDAL->setStateLoggedOut($this->sessionObj->sessionId);        
        $this->sessionObj->loggedIn = false;    
    }

    
    public function deactivateSession() {
        $results = $this->sessionDAL->deactivateSession($this->sessionObj->sessionId);    
        $this->sessionObj->loggedIn = false;
    }
    
}
?>
