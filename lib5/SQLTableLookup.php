<?php
/*  The SQLTableLookup performs a key value lookup based on two columns in the same table.
 * 
 *  SQLTableLookup takes the following parameter:
 *      - tableName (required): The name of the databse table that contains the lookup data
 *      - keyColumn (required): The name of the column to be used to look values up
 *      - valueColumn (required): The name of the column from which to return values
 *      - enableCache (optional): The lookup information from the table can optionally be cached in memory.
 *                                The default value is true.
 *  
 *  SQLTableLookup has the following methods:
 *      - lookupValue: Takes a key to lookup and returns the value corresponding to the key.
 * 
 *      - loadCache: This is a helper function that loads the lookup from the database into the cache
 *  
 *  TODO: raise an event when a key corresponds to more than one lookup value
 *  TODO: raise a different event when the key cannot be found in the lookup column
 *  TODO: Add proper checks and error handling to make sure that the table exists in the database
 *  TODO: Add propert checks and error handling to make sure the the value and key columns exist in the database
 */
class SQLTableLookup {
    private $tableName;
    private $keyColumn;
    private $valueColumn;
    private $enableCache;
    private $cache;
    
    public function __construct($params){
        global $app;
        
        if(isset($params["tableName"])){
            $this->tableName=$params["tableName"];
        } 
        else{
            throw new Exception("The parameter 'tableName' is required");
        }
        
        if(isset($params["keyColumn"])){
            $this->keyColumn=$params["keyColumn"];
        } 
        else{
            throw new Exception("The parameter 'keyColumn' is required");
        }
        
        if(isset($params["valueColumn"])){
            $this->valueColumn=$params["valueColumn"];
        } 
        else{
            throw new Exception("The parameter 'valueColumn' is required");
        }
        
        if(isset($params["enableCache"])){
            $this->enableCache=$params["enableCache"];
        } 
        else{
            $this->enableCache=true;
        }
        
        $this->dbConn=$app->getConnection();        
        $this->cache=array();
        
        if($this->enableCache){
            $this->loadCache();
        }        
    }   
    
    public function loadCache(){
        $SQL="SELECT {$this->valueColumn},{$this->keyColumn} from {$this->tableName};";        
        $rows=$this->dbConn->fetchData($SQL);        
        
        foreach($rows as $row){
            $this->cache[$row[1]]=$row[0];
        }
    }
    /*
     * lookupValue:
     *      Required Input Parameters:
     *          - key: The value to lookup
     * 
     *      WorkFlow:
     *          - check to see if caching is enabled
     *          - if caching is enable look for the value in the cache, if the value cannot be found
     *            in the cache check the database. (The cache could possibly be stale)
     *          - if caching is disabled check the database.
     *          - if the key is found, return the value associated with it. 
     *          - if the key is not found, return null
     */
    public function lookupValue($input){        
        if(!isset($input["key"])){
            throw new Exception("The input parameter 'key' is required");
        }
        else{
            $key=$input["key"];
        }
        
        if($this->enableCache){
            if(isset($this->cache[$key])){
                return $this->cache[$key];
            }            
        }
        
        $key=addslashes($key);
        $SQL="SELECT `{$this->valueColumn}` from `{$this->tableName}` where {$this->keyColumn}='{$key}'";
        $rows=$this->dbConn->fetchData($SQL);
        
        if(count($rows)===0){
            return null;
        }
        else{
            $value=$rows[0][0];
            return $value;
        }
        
    }
    
    public function getKeyColumn(){
        return $this->keyColumn;
    }
}

?>