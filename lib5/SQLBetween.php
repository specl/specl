<?php
/** @ObjDefAnn(objDefId="SQLBetween")  
 */
class SQLBetween extends SQLWhereClause{
    public function __construct($leftHS,$minStatement,$maxStatement){
        $this->leftHS=$leftHS;
        $this->minStatement=$minStatement;
        $this->maxStatement=$maxStatement;
    }
    
    public function generateSQLClause(){
        return "{$this->leftHS} BETWEEN {$this->minStatement} AND {$this->maxStatement}";
    }
}
?>