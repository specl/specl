<?php
//TODO: Add more specific subclasses with better error handling for the right and left hand side.

/** @ObjDefAnn(objDefId="SQLComparison")  
 */
class SQLComparison extends SQLWhereClause
{
    public function validateOperator($operator)
    {
        $validOperators=array("=","!=","<","<=",">",">=","like","not like","in","not in");
       
        if(array_search($operator,$validOperators)===false){
            throw new Exception("'{$operator}' is not a valid SQL comparison operator");
        }
    }
    
    public function generateSQLClause(){
        
        return parent::generateSQLClause();
    }
}
?>
