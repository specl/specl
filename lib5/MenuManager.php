<?php

/**
 * MenuManager
 *
 * @author Tony
 */
class MenuManager {
        
    public $menu;
    public $specManager;
    
    public function  __construct($specManager) {
        $this->specManager = $specManager;        
        $this->menu=array();
    }
        
    public function  loadFromXML() {        
        global $app;
        
        $tmp = $this->specManager->getObjectFromXML($app->dbPath."Menus.xml", "Menus");
        //krumo($tmp);
        //TODO:Error checking
        $this->menu=$tmp;        
    }
    
    public function doesMenuExist($menuItemId) {
        if (array_key_exists($menuItemId,$this->menu->menuItems)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getResource($menuItemId) {
        
        if (array_key_exists($menuItemId,$this->resourceObjs)) {
            return $this->resourceObjs[$menuItemId];
        } else {
            $resourceObj = $this->loadPage($menuItemId);            
            $this->resourceObjs[$menuItemId] = $resourceObj;
            return $resourceObj;
        }
    }

    public function loadResource($menuItemId) {                
        global $app;
        //TODO:Error checking, add try catch block
        $filename = $app->dbPath."menus/{$menuItemId}.xml";
        $resourceObj = $this->specManager->getObjectFromXML($filename, "Resource");
        $resourceObj->isLoaded = true;
        return $resourceObj;
    }
                   
//    public function addMenuItem($path, $id, $title, $menuItemId, $controllerId, $actionName) {                
//        if (array_key_exists($path, $this->menuItems)==False) {            
//            $menuItem = new MenuItem();        
//            $menuItem->title = $title;
//            $menuItem->setId($id);            
//            $menuItem->setPath($path);    
//            $menuItem->resourceId = $menuItemId;
//            $menuItem->controllerId = $controllerId;
//            $menuItem->actionName = $actionName;
//            $this->menuItems[$id] = $menuItem;
//            
//            return $menuItem;
//        } else {
//            return $this->menuItems[$path];
//        }        
//    }    
    
    public function addMenuItemObj($menuItemObj) {                
        if (array_key_exists($menuItemObj->menuItemId, $this->menu->menuItems)==False) {                     
            $this->menuItems[$menuItemObj->menuItemId] = $menuItemObj;
        }         
    }  
    
    public function getMenu($menuItemId) {
        if (array_key_exists($menuItemId,$this->menu->menuItems)) {
            return $this->menu->menuItems[$menuItemId];
        } else {
            return null;
        }
    }
         
    public function isVisible($item) {
        global $app;
                
        if ($item->isVisible()==false) {
            return false;
        }
        
        if (!is_null($item->showWhenLoggedIn)) {
            if ($app->loggedInState == "LoggedIn") {
                if ($item->showWhenLoggedIn=="false") {
                    return false;
                } 
            } else if ($app->loggedInState == "LoggedOut") {
                if ($item->showWhenLoggedIn=="true") {
                    return false;
                } 
            }
        }        
        
        return true;
    }
    
    public function getTopLevel($justVisible=true)
	{
        $addIt=true;
        
		$items =array();

		foreach($this->menu->menuItems as $item)
		{
			if ($item->parent == "/")
			{
                            if ($justVisible) {
                                $addIt = $this->isVisible($item);                
                            }

                            if ($addIt) {
                                $items[]=$item;
                            }
			}
		}
		return $items;
	}
    
	public function getChildren($id,$justVisible=true)
	{
        $addIt=true;         
		$items=array();

		foreach($this->menu->menuItems as $item)
		{
			if ($item->parent == $id)
			{
                if ($justVisible) {
                    $addIt = $this->isVisible($item);                
                }

                if ($addIt) {
                    $items[]=$item;
                }
			}
		}
		return $items;
	}        
    
    public function addDefaultMenus($resourceManager) {
        
        foreach($resourceManager->menus as $id=>$resource) {
            $params = new ParamBuilder();
            $params->add("path", "/{$id}");
            $params->add("id", "{$id}");
            $params->add("title", "{$id}");
            $params->add("resourceId", "{$id}");
            $menuItem = new MenuItem($params->getParams());               
            $this->addMenuItemObj($menuItem);
        }        
    }
}
?>
