<?php
/**
 * Connection 
 *
 * @author Tony
 */
class Connection {

    public $user;
    public $pass;
    public $host;
    public $schema;
    public $port;
    public $db;  // the actual MYSQL Resource reference
    public $rows;
    
    public function  __construct() {

        $this->user = null;
        $this->pass = null;
        $this->host = null;
        $this->schema = null;
        $this->port = 3306;        
        $this->db = null;
        $this->silent = false;
        $this->rows = 0;
        $this->properties = array();
        
        $this->properties['identifier_quoting'] = array('start' => '`',
                                                        'end' => '`',
                                                        'escape' => '`');        
    }

    public function connect() {
        global $log;

        if ($this->db!=null) {
            // I am already connected...
            //TODO: disconnect maybe???
            return true;
        }

        /*
         * If host, user and pass are the same an existing connection will be reused instead
         * of creating a new one. This is bad when trying to connect to different schema in
         * the same database, so the last parameter must be set to true so a new link is always created.
         */
        //;charset=$this->charset        
        $this->db = new PDO("mysql:host=$this->host;dbname=$this->schema", $this->user, $this->pass );

        if (!$this->db)
        {
            $log->error("Connection",'Connect Error ');
            return false;
        }
        //$this->selectSchema();
        //$log->info("Connection","Connected To database {$this->schema} on {$this->host}");
        return true;        
    }
    
    public function disconnect(){
       $this->db=null; 
    }

    /*
     * method: Connection->selectSchema
     *
     * author: Anthony L. Leotta
     *
     * date: 2011-03-08
     *
     * description: Set the connection to the specified $schema or when it is not 
     * specified use the current value of the schema property.  This function
     * acts as a combination setterm in that if the schema is specified and it is valid,
     * the schema parameter value is set to the schema property. The whole thing
     * is wrapped in a exception handler, thus if the schema
     * select API call fails, the function fails gracefully.I don't see how the
     * application as a whole can survive a failure of this method.  A failure
     * here seems fatal.
     *
     */
    public function selectSchema($schema='')
    {
        global $log;
        
        try
        {
            if ($schema == '') {
                $schema = $this->schema;
            }

            mysql_select_db($schema,$this->db);

            $this->schema = $schema;
        }
        catch(Exception $e)
        {
            $errMsg = "Could not select the specified database '$schema'.\n" . $e->getMessage();
            if(isset($log)){
                $log->error("Connection->selectSchema",$errMsg);
            }
            else{
                throw new Exception($errMsg);
            }
        }
    }

    public function select($sql)
    {
        global $log;
        //$result = mysql_query($sql,$this->db);
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        
//        if (is_resource($result)) {    
//            //$this->rows = mysql_num_rows($result);
//            return $result;
//        } 
//        else {
//            $errMsg = "select failed: MySQL error ".mysql_errno($this->db).": ".mysql_error($this->db)."\nWhen executing:\n{$sql}\n ";
//            throw new Exception($errMsg);
//        }
        return $result;
    }

    public function insert($sql)
    {
        global $log;

        if ($this->exec($sql)==true) {
            // Get the ID generated in the last query
            
            // The ID generated for an AUTO_INCREMENT column by the previous 
            // query on success, 0 if the previous query does not generate an 
            // AUTO_INCREMENT value, or FALSE if no MySQL connection 
            // was established.
            
            return mysql_insert_id($this->db);
        } else {
            //$log->error("Connection->insert", "Bad insert");
            throw new Exception("insert failed: MySQL error ".mysql_errno($this->db).": ".mysql_error($this->db)."\nWhen executing:\n{$sql}\n");
        }
    }

    public function update($sql)
    {
        global $log;

        if ($this->exec($sql)==true) {
            return true;
        } else {
            $log->error("Connection->update", "Bad update");
            throw new Exception("update failed: MySQL error ".mysql_errno($this->db).": ".mysql_error($this->db)."\nWhen executing:\n{$sql}\n");
        }
    }

    public function delete($sql)
    {
       // global $log;

        if ($this->exec($sql)==true) {
            return true;
        } else {
           // $log->error("Connection->delete", "Bad delete");
            throw new Exception("delete failed: MySQL error ".mysql_errno($this->db).": ".mysql_error($this->db)."\nWhen executing:\n{$sql}\n");
        }
    }

    public function getRowAssoc($results)
    {
        if (is_resource($results)) {
            return mysql_fetch_assoc($results);
        } else {
            throw new Exception("get row failed: Invalid Resource");
        }
    }


    public function getRowEnum($results)
    {
        if (is_resource($results)) {
            return mysql_fetch_row($results);
        } else {
            throw new Exception("get row failed: Invalid Resource");
        }
    }
    
    public function fetchAssoc($sql) {
        $results = $this->select($sql);        
        $rows = array();
        while ($row = $this->getRowAssoc($results)) {    
            $rows[] = $row;
        }        
        return $rows;
    }
    
    public function fetchData($sql){
        $results = $this->select($sql);        
        $rows = array();
        while ($row = $this->getRowEnum($results)) {    
            $rows[] = $row;
        }        
        return $rows;
    }
    
    public function fetchAll($results) {
        $rows = array();
            
        while($row = $this->getRowEnum($results)) {
            $rows[] = $row;
        }
        
        return $rows;
    }

    /*
     * For queries that only return a single column of data, return results of the query as an array.
     */
    public function fetchColumn($results) {
        $rows = array();
            
        while($row = $this->getRowEnum($results)) {
            $rows[] = $row[0];
        }
        
        return $rows;
    }
            
    public function close()
    {
        if (is_resource($this->db))
            mysql_close($this->db);
    }

    public function escape($value, $isString)
    {
        if (is_null($value)) {
            return NULL;            
        } else {
            if ($isString) {
                $str = mysql_real_escape_string($value,$this->db);
                return "'".$str."'";
            } else {
                return $value;
            }
        }
    }

    public function exec($sql) {
        global $log;

        try {
            $status = mysql_query($sql,$this->db);

//            if (mysql_errno()!=0) {
//                $errMsg = "mysql_errno() MySQL error ".mysql_errno().": ".mysql_error()."\nWhen executing:\n{$sql}\n";
//                //$log->error("Connection->exec",$errMsg);
//                return false;
//            }
            return $status;
            
        } catch(Exception $e) {
//            $errMsg = "Exception MySQL error ".mysql_errno().": ".mysql_error()."\nWhen executing:\n{$sql}\n";
//            $log->error("Connection->exec",$errMsg);
            return false;
        }
    }
    
    public function getNumRowsReturned($results){
        return mysql_num_rows($results);
    }
    
    public function getNumRowsAffected($results){
        return mysql_affected_rows($results);
    }


    /* Transactions functions */

    function begin(){
        $null = mysql_query("START TRANSACTION", $this->db);
        return mysql_query("BEGIN", $this->db);
    }

    function commit(){
        return mysql_query("COMMIT", $this->db);
    }

    function rollback(){
        return mysql_query("ROLLBACK", $this->db);
    }
 
    function quoteIdentifier($table, $checkOption=false) {
//        if ($checkOption && ! $this->conn->getAttribute(Doctrine_Core::ATTR_QUOTE_IDENTIFIER)) {
//            return $str;
//        }
        $tmp = $this->identifier_quoting;
        $str = str_replace($tmp['end'],
            $tmp['escape'] .
            $tmp['end'], $table);

        return $tmp['start'] . $table . $tmp['end'];        
    }
    
    /**
     * Maps a native array description of a field to a MDB2 datatype and length
     * (used in the DataBaseManager class which is used in genFromDDL)
     *
     * @param array  $field native field description
     * @return array containing the various possible types, length, sign, fixed
     */
    public function getPortableDeclaration(array $field)
    {
        $dbType = strtolower($field['type']);
        $dbType = strtok($dbType, '(), ');
        if ($dbType == 'national') {
            $dbType = strtok('(), ');
        }
        if (isset($field['length'])) {
            $length = $field['length'];
            $decimal = '';
        } else {
            $length = strtok('(), ');
            $decimal = strtok('(), ');
            if ( ! $decimal ) {
                $decimal = null;
            }
        }
        $type = array();
        $unsigned = $fixed = null;

        if ( ! isset($field['name'])) {
            $field['name'] = '';
        }

        $values = null;
        $scale = null;

        switch ($dbType) {
            case 'tinyint':
                $type[] = 'integer';
                $type[] = 'boolean';
                if (preg_match('/^(is|has)/', $field['name'])) {
                    $type = array_reverse($type);
                }
                $unsigned = preg_match('/ unsigned/i', $field['type']);
                $length = 1;
            break;
            case 'smallint':
                $type[] = 'integer';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
                $length = 2;
            break;
            case 'mediumint':
                $type[] = 'integer';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
                $length = 3;
            break;
            case 'int':
            case 'integer':
                $type[] = 'integer';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
                $length = 4;
            break;
            case 'bigint':
                $type[] = 'integer';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
                $length = 8;
            break;
            case 'tinytext':
            case 'mediumtext':
            case 'longtext':
            case 'text':
            case 'text':
            case 'varchar':
                $fixed = false;
            case 'string':
            case 'char':
                $type[] = 'string';
                if ($length == '1') {
                    $type[] = 'boolean';
                    if (preg_match('/^(is|has)/', $field['name'])) {
                        $type = array_reverse($type);
                    }
                } elseif (strstr($dbType, 'text')) {
                    $type[] = 'clob';
                    if ($decimal == 'binary') {
                        $type[] = 'blob';
                    }
                }
                if ($fixed !== false) {
                    $fixed = true;
                }
            break;
            case 'enum':
                $type[] = 'enum';
                preg_match_all('/\'((?:\'\'|[^\'])*)\'/', $field['type'], $matches);
                $length = 0;
                $fixed = false;
                if (is_array($matches)) {
                    foreach ($matches[1] as &$value) {
                        $value = str_replace('\'\'', '\'', $value);
                        $length = max($length, strlen($value));
                    }
                    if ($length == '1' && count($matches[1]) == 2) {
                        $type[] = 'boolean';
                        if (preg_match('/^(is|has)/', $field['name'])) {
                            $type = array_reverse($type);
                        }
                    }

                    $values = $matches[1];
                }
                $type[] = 'integer';
                break;
            case 'set':
                $fixed = false;
                $type[] = 'text';
                $type[] = 'integer';
            break;
            case 'date':
                $type[] = 'date';
                $length = null;
            break;
            case 'datetime':
            case 'timestamp':
                $type[] = 'timestamp';
                $length = null;
            break;
            case 'time':
                $type[] = 'time';
                $length = null;
            break;
            case 'float':
            case 'double':
            case 'real':
                $type[] = 'float';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
            break;
            case 'unknown':
            case 'decimal':
                if ($decimal !== null) {
                    $scale = $decimal;
                }
            case 'numeric':
                $type[] = 'decimal';
                $unsigned = preg_match('/ unsigned/i', $field['type']);
            break;
            case 'tinyblob':
            case 'mediumblob':
            case 'longblob':
            case 'blob':
            case 'binary':
            case 'varbinary':
                $type[] = 'blob';
                $length = null;
            break;
            case 'year':
                $type[] = 'integer';
                $type[] = 'date';
                $length = null;
            break;
            case 'bit':
                $type[] = 'bit';
            break;
            case 'geometry':
            case 'geometrycollection':
            case 'point':
            case 'multipoint':
            case 'linestring':
            case 'multilinestring':
            case 'polygon':
            case 'multipolygon':
                $type[] = 'blob';
                $length = null;
            break;
            default:
                $type[] = $field['type'];
                $length = isset($field['length']) ? $field['length']:null;
        }

        $length = ((int) $length == 0) ? null : (int) $length;
        $def =  array('type' => $type, 'length' => $length, 'unsigned' => $unsigned, 'fixed' => $fixed);
        if ($values !== null) {
            $def['values'] = $values;
        }
        if ($scale !== null) {
            $def['scale'] = $scale;
        }
        return $def;
    }
 
    public function getDatabaseName() {
        return $this->schema;
    }
            
    public function __get($name)
    {
        if (isset($this->properties[$name])) {
            return $this->properties[$name];
        }

        return null;
    } 
}
?>