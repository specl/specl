<?php
/**
 * Description of CLISession
 *
 * @author Tony
 */
class CLISession {

    public $get;
    public $post;
    public $cookie;
    public $remoteAddr;
    public $requestURL;
    public $requestMethod;
    public $serverName;
    public $userAgent;
   
    public function  __construct() {
        $this->get = array();
        $this->post = array();
        $this->cookie = array();
        
        $this->remoteAddr = gethostname(); 
        $this->requestURL = "";
        $this->requestMethod = "cli";
        $this->userAgent = "command line";
        $this->serverName = gethostname(); 
        
        $_SERVER["SERVER_PORT"] = "80";
        $_SERVER["SERVER_NAME"] = gethostname();
        $_SERVER["DOCUMENT_ROOT"] = getcwd();
        
    }

    public function recover() {
        $this->get["page"]="Login";
    }

    public function getVar($location, $key) {

        $value = null;

        if ($location == "GET") {
            if (array_key_exists($key, $this->get)) {
                $value = $this->get[$key];
            }
        }

        return $value;
    }
}
?>
