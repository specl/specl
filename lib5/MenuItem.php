<?php
/**
 * @author Tony
 */
class MenuItem {
        
    public $menuItemId;
    public $title;
    public $description;
    public $parent;
    
    public $path; // path seperated by forward slashes
    public $pathArray; // array version of the path
    
    public $allowAccess;
    public $allowDirectAccess;        
    
    public $rolesReq; // roles required to access this NavElement
    public $parameters; // parameters for this NavElement
    public $showInNav;
    public $visible;
    public $showInNavWhen;
    public $showInState;
    public $showWhenLoggedIn;    
    public $showInSessionState;    
    public $style;    
    public $children;    
    
    public $resourceId;
    public $actionName;                
    public $controllerId;
    public $requestType;
    public $aco=null;
    public $acr=null;
    public $acu=null;
    public $acf=null;
    public $act=null;
    
    public function  __construct($params=array()) {
        
        $this->parent = null;
        $this->pathArray = array();
        $this->children = array();        
                
        if (isset($params["title"])) {
            $this->title = $params["title"];
        } else {
            $this->title = null;
        }

        if (isset($params["description"])) {
            $this->description = $params["description"];
        } else {
            $this->description = null;
        }
        
        if (isset($params["menuItemId"])) {
            $this->setId($params["menuItemId"]);            
        } else {
            $this->menuItemId = null;
        }
                
        if (isset($params["path"])) {
            $this->setPath($params["path"]);   
        } else {
            $this->path = null;
        }
               
        if (isset($params["allowAccess"])) {
            $this->allowAccess = $params["allowAccess"];
        } else {
            $this->allowAccess = true;
        }
        
        if (isset($params["allowDirectAccess"])) {
            $this->allowDirectAccess = $params["allowDirectAccess"];
        } else {
            $this->allowDirectAccess = true;
        }
        
        if (isset($params["rolesReq"])) {
            $this->rolesReq = $params["rolesReq"];
        } else {
            $this->rolesReq = null;
        }
                
        if (isset($params["visible"])) {
            $this->visible = $params["visible"];
        } else {
            $this->visible = true;
        }

        if (isset($params["showInSessionState"])) {
            $this->showInSessionState = $params["showInSessionState"];
        } else {
            $this->showInSessionState = null;
        }
        
        if (isset($params["showInNav"])) {
            $this->showInNav = $params["showInNav"];
        } else {
            $this->showInNav = true;
        }

        if (isset($params["showInNavWhen"])) {
            $this->showInNavWhen = $params["showInNavWhen"];
        } else {
            $this->showInNavWhen = null;
        }
        
        if (isset($params["showInState"])) {
            $this->showInState = $params["showInState"];
        } else {
            $this->showInState = null;
        }

        if (isset($params["showWhenLoggedIn"])) {
            $this->showWhenLoggedIn = $params["showWhenLoggedIn"];
        } else {
            $this->showWhenLoggedIn = null;
        }
            
        if (isset($params["style"])) {
            $this->style = $params["style"];
        } else {
            $this->style = null;
        }
        
        if (isset($params["resourceId"])) {
            $this->resourceId = $params["resourceId"];
        } else {
            $this->resourceId = null;
        }
        
        if (isset($params["controllerId"])) {
            $this->controllerId = $params["controllerId"];
        } else {
            $this->controllerId = null;
        }
                        
        if (isset($params["actionName"])) {
            $this->actionName = $params["actionName"];
        } else {
            $this->actionName = null;
        }        
        
        if (isset($params["requestType"])) {
            $this->requestType = $params["requestType"];
        } else {
            $this->requestType = "page";
        }        
        
        if (isset($params["parameters"])) {
            $this->parameters = $params["parameters"];
        } else {
            $this->parameters = null;
        }        
    }

    public function setId($value) {
        $this->menuItemId = sanitize($value, PARANOID);
        if (is_null($this->title)) {
            $this->title = $this->menuItemId;
        }
    }

    public function setPath($path) {
        //global $log;
        $errors=array();

        // set path, root, parent and pathArray

        // 	strip any white space
        $path = trim($path);

        // strip leading and trailing \ if any
        if ($path[0] == '/') {
            $path = substr($path, 1);
        }

        if ($path[strlen($path)-1] == '/') {
            $path = substr($path, 0, strlen($path)-1);
        }

        $pathArray = array();
        $parts = explode('/' , $path);

        if (count($parts)==1)
        {
            $pageId = $parts[0];
            $pageParent = '/';
            $pageRoot = '/';
        } else {
            $pageId = $parts[count($parts)-1];
            $pageParent = $parts[count($parts)-2];
            $pageRoot = $parts[0];
        }

        foreach($parts as $part) {
            $id = sanitize($part, PARANOID);

            if (array_key_exists($id,$pathArray)) {
                // error duplicate page id in path
                $this->errors[] = "Error duplicate page id \"$id\" in path<br/>";
                return Null;
            } else {
                $pathArray[]=$id;
            }
        }

        $this->path = $path;
        $this->root = $pageRoot;
        $this->parent = $pageParent;
        $this->pathArray = $pathArray;
    }

    public function checkPermissions() {
        global $app;
          
        $show=true;
        $acoRequired = false;
        $acofRequired = false;
        $acrRequired = false;
        $acuRequired = false;
        $actRequired = false;


        if (!is_null($this->aco)) {
            // a aco is required
            $acoRequired = true;

            if (!is_null($this->acf)) {
                $acofRequired = true;
            }
        }

        if (!is_null($this->acr)) {
            // a acr is required
             $acrRequired = true;
        }
        if (!is_null($this->act)) {
            // a acr is required
            $actRequired = true;
        }
        if (!is_null($this->acu)) {
            // a acu is required, well aren't we special. Mr or Mrs fancy pants.
            $acuRequired = true;
        }            

        if (($show)  && ($acuRequired)) {
            if ($app->acManager->userObj->user != $this->acu) {
                $show=false;
            }
        } else {
            if ($acrRequired) {
                if ($app->acManager->userObj->hasACR($this->acr)==false) {
                    $show=false;    
                }                        
            }
            if (($show) && ($acofRequired)) {
                if ($app->acManager->userObj->hasACOF($this->aco,$this->acf)==false) {
                    $show=false;    
                }
            } else if ($acoRequired) {
                if ($app->acManager->userObj->hasACO($this->aco)==false) {
                    $show=false;    
                }                        
            }
        }
        
        if($actRequired==true){
            if ($app->acManager->userObj->hasACT($this->act)==false) {
                $show=false;                                                                              
            } 
        }
        // modes?
        // states?

        return $show;
    }
    
    public function isVisible() {
        global $app;

        if ($this->visible==false)
            return false;
        
        if ($this->showInNav==false)
            return false;

        if ($this->allowDirectAccess==false)
            return false;
  
        if (!is_null($this->showInSessionState)) {                    
            if ($this->showInSessionState!=$app->sessionManager->state()) {
                return false;
            }                            
        }
        
        if ($this->checkPermissions()==false)                
            return false;
                
        if (count($this->showInNavWhen)==0) {
            return true;
        }        
        
        foreach($this->showInNavWhen as $condition) {

            if (is_a($condition, "BooleanVariableCondition")) {
                $value = $app->getValue($condition->key);
                if ($condition->value != $value) {
                    return false;
                }
            } else if (is_a($condition, "LookupVariableCondition")) {
                $lookupArray = $app->getValue($condition->key);
                if ($lookupArray) {
                    if (array_key_exists($condition->value,$lookupArray)==false) {
                        return false;
                    }
                } else {
                    return false;
                }
            } else if (is_a($condition, "StringVariableCondition")) {
                $value = $app->getValue($condition->key);

                if ($condition->value != $value) {
                    return false;
                }
            }
        }
        return true;
    }    
}
?>