<?php
/**
 * Description of WebPageBody
 *
 * @author tony
 */
class WebPageBody {
    
    public $cssInline;
    public $jsInline;
    
    public function __construct() {
        $this->cssInline = new InlineCSSManager();
        $this->jsInline = new InlineJSManager();        
    }
        
    public function render($strbld) {
        
        $strbld->increaseIndentLevel();
        
        $this->renderStart($strbld);                        

        $strbld->increaseIndentLevel();
                
        $this->cssInline->render($strbld);
        
        $this->jsInline->render($strbld);
        
        $strbld->decreaseIndentLevel();
        
        $this->renderEnd($strbld);
    }    
    
    
    public function renderStart($strbld) {
        $strbld->addLine("<body>");       
        
        
    }
    
    public function renderEnd($strbld) {
        $strbld->addLines("</body>");
        $strbld->decreaseIndentLevel();
        $strbld->addLines("</html>");
    }    
    
    
}

?>
