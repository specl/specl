<?php
/**
 * Description of ControlModify
 *
 * @author Tony
 */
class ControlModify {

    public $id;

    public function  __construct() {

        $this->id = null;
        $this->keyValuePairs =array();
    }

    public function applyUpdates($ctrl) {

        foreach($this->keyValuePairs as $key=>$kvp) {
            $ctrl->{$key} = $kvp->value;
        }
    }



}
?>
