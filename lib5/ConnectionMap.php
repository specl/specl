<?php

/**
 * ConnectionMap
 *
 * @author Tony
 */
class ConnectionMap {
    public $idMap; // variable ids to connection Names
    public $connMap; // connection names to connections
    public $defaultConnection;
    
    public $prefixMap;
    public $usePrefixMap;
    
    public function __construct() {
        $this->idMap = array();
        $this->connMap = array();
        $this->defaultConnection = null;
        $this->prefixMap = array();
        $this->usePrefixMap = false;
    }
    
    public function getConnectionForId($id) {
        if (array_key_exists($id, $this->idMap)) {
            return $this->idMap[$id];            
        } else {            
            return $this->defaultConnection;
        }
    }

    public function getConnectionObjForConnection($connection) {
        if (array_key_exists($connection, $this->connMap)) {
            return $this->connMap[$connection];              
        } else {
            return null;
        }
    }

    public function getConnectionObjForId($id) {        
        $connection = $this->getConnectionForId($id);        
        return $this->getConnectionObjForConnection($connection);            
    }
        
    public function addId($id, $connection) {
        $this->idMap[$id] = $connection;
    }

    public function addConnection($connection, $connectionObj) {
        $this->connMap[$connection] = $connectionObj;
    }
    
    
}

?>
