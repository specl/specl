<?php
/**
 * @author tony
 */

class ResourceManager {

    public $resources;
        
    public function __construct() {
        $this->resources = array();                
    }
    
    public function addResource($id) {                
        if (array_key_exists($id, $this->resources)==False) {            
            $resource = new Resource($id);                        
            $this->resources[$id] = $resource;
        }        
    }
    
    public function doesResourceExist($id) {
        if (array_key_exists($id,$this->resources)) {
            return true;
        } else {
            return false;
        }
    }    
}
?>
