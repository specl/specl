<?php
/**
 * Description of SessionParser
 *
 * @author Tony
 */
class SessionParser {

    private $data;
    private $pathStack;
    private $specStack;
    private $dataStack;
    private $errors;

    public function __construct() {
        $this->errors=array();
        $this->reset();
    }

    public function parseSession($spec, $session, $locations) {
        $this->reset();
        $this->session = $session;
        $this->locations = $locations;
        $this->parse($spec);        
    }

    public function reset() {        
        $this->data = array();
        $this->pathStack = new PathStack(App::ZONESSEP);
        $this->specStack = new Stack();
        $this->dataStack = new Stack();
        $this->errors = array();
    }

    public function parse($spec) {
        global $log;

        if($spec->isCrossChecked()==false)
        {
            throw new Exception("UnChecked Spec: A spec must be cross checked before parsing.");
        }

        $keys = $this->session->getKeys($locations);

        foreach($keys as $key) {
            $parts=explode(App::ZONESSEP,$key);

            echo "{$parts[0]},{$parts[1]}<br/>";
        }
    }

    public function getData() {
        return $this->data;
    }

    public function getDataByKey($key)
    {
        if (array_key_exists(strtolower($key), $this->data)) {
            return $this->data[strtolower($key)];
        } else {
            return Null;
        }
    }
}
?>
