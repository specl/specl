<?php
/** @ObjDefAnn(objDefId="SQLIsNull")  
 */
class SQLIsNull extends SQLWhereClause{
    public function __construct($leftHS){
        $this->leftHS=$leftHS;
    }
    
    public function generateSQLClause(){
        return "{$this->leftHS} IS NULL";
    }
}
?>
