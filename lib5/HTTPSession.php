<?php
/**
 * Description of WebSession
 *
 * @author Tony
 */
class HTTPSession {

	public $remoteAddr;
	public $requestURL;
	public $requestMethod;
	public $serverName;
    public $userAgent;

    public function  __construct() {
		$this->remoteAddr = filter_input(INPUT_SERVER, "REMOTE_ADDR", FILTER_SANITIZE_STRING);
		$this->requestURL = filter_input(INPUT_SERVER, "PHP_SELF", FILTER_SANITIZE_STRING);
		$this->requestMethod = filter_input(INPUT_SERVER, "REQUEST_METHOD", FILTER_SANITIZE_STRING);
        $this->userAgent = filter_input(INPUT_SERVER, "HTTP_USER_AGENT", FILTER_SANITIZE_STRING);
        $this->serverName = filter_input(INPUT_SERVER, "SERVER_NAME", FILTER_SANITIZE_STRING);
    }

    public function recover() {
        //$this->dumpAll();
    }

    public function hasVar($location, $key) {

        if ($location == "GET") {
            return filter_has_var(INPUT_GET, $key);
        } else if ($location == "POST") {
            return filter_has_var(INPUT_POST, $key);
        } else {
            return false;
        }
    }

    public function getVarNoStrip($location, $key) {

        $value = null;

        // I wonder is a XSS attack can get through here...i think it can not

        if ($location == "GET") {
            $value = filter_input(INPUT_GET, $key, FILTER_UNSAFE_RAW);
        } else if ($location == "POST") {
            $value = filter_input(INPUT_POST, $key, FILTER_UNSAFE_RAW);
        }

        // decode double and single quote
        $value = htmlspecialchars_decode($value,ENT_QUOTES);

        return $value;
    }

    public function getVarNoStripNoDecode($location, $key) {

        $value = null;

        // I wonder is a XSS attack can get through here...i think it can not

        if ($location == "GET") {
            $value = filter_input(INPUT_GET, $key, FILTER_UNSAFE_RAW);
        } else if ($location == "POST") {
            $value = filter_input(INPUT_POST, $key, FILTER_UNSAFE_RAW);
        }

        return $value;
    }

    public function getVar($location, $key) {

        $value = null;

        // I wonder is a XSS attack can get through here...i think it can not
        $location = strtolower($location);
        if ($location == "get") {
            $value = filter_input(INPUT_GET, $key, FILTER_SANITIZE_STRING);
            
            // filter_input does not work when setting global variables manually, which is done in
            // automated tests, so we must get the value in an alternative manner.
            if($value===null){
                if(isset($_GET[$key])){
                    $value=filter_var($_GET[$key],FILTER_SANITIZE_STRING);
                }
            }
        } else if ($location == "post") {
            $value = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING);
        }

        // decode double and single quote
        if (!is_null($value)) {
            $value = htmlspecialchars_decode($value,ENT_QUOTES);
        }

        return $value;
    }

    public function getArrayVar($location, $key) {

        $value = null;

        if ($location == "GET") {
            $value = filter_input(INPUT_GET, $key, FILTER_SANITIZE_STRING,FILTER_REQUIRE_ARRAY);
        } else if ($location == "POST") {
            $value = filter_input(INPUT_POST, $key, FILTER_SANITIZE_STRING,FILTER_REQUIRE_ARRAY);
        }

        return $value;
    }

    public function getAllKeys($location) {
        if ($location == "GET") {
           return array_keys($_GET);
        } else if ($location == "POST") {
            return array_keys($_POST);
        }
    }
    
    
    public function getKeyValuePairs($location) {
        $kv = array();
        
        if ($location == "GET") {
           $keys = array_keys($_GET);
        } else if ($location == "POST") {
           $keys = array_keys($_POST);
        } else {
           $keys = array();
        }
        
        foreach($keys as $key) {
            $value =  $this->getVar($location, $key);
            $kv[$key]=$value;
        }
        return $kv;
    }
    
    public function getAllKeyValuePairs() {
        $kv = array();
        
        $keys = array_keys($_GET);

        foreach($keys as $key) {
            $value =  $this->getVar("get", $key);
            $kv[$key]=$value;
        }
        
        $keys = array_keys($_POST);

        foreach($keys as $key) {
            $value =  $this->getVar("post", $key);
            if (isset($kv[$key])) {                
                if (is_array($kv[$key])) {
                    $kv[$key][] = $value;
                } else {
                    $orig = $kv[$key];                
                    $kv[$key] = array();
                    $kv[$key][] = $orig;
                    $kv[$key][] = $value;
                }
            } else {
                $kv[$key]=$value;
            }
        }
        
        return $kv;
    }

    

    public function dumpAll() {
        
        foreach($GLOBALS as $key=>$value)
		{
			echo "GLOBALS ".$key."=".gettype($value)."<br/>";
		}

		echo "<hr/>";
		
		foreach($_SERVER as $key=>$value)
		{
			echo "SERVER ".$key."=".$value."<br/>";
		}

		echo "<hr/>";

		foreach($_POST as $key=>$value)
		{
			echo "POST ".$key."=".$value."<br/>";
		}

		echo "<hr/>";

		foreach($_GET as $key=>$value)
		{
			echo "GET ".$key."=".$value."<br/>";
		}

		echo "<hr/>";

		foreach($_FILES as $key=>$value)
		{
			echo "FILES ".$key."=".$value."<br/>";
		}

		echo "<hr/>";

		foreach($_REQUEST as $key=>$value)
		{
			echo "REQUEST ".$key."=".$value."<br/>";
		}

		echo "<hr/>";

		foreach($_SESSION as $key=>$value)
		{
			echo "SESSION ".$key."=".$value."<br/>";
		}

		echo "<hr/>";

		foreach($_ENV as $key=>$value)
		{
			echo "ENV ".$key."=".$value."<br/>";
		}

		echo "<hr/>";

		foreach($_COOKIE as $key=>$value)
		{
			echo "COOKIE ".$key."=".$value."<br/>";
		}

		echo "<hr/>";
    }
}
?>
