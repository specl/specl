<?php
/**
 * Description of EventCallback
 *
 * @author tony
 */
class EventCallback {
    public $event;
    public $objToCall;
    public $methodName;
    public $userData;
    
    public function __construct($event, $objToCall, $methodName, $userData) {
        $this->event = $event;
        $this->objToCall = $objToCall;
        $this->methodName = $methodName;
        $this->userData = $userData;        
    }
    
    public function fire($data) {        
        return $this->objToCall->{$this->methodName}($data,$this->userData);
    }
}

?>
