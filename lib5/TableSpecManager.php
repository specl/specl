<?php
/* This component manages spec data that represents a data table.
 * A new component can be written to handle more complex spec structures.
 * 
 */

// TODO: Turn SpecManager into a component and 
//       have table spec manager extend a spec manager component.
// TODO: Turn specDef classes into components

class TableSpecManager extends Component {
    
    public $specName;
    public $sourceType;
    
    
    public function __construct($params)
    {   
        parent::__construct($params);
        
        if(isset($params["specName"]) && !is_null($params["specName"]))
        {
            $this->specName=$params["specName"];
        }
        else
        {
            $this->specName=$this->id;
        }
        
        if(isset($params["specName"]) && !is_null($params["specName"]))
        {
            $this->specName=$params["specName"];
        }
        else
        {
            $this->specName=$this->id;
        }
    }
    
    
}

?>