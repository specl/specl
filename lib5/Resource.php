<?php
/**
 * @author Tony
 */
class Resource {
        
    public $id;    
    public $title;
    
    public $parameters; // parameters for this NavElement

    public $allowAccess;
    public $allowDirectAccess;                
    public $rolesReq; // roles required to access this NavElement
    
    public $showInNav;
    public $showInNavWhen;
    public $showInState;
    public $showWhenLoggedIn;    
    
    public $children;    
    
    public function  __construct() {
        $this->id = null;
        $this->title = null;
        
        $this->allowAccess = true;
        $this->allowDirectAccess = true;
        $this->rolesReq = array();
        $this->parameters = array();
        $this->showInNav = true;
        $this->showInNavWhen = null;
        $this->showInState = null;
        $this->showWhenLoggedIn = null;           
        
        $this->children = array();
    }

    public function setId($value) {
        $this->id = sanitize($value, PARANOID);
        if (is_null($this->title)) {
            $this->title = $this->id;
        }
    }

}
?>