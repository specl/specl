<?php
/** @ObjDefAnn(objDefId="SQLWhereClause")  
 */
class SQLWhereClause {
    
    /** @PropDefAnn(propId="leftHS",propType="object",objectType="string")       
     ** @DescriptionAnn("left hand side");
     */    
    public $leftHS; 
    
    /** @PropDefAnn(propId="rightHS",propType="object",objectType="string")       
     ** @DescriptionAnn("right hand side");
     */        
    public $rightHS;
    
    /** @PropDefAnn(propId="operator",propType="object",objectType="string")       
     ** @DescriptionAnn("operator hand side");
     */        
    public $operator;
    
    /** @PropDefAnn(propId="$conjunction",propType="object",objectType="string")       
     ** @DescriptionAnn("$conjunction hand side");
     */        
    public $conjunction;
    
    public function __construct($leftHS, $operator, $rightHS) { 
        
        $this->validateOperator($operator);
        $this->validateLeftHS($leftHS);
        $this->validateRightHS($rightHS);
        
        $this->leftHS = $leftHS; 
        $this->rightHS = $rightHS; 
        $this->operator = strtolower(trim($operator)); 
        $this->conjunction=null;
    }
    
    /** @NotImplementedAnn() */    
    public function validateOperator($operator){
       
    }
    
    /** @NotImplementedAnn() */    
    public function validateLeftHS($leftHS){
        
    }
    
    /** @NotImplementedAnn() */    
    public function validateRightHS($rightHS){
        
    }
    
    public function generateSQLClause(){
        $operator=strtoupper($this->operator);
        return "{$this->leftHS} {$this->operator} {$this->rightHS}";
    }
}

?>
