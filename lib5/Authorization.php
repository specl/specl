<?php
/**
 * Description of Authorization
 *
 * @author tony
 */
class Authorization {

    public function  __construct() {
        
    }

    // this function is not used by anything, yet.
    public function setSessionToLoggedInState($loginMessage) {
        global $app;

        $app->sessionManager->setStateLoggedIn($app->sessionId,$loginMessage->username);
    }

    public function loginAttempt($loginMessage) {
        global $log;
        global $app;

        if (get_class($loginMessage)!="LoginMessage") {
            $log->error("Authorization->attemptLogin","not passed a LoginMessage");
            return false;
        }

        $sql = "select exists(select * from UserAccount where username=%username%
                and password=%password% limit 1) as Success;";

        $app->sqlBuilder->setStatement($sql);
        $app->sqlBuilder->createParam("username", "string", $loginMessage->username);
        $app->sqlBuilder->createParam("password", "string", $loginMessage->password);
        $sql = $app->sqlBuilder->bindParams();
        
        $results = $app->dbConnSys->select($sql);

        if ($app->dbConnSys->rows!=0)
        {
            $row = $app->dbConnSys->getRowAssoc($results);

            $success = $row['Success'];

            if ($success==0) {
                return false;
            } else {

                $app->sessionManager->setStateLoggedIn($app->sessionId,$loginMessage->username);

                return true;
            }
        } else {
            return false;
        }
    }
}
?>
