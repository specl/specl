<?php

class Action {
    
    public $id;  // identifies which component to route this action to
    public $actionName;   // the name of this action used by the client
    public $functionName; // the name of this function to dispatch on the server,
                          // the component must have this function
    
    public $actionType; // can be 'page' or 'webservice'
    public $urlParameters;
    public $postParameters;
    
    public function __construct($actionType,$actionName,$functionName,$id) {        
        $this->actionName = $actionName; 
        $this->functionName = $functionName; 
        $this->id = $id;
        $this->actionType=$actionType;
    }
    
    public function addUrlParameter($key, $value) {
        if (!array_key_exists($key, $this->urlParameters)) {
            $this->urlParameters[$key] = $value;
        }
    }  
    
    public function addPostParameter($key, $value) {
        if (!array_key_exists($key, $this->oostParameters)) {
            $this->postParameters[$key] = $value;
        }
    }
    
    public function checkParameters()
    {
        // Are the proper parameters in the URL or POST to execute this action
    }
    
    
}
?>
