<?php
/*
 * The class provide the definition of a datasource. A datasource is an object comprised of data from
 * one or more sources. The datasource specifies where the data comes from and what the type of the datasource
 * is. It is made of the following properties:
 *      objectName: This is the name of the object that is the same name as the name used in the query model, this
 *      connects the query model to the datasource model.
 *      sourceType: Specifies the type of the datasource such as a relational database or an in-memory datasource,
 *      a datasource model will have different parameters depending on the datasource type.
 */
class DatasourceModel {
    public $objectName;
    public $sourceType;
}

/*
 *  This class provides the definition of a datasource that is a table in relational database. It has the following properties:
 *      databaseName: the name of the database where the data is found
 *      objDef: the name of the PHP objDef associated with this table
 */
class RDTableSourceModel extends DatasourceModel{
    public $databaseName;
    public $objDef;
    
    public function __construct(){
        $this->sourceType="RelationalDatabaseTable";
    }
}

/*
 * This class provides the definition of a datasource that is define as a global variable in memory. This will typically
 * be used to get data from global app data. It has the following properties:
 *      path: The path to where the data object is located starting with a / followed by the top level global object where
 *      the data can be found. Example: /app/acManager/userObj points to the object userObj inside the acManager which is
 *      inside the global app variable.
 */
class GlobalObjectSourceModel extends DatasourceModel{
    
}


?>