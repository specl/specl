<?php
/*
 *  All classes in this file define a standard query model and the different pieces of a standard
 *  query. They should all be importable and exportable to/from JSON.
 */

/*
 *  This class provides a definition of a standard get model. It is defined by the following properties
 *      object: the name of the object to get the data from, this should correspond to a datasource model
 *      properties: This can be a single string, a list of strings or the string "all", specifying which properties
 *      to get from the object.      
 *  
 */
class StandardGetModel {
    public $objectName;
    public $properties;
}

/*
 *  This class provides a definition of a standard filter model. It is defined by the following properties
 *      filterOperation: The name of the operation associated with the filter, this can be a standard filter
 *      type or the name of a custom function. A custom function should have a function source model associated
 *      with it that specifies where the custom function is defined.
 *      property: a get model or a query model specifying what data to filter on
 *      value: a primative value type, an object model, or a query model specifying to values to filter on
 */
class StandardFilterModel{
    public $filterOperation;
    public $property;
    public $value;
}
/*
 * This class provides a definition of a standard sort model. It is defined by the following properties:
 *      property: a get model, or a query model specifying the data to sort on
 *      direction: "asc" or "desc", "asc" by default.
 */
class StandardSortModel{
    public $property;
    public $direction;
    
    public function __construct(){
        $this->direction="asc";
    }
}

/*
 * This class provides a definition of a standard limit model. It is defined by the following properties:
 *      start: the row number to start on
 *      numToGet: the number of rows to get from the start.
 */
class StandardLimitModel{
    public $start;
    public $numToGet;
}

/**
 *  This class provides a definition of a standard query model. It is defined by the following
 *  pieces:
 *      getModels: Describes which object and properties to get. Is made of a an array of getModels.
 *      filtersModels: Describes the objects and properties from where the query filters come from. Is
 *      made of an array of filterModels
 *      sortModels: Describes the objects and properties from where the data to sort the query come from. Is
 *      made of an array of sortModels
 *      limitModel: limit the number of results. Made of a single limitModel.
 * 
 * All objects used in the query must be defined by a datasource when the query is executed. The
 * datasources can come from multiple physical sources including relational databases, session variables,
 * and global app variables.
 * 
 * TODO: Make a nicer api to build queries.
 *      
 */
class QueryModel {
    public $getModels;
    public $filterModels;
    public $sortModels;
    public $limitModel;
    
    public function __construct(){
        $this->getModels=array();
        $this->filterModels=array();
        $this->sortModels=array();
        $this->limitModel=null;
    }
}

?>