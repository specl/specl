<?php
/*
 *  The QueryEngine class executes a model based on three things:
 *      - A Query Model: Defines what data to get
 *      - Datasource Models: Defines where the data for each object used in the query is coming from and
 *        what type of datasource it is. An object can only be associated with one datasource.
 *      - Physical Models: Defines how to get each piece of data from a particular datasource. In our case,
 *        we use specs to define the relational database physical layer.
 *
 *  The QueryEngine also needs to optimize the queries so that as much of the work as possible is done in the
 *  database, whose operations have been heavily optimized and can be executed quickly and also to avoid transfering
 *  all the data over the network.
 * 
 *  This QueryEngine is table based and returns a single flat table of data, it does not get nested data. It does
 *  not return trees or graphs either.
 */

// TODO: Seperate out methods into seperate classes where appropriate.
// TODO: Create a new SelectStatementBuilder class only with the features necessary to be used
//       in the QueryEngine.
class QueryEngine {
    public $datasourceModels;
    public $queryModel;
    public $physicalModels;
    public $useSpecsForPhysicalModel;
    // stores the queries for each object to get data from the physical layer.
    protected $physicalQueries;
    // store all the results of the physical queries
    protected $physicalQueriesResults;
    
    public function __construct(){
        $this->datasourceModels=array();
        $this->queryModel=null;
        $this->physicalModels=array();
    }
    
    /*
     * This is the entry point to the query execution engine. The user builds a query model and passes
     * it into the queryExecution engine. The query model is the analyzed and processed based on a complex 
     * series of rules. Each rule and action is defined by a seperate function that are combined together in
     * the main flow of this function.
     */
    public function executeQuery($queryModel){
        $this->queryModel=$queryModel;
        $this->physicalQueries=array();
        foreach($queryModel->getModels as $getModel){
            $this->processGetModel($getModel);
        }
        $this->processFilterModels($queryModel);
        $this->executePhysicalQueries();
        $results=$this->mergeResults();        
        $results=$this->applyTransforms($results);
        return $results;
    }
    
    /*
     *  Process the get model
     * 
     *  Right now this function just checks the type of the datasource and calls the appropriate function
     *  to build up the query.
     */
    public function processGetModel($getModel){
        $datasource=$this->datasourceModels[$getModel->objectName];
        if($datasource->sourceType=="RelationalDatabaseTable"){
            $this->processGetModelRDTable($getModel,$datasource);              
            $this->processLimitModelRDTable();
        }
    }
    
    /*
     *  Handle processing of filter statements
     */
    public function processFilterModels($queryModel){
        foreach($queryModel->filterModels as $filterModel){            
            // if the property of the filter is defined by a get model, process the model like a standard
            // model except keep the property field blank. This ensures that the data needed for the filter is
            // included in the query. Then add a filter to the appropriate physical model.
            $propertyModel=$filterModel->property;
            $datasource=null;
            if(is_a($propertyModel,"StandardGetModel")){
                $getModel=new \StandardGetModel();
                $getModel->objectName=$propertyModel->objectName;
                $getModel->properties=array();
                $this->processGetModel($getModel);
                
                $datasource=$this->datasourceModels[$getModel->objectName];
            }
            
            if($datasource->sourceType=="RelationalDatabaseTable"){
                $this->processFilterModelRDTable($filterModel,$datasource);
            }
            
            
            
        }
    }
    
    /**
     *  This function takes a relational datasource table model and a getModel and adds the appropriate
     *  columns to a SQLBuilder.
     * 
     *  This function uses the SelectStatementBuilder PHP class as the model to build the SQL query. There is
     *  one query for all of the Relational Database datasource types from the same database.
     * 
     *  This function also requires access to the App class to retrieve Specs.
     * 
     *  The columns are retrieved from the properties defined in the objDef under the following circumstances:
     *      - the property is not a collection, the objects inside a collection are not part of the current object
     *      - the property is not derived, derived columns should be handled in the applyTransforms part of the query engine
     *      - the properties excludeFromQuery is property is not set to True, this is used for transform columns that don't
     *        exist in the database. This was used in fileupload, where a property existed in the file upload spreadsheet but
     *        not in the database.
     */
    public function processGetModelRDTable($getModel,$datasource){
        global $app;
        $dbName=$datasource->databaseName;
        if(!isset($this->physicalQueries["RD_{$dbName}"])){
            $this->physicalQueries["RD_{$dbName}"]=new SelectStatementBuilder();
        }
        $SQLBuilder=$this->physicalQueries["RD_{$dbName}"];
        $objDef=$app->specManager->findDef($datasource->objDef);
        
        
        if(count($SQLBuilder->tables)==0){
            $SQLBuilder->addTable($objDef->getAttribute("table"));
        }
        // For each new table that is added the appropriate joins must be added to the selectbuilder
        // to include all of the appropriate data in the query.
        else if (!array_key_exists($objDef->getAttribute("table"), $SQLBuilder->tables)){
            // pick an arbitrary table to join the new table to, in this case just pick the first table
            // in the list.
            $keys=array_keys($SQLBuilder->tables);
            $firstTable=$keys[0];
            $SQLBuilder->addTable($objDef->getAttribute("table"));
            
            // TODO: write function that adds all of the appropriate joins to the table
            //       given the names of two tables, based on a graph of the objDefs and how
            //       they are connected.
            $SQLBuilder->addAllJoins($firstTable,$objDef->getAttribute("table"));
            
        }
        
        
        
        if($getModel->properties==="all"){
            foreach($objDef->getProperties() as $propDef){                
                if(!$propDef->isCollection() && !$propDef->getAttribute("isDerived",false) && !$propDef->getAttribute("excludeFromeQuery",false)){
                    $SQLBuilder->addColumn($propDef->getAttribute("column"),$objDef->getAttribute("table"),null,$objDef->getAttribute("caption"));
                }
            }
        }
        else if(is_string($getModel->properties)){
            $propDef=$objDef->getProperty($getModel->properties);
            if($propDef!==null){
                $SQLBuilder->addColumn($propDef->getAttribute("column"),$objDef->getAttribute("table"),null,$objDef->getAttribute("caption"));
            }
            else{
                //report error somehow that the property specified does not exist. This should be checked in the query model validation.
            }
        }
        else if(is_array($getModel->properties)){
            foreach($getModel->properties as $propName){
                $propDef=$objDef->getProperty($propName);
                if($propDef!==null){
                    $SQLBuilder->addColumn($propDef->getAttribute("column"),$objDef->getAttribute("table"),null,$objDef->getAttribute("caption"));
                }
                else{
                    //report error somehow that the property specified does not exist. This should be checked in the query model validation.
                }
            }
        }   
    }
    
    public function processLimitModelRDTable(){
        if($this->queryModel->limitModel!==null && $this->checkAddLimitToSelectStatemenBuilder()===True){
            $limitModel=$this->queryModel->limitModel;
            $SQLBuilder->startingRow=$limitModel->start;
            $SQLBuilder->numberOfRowsToGet=$limitModel->numToGet;
        }
    }
    
    public function processFilterModelRDTable($filterModel,$datasource){
        if($this->checkAddFilterToSelectStatementBuilder($filterModel)){
            
        }
        
    }
    
    
    
    // Check limit model to see it if should be added directly to the select statement builder.
    // This is just a dummy function right now, but this needs to be completed as data is retrieved
    // from multiple datasources.
    // One case where this is true is when all the objects to get are all in the same relational database.
    public function checkAddLimitToSelectStatemenBuilder(){
        return true;
    }
    
    // Check filter model to see it if should be added directly to the select statement builder.
    // This is just a dummy function right now, but this needs to be completed as data is retrieved
    // from multiple datasources. This will most likely require checking the query and datasource model
    public function checkAddFilterToSelectStatementBuilder($filterModel){
        return true;
    }
    
    /*
     *  Add a datasource model to the query engine. The objectName property of the datasource is
     *  used as the key to store the datasource models in an associative array.
     * 
     *  TODO: The datasource models must also be validated before being added to the QueryEngine. This can include:
     *         - checking for required parameters
     *         - validating other parameter based rules and conflicts between parameter settings
     *         - checking to make sure that the datasources actually exist.
     * 
     *  This function checks for duplicate datasources and raise an error. Adding two datasources with the same name
     *  is most likely an error that can lead to unexpected behavior if the second datasource overwrites the first.
     * 
     *  TODO: Check to see if objectName is set in the datasource or if it is empty and raise an error
     */
    public function addDatasource($datasource){
        if(!isset($this->datasourceModels[$datasource->objectName])){
            $this->datasourceModels[$datasource->objectName]=$datasource;
        }
        else{
            throw new Exception("Duplicate Datasource error: The object {$datasource->objectName} already has a datasource defined");
        }
    }
    
    /**
     * Execute all of the physical queries and store the results in memory     *     
     * 
     * This function just routes the physical query to the appropriate execution type
     * based either on the queryName or by the class type of the queryObj.
     *      
     */
    public function executePhysicalQueries(){
        $this->physicalQueriesResults=array();
        
        foreach($this->physicalQueries as $queryName=>$queryObj){
            if(preg_match("/^RD.*/",$queryName)){
                $this->physicalQueriesResults[$queryName]=$this->executeRelationalQuery($queryObj);                
            }
        }
    }
    
    /**
     * Executes a Relational Database query from a SelectStatementBuilder.
     * 
     * This function gets the connection from the app
     *  
     *  TODO:
     *      Handle More than one connection and database
     */
    public function executeRelationalQuery($queryObj){
        global $app;
        
        $SQL = $queryObj->generateSQLSelectStatement();
        $conn = $app->getConnection();
        $results = $conn->select($SQL);
        $data = array();
        
        while ($row = mysql_fetch_assoc($results)) {
            $data[] = $row;
        }
        return $data;
    }
    
    /*
     * Merge the results of all the physical queries.
     */
    public function mergeResults(){
        if(count($this->physicalQueriesResults)==1){
            $valArray=array_values($this->physicalQueriesResults);
            return $valArray[0];
        }
    }
    
    /*
     * Right now this function does nothing, this needs to be changed in the future.
     */
    public function applyTransforms($results){
        return $results;
    }
            
}

?>