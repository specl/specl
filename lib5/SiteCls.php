<?php
/**
 * Description of Site
 *
 * @author tony
 */
class SiteCls extends SpecCls {

    public $id;
    public $title;
    public $tagLine;
    public $description;
    public $dbPath;
    public $sessionDBPath;
    public $templatePath;
    public $templateName;
    public $defaultController;
    public $defaultResourceId;
    public $siteName;
    public $webAddress;
    public $adminEmail;
    
    private $settings;
    
    public function  __construct() {
        
        parent::__construct('Site');
        
        $this->id = "untitled";
        $this->title = "untitled";
//        $this->tagLine = null;
//        $this->description = null;
//        $this->dbPath = null;
//        $this->sessionDBPath = null;
//        $this->templatePath = null;
//        $this->templateName = null;
//        $this->defaultController = null;
//        $this->defaultResourceId = null;
        
        $this->settings = array();
        
        $this->webAddress = curBaseURL();
                                
    }
        


    public function getSetting($key, $defaultValue=null) {
        
        if (isset($this->{$key})) {
            return $this->{$key};
        }
        
        if (isset ($this->settings[$key])) {
            return $this->settings[$key];
        } else {
            return $detaultValue;
        }
    }
}
?>
