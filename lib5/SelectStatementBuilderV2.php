<?php
require_once("QueryBuilder.php");
/**
 * The purpose of this class is to provide an api to build up a SQL select statement incrementally.
 * 
 * The SelectStatementBuilder follows the following rules:
 *      - a select statement has only one table, the rest of the tables are joins.
 *      - all columns must have a unique name or alias
 *      - all joins must have a unique name or alias
 */
class SelectStatementBuilderV2 extends QueryBuilder {
    protected $columns;
    protected $table;
    protected $joinTables;
    protected $limit;
    protected $whereStatement;
    protected $distinct;
    protected $sortColumns;
    protected $sortDirection;
    protected $groupByStatements;
    
    public function __construct(){
        $this->columns=array();
        $this->table=null;
        $this->joinTables=array();
        $this->filters=array();
        $this->distinct=false;
        $this->sortColumns=array();
        $this->groupByStatements=array();
        $this->limit=null;
    }
    
    /*
     * This function creates a standard SQLColumn (no subqueries or functions).
     */
    public function addColumn($column,$tableName,$columnAlias=Null){        
        if($columnAlias===null){
            $columnKeyName=$column;
        }
        else{
            $columnKeyName=$columnAlias;
        }  
        //do not add duplicate column names
        if(!array_key_exists($columnKeyName,$this->columns)){
            $this->columns[$columnKeyName]=new SQLColumnV2($column,$tableName,$columnAlias);
        }
    }
    
    public function addCountColumn(){
        if(!isset($this->columns["count"])){
            $this->columns["count"]=new SQLCountColumn();
        }
    }
    
    public function setTable($tableName,$tableAlias=null){
        $this->table=new SQLTableV2($tableName, $tableAlias);
    }
    
    public function addInnerJoinTable($fromTable,$toTable,$fromColumn,$toColumn,$alias=null){
        $this->addJoin($fromTable,$toTable,$fromColumn,$toColumn,$alias,"INNER");
    }
    
    public function addOuterJoinTable($fromTable,$toTable,$fromColumn,$toColumn,$alias=null){
        $this->addJoin($fromTable,$toTable,$fromColumn,$toColumn,$alias,"LEFT OUTER");
    }
    
    /*
     * Add a join table to the SQL query following the following rules:
     *      - each join table must be unique based on either the toTable or the alias name if it has one
     *      - if a join name or alias is passed in more than once the multiple conditions can be added to
     *      to same join.
     */
    public function addJoin($fromTable,$toTable,$fromColumn,$toColumn,$alias,$joinType){
        if($alias===null){
            $joinKeyName=$toTable;
        }
        else{
            $joinKeyName=$alias;
        }
        if(!array_key_exists($joinKeyName,$this->joinTables)){
            $joinTable=new SQLJoinTable($joinType,$toTable);
            $this->joinTables[$joinKeyName]=$joinTable;
        }
        else{
            $joinTable=$this->joinTables[$joinKeyName];
        }
        $joinTable->addCondition($fromTable,$fromColumn,$toColumn);
    }
    
    public function addSortColumn($tableName,$columnName,$sortOrder="ASC")
    {
        $this->sortColumns[]=new SQLSortColumn($tableName,$columnName,$sortOrder);
    }
    
    public function addValueFilter($tableName, $columnName, $operation, $value, $conjunction="AND"){
        $this->filters[]=new SQLValueFilter($tableName, $columnName, $operation, $value, $conjunction);
    }
    
    public function addFilter($filterObj){
        $this->filters[]=$filterObj;
    }
    
    public function addLimit($numRows,$startingRow=0){
        $this->limit=new SQLLimitV2($numRows,$startingRow);
    }
    
    public function addGroupBy($tableName,$columnName){
        // group by a specific column
        $this->groupByStatements[]=new SQLColumnV2($columnName,$tableName,null);
    }
    
    public function generateSQLSelectStatement(){
        $selectStatement="SELECT ";
        if($this->distinct===true){
            $selectStatement.="DISTINCT ";
        }
        $first=true;
        foreach($this->columns as $key=>$column){
            if($first){
                $sep="";
                $first=false;
            }
            else{
                $sep=",";
            }
            $selectStatement.="{$sep}".$column->generateSQL();
        }
        $selectStatement.=" FROM `{$this->table->tableName}`";
        if($this->table->tableAlias!==null){
            $selectStatement.=" as {$this->table->tableAlias}";
        }
        
        foreach($this->joinTables as $key=>$joinTable){
            $selectStatement.="\n".$joinTable->generateSQL();
        }
        if(count($this->filters)>0){
            $selectStatement.="\n WHERE ";
            $first=true;
            foreach($this->filters as $filter){
                if($first){
                    $selectStatement.=$filter->generateSQL();
                    $first=false;
                }else{
                    $selectStatement.="\n ".$filter->conjunction." ".$filter->generateSQL();
                }
            }
        }
        
        if(count($this->groupByStatements)>0){
            $groupByStatement=" GROUP BY ";
            $sep="";
            foreach($this->groupByStatements as $groupBy){                
                $groupByStatement.=$sep.$groupBy->generateSQL();                
                if($sep==""){
                    $sep=",";
                }
            }
            $selectStatement.=$groupByStatement;
        }
        
        if(count($this->sortColumns)>0){
            $orderByStatement=" ORDER BY ";
            $sep="";
            foreach($this->sortColumns as $sortCol){
                $orderByStatement.=$sep.$sortCol->tableName.".".$sortCol->columnName." ".$sortCol->direction;                
                if($sep==""){
                    $sep=",";
                }
            }
            $selectStatement.=$orderByStatement;
        }
        
        if($this->limit!==null){
            $selectStatement.=" LIMIT {$this->limit->startingRow},{$this->limit->numRows}";
        }
        
        $selectStatement.=";";
        
        return $selectStatement;
    }
}

?>
