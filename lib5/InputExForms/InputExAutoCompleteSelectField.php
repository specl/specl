<?php

class InputExAutoCompleteSelectField extends InputExCombineField {
    
    public $fieldList;
    
    public function __construct($params){
        parent::__construct($params);
        $this->fieldList=array();
        
        if(isset($params["label"])){
            $this->label=$params["label"];
        }
        else{
            $this->label="autoComplete Field";
        }
    }
    
    public function addField($fieldName,$datasource){
        $this->fieldList[]=array("fieldName"=>$fieldName,"datasource"=>$datasource);
    }
    
    public function render($strbld){
        
        $strbld->addLine("{type:'AutoCompleteSelect',name:'{$this->id}',label:'{$this->label}',");
        $strbld->addLine("fieldList:[");
        $strbld->increaseIndentLevel();
        $fields=array();
        foreach($this->fieldList as $field){
            $fieldName=$field["fieldName"];
            $datasource=$field["datasource"];
            $fields[]="{fieldName:'{$fieldName}',datasource:{$datasource}}";
        }
        $strbld->addList($fields);
        $strbld->addLine("],");
        $strbld->addLine('"autoComp":{');
        $strbld->addLine('"forceSelection":true,');
        $strbld->addLine('"typeAhead":true,');
        $strbld->addLine('"minQueryLength":1,');
        $strbld->addLine('"maxResultsDisplayed":50,');
                     
        $strbld->addLine('"formatResult":function(oResultItem, sQuery) 
                        { 
                            return oResultItem[0];
                        }');
        $strbld->addLine("}");
        $strbld->decreaseIndentLevel();
        $strbld->addLine("}");
    }
    
}

?>