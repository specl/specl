<?php

/**
 * SearchField
 *
 * @author Tony
 */
class SearchField {
    
    public $fieldName;
    public $fieldLabel;
    public $fieldType;
    public $fieldChoices; 
    public $choiceType;
    public $idType;
    public $canBeNull;
    
    public function __construct($params){
  
        if(isset($params["fieldName"]) && !is_null($params["fieldName"])) {
            $this->fieldName=$params["fieldName"];
        } else {
            $this->fieldName=Null;
        } 
        
        if(isset($params["fieldLabel"]) && !is_null($params["fieldLabel"])) {
            $this->fieldLabel=$params["fieldLabel"];
        } else {
            $this->fieldLabel=Null;
        } 

        if(isset($params["fieldType"]) && !is_null($params["fieldType"])) {
            $this->fieldType=$params["fieldType"];
        } else {
            $this->fieldType=Null;
        } 
        
        if(isset($params["fieldChoices"]) && !is_null($params["fieldChoices"])) {
            $this->fieldChoices=$params["fieldChoices"];
        } else {
            $this->fieldChoices=Null;
        } 
        
        if(isset($params["choiceType"]) && !is_null($params["choiceType"])) {
            $this->choiceType=$params["choiceType"];
        } else {
            $this->choiceType=Null;
        }  
        
        if(isset($params["url"]) && !is_null($params["url"])) {
            $this->url=$params["url"];
        } else {
            $this->url=Null;
        } 
        
        if(isset($params["canBeNull"]) && !is_null($params["canBeNull"])) {
            $this->canBeNull=$params["canBeNull"];
        } else {
            $this->canBeNull=Null;
        } 
        
        if(isset($params["handleAs"])){
            $this->handleAs=$params["handleAs"];
        }
        else{
            $this->handleAs=null;
        }
        
        if(isset($params["idType"])){
            $this->idType=$params["idType"];
        }
        else{
            $this->idType=null;
        }
        
    }
    
}

?>
