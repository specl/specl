<?php
/**
 * Description of InputExFileField
 *
 * @author tony
 */
class InputExFileField extends InputExField {
    
    public $accept;
    public $action;
    
    public function __construct($params)
    {        
        $this->fieldParams[] = "accept";
        $this->fieldParams[] = "action";
        $this->fieldParams[] = "size";

        parent::__construct($params);
        
                
        if(isset($params["accept"]) && !is_null($params["accept"]))
        {
            $this->accept=$params["accept"];
        }
        else{
            $this->accept="text/*";
        }
        
        
        if(isset($params["action"]) && !is_null($params["action"]))
        {
            $this->action=$params["action"];
        }
        else{
            $this->action=Null;
        }
        
        if(isset($params["size"])){
            $this->size=$params["size"];
        }
        else{
            $this->size=50;
        }
                
        $this->type="file2";
    } 
}

?>
