<?php
/**
 * Description of InputExColorField
 *
 * @author tony
 */
/* This field adds the following options
 * colors: list of colors to load as palette
 * palette: default palette to be used (if colors option not provided)
 * cellPerLine: how many colored cells in a row on the palette
 * ratio: screen-like ratio to display the palette, syntax: [with,height], default: [16,9] (if cellPerLine not provided)
 */
class InputExColorField extends InputExField {
    
    public $colors;
    public $palette;
    public $cellPerLine;
    public $ratio;
        
    public function __construct($params)
    {
        $this->fieldParams[] = "palette";
        $this->fieldParams[] = "cellPerLine";
        $this->fieldParams[] = "ratio";
                
        parent::__construct($params);
          
        if(isset($params["palette"]) && !is_null($params["palette"]))
        {
            $this->palette=$params["palette"];
        }

        if(isset($params["cellPerLine"]) && !is_null($params["cellPerLine"]))
        {
            $this->cellPerLine=$params["cellPerLine"];
        }

        if(isset($params["ratio"]) && !is_null($params["ratio"]))
        {
            $this->ratio=$params["ratio"];
        }
        
        $this->type="color";
    }    
    
    
    public function addColor($color) {
        
    }
}

?>
