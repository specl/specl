<?php
/* This field adds the following options
 * separators: array of string inserted
 */
class InputExCombineField extends InputExGroupField {
    
    public $separators;
        
    public function __construct($params)
    {
        $this->fieldParams[] = "separators";
                
        parent::__construct($params);
          
        if(isset($params["separators"]) && !is_null($params["separators"]))
        {
            $this->separators=$params["separators"];
        }
        
        $this->type="combine";
    }    
}

?>