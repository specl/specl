<?php
/**
 * Description of InputExRTEField
 *
 * @author tony
 */
/* This field adds the following options
 * opts: the options to be added when calling the RTE constructor (see YUI RTE)
 * editorType: if == 'simple', the field will use the SimpleEditor. Any other value will use the Editor class.
 */
class InputExRTEField extends InputExField {
        
    public function __construct($params)
    {
        $this->fieldParams[] = "opts";        
        $this->fieldParams[] = "editorType";        

        parent::__construct($params);
                        
        if(isset($params["opts"]) && !is_null($params["opts"]))
        {
            $this->opts=$params["opts"];
        }

        if(isset($params["editorType"]) && !is_null($params["editorType"]))
        {
            $this->editorType=$params["editorType"];
        }
        
        $this->type="html";
    }  
}
?>
