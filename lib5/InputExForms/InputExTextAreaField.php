<?php
/**
 * Description of InputExTextAreaField
 *
 * @author tony
 */
/* This field adds the following options
 * rows: rows attribute
 * cols: cols attribute
 */
class InputExTextAreaField extends InputExField {
        
    public function __construct($params)
    {
        $this->fieldParams[] = "rows";        
        $this->fieldParams[] = "cols";        

        parent::__construct($params);
                        
        if(isset($params["rows"]) && !is_null($params["rows"]))
        {
            $this->rows=$params["rows"];
        }

        if(isset($params["cols"]) && !is_null($params["cols"]))
        {
            $this->cols=$params["cols"];
        }
                
        $this->type="text";
    }  
}
?>
