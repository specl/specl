<?php
class InputExDateTimeField extends InputExStringField {
    
    public $dateFormat;
    public $valueFormat;
    
    public function __construct($params)
    {
        $this->fieldParams[] = "dateFormat";
        $this->fieldParams[] = "valueFormat";

        parent::__construct($params);
                
        if(isset($params["dateFormat"]) && !is_null($params["dateFormat"]))
        {
            $this->dateFormat=$params["dateFormat"];
        }
        else{
            $this->dateFormat="m/d/Y";
        }
        
        
        if(isset($params["valueFormat"]) && !is_null($params["valueFormat"]))
        {
            $this->valueFormat=$params["valueFormat"];
        }
        else{
            $this->valueFormat="Y-m-d";
        }
              
        $this->type="datetime";
    }      
}

?>