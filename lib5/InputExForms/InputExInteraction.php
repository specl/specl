<?php

/*
    interactions: [
                   {
                      valueTrigger: false, // this action will run when this field value is set to false
                      actions: [
                         {
                            name: 'username', // name of the field on which the action should run
                            action: 'show' // name of the method to run on the field instance !
                         }
                      ]
                   },
                   {
                      valueTrigger: true, // when set to true:
                      actions: [
                         {
                            name: 'username', 
                            action: 'hide'
                         },
                                     {
                                name: 'username', // name of the field on which the action should run
                                action: 'clear' // name of the method to run on the field instance !
                             }
                      ]
                   }
               ]
 */

/**
 * Description of InputExInteraction
 *
 * @author tony
 */
class InputExInteraction {
    public $valueTrigger;
    public $actions;
    
    public function __construct($params)
    {       
        if(isset($params["valueTrigger"]) && !is_null($params["valueTrigger"])) {
            $this->valueTrigger=$params["valueTrigger"];
        } else {
            $this->valueTrigger=Null;
        }
        $this->actions = array();
    }
        
    //$actionType : 'show', 'hide', 'clear'
    public function addAction($fieldName, $actionType) {
        //
        // TODO: validate $fieldName, does it exists?  this is hard to do 
        // before they are all added.
        // validate $actionType, check a enumeration of valid types.
        //
        $this->actions[] = new InputExAction($fieldName, $actionType);
    }
    
    public function render($strbld)
    {     
        $strbld->addLine('{', false);
        $strbld->addLine('"valueTrigger":', false);
        
        if (is_bool($this->valueTrigger))        
        {
            if ($this->valueTrigger==true) { $val = "true"; } else { $val = "false"; };        
        } else if (is_string($this->valueTrigger))  {
            $val = "\"{$value}\"";
        } else if (is_numeric($this->valueTrigger))  {
            $val = "{$value}";
        } else {
            $val = "{$value}";
        }

        $strbld->addLine("{$val},",false);
                    
        $strbld->addLine('"actions":[', false);        
        $sep="";
        foreach($this->actions as $action)
        {
            $strbld->addLine($sep.'{"name":"'.$action->fieldName.'","action":"'.$action->actionType.'"}',false);              
            $sep=",";
        }                        
        $strbld->addLine(']', false);        
        $strbld->addLine("}\n", false);        
    
    }    
}

?>
