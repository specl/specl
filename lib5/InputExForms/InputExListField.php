<?php
/**
 * Description of InputExListField
 *
 * @author tony
 */
/* This field adds the following options
 * sortable: Add arrows to sort the items if true (default false)
 * elementType: an element type definition (default is )
 * useButtons: use buttons instead of links (default false)
 * unique: require values to be unique (default false)
 * listAddLabel: if useButtons is false, text to add an item
 * listRemoveLabel: if useButtons is false, text to remove an item
 * maxItems: maximum number of items (leave undefined if no maximum, default)
 * minItems: minimum number of items to validate (leave undefined if no minimum, default)
 */
class InputExListField extends InputExField {
    
    public $fields; // array of fields
        
    public function __construct($params)
    {
        $this->fieldParams[] = "sortable";
        $this->fieldParams[] = "elementType";
        $this->fieldParams[] = "useButtons";
        $this->fieldParams[] = "unique";
        $this->fieldParams[] = "listAddLabel";        
        $this->fieldParams[] = "listRemoveLabel";
        $this->fieldParams[] = "maxItems";
        $this->fieldParams[] = "minItems";
        $this->fieldParams[] = "disableAddRemove";
        $this->fieldParams[] = "collapsible";
        $this->fieldParams[] = "collapsed";
                
        parent::__construct($params);
          
        if(isset($params["sortable"]) && !is_null($params["sortable"]))
        {
            $this->sortable=$params["sortable"];
        }
        
        if(isset($params["elementType"]) && !is_null($params["elementType"]))
        {
            $this->elementType=$params["elementType"];
        }
        
        if(isset($params["useButtons"]) && !is_null($params["useButtons"]))
        {
            $this->useButtons=$params["useButtons"];
        }

        if(isset($params["unique"]) && !is_null($params["unique"]))
        {
            $this->unique=$params["unique"];
        }

        if(isset($params["listAddLabel"]) && !is_null($params["listAddLabel"]))
        {
            $this->listAddLabel=$params["listAddLabel"];
        }

        if(isset($params["listRemoveLabel"]) && !is_null($params["listRemoveLabel"]))
        {
            $this->listRemoveLabel=$params["listRemoveLabel"];
        }

        if(isset($params["maxItems"]) && !is_null($params["maxItems"]))
        {
            $this->maxItems=$params["maxItems"];
        }

        if(isset($params["minItems"]) && !is_null($params["minItems"]))
        {
            $this->minItems=$params["minItems"];
        }
        
        if(isset($params["disableAddRemove"]) && !is_null($params["disableAddRemove"]))
        {
            $this->disableAddRemove=$params["disableAddRemove"];
        }
        
        if(isset($params["collapsible"]) && !is_null($params["collapsible"]))
        {
            $this->collapsible=$params["collapsible"];
        }
        
        if(isset($params["collapsed"]) && !is_null($params["collapsed"]))
        {
            $this->collapsed=$params["collapsed"];
        }
        
        
        $this->fields = array();
        
        $this->type="list2";
    }    
    
    public function addField($fieldObj)
    {
        $this->fields[]=$fieldObj;    
    }
           
    public function render($strbld)
    {
        $strbld->addLine("{", false);
        parent::renderParams($strbld);         
        
        $strbld->addLine(',"elementType": { "type":"group",', false,false);
        $strbld->addLine('"fields":[',false,false);
        $strbld->increaseIndentLevel();                

        $sep="";
        foreach($this->fields as $field) {                        
            $strbld->addLine($sep,true,false);            
            $field->render($strbld);                       
            $sep=",";
        }
        $strbld->decreaseIndentLevel();
        $strbld->addLine(']',true,false);
        
        $strbld->addLine("}", false);        
        $strbld->addLine("}", false,false);
    }        
}
?>
