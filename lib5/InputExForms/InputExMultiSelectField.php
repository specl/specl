<?php
/**
 * Description of InputExMultiSelectField
 *
 * @author tony
 */
/* This field adds the following options
 * choices: contains the list of choices configs ([{value:'usa'}, {value:'fr', label:'France'> 
 */
class InputExMultiSelectField extends InputExSelectField {
        
    public $choices;        

    public function __construct($params)
    {        
        $this->choices = array();

        parent::__construct($params);
                        
        $this->type="multiselect";
    }  
}
?>
