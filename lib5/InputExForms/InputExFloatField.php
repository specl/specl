<?php
/* This field adds the following properties
 *      min
 *      max
 *      
 */


class InputExFloatField extends InputExStringField{
    
    public $min;
    public $max;
    
    public function __construct($params)
    {
        $this->fieldParams[] = "min";
        $this->fieldParams[] = "max";

        parent::__construct($params);
        
        
        if(isset($params["min"]) && !is_null($params["min"]))
        {
            $this->min=$params["min"];
        }
        
         if(isset($params["max"]) && !is_null($params["max"]))
        {
            $this->max=$params["max"];
        }
        
        $this->type="number";
    }
    
}

?>