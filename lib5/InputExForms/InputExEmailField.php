<?php
/**
 * Description of InputExEmailField
 *
 * @author tony
 */
/* This field adds the following options
 * 
 */
class InputExEmailField extends InputExStringField {

    public function __construct($params)
    {        
        parent::__construct($params);
                        
        $this->type="email";
    }  
}
?>
