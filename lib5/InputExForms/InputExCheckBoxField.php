<?php

// TODO: add interactions

/* This field adds the following options
 *      - sentValues: 2D vector of values for checked/unchecked states 
 *                    (default is [true, false])
 */
class InputExCheckBoxField extends InputExField {
    
    public $sentValues;
        
    public function __construct($params)
    {
        $this->fieldParams[] = "sentValues";
        $this->fieldParams[] = "rightLabel";
        $this->fieldParams[] = "readOnly";
        
        parent::__construct($params);
          
        if(isset($params["sentValues"]) && !is_null($params["sentValues"]))
        {
            $this->sentValues=$params["sentValues"];
        } else {
            $this->sentValues=Null;
        }
        
        if(isset($params["rightLabel"]) && !is_null($params["rightLabel"]))
        {
            $this->rightLabel=$params["rightLabel"];
        } else {
            $this->rightLabel=Null;
        }

        if(isset($params["readOnly"]) && !is_null($params["readOnly"]))
        {
            $this->readOnly=$params["readOnly"];
        } else {
            $this->readOnly=Null;
        }
        
        $this->type="boolean";
    }    
}
?>