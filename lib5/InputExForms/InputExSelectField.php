<?php
/* This field adds the following options:
 *      choices: an array of key-value pairs representing label:value
 * 
 */


class InputExSelectField extends InputExField {
    
    public $choices;
        
    public function __construct($params){
        
        parent::__construct($params);
        
        $this->choices=array();
        if( isset($params["choices"]) && is_array($params["choices"]))
        {
            foreach($params["choices"] as $key=>$value)
            {
                $this->choices[$key]=$value;
            }
        }
        
        $this->type="select";
    }
    
    public function addChoice($key,$value=null)
    {
        if(is_null($value))
        {
            $value=$key;
        }
        $this->choices[$key]=$value;
    }
    
    public function render($strbld)
    {
        $strbld->addLine("{", false);
        parent::renderParams($strbld);    
        
        // now add choices
        $strbld->addLine(',"choices":[', false,false);
        $sep="";
        foreach($this->choices as $key=>$value)
        {
            $strbld->addLine($sep."{\"value\":\"{$key}\",\"label\":\"{$value}\"}",false,false);              
            $sep=",";
        }                        
        $strbld->addLine(']', false,false);        
        $strbld->addLine("}", false,false);
    }
}

?>