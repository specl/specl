<?php

class InputExButtonField extends InputExField {
    
    public $buttonText;
    public $callbackFunction;
    
    public function __construct($params){
        parent::__construct($params);
        
        $this->type="button";
        
        $this->fieldParams[]="buttonText";
        $this->fieldParams[]=array("callbackFunction","function");
        
        if(isset($params["buttonText"])){
            $this->buttonText=$params["buttonText"];
        }
        else{
            throw new Exception("buttonText is a required parameter");
        }
        
        if(isset($params["callbackFunction"])){
            $this->callbackFunction=$params["callbackFunction"];
        }  
    }
}

?>