<?php
/*
 * Description of InputExGroupField
 *
 * @author tony
 * 
 * 
 * This field adds the following properties
    fields: Array of input fields declared like { label: 'Enter the value:' , type: 'text' or fieldClass: inputEx.Field, optional: true/false, ... >
    legend: The legend for the fieldset (default is an empty string)
    collapsible: Boolean to make the group collapsible (default is false)
    collapsed: If collapsible only, will be collapsed at creation (default is false)
    flatten:
 * 
 */
class InputExGroupField extends InputExField {
    
    public $fields;
    public $legend;
    public $collapsible;
    public $collapsed;
    public $flatten;    
        
    public function __construct($params)
    {
        $this->fields = array();
        
        $this->fieldParams[] = "legend";
        $this->fieldParams[] = "collapsible";
        $this->fieldParams[] = "collapsed";
        $this->fieldParams[] = "flatten";
        $this->fieldParams[] = "className";
        
        parent::__construct($params);
          
        if(isset($params["legend"]) && !is_null($params["legend"]))
        {
            $this->legend=$params["legend"];
        }

        if(isset($params["collapsible"]) && !is_null($params["collapsible"]))
        {
            $this->collapsible=$params["collapsible"];
        }
        
        if(isset($params["collapsed"]) && !is_null($params["collapsed"]))
        {
            $this->collapsed=$params["collapsed"];
        }
        
        if(isset($params["flatten"]) && !is_null($params["flatten"]))
        {
            $this->flatten=$params["flatten"];
        }
        
        if(isset($params["className"]) && !is_null($params["className"]))
        {
            $this->className=$params["className"];
        }
                
        $this->type="group";
    }    
    
    public function addField($fld) {
        $this->fields[] = $fld;
    }
    
    public function render($strbld=null)        
    {        
        $strbld->addLine("{", false);
        parent::renderParams($strbld);         
        
        $strbld->addLine(',"fields":[',false,false);
        $strbld->increaseIndentLevel();                

        $sep="";
        foreach($this->fields as $field) {                        
            $strbld->addLine($sep,true,false);            
            $field->render($strbld);                       
            $sep=",";
        }
        $strbld->decreaseIndentLevel();
        $strbld->addLine(']',true,false);
        
        $strbld->addLine("}", false,false);        
    }    
}
?>
