<?php
class InputExSearchField extends InputExCombineField{
    
    public $fields; // array of SearchField objects
    
    // TODO: add additional parameters
    public function __construct($params){
        parent::__construct($params);
        $this->fields=array();
    }
    
    public function addField($field){
        // TODO : check for dups.
        // what happens when there are more than one name due to joins
        $this->fields[$field->fieldName]=$field;
    }
    
    public function render($strbldr){
        $strbldr->addLine("{type:'SearchList',name:'{$this->id}',");
        $strbldr->increaseIndentLevel();
        $strbldr->addLine("elementType: {type:'search',");    
        $strbldr->increaseIndentLevel();
        $strbldr->addLine("searchFields:[");
        
        $strbldr->increaseIndentLevel();
        
        $fieldList=array();
        foreach($this->fields as $key=>$field){
            
            $str = "{name:'{$field->fieldName}',label:'{$field->fieldLabel}',type:'{$field->fieldType}'";
            
            if ($field->fieldType=='choice') {
                $str .= ",choices:[";
                
                $sep="";
                foreach($field->fieldChoices as $key=>$value) {
                    $str.=$sep."{\"label\":\"{$key}\",\"value\":\"{$value}\"}";
                    if ($sep=="") $sep=",";
                }
                $str .= "]";
            }
            
            if($field->fieldType=='autocomplete'){
                $canBeNull=BoolToStr($field->canBeNull);
                $str.=",url:'{$field->url}',canBeNull:{$canBeNull}";
                
            }

            $str .= "}";
            
            $fieldList[]=$str;
        }
        
        $strbldr->addList($fieldList);
        
        $strbldr->decreaseIndentLevel(); 
        $strbldr->addLine("]},");
        $strbldr->addLine("listLabel:'Query Builder',");
        $strbldr->addLine("label:'Search Filters',");
        $strbldr->addLine("minItems:0");
        $strbldr->addLine("}");
    }
    
}

?>