<?php

/* This field adds the following parameters
 * 
 *  regexp: regular expression used to validate (otherwise it always validate)
 *  size: size attribute of the input
 *  maxLength: maximum size of the string field (no message display, uses the maxlength html attribute)
 *  minLength: minimum size of the string field (will display an error message if shorter)
 *  typeInvite: string displayed when the field is empty
 *  readonly: set the field as readonly
 * 
 */

class InputExStringField extends InputExField {
    
    public $regexp;
    public $size;
    public $maxLength;
    public $minLength;
    public $typeInvite;
    public $readonly;
        
    public function __construct($params)
    {
        $this->fieldParams[] = "regexp";
        $this->fieldParams[] = "size";
        $this->fieldParams[] = "maxLength";
        $this->fieldParams[] = "minLength";
        $this->fieldParams[] = "typeInvite";
        

        parent::__construct($params);
        
        if(isset($params["regexp"]) && !is_null($params["regexp"]))
        {
            $this->regexp=$params["regexp"];
        }
        
        if(isset($params["size"]) && !is_null($params["size"]))
        {
            $this->size=$params["size"];
        }
        
        if(isset($params["maxLength"]) && !is_null($params["maxLength"]))
        {
            $this->maxLength=$params["maxLength"];
        } 
        
        if(isset($params["minLength"]) && !is_null($params["minLength"]))
        {
            $this->minLength=$params["minLength"];
        }
        
        if(isset($params["typeInvite"]) && !is_null($params["typeInvite"]))
        {
            $this->typeInvite=$params["typeInvite"];
        }
        
        $this->type="string";
    }    
}
?>