<?php
/* This field adds the following parameters
 * 
 *  datasource: the datasource
 *  autoComp: autocompleter options
 *  returnValue: function to format the returned value (optional)
 * 
        <script type="text/javascript" src="/yui/build/connection/connection-min.js"></script>
        <script type="text/javascript" src="/yui/build/datasource/datasource-min.js"></script>
        <script type="text/javascript" src="/yui/build/autocomplete/autocomplete-min.js"></script>
        <script type="text/javascript" src="/yui/build/paginator/paginator-min.js"></script>
        <script type="text/javascript" src="/yui/build/logger/logger-min.js"></script>
        <script type="text/javascript" src="/yui/build/selector/selector-min.js"></script>


       <script>                            
            // Use an XHRDataSource
            var oDS = new YAHOO.util.XHRDataSource("ysearch_flat.php");
            // Set the responseType
            oDS.responseType = YAHOO.util.XHRDataSource.TYPE_TEXT;
            // Define the schema of the delimited results
            oDS.responseSchema = {
                recordDelim: "\n",
                fieldDelim: "\t"
            };
            // Enable caching
            oDS.maxCacheEntries = 5;
        </script>
  
    {                                    
        name:"AutoComplete",
        label:"AutoComplete",
        type:'autocomplete',
        datasource: oDs,
        autoComp:
        {
            forceSelection: true,
            typeAhead: true,
            minQueryLength: 0,
            maxResultsDisplayed: 50,
            formatResult: function(oResultItem, sQuery) {
                var sMarkup = oResultItem[0] ;
                return sMarkup;
            }
        }
    },
 */

class InputExAutoCompleteField extends InputExStringField {
   
    public $datasource; // the URL of the datasource
    public $dsName; // the javascript variabel name of the datasource
    public $url;          
      
    public function __construct($params)
    {
        parent::__construct($params);        
        
        
        if(isset($params["datasource"]) && !is_null($params["datasource"]))
        {
            $this->datasource=$params["datasource"];
        }
        else{
            $this->datasource=null;
        }
        
        if(isset($params["dsName"]) && !is_null($params["dsName"]))
        {
            $this->dsName=$params["dsName"];
        }
        else{
            $this->dsName=null;
        }
        
        if(isset($params["url"]) && !is_null($params["url"]))
        {
            $this->url=$params["url"];
        }
        else{
            $this->url=null;
        }
        
        $this->type="autocomplete";
    }       
        
    public function render($strbld)
    {
        $strbld->addLine("{", false);
        
        if(!is_null($this->url)){
            $this->type="autocomplete2";
        }
        
        parent::renderParams($strbld);                 
        $strbld->addLine(''); 
        
        if(!is_null($this->dsName)){
            $strbld->addLine(',"datasource":'.$this->dsName.',',true);
        }
        else if(!is_null($this->url)){
            $strbld->addLine(',"url":"'.$this->url.'"');
        }
        else{
            $strbld->addLine(',"datasource":new YAHOO.util.DataSource('.$this->datasource.'),');
        }
        
        if(is_null($this->url)){
            $strbld->addLine('"autoComp":{', true);
            $strbld->increaseIndentLevel();
            $strbld->addLine('"forceSelection":true,',true);
            $strbld->addLine('"typeAhead":false,',true);
            $strbld->addLine('"minQueryLength":1,',true);
            $strbld->addLine('"maxResultsDisplayed":20,',true);               

            if(!is_null($this->dsName) || !is_null($this->url)){
                $strbld->addLine('"generateRequest":function(sQuery) { return "&fieldId='.$this->name.'&query=" + sQuery ; },',true); 
            }
            $strbld->addLines(
'"formatResult":function(oResultItem, sQuery) 
{                           
    return oResultItem[0] ;
}',true);
            $strbld->decreaseIndentLevel();
            $strbld->addLine("}",true); 
        }
           
        
        $strbld->addLine("}",false);    
    }        

       
}
?>