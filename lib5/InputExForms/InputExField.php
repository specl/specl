<?php

/* All InputExFields share the following parameters:
 *      name: the name of the field
 *      required: boolean, the field cannot be null if true
 *      className: CSS class name for the div wrapper (default 'inputEx-Field')
 *      value: initial value
 *      parentEl: HTMLElement or String id, append the field to this DOM element
 *      label
 *      description
 *      showMsg: Show error messages (default true)
 * This class is never called directly.
 * 
 */

class InputExField extends Component {
    
    public $name;
    public $required;
    public $className;
    public $label;
    public $description;
    public $showMsg;
    public $value;
    public $type;
    public $path;
    //public $disabled; 
    
    public $fieldParams = array("name", "required", "className", "label",
                                "description","showMsg", "value", "type", "visible", 
                                "interactions", "size", "readonly");
    
    public $constructParams;    
    public $outputParams;
    
    public $errorMessages; // array of error validation messages 
    public $hidden; // true or false
    public $visible; // true of false           
    public $interactions; // array of InputExInteractions objects
    
    public $objDef;
    public $propDef;
    
    public function __construct($params)
    {        
        parent::__construct($params);

        $this->name=$this->id;                
        
        if(isset($params["required"]) && !is_null($params["required"])) {
            $this->required=$params["required"];
        }
        
        if(isset($params["className"]) && !is_null($params["className"])) {
            $this->className=$params["className"];
        }
        
        if(isset($params["label"]) && !is_null($params["label"])) {
            $this->label=$params["label"];
        }
        
        if(isset($params["description"]) && !is_null($params["description"])) {
            $this->description=$params["description"];
        } 
        
        if(isset($params["showMsg"]) && !is_null($params["showMsg"])) {
            $this->showMsg=$params["showMsg"];
        } else {
            $this->showMsg=false;
        }

        if(isset($params["value"]) && !is_null($params["value"])) {
            $this->value=$params["value"];
        } else {
            $this->value=null;
        }       
        
        if(isset($params["visible"]) && !is_null($params["visible"])) {
            $this->visible=$params["visible"];
        } else {
            $this->visible=true;
        }   

        if(isset($params["size"]) && !is_null($params["size"])) {
            $this->size=$params["size"];
        } else {
            $this->size=Null;
        }   
        
        if (isset($params["readOnly"]) && !is_null($params["readOnly"]))
        {
            $this->readonly=$params["readOnly"];
        } else {
            $this->readonly=false;
        }
        
        if (isset($params["disable"]) && !is_null($params["disable"]))
        {
            $this->disable=$params["disable"];
        } else {
            $this->disable=Null;
        }        
        
        $this->interactions = array(); // array of InputExInteractions objects        
        $this->conditions = array();
        $this->objDef = Null;
        $this->propDef = Null;                      
    }
        
    public function addInteraction($interaction) {
        // TODO: validate, error checking
        $this->interactions[] = $interaction;
    } 
    
    public function renderInteractions($sep,$strbld) {
        if (count($this->interactions)==0) {
            return;  // no interactions, do nothing
        } 
        $strbld->addLine($sep,false);
        $strbld->addLine('"interactions": [', false);
        $sep="";
        foreach($this->interactions as $interaction) {        
            $strbld->addLine($sep,false,false);
            $interaction->render($strbld);
            if ($sep=="") $sep=",";
        }
        $strbld->addLine("]",false,false);       
    }
    
    public function encodeArray($array, $strbld) {         
                         
         if (is_assoc($array)===False) {
             //$strbld->addLine("[",false);
             $mysep="";
             foreach($array as $value) {                 
                $strbld->addLine($mysep,false,false);         
                $this->encodeArray($value, $strbld);                  
                if ($mysep=="") $mysep=",";
             }             
            
             //$strbld->addLine("]",false);
             
         } else {         
             $strbld->addLine("{",false,false);         
             $mysep="";
             foreach($array as $key=>$value) {
                $strbld->addLine($mysep."\"{$key}\":'{$value}'",false,false);
                if ($mysep=="") $mysep=",";
            }
            $strbld->addLine("}",false,false);         
        }     
    }
        
    public function renderParams($strbld)        
    {
        $sep="";
        foreach($this->fieldParams as $key) {
            
            if ($key=="interactions") {    
                if (count($this->interactions)>0) {
                    $this->renderInteractions($sep,$strbld);
                    if ($sep=="") $sep=",";
                }                
            } else {            
                if(is_array($key)){
                    $type=$key[1];
                    $key=$key[0];
                }
                
                if (isset($this->{$key})) {                                
                    $value = $this->{$key};
                    
                    if (is_a($value, "SpecCls")) {
                        $val = "";
                    } else if (is_array($value)) {
                        $strbld->addLine($sep,false);                                                
                        $strbld->addLine("\"{$key}\":",false,false);  
                        
                        $dataArray = array();
                        foreach($value as $item) {                        
                            if (is_a($item, "SpecCls")) {                                
                                $dataArray[] = $item->getArrayOfData();                                                                
                            } else {
                                $strbld->addLine(json_encode($value),false,false);                                                    
                            }
                        }
                        
                        $strbld->addLine(json_encode($dataArray),false,false);   
                        
                        //$this->encodeArray($value, $strbld);  
                        //$strbld->addLine(json_encode($value),false,false);                                                    
                        
                    } else {                    
                        if (is_bool($value)) {
                            if ($value==true) { $val = "true"; } else { $val = "false"; };
                        } else if (is_string($value))  {
                            if(isset($type)){
                                if($type=="function"){
                                    $val=$value;
                                }
                                else{
                                    $val = cleanforJS($value);
                                    //$val = "\"{$value}\"";
                                }
                            }
                            else{
                                $val = cleanforJS($value);
                                //$val = "\"{$value}\"";                                
                            }
                        } else if (is_numeric($value))  {
                                                        
                            if (($this->type=="select") && ($key=="value")) {
                                $val = "\"{$value}\"";
                            } else {
                                $val = "{$value}";
                            }
                            
                        } else {
                            $val = "{$value}";
                        }
                        
                        
                        $strbld->addLine($sep."\"{$key}\":".$val."",false,false);
                        
                    }
                    if ($sep=="") $sep=",";
                }
            }                        
        }            
    }    
    
    public function render($strbld)        
    {
        //$strbld->clearIndentLevel();
        
        $strbld->addLine("{", false);
        $this->renderParams($strbld);         
        $strbld->addLine("}",false,false);        
    }       
          
    public function validate($value, $formMode)
    {
        
    }    
    
}

?>