<?php
/* This field adds the following properties
 *      min
 *      max
 *      negative: allow negative values (boolean)
 *      
 */


class InputExIntegerField extends InputExStringField{
    
    public $min;
    public $max;
    public $negative;    
    
    public function __construct($params)
    {
        $this->fieldParams[] = "min";
        $this->fieldParams[] = "max";
        $this->fieldParams[] = "negative";
                
        parent::__construct($params);
        
        if(isset($params["min"]) && !is_null($params["min"]))
        {
            $this->min=$params["min"];
        }
        
        if(isset($params["max"]) && !is_null($params["max"]))
        {
            $this->max=$params["max"];
        }
        
        if(isset($params["negative"]) && !is_null($params["negative"]))
        {
            $this->negative=$params["negative"];
        }
        
        $this->type="integer";
    }
}

?>