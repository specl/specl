<?php
/**
 * Description of InputExFormBuilder
 *
 * Builds an InputEx form from an Specl ObjDef.
 * 
 * @author tony
 */
class InputExFormBuilder {
    
    public $dataSources; // used for lookups and constraints
    public $formMode;
    public $nonDropDownTypes;
    public $datasources;
    public $dropDownLimit;
    
        
    public function __construct() {
        $this->dataSources = array();        
        $this->nonDropDownTypes = array('text', 'calendar');    
        $this->datasources = array(); 
        $this->dropDownLimit=20;
    }
        
    public function addDataSource($dataSource) {
        $this->dataSources[$dataSource->id] = $dataSource;
    }

    public function hasDataSource($id) {
        if (array_key_exists($id,$this->dataSources)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getDataSource($id) {
        if (array_key_exists($id,$this->dataSources)) {
            return $this->dataSources[$id];
        } else {
            return null;
        }
    }    
    
    public function addFormIncludes($webpage) {
        $webpage->includes->addCSSLink("/yui/build/button/assets/skins/sam/button.css");        
        $webpage->includes->addCSSLink("/yui/build/calendar/assets/skins/sam/calendar.css");            
        $webpage->includes->addJSLink("/yui/build/button/button-min.js");
        $webpage->includes->addJSLink("/yui/build/calendar/calendar-min.js");              
    }

    public function addInputExIncludes($webpage) {
        //$webpage->includes->addCSSLink("http://yui.yahooapis.com/2.8.2r1/build/assets/skins/sam/skin.css");                        
        //$webpage->includes->addCSSLink("/yui/build/button/container/assets/container.css");        
        $webpage->includes->addCSSLink("/yui/build/container/assets/container.css");                
        $webpage->includes->addJSLink("/yui/build/button/button-min.js");
        $webpage->includes->addCSSLink("/yui/build/button/assets/skins/sam/button.css");
        $webpage->includes->addJSLink("/yui/build/calendar/calendar-min.js"); 
        $webpage->includes->addCSSLink("/yui/build/calendar/assets/skins/sam/calendar.css");
        //$webpage->includes->addCSSLink("../inputex/build/inputex-min.css");        
        $webpage->includes->addCSSLink("../inputex/css/inputEx.css");
        //$webpage->includes->addCSSLink("../inputex/examples/css/demo.css");           
        $webpage->includes->addJSLink("../inputex/build/inputex.js");
        $webpage->includes->addJSLink("../lib5/javascript/ListField2.js");
        $webpage->includes->addJSLink("../lib5/javascript/AutoComplete2.js");
        $webpage->includes->addJSLink("../lib5/javascript/CustomForm.js");
        $webpage->includes->addCSSLink("siteInputEx.css");               
    }
        
    public function addFormEditorIncludes($webpage) {
        $webpage->includes->addCSSLink("/yui/build/editor/assets/skins/sam/editor.css");
        $webpage->includes->addJSLink("/yui/build/editor/editor-min.js");
    }
            
    public function addFormAutoCompleteIncludes($webpage) {
        $webpage->includes->addJSLink("/yui/build/connection/connection-min.js");
        $webpage->includes->addJSLink("/yui/build/datasource/datasource-min.js");
        $webpage->includes->addJSLink("/yui/build/autocomplete/autocomplete-min.js");        
        $webpage->includes->addJSLink("/yui/build/animation/animation-min.js");        
    }
       
    public function buildObjDef($objDefId) {
        global $app;

        $this->objDefId = $objDefId;
        $app->specManager->loadSpecDef($this->objDefId);
        $app->specManager->loadSpecDefExtra($this->objDefId);
        
        $this->objDef=$app->specManager->findDef($this->objDefId);

        if ($this->objDef==null)  {
            throw new Exception("InputExFormBuilder->buildFormObjDef {$objDefId} not found."); 
        } 
    }

    public function addList($parentField, $propDef, $objDef, $nodeCollectionDef, $webpage) {
        global $app;
        
        $visible = $propDef->getAttribute("visible", true, $this->formMode);
        $hidden = $propDef->getAttribute("hidden", false, $this->formMode);
        
        if ($visible===false || $hidden===true) {
            return;
        }
        
        $params = new ParamBuilder();
        $params->add("id", $propDef->name);
        
        if ($nodeCollectionDef->isCollection()) {
            // does not handle nested collections or collections with more than one type
            $childTypeObjDef = $nodeCollectionDef->getFirstChildDef();        
        } else {
            $params->add("disableAddRemove",true);
            $childTypeObjDef = $nodeCollectionDef;        
        }
                
        if($propDef->hasAttribute("caption")){
            $params->add("label",$propDef->getAttribute("caption"));
        }
        else{
            $params->add("label", $propDef->name);
        }

        if($propDef->hasAttribute("description")){
            $params->add("description",$propDef->getAttribute("description"));
        }
        
        $params->add("parentObj",$webpage);        
        if($propDef->isAttribute("readOnly",true,$this->inputExForm->formMode) || $propDef->isAttribute("readOnly",true,"any")||
                (isset($parentField->readonly) && $parentField->readonly===true)){
            $params->add("disableAddRemove",true);
        } 
        
        $params->add("collapsible",true);
        $params->add("collapsed",true);
        
        //$params->add("elementType", "group");                              
        $fld = new InputExListField($params->getParams());                
        //$fields = $this->createFieldsFromObjDef($objDef);                
        $fld->objDef = $objDef;
        $fld->propDef = $propDef;
        
        $parentPropDef=$propDef;
        
        // hide the primary keys of the parent
        foreach ($childTypeObjDef->getProperties() as $propDef) {        
            if ($objDef->isPartOfPrimaryKey($propDef->id)) {
                $propDef->setAttribute("hidden", true);
            }            
            if ($parentPropDef->isAttribute("readOnly",true,$this->inputExForm->formMode)) {
                //$propDef->setAttribute("readOnly",true);
                $fld->readonly = true;
            }
            
            if (isset($parentField->readonly)) {
                if ($parentField->readonly===true) {
                    $fld->readonly = true;
                }
            }
        }
        
        // nested collections should be read only
        if ($childTypeObjDef->isCollection()) {
            //$childTypeObjDef->setAttribute("readOnly",true);
            //$childTypeObjDef->setAttribute("popupEdit",false);
            $fld->readonly = true;
        }
        
        // do not add recursive properties
        if($childTypeObjDef->name!==$parentField->objDef->name){
            $this->addProperties($fld, $childTypeObjDef, $webpage);        
        } else{
            $this->addProperties($fld, $childTypeObjDef,$webpage,$recursive=false);
        }
        
        $parentField->addField($fld);                  
    }
    
    public function addGroup($parentField, $propDef, $objDef, $nodeCollectionDef, $webpage, $collapsed=false) {
        global $app;
        
        $visible = $propDef->getAttribute("visible", true, $this->formMode);
        $hidden = $propDef->getAttribute("hidden", false, $this->formMode);
        
        if ($visible===false || $hidden===true) {
            return;
        }
        
        if ($visible===false) {
            return;
        }
        
        $params = new ParamBuilder();
        $params->add("id", $propDef->name);
               
        if($propDef->hasAttribute("caption")){
            $legend = $propDef->getAttribute("caption");            
        } else {
            $legend = $propDef->name;
        }

        $params->add("label",$legend);
        
        if($propDef->hasAttribute("description")){
            $params->add("description",$propDef->getAttribute("description"));
        }
        
        $params->add("parentObj",$webpage);
        $params->add("className","inputEx-SubGroup");
        $params->add("collapsible",true);
        $params->add("collapsed",$collapsed);
        $params->add("legend",$legend); // This is teh actual form Caption
                                           
        $fld = new InputExGroupField($params->getParams());                        
        $fld->objDef = $objDef;
        $fld->propDef = $propDef;
        $objDefofPropDef = $app->specManager->findDef($fld->propDef->objectTypeOrig);
        
        //if the field is not a composite make all sub-fields readOnly: There should be a link.        
        if($propDef->getAttribute("isComposite",false)===False){
            foreach($objDefofPropDef->getProperties() as $subPropDef){
                $subPropDef->setAttribute("readOnly",true);
            }
        }else{
            $parentPropDef=$propDef;
            // for a composite do not display the primaryKey field
            foreach($objDefofPropDef->getProperties() as $propDef) { 
                    if ($objDef->isPartOfPrimaryKey($propDef->id)) {
                        $propDef->setAttribute("visible", false);
                    }
                
                // if the parentField is readOnly make all the child fields readOnly
                if($parentPropDef->isAttribute("readOnly",true,$this->inputExForm->formMode)){
                    $propDef->setAttribute("readOnly",true);
                }
            }
        }
        
        $this->addProperties($fld, $objDefofPropDef, $webpage); 
        $parentField->addField($fld);                  
    }
    
        
    public function handleAutoNumber($params, $propDef) {
        if ($propDef->hasAttribute("autoIncrement")) {
            $autoIncrement = $propDef->getAttribute("autoIncrement", false, $this->inputExForm->formMode);
            
            if ($autoIncrement==true) {
                $params->add("readOnly",true);  
            }
        }        
    }
    
    public function handleAutoComplete($fld,$objDef, $propDef,$webpage) {
        

        $this->addFormAutoCompleteIncludes($webpage);            
        $autoComplete = $propDef->getAttribute("autoComplete", false, $this->inputExForm->formMode);

        $datasource = "index.php?resourceId={$webpage->id}&controllerId={$this->inputExForm->id}&actionName=autoComplete&objDefId={$objDef->name}";

        $fld->datasource = $datasource;
        

        $cnt = count($this->datasources);
        $dsName = "{$propDef->name}_formDS".$cnt;
        $this->datasources[] = $dsName;

        $fld->dsName = $dsName;
        
        $sb = new StringBuilder();
        $sb->addLines('var '.$dsName.' = new YAHOO.util.XHRDataSource("'.$datasource.'");
        // Set the responseType
        '.$dsName.'.responseType = YAHOO.util.XHRDataSource.TYPE_TEXT;
        // Define the schema of the delimited results
        '.$dsName.'.responseSchema = {
            recordDelim: "\n",
            fieldDelim: "\t"
        };
        // Enable caching
        '.$dsName.'.maxCacheEntries = 5;');                        

//        $acName = "oAC".$cnt;
//        $fld->acName = $acName;
//        
//        $sb = new StringBuilder();
        
        $webpage->jsInlineHead->addJS($sb->getString());                        
    }
    
    public function addDescription($params, $propDef) {
        if ($propDef->hasAttribute("description")) {
            $description = $propDef->getAttribute("description");
            $params->add("description",$description);  
        }        
    }
    
    /**
     *  This function is outdated. The data set in this function is overridden by a call to getData
     *  in the form page. This function performs a combination of what the propDef getDefaultValue
     *  function and the transforms from setValuesFromObjdef. setValuesFromObjDef performs more
     *  transformations then this function does.
     * 
     *  There are other places in the code where the form is built without calling setValuesFromObjdef,
     *  so this function needs to be updated to call the proper transformations.
     * 
     */
    public function addDefaultValue($params, $propDef) {
        
        global $app;
        
        if ($propDef->hasAttribute("defaultValue", $this->inputExForm->formMode)) {
            $defaultValue = $propDef->getAttribute("defaultValue", null, $this->inputExForm->formMode);
            
            if ($defaultValue=== "SESSION.userId") {            
                $defaultValue = $app->sessionManager->sessionObj->user;
            } else if ($defaultValue==="now()") {
                //$defaultValue = date("D M j G:i:s T Y");
                if ($propDef->hasAttribute("valueFormat", $this->inputExForm->formMode)) {
                    $valueFormat = $propDef->getAttribute("valueFormat", null, $this->inputExForm->formMode);
                } else {
                    $valueFormat = "Y/m/d G:i:s";
                }
                
                $defaultValue = date($valueFormat);
                
                if($propDef->objectType=="datetime" && 
                   $propDef->getAttribute("readOnly",false,$this->inputExForm->formMode)===false){                    
                   $defaultValue=str_replace("-","/",$defaultValue);
                }
                
            } else if ($defaultValue==="USER.defaultLaboratory") { 
                if ($propDef->hasConstraint()){               
                   $constraint=$propDef->getConstraint();
                   $constraint->generate($this->formMode);
                   $lookup=$constraint->get();

                   //if ((count($lookup)>20)||($propDef->isReadOnly()==True)) {
                   if (count($lookup)>20 ||  $propDef->isReadOnly($this->formMode)){
                       $defaultValue = $app->acManager->userObj->defaultLaboratoryName;  
                   }
                   else{
                       $defaultValue = $app->acManager->userObj->defaultLaboratory;  
                   }
                } 
                else{
                    $defaultValue = $app->acManager->userObj->defaultLaboratory;  
                }                                                      
            }                            
            $params->add("value",$defaultValue);  
        }
    }    
    
    public function shouldDisplayProperty($propDef) {
        if ($propDef->getAttribute("autoIncrement",false)) {
            $autoIncrement = $propDef->getAttribute("autoIncrement");            
            if (($autoIncrement==true) && ($this->formMode == "add")) {
                return false;
            }
        }        
        return true;
    }
    
    public function addHiddenProperty($parentField, $objDef, $propDef, $webpage, $id) {                
                
        $params = new ParamBuilder();   
        $params->add("id", $id); // the id of the hidden field        
        $params->add("parentObj",$webpage);      
        
        $this->addDefaultValue($params, $propDef);
                    
        $fld = new InputExHiddenField($params->getParams());
        $fld->objDef = $objDef;
        $fld->propDef = $propDef;
        
        $parentField->addField($fld); 
    }
     
    //attributes
    //caption
    //columnType
    //comment
    //nullAllowed
    //variableLength
    
    public function addProperty($parentField, $objDef, $propDef, $webpage) {
        global $app;
        
        // hide autoid values in add mode in the top level of the form only
        if(is_a($parentField, "InputExForm")){
            if ($this->shouldDisplayProperty($propDef)==false) {
                return;
            }
        }
        
        $label = $propDef->getAttribute("caption",$propDef->name);                                                      
        $propName = $propDef->name;
        
        $params = new ParamBuilder();   
        $params->add("id", $propName);
        $params->add("label", $label);
        $params->add("parentObj",$webpage);      
        
        $readOnly = $propDef->getAttribute("readOnly",false, $this->formMode);
        
        if (isset($parentField->readonly)) {
            if ($parentField->readonly===true) {
                $readOnly=true;
            }
        }
        
        if ($readOnly) {
            $params->add("readOnly", true);          
        }                
        
        $visible = $propDef->getAttribute("visible", true, $this->formMode);
        
        if ($visible===false) {
            return;
        }
        
        // a field is a drop down type if it has a constraint and there is no overriding control type
        // set in the Spec.
        if (($propDef->getConstraint())) {                        
            $dropdownType = true;                
            // Check to see if definition overrides the default user interface control type                        
            if ($propDef->hasAttribute("fieldControl")) {
                $fieldControl = $propDef->getAttribute("fieldControl");               
                if ($fieldControl=="text") {
                    $dropdownType = false;    
                }
            }             
        } else {
            $dropdownType = false;
        }                

        if ($dropdownType) {    
            $constraint = $propDef->getConstraint($this->formMode);            
            $constraint->generate($this->formMode);
            $numOptions= $constraint->count();
            $this->addDescription($params, $propDef);
            $this->addDefaultValue($params, $propDef);
            
            // This only works for database lookup constraints that do not use custom SQL 
            if (isset($constraint->tableName)) {
                $displayObjDef=$app->specManager->findDef($constraint->tableName);
                $displayPropDef=$displayObjDef->getProperty($constraint->displayColumnName); 
                $displaySize=$displayPropDef->getAttribute("variableLength",80);
            } else {
                $displaySize = 45;
            }
             
            // always add the size
            if($displaySize>80){
                $displaySize=80;
            }
            
            $params->add("size",$displaySize);
            
            // if the field is constrainted and has a read only value, then
            // the displayColumn value must be shown in the display field, 
            // while the real value is set in a hidden field in the form.
            if ($readOnly==true) {                         
                // always add a hidden field for the real value!
                $paramsHidden = new ParamBuilder();   
                $paramsHidden->add("id", $propName);
                $paramsHidden->add("label", $label);
                $paramsHidden->add("parentObj",$webpage); 
                $this->addDescription($paramsHidden, $propDef);
                $this->addDefaultValue($paramsHidden, $propDef);

                $fld = new InputExHiddenField($paramsHidden->getParams());
                $fld->objDef = $objDef;
                $fld->propDef = $propDef;        
                $parentField->addField($fld); 
                
                // this is the display value (display is never used anywhere, must be an old idea that was replaced)
                $params->add("id", "display-".$propName);
                
                $fld = new InputExStringField($params->getParams());      
                $fld->objDef = $objDef;
                $fld->propDef = $propDef;                                       
                
            } else {
                                
                if($numOptions<=$this->dropDownLimit){
                    $choices = $constraint->get();
                    // sort choices by key                
                    if ($propDef->getAttribute("sortLookup",true)) {
                        uasort($choices,"strnatcasecmp");
                    }
                    $params->add("choices",$choices);        
                    $fld = new InputExSelectField($params->getParams());
                    $fld->objDef = $objDef;
                    $fld->propDef = $propDef;
                }
                else{
                    
                    if($displaySize>80){
                        $displaySize=80;
                    }
                    
                    $this->addFormAutoCompleteIncludes($webpage);                   
                    
                    if (!isset($webpage->page)) {
                        echo "ERROR";
                    }
                    
                    if ($webpage->buildPage) {
                        
                        if ($webpage->buildPage===true) {
                            $buildPage = "true";
                        } else {
                            $buildPage = "false";
                        }
                    } else {
                        $buildPage = "false";
                    }
                            
                    $params->add('url',"index.php?pageId={$webpage->id}&resourceId={$webpage->page->resourceId}&controllerId={$this->inputExForm->id}&actionName=autoComplete&buildPage={$buildPage}");
                    
                    $fld= new InputExAutoCompleteField($params->getParams());
                    $fld->objDef = $objDef;
                    $fld->propDef = $propDef;
                }                           
            }            
        } else  {
            
                //$dataType = $propDef->getAttribute("columnType");            
                //if ($dataType=="tinyint") {
                //  
                //}
                
                $size=$propDef->getAttribute("variableLength",80);
                if($size>80){
                    $size=80;
                }
                $params->add("size",$size);
                
                $fieldType = null;
                
                if ($propDef->hasAttribute("fieldControl")) {
                    $fieldControl = $propDef->getAttribute("fieldControl");
                    
                    switch ($fieldControl) {
                        case "password":
                            $fieldType = "InputExPasswordField";
                            break;
                        case "textarea":
                            $fieldType = "InputExTextAreaField";
                            break;
                    }
                }                
                
                if ($fieldType == null) {
                    // checks for auto, string, integer, double, float, boolean, date            
                    switch ($propDef->objectType) {
                        case "auto":
                            $fieldType = "InputExStringField";
                            break;

                        case "string":
                            $fieldType = "InputExStringField";

                            if ($propDef->hasAttribute("displayLength")) {
                                $params->add("size",$propDef->getAttribute("displayLength"));
                            } 

                            if ($propDef->isAttribute("columnType","longtext") || $propDef->isAttribute("isBlob",true)) {
                                $fieldType = "InputExTextAreaField";
                            }                             
                            break;
                            
                        case "integer":           
                            
                            $params->add("size",8);
                            
                            if ($propDef->isAttribute("autoComplete",true)) {
                                $fieldType = "InputExAutoCompleteField";
                            } else {                            
                                if ($propDef->getAttribute("clientValidate",false)===false) {
                                    $fieldType = "InputExStringField";
                                } else {                    
                                    $fieldType = "InputExIntegerField";
                                }
                            }                                                        
                            //$fieldType = "InputExStringField";
                            break;                        
                        case "double":
                        case "float":                                       
                           $params->add("size",8);
                            
                           if ($propDef->getAttribute("clientValidate",false)===false) {
                                $fieldType = "InputExStringField";
                            } else {                    
                                $fieldType = "InputExFloatField";
                            }                    
                            break;                        
                        case "boolean":
                            $params->add("size",5);
                            
                            if ($readOnly==true) {
                                
                                $paramsHidden = new ParamBuilder();   
                                $paramsHidden->add("id", $propName);
                                $paramsHidden->add("label", $label);
                                $paramsHidden->add("parentObj",$webpage);      

                                $this->handleAutoNumber($paramsHidden, $propDef);            
                                $this->addDescription($paramsHidden, $propDef);
                                $this->addDefaultValue($paramsHidden, $propDef);

                                $fld = new InputExHiddenField($paramsHidden->getParams());
                                $fld->objDef = $objDef;
                                $fld->propDef = $propDef;        
                                $parentField->addField($fld); 

                                // this is the display value
                                $params->add("id", "display-".$propName);
                                $fieldType = "InputExStringField";
                                
                            } else {
                                $fieldType = "InputExCheckBoxField";
                            }
                            
                            break;                        
                        case "date":             
                            
                            if ($propDef->hasAttribute("dateFormat", $this->inputExForm->formMode)) {
                                $dateFormat = $propDef->getAttribute("dateFormat", null, $this->inputExForm->formMode);
                                $params->add("dateFormat", $dateFormat);
                            }

                            if ($propDef->hasAttribute("valueFormat", $this->inputExForm->formMode)) {
                                $dateFormat = $propDef->getAttribute("valueFormat", null, $this->inputExForm->formMode);
                                $params->add("valueFormat", $dateFormat);
                            }
                            
                            if ($readOnly) {
                                $fieldType = "InputExStringField";
                                $params->add("size", 18);
                            } else {                            
                                $fieldType = "InputExDatePickerField";
                            }                            
                            break;
                        case "datetime":
                            
                            if ($propDef->hasAttribute("dateFormat", $this->inputExForm->formMode)) {
                                $dateFormat = $propDef->getAttribute("dateFormat", null, $this->inputExForm->formMode);
                                $params->add("dateFormat", $dateFormat);
                            }

                            if ($propDef->hasAttribute("valueFormat", $this->inputExForm->formMode)) {
                                $dateFormat = $propDef->getAttribute("valueFormat", null, $this->inputExForm->formMode);
                                $params->add("valueFormat", $dateFormat);
                            }
                            
                            if ($readOnly) {
                                $fieldType = "InputExStringField";
                                $params->add("size", 18);
                            } else {                            
                                $fieldType = "InputExDateTimeField";
                            }
                            break;
                        default:
                            echo "ERROR: FormController->buildFormObjDef Unhandled type {$propDef->objectType}<br/>";
                            break;
                }
            }
            
            $this->handleAutoNumber($params, $propDef);            
            $this->addDescription($params, $propDef);
            $this->addDefaultValue($params, $propDef);
                        
            $fld = new $fieldType($params->getParams());      
            $fld->objDef = $objDef;
            $fld->propDef = $propDef;
        
            if ($propDef->isAttribute("autoComplete",true)) {                               
                $this->handleAutoComplete($fld, $objDef, $propDef, $webpage);
            }                                
        }       
        
        $parentField->addField($fld);   
    }
    
    public function addProperties($parentField, $objDef, $webpage, $recursive=true) {
        global $app;
        
        // first add hidden fields
        foreach($objDef->getPrimaryKey() as $propKeyName) {                 
            $propDef = $objDef->getProperty($propKeyName);                        
            $this->addHiddenProperty($parentField,$objDef, $propDef, $webpage, "original-".$propDef->name);                                    
        }
        
        foreach($objDef->getProperties() as $propDef) {
            $add = false;

            if ($propDef->getAttribute("ignore", false, $this->inputExForm->formMode)==true) {
                continue;
            }
            
            // hide autoIncrement fields in add mode.
            if($this->inputExForm->formMode=="add" && $propDef->getAttribute("autoIncrement",false)===True){
                    $this->addHiddenProperty($parentField, $objDef, $propDef, $webpage, $propDef->name); 
                    continue;
            }
                            
            if ($propDef->getAttribute("hidden", false, $this->inputExForm->formMode)==false) {            
                if ($propDef->isScalar()) {
                    switch ($propDef->objectType) {
                        case "auto":
                            $disabled=true;
                            $add=true;
                            break;
                        case "string":
                        case "integer":
                        case "double":
                        case "float":
                        case "boolean":
                        case "date":
                        case "datetime":
                            $add=true;
                            break;
                        default:
                            echo "ERROR: InputExFormBuilder->build Unhandled type {$propDef->objectType}<br/>";
                            $add=false;
                            break;
                    }        
                } else {                
                    if ($propDef->isObject()) {
                        if ($propDef->getAttribute("isActive",true)===true) {                                             
                            $nodeDef = $app->specManager->findDef($propDef->objectTypeOrig);                            
                            if ($propDef->getAttribute("isComposite",false)===true) {
                                $collapsed=false;
                            } else {
                                $collapsed=true;
                            }
                            if ($recursive===true) {
                                $this->addGroup($parentField, $propDef, $objDef, $nodeDef, $webpage, $collapsed);
                            }
                        }
                    } else if ($propDef->isCollection()) {       
                        if ($recursive===true) {
                            $nodeCollectionDef = $app->specManager->findDef($propDef->objectTypeOrig);                                               
                            $this->addList($parentField, $propDef, $objDef, $nodeCollectionDef, $webpage);                    
                        }
                    }                
                }
            }
            else {
                $this->addHiddenProperty($parentField, $objDef, $propDef, $webpage, $propDef->name); 
            }
                        
            if ($add) {
                $this->addProperty($parentField, $objDef, $propDef, $webpage);
            }        
        }
    }
    
    public function addErrorMessageField($parentField, $objDef, $webpage) {
        $params = new ParamBuilder();   
        $params->add("id", "formMessage");
        $params->add("parentObj",$webpage);    
        $params->add("value","");
        $params->add("visible",false);
        
        $fld = new InputExUneditableField($params->getParams());      
        $parentField->addField($fld);   
        
    }
              
    /* 
     * build
     * 
     * (1) objDef that will be used to create a new InputExForm component
     * (2) webPage that will contain the InputExForm component
     * 
     * The objDef will be used to create a form model.
     * 
     */
    public function build($formId, $formMode, $objDef, $webpage,$recursive=true) {        
        $this->formMode = $formMode;        
        $hasRTEEditor = false;        
        
        $this->addFormIncludes($webpage);
        $this->addInputExIncludes($webpage);        
        
        $this->inputExForm = new InputExForm($formId, $webpage);
        $this->inputExForm->objDef = $objDef;
        $this->inputExForm->formMode = $formMode;
        
        if($formMode=="view"){
            $this->inputExForm->renderSubmitButton=false;
            $this->inputExForm->cancelButtonText="Go to Browse Page";
        }
        
        $this->addProperties($this->inputExForm, $objDef, $webpage,$recursive);
        
        // add error message field        
        $this->addErrorMessageField($this->inputExForm, $objDef, $webpage);
        
        if ($hasRTEEditor) {
            $this->addFormEditorIncludes($webpage);
        }        
        return $this->inputExForm;
    }
    
    public function setValues($objInstance) {
        foreach($this->inputExForm->fields as $fld) {            
            if ($objInstance->hasProperty($fld->id)) {                
                $fld->value = $objInstance->{$fld->id};
            }            
        }
    }
    
    public function setValuesFromObjDef($inputExField, $obj,$setFieldValue=True) {
        $this->applyFieldDataTransformations($inputExField,$obj);
        $this->setFormValues($inputExField,$obj);        
    }
    
    public function convertBooleanValues($obj){                
        foreach($obj->getObjDef()->getProperties() as $propDef){
             if($propDef->objectType=="boolean"){
                if (isset ($obj->{$propDef->name})) {
                    $value=$obj->{$propDef->name};
                    $displayName="display-{$propDef->name}";
                    $trueValues=array("true","1",true,1);
                    if (in_array($value, $trueValues,$strict=True)) {
                            $obj->{$displayName}="true";
                            $obj->{$propDef->name}="true";
                    } else {                        
                        $obj->{$propDef->name}="false";
                        $obj->{$displayName}="false";
                    }    
                }
            }
        }
    }
    
    // convert integer values to strings
    public function convertIntegerValues($obj){
        foreach($obj->getObjDef()->getProperties() as $propDef){
             if($propDef->objectType=="integer"){
                $obj->{$propDef->name}=strval($obj->{$propDef->name});
            }
        }
    }
    
    public function convertDateTimeValues($obj){
        foreach($obj->getObjDef()->getProperties() as $propDef){
             if($propDef->objectType=="datetime" && 
                $propDef->getAttribute("readOnly",false,$this->inputExForm->formMode)===false) {                 
                $value=trim($obj->{$propDef->name});                
                // javascript Date strings must have a "/" seperators instead of "-"
                                
                // TODO: I did this because the InputEx DateTime field fails for a null value horrible                
                if (empty($value)) {
                    $value = date('Y-m-d h:i:s');                    
                }
                $value=str_replace("-","/",$value);
                $obj->{$propDef->name}=$value;
            }
        }
    }
    
    // apply transformations to the data based of field type recursively
    public function applyFieldDataTransformations($inputExField,&$obj){
        $this->convertBooleanValues($obj);
        $this->convertIntegerValues($obj);
        $this->convertDateTimeValues($obj);
        
        // use the field objDef to perform the data transforms, not the data objDef.
        // the field objDef has transforms, like security transforms applied to it.
        if(isset($inputExField->objDef)){
            $objDef=$inputExField->objDef;
        }
        else{
            $objDef=$obj->getObjDef();
        }
        foreach($inputExField->fields as $fld) {  
            if (is_null($fld->propDef)==False && is_null($objDef->getProperty($fld->propDef->name))==False) {
                $propDef=$objDef->getProperty($fld->propDef->name);
                
                if (is_a($fld, 'InputExGroupField')) {
                    if (isset($obj->{$fld->propDef->name})) {                           
                             $subObj = $obj->{$fld->propDef->name};                                                    
                             $this->applyFieldDataTransformations($fld,$subObj);
                             $obj->{$fld->name}=$subObj;
                    }                   
                } else if (is_a($fld, 'InputExListField')) {
                    if (isset($obj->{$fld->propDef->name})) {   
                        
                        // for list fields the value of the individual fields must not be set.                        
                        $associationType = $propDef->getAttribute("associationType", false);

                        if ($associationType=="HasMany") {                                                                   
                            $subObjs = $obj->{$fld->propDef->name};                        
                            $newSubObjs=array();
                            foreach($subObjs as $subObj){
                                $this->applyFieldDataTransformations($fld,$subObj);
                                $newSubObjs[]=$subObj;
                            }
                            $obj->{$fld->name}=$newSubObjs;
                        } else if ($associationType=="HasManyThrough") {
                            $intersectionObjs = $obj->{$fld->propDef->name};                        
                            $newSubObjs=array();
                            foreach($intersectionObjs as $intersectionObj){
                                $subObj=$intersectionObj->__data__;
                                $this->applyFieldDataTransformations($fld,$subObj);
                                $newSubObjs[]=$subObj;
                            }
                            $obj->{$fld->name}=$newSubObjs;
                                                        
                        } else if ($associationType=="HasOne") {  
                             $subObj = $obj->{$fld->propDef->name};                                                    
                             $this->applyFieldDataTransformations($fld,$subObj);
                             $obj->{$fld->name}=$subObj;
                        }
                    }
                        
                }
                // if the field is hidden do not apply the autocomplete/readonly transformations
                else if(is_a($fld,"InputExHiddenField")){
                    $obj->{$fld->name}=$obj->{$fld->propDef->name};
                }
                // if field is autocomplete or readonly perform value substitution if the value has a constraint
                else if (is_a($fld,"InputExAutoCompleteField") || ((isset($fld->readonly)) && $fld->readonly===true)){                    
                    if (isset($obj->{$fld->propDef->name})) {
                        $constraint=$propDef->getConstraint();
                        if(!is_null($constraint)){
                            // for nested objects, avoid generating the same lookup over and over again by reusing the lookup values
                            // stored in memory. This is especially needed for nested library inputs.
                            if(!$constraint->optionsGenerated){
                                $constraint->generate($this->formMode,true);
                            }
                            $lookup=$constraint->get();                            
                            // If the default value is a key in the lookup
                            // transform the key value into a string value
                            if(isset($lookup[$obj->{$fld->propDef->name}]))
                            {
                                $value=$lookup[$obj->{$fld->propDef->name}];
                                if($value==false){
                                    $obj->{$fld->name}=null;
                                }
                                else{
                                    $obj->{$fld->name}=$value;
                                }
                            }
                            // If the default value is a lookup value, set the field without any transformation.
                            else if(in_array($obj->{$fld->propDef->name}, $lookup, $strict=true)){
                                $obj->{$fld->name}=$obj->{$fld->propDef->name};
                            }                            
                            else{
                                $obj->{$fld->name}="";
                            }
                        }
                    }
                }
            }
        }
    }
    
    // fill in the values of the fields, this is not recursive for listfields in order to avoid filling in
    // default values when adding a new item to the collection using the gui.
    public function setFormValues($inputExField,$obj){
        if(isset($obj->objDef)){
                $objDef=$obj->objDef;
        }
        else{
            $objDef=$obj->getObjDef();
        }
        foreach($inputExField->fields as $fld) {
            if (is_null($fld->propDef)==False && is_null($objDef->getProperty($fld->propDef->name))==False) {                
                // fill in group fields recursivly, every thing else is not recursive.
                if (is_a($fld, 'InputExGroupField')) {
                    if (isset($obj->{$fld->name})) {                           
                             $subObj = $obj->{$fld->name};                                                    
                             $this->setFormValues($fld,$subObj);
                    } else {            
                        // make it invisible
                        $fld->visible = false;
                    }                   
                } else {
                    if(isset($obj->{$fld->name})){
                        $newValue = $obj->{$fld->name};
                        $fld->value=$newValue;
                    }
                }
            }           
        }
    }
}

?>
