<?php
/**
 * Description of InputExPasswordField
 *
 * @author tony
 */
class InputExPasswordField extends InputExStringField{
    
    public function __construct($params)
    {
        parent::__construct($params);        
        $this->type="password";
    }
}
?>
