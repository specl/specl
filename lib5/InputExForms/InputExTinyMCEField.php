<?php
/**
 * Description of InputExTinyMCEField
 *
 * @author tony
 */
/* This field adds the following options
 *  opts: the options to be added when calling the TinyMCE constructor
 */
class InputExTinyMCEField extends InputExField {
        
    public function __construct($params)
    {
        $this->fieldParams[] = "opts";        

        parent::__construct($params);
                        
        if(isset($params["opts"]) && !is_null($params["opts"]))
        {
            $this->opts=$params["opts"];
        }
                
        $this->type="tinymce";
    }  
}
?>
