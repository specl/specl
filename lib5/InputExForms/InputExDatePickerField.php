<?php

class InputExDatePickerField extends InputExDateField {
    
    public function __construct($params)
    {
        parent::__construct($params);
                
        $this->type="datepicker";
    }
    
}

?>