<?php
// Every Form has a ObjDef that describes it.
//
// Every Field has a PropDef inside of the Form Obj Def (PropDefs can be COllections of ObjDefs...)
//
// When you add a field, you also create a PropDef that gets added to the FormObjDef.
//
// PropDefs contain Conditions used for validation.
// PropDefs contain COnstraints used to populate drop down lists, radio groups, check box groups etc.
//
// PropDefs can be collections, which translate to a sub-group which can contain multiple fields.
//
//          !!!!!!!!!!!  FORMS DO NOT MAP TO A SINGLE DATABASE TABLE  !!!!!!!!!! 
//
// After the form is validated, it does not perform ANY database transaction itself.  It sends the
// validated data up the chain to its parentObj.  Which is usually a WebPage sub class.
// The WebPage then decides how to handle the data based on the application needs. 
//
// There can be MANY form modes.  Not just add, edit.   The modes can determine more than
// validation conditions...they can determine what fields are visible...
// the values in a drop-down list...
// the color or other CSS sytle of backgrounds, text etc. ...
// 
// formMode is application specific.
class InputExForm extends Component {
    
    public $formMode; // add, edit
    public $fields; // array of InputExField classes
    public $fieldsById; // array of InputExField classes by Id, dictionary for lookups    
    public $pageObj; //  WebPage this form is on
    public $formDef; // Spec with form metadata
    public $visible;
    public $validateAction;
    public $formTitle;
    public $enctype; // 'multipart/form-data'
    public $action;
    public $isAJAX; // true or false
    public $renderButtons;
    public $renderSuccessDialog;
    public $successMessage;
    public $formCSS; // Specify a specific CSS for this form.  Make sure to add this to a CSS file!
    public $fieldErrors;
    public $fieldWarnings;
    public $objDef;
    public $onSuccessRedirect;
            
    // TODO: change constructor to except $params instead of $id,$parentObj
    public function __construct($id, $parentObj)  // TODO: is $parentObj always the same as $pageObj?
    {        
        $params= new ParamBuilder();
        $params->add("id",$id);
        $params->add("parentObj",$parentObj);
        parent::__construct($params->getParams());
        
        $this->fields=array();
        $this->fieldsById=array();
        $this->pageObj=null;
        $this->formMode="any";  // application has to set to add or edit or what ever.... 
        $this->visible=true;
        $this->submitAction = "validateData";
        $this->addAction("webservice","validateData");
        $this->addAction("webservice","autoComplete");
        $this->addAction("webservice","onFieldChange");
        $this->formTitle = null;
        $this->enctype = null;
        $this->isAJAX = true; // default is AJAX mode
        $this->action = Null;
        $this->cancelCallback=null;
        $this->renderButtons = true;
        $this->renderSuccessDialog=false;
        $this->successMessage="Submit Successful";
        $this->renderSubmitButton=true;
        $this->renderCancelButton=true;
        $this->cancelButtonText="Cancel";
        
        $this->formCSS = Null;  // don't use this if this is set.
        
        $this->fieldErrors = array();
        $this->fieldWarnings = array();
        
        $this->objDef=Null;
        
        $this->onSuccessRedirect=Null;
    }
        
    public function addFieldBefore($beforeFldName, $fld) {
        $beforeFld = $this->getField($beforeFldName);
        
        // Error checking is it NULL?
        if (is_null($beforeFld)) {
            return;            
        }
        
        $fields2 = array();                
        foreach($this->fields as $field) {
            if ($field->id == $beforeFld->id) {
                $fields2[]=$fld;                
                $fields2[]=$field;                
                $this->fieldsById[$fld->id]=$fld;                       
            } else {
                $fields2[]=$field;                                                
            }
        }        
        $this->fields=$fields2;
    }
  
    public function addField($fieldObj)
    {
        $this->fields[]=$fieldObj;
        $this->fieldsById[$fieldObj->id]=$fieldObj;        
    }

    public function getField($fieldId)
    {
        if (isset($this->fieldsById[$fieldId])){
            return $this->fieldsById[$fieldId];
        } else {
            return Null;
        }
    }
        
    public function render($strbld=null) {
        if($this->visible===false) {
            return;
        }
        
        if(is_null($strbld)) {
            $strbld=new StringBuilder();
        }
        
        if($this->renderSuccessDialog){
            $this->buildSuccessDialog();
            $this->renderSuccessDialogCSS($strbld);
        }        
        $this->renderHTML($strbld); 
        $this->renderJavascript($strbld);
    }
    
    public function buildSuccessDialog(){
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_SuccessPanel");
        $params->add("parentObj",$this);
        $params->add("pageObj",$this->pageObj);
        $params->add("title","Success");
        $this->successPanel=new PopupComponent($params->getParams());
        
        $params=new ParamBuilder();
        $params->add("id","{$this->id}_SuccessMessage");
        $params->add("innerHTML",$this->successMessage);
        $messageDiv=new DivComponent($params->getParams());
        $this->successPanel->addComponent($messageDiv);
//        
//        $params=new ParamBuilder();
//        $params->add("id","{$this->id}_DeleteOkButton");
//        $params->add("text","OK");
//        $params->add("clickCallback","{$this->id}_onSuccessOk");
//        $okButton=new ButtonComponent($params->getParams());
//        $this->successPanel->addComponent($okButton);
    }
    
    public function renderHTML($strbld) {
         if (is_null($this->formCSS)) {
             $class = "";
         } else {
             $class = "class='".$this->formCSS."'";
         }
         
         $strbld->addLine("<div id='{$this->id}' {$class}></div>");
         
         if($this->renderSuccessDialog){
             $this->successPanel->renderHTML($strbld);
         }
    }

    public function renderJavaScript($strbld) {
        if ($this->isAJAX == true) {        
            $this->renderFormAjax($strbld);
        } else {
            $this->renderFormPost($strbld);
        }
        
        if($this->renderSuccessDialog){
             $this->renderOnSuccessOkCallback($strbld);
             $this->successPanel->renderJavascript($strbld);
             $this->renderSuccessCallback($strbld);
             
        }
        
        
        if(get_class($this)=="InputExForm"){
            
            if ($this->pageObj->buildPage===true) {
                $buildPage = "true"; 
            } else {
                $buildPage = "false"; 
            }
        
            $params=array("url"=>"index.php?pageId={$this->pageObj->id}&resourceId={$this->pageObj->page->resourceId}&controllerId={$this->id}&actionName=onFieldChange&buildPage={$buildPage}");
            $renderOnChangeEventsComponent=new AddInputExOnChangeListeners($params);
            $input=array("objDef"=>$this->objDef,"strbld"=>$strbld,"formName"=>$this->id);
            $strbld=$renderOnChangeEventsComponent->execute($input);
        }
    }
    
    public function renderCSS($strbld){
        if($this->renderSuccessDialog){
            $this->renderSuccessDialogCSS($strbld);
        }
    }
    
    public function renderSuccessDialogCSS($strbld){
        $strbld->addLines(
"
<style>
     #{$this->successPanel->id} .bd {
        text-align:center;
        max-width: 25em;
    }
    #{$this->id}_SuccessOkButton {
        margin-top: 1em;
        
    }
</style>
");
    }
    
    public function renderSuccessCallback($strbld){
        $strbld->addLines(
"
<script>
    var {$this->id}_onSubmitSuccess=function(e){
        {$this->successPanel->id}.center();
        {$this->successPanel->id}.show();
    }
    {$this->id}.submitSuccess.subscribe({$this->id}_onSubmitSuccess);
</script>
");
        
    }
    
    public function renderOnSuccessOkCallback($strbld){
        $strbld->addLines(
"
<script>
    var {$this->id}_onSuccessOk=function(e){
        {$this->id}.clear();
        {$this->successPanel->id}.hide();
    }
</script>
");
    }    
    
    public function renderFormPost($strbld) {
        $strbld->addLine("<!-- (renderFormPost) Start InputExViewer {$this->id} -->");        

        $strbld->addLine("<script>");
        $strbld->addLine("var {$this->id}=new inputEx.Form({");
        $strbld->increaseIndentLevel();

        $strbld->addLine("\"parentEl\": \"{$this->id}\"");
        if ($this->enctype) {
            $strbld->addLine(",\"enctype\": \"{$this->enctype}\"");
        }
        if($this->action){
            $strbld->addLine(",\"action\": \"{$this->action}\"");
        }
        $strbld->addLine(",");
        if (!is_null($this->formTitle)) {
            $strbld->addLine("legend: \"{$this->formTitle}\",");
        }
        $this->renderFields($strbld);
        $strbld->addLine(",");
        
        if ($this->renderButtons == true) {
            $this->renderFormButtons($strbld);
        }       
        
        $strbld->addLine("});");
                
        $strbld->addLine("</script>");                     
        
        $strbld->addLine("<!-- End InputExViewer {$this->id} -->");       
    }
    
    public function renderFormAjax($strbld) {
        
        $strbld->addLine("<!-- (renderFormAjax) Start InputExViewer {$this->id} -->");        
               
        $strbld->addLine("<script>");
        $strbld->addLine("var {$this->id}=new inputEx.CustomForm({");
        $strbld->increaseIndentLevel();
        $strbld->addLine("name:'{$this->id}',");
        if (!is_null($this->formTitle)) {
            $strbld->addLine("legend: \"{$this->formTitle}\",");
        }
        $this->renderFields($strbld);
        $strbld->addLine(",");
        
        if ($this->renderButtons == true) {
            $this->renderFormButtons($strbld);
        }
 
        $strbld->addLine("parentEl: '{$this->id}',");
        $this->renderAjaxConfig($strbld);
         
        $strbld->addLine("}");
        
        $strbld->decreaseIndentLevel();
        $strbld->addLine("});");
        $strbld->addLine("</script>");        
        $strbld->addLine("<!-- End InputExViewer {$this->id} -->");
        
        
//        $strbld->addLines("<script>            
//            YAHOO.util.Event.addListener(window,'load',function(){                
//                for(i=0;i<{$this->id}.inputs.length;i++){
//                    currentField={$this->id}.inputs[i];
//                    if (currentField.options['visible'] === false) {
//                        currentField.hide();
//                    }                    
//                } 
//            });
//        </script>");        
    }
    
    public function renderAjaxConfig($strbld){
        
        if ($this->pageObj->buildPage===true) {
            $buildPage = "true"; 
        } else {
            $buildPage = "false"; 
        }
    
        $strbld->addLines(
"
ajax: {
    method:'POST',
    uri:'index.php?pageId={$this->pageObj->id}&resourceId={$this->pageObj->page->resourceId}&controllerId={$this->id}&actionName={$this->submitAction}&mode={$this->formMode}&buildPage={$buildPage}',
    callback: {
        success:function(response)
        {
            {$this->id}.clearErrorMessages()
            var results=YAHOO.lang.JSON.parse(response.responseText);

            if (results.messageType=='form') {            
                if (results.formStatus === true) {            
                    if (results.redirect === true) {
                        window.location.href = results.redirectLocation;
                    }    
            
                    var field={$this->id}.getFieldByName('formMessage');        
                    field.hide();
                         
                    // could display formMessage in a popup or other...            
                    {$this->id}.submitSuccess.fire();
                    
                } else { // results.formStatus === false
                    if  (!YAHOO.lang.isUndefined(results.errorMessages)) {
                        var errors=results.errorMessages;
                        {$this->id}.showErrorMessages(errors);
                    }            

                    if  (!YAHOO.lang.isUndefined(results.warningMessages)) {
                        var errors=results.warningMessages;
                        {$this->id}.showWarningMessages(errors);
                    }            

                        
                    if  (!YAHOO.lang.isUndefined(results.formMessage)) {
                        var formMessage=results.formMessage;                              
                        {$this->id}.showFieldErrorMessage('formMessage', formMessage);                                                
                        var field={$this->id}.getFieldByName('formMessage');        
                        field.show();
                    }
                }                        
            } else {
                alert('Error : Can not process messageType.');
            }          
        },
        failure: function(response){return false}        
    }
");                            
    }
    
    public function renderFormButtons($strbld){
        if (is_null($this->cancelCallback)) {
            $this->cancelCallback="function(){return false;}";
        }
                
        $strbld->addLines("buttons:[",false,false);
        
        if($this->renderSubmitButton){
            $strbld->addLines("{type:'submit',value:'Submit'},");
        }
                
        if($this->renderCancelButton){            
            $strbld->addLines(
"       {type:'button',value:'{$this->cancelButtonText}',
                onClick:{$this->cancelCallback}
        }");
        }
        
        $strbld->increaseIndentLevel();
        $strbld->addLine("],");
        $strbld->decreaseIndentLevel();
    }
    
    public function renderFields($strbld,$varName="fields") {
        $strbld->addLine("$varName:[");
        $strbld->increaseIndentLevel();                
        $count=0;
        foreach($this->fields as $component) {
            $field=$component->render($strbld);
            $count+=1;
            if($count<count($this->fields)) {
                $field.=",";
            }
            $strbld->addLine($field, true,false);             
        }        
        $strbld->decreaseIndentLevel();
        $strbld->addLine("]",false);
    }
    
    public function decodeFormData(&$formData) {
        foreach($this->fields as $component) {            
            if ($component->type == "select") {
                if (isset($formData[$component->id])) {
                    $value = $formData[$component->id];
                    if ($value=="SNULL") {
                        $formData[$component->id] = NULL;
                    } else {
                        // what was this code doing?
                        //$formData[$component->id] = substr($formData[$component->id],1);
                    }                        
                }
            }
            else if($component->type=="autocomplete"){               
               // trim all the excess whitespace from the data, which can lead to lookup errors
               // this happens in fileupload where the project name has a trailing whitespace
               $formData[$component->id]=trim($formData[$component->id]);
               $propDef=$this->formDef->getProperty($component->id);
               $constraint=$propDef->getConstraint();
               $constraint->generate($this->formMode);
               $lookup=$constraint->get();
                              
               $value=array_search($formData[$component->id],$lookup);
               
               if ($value=="SNULL" || is_null($formData[$component->id]) || trim($formData[$component->id])=="") {;
                        $formData[$component->id] = NULL;
               }
               else if($value===false){
                   // if the lookup value does not exist do not do transform the value and leave it as it is.
                   // TODO: This should raise some sort of lookup validation error.
               }else{
                   $formData[$component->id]=$value;
               }
            } else if ($component->type=="datetime") {

                if (isset($formData[$component->id])) {
                    $value = $formData[$component->id];                

                    $UTC = new DateTimeZone("UTC");
                    $newTZ = new DateTimeZone("America/New_York");
                    $date = new DateTime( $value, $UTC );
                    $date->setTimezone( $newTZ );
                    $value=$date->format('Y-m-d G:i:s');    
                    
                    $formData[$component->id] = $value;
                }                
            }
            //$component->decodeFormData(&$formData) {
        }           
        
        // now that things are mapped, used the modified values in formData
        
        foreach($this->formDef->getProperties() as $propDef) {
            
            if ($propDef->hasAttribute("ignore",true,$this->formMode)) {
                continue;
            }
            // Some of the propDefs may not be in the form.  Check for them
            // first.  This happens because some propDefs are not in the form.
            
            if (isset($formData[$propDef->name])) {
                $input = $formData[$propDef->name];                    
                $value = javascript2php($propDef,$input);
                $formData[$propDef->name] = $value;
            }
        }                                       
    }
        
    public function validateData() {
        global $app;
        
        $output=array();
                
        $this->formMode=$app->request->mode;                
        $data=$app->activeSession->getVarNoStrip("POST",'value');               
        $formData=json_decode($data,$associative=true);                       
        $this->decodeFormData($formData);   
        
        // need to wrap the form data in an object to modify it in place to work with the callback system.
        if ($this->formMode=="add") {
            if ($this->hasCallback("onFormPreAddValidCallback")) {
                $formObj=new stdClass();
                $formObj->formData=$formData;
                $this->fireCallback("onFormPreAddValidCallback",$formObj);
                $formData=$formObj->formData;
            } else {
                //$this->onFormPreAddValidCallback($formData);
            }
        } else if ($this->formMode=="edit" || $this->formMode=="upload") {                            
            if ($this->hasCallback("onFormPreEditValidCallback")) {
                $formObj=new stdClass();
                $formObj->formData=$formData;    
                $this->fireCallback("onFormPreEditValidCallback",$formObj);
                $formData=$formObj->formData;
            }                 
        } else if ($this->formMode=="delete") {
            if ($this->hasCallback("onFormPreDeleteValidCallback")) {
                $formObj=new stdClass();
                $formObj->formData=$formData;    
                $this->fireCallback("onFormPreDeleteValidCallback",$formObj);
                $formData=$formObj->formData;
            }                 
        } 
            
        $this->formDef->applyTransforms($formData,$this->formMode);
        $this->formDef->validate($formData,$this->formMode);
        
        $errors=$this->formDef->validationErrors;
        
        foreach($errors as $error)
        {
            $this->addError($error);
        }
        
        if(count($errors)>0)
        {            
            $formErrorMessage = 'Error: Form validation failed.';            
            $this->sendError($formErrorMessage);      
        } else {                                    
            if ($this->formMode=="add") {
                if ($this->hasCallback("onFormAddValidCallback")) {
                    $this->fireCallback("onFormAddValidCallback",$formData);
                } else {
                    $this->onFormAddValidCallback($formData);
                }
            } else if ($this->formMode=="edit" || $this->formMode=="upload") {
                if ($this->hasCallback("onFormEditValidCallback")) {
                    $this->fireCallback("onFormEditValidCallback",$formData);
                }                 
            } else if ($this->formMode=="delete") {
                if ($this->hasCallback("onFormDeleteValidCallback")) {
                    $this->fireCallback("onFormDeleteValidCallback",$formData);
                }                 
            } else{
                if($this->hasCallback("onFormSubmitValidCallback")){
                    $this->fireCallback("onFormSubmitValidCallback",$formData);
                }
            }    
        }
    }
    
    public function onFormAddValidCallback($formData) {            
        global $app;
                                
//        $dalClassName = $this->objDef->name.'DAL';       
//        if (autoloadSearch($dalClassName)){
//            $dal =  new $dalClassName();
//        } else {
//            $dalClassName=$this->objDef->getAttribute("parent").'DAL';
//            $dal =  new $dalClassName();
//        }

        $tableName=$this->objDef->getAttribute("table", $this->objDef->name);
        $dal = NewDALObj($tableName);
        
        $obj = $dal->createFromData2($this->inputExForm->formDef, $formData,$nestedObjects=true);
                
        $results = $dal->insertTransaction($obj);        
        
        if ($results->success == true) {                                
            $this->sendSuccess(null,$this->onSuccessRedirect);          
        } else {
            $this->sendError($results->getMessage());          
        }
    }
    
    public function autoComplete()
    {
        global $app;
        $output=array();
                
        $eventData = array();
        
        $fieldId=$app->activeSession->getVarNoStrip("GET",'fieldId');               
        $query=$app->activeSession->getVarNoStrip("GET",'query');
        
        $eventData['fieldId']=$fieldId;
        $eventData['query']=$query;
        
        if ($this->hasCallback("onAutoComplete")==true) {
            $this->fireCallback("onAutoComplete",$eventData);
        } else {
            $this->autoCompleteDefault($fieldId,$query);
        }
    }
    
    public function autoCompleteDefault($fieldId,$query){
        global $app;        
        $results=array();
        $results["data"]=array();
        $objDef=$this->objDef;
        
        //$securityTransform=new SecurityTransformations();  
           
        $propDef=$objDef->getProperty($fieldId);
        $constraint=$propDef->getConstraint(); 
        // only display unique constraint values, this is necessary if the same value is defined
        // for multiple labs and you are typing in an autocomplete field where the values are
        // not constrained by lab.
        $constraint->selectUniqueValues=true;
        $constraint->whereStatement->addClause(new SQLComparison("{$constraint->displayColumnName}","like","'{$query}%'"));        
        $constraint->numberOfRowsToGet=20;
        $constraint->addEmptyOption=false;
        // always constrain the values shown by hideOnAdd, just do not raise a validation error for the old values
        $constraint->generate("add");
        $results=$constraint->get();
        
        $data=array();
        $data["data"]=array();
        foreach($results as $value=>$key){
            $data["data"][]=array("value"=>$key);
        }
        
        $json_out=json_encode($data);
        echo $json_out;        
    }    
    
    public function setFormData($obj) {        
    }
    
    public function sendSuccess($formSuccessMessage,$redirectLocation=Null) {                                
        $output=array();
        
        $output["messageType"] = "form";
        if (!is_null($redirectLocation)) {
            $output["redirect"]=true;
            $output["redirectLocation"]=$redirectLocation;
        } else {
            $output["redirect"]=false;
        }
        $output["formStatus"]=true;
        $output["formMessage"]=$formSuccessMessage;
        
        $json_output=json_encode($output);
        echo $json_output;
    }   
    
    public function sendError($formErrorMessage) {                                
        $output=array();
        
        $output["messageType"] = "form";
        $output["formStatus"]=false;
        $output["formMessage"]=$formErrorMessage;
        $output["errorMessages"]=$this->fieldErrors;
        $output["warningMessages"]=$this->fieldWarnings;
        
        $json_output=json_encode($output);
        echo $json_output;
    }
    
    
    public function addError($errorObj) {
        if ($errorObj->errorLevel==0) {
            $this->addFieldError($errorObj);
        } else {
            $this->addFieldWarning($errorObj);
        }
    }
    public function addFieldError($errorObj) {        
        if (count($this->fieldErrors)==0) {
            $sep="";
        } else {
            $sep="<br/>";
        }
        array_set($this->fieldErrors,$errorObj->fullPath,$errorObj->message);                
    }
    
    public function addFieldWarning($errorObj) {
        
        if (count($this->fieldWarnings)==0) {
            $sep="";
        } else {
            $sep="<br/>";
        }
        $this->fieldWarnings[$errorObj->propertyName][]=$sep.$errorObj->message;
    }
    
    public function onFieldChange($request){
        global $app;
        $data=$app->activeSession->getVarNoStrip('POST','data');
        $data=json_decode($data, $assoc=true);
        $propDef=$this->formDef->getProperty($data["fieldName"]);
        $oldValue=javascript2php($propDef, $data["oldValue"]);
        $newValue=javascript2php($propDef,$data["newValue"]);      
        
        $columnCompositeLookups = array();        
        foreach($this->formDef->getProperties() as $propDef) {            
            if ($propDef->getAttribute("compositeTrigger",false)) {                
                // get constraint of the propdef and use it to build up a list of choices
                // for the fieldValueToCompositeSpecMap 
                
                $constraint = $propDef->getConstraint();
                if ($constraint) {
                     $constraint->generate($this->formMode);
                     $columnCompositeLookups[$propDef->name]=$constraint->get();
                }
            }
        }
        
        $valueToSpecMap=$columnCompositeLookups[$data["fieldName"]];
        
        if(isset($valueToSpecMap[$oldValue])){
            $specName=$valueToSpecMap[$oldValue];            
            foreach($this->objDef->getProperties() as $propDef) {                
                if (($propDef->getAttribute("associationType",NULL)=="HasOne") 
                    && ($propDef->getAttribute("isComposite",false)===true) 
                    && ($propDef->objectTypeOrig==$specName)){                    
                    $oldCompositePropName=$propDef->name;
                }
            }
        }
        else{
            $oldCompositePropName=null;
        }
        
        if(isset($valueToSpecMap[$newValue])){
            $specName=$valueToSpecMap[$newValue];            
            foreach($this->objDef->getProperties() as $propDef) {                
                if (($propDef->getAttribute("associationType",NULL)=="HasOne") 
                    && ($propDef->getAttribute("isComposite",false)===true) 
                    && ($propDef->objectTypeOrig==$specName)){                    
                    $newCompositePropName=$propDef->name;                    
                }
            }
            $newSpec=$app->specManager->findDef($specName);
            $fb= new InputExFormBuilder();
            $fb->buildObjDef($newSpec->name);
            $compositeForm=$fb->build("{$newSpec->name}",$this->formMode,$newSpec,$this->pageObj);
            $compositeForm->pageObj=$this->pageObj;
            $compositeForm->formDef=$newSpec;   
            
        }
        else{
            $newCompositePropName=null;
        }
        $output["formCommands"]=array();
        if($oldCompositePropName!=null){
             $output["formCommands"][]=array("command"=>"removeField","fieldName"=>$oldCompositePropName);
        }
        
        if($newCompositePropName!=null){
            $params=array("id"=>$newCompositePropName,"name"=>$newCompositePropName,
                "label"=>$newCompositePropName,"legend"=>$newCompositePropName,"className"=>"inputEx-SubGroup",
                "visible"=>false,"collapsible"=>true,"collapsed"=>false);
            $groupField=new InputExGroupField($params);
            foreach($compositeForm->fields as $field){                
                // do not add primary key fields to the composite
                if(isset($field->propDef)){
                    if(!$compositeForm->formDef->isPartOfPrimaryKey($field->propDef->id)) {
                        $groupField->addField($field);
                    }
                }
            }
            $strbld=new StringBuilder();
            $groupField->render($strbld);
            $fieldParams=$strbld->getString();
            $fieldParams=json_decode($fieldParams);
            $output["formCommands"][]=array("command"=>"addField","fieldName"=>$newCompositePropName,"fieldParams"=>$fieldParams);
        }         
        echo json_encode($output);
    }
}
?>