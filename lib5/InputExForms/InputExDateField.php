<?php
/* This field adds the following properties
 *      dateFormat: date format presented to the user default to 'm/d/Y'
 *      valueFormat: date format that the value is parsed into, returns a standard javascript
 *                   date object by default.
 */

class InputExDateField extends InputExStringField {
    
    public $dateFormat;
    public $valueFormat;
    
    public function __construct($params)
    {
        $this->fieldParams[] = "dateFormat";
        $this->fieldParams[] = "valueFormat";

        parent::__construct($params);
        
                
        if(isset($params["dateFormat"]) && !is_null($params["dateFormat"]))
        {
            $this->dateFormat=$params["dateFormat"];
        }
        else{
            $this->dateFormat="m/d/Y";
        }
        
        
        if(isset($params["valueFormat"]) && !is_null($params["valueFormat"]))
        {
            $this->valueFormat=$params["valueFormat"];
        }
        else{
            $this->valueFormat="Y-m-d";
        }
        
        
        $this->type="date";
    }  
    
}

?>