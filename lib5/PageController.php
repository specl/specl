<?php
class PageController
{
	public $pageObj;		
    public $pageActionsObj;
	public $controllers; // array of all controllers

	public function __construct($pageObj)
	{	
		$this->pageObj = $pageObj;					

        if ($this->pageObj->pageActions) {
            $this->pageActionsObj = new $this->pageObj->pageActions($pageObj);
        } else {
            $this->pageActionsObj = null;
        }
	}

    public function fireOnCreate() {
        if ($this->pageActionsObj) {
            $this->pageActionsObj->onCreate();
        }
    }
    public function fireOnRecover() {
        if ($this->pageActionsObj) {
            $this->pageActionsObj->onRecover();
        }
    }
    public function fireOnProcess() {
        if ($this->pageActionsObj) {
            $this->pageActionsObj->onProcess();
        }
    }
    public function fireOnRender() {
        if ($this->pageActionsObj) {
            $this->pageActionsObj->onRender();
        }
    }
    public function fireOnRenderZone($zoneId) {
        if ($this->pageActionsObj) {
            $this->pageActionsObj->onRenderZone($zoneId);
        }
    }
    public function fireOnEnd() {
        if ($this->pageActionsObj) {
            $this->pageActionsObj->onEnd();
        }
    }
	

	
	public function create()
	{
        $this->fireOnCreate();
	}

	
	public function recover() {

        $this->fireOnRecover();
        
		// get client cookie (if any) 
		
		// recover session 
		
			// from XML
			
			// from MySQL
			
			// from cookie
		
		// get URL request data
				
		//print_r($filterInputDef);		
		//$this->getParams = $request->filterGet($filterInputDef);
		
		//foreach($this->getParams as $key=>value) {			
		//	echo "{$param->key}<br/>";
		//}
			// validate URL data
		
		// get POST data
		
			// validate POST data
	}
		
	public function process()
	{
        global $app;

        $this->fireOnProcess();


//		$this->pageObj->recover();
//
//		foreach($this->controllers as $controller)
//		{
//			//$myParams = $this->pageObj->getControllerParams($controllerId);
//			//echo "{$controller->id}<br>";
//			$controller->recover();
//		}

		// What paramaters is this page expecting?

//		foreach($this->pageObj->parameters as $parameterObj) {
//			echo "Parameter {$parameterObj->key}<br/>";
//			// is this Parameter required?
//			if ($parameterObj->required==True) {
//
//			}
//		}


        // Flatten COntrollers
        foreach($this->pageObj->zones as $zone) {
            $controllers = $this->pageObj->getControllersForZone($zone->id);
            foreach($controllers as $controller) {
                $controller->pageObj = $this->pageObj;
                $controller->zoneObj = $zone;
            }
        }

        // was there an action?

        foreach($this->pageObj->zones as $zone) {
            $controllers = $this->pageObj->getControllersForZone($zone->id);
            foreach($controllers as $controller) {

                if ($controller->processed == false) {

                    $result=$controller->process();

                    $controller->processed = true;

                    if(!is_null($result))
                    {
                        $pageRequest=$result;
                        $serverLocation="http://".$app->activeSession->serverName;
                        $fileLocation=$app->activeSession->requestURL;

                        $newPage=$serverLocation.$fileLocation."?page=$pageRequest";
                        header("Location: $newPage");
                    }
                }
            }
        }
	}			

	public function checkAllowAccess()
	{
		return $this->pageObj->allowAccess;
	}

	public function getController($id)
	{
		if (array_key_exists($id, $this->pageObj->controllers))
		{
			return $this->pageObj->controllers[$id];
		}
		else 
		{
			return null;
		}
	}
			
	public function getControllersForZone($zoneId)
	{
		if (array_key_exists($zoneId, $this->pageObj->zones))
		{
			return $this->pageObj->zones[$zoneId]->controllers;
		}
		else 
		{
			return array();
		}		
	}
	
	public function renderZone($zoneId)
	{
        $this->fireOnRenderZone($zoneId);

		// get the Zone for this path
		if (array_key_exists($zoneId,$this->pageObj->zones)==False)
		{
			return;
		}
		
		$zoneObj = $this->pageObj->zones[$zoneId];
		
   		$controllerObjs = $this->pageObj->getControllersForZone($zoneId);
		
   		if ($controllerObjs)
   		{
   			foreach($controllerObjs as $obj)
			{
                if ($obj->shouldRender() == true) {
                    $zoneObj->renderStart($obj);
                    $obj->render();
                    $zoneObj->renderEnd($obj);
                }
       		}
   		}
	}

    public function renderHeader($zoneId)
	{
		// get the Zone for this path
		if (array_key_exists($zoneId,$this->pageObj->zones)==False)
		{
			return;
		}

		$zoneObj = $this->pageObj->zones[$zoneId];

   		$controllerObjs = $this->pageObj->getControllersForZone($zoneId);

   		if ($controllerObjs)
   		{
   			foreach($controllerObjs as $obj)
			{
				$obj->displayHeader();
       		}
   		}
	}
}