<?php
/**
 * Access Control Manager
 */
class ACManager {

    private $loginAttempts;
    private $loggedIn;
    public $userObj;
    private $defaultRoles;
    public $currentPageRole;   
    
    public function  __construct() {
        $this->loginAttempts = 0;
        $this->loggedIn = false;
        
        $this->acuDAL = NewDALObj('ACU');
        $this->userObj = NewSpecObj('ACU');        
        $this->defaultRoles = array();        
        $this->defaultRoles[] = "user";
        $this->defaultRoles[] = "tech";
    }
 
    public function login($user, $password) {
        
        $results = $this->acuDAL->login($user, $password);
        
        if ($results) {
            $this->loginAttempts = 0;
            $this->loggedIn = true;
        } else {
            $this->loginAttempts += 1;
        }
        return $results;
    }
        
    public function getUser($user) {
        
        $acuObj = $this->acuDAL->getUser($user);
        
        if (count($acuObj->ACUR)==0) {            
            $this->addDefaultRoles($acuObj);                        
            $acuObj = $this->acuDAL->getUser($user);
        }
        
        if (is_null($acuObj)) {
            return null; // failed
        } else {            
            $this->userObj = $acuObj;
            return $acuObj;
        }        
    }    
        
    public function getUserByEmail($email) {
        
        $acuObj = $this->acuDAL->getUserByEmail($email);
        if (!isnull($acuObj)) {
            $this->userObj = $acuObj;
        }
        
        if (is_null($acuObj)) {            
            // failed
            return $acuObj;
        } else {
            return $acuObj;
        }        
    } 
    
    public function getAnonymousUser() {        
        $acuObj = $this->acuDAL->getAnonymousUser();
        $this->userObj = $acuObj;
        return $acuObj;
    }
    
    public function sendUserEmail($subject,$message){
        global $app;
        $siteName = $app->site->getSetting("siteName");        
        $webAddress = $app->site->getSetting("webAddress");
        $adminEmail = $app->site->getSetting("adminEmail");        
        mailto($adminEmail, $this->userObj->email, $subject, $message);        
    }
    
    public function registerNotification($acuObj) {
        global $app;
	
        $siteName = $app->site->getSetting("siteName");        
        $webAddress = $app->site->getSetting("webAddress");
        $adminEmail = $app->site->getSetting("adminEmail");
        
        //get_settings('sitename')
        
        $message  = sprintf("Welcome to the CSHL Next Gen Sequencing LIMS!<br/>");        
        $message .= sprintf("Your account information is :<br/>");
	$message .= sprintf('Username: %s', $acuObj->user) . "<br/>\n";
	$message .= sprintf('Password: %s', $acuObj->password) . "<br/>\n";
        $message .= sprintf("<a href=\"%s\">Login</a> to LIMS.", $webAddress) . "<br/>\n";        
	
        
        $subject = 'Register Success '. $siteName;

        //mailto($from, $to, $subject, $message, $headers = '') 
        mailto($adminEmail, $acuObj->email, $subject, $message);
        
        mailto($adminEmail, $adminEmail, "RE: LIMS ".$subject, $message);
        
        return True;        
    }
    
	public function passwordReminderNotification($acuObj) 
	{
        global $app;
	
        $siteName = $app->site->getSetting("siteName");        
        $webAddress = $app->site->getSetting("webAddress");
        $adminEmail = $app->site->getSetting("adminEmail");
        
        //get_settings('sitename')
        
        $message  = sprintf("User Account information for site \"%s\"", $siteName) . "<br/>\n";
        //$message .= sprintf("Web address %s", $webAddress) . "<br/>\n";        
        $message .= sprintf("Username: %s", $acuObj->user) . "<br/>\n";
        $message .= sprintf("Password: %s", $acuObj->password) . "<br/>\n";
	$message .= sprintf("<br/>\n");
        $message .= sprintf("<a href=\"%s\">Login</a>", $webAddress) . "<br/>\n";        
        
        $subject = 'Password Reminder from '. $siteName;

        //mailto($from, $to, $subject, $message, $headers = '') 
        mailto($adminEmail, $acuObj->email, $subject, $message);
        
        $status = mailto($adminEmail, $adminEmail, "RE: LIMS ".$subject, $message);
        
        return True;	
	}
    
    public function register($eventData) {                
        global $app;
        
        $results = $this->acuDAL->insertOne($eventData);
        
        if($results->success===true){
            $userPreferencesDAL=NewDALObj('UserPreferences');
            $userPrefObjDef= $app->specManager->findDef("UserPreferences");
            $userPrefData=array("user"=>$eventData->user);
            $userPrefObjDef->addDefaultValues($userPrefData,"any");
            $userPreferencesDAL->insertOne($userPrefData);
            
            // give all users customer privileges for the CSHLGenomeCenter
            $dal=NewDALObj('UserFacilityCustomer');
            $objdef= $app->specManager->findDef("UserFacilityCustomer");
            $data=array("user"=>$eventData->user, 'facilityId'=>'CSHLGenomeCenter');
            $objdef->addDefaultValues($data,"any");
            $dal->insertOne($data);
            
            // give McCombie users manager privileges for the CSHLGenomeCenter
            if($eventData->defaultLaboratory=="McCombie"){
                $dal=NewDALObj('UserFacilityManager');
                $objdef= $app->specManager->findDef("UserFacilityManager");
                $data=array("user"=>$eventData->user, 'facilityId'=>'CSHLGenomeCenter');
                $objdef->addDefaultValues($data,"any");
                $dal->insertOne($data);
            }
        }
        
        return $results;
    }

    public function changePassword($password) {                
               
        // create the new
        $newACUObj = clone $this->userObj;        
        $newACUObj->password = $password;        
        $results = $this->acuDAL->updateOneByPk($this->userObj,$newACUObj);                
        
        return $results;
    }
    
    public function addDefaultRoles($acuObj) {
        
        $acurDAL = NewDALObj('ACUR');
        foreach($this->defaultRoles  as $acr) {            
            $acurObj = NewSpecObj('ACUR');
            $acurObj->user = $acuObj->user;
            $acurObj->acr = $acr;
            $this->userObj->ACUR[$acr] = $acr;
            $acurDAL->insertOne($acurObj);
        }
    }
    
    /*
     * This function can be used to check if the current user of the session
     * has a role.  The input parameter role can be an array of roles.
     */
    public function checkUserRole($role) {
        
        if (is_array($role)) {
            $result = array_intersect($this->userObj->roles, $role);        
        
            if (count($result)>0) {
                return true;
            } else {
                return false;
            }
        } else {
            if(in_array($this->userObj->roles,$role)){
                return true;
            } else {
                return false;
            }                  
        }
        
    }
}
?>