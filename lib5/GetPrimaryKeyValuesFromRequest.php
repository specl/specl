<?php

class GetPrimaryKeyValuesFromRequest extends WorkFlowComponent {
    public function __construct($params){
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        }
        else{
            throw new Exception("the parameter 'objDef' is requried");
        }
    }
    
    public function execute($request){        
        $keys = array();        
        foreach($this->objDef->getPrimaryKey() as $propKeyName) {                 
            $propDef = $this->objDef->getProperty($propKeyName);
            if(isset($request->parameters[$propDef->name])){
                $keys[$propDef->name]=$request->parameters[$propDef->name];
            }
        }
        return $keys;
    }
}

?>