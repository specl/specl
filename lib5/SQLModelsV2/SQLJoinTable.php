<?php
class SQLJoinTable {
    public function __construct($joinType,$toTable){
        $this->joinType=$joinType;
        $this->toTable=$toTable;
        $this->conditions=array();
    }
    
    public function addCondition($fromTable,$fromColumn,$toColumn){
        $this->conditions[]=new SQLJoinCondition($fromTable,$this->toTable,$fromColumn,$toColumn);
    }
    
    public function generateSQL(){
        $SQL="{$this->joinType} JOIN `{$this->toTable}`";        
        if(!empty($this->conditions)){
            $first=true;
            foreach($this->conditions as $condition){
                if($first===true){
                    $SQL.=" ON (";
                    $first=false;
                }
                else{
                    $SQL.=" AND ";
                }
                $SQL.="`{$condition->fromTable}`.`{$condition->fromColumn}`=`{$condition->toTable}`.`{$condition->toColumn}`";
            }
            $SQL.=")";
        }
        return $SQL;
    }
}

?>
