<?php

class SQLColumnV2 {
    public function __construct($columnName, $tableName,$columnAlias){
        $this->columnName=$columnName;
        $this->tableName=$tableName;
        $this->columnAlias=$columnAlias;
    }
    
    public function generateSQL(){
        $SQL="";
        if($this->tableName!==null){
            $SQL.="`{$this->tableName}`.";
        }
        $SQL.="`{$this->columnName}`";
        if($this->columnAlias!==null){
            $SQL.=" as {$this->columnAlias}";
        }
        return $SQL;
    }
}

?>
