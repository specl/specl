<?php
//TODO: Better error handling making sure the correct types go with the correct operations:
class SQLValueFilter {
    public $tableName;
    public $columnName;
    public $operation;
    public $value;
    public function __construct($tableName,$columnName,$operation,$value,$conjunction="AND"){
        $this->tableName=$tableName;
        $this->columnName=$columnName;
        $this->operation=$operation;
        $this->value=$value;
        $this->conjunction=$conjunction;
    }
    public function generateSQL(){
        global $app;
        $SQL="`{$this->tableName}`.`{$this->columnName}`";
        $objDef=$app->specManager->findDef("{$this->tableName}");
        $propDef=$objDef->getProperty("{$this->columnName}");
        // TODO: Add error handling when the value is null and the operator is not "=" or "!="
        if($this->value===null){
            if($this->operation=="="){
                $SQL.=" is null";
            }
            else{
                $SQL.=" is not null";
            }
        }
        // TODO: Add error handling when the value is an array and the operator is not "in" or "not in"
        else if(is_array($this->value)){
            $arrayValue="(";
            $first=true;
            foreach($this->value as $singleValue){
                if($first===true){
                    $arrayValue.=ConvertValueToMySQL($singleValue,$propDef->objectType);
                    $first=false;
                }
                else{
                   $arrayValue.=",";
                   $arrayValue.=ConvertValueToMySQL($singleValue,$propDef->objectType); 
                }                
            }
            $arrayValue.=")";
            $SQL.=" ".$this->operation." {$arrayValue}";
        }
        else{
            $value=ConvertValueToMySQL($this->value,$propDef->objectType);
            $SQL.=" ".$this->operation." {$value}";
        }
        return $SQL;
    }
}

?>