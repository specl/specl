<?php

class SQLFilterGroup {
    public function __construct(){
        $this->filters=array();
    }
    
    public function addValueFilter($objectName,$propertyName,$operation,$value,$conjunction="AND"){
       $this->filters[]=new SQLValueFilter($objectName,$propertyName,$operation,$value,$conjunction);
    }
    
    public function generateSQL(){
        $SQL="(";
        $first=true;
        foreach($this->filters as $filter){
            if($first===true){
                $SQL.=$filter->generateSQL();
                $first=false;
            }
            else{
                $SQL.=$filter->conjunction.$filter->generateSQL();
            }
        }
        $SQL.=")";
        return $SQL;
    }
}

?>