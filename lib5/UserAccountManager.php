<?php
/**
 * Description of PagesManager
 *
 * This class is for validating page requests, determining which pages
 * to display in navigations, loading page Page definitions into a pages
 * array from either XML of MySQL.
 *
 * @author Tony
 */
class UserAccountManager {

    private $_unsuccessfulLoginAttempts;
    private $_loggedIn;
    
    public function  __construct() {
        $this->_unsuccessfulLoginAttempts = 0;
        $this->_loggedIn = false;
    }

    public function attemptLogin($loginMessage) {
        global $log;
        global $app;

        if (get_class($loginMessage)!="LoginMessage") {
            $log->error("Authorization->attemptLogin","not passed a LoginMessage");
            return false;
        }

        $sql = "select exists(select * from UserAccount where username=%username%
                and password=%password% limit 1) as Success;";
        
        $app->sqlBuilder->setStatement($sql);
        $app->sqlBuilder->createParam("username", "string", $loginMessage->username);
        $app->sqlBuilder->createParam("password", "string", $loginMessage->password);
        $sql = $app->sqlBuilder->bindParams();

        //  echo $sql;
        $results = $app->dbConnSys->select($sql);

        if ($results!=0)
        {
            $row = $app->dbConnSys->getRowAssoc($results);

            $success = $row['Success'];

            if ($success==0) {
                return false;
            } else {

                $app->sessionManager->setStateLoggedIn($app->sessionId,$loginMessage->username);

                return true;
            }
        } else {
            return false;
        }
    }

    public function logout() {
        global $app;

        $app->sessionManager->setStateLoggedOut($app->sessionId);


    }

    
    public function attemptRegister($registerMessage) {
        global $log;
        global $app;

        if (get_class($registerMessage)!="RegisterMessage") {
            $log->error("Authorization->attemptRegister","not passed a RegisterMessage");
            return false;
        }

        $userObj = new UserAccount();

        $userObj->username = $registerMessage->username;
        $userObj->password = $registerMessage->password;
        $userObj->email = $registerMessage->email;
        $userObj->dateCreated = getCurrentDateTime();
        $userObj->active = true;

        try {
            $app->zorm->save($userObj);
            return true;
        } catch (Exception $exception) {
            return false;
        }

    }

    public function  loadFromXML() {
        global $app;
        
        //TODO:Error checking, add try catch block
        $tmp = $this->specManager->getObjectFromXML($app->dbPath."Users.xml", "Users");
        //TODO:Error checking
        $this->pages=$tmp;
    }

    public function doesUserExist($userId) {

        if (array_key_exists($userId,$this->users)) {
            return true;
        } else {
            return false;
        }
    }

     public function doesUsernameExist($username) {
        global $app;

        $sql = "select exists(select * from UserAccount where username=%username% limit 1) as Success;";

        $app->sqlBuilder->setStatement($sql);
        $this->sqlBuilder->createParam("username", "string", $username);
        $sql = $app->sqlBuilder->bindParams();
        
        $results = $app->dbConnSys->select($sql);

        if ($results!=0)
        {
            $row = $app->dbConnSys->getRowAssoc($results);

            $success = $row['Success'];

            if ($success==0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
     }

     public function doesEmailExist($email) {
        global $app;

        $sql = "select exists(select * from UserAccount where email=%email% limit 1) as Success;";

        $app->sqlBuilder->setStatement($sql);
        $this->sqlBuilder->createParam("email", "string", $email);
        $sql = $app->sqlBuilder->bindParams();
        $results = $app->dbConnSys->select($sql);

        if ($results!=0)
        {
            $row = $app->dbConnSys->getRowAssoc($results);

            $success = $row['Success'];

            if ($success==0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
     }

    public function getUser($userId) {

        if (array_key_exists($userId,$this->users)) {

            $userObj = $this->users{$userId};

          	if ($userObj->isLoaded == False) {
                $this->loadUser($userObj);
            }

            return $userObj;

        } else {
            return null;
        }
    }

    public function loadUser($userObj) {
        global $app;
        //TODO:Error checking, add try catch block
        $filename = $app->dbPath."users/{$userObj->id}.xml";
        $tmp = $this->specManager->getObjectFromXML($filename, "User");
        $userObj->isLoaded = true;
    }

    public function getProperty($property) {

        return null;

    }

}
?>