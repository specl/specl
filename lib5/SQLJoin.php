<?php
class SQLJoin
{
    public $err;    
    
    public function __construct($table1,$table2,$table1Alias,$table2Alias,$column1,$column2,$jointype="inner")
    {
        $this->err=array();
        
        $this->table1=$table1;
        
        $this->table2=$table2;

        $this->table1Alias=$table1Alias;
        
        $this->table2Alias=$table2Alias;
        
        $this->column1=$column1;
        
        $this->column2=$column2;
        
        $this->jointype=$jointype;
        
        if (empty($this->table1))
        {
            $this->err[]="table1 passed empty string";
        }
        if (empty($this->table2))
        {
            $this->err[]="table2 passed empty string";
        }
        if (empty($this->table1Alias))
        {
            $this->err[]="table1Alias passed empty string";
        }
        if (empty($this->table2Alias))
        {
            $this->err[]="table2Alias passed empty string";
        }
        if (empty($this->column1))
        {
            $this->err[]="column1 passed empty string";
        }
        if (empty($this->column2))
        {
            $this->err[]="column2 passed empty string";
        }
        
    }


}
?>