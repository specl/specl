<?php
/**
 * Description of App
 *
 * @author tony
 */
class App {
    const JSSEP='$_$';
    const ZONESSEP='/';
    const CLI='cli';
    const HTTP='http';
    
    public $runtype; // is this being from command line or HTTP?
    public $specManager; // zones, really runs from specs...    
    
    public $sessionManager; //Session
    public $sessionObj;
    public $acManager; // Access Control 
    
    public $site; // Site
    public $connections; // Array of Connection
    public $zorm;

    public $resourceManager;
    public $menuManager;
    public $userManager;

    public $httpSession;
    public $cliSession;
    public $activeSession;

    public $request;
    
    public $webPage;
    public $webService;
        
    public $defaultWebPageClass;
    public $defaultMenuItemId;
    public $menuItemId;
    public $menuItemObj; 
    
    public $connectionMap;
    
    public $requestedComponent;
    public $redirect;
    public $userStorage;
    public $globalData;
    
    public $effectiveRole;
    
    public $relPath;
    
    public $errors;  
            
    public $cacheEnabled;
    public $siteBase;
    
    public $loadMenuXML; /* This variable is used in automated testing so the menu XML 
                            does not have to be parsed every time which is slow */
     
    
    public function  __construct() {
        
        $this->errors = array();
        $this->loadMenuXML=true;

        if (isset($_SERVER['SERVER_ADDR'])) {
            $this->runtype = self::HTTP;
            $this->httpSession = new HTTPSession();
            $this->activeSession = $this->httpSession;
        } else {
            $this->runtype = self::CLI;
            $this->cliSession = new CLISession();
            $this->activeSession = $this->cliSession;
        }

        //$this->dbPath = "./db/";                
        //$this->configPath = "./Config/";
        $this->webPage = null;
        $this->webService = null;
        $this->defaultWebPageClass = "SiteBasePage";
        $this->request = null;
        $this->defaultMenuItemId = null;
        $this->menuItemId = null;
        $this->menuItemObj = null;
        $this->connectionMap = new ConnectionMap();        
        $this->redirect = null;
        $this->globalData = array();
        $this->effectiveRole = Null;        
        $this->requestedComponent = Null;
        $this->cacheEnabled = false;
        
        /* TODO : GEt rid of this!!!!!!
         * Required for CompositeSpecs and BaseDALV2.
         */
        $this->associationGraphFile=dirname(__FILE__) ."/../AdventureWorksDB/CodeGen/association_graph.data";
    }

    public function go() {
        $this->start();    
        $this->recover();    
        $this->dispatch();
        $this->end();
    }
    
    
    public function start() {
        global $zonesBase;

        $this->machinename=strtolower(php_uname('n'));
        
        $this->specManager = new SpecManager();
        $this->specManager->init();               
        
      try {
            $this->site=$this->specManager->getObjectFromXML($this->configPath."$this->machinename".App::ZONESSEP."Site.xml", "Site");       
            
            // check to see if config is writable?
            
        } catch(Exception $e) {
            echo "FATAL ERROR : Site.xml could not be read.\n";
            throw $e;
            // the config directory is not created
            // the config directory is not readable
            // the site.xml fiel is missing or not readable
            // the XML was bad            
        }
        
        
        if ($this->cacheEnabled) {
//            if(xcache_isset("site")){             
//                $site= unserialize(xcache_get("site"));
//                $this->site= $site;
//            }
//            else{          
//                $siteObj=$this->specManager->getObjectFromXML($this->dbPath."Site.xml", "Site");          
//                $this->site=$siteObj;
//                $site=serialize($siteObj);
//                xcache_set("site",$site);
//            } 
        }
        else{
             // delete existing object from cache
             //xcache_unset("site");
        }
        
        //$this->loadResources();        
        // this is wrong
        // there needs to be a connection for authorization
        // there needs to be a connection for sessions and pages and app stuff
        // there needs to be an application specific  connection or connections
        // the good news is this is easy to accomplish.
        
        
        $this->dbSeqLims = new Connection();        
        $this->dbSeqLims->user = $this->site->Connection->user;
        $this->dbSeqLims->pass = $this->site->Connection->pass;
        $this->dbSeqLims->host = $this->site->Connection->host;
        $this->dbSeqLims->schema = $this->site->Connection->schema;
                
        
        try {
            $this->dbSeqLims->connect();            
        } catch (Exception $e) {
            die('connection fails');
        }
                
        $this->connectionMap->addConnection("seqlims" , $this->dbSeqLims);
        $this->connectionMap->defaultConnection = "seqlims";
        
        $this->sessionManager = new SessionManager();   
        
        if ($this->runtype == self::HTTP) {            
            $this->sessionManager->start();
            $uploadDir=$zonesBase."../uploads";            
            $this->createUploadDir($uploadDir,$this->sessionManager->sessionObj->sessionId);               
        } else {
            // TODO: figure out how to simulate the PHp session 
            //$this->sessionManager->start();
        }
        
        $this->resourceManager = new ResourcesManager($this->specManager);        
        $this->menuManager = new MenuManager($this->specManager);                                    
        $this->pagesManager = new PagesManager($this->specManager);          
        //$this->pagesManager->loadFromXML();             
        $this->acManager = new ACManager();             
        $this->pageCacheDir=$zonesBase."../pageCache";
        
        if ($this->runtype == self::HTTP) {  
            if(!file_exists($this->pageCacheDir)){
                mkdir($this->pageCacheDir);
            }        
            $this->sessionStorage=new SessionStorage2($this->sessionManager->sessionObj->sessionId);   
        }
    }

//    public function loadResources() {        
//    }
    
    public function recover() {        
        if ($this->sessionManager->sessionObj->loggedIn==true) {
            $this->acManager->getUser($this->sessionManager->sessionObj->user); 
            $this->userStorage=new UserStorage($this->sessionManager->sessionObj->user);
            $this->userPageCacheDir=$this->pageCacheDir."/".$this->sessionManager->sessionObj->user;
            if(!file_exists($this->userPageCacheDir)){
                mkdir($this->userPageCacheDir);
            }
        } else {
            $this->acManager->getAnonymousUser();
        }  
        
        // set the session and user global parameters
        $this->setValue("SESSION.userId", $this->sessionManager->sessionObj->user);
        $this->setValue("USER.defaultLaboratory", $this->acManager->userObj->defaultLaboratory);
        $this->setValue("USER.laboratories", $this->acManager->userObj->laboratoriesString);
    }

    public function redirectToDefaultRequest() {
        // this need to be done better.
        $this->request->resourceId = $this->site->defaultResourceId;
        $this->request->pageId = 'Home';
        $this->request->menuItemId = null;
        $this->request->requestType = null;
        $this->request->controllerId = null;
        $this->request->actionName = null;
        $this->request->mode = "any";
        $this->request->parameters = array();    
        
        $this->menuItemObj=Null;
    }
            
    
    public function dispatchEffectiveRoleAccessCheck() {  
        // check to make sure that the user has sufficient privilages to view a page as the role specified in the request.
        if(isset($this->request->effectiveRole)){            
            $this->effectiveRole = $this->request->effectiveRole;            
            if(!$this->acManager->userObj->hasACR($this->request->effectiveRole)){
                $this->accessError=true;
                $this->accessErrorMessage="Access denied: You do not have the proper permissions to view a page in the role of {$this->request->effectiveRole}";                
                return false;
            }
        }        
        return true;
    }
    
    public function dispatchAccessCheck($page) {  
        
        $allow=false;
        
        if (!is_null($page->resourceId)) {

            $allow=true;
            $acoRequired = false;
            $acofRequired = false;
            $acrRequired = false;
            $acuRequired = false;
            $actRequired = false;

            if (!is_null($page->aco)) {
                // a aco is required
                $acoRequired = true;

                if (!is_null($page->acf)) {
                    $acofRequired = true;
                }
            }

            if (!is_null($page->acr)) {
                // a acr is required
                 $acrRequired = true;
            }
            if (!is_null($page->act)) {
                // a acr is required
                $actRequired = true;
            }
            if (!is_null($page->acu)) {
                // a acu is required, well aren't we special. Mr or Mrs fancy pants.
                $acuRequired = true;
            }            

            if (!is_null($page->showInSessionState)) {                    
                if ($page->showInSessionState!=$this->sessionManager->state()) {
                    $allow=false;                        
                    $this->accessError=true;
                    $this->accessErrorMessage="Access denied: Page can not be access in current state.";                        
                }                            
            }

            if (($allow==true)  && ($acuRequired==true)) {
                if ($this->acManager->userObj->user != $page->acu) {
                    $allow=false;
                    $this->accessError=true;
                    $this->accessErrorMessage="Access denied: Page requires ACU {$page->acu} .";                                               
                }
            } else {
                if ($acrRequired==true) {
                    if ($this->acManager->userObj->hasACR($page->acr)==false) {
                        $allow=false;    

                        $this->accessError=true;
                        $this->accessErrorMessage="Access denied: Page requires ACR {$page->acr} .";                                               

                    }                        
                }
                if (($allow) && ($acofRequired==true)) {
                    if ($this->acManager->userObj->hasACOF($page->aco,$page->acf)==false) {
                        $allow=false;    

                        $this->accessError=true;
                        $this->accessErrorMessage="Access denied: Page requires ACOF {$page->aco} {$page->acf} .";                                               

                    }
                } else if ($acoRequired==true) {
                    if ($this->acManager->userObj->hasACO($page->aco)==false) {
                        $allow=false;    

                        $this->accessError=true;
                        $this->accessErrorMessage="Access denied: Page requires ACO {$page->aco} .";                                                                           
                    }                        
                }
            }
            
            if(($allow==true)  && ($actRequired==true)){
                 if ($this->acManager->userObj->hasACT($page->act)==false) {
                        $allow=false;    

                        $this->accessError=true;
                        $this->accessErrorMessage="Access denied: Page requires ACT {$page->act} .";                                                                           
                } 
            }

        }       
        
        return $allow;
    }
    
    public function appChecks() {
        return true;
    }
    
    /*
     *  CheckDeletePermissions is responsible for enabling/disabling the users ability to delete
     *  data from the database. This function modifies the page object variable enableDelete to
     *  achieve the desired effect. This function overrides the values set in the page XML.
     * 
     *
     *   Currently the rule is if the user has admin or siteadmin roles then they can delete data,
     *   else they are not able to delete data.     * 
     *  
     *   TODO: Implement delete permissions on a more fine grained level based on the security
     *        permissions in the database.
     */
    public function checkDeletePermissions(){
        $pageObj=$this->pagesManager->getPage($this->request->pageId);
        if($this->acManager->userObj->hasACR("admin") || $this->acManager->userObj->hasACR("siteadmin")){
            if(is_null($pageObj->getParam("enableDelete"))){
                $pageObj->setParam("enableDelete","true");
            }
        }
        else{
            $pageObj->setParam("enableDelete","false");
        }
        
    }
    
    public function dispatch() {  
        
        $this->getRequest();          
        $this->accessError=false;
        $this->accessErrorMessage="";
        
        if ($this->dispatchEffectiveRoleAccessCheck()===false) {
            $this->redirectToDefaultRequest();                        
        }
                        
        // Perform security checks based on parameters set in the page xml
        if ($this->request->requestType == "page" || !is_null($this->request->page)) {                      
            if ($this->dispatchAccessCheck($this->request->page)===false) {              
                $this->redirectToDefaultRequest();            
            }            
        }
        
        // checkDeletePermissions modifies the page object, which is loaded from cache on a webservice call
        // therefore it has no effect unless you are building the page during a web request. Parsing the page XML
        // slows down the webservice request. Do we even use this function at all?
        if (($this->request->getParameterValue("buildPage",true)===true) || ($this->request->getParameterValue("buildPage",true)==="true")) {          
           $this->checkDeletePermissions();
        }         
        
        // workflow checks        
        if ($this->appChecks()===false) {
            $this->redirectToDefaultRequest();                        
        }
        
        // actually dispatch
        
        // check page to make sure component exists, if not send error response.
        $this->webPageClass = $this->request->resourceId."Page";
        if (!autoLoadSearch($this->webPageClass)) {                 
            $this->addError("autoLoadSearch could not find page {$this->webPageClass}.");
            $this->webPageClass = $this->defaultWebPageClass;                        
            $this->menuItemObj=Null;
        }
                
        // decide if this WebPage should use Web Page Template
        $this->webPage = new $this->webPageClass($this->request->pageId);
        
        // now we know what the page has available?
        if ($this->menuItemObj) {
            $this->webPage->title = $this->menuItemObj->title;
        } else {
            $this->webPage->title = $this->request->resourceId;
        }
        
        if (($this->request->getParameterValue("buildPage",true)===true) ||
                ($this->request->getParameterValue("buildPage",true)==="true")) {  
            // if the effective role is not define in the URL or menu, get the effectiveRole from the page acr. 
            if(!isset($this->request->effectiveRole)){
                $this->request->effectiveRole=$this->request->page->acr;
            } 
            $this->acManager->currentPageRole=$this->request->effectiveRole;
            $this->webPage->page = $this->pagesManager->getPage($this->request->pageId);
            $this->webPage->build($this->request);
            
            // process the webpage before serializng it, because it could change the components on the page
            // based on user input data. This is what happens in fileupload component.
            $this->webPage->process($this->request);
                                  
            //store page in file cache
            if($this->sessionManager->sessionObj->loggedIn==true){
                $outFileName=$this->userPageCacheDir."/".$this->request->pageId;
                $outData=serialize($this->webPage);   
               // xcache_set($outFileName,$outData);
                file_put_contents($outFileName, $outData);
            }
            
            // store the effectiveRole in a session variable, so the all webservices called from this page have the same effective role.
            // this is necessary because the effectiveRole could be determined by a page parameter in the XML file, which is not loaded on a webservice call.
            $this->sessionStorage->store("{$this->webPage->id}_effectiveRole",$this->request->effectiveRole);
        }
        else{
            if($this->sessionManager->sessionObj->loggedIn==true){                
                //load page from file cache
                $inFileName=$this->userPageCacheDir."/".$this->request->pageId;
                $inData=file_get_contents($inFileName);
                //$inData=xcache_get($inFileName);
                $this->webPage=unserialize($inData);
                
                $effectiveRole=$this->sessionStorage->retrieve("{$this->webPage->id}_effectiveRole");
                
                $this->request->effectiveRole=$effectiveRole;
                $this->acManager->currentPageRole=$effectiveRole;
            }
                
            else{
                $this->webPage->build($this->request);
            }            
        }
        
        // Determine the type of action. Is it a page rendering action or is it a webservice.
        // if a controllerId is not specified get action from page component
        if(is_null($this->request->controllerId)){
            $this->requestedComponent=$this->webPage;
        }
        // if the controller id is specified get action from the specified component
        else{
            $this->requestedComponent=$this->webPage->getPageComponent($this->request->controllerId);  
        }
        
        // if actionName is not specified in the request get the default action name
        if(is_null($this->request->actionName)){
            $this->request->actionName=$this->requestedComponent->getDefaultAction();
        }         
        // get requestType from actionName
        $action=$this->requestedComponent->getAction($this->request->actionName);
        $this->request->requestType=$action->actionType;
                        
        // TODO: Add better dispatching for page and web requests
        if($this->request->requestType=="page"){            
            
            if (!is_null($this->redirect)) {
                // oh no!  we won't get to render this page because it called
                // a redirect...
                header("Location: {$this->redirect}");
            }
            $this->webPage->render();
        }
        else{                    
            $this->requestedComponent->{$action->functionName}($this->request);
        }   
    }             

    public function end() {
    }
    
    public function getConnection($id=null) {       
        return $this->connectionMap->getConnectionObjForId($id);    
    }
    
    public function loadPage() {
                
        try {
            $page = $this->pagesManager->loadPage($this->request->pageId);
            $this->request->resourceId = $page->resourceId; 

            // Check which actionName to use.  It could come directly from the request or from the page.
            if (!isset($this->request->actionName)) {
                if (isset($page->actionName)){
                    $this->request->actionName = $page->actionName;
                }
            }
            
            // Check which controllerId to use.  It could come directly from the request or from the page.
            if (!isset($this->request->controllerId)) {
                if(isset($page->controllerId)){
                    $this->request->controllerId = $page->controllerId;
                }                
            } 
            
            $this->request->page = $page;
        } catch (Exception $e) {
            $this->request->page = Null;
            $this->request->resourceId = Null;
            echo "page '{$this->request->pageId}' not found";
        }          
    }
    
    
    public function getMenuItem($menuItemId) {
                
        $menuItemObj = Null;
        
        // first, is the menuItemId valid?
        if ($this->menuManager->doesMenuExist($menuItemId)==False) {
            // TODO: redirect to the default page, do not let bad requests
            // linger in the URL
            return Null;
        } else {
            $menuItemObj = $this->menuManager->getMenu($menuItemId);                                                
        }        
        
        return $menuItemObj;
    }
    
    public function getRequest() {
        
        $kv = $this->activeSession->getAllKeyValuePairs();
        $this->request = new Request();
        $this->request->build($kv);
        
        // Only load the page and the menu XML if a webservice is not being called.
        
        $buildPage = $this->request->getParameterValue("buildPage",true);
                
                
        if (($buildPage===true)||($buildPage==="true")){
            $loadPage=true;
            
            if ($this->cacheEnabled) {
//                if(xcache_isset("xmlMenu")){  
//                    $menu= unserialize(xcache_get("xmlMenu"));
//                    $this->menuManager->menu= $menu;
//                }
//                else{
//                    $this->menuManager->loadFromXML(); 
//                    $menu=serialize($this->menuManager->menu);
//                    xcache_set("xmlMenu",$menu);
//                }
            } else {    
                // Delete existing object in cache
//                xcache_unset("xmlMenu");
                if($this->loadMenuXML){
                    $this->menuManager->loadFromXML(); 
                }
            }
        }
        else{
            $loadPage=false;
        }
        
        
   
        
        // cache the resource manager data so the XML does not need to be parsed for webservices.
        // this information should be stored in a database table, not in an xml file.
//        if (($buildPage===true)||($buildPage==="true")){ 
//            
//            if ($this->cacheEnabled) {
////                if(xcache_isset("resources")){  
////                    $resources= unserialize(xcache_get("resources"));
////                    $this->resourceManager->resources= $resources;
////                }
////                else{   
////                    $this->resourceManager->loadFromXML(); 
////                    $resources=serialize($this->resourceManager->resources);
////                    xcache_set("resources",$resources);
////                }
//            } else {
////                // Delete existing object in cache
////                xcache_unset("resources");
//                $this->resourceManager->loadFromXML(); 
//            }
//            
//            if($this->sessionManager->sessionObj->loggedIn==true){
//                $outFileName=$this->userPageCacheDir."/"."resourceManager";
//                $outData=serialize($this->resourceManager);                
//                file_put_contents($outFileName, $outData);
//            }
//        }
//        else{
//            if($this->sessionManager->sessionObj->loggedIn==true){                                
//                $inFileName=$this->userPageCacheDir."/"."resourceManager";
//                $inData=file_get_contents($inFileName);
//                $this->resourceManager=unserialize($inData);
//            }
//            else{
//                $this->resourceManager->loadFromXML();
//            }
//        }
        
        // is this for a menuItemId?        
        if (is_null($this->request->menuItemId)===False) { // there is a menuItemId        
            $this->createRequestFromMenuItemId($this->request->menuItemId);           
        } else if (isset($this->request->pageId) && $loadPage) { // there is a pageId            
            $this->loadPage();                  
        }
        
//        if (is_null($this->request->resourceId)) {
//            $this->request->resourceId = $this->site->defaultResourceId;
//        }
//            
//        if ($this->resourceManager->doesResourceExist($this->request->resourceId)==False) {
//            $this->request->resourceId = $this->site->defaultResourceId;
//        }
        
        if(isnull($this->request->page) && $loadPage) {                        
            $this->request->resourceId = $this->site->defaultResourceId;
            $this->request->pageId=$this->request->resourceId;            
            $this->loadPage();            
        }       
        
        // Storing the effective role here is wrong but it is still currently needed by some custom Specs
        // and the security component.
        if(isset($this->request->effectiveRole)){
            $this->acManager->currentPageRole = $this->request->effectiveRole;
        }
    }
    
    public function createRequestFromMenuItemId($menuItemId) {
        
        $menuItemObj = $this->getMenuItem($menuItemId);

        if (is_null($menuItemObj)) {
            $this->request->menuItemId = null;
        } else {
            // what resource does this menu id map to?                
            $this->menuItemId = $this->request->menuItemId;
            $this->menuItemObj = $menuItemObj;                                                
            $this->request->requestType = "page";                                                                      
            $this->request->pageId = $this->menuItemObj->pageId;                
            $this->loadPage();

            // If the effective role is not set in the URL get it from the MenuObj, if it is set.
            if(isset($this->menuItemObj->effectiveRole) && !isset($this->request->effectiveRole)){
                $this->request->effectiveRole=$this->menuItemObj->effectiveRole;
            }                
        }
    }
            
    public function dispatchWebPage() {
    }

    public function dispatchWebService() {
    }    

    
    // added May 25 by ALL
    // Required in order to merge the Schema Compiler sub-system into the main Zones/Specl library
    public function loadTableSchemaSpecs() {
        $specs = new TableSchemaSpec();
        $specs->defSpec($this->specManager);
        $this->specManager->crossCheck();
    }
    
    public function setRedirect($location=null) {
        
        // the location should be validated.        
        // there is the possiblity of infinte loops if the location
        // is something that also redirects...
        if ($location==null) {
            $this->redirect="index.php?menuItemId=Home";
        } else {
            $this->redirect=$location;
        }
    }
    
    public function createUploadDir($uploadDir,$sessionId){
        
        if(file_exists($uploadDir)==false){
            mkdir($uploadDir);
        }
        
        $sessionDir=$uploadDir.'/'.$sessionId;
        $this->sessionUploadDir=$sessionDir;
        
        if(file_exists($sessionDir)==false){
            mkdir($sessionDir);
        }
    }
    
    
    public function getValue($key, $defaultValue=Null) {
        
        
        $key2 = trim($key,"%");
        $parts = explode(".",$key2);
        $varRoot = $parts[0];                
        $varName = $parts[1];                               
        
        if (isset($this->globalData[$varRoot])) {
            $global = $this->globalData[$varRoot];    
            
            if (isset($global[$varName])) {            
                return $global[$varName];            
            }
        }

        return $defaultValue;
    }
    
    public function setValue($key, $value) {
        
        $key2 = trim($key,"%");
        $parts = explode(".",$key2);
        $varRoot = $parts[0];                
        $varName = $parts[1];                               
        
        if (isset($this->globalData[$varRoot])) {
            $global = $this->globalData[$varRoot];                                            
        } else {
            $this->globalData[$varRoot]=array();            
        }
            
        $this->globalData[$varRoot][$varName]=$value;               
    }
   
    public function setGlobalData($key, $data) {
        $this->globalData[$key]=$data;
    }
    
     
    public function globalReplace($str) {
        
        $regex="/\%([a-zA-Z0-9_]+)\.([a-zA-Z0-9_]+)\%/";

        preg_match_all($regex,$str,$matches);
         
        if (count($matches)==3) {
            $theMatch = $matches[0][0];
            $key1 = $matches[1][0];
            $key2 = $matches[2][0];
                        
            if (isset($this->globalData[$key1])) {
                $dataObj = $this->globalData[$key1];
                
                if(is_array($dataObj)){
                     if (isset($dataObj[$key2])) {
                        $value = $dataObj[$key2];                        
                        $str = str_replace($theMatch, $value, $str);        
                    }
                }
                else{
                     if (isset($dataObj->{$key2})) {
                        $value = $dataObj->{$key2};                        
                        $str = str_replace($theMatch, $value, $str);        
                    }
                }               
            }            
        }        
        
        return $str;
    }         
    
    public function fillInGlobals($str) {                
                
        $regex = "/(\%OBJ\.[A-Za-z]*\%)|(\%SESSION\.[A-Za-z]*\%)|(\%USER\.[A-Za-z]*\%)/";
        preg_match_all($regex,$str,$matches);
        if (is_array($matches)) {
            foreach ($matches[0] as $value) {
                $key = trim($value,"%");
                $parts = explode(".",$key);
                $varRoot = $parts[0];                
                $varName = $parts[1];                
                if (isset($this->globalData[$varRoot])) {
                    $global = $this->globalData[$varRoot];                    
                    if (isset($global[$varName])) {
                        $value = $global[$varName];                        
                        $str = preg_replace("/\%".$key."\%/", $value, $str);                           
                    }
                }
            }
        }                              
        
        return $str;
    }    
    
//    public function getValue($path) {
//
//        $value = null;
//        $path = trim($path);
//
//        if ($path[0]==App::ZONESSEP) {
//            $index=1;
//        } else {
//            $index=0;
//        }
//        
//        $paths = explode(App::ZONESSEP,$path);
//        $topLevelObj = null;
//        $topLevel = strtolower($paths[$index]);
//        $location=null;
//        
//        switch ($topLevel) {
//            case "session";
//                $topLevelObj = $this->sessionManager;
//                break;
//            case "get";
//                $topLevelObj = $this->activeSession;
//                $location = "GET";
//                break;
//            case "post";
//                $topLevelObj = $this->activeSession;
//                $location = "POST";
//                break;
//            case "user";
//                $topLevelObj = $this->userManager;
//                break;
//        }
//
//        if ($topLevelObj) {
//
//            if ($location) {
//                $value = $topLevelObj->getVar($location, $paths[$index+1]);
//            } else {
//                $value = $topLevelObj->getProperty($paths[$index+1]);
//            }
//            //if (isset($topLevelObj->{$paths[2]})) {
//            //    $value = $topLevelObj->{$paths[2]};
//            //}
//        }
//
//        return $value;
//    }
 
    public function addError($err) {
        $this->errors[] = $err;
    }
    
    public function addDebugInfo($strbld) {
        global $app;
        
         $strbld->addLine("<p>sessionId : {$this->sessionManager->sessionObj->sessionId}<br/>");
        
        if ($this->sessionManager->sessionObj->loggedIn) {
            $strbld->addLine("Logged In as <b>{$this->sessionManager->sessionObj->user}</b><br/>");
        } else {
            $strbld->addLine("Not Logged In default to anonymous user<br/>");
        }
        $acu = $this->acManager->userObj;   
        $strbld->addLine("Role(s):");                
        foreach($acu->roles as $key=>$aco) {
            $strbld->addLine("{$key}");            
        }
        $strbld->addLine("<br/>");

        $strbld->addLine("Laboratories:");
        foreach($acu->laboratories as $lab) {
            $strbld->addLine("{$lab->laboratoryId}");            
        }
        $strbld->addLine("");

        $strbld->addLine("<br/>");
        
//        $strbld->addLine("Facilities:");
//        if (count($acu->facilities)==0) {        
//            $strbld->addLine("None");            
//        } else {
//            foreach($acu->facilities as $facility) {
//                $strbld->addLine("{$facility->facilityId}");            
//            }
//        }
        $strbld->addLine("");
        
//        $strbld->addLine( "<br/>");
//        foreach($acu->acof as $key=>$aco) {
//            $strbld->addLine( "\t<b>{$key}</b>:");
//            foreach($aco as $acf=>$value) {
//                $strbld->addLine("{$acf} ");
//            }
//            $strbld->addLine( "<br/>");
//        }
            
        $strbld->addLine("<br/>Page Id : {$this->request->pageId}<br/>"); 
        $strbld->addLine("Resource Id : {$this->request->resourceId}<br/>");         
        $strbld->addLine("Effective Role : {$app->acManager->currentPageRole}<br/>"); 
        $strbld->addLine("Database : {$this->site->Connection->schema}<br/>"); 
                        
        if ($this->accessError==true) {
            $strbld->addLine("<font color=\"red\"><strong>{$this->accessErrorMessage}</strong></font><br/>");    
        }
        
        foreach($this->errors as $err) {
            $strbld->addLine($err."<br/>");    
        }
        $strbld->addLine("</p>"); 
    }
}
?>