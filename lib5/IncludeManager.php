<?php
/**
 * Description of IncludeManager
 *
 * @author tony
 */
class IncludeManager {

    private $name;
    private $cssLinks;
    private $jsLinks;
    
    public function __construct($name) {
        $this->name = $name;
        $this->cssLinks = array();
        $this->jsLinks = array();
    }
    
    public function addCSSLink($include) {        
        $key = array_search($include, $this->cssLinks);
        if ($key == false) {
            $this->cssLinks[] = $include;
        }
    }

    public function addJSLink($include) {        
        $key = array_search($include, $this->jsLinks);
        if ($key == false) {
            $this->jsLinks[] = $include;
        }
    }
    
    public function render($strbld) {
        $strbld->addLine("<!-- {$this->name} IncludeManager -->");
        
        foreach($this->cssLinks as $include) {
            $strbld->addline("<link rel=\"stylesheet\" type=\"text/css\" href=\"{$include}\"/>");
        }

        foreach($this->jsLinks as $include) {
            $strbld->addline("<script type=\"text/javascript\" src=\"{$include}\"></script>");
        }        
    }

}
?>
