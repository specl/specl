<?php

class SQLWhereString extends SQLWhereClause{
    public function __construct($whereStatement){
        $this->whereStatement=$whereStatement;
    }
    
    public function generateSQLClause(){
        return $this->whereStatement;
    }
}
?>
