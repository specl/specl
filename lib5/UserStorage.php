<?php

class UserStorage {
    
    public function __construct($user){
        $this->user=$user;
        $this->DAL=NewDALObj('UserKV');
    }
    
    public function store($key,$value){
        
        $value=serialize($value);
        $keys=array("user"=>$this->user,"k"=>$key);
        
        $oldUserKVObj=$this->DAL->getOneByPk($keys);
        
        if(is_null($oldUserKVObj)){
            $UserKVObj=NewSpecObj('UserKV');
            $UserKVObj->user=$this->user;
            $UserKVObj->dateCreated="now()";
            $UserKVObj->k=$key;
            $UserKVObj->v=$value;
            $this->DAL->insertOne($UserKVObj);
        }
        else{
            $newUserKVObj=clone $oldUserKVObj;
            $newUserKVObj->k=$key;
            $newUserKVObj->v=$value;
            $newUserKVObj->dateCreated="now()";
            $this->DAL->updateOneByPk($oldUserKVObj,$newUserKVObj);
        }
    }
    
    public function retrieve($key,$throwException=true){
        $keys=array("user"=>$this->user,"k"=>$key);
        $UserKVObj=$this->DAL->getOneByPk($keys);
        
        if (is_null($UserKVObj)==True){
            if($throwException){
                throw new Exception("The key '$key' cannot be found for the user '{$this->user}'");
            }
            else{
                return null;
            }
            
        }else{
            $value=unserialize($UserKVObj->v);
            return $value;
        }  
    }
    
}

?>