<?php
//require_once("../speclib2/autoload.php");
global $classPaths;
$classPaths[]=$zonesBase;

$classPaths[]=$zonesBase."../SchemaGen/";

$classPaths[]=$zonesBase."../UtilLib/";

$classPaths[]=$zonesBase."../SpeclLib/";
$classPaths[]=$zonesBase."../SpeclLib/Conditions/";
$classPaths[]=$zonesBase."../SpeclLib/Constraints/";
$classPaths[]=$zonesBase."../SpeclLib/TableSchema/";
$classPaths[]=$zonesBase."../SpeclLib/Transform/";
$classPaths[]=$zonesBase."../SpeclLib/SpecDefs/";
$classPaths[]=$zonesBase."../SpeclLib/SpecDefs/Conditions/";
$classPaths[]=$zonesBase."../SpeclLib/CompositeObjDef/";
//$classPaths[]=$zonesBase."../SpecLib2/";

$classPaths[]=$zonesBase."ZonesSpecs/";
$classPaths[]=$zonesBase."ZonesSpecs/Controllers/";
$classPaths[]=$zonesBase."ZonesSpecs/Elements/";
$classPaths[]=$zonesBase."ZonesSpecs/Workflow/";
//$classPaths[]=$zonesBase."AppSpecs/";
//$classPaths[]=$zonesBase."Conditions/";
//$classPaths[]=$zonesBase."Constraints/";
$classPaths[]=$zonesBase."Components/";
//$classPaths[]=$zonesBase."ComponentControllers/";
//$classPaths[]=$zonesBase."ComponentViewers/";
//$classPaths[]=$zonesBase."FormElements/";
//$classPaths[]=$zonesBase."PageControllers/";
//$classPaths[]=$zonesBase."PageViewers/";
//$classPaths[]=$zonesBase."querySpecs/";
$classPaths[]=$zonesBase."InputExForms/";
$classPaths[]=$zonesBase."DataSourceComponents/";
$classPaths[]=$zonesBase."SpecTransforms/";
//$classPaths[]=$zonesBase."MySQLCodeGen/";
$classPaths[]=$zonesBase."../SpeclLib/AutoGenClasses/";
$classPaths[]=$zonesBase."SQLModelsV2/";


function NewSpecObj($objDefId){
    
    // this is a coding error and needs to be fixed
    if($objDefId===null){
        throw new Exception("Null value passed into NewSpecObj function");
    }
    
    $clsName = $objDefId . "Cls";
    
    if (autoloadSearch($clsName)) {
        $obj = new $clsName();
    } else {    
        $obj = new SpecCls($objDefId);
    }
    
    return $obj;
}

function NewDALObj($objDefId) {
    
    // this is a coding error and needs to be fixed
    if($objDefId===null){
        throw new Exception("Null value passed into NewDALObj function");
    }
    
    $clsName = $objDefId . "DAL";
    
    if (autoloadSearch($clsName)) {
        $obj = new $clsName();
    } else {    
        $obj = new BaseDAL(array("objDefId"=>$objDefId));
    }
    
    return $obj;
}


function autoloadAddPath($path, $location=ADDTOP) {
    global $classPaths;
    
    $path = trim($path);
    
    $l = strlen($path)-1;
        
    if (($path[$l]!="\\")&&($path[$l]!='/')) {
        $path .=  DIRECTORY_SEPARATOR;
    } 
    
    //if (preg_match('/' . preg_quote(DIRECTORY_SEPARATOR) . '$/', $path)==false) {
    //    $path .=  DIRECTORY_SEPARATOR;
    //} 
    
    $tmp = array();
    $tmp[]=$path;
    if ($location==ADDTOP) {
        $classPaths = array_merge($tmp,$classPaths);        
    } else {
        $classPaths = array_merge($classPaths,$tmp);        
    }
} 

function autoloadSearch($className) {

    global $classPaths;
    
    foreach($classPaths as $path)
    {
    	$filename = $path .$className. '.php';
        if (file_exists($filename))
        {
            return true;
        }
    }
    return false;
}

function zonesAutoload($className)
{
    global $classPaths;
    global $log;
        
    foreach($classPaths as $path)
    {    
    	$filename = $path .$className. '.php';
        if (file_exists($filename))
        {
            include_once($filename);
            return;
        }
    }
}

spl_autoload_register('zonesAutoload');
?>