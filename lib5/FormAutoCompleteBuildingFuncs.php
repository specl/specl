<?php
namespace FormAutoCompleteBuildingFuncs;

/**
 * This file contains the functions for adding autocomplete popups to InputExForms
 */

/**
 * This function adds a button above each autocomplete field that brings up a popup grid.
 * 
 * It does the following:
        - Find all of the AutoComplete form fields
        - Add a button above each field:
            - use the field label as the text of the button
            - each button as a callback associated with it based on the field propDef name,
            - which brings up the popup.
        - Get the objDef for the form panel based on the constraint of the propDef
        - apply the page transforms to the objDef which are the security and property transforms,
          if the transformSpec callback is defined on the page.
        - Define the filters for the objDef:
            - initialize the whereStatement using the whereStatementBuilder, which builds a SQLWhere
            object based on the page parameters.
            - get the filters defined in the objDef for the constraint and add it to the
            popup objDef, after performing global variable substitution. This filters are
            defined in the whereClause variable. They come from the XML specs.            
            - if the objDef has any custom whereStatements that did not come from the objDef.
            These variables come form the whereStatement variable. They most likely come from
            the security transforms.
            - if the panel objDef has a hideOnAdd property add a hideOnAdd constraint, to
              hide all of the variables where hideOnAdd is true.
        - Use the caption of the propDef, if it exists, for the name of the select button in the popup,
          if the caption does not exist use the propDef name.
        - build a panel based on the panel's objDef name.
        - add an autoincrement number to the panel name to insure that the panel names are
          all unique even if multiple panels have the same objDef.
        - by default hide the addedOn,addedBy and hideOnAdd columns.
        - hide all of the columns defined in hideInPopUp
        - build a datagrid select popup component
        - store the grid as a class variable of the page based on the propDef name of the field.
        - add the datagrid popup component to the page.
 * parameters:
 *    pageObj: The page object you wish to add the popups components to. The popup components are always added to
 *    the toplevel page object
 *    formObj: The form object containing the autocomplete fields you wish to add popups for. The button fields will
 *    be added to this form. The formObj does not have to be in the toplevel pageObj. This is used to add popups to the
 *    editForm in the fileupload component.
 * 
 *  building autocomplete popups requires a mode to be set in the page and to include the javascript 
 *  for the button fields
 */
function buildAutoCompleteSelectPopups($pageObj,$formObj){
    global $app;

    $whereStatementBuilder=new \WhereStatementBuilder();
    $whereStatementBuilder->processPageParameters($pageObj->page);

    // must iterate through the form obj, because a field with a constraint can either be an autocomplete field
    // or a dropdown field. We only want to add select popups over the autocomplete fields that are visible.
    foreach($formObj->fields as $field){
        if(is_a($field,"InputExAutoCompleteField")){

            // build a button and add it before the autocomplete field
            $constraint=$field->propDef->getConstraint();
            $buttonParams=array(
                "id"=>"select{$field->propDef->name}Button",
                "buttonText"=>"Select {$field->label}",
                "callbackFunction"=>"{$field->propDef->name}_AutoCompleteCallback",
                "className"=>"AutoCompletePopupButton"
            );
            $fld=new \InputExButtonField($buttonParams);
            $formObj->addFieldBefore("{$field->propDef->name}",$fld);

            // get the panel objDef from the constraint.
            $panelObjDef=$app->specManager->findDef($constraint->tableName);
            // not all pages have a transformSpec callback
            if($pageObj->hasCallback("transformSpec")){                
                $pageObj->fireCallback("transformSpec", $panelObjDef);
            }

            // build wherestatement based on buildParameters
            $whereStatementBuilder->processParameters(array("objDef"=>$panelObjDef));
            $whereStatement=$whereStatementBuilder->buildWhereStatement();                                

            // if the constraint has a filter add it to the whereStatement
            if(!is_null($constraint->whereClause)){
                $SQL=$app->fillInGlobals($constraint->whereClause);
                $whereStatement->addClause(new \SQLWhereString($SQL));
            }
            else if(isset($constraint->whereStatement)){
                foreach($constraint->whereStatement->clauses as $whereClause){
                    $whereStatement->addClause($whereClause);
                }
            }

            // always constrain the values shown by hideOnAdd, just do not raise a validation error for the old values
            if($field->propDef->hasConstraint()){
                $constraint=$field->propDef->getConstraint();
                $objDef=$app->specManager->findDef($constraint->tableName);
                if($objDef->isProperty("hideOnAdd")){
                    $whereClause=new \SQLComparison("{$constraint->tableName}.hideOnAdd","=","0");
                    $whereStatement->addClause($whereClause);
                }
            }

            if($field->propDef->hasAttribute("caption"))
            {
                $displayName = $field->propDef->getAttribute("caption"); 
            } else {
                $displayName = $field->propDef->name;
            }        

            // if multiple autocomplete fields refer to the same propDef, then
            // a we must make sure a unique component id exists by adding a number
            // to the end of the name.   
            $panelNameBase="{$panelObjDef->name}_panel";
            $panelName=$panelNameBase;
            $number=0;
            while ($pageObj->hasComponent($panelName)){
                $number+=1;
                $panelName=$panelNameBase.$number;
            }
            
            $gridPanelName="{$field->propDef->name}AutoCompletePopupPanel";
            $panelParams=array(
                "id"=>"$panelName",
                "parentObj"=>$pageObj,
                "pageObj"=>$pageObj,
                "objDef"=>$panelObjDef,
                "title"=>"Select {$field->label}",
                "selectButtonText"=>"Select {$displayName}",                            
                "formId"=>$formObj->id,
                "fieldName"=>$field->propDef->name,
                "mode"=>$pageObj->mode,
                "whereStatement"=>$whereStatement,
                "selectOneMode"=>true,
                "enableClearAll"=>false,
                "initialLoad"=>false,        
                "onSelectCallback"=>"{$field->propDef->name}_autocomplete_select",
                "initialSortColumn"=>$constraint->displayColumnName
            );


            $pageObj->{$gridPanelName}= new \DataGridSelectPopupComponent($panelParams);
            $pageObj->addComponent("Content",$pageObj->{$gridPanelName});    

        }
        else{
            continue;
        }
    }        
}

/**
 * This function adds the javascript callbacks for the button fields that show the autocomplete popup
 * and the callbacks for the select button in the autocomplete popup. The javascript is added to the
 * toplevel page object.
 * 
 * This function specifically does the following:
 *      - Find all of the AutoComplete form fields
        - for each autocomplete field button add a callback the loads the grid:
            - make sure to load the data only once when a popup is clicked
            - show the popup in the center of the screen
        - generate a map between the propDef names and grid names.
        - for each popup generate a select button callback:
            - get the select row from the grid
            - get the grid key of the display column of the constraint.
            - get the data for the autocomplete field from the grid key
            - get the field name associated with the autocomplete, which is different,
              from the fieldName property of the gridPanel.
            - set the form value
            - hide the popup
 *    parameters:
 *    pageObj: The page object you wish to add the popups components to. The popup components are always added to
 *    the toplevel page object
 *    formObj: The form object containing the autocomplete fields you wish to add popups for. The button fields will
 *    be added to this form. The formObj does not have to be in the toplevel pageObj. This is used to add popups to the
 *    editForm in the fileupload component.
 * 
 *    If called in a component, this function must be called before the component is rendered, because jsInlineHead is rendered
 *    before the component is rendered, so this function must be called in the build phase.
 */
function renderAutoCompleteSelectCallbacks($pageObj,$formObj){
    foreach($formObj->fields as $field){
            if(is_a($field,"InputExAutoCompleteField")){   
                $gridPanelName="{$field->propDef->name}AutoCompletePopupPanel";                 
                // generate callback for button                 
                $pageObj->jsInlineHead->addJS(
    "
    var {$field->propDef->name}_AutoCompleteCallback = function(e){    
        // Call the initial dataload in the data has not already be loaded
        if(!{$pageObj->{$gridPanelName}->grid->id}.dataLoaded===true){
            {$pageObj->{$gridPanelName}->grid->id}.load();
            {$pageObj->{$gridPanelName}->grid->id}.dataLoaded=true;
        }
        {$pageObj->{$gridPanelName}->grid->id}.render();
        {$pageObj->{$gridPanelName}->id}.center();
        {$pageObj->{$gridPanelName}->id}.show();
        {$pageObj->{$gridPanelName}->id}.center();
    };
    ");
            // generate callback for the select button on the panel
            $constraint=$field->propDef->getConstraint();

            // need propmap to map from integer keys on the grid in javascript to propDefNames;
            $propMap=$pageObj->{$gridPanelName}->grid->getPropNameToKeyMap();
            $jsonPropMap=json_encode($propMap);

            $pageObj->jsInlineHead->addJS(
    "
    var {$field->propDef->name}_autocomplete_select = function(e){
        var record={$pageObj->{$gridPanelName}->grid->id}.getSelectedRows();
        var propNameToKey={$jsonPropMap};
        var columnKey=propNameToKey['{$constraint->displayColumnName}'];
        var selectedData=record[columnKey];
        var field={$pageObj->{$gridPanelName}->formId}.getFieldByName('{$pageObj->{$gridPanelName}->fieldName}');
        field.setValue(selectedData);
        {$pageObj->{$gridPanelName}->id}.hide();
    }
    ");
            }
            else{
                continue;
            }
        }
}

    
    

?>
