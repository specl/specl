<?php
/**
 * Description of Request
 *
 * @author Tony
 */
class Request {    
    
    public $menuItemId;
    public $resourceId;
    public $requestType; // page or webservice
    public $controllerId;
    public $actionName;
    public $mode;
    public $parameters; //KeyValuePairs
    public $page;
    public $pageId;
    public $originalPageId;
    
    public function  __construct() {        
        $this->menuItemId = null;
        $this->resourceId = null;
        $this->actionName=null;
        $this->contorllerId=null;
        $this->mode="any";
        $this->parameters = array();
        $this->page = Null;         
        $this->pageId = Null;
        $this->originalPageId  = Null;
    }
    
    public function build($kv) {
        global $app;
        
        foreach($kv as $key=>$value) {
            
            if (is_array($value)) {
                // which one are we going to use?  the first of the last?                
                
                $value = $value[0]; // user first one
            }          
                        
            if (is_string($value)) {
                $value = trim($value);
            }
            
            if (($key=="resource") || ($key=="resourceId")) {
                $this->resourceId = $value; 
            } else if ($key=="menuItemId") {
                $this->menuItemId = $value;
            } else if ($key=="controllerId") {
                $this->controllerId = $value;
            } else if (($key=="actionName") || ($key=="action")) {
                $this->actionName = $value;
            } else if ($key=="mode") {
                $this->mode=$value;
            } else if ($key=="requestType"){
                $this->requestType=$value;
            } else if ($key=="pageId"){
                $this->pageId=$value;
                if(isset($app->pagesManager->pages[$value])){
                    $page = $app->pagesManager->pages[$value];
                    $this->resourceId=$page->resourceId;
                }  
            } else if($key=="effectiveRole"){
                $this->effectiveRole=$value;
            }
            else {
                $this->addParameter($key, $value);
            }
        }
        
        //$this->buildFromGet();
        //$this->buildFromPost();
    }
        
    public function buildFromGet() {
        $keys = $this->app->activeSession->getAllKeys("GET");
        
        foreach($keys as $key) {
            $value =  $app->activeSession->getVar("GET", $key);
            //$key = strtolower($key);
            if (($key=="resource") || ($key=="resourceId")) {
                $this->resourceId = $value; 
            } else if ($key=="menuItemId") {
                $this->menuItemId = $value;
            } else if ($key=="controllerId") {
                $this->controllerId = $value;
            } else if (($key=="actionName") || ($key=="action")) {
                $this->actionName = $value;
            } else if ($key=="mode") {
                $this->mode=$value;
            }else {
                $this->addParameter($key, $value);
            }
        }
    }
    
    public function buildFromPOST() {
        global $app;
        
        $keys = $app->activeSession->getAllKeys("POST");        
        foreach($keys as $key) {
            $value =  $app->activeSession->getVar("POST", $key);
            $this->addParameter($key, $value);
        }
    }
            
    public function addParameter($key, $value) {
        if (!array_key_exists($key, $this->parameters)) {
            $this->parameters[$key] = $value;
        }
    }

    public function clearParameters() {
        $this->parameters = array();
    }
    
    public function getParameter($key){
        if($this->hasParameter($key)){
            return $this->parameters[$key];
        }
        else{
            throw new Exception("The parameter:{$key} does not exist in the request");
        }
    }
    
    public function getParameterValue($key,$default=null){
        if($this->hasParameter($key)){
            return $this->parameters[$key];
        }
        else{
            return $default;
        }
    }
    
    public function hasParameter($key){
        if(array_key_exists($key, $this->parameters)){
            return true;
        }
        else{
            return false;
        }
        
    }
}
?>
