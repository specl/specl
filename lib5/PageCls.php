<?php
/**
 * Description of Page
 *
 * @author Tony
 */
class PageCls extends Component {

    public $id;
    public $title;
    public $path; // path seperated by forward slashes

    public $parent;    
    public $pathArray; // array version of the path

    public $children;
    public $showWhenLoggedIn;
    public $style;
    
    public $allowAccess;
    public $showInNav;
    public $allowDirectAccess;

    public $showInState;

    public $isLoaded;

    public $zones; // list of top level zones on this page.
                   // Zones are first added from the base page of the selected
                   // template.  The template is a site wide variable
                   // a page may have its own Zones.
                   // Controllers assigned to Zones on the base page are
                   // "merged" with the controllers defined by the page.
                   // The new merged list is then flattened and all
                   // controllers are added to the $controllers array.
    public $roles; // list of roles required to access this page
    public $parameters; // The list of parameters for this page.

    public $controllers; // The final list of all controllers on this page.

    public $showInNavWhen;

    public $pageController; // the name of the controller class
    public $pageControllerObj;
    
    public $pageActions; // the name of the actions class
    public $pageActionsObj;

    public $includeDojo;
    
    public $resourceId;
    public $controllerId;
    public $actionName;    
    public $aco;
    public $acu;
    public $acr;
    public $acf;
    public $act;
    
    public $showInSessionState;    
        

    public function  __construct($id) {
        $this->id = $id;
        
        parent::__construct($id);
        
        
        $this->title = null;
        $this->parent = null;
        $this->path = null;
        $this->children = null;
        $this->showWhenLoggedIn = True;
        $this->showInNav = True;
        $this->allowDirectAccess = True;
        $this->zones = array();
        $this->roles = array();
        $this->parameters = array();
        $this->isLoaded = false;
        $this->style = null;
        $this->showInNavWhen = array();
        $this->controllers = array();      
        $this->pageController = null;
        $this->pageControllerObj = null;        
        $this->pageActions = null;
        $this->pageActionsObj = null;
        $this->allowAccess = True;        
        $this->includeDojo=false;
        $this->includeSmartClient=false;
        
        $this->aco = null;
        $this->acu = null;
        $this->acr = null;
        $this->acf = null;
        $this->act = null;
     
        $this->showInSessionState = null;        
    
    }

    public function addZone($zone)
	{
	   	if (array_key_exists($zone->id,$this->zones)==False)
	   	{
			$this->zones[$zone->id] = $zone;
	   	}
	}

    public function addController($path, $controllerObj)
	{
		// default zone
		// add to default zone, true/false
		// adding rules
		// merge rules
		// replace rules

		if (array_key_exists($path, $this->zones))
		{
			$zone = $this->zones[$path];
			$zone->addController($controllerObj);
            $controllerObj->pageObj = $this;
            // check for for a duplicate?
			$this->controllers[$controllerObj->id] = $controllerObj;
		}
	}

    public function getController($id)
	{
		if (array_key_exists($id, $this->controllers))
		{
			return $this->controllers[$id];
		} else {
            return null;
        }
	}

    public function setId($value) {
        $this->id = sanitize($value, PARANOID);

        if (is_null($this->title)) {
            $this->title = $this->id;
        }
    }

    public function setPath($path) {
        global $log;
        $errors=array();

        // set path, root, parent and pathArray

        // 	strip any white space
        $path = trim($path);

        // strip leading and trailing \ if any
        if ($path[0] == "\\")
        {
            $path = substr($path, 1);
        }

        if ($path[strlen($path)-1] == "\\")
        {
            $path = substr($path, 0, strlen($path)-1);
        }

        $pathArray = array();

        $parts = explode("\\" , $path);

        if (count($parts)==1)
        {
            $pageId = $parts[0];
            $pageParent = "\\";
            $pageRoot = "\\";
        }
        else
        {
            $pageId = $parts[count($parts)-1];
            $pageParent = $parts[0];
            $pageRoot = $parts[0];
        }

        foreach($parts as $part) {
            $id = sanitize($part, PARANOID);

            if (array_key_exists($id,$pathArray))
            {
                // error duplicate page id in path
                $this->errors[] = "Error duplicate page id \"$id\" in path<br/>";
                return Null;
            } else {
                $pathArray[]=$id;
            }
        }

        $this->path = $path;
		$this->root = $pageRoot;
		$this->parent = $pageParent;
		$this->pathArray = $pathArray;

    }

    public function isVisible() {
        global $app;

        if ($this->showInNav==false)
            return false;

        if ($this->allowDirectAccess==false)
            return false;

        if (count($this->showInNavWhen)==0) {
            return true;
        }

        foreach($this->showInNavWhen as $condition) {

            if (is_a($condition, "BooleanVariableCondition")) {
                $value = $app->getValue($condition->key);
                if ($condition->value != $value) {
                    return false;
                }
            } else if (is_a($condition, "LookupVariableCondition")) {
                $lookupArray = $app->getValue($condition->key);
                if ($lookupArray) {
                    if (array_key_exists($condition->value,$lookupArray)==false) {
                        return false;
                    }
                } else {
                    return false;
                }
            } else if (is_a($condition, "StringVariableCondition")) {
                $value = $app->getValue($condition->key);

                if ($condition->value != $value) {
                    return false;
                }
            }
        }
        return true;
    }

	public function getControllersForZone($zoneId)
	{
		if (array_key_exists($zoneId, $this->zones))
		{
			return $this->zones[$zoneId]->controllers;
		}
		else
		{
			return array();
		}
	}

    public function mergeZones()
	{
		$this->addPageControllers($this->pageObj);
	}

	public function addPageControllers($pageObj)
	{
		if (isset($pageObj->zones))
		{
			foreach($pageObj->zones as $zoneObj)
			{
	            $this->addZone($zoneObj);

				//$this->zones[$zoneObj->id] = $zoneObj;

	            foreach($zoneObj->controllers as $controllerId=>$class)
	            {
	                // check if ZoneId actually is on this page????
	                //$controllerObj = new $class($controllerId);
	                //$this->addController($zoneObj->id, $controllerObj);

	                //$controllerObj = new $class($controllerId);
	                $this->addController($zoneObj->id, $class);

	                //$this->controllerObj->items[] = $controllerObj;

	                //$controllerObj->load();
	                //if (array_key_exists($controllerId, $this->zones))
	                //{
	                //$this->addController($zoneObj->id, $controllerObj);
	                //}

	                // Add Zones from page? the add them?  Where will they render???
	                //$this->controllerObj->slotNames[$zoneId][] = $controllerObj;
	            }
			}
		}
	}
    
    public function getACR(){
        global $app;
        if(is_null($this->acr)){
            $parents=explode("/", $this->path);
            array_pop($parents);
            $parents=array_reverse($parents);
            foreach($parents as $parentName){
                if(isset($app->pagesManager->pages[$parentName])){
                    $parentPage=$app->pagesManager->pages[$parentName];
                    if(!is_null($parentPage->acr)){
                        return $parentPage->acr;
                    }
                }
            }
            return null;
        }
        else{
            return $this->acr;
        }
    }
 
    
    // @param $key
    // @param $default
    // 
    // Looks for the the key in the parameterValues array and if it exists, 
    // the value of the key is returned, otherwise either null or a default 
    // value are returned.
    public function getParam($key, $default=null) {
        if(isset($this->parameterValues[$key])){
            return $this->parameterValues[$key]->value;
        }
        else{
            return $default;
        }            
    }
    
    public function setParam($key,$value){
        $valueObj= NewSpecObj('ParameterValue');
        $valueObj->key=$key;
        $valueObj->value=$value;
        $this->parameterValues[$key]=$valueObj;
    }
    
    public function getAllParameters(){
        return $this->parameterValues;
    }
    
    public function getAllParameterValues(){
        $paramValues=array();
        foreach($this->getAllParameters() as $parameter){
            $paramValues[$parameter->key]=$parameter->value;
        }
        return $paramValues;
    }
            
}
?>
