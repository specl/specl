<?php

class ComponentController {

    public $id;
    public $controllerClass;
    public $wasGeneric;

    public $theModel;
    public $theView;	
	public $pageObj;

    public $zoneObj;

    public $childrenControllers; // an array of children Controllers
    public $parentController; // the parent Controller is a child.
                              // non-null if a child

    public $rendered;  // true/false don't render if already rendered
    public $processed; // true/false  don't process if already processed

    public $visible; // true/false don;t render is false

    public $actions; // array of actions that this controller will accept
    public $parameters; // array of parameters

    public function __construct($params) {

        if(is_string($params)) {
            $this->id=$params;
        } else {
            if (isset($params["id"])) {
                $this->id = $params["id"];
            } else {
                $this->id = null;
            }
            
            if (isset($params["pageObj"])) {
                $this->pageObj = $params["pageObj"];
            } else {
                $this->pageObj = null;
            }                        
        }

        $this->controllerClass = Null;
        $this->theModel = Null;
        $this->theView = Null;
        $this->wasGeneric = True;
        $this->zoneObj= null;        
        $this->childrenControllers = array();
        $this->parentController = null;

        $this->visible = true;

        $this->actions = array(); // ControllerAction
        $this->parameters = array();

    }

    public function addAction($action) {
        // should this be lowercased?

        if (!array_key_exists($action->id, $this->actions)) {
            $this->actions[$action->id] = $action;
        }
    }
    
    public function addChildController($controllerObj) {

        $controllerObj->pageObj = $this->pageObj;
        $controllerObj->parent = $this;
        $this->childrenControllers[] = $controllerObj;        
        $this->pageObj->addController($controllerObj->parent->zoneObj->id, $controllerObj);        
    }


    public function recover() {    	    	
    }
        
    public function process($request) {
        if ($this->theModel) {
            $this->theModel->prepare($request);
        }
    }

    public function render($strbld=null) {
        if ($this->shouldRender()) {
            if ($this->theView != Null) {
                $this->theView->render($strbld);
            }
            $this->rendered = true;
        }
    }

    public function shouldRender() {
         if (($this->rendered==false)) {
             return true;
         } else {
             return false;
         }
    }

    public function displayHeader() {
        if ($this->theView != Null) {
            $this->theView->displayHeader();
        }
    }

    public function setPage($pageObj) {
        $this->pageObj = $pageObj;
    }

    public function setId($id) {
        $this->id = $id;
    }
}
?>