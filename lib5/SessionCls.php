<?php

class SessionCls extends SpecCls {
    
    public $sessionId;
    public $dateCreated;
    public $loggedIn;
    public $username;
    public $active;
    public $lastActiveOn;
    public $ipAddress;
    public $domain;
    public $client;
    public $requestCount;

    public function  __construct() {
        parent::__construct('Session');
    } 
    
    public function create($sessionId, $ipAddress, $userAgent) {
        
        parent::__construct('Session');
        
        $this->sessionId = $sessionId; 
        $this->loggedIn = 0; 
        $this->active = 1; 
        $this->dateCreated = getCurrentDateTime(); 
        $this->lastActiveOn = getCurrentDateTime();
        $this->user = null; 
        $this->ipAddress = $ipAddress;  
        $this->domain = null; // could do a reverse DNS lookup now.
        $this->userAgent = $userAgent; 
        $this->requestCount = 0; 
    }    
    
    public function init($params) {

        if (isset($params["sessionId"])) {
            $this->sessionId = $params["sessionId"];
        } else {
            $this->sessionId = null;
        }

        $this->dateCreated = getCurrentDateTime();
        $this->loggedIn = false;
        $this->username = null;
        $this->active = true;
        $this->lastActiveOn = $this->dateCreated;

        if (isset($params["ipAddress"])) {
            $this->ipAddress = $params["ipAddress"];
        } else {
            $this->ipAddress = null;
        }

        $this->domain = null;

        if (isset($params["userAgent"])) {
            $this->userAgent = $params["userAgent"];
        } else {
            $this->userAgent = null;
        }
        
        $this->requestCount = 0;
    }
}
?>
