<?php

class Process {
    public function __construct($params=null){
        if(is_null($params)===false){
            $this->processParameters($params);
        }
    }
    
    public function processParameters($params)
    {
        
    }
    
    public function execute()
    {
        throw (new Exception("Function must be implemented in the child class"));
    }
    
}

?>