<?php
/** @ObjDefAnn(objDefId="SQLNotBetween")  
 */
class SQLNotBetween extends SQLWhereClause{
    public function __construct($leftHS,$minStatement,$maxStatement){
        $this->leftHS=$leftHS;
        $this->minStatement=$minStatement;
        $this->maxStatement=$maxStatement;
    }
    
    public function generateSQLClause(){
        return "{$this->leftHS} NOT BETWEEN {$this->minStatement} AND {$this->maxStatement}";
    }
}
?>
