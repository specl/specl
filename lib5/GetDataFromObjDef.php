<?php
/**
 *  This class wraps a function that does the following:
 *      - if the mode is view or edit, get the primary keys from the request and use the primary keys to
 *        get the information from the database.
 *      - if the mode is add, construct an empty object and fill in the default values
 *      - apply spec transforms based on the data and the transforms described in the spec
 *      - apply value transforms based on the transforms described in the spec 
 * 
 *  This is not ideal, spec transforms and data retrieval should really be seperate steps, but it works well enough to be reused for what we need.
 *      
 */
class GetDataFromObjDef extends WorkFlowComponent{
    public $objDef;
    
    public function __construct($params){
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        }
        else{
            throw new Exception("the parameter 'objDef' is requried");
        }
    }
    
    public function execute($input){
        $mode=$input["mode"];
        if($mode!="add"){
            $getKeysComponent=new GetPrimaryKeyValuesFromRequest(array("objDef"=>$this->objDef));        
            $keys=$getKeysComponent->execute($input["request"]);
                                    
            $tableName = $this->objDef->getAttribute("table");                         
            $dal = NewDALObj($tableName);            
            
            $data=null;
            if(count($keys)>0){
                // get object by Primary Key       
                $data = $dal->getOneByPk($keys);
                
                if (!isnull($data)) {
                    $this->objDef=$this->objDef->transformSpec($data,$mode);
                    $this->objDef->transformValues($data);
                    $data->objDef=$this->objDef;                    
                }
            }
            return $data;
        }
        else{            
            $obj = NewSpecObj($this->objDef->name);
            
            $this->objDef->addDefaultValues($obj,$mode);
            $this->objDef=$this->objDef->transformSpec($obj,$mode);
            $obj->objDef=$this->objDef;
            return $obj;
        }
    }
}

?>