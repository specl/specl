<?php
/**
 * Description of IncludeManager
 *
 * @author tony
 */
class InlineCSSManager {
    private $css;
    
    public function __construct() {
        $this->css=array();
    }
    
    public function addCSS($css){
        $this->css[]=$css;
    }
    
    public function render($strbld) {
        foreach($this->css as $css) {
            $strbld->addline("<style>{$css}</style>");
        }
        
    }
        
}
?>
