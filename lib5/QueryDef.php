<?php
/**
 * Description of QueryDef
 *
 * @author Tony
 */
class QueryDef {

    public $selectedProperties; // These are PropDefs
    public $sql;

    public $columnNames;
    public $tableNames;
    public $selectStatement;
    public $startingRow;
    public $numberOfRowsToGet;
    public $DBModel;
    public $totalNumberOfRows;
    public $filters;
    public $sorting;
    public $paging;
    public $sortColumns;
    public $sortDirection;

    public function __construct()
    {
        $this->selectedProperties = array();
        $this->sql=null;
        
        $this->columnNames = array();
        $this->tableNames = array();
        $this->filters = array();
        $this->sorting = array();
        $this->sortColumns = array();
        $this->sortDirection = "ASC";
        $this->startingRow = Null;
        $this->numberOfRowsToGet = Null;
        $this->paging = False;
    }

    public function addColumn($columnName)
    {
        $this->columnNames[] = $columnName;
    }

    public function addTable($tableName)
    {
        $this->tableNames[] = $tableName;
    }

    public function addFilter($columnName, $operator, $value, $conjunction="AND") {
        $filter = array("columnName" => $columnName,
            "operator" => $operator,
            "value" => $value,
            "conjunction" => $conjunction);

        $this->filters[] = $filter;
    }

    public function addSortColumn($columnName)
    {
        $this->sortColumns[] = $columnName;
    }
 }
?>
