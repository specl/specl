<?php
/**
 * Description of PageActions
 *
 * @author Tony
 */
class PageActions
{
    public $pageObj;
	public function __construct($pageObj) {
		$this->pageObj = $pageObj;
	}

	public function onCreate() {
	}

    public function onRecover() {
	}

	public function onProcess() {
	}

	public function onRender() {
	}

	public function onRenderZone($zoneId) {
	}

	public function onEnd()	{
	}
}
?>
