<?php
/**
 * Pages and there parameters are defined in XML files. The PagesManager class
 * is used to manage and load the page parameters from these XML files.
 * 
 */
class PagesManager {

    public $pages;
    public $pageObjs;
    public $specManager;
    private $fm;
    
    public function  __construct($specManager) {
        global $app;
        
        $this->specManager = $specManager;
        $this->pages=array();
        $this->pageObjs=array();
        $this->fm = new FilePathManager();
        
        
        $this->fm->addPath($app->dbPath."pagesDB/Add/");
        $this->fm->addPath($app->dbPath."pagesDB/View/");
        $this->fm->addPath($app->dbPath."pagesDB/ViewOnly/");
        $this->fm->addPath($app->dbPath."pagesDB/BrowseOnly/");
        $this->fm->addPath($app->dbPath."pagesDB/Browse/");
        $this->fm->addPath($app->dbPath."pagesDB/Edit/");
        $this->fm->addPath($app->dbPath."pagesDB/Test/");
        $this->fm->addPath($app->dbPath."pagesDB/Auth/");        
        $this->fm->addPath($app->dbPath."pagesDB/");
        
    }

    public function doesPageExist($pageId) {

        if (array_key_exists($pageId,$this->pages)) {
            return true;
        } else {
            return false;
        }
    }
        
    public function getPage($pageId) {
        
        if (array_key_exists($pageId,$this->pages)) {
            return $this->pages[$pageId];
        } else {
            $pageObj = $this->loadPage($pageId);            
            $this->pages[$pageId] = $pageObj;
            return $pageObj;
        }
    }

    public function loadPage($pageId) {        
        //TODO:Error checking, add try catch block
        $searchName = "{$pageId}.xml";
        
        $filename = $this->fm->getFullPath($searchName);
                
        $pageObj = $this->specManager->getObjectFromXML($filename, "Page");

        foreach($pageObj->zones as $zone) {
            //echo "{$zone->id}<br/>";
            foreach($zone->controllers as $controllerObj) {
                //echo "{$controllerObj->id}<br/>";
                $pageObj->controllers[$controllerObj->id] = $controllerObj;
            }
        }
        //$pageObj->id = $pageId;
        $pageObj->isLoaded = true;

        $this->pages[$pageId] = $pageObj;

        return $pageObj;        
    }
    
    public function loadAllPages(){
        $files=$this->fm->findFilesMatchingPattern("*.xml");
        foreach($files as $file){
            $pageId=basename($file,".xml");
            $this->loadPage($pageId);
        }
    }
    
    public function getPages(){
        return $this->pages;
    }
}
?>
