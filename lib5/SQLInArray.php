<?php
/** @ObjDefAnn(objDefId="SQLInArray")  
 */
class SQLInArray extends SQLWhereClause{
    public function __construct($columnName,$valuesArray,$type="string"){
        $this->columnName=$columnName;
        $this->valuesArray=$valuesArray;
        $this->type=$type;
    }
    
    public function generateSQLClause(){
        
        if (count($this->valuesArray)==0) {            
            $sql = $this->columnName." is NULL ";
            return $sql;            
        } else {
            if($this->type=="string"){
                $newValues=array();
                foreach($this->valuesArray as $value){
                    $newValues[]="'$value'";
                }           
            } else{
                $newValues=$this->valuesArray;
            }
            $sql = $this->columnName." in "."(".implode(",", $newValues).")";
            return $sql;
        }
    }
}
?>
