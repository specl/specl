<?php
/** @ObjDefAnn(objDefId="SQLIsNotNull")  
 */
class SQLIsNotNull extends SQLWhereClause{
    public function __construct($leftHS){
        $this->leftHS=$leftHS;
    }
    
    public function generateSQLClause(){
        return "{$this->leftHS} IS NOT NULL";
    }
}

?>
