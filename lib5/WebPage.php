<?php
/**
 * Description of PageModel
 *
 * @author tony
 */
class WebPage extends Component{
    
    public $id;
    public $title;
    public $description;
    public $metaTags;
    public $cssIncludes;
    public $jsIncludes;
    
    public $cssInlineHead;
    public $jsInlineHead;

    public $cssInlineBody;
    public $jsInlineBody;
        
    public $bodyId;
    public $bodyClass;
    
    public $zones;
    
    public $components;
    
    public $dispatchComponents;
    public $buildPage;
    
    
    public function __construct($id) {     
        parent::__construct($id);
        $this->id = $id;
        $this->metaTags = new MetaTagsManager();       
        $this->includes = new IncludeManager("CSS and JavaScript");
        //$this->jsIncludes = new IncludeManager("JS");
        $this->cssInlineHead = new InlineCSSManager();
        $this->jsInlineHead = new InlineJSManager();
        $this->cssInlineBody = new InlineCSSManager();
        $this->jsInlineBody = new InlineJSManager();
        $this->bodyId = null;
        $this->bodyClass = null;    
        $this->zones = array();        
        $this->title = "untitled";
        
        $this->components = array();
        $this->dispatchComponents = array();
        
        $this->buildPage = false;
    }    
    
    public function build() {  
        
    }
    
    public function process($request) {            
        foreach($this->components as $component) {
            $component->process($request);
        }        
    }       
    
    public function renderToFile($filename) {        
        ob_start();        
        $this->render();
        $str=ob_get_contents();
        ob_end_clean();
        file_put_contents($filename,$str);    
    }
    
    public function render() {
        $strbld = new StringBuilder();
        
        $this->headStart($strbld);                                
        $this->metaTags->render($strbld);
        $this->includes->render($strbld);        
        $this->cssInlineHead->render($strbld);
        $this->jsInlineHead->render($strbld);                                        
        $this->headEnd($strbld);        
        $strbld->decreaseIndentLevel();
        $strbld->increaseIndentLevel();        
        $this->bodyStart($strbld);                        
        $strbld->increaseIndentLevel();        
        $this->bodyRender($strbld);                        
        $this->cssInlineBody->render($strbld);        
        $this->jsInlineBody->render($strbld);        
        $strbld->decreaseIndentLevel();        
        $this->bodyEnd($strbld);
               
        echo $strbld->getString();
    }
        
    
    public function headStart($strbld) {
        global $app;
        
        $strbld->addLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
        $strbld->addLine("<html>");
        $strbld->increaseIndentLevel();
        $strbld->addLine("<head>");
        $strbld->increaseIndentLevel();                
        $strbld->addLine("<title>{$app->site->title}:{$this->title}</title>");        
    }
    
    public function headEnd($strbld) {
        $strbld->decreaseIndentLevel();
        $strbld->addLine("</head>");   
        $strbld->decreaseIndentLevel();        
    }       
    
    public function bodyStart($strbld) {
        $strbld->addLine("<body id=\"{$this->bodyId}\" class=\"{$this->bodyClass}\">");     
    }

    public function bodyRender($strbld) {        
    }
    
    public function bodyEnd($strbld) {
        $strbld->addLines("</body>");
        $strbld->decreaseIndentLevel();
        $strbld->addLines("</html>");
    }        

    public function createZone($zoneId) {
        if (array_key_exists($zoneId,$this->zones)==False)
	   	{
            $zoneObj = new ZoneComponent($zoneId,$this);
			$this->zones[$zoneId] = $zoneObj;
	   	}
        else{
            throw new Exception("{$zoneId} already exists");
        }
    }
    
    public function addZone($zoneObj)
	{
	   	if (array_key_exists($zoneObj->id,$this->zones)==False)
	   	{
			$this->zones[$zoneObj->id] = $zoneObj;
	   	}
        else{
            throw new Exception("Cannot add zone:{$zoneObj->id}, a zone with that id already exists");
        }
        
	}    

    public function getZone($zoneId)
	{
	   	if (array_key_exists($zoneId,$this->zones)==True)
	   	{
			return $this->zones[$zoneId];
	   	} else {
            return null;
        }
	}    
    
    public function renderZones($strbld) {
    }
    
    public function renderZone($strbld, $zoneId) {                
        $zoneObj = $this->getZone($zoneId);        
        if ($zoneObj) {
            $zoneObj->render($strbld);
        } else {
            $strbld->addline("<!-- Zone {$this->id} Missing -->");            
        }
    }
    
    
    public function addPageComponent($componentObj) {
        if (!array_key_exists($componentObj->id, $this->dispatchComponents))
	   	{
             $this->dispatchComponents[$componentObj->id]=$componentObj;
	   	}
    }
    
    
    public function getPageComponent($id) {
        if (array_key_exists($id, $this->dispatchComponents))
	   	{
             return $this->dispatchComponents[$id];
	   	} else {
                    return Null;
                }
    }
    
    public function addComponent($zoneId, $componentObj) {
        
        $this->addPageComponent($componentObj);
        
        parent::addComponent($componentObj);
                
        if (array_key_exists($zoneId,$this->zones)==True)
	   	{
			$zoneObj = $this->zones[$zoneId];
            $zoneObj->addComponent($componentObj);            
	   	}
        else{
            throw new Exception("{$zoneId} does not exist");
        }
    }   
}

?>