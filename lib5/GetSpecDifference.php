<?php

// returns all values in SPEC1 that are not in SPEC2
class GetSpecDifference {
    public function execute($input){
        $spec1=$input["spec1"];
        $spec2=$input["spec2"];
        
        if(is_null($spec1)){
            return array();
        }        
        else if(is_null($spec2)){
            return array_keys($spec1->getProperties());
        }       
        else{
            return array_diff(array_keys($spec1->getProperties()),array_keys($spec2->getProperties()));
        }
    }
}

?>