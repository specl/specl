<?php

class SessionStorage2 {
    
    public function __construct($sessionId){
        $this->sessionId=$sessionId;
        $this->DAL=NewDALObj('SessionKV');
    }
    
    public function store($key,$value){
        
        $value=serialize($value);
        $keys=array("sessionId"=>$this->sessionId,"k"=>$key);
        
        $oldSessionKVObj=$this->DAL->getOneByPk($keys);
        
        if(is_null($oldSessionKVObj)){
            $sessionKVObj=NewSpecObj('SessionKV');
            $sessionKVObj->sessionId=$this->sessionId;
            $sessionKVObj->dateCreated="now()";
            $sessionKVObj->k=$key;
            $sessionKVObj->v=$value;
            $this->DAL->insertOne($sessionKVObj);
        }
        else{
            $newSessionKVObj=clone $oldSessionKVObj;
            $newSessionKVObj->k=$key;
            $newSessionKVObj->v=$value;
            $newSessionKVObj->dateCreated="now()";
            $this->DAL->updateOneByPk($oldSessionKVObj,$newSessionKVObj);
        }
    }
    
    public function retrieve($key,$throwException=true){
        $keys=array("sessionId"=>$this->sessionId,"k"=>$key);
        $sessionKVObj=$this->DAL->getOneByPk($keys);
        
        if (is_null($sessionKVObj)==True){
            if($throwException){
                throw new Exception("The key '$key' cannot be found in session '{$this->sessionId}'");
            }
            else{
                return null;
            }
            
        }else{
            $value=unserialize($sessionKVObj->v);
            return $value;
        }  
    }    
}
?>