<?php

class MergeCompositeData extends WorkFlowComponent {
    public $fieldName;
    public $valueToSpecMap;
    public $dataColumnName;
    
    public function __construct($params){
        if(isset($params["fieldName"])){
            $this->fieldName=$params["fieldName"];
        }
        else{
            throw new Exception("the parameter 'fieldName' is requried");
        }
        
        if(isset($params["valueToSpecMap"])){
            $this->valueToSpecMap=$params["valueToSpecMap"];
        }
        else{
            $this->valueToSpecMap=array();
        }
    }
    
    public function execute($input){
        global $app;
        $obj=$input["data"];
        $objDef=$input["objDef"];
        // handle case where the value is a constraint id
        if(isset($obj[$this->fieldName]) && isset($this->valueToSpecMap[$obj[$this->fieldName]])){
            $compositeSpecName=$this->valueToSpecMap[$obj[$this->fieldName]];
        }
        // handle case where the value is a constraint name
        else if (isset($obj[$this->fieldName]) && in_array($obj[$this->fieldName], array_values($this->valueToSpecMap))){
            $compositeSpecName=$obj[$this->fieldName];
        }
        else{
            $compositeSpecName=null;
        }
        
        if($compositeSpecName!==null){ 
            $compositeSpecObjDef=$app->specManager->findDef($compositeSpecName);
            $primaryKeys=array_values($compositeSpecObjDef->getPrimaryKey());
            $keyValues=array();
            foreach($primaryKeys as $primaryKey){
                $propDef=$compositeSpecObjDef->getProperty($primaryKey);
                $keyValues[$propDef->name]=$obj[$propDef->name];
            }
            
            // loop through the objdef and find a property that is 
            // 1) attribute associationType is hasOne 
            // 2) attribute isComposite is true
            // 3) its objectType equals the $compositeSpecName            
            
            foreach($objDef->getProperties() as $propDef) {                
                if (($propDef->getAttribute("associationType",NULL)=="HasOne") 
                    && ($propDef->getAttribute("isComposite",false)===true) 
                    && ($propDef->objectTypeOrig==$compositeSpecName)){                    
                    $compositeFieldName=$propDef->name;
                }
            }
            
            $dal = NewDALObj($compositeSpecName);
            $compositeData=$dal->getOneByPk($keyValues);
            $obj->{$compositeFieldName}=$compositeData;
            return $obj;
        }
        else{
            return $obj;
        }
        
    }
    
}

?>