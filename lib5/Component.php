<?php
require_once("Action.php");

class Component extends SpecCls {
    
    public $parentObj;
    public $pageObj;
    
    public $id;
    public $actions; // a page could have seperate actions of its own.
                     // but it does not contain information about the actions of
                     // its components.   
    public $components;
    public $defaultAction;
    
    public $callbacks;
    public $attributes;
    public $validParams;
    
    
    // Parameters
    //  id
    //  parentObj
    //  pageObj
    //  defaultAction
    // Actions
    
    public function __construct($params) {
        
        parent::__construct('Component');
        
        $this->actions = array();
        $this->components = array();
        $this->attributes=array();
        $this->validParams=array();
        
        if(is_string($params))
        {                        
            $this->id=$params;
            $this->parentObj=null;
            $this->defaultAction="render";
            $this->addAction("page","render");
        }
        else
        {        
            if(isset($params["id"]))
            {
                $this->id=$params["id"];
            }
            else
            {
                throw new Exception("The id parameter must be specified when building a component");
            }

            if(isset($params["parentObj"]) && !is_null($params["parentObj"]))
            {
                $this->parentObj=$params["parentObj"];
            }
            else
            {
                $this->parentObj=null;
            }
            
            if(isset($params["pageObj"]) && !is_null($params["pageObj"]))
            {
                $this->pageObj=$params["pageObj"];
            }
            else
            {
                $this->pageObj=null;
            }

            if(isset($params["defaultAction"])  && !is_null($params["defaultAction"]))
            {
                $this->defaultAction=$params["defaultAction"];
            }
            else
            {
                $this->defaultAction="render";
            }
            
            if(is_null($this->pageObj) && is_a($this->parentObj,"WebPage")){
                $this->pageObj=$this->parentObj;
            }
            
            $this->addAction("page","render");
            
        }
        $this->callbacks = array();
    
    }
    
    // TODO: add required params and default values;
    public function initializeParams($params){
        foreach($this->validParams as $paramName){
            if(isset($params[$paramName])){
                $this->attributes[$paramName]=$params[$paramName];
            }
        }
    }
    
    public function initialize() {    
        
    }
            
    public function setDefaultAction($actionName) {
        if ($this->hasAction($actionName)) {
            $this->defaultAction = $actionName;
        }
    }

    public function getDefaultAction() {   
        return $this->defaultAction;
    }    
    
        
    public function addAction($actionType,$actionName, $functionName=null) {
        if (!array_key_exists($actionName, $this->actions)) {      
            if (is_null($functionName)) {
                $functionName=$actionName;
            }
            $action = new Action($actionType,$actionName, $functionName, $this->id);            
                        
            $this->actions[$actionName] = $action;        

//            if ($this->parentObj) {
//                $this->parentObj->addActionObj($action);
//            }
        } 
        else{
            throw new Exception("The action{$actionName} already exists for the component {$this->id}");
        }
    }
    
    public function addActionObj($actionObj) {
        if (!array_key_exists($actionObj->actionName, $this->actions)) {      
            $this->actions[$actionObj->actionName] = $actionObj;        
            if ($this->parentObj) {
                $this->parentObj->addActionObj($actionObj);
            }
        }                
    }
    
    public function hasAction($actionName)
    {
        if(array_key_exists($actionName,$this->actions))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getAction($actionName)
    {
        if (empty($actionName)) {
            return $this->getDefaultAction();
        }
        
        if (array_key_exists($actionName,$this->actions))
        {
            return $this->actions[$actionName];
        }
        else
        {
            throw new Exception("The action:{$actionName} is not defined in component '{$this->id}'");
        }
    }
            
    public function addComponent($componentObj) {
                
        if (is_null($componentObj)) {
            throw new Exception("componentObj cannot be null");
        }
        
        // TODO: call genId function;
        if (is_null($componentObj->id)) {
            throw new Exception("componentObj must have an id");
        }
        
        if(!is_a($this,"WebPage") && count($this->actions)>0 && is_null($this->pageObj)){
            throw new Exception("componentObj must have pageObj for actions to work");
        }
        
        if ($this->pageObj) {
            $this->pageObj->addPageComponent($componentObj);
        }
        
        if ($this->hasComponent($componentObj->id)==false) {
            
            $this->components[$componentObj->id] = $componentObj;                        
            $componentObj->parentObj = $this;
            
            foreach($componentObj->actions as $action) {
                $this->addActionObj($action);
            }
            
//            if ($this->parentObj) {
//                $this->parentObj->addComponent($componentObj);
//            }                                 
        }
        else{
            throw new Exception("The id: {$componentObj->id} is already being used by another component");
        }        
    }
       
    public function hasComponent($componentId)
    {
        if(array_key_exists($componentId,$this->components))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getComponent($componentId)
    {
        if(array_key_exists($componentId,$this->components))
        {
            return $this->components[$componentId];
        } else {
            throw new Exception("The subComponent:{$componentId} does not exist in component '{$this->id}'");
        }
    }    
    
    public function validateRequest($request) {        
        return true;        
    }
    
    public function dispatch($request) {
                
        //if (is_null($request->actionName)) {
        //    //$request->actionName = $this->getDefaultAction();
        //    return;
        //}
        
        // If the request has no controllerId then call the
        // top level component action.
        if(is_null($request->controllerId)){
            $action=$this->getAction($request->actionName);
            //$this->{$action->functionName}();          
        }
//        // If a controllerId is define look for a subComponent
//        else{
//            $componentObj=$this->getComponent($request->controllerId);
//            $action=$componentObj->getAction($request->actionName);
//            $componentObj->{$action->functionName}();
//        }
    }           


    public function build($request) {
        foreach($this->components as $component){
            $component->build($request);
        }
    }
    
    public function process($request) {
    }

    public function render($strbld=null) {        
        foreach($this->components as $component) {
            $component->render($strbld);
        }
    }
        
    public function display() {
        echo "{$this->id} display<br/>";
    }
    
    // parameters:
    // the name of the event
    // the object to call
    // the name of the method to call

    public function registerCallback($event, $objToCall, $methodName, $userData=null) {        
        $eventCallback = new EventCallback($event, $objToCall, $methodName, $userData);        
        $this->callbacks[$event][] = $eventCallback;
    }
    
    public function hasCallback($event) {
        if (array_key_exists($event, $this->callbacks)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function fireCallback($event, $data) {
        if (array_key_exists($event, $this->callbacks)) {
            foreach($this->callbacks[$event] as $callback){
                $callback->fire($data);
            }                
        } else {
            throw new Exception("The callback: {$event} does not exist in component '{$this->id}'");
        }
    }
    
    public function setAttribute($name,$value){
        $this->attributes[$name]=$value;
    }
    
    public function getAttribute($name){
        if(isset($this->attributes[$name])){
            return $this->attributes[$name];
        }
        else{
            $className=get_class($this);
            throw new Exception("The {$className} attribute '{$name}' is not defined");
        }
    }
    
    public function hasAttribute($name){
        if(isset($this->attributes[$name])){
            return true;
        }
        else{
            return false;
        }
    }
}
?>