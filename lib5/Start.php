<?php
include("../krumo/class.krumo.php");
include('Utility.php');
include('SQLFuncs.php');
include('Sanitize.php');

$zonesBase = dirname(__FILE__) ."\\";
include_once($zonesBase."AutoLoad.php");

//$catch = new CatchAll();

$log = Logger::factory('Start.php');
$log->info('Start','---------------------------------------------------------------');

$app = new App();
$app->start();
$app->recover();
$app->dispatch();
$app->end();

$log->info('End','---------------------------------------------------------------');
?>
