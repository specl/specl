<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UpdateFormAction
 *
 * @author Tony
 */
class UpdateFormAction {

    public $formUpdates;

    public function  __construct() {
        $this->formUpdates = array();
    }

    public function updateForm($formObj) {
        foreach($this->formUpdates as $formUpdate) {

            if (is_a($formUpdate, "ControlModify")) {
                // get control element
                $ctrl = $formObj->getElementById($formUpdate->id);
                if ($ctrl) {
                    $formUpdate->applyUpdates($ctrl);
                } else {
                    // TODO: log error
                }
            }
        }
    }

}
?>
