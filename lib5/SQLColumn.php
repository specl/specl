<?php
class SQLColumn
{
    public function __construct($columnName, $columnAlias, $tableRef, $tableAlias, $isSQL, $label, $sql=Null)
    {
        $this->columnName=$columnName;
        $this->tableRef=$tableRef;
        $this->tableAlias=$tableAlias;        
        $this->columnAlias=$columnAlias;
        $this->isSQL=$isSQL;
        $this->label=$label;        
        $this->columnKey = Null;
        $this->sql=$sql;
        //$this->schemaRef=$schemaRef;  // for cross schema joins TODO add this when needed.
    }
    
    public function getColumnName() {
        
        if (is_null($this->tableAlias)) {
            $table = $this->tableRef;
        } else {
            $table = $this->tableAlias;
        }

        if (is_null($this->columnAlias)) {
            $column = $this->columnName;
        } else {
            $column = $this->columnAlias;
        }
        
        $name = $table.".".$column;
        
        return $name;        
    }      
        
    public function getColumnNameForSort() {
        
        if (is_null($this->tableAlias)) {
            $table = $this->tableRef;
        } else {
            $table = $this->tableAlias;
        }

        $column = $this->columnName;
        if($table!==null){
            $name = $table.".".$column;
        }
        else{
            $name=$column;
        }
        
        return $name;        
    }      
    
    public function getColumnKey() {
        return $this->columnKey;        
    }       
}    

?>
