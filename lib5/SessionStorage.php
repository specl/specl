<?php
// TODO: Rewrite store and retrieve to access to database directly and get rid of save and recover.

// TODO: Insert objects into the database as seperate individual properties the have paths as their keys, 
//       instead of serializing the object.


require_once("QueryBuilder.php");

class SessionStorage {

    public function __construct($sessionId,$dbConn)
    {
        $this->dbConn=$dbConn;
        $this->sessionId=$sessionId;
        $this->data=array();
        $this->queryBuilder=new QueryBuilder();
    }

    public function store($key,$value)
    {
        $this->data[$key]=$value;
    }

    public function retrieve($key)
    {
        if(array_key_exists($key, $this->data))
        {
            return $this->data[$key];
        }
        else
        {
            return null;
        }

    }

    public function remove($key)
    {
        unset($this->data[$key]);
    }

    public function clear($deleteData=false)
    {
        $deleteStatement="DELETE from SessionVars WHERE SessionId=%sessionId%";
        $this->queryBuilder->setStatement($deleteStatement);
        $this->queryBuilder->createParam("sessionId", "string", $this->sessionId);
        $deleteSQL=$this->queryBuilder->bindParams();
        $this->dbConn->delete($deleteSQL);

//        if ($deleteData===true) {
//            $this->data[$key]=array();
//        }
    }

    public function clearVar($varName)
    {
        $deleteStatement="DELETE from SessionVars WHERE SessionId=%1% and sKey=%2%";
        $this->queryBuilder->setStatement($deleteStatement);
        $this->queryBuilder->createParam("1", "string", $this->sessionId);
        $this->queryBuilder->createParam("2", "string", $varName);
        $deleteSQL=$this->queryBuilder->bindParams();
        $this->dbConn->delete($deleteSQL);

        if (isset($this->data[$varName])) {
            unset($this->data[$varName]);
        }
    }

    public function save()
    {
        //return;
        // clear the old session data from the table and insert data into the new one.
        $this->clear();

        $statement="INSERT INTO SessionVars VALUES(%sessionId%,%key%,%value%);";


        $this->queryBuilder->setStatement($statement);

        $this->queryBuilder->createParam("sessionId", "string", null);
        $this->queryBuilder->createParam("key", "string",  null);
        $this->queryBuilder->createParam("value", "string",  null);


        foreach($this->data as $key=>$value)
        {
            // serialize all variables in the database so any type can be stored in the same column
            // including objects.

            $serializedValue=serialize($value);

            $this->queryBuilder->setParamValue("sessionId", $this->sessionId);
            $this->queryBuilder->setParamValue("key",  $key);
            $this->queryBuilder->setParamValue("value", $serializedValue);

            $InsertSQL=$this->queryBuilder->bindParams();
            
            $this->dbConn->insert($InsertSQL);
        }
    }

    public function recover()
    {
        
        $selectStatement="SELECT sKey,sValue from SessionVars where SessionId=%sessionId%;";
        $this->queryBuilder->setStatement($selectStatement);

        $this->queryBuilder->createParam("sessionId", "string", $this->sessionId);
        $SQL=$this->queryBuilder->bindParams();

        //echo $SQL;

        $result=$this->dbConn->select($SQL);

        while($row=mysql_fetch_assoc($result))
        {
            $value=unserialize($row["sValue"]);
            $this->store($row["sKey"],$value);
        }

    }


}
?>
