<?php
require_once("QueryBuilder.php");
class SelectStatementBuilder extends QueryBuilder
{
    private $columnNames;
    private $tableNames;
    public $columns; // array of SQLColumn
    public $columnsIndex;  // numeric index with pointers the SQLColumn
    public $tables;  // array of SQLTable
    public $tableAliases;  // array of SQLTable    
    public $selectStatement;
    public $startingRow;
    public $numberOfRowsToGet;
    public $DBModel;
    public $totalNumberOfRows;
    public $filters;
    public $sorting;
    public $paging;
    public $sortColumns;
    public $sortDirection;
    public $joins;
    public $whereStatement;
    public $wheres;
    public $distinct;
    public $customCountColumn;
    
    public function __construct()
    {
        parent::__construct();

        $this->columns = array();
        $this->columnsIndex = array();        
        $this->tables = array();
        $this->tableAliases = array();
        $this->filters = array();
        $this->sorting = array();
        $this->sortColumns = array();
        $this->sortDirection = "ASC";
        $this->startingRow = Null;
        $this->numberOfRowsToGet = Null;
        $this->paging = False;
        $this->joins=array();
        $this->whereStatement=new SQLWhere();    
        $this->distinct = false;
        $this->customCountColumn="";
    }
            
    public function addColumns($columnNames, $tableName=null) {
        foreach($columnNames as $columName) {
            $this->addColumn($columName, $tableName, Null, Null);
        }
    }

    public function addColumn($column, $tableName, $columnAlias, $columnDisplay)
    {
        global $log;
        
        if (is_a($column,"SQLColumn")) {
            
            if ($column->columnAlias) {            
                $columnAlias=$column->columnAlias;                
            } else {
                $columnAlias=$column->columnName;
            }            
            
            $columnObj=$column;            
            
        } else if (is_string($column)) {
            $columnName=$column;        
            
            if(is_null($columnDisplay)) {
                $columnDisplay=$columnName;
            }            
            
            if(is_null($columnAlias)) {
                $columnAlias=$columnName;
            }                        
            
            if (array_key_exists($tableName, $this->tableAliases))  {    
                $tableAlias  =$tableName;
                $tableName  = $this->tableAliases[$tableAlias];
            } else {
                $tableAlias = $tableName;
            }
                
                    
            $columnObj = new SQLColumn($columnName, $columnAlias, $tableName, $tableAlias, False, $columnDisplay);                    
            
        } else {
            $msg="addColumn: Column type Invalid  a column must be a string or a SQLColumn object";
            //$log->error("SelectStatementBuilder->addColumn",$msg);
            throw new Exception($msg);            
        }
        
        # do not add duplicate columns
        if(!array_key_exists($columnAlias,$this->columns)){
            $this->columns[$columnAlias] = $columnObj;        
            $columnObj->columnKey = (string)count($this->columns);        
            $this->columnsIndex[$columnObj->columnKey] = $columnObj;
        }
    }

    public function addCaseColumn($sql, $columnName, $label=NULL)
    {
        global $log;
                        
        $columnObj = new SQLColumn($columnName, $columnName, NULL, NULL, True, $label, $sql);            
        $this->columns[$columnName] = $columnObj;        
        $columnObj->columnKey = (string)count($this->columns);        
        $this->columnsIndex[$columnObj->columnKey] = $columnObj;
    }
    
    
    public function addColumn2($columnName, $columnAlias, $tableName, $tableAlias)
    {                        
        $columnObj = new SQLColumn($columnName, $columnAlias, $tableName, $tableAlias, False, $columnName);            
        $this->columns[$columnName] = $columnObj;
        $columnObj->columnKey = (string)count($this->columns);
        $this->columnsIndex[$columnObj->columnKey] = $columnObj;
    }

    public function addColumnEx($columnName, $columnAlias, $tableRef=null)
    {
        $columnObj = new SQLColumn($columnName, $columnAlias, $tableRef);

        // error checking...

        // does $tableRef exist?

        // has column Alias been used already?

        // does column exist in the tableRef?

        $this->columns[$columnAlias] = $columnObj;

    }

    public function getColumn($name)
    {
        if(array_key_exists($name, $this->columns))
        {
            return $this->columns[$name];
        }
        else
        {
            return null;
        }
    }
    
    public function getColumnByIndex($index)
    {
        if(array_key_exists($index, $this->columnsIndex))
        {
            return $this->columnsIndex[$index];
        }
        else
        {
            return null;
        }
    }
    
    
    public function getColumnNames() {
    
        $names = array();
        
        foreach ($this->columns as $columnObj) {
            $names[] = $columnObj->getColumnName();
        }
        
        return $names;
    }
    
    public function getColumnNamesJS() {
            $names = array();
        
        foreach ($this->columns as $columnObj) {
            if(isset($columnObj->columnAlias)){
                $names[] = "{$columnObj->tableRef}_{$columnObj->columnAlias}";
            }
            else{
                 $names[] = "{$columnObj->tableRef}_{$columnObj->columnName}";
            }
        }
        
        return $names;
    }
    
    public function addTable($tableName, $tableAlias=null)
    {
        // don't add duplicate tables
        if (!array_key_exists($tableName, $this->tables)) {
            $tableObj = new SQLTable($tableName,$tableAlias,true);
            $this->tables[$tableName] = $tableObj;
            if (!isnull($tableAlias)) {
                $this->tableAliases[$tableAlias]=$tableName;            
            }
        }
    }
    
    
    public function getTable($tableName)
    {
        if (array_key_exists($tableName, $this->tables)) {
            return $this->tables[$tableName];
        } else {
            return null;
        }
    }
    public function remap($a) {
        $b = array();
        
        foreach($this->columnsIndex as $column) {
            if (isset($a[$column->columnKey])) { 
                $b[$column->columnName] = $a[$column->columnKey];
            } else {
                //echo "error";
            }
                
        }
        
        foreach ($a as $key=>$value) {
            if (array_key_exists($key, $this->columnsIndex)===False) {
                $b[$key]=$value;
            }
        }
        
        return $b;
    }
    

    public function addTableEx($tableName, $tableAlias, $isSubQuery)
    {
        // the table alias must be unique
        if ($tableAlias==null) {
            //$tableAlias = $tableName;
        }

        $tableObj = new SQLTable($tableName, $tableAlias, $isSubQuery);

        // don't add duplicate tables
        if (!array_key_exists($tableAlias, $this->tables)) {
            $this->tables[$tableAlias] = $tableObj;
        }
    }

    public function addFilter($tableName, $columnName, $operator, $value, $conjunction="AND") {
        $filter = array(
            "tableName" => $tableName,
            "columnName" => $columnName,
            "operator" => $operator,
            "value" => $value,
            "conjunction" => $conjunction);

        $this->filters[] = $filter;
    }

    public function addSortColumn($column)  // a SQLColumn 
    {
        if (is_string($column)) {
            $column = $this->columns[$column];
        }
        $this->sortColumns[] = $column;
    }
    
    public function clearSortColumns(){
        $this->sortColumns=array();
    }

    public function addJoin($table1,$table2,$table1Alias,$table2Alias,$column1,$column2,$jointype="inner")
    {
        $newJoin=new SQLJoin($table1,$table2,$table1Alias,$table2Alias,$column1,$column2,$jointype);        
        $this->joins[]=$newJoin; 
    }

    public function addOuterJoinToTable($tableName,$table2,$tableAlias,$operation,$columnFrom,$columnTo)
    {               
        if (array_key_exists($tableName, $this->tables)) {
            $tableObj = $this->tables[$tableName];
        } else {
            return;
        }
        
        if (!isnull($tableAlias)) {
            $this->tableAliases[$tableAlias]=$table2;
        }
                
        $outerJoin = new SQLTableOuterJoin($table2,$tableAlias,$operation, $columnFrom, $columnTo);        
        $tableObj->addOuterJoin($outerJoin);
    }
        
    public function generateTableReferences() {
        $tableNames=array();

        foreach($this->tables as $tableName => $tableObj)
        {
            if ($tableObj->tableAlias) {
                $name="`{$tableObj->tableName}` as {$tableObj->tableAlias}";
            } else {
                $name="`{$tableObj->tableName}`";
            }
            
            $tableNames[] = $name . "\n";
        }

        $tablesReferences = implode(",", $tableNames);

        return $tablesReferences;
    }

    public function generateColumnReferences() {

        $columnNames=array();

        foreach($this->columns as $columnName => $columnObj)
        {
            $buf = "";

            if ($columnObj->tableAlias) {
                $buf .= "`{$columnObj->tableAlias}`.";
            } 
//            if ($columnObj->tableRef) {
//                $buf .= "`{$columnObj->tableRef}`.";
//            } 

            if ($columnObj->isSQL) {
                $escape="";
                $buf .= "{$escape}{$columnObj->sql}{$escape}";
            } else {
                $escape="`";
                $buf .= "{$escape}{$columnObj->columnName}{$escape}";
            }

            

            if ($columnObj->columnAlias) {
                $buf .= " as {$columnObj->columnAlias}";
            }

            $buf .= "\n";
            
            $columnNames[]=$buf;
        }

        $columnReferences = implode(",", $columnNames);

        return $columnReferences;
    }

    public function generateSQLSelectStatement()
    {
        $selectStatement = '';
        $joinstatements=array();
        $joinedTables=array();

        
        // build join statements

        foreach($this->joins as $join)
        {
            // if the table has not been joined on yet...
            // 5/29/2011 This is all wrong...STEVEN!
            // this all depends on why the tables is being joined.
            // 
            // 
//            if(!array_key_exists($join->table2, $joinedTables))
//            {
//               $joinedTables[]=$join->table2;
//               $joinstatement=" ".strtoupper($join->jointype)." JOIN ".$join->table2;
//               $joinstatement.=" ON ".$join->table1.".".$join->column1;
//               $joinstatement.=" = ".$join->table2.".".$join->column2;
//               $joinstatements[$join->table2]=$joinstatement;
//            }
//
//            // if the table has already been add, we must and a new condition to the existing join statement.
//            else
//            {
//               $joinstatement=$joinstatements[$join->table2];
//               $joinstatement.=" AND ".$join->table1.".".$join->column1;
//               $joinstatement.=" = ".$join->table2.".".$join->column2;
//               $joinstatements[$join->table2]=$joinstatement;
//            }            
            if(!array_key_exists($join->table2Alias, $joinedTables))
            {
               $joinedTables[$join->table2Alias]=$join->table2;
               $joinstatement=" ".strtoupper($join->jointype)." JOIN `".$join->table2."` as `".$join->table2Alias."`";
               $joinstatement.=" ON `".$join->table1Alias."`.`".$join->column1."`";
               $joinstatement.=" = `".$join->table2Alias."`.`".$join->column2."`";
               $joinstatements[$join->table2Alias]=$joinstatement . "\n";
            }            
            else
            {
               // give the table an alias...

               $joinstatement=$joinstatements[$join->table2];
               $joinstatement.=" AND ".$join->table1.".".$join->column1;
               $joinstatement.=" = ".$join->table2.".".$join->column2;
               $joinstatements[$join->table2]=$joinstatement . "\n";
            }            
        }
                
        $selectColumns = $this->generateColumnReferences();
        $selectTables = $this->generateTableReferences();

        if ($this->distinct) {
            $selectStatement = "SELECT distinct $selectColumns FROM $selectTables \n";
        } else {                        
            $selectStatement = "SELECT $selectColumns FROM \n";
                    
            $tableSep = "";
            foreach ($this->tables as $tableName => $tableObj)
            {
                if ($tableObj->tableAlias) {
                    $selectStatement .= "{$tableSep}`{$tableObj->tableName}` as {$tableObj->tableAlias}";
                } else {
                    $selectStatement .= "{$tableSep}`{$tableObj->tableName}`";
                }
                
                foreach($tableObj->outerJoins as $outerJoin) {
                    //left outer join IlluminaRunResult RR on FC.flowCellId = RR.flowCellId                    
                    $selectStatement .= " left outer join `{$outerJoin->tableName}` {$outerJoin->tableAlias} on `{$tableObj->tableAlias}`.`{$outerJoin->columnFrom}` = `{$outerJoin->tableAlias}`.`{$outerJoin->columnTo}` ";
                }
                
                if ($tableSep=="") {
                   $tableSep=","; 
                }
            }           
        }
                
        // add join statements to select statment
        foreach($joinedTables as $key=>$tableName)
        {
            $selectStatement.=$joinstatements[$key];
        }
                               
                
        $selectStatement.=$this->generateWhereStatement();

        if (count($this->sortColumns) > 0)
        {
            $orderByStatement = " ORDER BY ";            
            $orderByColumnsString = "";
            
            $sep="";
            foreach($this->sortColumns as $column) {
                $orderByStatement .= $sep . $column->getColumnNameForSort();
                
                if ($sep=="") {
                    $sep=",";
                }
            }
            
            $orderByStatement.=$orderByColumnsString . " " . $this->sortDirection;
            $selectStatement.=$orderByStatement;
        }


        if (($this->numberOfRowsToGet>0) || $this->paging) {
            // generate limit statement

            if ($this->startingRow) {
                $limit = " LIMIT $this->startingRow,$this->numberOfRowsToGet";
            } else {
                $limit = " LIMIT $this->numberOfRowsToGet";
            }

            $selectStatement .= $limit . "\n";
        }

        $selectStatement .= ";" . "\n";

        $this->setStatement($selectStatement,false);
        
        $selectStatement=$this->bindParams();

        return $selectStatement;
    }

    // generate wherestatement with conditions
    public function generateWhereStatement()
    {
        global $log;        
        
        $whereStatement = "WHERE\n";
        
        if(!is_null($this->whereStatement) && count($this->whereStatement->clauses)>0){
            $whereStatement .= $this->whereStatement->generateWhereClause();            
        } else {
            
            if (count($this->filters) == 0)
            {
                $whereStatement = "";
            } else {
            
                $firstFilter = true;
                $subId=1;
                foreach ($this->filters as $filter)
                {
                    $key=$subId;
                    $subId+=1;
                    $filterStatement=$filter["tableName"]. "." .$filter["columnName"]. " " .$filter["operator"]. " " . $this->delim.$key.$this->delim;
                    $value=$filter["value"];
                    $valueType="";

                    if(is_string($value)) {
                        $valueType="string";
                    } else if (is_bool($value)) {
                        $valueType="boolean";
                    } else if (is_float($value)) {
                        $valueType="float";
                    } else if (is_int($value)) {
                        $valueType="integer";
                    } else if (is_a($value,"SQLColumn")) {
                        $valueType="SQLColumn";                    
                    } else {
                        $valueType=get_class($value);
                        $msg="Invalid filter type of {$valueType} recieved";
                        $log->error("SelectStatementBuilder->buildWhereStatement",$msg);
                        throw (new Exception($msg)); // Really?  TODO: Fix this!
                    }

                    if ($firstFilter) {
                        // add where clause before the first statement

                        $whereStatement.=$filterStatement."\n";
                        $firstFilter = False;
                    } else {
                        $whereStatement.=" " . $filter["conjunction"] . " ";
                        $whereStatement.=$filterStatement."\n";
                    }
                    $this->createParam($key,$valueType,$value);
                }
            }
        }
        
        return $whereStatement;
    }

    
    public function genenerateRowCountQuery()
    {
        if($this->distinct===false){
            $rowCountBuilder= clone $this;

            $rowCountBuilder->columns=array();
            $rowCountBuilder->addColumn(new SQLColumn("Count",null, null, null, True, "Count", "count(*)"), Null, Null, Null);
            $rowCountBuilder->paging=false;
            $rowCountBuilder->numberOfRowsToGet=0;

            $SQL = $rowCountBuilder->generateSQLSelectStatement();

            return $SQL;
        }
        else{
            if($this->customCountColumn===""){
                throw new Exception("Queries using DISTINCT must have a customCountColumn");
            }
            
            $rowCountBuilder= clone $this;

            $rowCountBuilder->columns=array();
            $rowCountBuilder->addColumn(new SQLColumn("Count",null, null, null, True, "Count", $this->customCountColumn), Null, Null, Null);
            $rowCountBuilder->paging=false;
            $rowCountBuilder->numberOfRowsToGet=0;

            $SQL = $rowCountBuilder->generateSQLSelectStatement();

            return $SQL;
        }

    }
    

    public function setNumberOfRowsToGet($value) {

        $this->numberOfRowsToGet = $value;
    }
    
    public function setDistinct($value) {
        $this->distinct = $value;
    }
    
    /**
     *  This function takes the names of two tables and the uses a graph of the objDefs to
     *  add all of the appropriate join statements to the query. When a join is added, the table
     *  is added to the table list. If the table has already been added then the a join is not
     *  added as that table is already included in the query.
     */
    public function addAllJoins($tableOne,$tabletTwo){
        
    }
}