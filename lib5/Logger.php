<?php
class Logger {

    private static $instance;
    public $runClass;

    const INFO = 'info';
    const WARNING = 'warning';
    const ERROR = 'error';

    const LOGFILE = 'errors.log';
    const DELIM = '|';

    private function  __construct($runClass=Null, $filename=Null, $delim=Null) {                                        
        $this->runClass=$runClass;
        $this->filename = $filename;
        $this->delim = $delim;
    }

    // error class, error type, message
    public function log($logType,$errClass,$errMsg)
    {
        $date = date('Y-m-d h:i:s');

        //  error type first
        //$out = $type.self::DELIM.$date.self::DELIM.$class.self::DELIM.$msg."\r\n";

        // date first
        if (is_array($errMsg)) {
            foreach($errMsg as $m) {
                $out = $date.self::DELIM.$this->runClass.self::DELIM.$logType.self::DELIM.$errClass.self::DELIM.$m."\r\n";
            }
        } else {
            $out = $date.self::DELIM.$this->runClass.self::DELIM.$logType.self::DELIM.$errClass.self::DELIM.$errMsg."\r\n";
        }
        
        error_log($out, 3, $this->filename);
    }

    public function info($errClass, $errMsg)
    {
        $this->log("inf",$errClass, $errMsg);
    }

    public function warn($errClass, $errMsg)
    {
        $this->log("wrn",$errClass, $errMsg);
    }

    public function error($errClass, $errMsg)
    {
        $this->log("err",$errClass, $errMsg);
    }

    public static function factory($runClass, $filename=Null) {
        
        if (!isset(self::$instance)) {
            
            if (is_null($filename)) {
                $filename=self::LOGFILE;
            }
                
            self::$instance = new Logger($runClass, $filename, self::DELIM);
        }

        return self::$instance;
    }
}

?>
