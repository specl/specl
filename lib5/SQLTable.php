<?php

class SQLTableOuterJoin
{
    public function __construct($tableName,$tableAlias,$operation, $columnFrom, $columnTo)
    {
        $this->tableName = $tableName;
        $this->tableAlias = $tableAlias;
        $this->operation = $operation;
        $this->columnFrom = $columnFrom;
        $this->columnTo = $columnTo;
    }    

}


class SQLTable
{
    public function __construct($tableName,$tableAlias,$isSubQuery=false)
    {
        $this->tableName=$tableName;
        $this->tableAlias=$tableAlias;
        $this->isSubQuery=$isSubQuery;
        $this->count=1;        
        $this->outerJoins = Array();
    }
    
    public function addOuterJoin($outerJoin) {
        $this->outerJoins[] = $outerJoin;
    }
}
?>
