<?php
/**
 * Description of SQLBuilder
 *
 * @author tony
 */


class SQLBuilder extends QueryBuilder {

    private $dbConn;
    private $specMgr;

    public $addDropStatements;
    public $tablePrefix;
    public $indent;
    public $defaultStringLength;
    public $defaultIntegerLength;
    public $outputPath;
    public $saveSeperateFiles;
    public $saveSQL;
    public $statements;
    public $variableRegex;
    public $SQL;
    public $storageEngine;
    public $charSet;
    public $useSchemaPrefix;
    
    public function  __construct($dbConn, $specMgr) {

        parent::__construct();
        
        $this->dbConn = $dbConn;
        $this->specMgr = $specMgr;
        $this->addDropStatements = true;
        $this->tablePrefix ="";
        $this->indent = "    ";
        $this->defaultStringLength = 50;
        $this->defaultIntegerLength = 10;
        $this->outputPath = dirname(__FILE__);
        $this->saveSeperateFiles = true;
        $this->saveSQL = true;
        //$this->queryBuilder=new QueryBuilder();
        $this->storageEngine = "InnoDB";
        $this->charSet = "latin1";
        $this->useSchemaPrefix=true;
    }
    
    public function getTableName($objDef) {

        if ($objDef->hasAttribute('schemaName')) {
            $schemaName = $objDef->getAttribute('schemaName');
        } else {
            $schemaName = $this->dbConn->schema;
        }

        if ($objDef->hasAttribute('table')) {
            $tableName = $objDef->getAttribute('table');
        } else {
            $tableName = $this->tablePrefix.$objDef->name;
        }

        if ($this->useSchemaPrefix) {
            $tableName = "`".$schemaName."`.`".$tableName."`";
        } else {
            $tableName = "`".$tableName."`";
        }
        return $tableName;
    }

    static protected function truncate($value, $length)
    {
        return substr($value, 0, $length);
    }


    public function genWhereClauseFromKeys($objDef) {

        $whereClause = "";
        $sep = "";
        foreach($objDef->primaryKey as $key) {
            $pkPropDef = $objDef->getProperty($key);

            $whereClause .= $sep;
            $whereClause .= "`{$pkPropDef->name}`";
            $whereClause .= "=";
            $whereClause .= "%".$pkPropDef->name."%";

            if ($sep == "") $sep = " AND ";
        }
        return $whereClause;
    }

    
    public function genWhereClauseFromArrayKeys($objDef, $keys) {

        $whereClause = "";
        $sep = "";
        foreach($keys as $key=>$value) {
            $pkPropDef = $objDef->getProperty($key);

            if ($pkPropDef) {
                $whereClause .= $sep;
                $whereClause .= "`{$pkPropDef->name}`";
                $whereClause .= "=";
                $whereClause .= "%".$pkPropDef->name."%";

                if ($sep == "") $sep = " AND ";
            }
        }
        return $whereClause;
    }
    
    public function genWhereClauseFromAssociation($objDef,$associationObj) {

        $toObjDef = $this->specMgr->findDef($associationObj->objDefId);

        $whereClause = "";
        $sep = "";

        foreach($associationObj->links as $link) {

            $fromPropDef = $objDef->getProperty($link->fromPropDefId);
            $toPropDef = $toObjDef->getProperty($link->toPropDefId);

            $whereClause .= $sep;
            $whereClause .= "`{$toPropDef->name}`";
            $whereClause .= "=";
            $whereClause .= "%".$fromPropDef->name."%";

            if ($sep == "") $sep = " AND ";
        }
        return $whereClause;
    }


    public function getValueAsSQL($pkPropDef,$obj) {
        
        if (isset($obj[$pkPropDef->name])) {
                $value=$obj[$pkPropDef->name];
        }
        
        $value = null;

        if (is_array($value)) {

            if (!isset ($value['type'])) {
                //print 'here';
            } else {                                    
                if ($value['type']=='function') {
                    $type = "function";        
                    $value = $value['value'];        
                }
            }
        } else {

            if (is_a($value,"EmptyValue")) {
                $value = null;
            } 

            if (is_null($value)) {
                $type = "null";                
            } else if ($pkPropDef->objectType=="string") {
                $type = "string";                
            } else if ($pkPropDef->objectType=="date") {
                $type = "date";                
            } else if ($pkPropDef->objectType=="datetime") {
                $type = "datetime";                
            } else if (($pkPropDef->objectType=="integer")||($pkPropDef->objectType=="auto")) {
                $type = "integer";                
            } else if ($pkPropDef->objectType=="boolean") {
                $type = "boolean";                
            } else if ($pkPropDef->objectType=="float") {
                $type = "float";                
            } else if ($pkPropDef->objectType=="double") {
                $type = "double";                
            } else {
                //throw new Exception("genSQLWhereClauseFromKeys does not handle datatype {$pkPropDef->objectType}, please fix immediately.");
                $type = null;                
            }
        }
        
        if (!is_null($type)) {                        
            $newValue = $qb->convertParam($pkPropDef->name, $paramType, $paramValue);                
        }        
    }
        
    // Create Substitution Parameters From PropDef Array Using Either Array Or Object
    public function createSubParametersFromPropDefs($propDefArray,$obj) {        
        foreach($propDefArray as $pkPropDef) {
            $value = null;
            
            if (isset($obj[$pkPropDef->name])) {
                $value=$obj[$pkPropDef->name];
            }
            
            if (is_array($value)) {
                
                if (!isset ($value['type'])) {
                    //print 'here';
                } else {                                    
                    if ($value['type']=='function') {
                        $type = "function";        
                        $value = $value['value'];        
                    }
                }
            } else {
                
                if (is_a($value,"EmptyValue")) {
                    $value = null;
                } 

                if (is_null($value)) {
                    $type = "null";                
                } else if ($pkPropDef->objectType=="string") {
                    $type = "string";                
                } else if ($pkPropDef->objectType=="date") {
                    $type = "date";                
                } else if ($pkPropDef->objectType=="datetime") {
                    $type = "datetime";                
                } else if (($pkPropDef->objectType=="integer")||($pkPropDef->objectType=="auto")) {
                    $type = "integer";                
                } else if ($pkPropDef->objectType=="boolean") {
                    $type = "boolean";                
                } else if ($pkPropDef->objectType=="float") {
                    $type = "float";                
                } else if ($pkPropDef->objectType=="double") {
                    $type = "double";                
                } else {
                    //throw new Exception("genSQLWhereClauseFromKeys does not handle datatype {$pkPropDef->objectType}, please fix immediately.");
                    $type = null;                
                }
            }
            
            if (!is_null($type)) {                
                $this->createParam($pkPropDef->name, $type, $value);
            }
        }
    }

    public function genSQLWhereClauseFromKeys($objDef, $obj) {

        $whereClause = $this->genWhereClauseFromKeys($objDef);

        $this->setStatement($whereClause);

        $propDefArray = array();

        foreach($objDef->primaryKey as $key) {
            $pkPropDef = $objDef->getProperty($key);
            $propDefArray[] = $pkPropDef;
        }

        $this->createSubParametersFromPropDefs($propDefArray,$obj);

        //$sql=$this->queryBuilder->bindParams();
        $sql=$this->bindParams();

        return $sql;
    }

    public function genSQLWhereClauseFromArrayKeys($objDef, $keys) {

        $whereClause = $this->genWhereClauseFromArrayKeys($objDef,$keys);

        $this->setStatement($whereClause);

        $propDefArray = array();

        foreach($keys as $key=>$value) {
            $pkPropDef = $objDef->getProperty($key);
            if ($pkPropDef) {
                $propDefArray[] = $pkPropDef;
            }
        }

        $this->createSubParametersFromPropDefs($propDefArray,$keys);

        //$sql=$this->queryBuilder->bindParams();
        $sql=$this->bindParams();

        return $sql;
    }

    
    public function genSQLWhereClauseFromAssociation($objDef, $obj, $associationObj) {

        $whereClause = $this->genWhereClauseFromAssociation($objDef,$associationObj);

        $this->queryBuilder->setStatement($whereClause);

        $propDefArray = array();
        foreach($associationObj->links as $link) {
            $propDef = $objDef->getProperty($link->fromPropDefId);
            $propDefArray[] = $propDef;
        }

        $this->createSubParametersFromPropDefs($propDefArray,$obj);

        $sql=$this->queryBuilder->bindParams();

        return $sql;
    }


    public function genExistsByPK($objDef, $obj) {
        global $log;
        
        if (is_null($objDef)) {
            return null;
        }

        $out="";

        $whereClause = $this->genSQLWhereClauseFromKeys($objDef, $obj);

        if ($whereClause=="") {
            // throw error, no primary keys defined, this is a bug in the Spec Def
            return $out;
        }
        
        $out .= "select exists(select * from ".$this->getTableName($objDef)." ";
        $out .= "where ";
        $out .= $whereClause;
        $out .= " limit 1) as Success;";

        return $out;
    }

    public function genGetByPK($objDef, $obj) {
        global $log;

        if (is_null($objDef)) {
            return null;
        }

        $qs = new QueryDef();

        $out="";
        $keyProps=array();
        $columnsList = "";
                
        $propIdsNotToInclude = $this->propsNotToInclude($objDef);        
        
        $whereClause = $this->genSQLWhereClauseFromKeys($objDef,$obj);
        if ($whereClause=="") {
            // no primary key? fail
            return $out;
        }

        $term="";
        foreach($objDef->getProperties() as $key=>$property) {                   
            if (!array_key_exists($property->name, $propIdsNotToInclude)) {
                $columnsList .= $term."`".$property->name."`";
                $qs->selectedProperties[] = $property;
                if ($term=="") $term=",";
            }
        }

        $out .= "select ";
        $out .= $columnsList;
        $out .= " from ". $this->getTableName($objDef) ;
        $out .= " where ";
        $out .= $whereClause;
        $out .= ";";

        $qs->sql = $out;

        return $qs;
    }
    
    public function genGetByPK2($objDef, $obj, $includeConstraints=true) {
        global $log;
        global $app;
        
        if (is_null($objDef)) {
            return null;
        }
        
        $qs = new QueryDef();   
        // part 1
        $joinTables=array();
        
        $tableName=$objDef->getAttribute("table",$objDef->name);        
        $this->selectBuilder=new SelectStatementBuilder();       
        //$this->selectBuilder->addTable($objDef->name);
        $this->selectBuilder->addTable($tableName);
        
        foreach($objDef->getProperties() as $propDef)
        {
            // 5/29/2011 ALL
            // we need to handle PropDefs that are collections seperately
            // perhaps as Master/Detail, a grid within a grid, or a popup            
            $tableName=$propDef->getAttribute("table");
          
            if ($propDef->isCollection() || !$propDef->isScalar()) 
            {
                // TODO: handle this
                continue;
            }
            
            if($propDef->hasAttribute("excludeFromQuery")){
                if($propDef->getAttribute("excludeFromQuery")===true){
                   continue;
                }
            }
        
            if($propDef->hasAttribute("caption"))
            {
                $label = $propDef->getAttribute("caption");
            } else {
                $label = $propDef->name;
            }
                
            if(!$includeConstraints || is_null($propDef->getConstraint()))
            {
                //$this->selectBuilder->addColumn($propDef->name,$objDef->name);                    
                $this->selectBuilder->addColumn($propDef->name, $tableName, $label);                
                $qs->selectedProperties[] = $propDef;                   
            }
            else
            {
                $constraint=$propDef->getConstraint(); //TODO: what mode?
                $this->selectBuilder->addColumn($propDef->name,$tableName, $label);                     
                $qs->selectedProperties[] = $propDef;
                
                if ($constraint->includeInQuery==false) {
                    
                    // still include the column in the query, just don't use the constraint
                    // to buyild another join
                    //$this->selectBuilder->addColumn($propDef->name,$objDef->name);
                } else {
                
                    if ($propDef->hasAttribute("nullAllowed")) {
                        $nullAllowed = $propDef->getAttribute("nullAllowed");                    
                        if ($nullAllowed) {
                            $joinType="left";
                        } else {
                            $joinType="inner";
                        }
                    } else {
                        $joinType="inner";
                    }

                    //$tableRef = $this->selectBuilder->getTable($constraint->tableName);
                    if (array_key_exists($constraint->tableName, $joinTables)) {
                        $count = $joinTables[$constraint->tableName];
                    } else {
                        $count = 1;
                        $joinTables[$constraint->tableName] = $count;
                    }

                    $tableAlias=$constraint->tableName.$count;
                    $joinTables[$constraint->tableName] += 1;

                    //$this->selectBuilder->addJoin($objDef->name,$constraint->tableName,
                    //                              $propDef->name,$constraint->idColumnName, $joinType);
                    //                
                    //$column=new SQLColumn($constraint->displayColumnName, $propDef->name, $constraint->tableName);
                    //$this->selectBuilder->addJoin($objDef->name,$constraint->tableName,$objDef->name,$tableAlias,
                    //                              $propDef->name,$constraint->idColumnName, $joinType);

                    $this->selectBuilder->addJoin($tableName, $constraint->tableName,$tableName,$tableAlias,
                                                  $propDef->name,$constraint->idColumnName, $joinType);

                    if ($constraint->idColumnName!=$constraint->displayColumnName) {
                        $joinObjDef = $app->specManager->findDef($constraint->tableName); 
                        $joinPropDef = $joinObjDef->getProperty($constraint->displayColumnName);
                        $qs->selectedProperties[] = $propDef;
                        $qs->selectedProperties[] = $joinPropDef;
                    }
                    //$column=new SQLColumn($constraint->displayColumnName, $propDef->name, $constraint->tableName, $tableAlias, False, $label);                   
                    $column=new SQLColumn($constraint->displayColumnName,$constraint->displayColumnName, $constraint->tableName, $tableAlias, False, $label);                   
                    $this->selectBuilder->addColumn($column,Null, Null);
                }
            }
        }
                       
        //$whereClause = $this->genSQLWhereClauseFromKeys($objDef,$obj);        
        //$this->selectBuilder->addWhereClause($whereClause, $conjunction="AND");                        
     
        $tableName=$objDef->getAttribute("table",$objDef->name);
        foreach($objDef->primaryKey as $key) {       
            $pkPropDef = $objDef->getProperty($key);
                        
            $tableName = $tableName;
            $columnName = $pkPropDef->name;
            $operator = "=";
            
            if (is_array($obj)==True) {
                if (isset($obj[$pkPropDef->name])) {
                    $value=$obj[$pkPropDef->name];
                }
            } else {
                if (isset($obj->{$pkPropDef->name})) {
                    $value=$obj->{$pkPropDef->name};
                }
            }
            
            if (is_null($value)) {
                $value = null;
            }
            //$subValue = $this->convertParam($key, $paramValue, $pkPropDef->objectType);              
            //$value = $subValue;
            $conjunction="AND";                
            $this->selectBuilder->addFilter($tableName, $columnName, $operator, $value, $conjunction);
        }            

        $sql = $this->selectBuilder->generateSQLSelectStatement();
        $qs->sql = $sql;

        return $qs;
    }
        
    public function buildSelectFromObjDef($objDef, $includeConstraints=true)
    {
        $joinTables=array();        
        $tableName=$objDef->getAttribute("table",$objDef->name);        
        $selectBuilder=new SelectStatementBuilder();
        
        //$selectBuilder->addTable($objDef->name);
        $selectBuilder->addTable($tableName);
        
        foreach($objDef->getProperties() as $propDef)
        {
            // 5/29/2011 ALL
            // we need to handle PropDefs that are collections seperately
            // perhaps as Master/Detail, a grid within a grid, or a popup            
            $tableName=$propDef->getAttribute("table");
          
            if ($propDef->isCollection() || !$propDef->isScalar()) 
            {
                // TODO: handle this
                continue;
            }
            
            if($propDef->hasAttribute("excludeFromQuery")){
                if($propDef->getAttribute("excludeFromQuery")===true){
                   continue;
                }
            }
            
            if($propDef->hasAttribute("caption"))
            {
                $label = $propDef->getAttribute("caption");
            } else {
                $label = $propDef->name;
            }
                
            if(!$includeConstraints || is_null($propDef->getConstraint()))
            {
                //$selectBuilder->addColumn($propDef->name,$objDef->name);
    
                
                $selectBuilder->addColumn($propDef->name, $tableName, $label);
            }
            else
            {
                $constraint=$propDef->getConstraint(); //TODO: what mode?
                                
                if ($constraint->includeInQuery==false) {
                    
                    // still include the column in the query, just don't use the constraint
                    // to buyild another join
                    //$selectBuilder->addColumn($propDef->name,$objDef->name);
                    $selectBuilder->addColumn($propDef->name,$tableName, $label);
                     
                } else {
                
                    if ($propDef->hasAttribute("nullAllowed")) {
                        $nullAllowed = $propDef->getAttribute("nullAllowed");                    
                        if ($nullAllowed) {
                            $joinType="left";
                        } else {
                            $joinType="inner";
                        }
                    } else {
                        $joinType="inner";
                    }


                    //$tableRef = $selectBuilder->getTable($constraint->tableName);
                    if (array_key_exists($constraint->tableName, $joinTables)) {
                        $count = $joinTables[$constraint->tableName];
                    } else {
                        $count = 1;
                        $joinTables[$constraint->tableName] = $count;
                    }

                    $tableAlias=$constraint->tableName.$count;

                    $joinTables[$constraint->tableName] += 1;

    //                $selectBuilder->addJoin($objDef->name,$constraint->tableName,
    //                                              $propDef->name,$constraint->idColumnName, $joinType);
    //                
    //                $column=new SQLColumn($constraint->displayColumnName, $propDef->name, $constraint->tableName);

                    
                    //$selectBuilder->addJoin($objDef->name,$constraint->tableName,$objDef->name,$tableAlias,
                    //                              $propDef->name,$constraint->idColumnName, $joinType);

                    $selectBuilder->addJoin($tableName, $constraint->tableName,$tableName,$tableAlias,
                                                  $propDef->name,$constraint->idColumnName, $joinType);

                    
                    $column=new SQLColumn($constraint->displayColumnName, $propDef->name, $constraint->tableName, $tableAlias, False, $label);                   
                    $selectBuilder->addColumn($column,Null, Null);
                }
            }
            
        }
        return $selectBuilder;
    }
    
    
    public function genGetByKeys($objDef, $keys) {
        global $log;

        if (is_null($objDef)) {
            return null;
        }

        $qs = new QueryDef();

        $out="";
        $keyProps=array();
        $columnsList = "";
                
        $propIdsNotToInclude = $this->propsNotToInclude($objDef);        
        

        $whereClause = $this->genSQLWhereClauseFromArrayKeys($objDef,$keys);
        if ($whereClause=="") {
            // no primary key? fail
            return $out;
        }

        $term="";
        foreach($objDef->getProperties() as $key=>$property) {                   
            if (!array_key_exists($property->name, $propIdsNotToInclude)) {
                $columnsList .= $term."`".$property->name."`";
                $qs->selectedProperties[] = $property;
                if ($term=="") $term=",";
            }
        }

        $out .= "select ";
        $out .= $columnsList;
        $out .= " from ". $this->getTableName($objDef) ;
        $out .= " where ";
        $out .= $whereClause;
        $out .= ";";

        $qs->sql = $out;

        return $qs;
    }
    
    public function genGetByWhere($objDef,$whereDef){
         global $log;

        if (is_null($objDef)) {
            throw new Exception ("objectDef is null");
        }

        $qs = new QueryDef();

        $out="";
        $keyProps=array();
        $columnsList = "";
        
        $propIdsNotToInclude = $this->propsNotToInclude($objDef);

        $whereClause = $whereDef->generateWhereClause();
        $whereDef->addTable($this->getTableName($objDef));
        $tables = $whereDef->generateTables();

        $term="";
        foreach($objDef->getProperties() as $key=>$property) {
            if (!array_key_exists($property->name, $propIdsNotToInclude)) {
                $columnsList .= $term."`".$objDef->name."`.`".$property->name."`";
                $qs->selectedProperties[] = $property;
                if ($term=="") $term=",";
            }
        }
        
        $out .= "select ";
        $out .= $columnsList;
        $out .= " from ". $tables; //$this->getTableName($objDef) ;
        
        if ($whereClause!="") {
            $out .= " where ";
            $out .= $whereClause;   
        }

        $out .= ";";

        $qs->sql = $out;

        return $qs;
    
    }

    public function genGetByFK2($objDef, $toObjDef, $obj, $associationObj) {
        global $log;

        if (is_null($objDef)) {
            return null;
        }

        $qs = new QueryDef();

        $out="";
        $keyProps=array();
        $columnsList = "";

        $propIdsNotToInclude = array();

        $whereClause = $this->genSQLWhereClauseFromAssociation($objDef,$obj,$associationObj);
        if ($whereClause=="") {
            // no primary key? fail
            return $out;
        }


        // don't select none scalar properties...
        foreach($toObjDef->getProperties() as $key=>$property) {
            if (!$property->isScalar()) {
                $propIdsNotToInclude[$property->name]=$property;
            }
        }

        $term="";
        foreach($toObjDef->getProperties() as $key=>$property) {
            if (!array_key_exists($property->name, $propIdsNotToInclude)) {
                $columnsList .= $term."`".$property->name."`";
                $qs->selectedProperties[] = $property;
                if ($term=="") $term=",";
            }
        }

        $out .= "select ";
        $out .= $columnsList;
        $out .= " from ". $this->getTableName($toObjDef) ;
        $out .= " where ";
        $out .= $whereClause;
        $out .= ";";

        $qs->sql = $out;

        return $qs;
    }

    public function genGetByFK($objDef, $collectionDef, $obj) {
        global $log;

        if (is_null($objDef)) {
            return null;
        }

        $qs = new QueryDef();

        $out="";
        $keyProps=array();
        $columnsList = "";

        $propIdsNotToInclude = array();

        $whereClause = $this->genSQLWhereClauseFromKeys($objDef,$obj);
        if ($whereClause=="") {
            // no primary key? fail
            return $out;
        }

        $propIdsNotToInclude = $this->propsNotToInclude($collectionDef);
        
        $term="";
//        $chars=0;
        foreach($collectionDef->getProperties() as $key=>$property) {
            if (!array_key_exists($property->name, $propIdsNotToInclude)) {
                $columnsList .= $term."`".$property->name."`";
                $qs->selectedProperties[] = $property;
                if ($term=="") $term=",";
                
//                $chars += len($property->name);
//                if ($chars>60) {
//                    $chars=0;
//                    $columnsList .= "<br/>\n";
//                }
            }
        }

        $out .= "select ";
        $out .= $columnsList;
        $out .= " from ". $this->getTableName($collectionDef) ;
        $out .= " where ";
        $out .= $whereClause;
        $out .= ";";

        $qs->sql = $out;

        return $qs;
    }

    public function propsNotToInclude($objDef) {
        $propIdsNotToInclude = array();
        
        foreach($objDef->getProperties() as $key=>$propDef) {
            if ($propDef->isAttribute("excludeFromQuery", true)) {
                $propIdsNotToInclude[$propDef->name] = $propDef;    
            }
            
            if (!$propDef->isScalar()) {
                $propIdsNotToInclude[$propDef->name]=$propDef;
            }                    
        }                
        
        return $propIdsNotToInclude;
    }

    public function propsNotToIncludeInsert($objDef) {
        $propIdsNotToInclude = array();
        
        foreach($objDef->primaryKey as $key) {
            $pkPropDef = $objDef->getProperty($key);
            // do not include if property an AUTO_NUMBER and if it is a JOIN
            if (($pkPropDef->objectType=="auto")||($pkPropDef->isAttribute("autoIncrement",true))) {
                $propIdsNotToInclude[$pkPropDef->name] = $pkPropDef;
            }
        }    
        
        foreach($objDef->getProperties() as $key=>$propDef) {
            if ($propDef->isAttribute("excludeFromQuery", true)) {
                $propIdsNotToInclude[$propDef->name] = $propDef;    
            }
            
            if (!$propDef->isScalar()) {
                $propIdsNotToInclude[$propDef->name]=$propDef;
            }        
            
        }                
        
        return $propIdsNotToInclude;
    }

    
    public function genInsert($objDef, $obj) {
        global $log;

        // need to know if any of the forgein keys are based on a field that is auto increment.
        
        if (is_null($objDef)) {
            return null;
        }

        $out="";
        $propIdsNotToInclude = $this->propsNotToIncludeInsert($objDef);
        $columnsToInsert = array();

        // no primary key? fail?
        $columnsList = "";
        $valuesList = "";
        $term="";
        foreach($objDef->getProperties() as $key=>$property) {
            
            if (($property->isCollection() == false) && ($property->isScalar()==true)) {
                if (!array_key_exists($property->name, $propIdsNotToInclude)) {
                    $columnsList .= $term."`{$property->name}`";
                    $valuesList .= $term."%$property->name%";
                    if ($term=="") $term=",";
                }
            }
        }

        $out .= "insert into ". $this->getTableName($objDef) ;
        $out .= " (";
        $out .= $columnsList;
        $out .= ") values (";
        $out .= $valuesList;
        $out .= ");";

        //echo "{$out}<br/>";        
        $this->setStatement($out);

        $propDefArray = array();
        foreach($objDef->getProperties() as $key=>$propDef) {

            if (($propDef->isCollection() == false) && $propDef->isScalar()) {
                if (!array_key_exists($propDef->name, $propIdsNotToInclude)) {
                    $propDefArray[] = $propDef;
                }
            }
        }

        $this->createSubParametersFromPropDefs($propDefArray,$obj);
        
        $SQL=$this->bindParams();

        return $SQL;
    }
    
    /**
     * 
     *  This method is here to maintain backwards compatability with the existing LIMS code
     *  the actual code has been moved to the SpecUtils.php file.
     */
    //public function compareAToB($objDef, $objA, $objB) {
    //    return compareAToB($objDef,$objA,$objB);
    //}
        
    // returns a SQL string on success
    // returns NULL on failure
    public function genUpdateByPK($objDef, $objOld, $objNew) {
        $SQL = "";

        // get object using primary key in the objDef and the values in the objOld
        // compare all values in objOld to the values inb obJNew and
        // only update those that have actually changed.
        
        // if objOld (Primary Key) does not exist in the database raise an error.
        
        // The SpecDef should have validated that the object can be updated BEFORE
        // this function is ever called.
        
        if (is_null($objDef)) {
            return null;
        }

        $propIdsNotToInclude = $this->propsNotToInclude($objDef);
        
        $different = compareAToB($objDef, $objOld, $objNew);
        
        if (count($different)==0) {
            // there is nothing different between old and new.
            return null;
        }
        
        
        $SQL .= "update ". $this->getTableName($objDef). " set " ;
        
        $term="";
        $setValues = "";
        $propDefArray = array();
        foreach($objDef->getProperties() as $property) {
            
            if (isset ($different[$property->name]) &&
                (array_key_exists($property->name,$propIdsNotToInclude)==false)) {
                    
                $propDefArray[] = $property; // this property will be used in the bindStatement call
                
                $setValues .= $term."`{$property->name}`=%$property->name%";
                if ($term=="") $term=",";
            }
        }
        
        $SQL .= $setValues . " where ";
        
        // use objOld for Primary key in Where clause.
        $whereClause = $this->genSQLWhereClauseFromKeys($objDef,$objOld);
        if ($whereClause=="") {
            // throw error, no primary keys defined, this is a bug in the Spec Def
            return null;
        }
        
        $SQL .= $whereClause. ";";
                
        $this->setStatement($SQL);
        
        $this->createSubParametersFromPropDefs($propDefArray,$objNew);
        
        $SQL=$this->bindParams();
                
        return $SQL;
    }
    
    
    // returns a SQL string on success
    // returns NULL on failure
    public function genUpdateByPK2($objDef, $objOld, $objNew) {
        $SQL = "";

        // get object using primary key in the objDef and the values in the objOld
        // compare all values in objOld to the values inb obJNew and
        // only update those that have actually changed.
        
        // if objOld (Primary Key) does not exist in the database raise an error.
        
        // The SpecDef should have validated that the object can be updated BEFORE
        // this function is ever called.
        
        if (is_null($objDef)) {
            return null;
        }

        $propIdsNotToInclude = $this->propsNotToInclude($objDef);
        
        $different = compareAToB($objDef, $objOld, $objNew);
        
        if (count($different)==0) {
            // there is nothing different between old and new.
            return null;
        }
        
        
        $SQL .= "update ". $this->getTableName($objDef). " set " ;
        
        $term="";
        $setValues = "";
        $propDefArray = array();
        foreach($objDef->getProperties() as $property) {
            
            if (isset ($different[$property->name]) &&
                (array_key_exists($property->name,$propIdsNotToInclude)==false)) {
                    
                $propDefArray[] = $property; // this property will be used in the bindStatement call
                
                $setValues .= $term."`{$property->name}`=%$property->name%";
                if ($term=="") $term=",";
            }
        }
        
        $SQL .= $setValues . " where ";
        
        // use objOld for Primary key in Where clause.
        $whereClause = $this->genSQLWhereClauseFromKeys($objDef,$objOld);
        if ($whereClause=="") {
            // throw error, no primary keys defined, this is a bug in the Spec Def
            return null;
        }
        
        $SQL .= $whereClause. ";";
                
        $this->setStatement($SQL);
        
        $this->createSubParametersFromPropDefs($propDefArray,$objNew);
        
        $SQL=$this->bindParams();
                
        return $SQL;
    }
    
    
    public function genUpdateByWhere($objDef, $obj, $whereDef){

    }
    
    public function genUpdate($objDef,$currentObj,$updateObj) {
       
        // check to make sure the data passed in has primary key data
        // 
        // check first for original primary key columns then for regular primary key
        // columns
        
        // update row based on primary key specified
        // SQL would be like "update {$tableName} SET {$propKey}={$propValue} where {primaryKey}={primaryIdValue}
        
        // Primary key columns can only be updated if a primary key original_value column exists.
        
        // If the primary key original value is pased in then that is used for the lookup
        // rather than the primary key column.
        
        // an original primary key starts takes the form "original-{$primaryKeyName}"
    
         return $this->genUpdateByPK($objDef, $currentObj, $updateObj);
        
    }
    
    

    public function genTableDrop($objDef) {

        $out = "DROP TABLE IF EXISTS ".$this->getTableName($objDef).";\n";

        return $out;
    }

    public function  genResetAutoIncrement($objDef, $start) {

        $out = "  ALTER TABLE ".$this->getTableName($objDef)." AUTO_INCREMENT = ".$start.";\n";

        return $out;
    }


    public function genSelectAll($objDef) {
        $qs = new QueryDef();
        
        $propIdsNotToInclude = $this->propsNotToInclude($objDef);        
        $columnsList = "";
        $term="";
        foreach($objDef->getProperties() as $key=>$property) {                   
            if (!array_key_exists($property->name, $propIdsNotToInclude)) {
                $columnsList .= $term."`".$property->name."`";
                $qs->selectedProperties[] = $property;
                if ($term=="") $term=",";
            }
        }
        
        $qs->sql = "SELECT ".$columnsList." FROM ".$this->getTableName($objDef).";\n";
        return $qs;
    }
    
    public function genDeleteAll($objDef) {
        $out = "DELETE FROM ".$this->getTableName($objDef).";\n";
        return $out;
    }
            
    public function genTableCreate($name) {
        $objDef = $this->specMgr->findDef($name);

        if (is_null($objDef)) {
            return null;
        }
        
        $out="";

        $out .= "CREATE TABLE  ".$this->getTableName($name)." (\n";

        $term="";

        foreach($objDef->getProperties() as $key=>$property) {

            $out .= $term.$this->indent;

            if ($property->isScalar()) {

                $error = false;


                if ($property->objectType=="auto") {
                    $dataType = " int(10) unsigned auto_increment";
                } else if ($property->objectType=="string") {
                    if ($property->hasAttribute("fixedLength")) {
                        $dataType = " char(".$property->getAttribute("fixedLength").")";
                    } else if ($property->hasAttribute("variableLength")) {
                        $dataType = " varchar(".$property->getAttribute("variableLength").")";
                    } else {
                        $dataType = " varchar(".$this->defaultStringLength.")";
                    }
                } else if ($property->objectType=="date") {
                    $dataType = " datetime";
                } else if ($property->objectType=="datetime") {
                    $dataType = " datetime";
                } else if ($property->objectType=="integer") {

                    $dataType = " int";

                    if ($property->hasAttribute("displayWidth")) {
                        $displayWidth = $property->getAttribute("displayWidth");

                        if (is_int($displayWidth)) {
                            if (($displayWidth>0) && ($displayWidth<100)) {
                                $dataType .= "(".$displayWidth.")";
                            }
                        }
                    }

                    if ($property->hasAttribute("unsigned")) {
                        if ($property->getAttribute("unsigned")==true) {
                            $dataType .= " unsigned";
                        }
                    }

                    if ($property->hasAttribute("zeroFill")) {
                        if ($property->getAttribute("zeroFill")==true) {
                            $dataType .= " zerofill";
                        }
                    }

                } else if ($property->objectType=="double") {

                    $dataType = " double";

                    if ($property->hasAttribute("displayWidth")) {
                        $displayWidth = $property->getAttribute("displayWidth");

                        if (is_int($displayWidth)) {
                            if (($displayWidth>0) && ($displayWidth<100)) {
                                $dataType .= "(".$displayWidth.")";
                            }
                        }
                    }

                    if ($property->hasAttribute("unsigned")) {
                        if ($property->getAttribute("unsigned")==true) {
                            $dataType .= " unsigned";
                        }
                    }

                    if ($property->hasAttribute("zeroFill")) {
                        if ($property->getAttribute("zeroFill")==true) {
                            $dataType .= " zerofill";
                        }
                    }

                } else if ($property->objectType=="boolean") {
                    $dataType = " tinyint(1)";

                } else if ($property->objectType=="numeric") {

                } else {
                    $error = true;
                    $out .= "# ERROR for property \"{$property->name}\" type \"{$property->objectType}\" not handled";
                }

                if ($error == false) {
                    $notNull="";
                    if ($property->hasAttribute("nullAllowed")) {
                        if ($property->getAttribute("nullAllowed")==false) {
                            $notNull = " NOT NULL";
                        } else {
                            $notNull = " NULL";
                        }
                    }

                    if ($property->hasAttribute("defaultValue")) {
                        $defValue = $property->getAttribute("defaultValue");
                        $defaultValue = " default ".$defValue."";
                    } else {
                        $defaultValue="";
                    }

                    if ($property->hasAttribute("comment")) {
                        $comment = $property->getAttribute("comment");
                        // escape this string
                        $comment = $this->dbConn->escape($comment, "string");
                        $comment = " comment  ".$comment."";
                    } else {
                        $comment="";
                    }

                    $out .= "`{$property->name}`{$dataType}{$notNull}{$defaultValue}{$comment}";
                }

            } else {
                $out .= "# ERROR for property \"{$property->name}\" non scalar type \"{$property->objectType}\" not handled";
            }
            if ($term=="") $term=",\n";
        }

        // get primary key
        if (!is_null($objDef->keyId)) {

            // look up ObjDef of PK
            $pkPropDef = $objDef->getProperty($objDef->keyId);

            // TODO: is primarty key a composition?
            $out .= $term.$this->indent."PRIMARY KEY  (`".$pkPropDef->name."`)\n";

        } else {
            // no primary key? fail?
        }


        $out .= ") ENGINE=".$this->storageEngine." DEFAULT CHARSET=".$this->charSet.";\n";



        return $out;
    }
    
    
    
    // returns a SQL string on success
    // returns NULL on failure
    public function genDeleteByPK($objDef, $obj) {
        $SQL = "";

        if (is_null($objDef)) {
            return null;
        }
               
        $SQL .= "delete from ". $this->getTableName($objDef);                
        $SQL .= " where ";
        
        // use objOld for Primary key in Where clause.
        $whereClause = $this->genSQLWhereClauseFromKeys($objDef,$obj);
        if ($whereClause=="") {
            // throw error, no primary keys defined, this is a bug in the Spec Def
            return null;
        }
        
        $SQL .= $whereClause. ";";
                
        $this->setStatement($SQL);                
        
        $SQL=$this->bindParams();
                
        return $SQL;
    }
    
    
//    // for this PK the columsn equals the value
//    public function genPKC($objDef, $propDefId, $value) {
//                
//        global $log;
//
//        if (is_null($objDef)) {
//            return null;
//        }
//
//        $qs = new QueryDef();
//
//        $out="";
//        $keyProps=array();
//        $columnsList = "";
//                                
//        $whereClause = $this->genSQLWhereClauseFromKeys($objDef,$obj);
//        if ($whereClause=="") {            
//            return null;
//        }
//
//        $term="";
//        foreach($objDef->getProperties() as $key=>$property) {                   
//            if (!array_key_exists($property->name, $propIdsNotToInclude)) {
//                $columnsList .= $term."`".$property->name."`";
//                $qs->selectedProperties[] = $property;
//                if ($term=="") $term=",";
//            }
//        }
//
//        $out .= "select ";
//        $out .= $columnsList;
//        $out .= " from ". $this->getTableName($objDef) ;
//        $out .= " where ";
//        $out .= $whereClause;
//        $out .= ";";
//
//        $qs->sql = $out;
//
//        return $qs;        
//    }
    
    
}
?>
