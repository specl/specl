<?php
require_once dirname(__FILE__) ."/../../UtilLib/SearchUtils.php";
require_once dirname(__FILE__) ."/../../UtilLib/SaveDataUtils.php";

/*
 *  Handles MySQL generation from an ObjDef that describes a single primary table including constraints. This 
 *  does not handle reports that contain data from multiple tables, this requires a completly new system.
 */
class MySQLObjDefSource extends JSONDataSource {

    public $objDef;
    public $selectBuilder;
    public $rowKeys;
    public $columnNames;
    public $hideProps;

    public function __construct($params) {
        parent::__construct($params);

        $this->addAction("webservice", "getRow", "getRowJSON");
        $this->addAction("webservice", "deleteRow", "deleteRowJSON");
        $this->addAction("webservice", "updateRows", "updateRowsJSON");
        $this->addAction("webservice", "getRow2", "getRowJSON2");
        $this->addAction("webservice", "getObjByPK", "getObjByPK");
        $this->addAction("webservice", "saveDataAsCSV", "saveDataAsCSV");

        if (isset($params["hideProps"])) {
            $this->hideProps = $params["hideProps"];
        } else {
            $this->hideProps = array();
        }

        if (isset($params["objDef"]) && !is_null($params["objDef"])) {
            $this->objDef = $params["objDef"];
            $this->selectBuilder = Null;
        } else {
            $this->objDef = Null;
            //throw new Exception("The parameter 'objDef' is required");            
            // selectBuilder is required if the objDef is not specified as a parameter
        }

        if (isset($params["buildForm"])) {
            $this->buildForm = $params["buildForm"];
        } else {
            $this->buildForm = true;
        }

        if (isset($params["selectBuilder"]) && !is_null($params["selectBuilder"])) {
            $this->selectBuilder = $params["selectBuilder"];
            $this->objDef = Null;
        } else {
            $this->buildSelectStatement();

            // if an objdef is defined, build a form, to perform the data transform from the DAL into a suitable
            // form to put in the form. This is used by getObjByPK. It is done here because
            // It is faster to build the form once and not have to rebuild it every time a webservice is called.
            // This is wrong and should be changed in the future.            
            // This is only needed for add/remove collections to return nested data in the old form page
            if ($this->buildForm) {
                $this->buildFormBuilder("view");
            }
        }
    }

    public function buildSelectStatement($includeConstraints = true) {
        $joinTables = array();

        $tableName = $this->objDef->getAttribute("table", $this->objDef->name);

        $this->selectBuilder = new SelectStatementBuilder();

        //$this->selectBuilder->addTable($this->objDef->name);
        $this->selectBuilder->addTable($tableName);

        if (count($this->objDef->propertiesOrdered) > 0) {
            $props = $this->objDef->propertiesOrdered;
        } else {
            $props = $this->objDef->getProperties();
        }

        foreach ($props as $propDef) {
            // 5/29/2011 ALL
            // we need to handle PropDefs that are collections seperately
            // perhaps as Master/Detail, a grid within a grid, or a popup            
            $tableName = $propDef->getAttribute("table");

            if ($propDef->isCollection()) {
                // This section of code is used once in the LabAdminUserSpec to allow the LabAdminUsers
                // to be filtered by laboratory. Laboratory is directly associated to this table through
                // an association. This does not work on with indirect associations or tables related through
                // constraints.
                if ($propDef->isAttribute("joinOnAssociation", true)) {
                    $associationType = $propDef->getAttribute("associationType", false);
                    if ($associationType == "HasOne") {
                        $tableName = $this->objDef->getAttribute("table", $this->objDef->name);
                        $column = $propDef->getAttribute("column");
                        $referenceTable = $propDef->getAttribute("referenceTable");
                        $referenceColumn = $propDef->getAttribute("referenceColumn");
                        $this->selectBuilder->addJoin($tableName, $referenceTable, $tableName, $referenceTable, $column, $referenceColumn, "inner");
                    } else if ($associationType == "HasMany") {
                        
                    } else if ($associationType == "HasManyThrough") {
                        $tableName = $this->objDef->getAttribute("table", $this->objDef->name);
                        $column = $propDef->getAttribute("column");
                        $referenceTable = $propDef->getAttribute("throughTable");
                        $referenceColumn = $propDef->getAttribute("throughColumnFrom");                        
                        $this->selectBuilder->addJoin($tableName, $referenceTable, $tableName, $referenceTable, $column, $referenceColumn, "inner");
                    }
                }
                continue;
            }

            if (!$propDef->isScalar()) {
                continue;
            }

            if (in_array($propDef->name, $this->hideProps)) {
                continue;
            }

            if ($propDef->hasAttribute("excludeFromQuery")) {
                if ($propDef->getAttribute("excludeFromQuery") === true) {
                    continue;
                }
            }

            $label = null;
            if ($propDef->hasAttribute("caption")) {
                $label = $propDef->getAttribute("caption");
            } else {

                $label = $propDef->name;
            }

            if (!$includeConstraints || is_null($propDef->getConstraint())) {
                $this->selectBuilder->addColumn($propDef->name, $tableName, NULL, $label);
            } else {
                $constraint = $propDef->getConstraint(); //TODO: what mode?                                
                if (!isset($constraint->includeInQuery) || $constraint->includeInQuery == false) {
                    // still include the column in the query, just don't use the constraint
                    // to build another join
                    $this->selectBuilder->addColumn($propDef->name, $tableName, NULL, $label);
                } else {
                    if ($propDef->hasAttribute("nullAllowed")) {
                        $nullAllowed = $propDef->getAttribute("nullAllowed");
                        if ($nullAllowed) {
                            $joinType = "left";
                        } else {
                            $joinType = "inner";
                        }
                    } else {
                        $joinType = "inner";
                    }

                    if (array_key_exists($constraint->tableName, $joinTables)) {
                        $count = $joinTables[$constraint->tableName];
                    } else {
                        $count = 1;
                        $joinTables[$constraint->tableName] = $count;
                    }

                    if ($count > 1) {
                        $tableAlias = $constraint->tableName . $count;
                    } else {
                        // if the table already exists in the SQL statement create an alias,
                        // if not do not create an alias. This garuntees that the table will
                        // always be able to be called by its non-alaised name in the whereStatements.
                        if (array_key_exists($constraint->tableName, $this->selectBuilder->tables)) {
                            $tableAlias = $constraint->tableName . $count;
                        } else {
                            $tableAlias = $constraint->tableName;
                        }
                    }

                    $joinTables[$constraint->tableName] += 1;
                    $this->selectBuilder->addJoin($tableName, $constraint->tableName, $tableName, $tableAlias, $propDef->name, $constraint->idColumnName, $joinType);

                    if ($constraint->manyToMany) {
                        # example: 
                        #and IlluminaLibrary.laboratoryid=UserLaboratory.laboratoryid
                        
                        $clause = new SQLComparison("{$tableName}.laboratoryId","=", "{$tableAlias}.laboratoryId");
                        
                        $this->selectBuilder->whereStatement->addClause($clause);                                            
                    }                    
                    
                    
                    $column = new SQLColumn($constraint->displayColumnName, $propDef->name, $constraint->tableName, $tableAlias, False, null);
                    $this->selectBuilder->addColumn($column, Null, Null, Null);

                    
                    // keep original column in datasource, just don't display it
                    $column = new SQLColumn($constraint->idColumnName, "{$propDef->name}_orig", $constraint->tableName, $tableAlias, False, null);
                    $this->selectBuilder->addColumn($column, Null, Null, Null);
                }
            }
        }
        foreach ($this->objDef->filters->clauses as $clause) {
            $this->selectBuilder->whereStatement->addClause($clause);
        }
        
        
        
        // the joins added by the MySQLObjDefSource sometimes introduce duplicates, so get distinct rows based on the primary key.
        // One example is Browsing SeqSample as tech will cause duplicates to show if the user who added the SeqSample belongs to multiple
        // laboratories. Since MySQLObjDefSource only works for a single primary table anyway, we can solve this problem by
        // using distinct on the primaryKeys of the table.
        $this->selectBuilder->distinct=true;
        $primaryKeys=$this->objDef->getPrimaryKey();
        $keys=array();
        foreach($primaryKeys as $propId) {
            $propDef=$this->objDef->getProperty($propId);
            $keys[]="`{$this->objDef->getAttribute("table")}`.`$propDef->name`";
        }
        $keysStr=implode(",",$keys);
        $this->selectBuilder->customCountColumn="count(distinct $keysStr)";
        
    }

    public function buildFormBuilder($mode) {
        // all fields are read only in view mode
        if ($mode == "view") {
            foreach ($this->objDef->getProperties() as $propDef) {
                $propDef->setAttribute("readOnly", true);
            }
        }

        $this->fb = new InputExFormBuilder();
        $this->fb->buildObjDef($this->objDef->name);
        $this->inputExForm = $this->fb->build("CollectionForm", $mode, $this->objDef, $this->pageObj);
        $this->inputExForm->pageObj = $this->pageObj;
        $this->inputExForm->formDef = $this->objDef;
        $this->inputExForm->renderSuccessDialog = true;
        $this->inputExForm->renderCancelButton = true;
        $this->inputExForm->cancelCallback = "function(){{$this->inputExForm->id}_onCancelRedirect(); return false;}";
    }

    public function getColumnNames() {
        return $this->selectBuilder->getColumnNames();
    }

    public function getColumnNamesJS() {
        return $this->selectBuilder->getColumnNamesJS();
    }

    /** get the column names for the datasource schema
     *  
     *  The columnNames for the schema include all the column indexes from the selectbuild. 
     * 
     *  If the datasource has an objDef defined a column will also be added for each primary key
     *  named "{$columnName}_original" inorder to store original value of each primary key before
     *  the display transformation defined in the objDef.
     */
    public function getSchemaColumnNames() {
        if (!is_null($this->objDef)) {
            $columnNames = array();
            foreach ($this->objDef->primaryKey as $key => $value) {
                $propDef = $this->objDef->getProperty($key);
                $columnNames[] = "{$propDef->name}_original";
            }

            if (count($this->objDef->propertiesOrdered) > 0) {
                $props = $this->objDef->propertiesOrdered;
            } else {
                $props = $this->objDef->getProperties();
            }

            foreach ($props as $propDef) {

                if ($propDef->getAttribute("isDerived", false) === true) {
                    $columnName = $propDef->name;
                } else if ($propDef->getAttribute("excludeFromQuery", false)) {
                    continue;
                } else if ($propDef->isCollection() == true || $propDef->isScalar() == false) {
                    continue;
                } else if (in_array($propDef->name, $this->hideProps)) {
                    continue;
                } else if ($propDef->hasConstraint() === false) {
                    $table = $propDef->getAttribute("table");
                    $columnName = "{$table}_{$propDef->name}";
                } else {
                    $constraint = $propDef->getConstraint();
                    $table = $constraint->tableName;
                    $columnNames[] = "{$table}_{$propDef->name}";
                    $table = $propDef->getAttribute("table");
                    $columnName = "{$table}_{$propDef->name}";
                }
                $columnNames[] = $columnName;
            }
        } else {
            $columnNames = $this->selectBuilder->getColumnNamesJS();
        }
        return $columnNames;
    }
    
    public function getData_new($request,$mode="DataGrid"){        
        $this->processGetDataParams($request);
        $this->mapParamAttributesToSelectBuilder();
        $data=$this->executeQuery();
        $data=$this->applyPrimaryKeyTransforms($data);
        $data=$this->applyObjDefTransforms($data);
        $data=$this->applyDerivedColumnTransforms($data);
        $data=$this->applyInMemoryNaturalSortTransform($data);
        $data=$this->applyWebTransforms($data);
        $output["data"]=$data;
        $numberOfRows = $this->getRowCount();
        $output["totalRecords"] = $numberOfRows;      
        return $output;
    }
    
    public function getData2_new($request,$mode="DataGrid"){
        $this->processGetDataParams($request);
        $this->mapParamAttributesToSelectBuilder();
        $data=$this->executeQuery();
        $data=$this->applyObjDefTransforms($data);
        $data=$this->applyDerivedColumnTransforms($data);
        $data=$this->applyInMemoryNaturalSortTransform($data);
        $output["data"]=$data;
        $numberOfRows = $this->getRowCount();
        $output["totalRecords"] = $numberOfRows;    
        return $output;
    }
    
    // map the attributes set by processGetDataParams to the SQLSelectBuilder.
    // The effect of this is to vary the data based on the actions of the user.
    public function mapParamAttributesToSelectBuilder(){
        if (count($this->whereStatement->clauses) > 0) {
            foreach ($this->whereStatement->clauses as $clause) {
                $this->selectBuilder->whereStatement->addClause($clause);
            }
        }

        if ($this->limitData) {
            $this->selectBuilder->paging = true;

            $this->selectBuilder->startingRow = $this->startingRow;
            $this->selectBuilder->numberOfRowsToGet = $this->numberOfRows;
        }

        if (!is_null($this->sortCol)) {
            $this->selectBuilder->clearSortColumns();
            $this->selectBuilder->addSortColumn($this->sortCol);
            $this->selectBuilder->sortDirection = $this->sortDir;
        }
    }
    
    // build the SQL with the selectStatementBuilder, execute the query and get the data.
    public function executeQuery(){
        global $app;
        $output = array();
        $SQL = $this->selectBuilder->generateSQLSelectStatement();
        $SQL = $app->fillInGlobals($SQL);
        //  echo $SQL;
        $conn = $app->getConnection();
        $results = $conn->select($SQL);
        while ($row = mysql_fetch_assoc($results)) {
            $output[]=$row;
        }
        return $output;
    }
    
    public function applyObjDefTransforms($data){
        global $app;
        $newData=array();
        foreach($data as $row){
            foreach ($row as $key => $value) {            
                $columnObj = $this->selectBuilder->getColumn($key);
                // The *_original row values from the primary key transforms do not exist in the SQLBuilder                
                if($columnObj!==null){                
                    if (!$columnObj->isSQL) {
                        $colObjDef = $app->specManager->findDef($columnObj->tableRef);
                        // The table may not exists in the database because it is created by a join
                        if ($colObjDef !== null) {
                            $colPropDef = $colObjDef->getProperty($columnObj->columnName);
                            $transforms = $colPropDef->getTransforms();
                            if (is_array($transforms)) {
                                foreach ($transforms as $transform) {
                                    $transform->apply($row);
                                }
                            }
                        }
                    }
                }
            }
            $newData[]=$row;
        }
        return $newData;
    }
    
    // Preserve the original primary key before applying transformations. This is needed by the
    // javascript callbacks in the data grid. This transform is only applied to webstranforms.
    public function applyPrimaryKeyTransforms($data){
        if (!is_null($this->objDef)) {
            $newData=array();
            foreach ($this->objDef->primaryKey as $key => $value) {
                $propDef = $this->objDef->getProperty($key);
                $primaryKeys[] = $propDef->name;
            }
            
            foreach($data as $row){
                foreach ($row as $key => $value) {  
                    if (in_array($key, $primaryKeys)) {
                        $origKey = "{$key}_original";
                        $row[$origKey] = $value;
                    }
                }
                $newData[]=$row;
            }
        }
        else{
            $newData=$data;
        }
        return $newData;
    }
    
    // Add derived columns that are defined in the objDef
    public function applyDerivedColumnTransforms($data){
        
        if (!is_null($this->objDef)) {
            if (count($this->objDef->propertiesOrdered) > 0) {
                $props = $this->objDef->propertiesOrdered;
            } else {
                $props = $this->objDef->getProperties();
            }

            foreach ($props as $propDef) {
                $outputDataNew = array();
                if ($propDef->getAttribute("isDerived", false) === true) {
                    foreach ($data as $row) {
                        $transforms = $propDef->getTransforms();
                        if (is_array($transforms)) {

                            foreach ($transforms as $transform) {
                                $transform->apply($row);
                            }
                        }
                        $outputDataNew[] = $row;
                    }
                    $data = $outputDataNew;
                }
            }
        } 
        return $data;
    }
    
    // sorts the results in memory by natural sort order, this does not work correctly with
    // order by and LIMIT in SQL because the wrong results are returned from the database before
    // the sort order is applied. This was done because SQL does not use natural sort ordering,but
    // it does not work correctly.
    public function applyInMemoryNaturalSortTransform($data){
        if (!is_null($this->objDef)) {
            if (!is_null($this->sortCol)) {
                //$colObjDef=$app->specManager->findDef($this->sortCol->tableRef);
                //$sortPropDef=$colObjDef->getProperty($this->sortCol->columnName);

                $sortPropDef = $this->objDef->getProperty($this->sortCol->columnAlias);
                if (isnull($sortPropDef)) {
                    $sortPropDef = $this->objDef->getProperty($this->sortCol->columnName);
                }
                $sortType = $sortPropDef->objectType;

                if (is_null($this->sortDir)) {
                    $this->sortDir = "asc";
                }
                $columnName = "{$sortPropDef->name}";
                $sortColumn = new sortColumn($columnName, $sortType, $this->sortDir);
                $sortedRows = sortRows($data, $sortColumn);
                $newData= $sortedRows;
            }
        }
        if(!isset($newData)){
            $newData=$data;
        }
        return $newData;
    }
    
    
    public function applyWebTransforms($output){
        if($this->objDef!==null){
            
            if (count($this->objDef->propertiesOrdered) > 0) {
                $props = $this->objDef->propertiesOrdered;
            } else {
                $props = $this->objDef->getProperties();
            }
          
            $outputDataNew = array();
            foreach ($output as $row) {
                $newRow = array();
                foreach ($props as $propDef) {
                    // only apply transforms to propDefs that have data associated with them
                    if(!isset($row[$propDef->name])){
                        continue;
                    }
                    if ($propDef->getAttribute("isDerived", false) === false) {
                        if ($propDef->hasConstraint() === false) {
                            $table = $propDef->getAttribute("table");
                            $newColName = "{$table}_{$propDef->name}";
                        } else {
                            $constraint = $propDef->getConstraint();
                            $table = $constraint->tableName;
                            $newColName = "{$table}_{$propDef->name}";

                            $orig_table = $propDef->getAttribute("table");
                            $orig_column = "{$orig_table}_$propDef->name";
                            $row_col = "{$propDef->name}_orig";
                            $newRow[$orig_column] = $row[$row_col];
                        }

                        $newRow[$newColName] = $row[$propDef->name];
                    } else {
                        $columnName = $propDef->getAttribute("derivedName", $propDef->name);
                        $newRow[$columnName] = $row[$columnName];
                    }
                }
                foreach ($this->objDef->primaryKey as $key => $value) {
                    $propDef = $this->objDef->getProperty($key);
                    $newRow["{$propDef->name}_original"] = $row["{$propDef->name}_original"];
                }
                $outputDataNew[] = $newRow;
            }
        } else {
            $outputDataNew = array();
            foreach ($output as $oldRow) {
                $newRow = array();
                foreach ($this->selectBuilder->columns as $key => $columnObj) {
                    $newColName = "{$columnObj->tableRef}_{$key}";
                    $newRow[$newColName] = $oldRow[$key];
                }
                $outputDataNew[] = $newRow;
            }
        }
        
        return $outputDataNew;
    }
    
     public function getData($request, $mode = "DataGrid") {
        global $app;

        $this->processGetDataParams($request);

        $output = array();

        if (count($this->whereStatement->clauses) > 0) {
            foreach ($this->whereStatement->clauses as $clause) {
                $this->selectBuilder->whereStatement->addClause($clause);
            }
        }

        if ($this->limitData) {
            $this->selectBuilder->paging = true;

            $this->selectBuilder->startingRow = $this->startingRow;
            $this->selectBuilder->numberOfRowsToGet = $this->numberOfRows;
        }

        if (!is_null($this->sortCol)) {
            $this->selectBuilder->clearSortColumns();
            $this->selectBuilder->addSortColumn($this->sortCol);
            $this->selectBuilder->sortDirection = $this->sortDir;
        }

        $SQL = $this->selectBuilder->generateSQLSelectStatement();
        $SQL = $app->fillInGlobals($SQL);
        //  echo $SQL;
        $conn = $app->getConnection();
        $results = $conn->select($SQL);
        $output["data"] = array();

        $primaryKeys = array();
        if (!is_null($this->objDef)) {
            foreach ($this->objDef->primaryKey as $key => $value) {
                $propDef = $this->objDef->getProperty($key);
                $primaryKeys[] = $propDef->name;
            }
        }

        while ($row = mysql_fetch_assoc($results)) {
            $newRow = array();
            foreach ($row as $key => $value) {
                $columnObj = $this->selectBuilder->getColumn($key);
                if (!$columnObj->isSQL) {
                    // get the objDef from the columnObj tableRef in order to handle constraint columns
                    // properly. This does not work with custom objdefs, where the objDef name does not
                    // match the table name. That is because this class is designed to work with objDefs
                    // that define physical objects in the database. This problem will be solved by seperating
                    // out the transforms from the objDefs.
                    
                    $colObjDef = $app->specManager->findDef($columnObj->tableRef);
                    // The table may not exists in the database because it is created by a join
                    if ($colObjDef !== null) {
                        $colPropDef = $colObjDef->getProperty($columnObj->columnName);

                        if (in_array($key, $primaryKeys)) {
                            $origKey = "{$key}_original";
                            $row[$origKey] = $value;
                        }
                        $transforms = $colPropDef->getTransforms($mode);

                        if (is_array($transforms)) {
                            foreach ($transforms as $transform) {
                                $transform->apply($row);
                            }
                        }
                    }
                }
            }
            $output["data"][] = $row;
        }

        if (!is_null($this->objDef)) {
            // add derived columns

            if (count($this->objDef->propertiesOrdered) > 0) {
                $props = $this->objDef->propertiesOrdered;
            } else {
                $props = $this->objDef->getProperties();
            }

            foreach ($props as $propDef) {
                $table = $propDef->getAttribute("table");
                $outputDataNew = array();
                if ($propDef->getAttribute("isDerived", false) === true) {
                    foreach ($output["data"] as $row) {
                        $transforms = $propDef->getTransforms($mode);
                        if (is_array($transforms)) {

                            foreach ($transforms as $transform) {
                                $transform->apply($row);
                            }
                        }
                        $outputDataNew[] = $row;
                    }
                    $output["data"] = $outputDataNew;
                }
            }

            // convert keys to numbers in the proper order
            $outputDataNew = array();

            foreach ($output["data"] as $row) {
                $newRow = array();
//                $i=1;

                foreach ($props as $propDef) {

                    if ($propDef->isCollection() || !$propDef->isScalar()) {
                        continue;
                    }

                    if (in_array($propDef->name, $this->hideProps)) {
                        continue;
                    }

                    if ($propDef->getAttribute("excludeFromQuery", false) && !$propDef->getAttribute("isDerived", false)) {
                        continue;
                    }
                    if ($mode == "SaveData" && $propDef->getAttribute("visible", true) == false) {
                        continue;
                    }
                    if ($propDef->getAttribute("isDerived", false) === false) {
                        if ($propDef->hasConstraint() === false) {
                            $table = $propDef->getAttribute("table");
                            $newColName = "{$table}_{$propDef->name}";
                        } else {
                            $constraint = $propDef->getConstraint();
                            $table = $constraint->tableName;
                            $newColName = "{$table}_{$propDef->name}";

                            $orig_table = $propDef->getAttribute("table");
                            $orig_column = "{$orig_table}_$propDef->name";
                            $row_col = "{$propDef->name}_orig";
                            $newRow[$orig_column] = $row[$row_col];
                        }

                        $newRow[$newColName] = $row[$propDef->name];
                    } else {
                        $columnName = $propDef->getAttribute("derivedName", $propDef->name);
                        $newRow[$columnName] = $row[$columnName];
                    }
//                    $i+=1;
                }

                if ($mode == "DataGrid") {
                    foreach ($this->objDef->primaryKey as $key => $value) {
                        $propDef = $this->objDef->getProperty($key);
                        $newRow["{$propDef->name}_original"] = $row["{$propDef->name}_original"];
                    }
                }
                $outputDataNew[] = $newRow;
            }
        } else {
            $outputDataNew = array();
            foreach ($output["data"] as $oldRow) {
                $newRow = array();
                foreach ($this->selectBuilder->columns as $key => $columnObj) {
                    $newColName = "{$columnObj->tableRef}_{$key}";
                    $newRow[$newColName] = $oldRow[$key];
                }
                $outputDataNew[] = $newRow;
            }
        }

        $output["data"] = $outputDataNew;

        if (!is_null($this->objDef)) {

            if (!is_null($this->sortCol)) {
                //$colObjDef=$app->specManager->findDef($this->sortCol->tableRef);
                //$sortPropDef=$colObjDef->getProperty($this->sortCol->columnName);

                $sortPropDef = $this->objDef->getProperty($this->sortCol->columnAlias);
                if (isnull($sortPropDef)) {
                    $sortPropDef = $this->objDef->getProperty($this->sortCol->columnName);
                }
                $sortType = $sortPropDef->objectType;

                if (is_null($this->sortDir)) {
                    $this->sortDir = "asc";
                }

                if ($sortPropDef->hasConstraint() === false) {
                    $table = $sortPropDef->getAttribute("table");
                    $columnName = "{$table}_{$sortPropDef->name}";
                } else {
                    $constraint = $sortPropDef->getConstraint();
                    $table = $constraint->tableName;
                    //$columnNames[]="{$table}_{$sortPropDef->name}";   
                    //$table=$sortPropDef->getAttribute("table");
                    $columnName = "{$table}_{$sortPropDef->name}";
                }
                //$name="{$this->sortCol->tableRef}_{$this->sortCol->columnName}";

                $sortColumn = new sortColumn($columnName, $sortType, $this->sortDir);
                $sortedRows = sortRows($output["data"], $sortColumn);
                $output["data"] = $sortedRows;
            }
        }

        $numberOfRows = $this->getRowCount();
        $output["totalRecords"] = $numberOfRows;

        return $output;
    }

    public function getData2($request, $mode = "DataGrid") {
        global $app;

        $this->processGetDataParams($request);

        $output = array();

        if (count($this->whereStatement->clauses) > 0) {
            foreach ($this->whereStatement->clauses as $clause) {
                $this->selectBuilder->whereStatement->addClause($clause);
            }
        }

        if ($this->limitData) {
            $this->selectBuilder->paging = true;

            $this->selectBuilder->startingRow = $this->startingRow;
            $this->selectBuilder->numberOfRowsToGet = $this->numberOfRows;
        }

        if (!is_null($this->sortCol)) {
            $this->selectBuilder->clearSortColumns();
            $this->selectBuilder->addSortColumn($this->sortCol);
            $this->selectBuilder->sortDirection = $this->sortDir;
        }

        $SQL = $this->selectBuilder->generateSQLSelectStatement();
        $SQL = $app->fillInGlobals($SQL);

        $conn = $app->getConnection();
        $results = $conn->select($SQL);
        $output["data"] = array();

        while ($row = mysql_fetch_assoc($results)) {
            $newRow = array();
            foreach ($row as $key => $value) {
                $columnObj = $this->selectBuilder->getColumn($key);
                if (!$columnObj->isSQL) {
                    $colObjDef = $app->specManager->findDef($columnObj->tableRef);
                    // The table may not exists in the database because it is created by a join
                    if ($colObjDef !== null) {
                        $colPropDef = $colObjDef->getProperty($columnObj->columnName);
                        
                        $transforms = $colPropDef->getTransforms($mode);
                        if (is_array($transforms)) {
                            foreach ($transforms as $transform) {
                                $transform->apply($row);
                            }
                        }
                    }
                }
            }
            $output["data"][] = $row;
        }

        if (!is_null($this->objDef)) {
            if (count($this->objDef->propertiesOrdered) > 0) {
                $props = $this->objDef->propertiesOrdered;
            } else {
                $props = $this->objDef->getProperties();
            }

            foreach ($props as $propDef) {
                $outputDataNew = array();
                if ($propDef->getAttribute("isDerived", false) === true) {
                    foreach ($output["data"] as $row) {
                        $transforms = $propDef->getTransforms($mode);
                        if (is_array($transforms)) {

                            foreach ($transforms as $transform) {
                                $transform->apply($row);
                            }
                        }
                        $outputDataNew[] = $row;
                    }
                    $output["data"] = $outputDataNew;
                }
            }
        }
        
        // sorts the results in memory by natural sort order, this does not work correctly with
        // order by and LIMIT in SQL because the wrong results are returned from the database before
        // the sort order is applied.
        if (!is_null($this->objDef)) {

            if (!is_null($this->sortCol)) {
                $sortPropDef = $this->objDef->getProperty($this->sortCol->columnAlias);
                if (isnull($sortPropDef)) {
                    $sortPropDef = $this->objDef->getProperty($this->sortCol->columnName);
                }
                $sortType = $sortPropDef->objectType;

                if (is_null($this->sortDir)) {
                    $this->sortDir = "asc";
                }
                $columnName = "{$sortPropDef->name}";
                $sortColumn = new sortColumn($columnName, $sortType, $this->sortDir);
                $sortedRows = sortRows($output["data"], $sortColumn);
                $output["data"] = $sortedRows;
            }
        }

        $numberOfRows = $this->getRowCount();
        $output["totalRecords"] = $numberOfRows;

        return $output;
    }
    
    // webservice call for getRow2, formats the output as JSON
    public function getRowJSON2($request) {
        $data = $this->getRow2($request);

        $output = array();
        $output["data"] = $data;
        $json_output = json_encode($output);

        echo $json_output;
    }

    // get Object by
    public function getObjByPK($request) {
        global $app;

        if (is_null($this->objDef)) {
            throw new Exception("getObjByPK requires an objDef to be defined");
        }

        $DAL = NewDALObj($this->objDef->name);
        $keysString = $app->activeSession->getVarNoStrip("GET", "keys");
        $keysToGet = json_decode($keysString, $associative = true);
        $data = array();

        foreach ($keysToGet as $key) {
            $obj = $DAL->getOneByPk($key);
            $this->fb->applyFieldDataTransformations($this->inputExForm, $obj);
            $data[] = $obj->getArrayOfData();
        }
        $output["data"] = $data;
        $json_output = json_encode($output);
        echo $json_output;
    }

    // get the form data for a selected row. Used by the default edit button callback in the CRUDGridComponent.
    // The default edit behavior is not used on any of our pages.    
    public function getRow2($request) {
        global $app;

        $tableName = $this->objDef->getAttribute("table", $this->objDef->name);

        if (is_null($this->objDef)) {
            throw new Exception("getRow2 requires an objDef to be defined");
        }

        $this->processGetRowParams($request);

        $keys = array_keys($this->objDef->getPrimaryKey());

        $dal = NewDALObj($tableName);
        $results = $dal->getOneByPKDCC($this->rowKeys);
        // don't convert constraint column values               
        foreach ($this->objDef->getPrimaryKey() as $propKeyName) {
            $propDef = $this->objDef->getProperty($propKeyName);
            $key = "original-" . $propDef->name;
            $results->{$key} = $results->{$propDef->name};
        }

        $newResults = clone $results;

        // Apply the database to form transformations, this should really should call 
        // $this->fb->applyFieldDataTransformations($this->inputExForm, $obj), instead
        // of calling this code.
        foreach ($results as $key => $value) {
            $propDef = $this->objDef->getProperty($key);

            if (!is_null($propDef)) {
                if ($propDef->hasConstraint()) {
                    $constraint = $propDef->getConstraint();
                    $constraint->generate();
                    $lookup = $constraint->get();

                    //if ((count($lookup)>20)||($propDef->isReadOnly()==True)) {
                    if (count($lookup) > 20 || $propDef->isReadOnly("edit") == true) {
                        if (isset($results->{$key}) && !is_null($results->{$key}) && $results->{$key} != '') {
                            $value = $lookup[$results->{$key}];
                            if ($propDef->isReadOnly("edit") == true) {
                                $key = "display-{$key}";
                            }
                            $newResults->{$key} = $value;
                        }
                    }
                } else if (($propDef->objectType == "boolean") && $propDef->isReadOnly("edit") == true) {
                    if ($value == "true") {
                        $newResults->{$key} = 1;
                        $key = "display-{$key}";
                        $newResults->{$key} = $value;
                    } else {
                        $newResults->{$key} = 0;
                        $key = "display-{$key}";
                        $newResults->{$key} = $value;
                    }
                }
            }
        }

        return $newResults;
    }

    public function deleteRowJSON($request) {
        global $app;

        if ($request->hasParameter("keys")) {
            $param = $app->activeSession->getVarNoStrip("GET", "keys");
            $this->rowKeys = json_decode($param, $associative = true);
        } else {
            throw new Exception("The function 'deleteRow' requires the 'keys' parameter");
            // TODO check return error to the client
        }

        if ($this->hasCallback("onDeleteRow")) {
            // return a DALResults
            $results = $this->fireCallback("onDeleteRow", $this);
        } else {
            // get the object using the primary key(s)
            $dal = NewDALObj($this->objDef->name);

            $rowObj = $dal->getOneByPK($this->rowKeys);

            $this->objDef->checkPropertyConditions($rowObj, "delete");
            $errors = $this->objDef->validationErrors;
            $errorMsgs = array();

            $sep = "";
            foreach ($errors as $error) {
                $errorMsgs[$error->propertyName][] = $sep . $error->getErrorMessage();
                if ($sep == "")
                    $sep = "<br/>";
            }

            if (count($errors) > 0) {
                $output["messageType"] = "failure";
                $output["errorMessages"] = $errorMsgs;
                $json_output = json_encode($output);
                echo $json_output;
                return;
            } else {
                $results = $dal->deleteTransaction($this->rowKeys);
            }
        }

        // TODO check results.
        if ($results->success == false) {
            $output = array();
            $output["messageType"] = "failure";
            $output["errorMessages"] = $results->errorMessage;
            $json_output = json_encode($output);
            echo $json_output;
        } else {
            $output = array();
            $output["messageType"] = "success";
            $json_output = json_encode($output);
            echo $json_output;
        }
    }

    public function updateRowsJSON($request) {
        global $app;

        // get data from post

        $param = $app->activeSession->getVarNoStrip('POST', 'rowsToChange');
        $rowsToChange = json_decode($param, $associative = true);

        $dal = NewDALObj($this->objDef->name);

        foreach ($rowsToChange as $row) {

            $oldObj = $dal->getOneByPK($row["keys"]);
            $newObj = clone $oldObj;

            foreach ($row["dataToChange"] as $key => $value) {
                $newObj->{$key} = $value;
            }

            $results = $dal->updateOneByPK($oldObj, $newObj);

            if ($results->success == true) {
                $this->objDef->onUpdate($oldObj, $newObj);
            } else {
                // what to do?
            }
        }
    }

    // webservice call to getRow, formats the output of getRow into JSON
    public function getRowJSON($request) {
        $row = $this->getRow($request);

        $output = array();
        $output["data"] = $row;
        $json_output = json_encode($output);

        echo $json_output;
    }

    // not used, replaced by getRow2, this function was used to get the top level form data, before
    // the DAL was built.
    public function getRow($request) {

        //ActionName	getRow
        //controllerId	IlluminaLibrary_DataSource
        //keys	{"illuminaLibraryId":"7"}
        //resourceId	ViewMyLibraries

        global $app;
        $this->buildSelectStatement(false);
        $this->processGetRowParams($request);

        $rowQueryBuilder = clone $this->selectBuilder;

        foreach ($this->rowKeys as $key => $value) {
            $rowQueryBuilder->addFilter($this->objDef->name, $key, "=", $value);
        }

        $SQL = $rowQueryBuilder->generateSQLSelectStatement();

        //echo $SQL;
        $conn = $app->getConnection();
        $results = $conn->select($SQL);

        $row = mysql_fetch_assoc($results);

        foreach ($row as $key => $value) {

            $propDef = $this->objDef->getProperty($key);
            if ($propDef->objectType == "boolean") {
                if ($row[$key] == "0") {
                    $row[$key] = false;
                } else {
                    $row[$key] = true;
                }
            }
        }

        return $row;
    }

    public function processGetRowParams($request) {
        global $app;

        if ($request->hasParameter("keys")) {
            $param = $app->activeSession->getVarNoStrip("GET", "keys");
            $this->rowKeys = json_decode($param, $associative = true);
        } else {
            throw new Exception("The function 'getRows' requires the 'keys' parameter");
        }
    }

    public function getRowCount() {
        global $app;

        $SQL = $this->selectBuilder->genenerateRowCountQuery();
        $SQL = $app->fillInGlobals($SQL);

        $conn = $app->getConnection();

        $results = $conn->select($SQL);

        $row = mysql_fetch_array($results);

        $rowCount = $row[0];

        return $rowCount;
    }

    /**
     *  Only Saves the data of the columns that are visible. Includes derived columns.
     *  The ultimate goal is the have what the user sees in the GUI be exported, as is,
     *  to a csv file.
     */
    public function saveDataAsCSV($request) {
        global $app;
        $this->processGetDataParams($request);
        $this->limitData = false;
        $output = $this->getData($request, "SaveData");
        $data = $output['data'];
        $gridName = $request->getParameterValue("gridName");

        // get the state of the columns in the data grid and turn it into a key/value lookup.
        // this is needed to check whether or not a column is hidden or visible.
        $gridStateJSON = $app->userStorage->retrieve("{$this->pageObj->id}_{$gridName}_state");
        $gridState = json_decode($gridStateJSON, $assoc = true);
        $columnState = $gridState["columnState"];
        $columnStateLookup = array();
        foreach ($columnState as $column) {
            $columnStateLookup[$column["key"]] = $column;
        }

        $header = array();

        if (!is_null($this->objDef)) {
            if (count($this->objDef->propertiesOrdered) > 0) {
                $props = $this->objDef->propertiesOrdered;
            } else {
                $props = $this->objDef->getProperties();
            }

            // create headers according to the prodef
            // choose which columns to keep based of the propdef
            $columnsToKeep = array();
            foreach ($props as $propDef) {
                if ($propDef->isCollection() || !$propDef->isScalar()) {
                    continue;
                } else if (($propDef->getAttribute("excludeFromQuery", false) || ($propDef->getAttribute("visible", true)) === false) && $propDef->getAttribute("isDerived", false) === false) {
                    continue;
                } else if ($propDef->getAttribute("isDerived", false) === true) {
                    // derived columns do not have a table name associated with them so the column name
                    // in the grid is the same as the propDef name.
                    // check to see if the column is hidded, only keep the column if it is not hidden
                    $columnName = $propDef->name;
                    $columnData = $columnStateLookup[$columnName];
                    if ($columnData["hidden"] === false) {
                        $header[] = $propDef->getAttribute("caption", $propDef->name);
                        $columnsToKeep[$columnName] = $columnName;
                    }
                } else if (in_array($propDef->name, $this->hideProps)) {
                    continue;
                } else {
                    // The column name in the grid is defined based on the table where the data comes from
                    // and the name of the column in the current data grid. For constraints the data comes
                    // from another table, which is determined based on the constraint information.
                    if ($propDef->hasConstraint()) {
                        $constraint = $propDef->getConstraint();
                        $table = $constraint->tableName;
                    } else {
                        $table = $propDef->getAttribute("table");
                    }
                    $columnName = "{$table}_{$propDef->name}";

                    // check to see if the column is hidded, only keep the column if it is not hidden
                    $columnData = $columnStateLookup[$columnName];
                    if ($columnData["hidden"] === false) {
                        $header[] = $propDef->getAttribute("caption", $propDef->name);
                        $columnsToKeep[$columnName] = $columnName;
                    }
                }
            }

            $dataToExport = array();
            foreach ($data as $row) {
                $dataToExport[] = array_intersect_key($row, $columnsToKeep);
            }
        } else {
            $columnsToKeep = array();
            foreach ($this->selectBuilder->columns as $column) {
                $columnName = "{$column->tableRef}_{$column->columnName}";
                // check to see if the column is hidded, only keep the column if it is not hidden
                $columnData = $columnStateLookup[$columnName];
                if ($columnData["hidden"] === false) {
                    $header[] = $column->label;
                    $columnsToKeep[$columnName] = $columnName;
                }
            }
            $dataToExport = array();
            foreach ($data as $row) {
                $dataToExport[] = array_intersect_key($row, $columnsToKeep);
            }
        }

        $userName = $app->sessionManager->sessionObj->user;
        $pageName = $request->pageId;

        if ($this->hasCallback("getExportFilenameCallback")) {
            $this->fireCallback("getExportFilenameCallback", $this);
            $fileName = $this->fileName;
        } else {
            $fileName = "{$userName}_{$pageName}.csv";
        }

        $fileDir = "{$app->sessionUploadDir}/{$userName}";

        if (!file_exists($fileDir)) {
            mkdir($fileDir);
        }

        $filePath = "{$fileDir}/{$fileName}";

        saveDataToCSV($dataToExport, $filePath, $header);
        openDownloadFileDialog($filePath);
    }
    
    

}

?>