<?php
class CSVDataSourceComponent extends JSONDataSource {
    
    public $filePath;
    public $hasHeader;
    public $associative;

    public function __construct($params) {
        parent::__construct($params);
        
        if(isset($params["filePath"]) && !is_null($params["filePath"]))
        {
            $this->filePath=$params["filePath"];
        }
        else
        {
            $this->filePath=null;
        }
        
        if(isset($params["hasHeader"]) && !is_null($params["hasHeader"]))
        {
            $this->hasHeader=$params["hasHeader"];
        }
        else
        {
            $this->hasHeader=true;
        }
        
        if(isset($params["associative"]) && !is_null($params["associative"]))
        {
            $this->associative=$params["associative"];
        }
        else
        {
            $this->associative=false;
        } 
        
    }
    
    public function getData()
    {
        if(is_null($this->filePath))
        {
            throw new Exception("CSVDataSourceComponent->getData: filepath is empty");
        }
    
        $inputFile=fopen($this->filePath,'r');
        
        if($this->associative===true)
        {
            $columnNames=$this->getColumnNames();
        }
        // skip header if the column has one
        if($this->hasHeader)
        {
            $header=fgetcsv($inputFile);
        }
        
        $responseData=array();
        $responseData["data"]=array();
        
        $numberOfRows=0;
        
        while(($data=fgetcsv($inputFile))!==false)
        {
            $row=array();
            if($this->associative)
            {
                for($i=0;$i<count($columnNames);$i++)
                {
                    $colName=$header[$i];
                    $row[$colName]=$data[$i];
                }
            }
            else
            {
                $row[]=$data;
            }
            $responseData["data"][]=$row;
            
            $numberOfRows+=1;
        }
        
        $responseData["numberOfRows"]=$numberOfRows;
        
        return $responseData;
    }
    
    
    public function getColumnNames()
    {         
        if($this->hasHeader)
        {
            $inputFile=fopen($this->filePath,'r');
            $columnNames=fgetcsv($inputFile);
            fclose($inputFile);
            
            return $columnNames;
        }
        else
        {   
            // TODO: If file has no header return the names:
            // col1,col2,col3,col4, etc.
            return false;
        }
    }
}
?>