<?php
/* This is the base class for datasources that return results in JSON
 * format.
 * 
 */

class JSONDataSource extends DataSourceComponent{
    
    public $limitData;
    public $startingRow;
    public $numberOfRows;
    public $sortCol;
    public $sortDir;
    public $whereStatement;
    public $mode;
    
    public function __construct($params)
    {
        if(isset($params["mode"]) && !is_null($params["mode"]))
        {
            $this->mode=$params["mode"];
        } else {
            $this->mode=null;
        }
        
        parent::__construct($params);
        

        
        if(isset($params["resultsList"]) && !is_null($params["resultsList"]))
        {
            $this->resultsList=$params["resultsList"];
        } else {
            $this->resultsList="data";
        }
        
        if(isset($params["metaFields"]) && !is_null($params["metaFields"]))
        {
            $this->metaFields=$params["metaFields"];
        } else {
            $this->metaFields="{totalRecords:'totalRecords'}";
        }
        
        if(isset($params["fields"]) && !is_null($params["fields"]))
        {
            $this->fields=$params["fields"];
        } else {
            $this->fields=null;
        }
        
          
        if(isset($params["limitData"]) && !is_null($params["limitData"]))
        {
            $this->limitData=$params["limitData"];
        }
        else
        {
            $this->limitData=true;
        }
        
        if(isset($params["startingRow"]) && !is_null($params["startingRow"]))
        {
            $this->startingRow=$params["startingRow"];
        }
        else
        {
            $this->startingRow=0;
        }
        
        if(isset($params["numberOfRows"]) && !is_null($params["numberOfRows"]))
        {
            $this->numberOfRows=$params["numberOfRows"];
        }
        else
        {
            $this->numberOfRows=20;
        }
        
        if(isset($params["sortCol"]) && !is_null($params["sortCol"]))
        {
            $this->sortCol=$params["sortCol"];
        }
        else
        {
            $this->sortCol=null;
        }
        
        if(isset($params["sortDir"]) && !is_null($params["sortDir"]))
        {
            $this->sortDir=$params["sortDir"];
        }
        else
        {
            $this->sortDir=null;
        }
        
        if(isset($params["whereStatement"]) && !is_null($params["whereStatement"]))
        {
            $this->whereStatement=$params["whereStatement"];
        }
        else
        {
            $this->whereStatement=new SQLWhere();
        }
        
        $this->addAction("webservice","getData","renderJSONData");
    }
    
    public function renderJSONData($request)
    {
        $data=$this->getData($request);
        
        $JSON=json_encode($data);
        
        echo $JSON;
    }
    
    public function getColumnNames() {    
        throw new Exception("function must be implemented in child class");
    }
    
    public function getRowCount() {    
        throw new Exception("function must be implemented in child class");
    }
    
    public function renderSchema($strbld)
    {
        $strbld->addLine("{$this->id}.responseSchema={");
        $strbld->increaseIndentLevel();
        
        $schemaConfig=array();
                        
        if(is_null($this->fields))
        {
            $columnNames=$this->getSchemaColumnNames();
            $this->fields=$columnNames;
        }
        
        if(is_array($this->fields)){
            $fieldNames=array();
            foreach($this->fields as $columnName)
            {
                $fieldNames[]="'{$columnName}'";
            }
            $this->fields="[".implode(",",$fieldNames)."]";
        
        }
        
        $schemaConfig[]="fields:{$this->fields}";
        $schemaConfig[]="resultsList:'{$this->resultsList}'";
        
        if($this->metaFields!=""){
            $schemaConfig[]="metaFields:{$this->metaFields}";
        }
        
        $strbld->addList($schemaConfig);
        $strbld->decreaseIndentLevel();
        $strbld->addLine("};");   
    }
    
    public function processGetDataParams($request)
    {
        global $app;
        
        if($request->hasParameter("startingRow")){
            $this->startingRow=$request->getParameter("startingRow");
        }
        
        if($request->hasParameter("numberOfRows")){
            $this->numberOfRows=$request->getParameter("numberOfRows");
        }
        
        if($request->hasParameter("sortBy")){            
            $columnName = $request->getParameter("sortBy");           
            // lookup the column            
            $this->sortCol = $this->selectBuilder->columns[$columnName];
        }
        
        if($request->hasParameter("sortDir")){
            $this->sortDir=$request->getParameter("sortDir");
        }
        
        if($request->hasParameter("whereStatement")){
            $whereName=$request->getParameter("whereStatement");
            $whereFilters=$app->sessionStorage->retrieve($whereName,false);
            if(!is_null($whereFilters)){
                foreach($whereFilters->clauses as $whereClause){
                        $this->whereStatement->addClause($whereClause);
                    }
            }
        }   
    }
}

?>