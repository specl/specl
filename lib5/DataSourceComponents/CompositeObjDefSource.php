<?php

class CompositeObjDefSource extends JSONDataSource {   
    /*
     * Filters defined by the user in the gui.
     */
    protected $userFilters;
    public function __construct($params){
        parent::__construct($params);        
        if(isset($params["compositeDef"])){
            $this->compositeDef=$params["compositeDef"];
        }   
        if(isset($params["connection"])){
            $this->connection=$params["connection"];
        }
        else{
            $this->connection=null;
        }
        /*
         * Filters is an array of Composite Filter Objects. These filters cannot be altered by the user.
         */
        if(isset($params["filters"])){
            $this->filters=$params["filters"];
        }
        else{
            $this->filters=null;
        }
        
        /*
         * userFilters in an array of filters that is defined in the gui by the user.
         */
        $this->userFilters=null;
    }
    
    public function getData($request){
        $this->processGetDataParams($request);
        $params=array();       
        if($this->limitData){
            $params["limit"]=array("numRows"=>$this->numberOfRows, "startingRow"=>$this->startingRow);            
        }

        // All columns in the grid are named ObjectName_PropertyName, so that is the name that comes
        // back from ajax when clicking on a column in the gui.
        if (!is_null($this->sortCol)) {
            $split=explode("_", $this->sortCol);
            $params["sort"]=array(
                array("object"=>$split[0],"property"=>$split[1],"order"=>$this->sortDir)
            );
        }
        
        // copy the compositeDef as to not affect the original stored in the specManager.
        $spec=$this->compositeDef->copy();            
        
        if(!is_null($this->userFilters)){
            foreach($this->userFilters as $filterObj){
                $spec->addFilter($filterObj,$filterObj->conjunction);
            }
        }
        
        if(!is_null($this->filters)){
            foreach($this->filters as $filterObj){
                $spec->addFilter($filterObj,$filterObj->conjunction);
            }
        }
        
        $dal=new BaseDALV2(array("connection"=>$this->connection));
        $data=$dal->select($spec,$params);        
        $returnData=array("data"=>$data);
        
        /*
         * Get total count of data without paging for the paging object.
         */
        $countSpec=$spec->copy();
        $countSpec->removeAllProperties();
        $countSpec->addCountProperty();
        $countData=$dal->select($countSpec);
        $returnData["totalRecords"]=$countData[0]["count"];
        return $returnData;
    }
    
    public function getSchemaColumnNames(){
        $columnNames=array();
        foreach($this->compositeDef->getProperties() as $property){
            if(is_a($property,"CompositeProperty")){
                $columnNames[]="{$property->objectName}_{$property->propertyName}";
            }
            else if(is_a($property,"CountProperty")){
                $columnNames[]="count";
            }
        }
        return $columnNames;
    }
    
    public function processGetDataParams($request)
    {       
        global $app;
        
        if($request->hasParameter("startingRow")){
            $this->startingRow=$request->getParameter("startingRow");
        }
        
        if($request->hasParameter("numberOfRows")){
            $this->numberOfRows=$request->getParameter("numberOfRows");
        }
        
        if($request->hasParameter("sortBy")){            
            $this->sortCol = $request->getParameter("sortBy");
        }
        
        if($request->hasParameter("sortDir")){
            $this->sortDir=$request->getParameter("sortDir");
        }
        
        // get the userFilters which are store in a session variable as an array of Composite Filter Objects
        // the whereStatement parameter variable from legacy code, the rendering of the CompositeSearch should
        // be updated in the future to use the userFilter parameter name instead of whereStatement.
        if($request->hasParameter("whereStatement")){
            $whereName=$request->getParameter("whereStatement");
            $userFilters=$app->sessionStorage->retrieve($whereName,false);
            if(!is_null($userFilters)){
                $this->userFilters=$userFilters;
            }
        }   
    }
    
}

?>