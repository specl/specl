<?php
/* This is the base DataSourceComponent and is not ment to be called
 * directly.
 */
class DataSourceComponent extends Component {
 
    public function __construct($params) {
        parent::__construct($params);  

        if(isset($params["mode"]) && !is_null($params["mode"]))
        {
            $this->mode=$params["mode"];
        } else {
            $this->mode=null;
        }                        
        
        if(isset($params["source"]) && !is_null($params["source"]))
        {
            $this->source=$params["source"];
        } else {
            $this->source=null;
        }                        
        
        // json, text, xml, array
        if(isset($params["responseType"]) && !is_null($params["responseType"]))
        {
            $this->responseType=strtoupper($params["responseType"]);
        } else {
            $this->responseType="JSON";
        }
        
        if(isset($params["maxCacheEntries"]) && !is_null($params["maxCacheEntries"]))
        {
            $this->maxCacheEntries=$params["maxCacheEntries"];
        } else {
            $this->maxCacheEntries=null;
        }
        
        if(isset($params["fields"]) && !is_null($params["fields"]))
        {
            $this->fields=$params["fields"];
        } else {
            $this->fields=null;
        }

        if(isset($params["isMemory"]) && !is_null($params["isMemory"]))
        {
            $this->isMemory=$params["isMemory"];
        } else {
            $this->isMemory=False;
        }
        
        if (is_null($this->source)) {
            if (isset($this->pageObj)) {
                $this->source = "index.php?pageId={$this->pageObj->id}&resourceId={$this->pageObj->page->resourceId}&controllerId={$this->id}&actionName=getData&buildPage=false";            
            
                if (is_null($this->mode)===False) {            
                    $this->source .= "&mode=".$this->mode;
                } 
            }
        }
        
        }
    
    /* This function returns data as a php array.
     * 
     */
    public function getData() {    
        throw new Exception("function must be implemented in child class");
    }
    
    public function renderSchema() {
         throw new Exception("function must be implemented in child class");
    }
        
    public function render($strbld)
    {
        $this->renderHTML($strbld);
        $this->renderJavascript($strbld);
    }
    
    public function renderHTML($strbld){
        
    }
    
    public function renderJavascript($strbld){
          
        $strbld->addLine("<script type='text/javascript'>");
        
        $strbld->increaseIndentLevel();
        
        if ($this->isMemory==False) {
            $strbld->addLine("var {$this->id}= new YAHOO.util.DataSource('{$this->source}');");
        } else {
            $strbld->addLine("var {$this->id}= new YAHOO.util.DataSource({$this->source});");
        }
        
        $strbld->addLine("{$this->id}.responseType=YAHOO.util.DataSource.TYPE_{$this->responseType};");
      
   
    
        if (!is_null($this->maxCacheEntries)) {            
            $strbld->addLine("{$this->id}.maxCacheEntries = {$this->maxCacheEntries};");
        }            
        
        $this->renderSchema($strbld);
        $strbld->decreaseIndentLevel();
        
        $strbld->addLine("</script>");
    }
    
}

?>