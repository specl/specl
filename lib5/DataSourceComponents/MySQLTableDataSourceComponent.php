<?php

// TODO: Seperate datasource based on a MySQL query.

/* TODO: - Query with constraints
 *       - Edit
 *       - Add
 *       - Delete
 */
           

class MySQLTableDataSourceComponent extends JSONDataSource {
    
    public $tableName;
    public $associative;
    public $columnNames;
    
   
    public function __construct($params)
    {
        parent:: __construct($params);
     
        if(isset($params["tableName"]) && !is_null($params["tableName"]))
        {
            $this->tableName=$params["tableName"];
            
        }
        else
        {
             throw new Exception("The parameter 'tableName' is required");
        }
        
        if(isset($params["associative"]) && !is_null($params["associative"]))
        {
            $this->associative=$params["associative"];
        }
        else
        {
            $this->associative=false;
        }
        
        $this->columnNames=$this->getColumnNames();
    }
    
    public function getColumnNames()
    {
        global $app;
        
        $conn = $app->getConnection();        
        
        $schemaName=$conn->schema;
        $tableName=mysql_escape_string($this->tableName);
        $SQL="SELECT COLUMN_NAME FROM information_schema.`COLUMNS` C where TABLE_SCHEMA='{$schemaName}' and TABLE_NAME='{$tableName}';";
        
        $conn = $app->getConnection();        
        $results=$conn->select($SQL);
        
        $colNames=array();
        
        while($row=mysql_fetch_array($results))
        {
            $colNames[]=$row[0];
        }
        
        return $colNames;
        
    }
    
    public function getData()
    {
        global $app;
       
        $this->processGetDataParams();
        
        $output=array();
       
        $SQLBuilder=new SelectStatementBuilder();
        
        if($this->limitData)
        {
            $SQLBuilder->paging=true;
            
            $SQLBuilder->startingRow=$this->startingRow;
            $SQLBuilder->numberOfRowsToGet=$this->numberOfRows;
        }
        
        if(!is_null($this->sortCol))
        {
            $SQLBuilder->addSortColumn($this->sortCol);
            $SQLBuilder->sortDirection=$this->sortDir;
        }
                
        
        foreach($this->columnNames as $colName)
        {
            $SQLBuilder->addColumn($colName);
        }
        
        $SQLBuilder->addTable($this->tableName);
        
        $SQL=$SQLBuilder->generateSQLSelectStatement();
        
        $conn = $app->getConnection();        
        $results=$conn->select($SQL);
        
        $output["data"]=array();
        
        while($row=mysql_fetch_assoc($results))
        {
            $output["data"][]=$row;
        }
        
        $numberOfRows=$this->getRowCount();
       
        $output["totalRecords"]=$numberOfRows;
        
        return $output;
    }
    
    public function getRowCount()
    {
        global $app;
        
        $tableName=mysql_escape_string($this->tableName);
        $SQL="SELECT count(*) from `$tableName`;";
        
        $conn = $app->getConnection();        
        $results=$conn->select($SQL);
        
        $row=mysql_fetch_array($results);
        
        $rowCount=$row[0];
        
        return $rowCount;
    }
    
}

?>