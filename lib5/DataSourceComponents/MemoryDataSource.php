<?php

class MemoryDataSource extends JSONDataSource {
 
    public $columnNames;
    public $data;
    public $associative;    
      
    public function __construct($params) {
        parent::__construct($params);
        
        if(isset($params["associative"]) && !is_null($params["associative"]))
        {
            $this->associative=$params["associative"];
        }
        else
        {
            $this->associative=false;
        } 
        
        $this->columnNames = array();
        $this->data = array();        
    }    
    
    public function addColumnName($columnName) {        
        $this->columnNames[] = $columnName;   
    }

    public function addDataRow($row) { 
        $this->data[] = $row;
    }
    
    public function getColumnNames()
    {         
        return $this->columnNames;
    }
    
    public function getData() {   
        if(is_null($this->filePath))
        {
            throw new Exception("MemoryDataSource->getData: filepath is empty");
        }
    
        $inputFile=fopen($this->filePath,'r');
        
        if($this->associative===true)
        {
            $columnNames=$this->getColumnNames();
        }
        
        $responseData=array();
        $responseData["data"]=$this->data;        
        $responseData["numberOfRows"]=count($this->data);
        
        return $responseData;                
    }
}

?>