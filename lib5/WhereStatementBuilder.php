<?php

class WhereStatementBuilder extends Process {
    private $viewAs=null;
    private $hideControls=false;
    private $showOnlyControls=false;
    private $statusValue=null;
    private $statusColumn=null;
    private $objDef=null;
    private $whereStatement;
    
    // TODO: make this spec driven
    public function processParameters($params){
        if(isset($params["objDef"])){
            $this->objDef=$params["objDef"];
        }
        
        if(isset($params["viewAs"])){
            $this->viewAs=$params["viewAs"];
        }
        
        if(isset($params["hideControls"])){
            $this->hideControls=$params["hideControls"];
        }
        
        if(isset($params["showOnlyControls"])){
            $this->showOnlyControls=$params["showOnlyControls"];
        }
        
        if(isset($params["statusValue"])){
            $this->statusValue=$params["statusValue"];
        }
        
        if(isset($params["statusColumn"])){
            $this->statusColumn=$params["statusColumn"];
        }        
    }
    
    // TODO: make this a seperate process driven by a spec def
    public function processPageParameters($page){
        global $app;
        $params=array();
        //$params["viewAs"]=$page->getParam("viewAs",$page->acr);
        $params["hideControls"]=StrToBool($page->getParam("hideControls"),"false");
        $params["showOnlyControls"]=StrToBool($page->getParam("showOnlyControls"),"false");
        $params["statusValue"]=$page->getParam("statusValue",null);
        $params["statusColumn"]=$page->getParam("statusColumn",null);
        $params["objDef"]=$app->specManager->findDef($page->getParam("objDef",null));
        $this->processParameters($params);
    }
    
    public function execute(){
        global $app;
        if(is_null($this->objDef)){
            throw(new Exception("The parameter 'objDef' is required to build a wherestatement"));
        }
        $this->whereStatement=new SQLWhere();
                
        // techs can only view data from their own lab
        if($this->viewAs=="tech"){
            // laboratoryId may not be in the same table
            $labPropDef=$this->objDef->getProperty("laboratoryId");
            if(!is_null($labPropDef)){
                $labTableName=$labPropDef->getAttribute("table","{$this->objDef->name}");            
                $this->whereStatement->addClause(new SQLComparison("{$labTableName}.laboratoryId","=","'{$app->acManager->userObj->defaultLaboratory}'"));
            }
        }
        
        if($this->hideControls===true){
            // laboratoryId may not be in the same table
            $isControlPropDef=$this->objDef->getProperty("isControl");
            if(!is_null($isControlPropDef)){
                $isControlTableName=$isControlPropDef->getAttribute("table","{$this->objDef->name}");            
                $this->whereStatement->addClause(new SQLComparison("{$isControlTableName}.isControl","=","0"));
            }
        }
        
        if($this->showOnlyControls===true){
            // laboratoryId may not be in the same table
            $isControlPropDef=$this->objDef->getProperty("isControl");
            if(!is_null($isControlPropDef)){
                $isControlTableName=$isControlPropDef->getAttribute("table","{$this->objDef->name}");            
                $this->whereStatement->addClause(new SQLComparison("{$isControlTableName}.isControl","=","1"));
            }
        }
        
        if(!is_null($this->statusValue)){
            $statusProp=$this->objDef->getProperty($this->statusColumn);
            if(!is_null($statusProp)){
                $statusTableName=$statusProp->getAttribute("table","{$this->objDef->name}");
                $this->whereStatement->addClause(new SQLComparison("{$statusTableName}.{$this->statusColumn}","=","'{$this->statusValue}'"));
            }
        }
        
        return $this->whereStatement;
    }
    
    // alias to exectute
    public function buildWhereStatement(){
        return $this->execute();
    }
}

?>