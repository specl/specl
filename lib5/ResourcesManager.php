<?php
/**
 *
 * @author Tony
 */
class ResourcesManager {

    public $resources;
    public $resourceObjs;
    public $specManager;
    
    public function  __construct($specManager) {
        $this->specManager = $specManager;
        $this->resources=array();
        $this->resourceObjs=array();
    }
        
    public function  loadFromXML() {        
        global $app;
        
        $tmp = $this->specManager->getObjectFromXML($app->dbPath."Resources.xml", "Resources");
        //krumo($tmp);
        //TODO:Error checking
        $this->resources=$tmp;        
    }

    public function doesResourceExist($resourceId) {

        if (array_key_exists($resourceId,$this->resources)) {
            return true;
        } else {
            return false;
        }
    }

    public function getResource($resourceId) {
        
        if (array_key_exists($resourceId,$this->resourceObjs)) {
            return $this->resourceObjs[$resourceId];
        } else {
            $resourceObj = $this->loadPage($resourceId);            
            $this->resourceObjs[$resourceId] = $resourceObj;
            return $resourceObj;
        }
    }

    public function loadResource($resourceId) {
        global $app;
        //TODO:Error checking, add try catch block
        $filename = $app->dbPath."resources/{$resourceId}.xml";
        $resourceObj = $this->specManager->getObjectFromXML($filename, "Resource");
        //$resourceObj->id = $resourceId;
        $resourceObj->isLoaded = true;
        return $resourceObj;
    }

}
?>
