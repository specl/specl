<?php

class Hash implements Iterator {
    
    private $keys;    
    private $position = 0;
    private $ordered;    
    
    public function __construct() {    
        $this->keys = Array();
        $this->ordered = Array();
        $this->position = 0;
    }
    
    public function add($key,$value) {    
        if (!array_key_exists($key, $this->keys)) {
            $this->keys[$key] = $value;            
            $this->ordered[] = $key;            
        }
    }
    
    public function get($key) {    
        if (array_key_exists($key, $this->keys)) {
            return $this->keys[$key];
        } else {
            return Null;
        }
    }

    public function exists($key) {    
        if (array_key_exists($key, $this->keys)) {
            return True;
        } else {
            return False;
        }
    }
    
    function rewind() {
        //var_dump(__METHOD__);
        $this->position = 0;
    }

    function current() {
        //var_dump(__METHOD__);
        
        $key = $this->ordered[$this->position];
        $value = $this->keys[$key];
        return $value;
    }

    function key() {
        //var_dump(__METHOD__);
        
        $key = $this->ordered[$this->position];
        
        return $key;
    }

    function next() {
        //var_dump(__METHOD__);
        ++$this->position;
    }

    function valid() {
        //var_dump(__METHOD__);
        return isset($this->ordered[$this->position]);
    }
    
}

?>
