<?php
class GraphNode{
    public $name;
    public $edges;
    public $data;
    
    public function __construct($name,$data=array()){
        $this->name=$name;
        $this->edges=array();
        $this->data=$data;
    }
    
}

class GraphEdge{    
    public $startNode;
    public $endNode;
    public $data;
    
    public function __construct($startNode,$endNode,$data=array()){
        $this->startNode=$startNode;
        $this->endNode=$endNode;
        $this->data=$data;
    }
}

// A simple graph with a list of nodes: each node must have a unique name
// could be made more sophisticate later, if necessary
class GraphStructure {
    public $nodes;
    
    public function __construct(){
        $this->nodes=array();
    }
    
    public function addNode($name){
        $this->nodes[$name]=new GraphNode($name);
        return $this->nodes[$name];
    }
    
    public function getNode($name){
        if(isset($this->nodes[$name])){
            return $this->nodes[$name];
        }
        else{
            return null;
        }
    }
    
    public function getEdge($node1Name,$node2Name){
        $node1=$this->getNode($node1Name);
        if($node1===null){
            return null;
        }
        foreach($node1->edges as $edge){
            $endNode=$edge->endNode;
            if($endNode->name==$node2Name){
                return $edge;
            }
        }
        return null;
    }
    
    public function addUndirectedEdge($startNodeName,$endNodeName,$data=array()){
        $this->addEdge($startNodeName, $endNodeName, $undirected=true,$data);
    }
    
    public function addDirectedEdge($startNodeName,$endNodeName,$data=array()){
        $this->addEdge($startNodeName, $endNodeName, $undirected=false,$data);
    }
    
    public function addEdge($startNodeName,$endNodeName,$undirected=true,$data=array()){
        if(!isset($this->nodes[$startNodeName])){
            throw new Exception("The start node '{$startNodeName}' does not exist in the graph");
        }
        
        if(!isset($this->nodes[$endNodeName])){
            throw new Exception("The start node '{$endNodeName}' does not exist in the graph");
        }
        $startNode=$this->nodes[$startNodeName];
        $endNode=$this->nodes[$endNodeName];
        
        $newEdge=new GraphEdge($startNode,$endNode,$data);
        $startNode->edges[]=$newEdge;
        
        // if the graph is undirected represent as two directed edges
        if($undirected===true){
            $startNode=$this->nodes[$endNodeName];
            $endNode=$this->nodes[$startNodeName];

            $newEdge=new GraphEdge($startNode,$endNode,$data);
            $startNode->edges[]=$newEdge;
        }        
        return $newEdge;
    }
        
    // Find the shortest path between two points of an unweigthed using breadth first search and
    // Dijkstra algorithm.
    // returns a list of nodes with the shortest path from the start to the end node
    // The edgeType variable gets the shortestpath only using the specified edge types
    public function getShortestPath($startNodeName,$endNodeName,$edgeTypes=array()){
        // Initialize two arrays with an entry for each node in the graph:
        //      - distance: stores the distance from the source to the node (initial value -1, representing infinity)
        //      - previous: stores the name of the previous node in the path (initial value null)
        
        $distance=array();
        $previous=array();
        foreach($this->nodes as $node){
            $distance[$node->name]=-1;
            $previous[$node->name]=null;
        }
        
        // Initialize a queue that will contain all unvisited nodes, using a queue ensures we visit every node only once.        
        // A PHP array can be used as a queue:
        //     - To have an array emulate a queue use array_push() to enqueue and array_shift() to deque
        
        $queue=array();
        
        // initialize the distance of the source node to 0 and add it to the queue
        $distance[$startNodeName]=0;
        array_push($queue, $this->nodes[$startNodeName]);
        
//        While the queue is not empty:
//            - deque the first value of the front of the queue 
//            - foreach node adjacent to the dequed node:
//                - if the dist= -1 (the node has not been visited)
//                    - set the dist of the adjacent node to the distance of the current node + 1
//                    - set previous of the adjacent node to the current node
//                    - add the adjacent node to the queue        
        while(!empty($queue)){
            $currentNode=array_shift($queue);            
            
            // when the currentNode equals the target node stop
            if($currentNode->name==$endNodeName){
                break;
            }
            
            foreach($currentNode->edges as $edge){
                if(in_array($edge->data["associationType"],$edgeTypes) || empty($edgeTypes)){
                    $adjacentNode=$edge->endNode;
                    if($distance[$adjacentNode->name]==-1){
                        $distance[$adjacentNode->name]=$distance[$currentNode->name]+1;
                        $previous[$adjacentNode->name]=$currentNode;
                        array_push($queue, $adjacentNode);
                    }
                }
            }
        }
        
        //  Get the shortest path by following the path variable back, until you reach the source node, whose previous node value is null
        $shortestPath=array();      
        $currentNodeName=$endNodeName;
        while(!is_null($previous[$currentNodeName])){
              // add node name to beginning to the array
              array_unshift($shortestPath,$currentNodeName);
              $currentNode=$previous[$currentNodeName];
              $currentNodeName=$currentNode->name;
        }
        
        array_unshift($shortestPath,$startNodeName);
        
        // if the last value of the path does not equal the endNode then a path between the two nodes does not exist
        if($shortestPath[count($shortestPath)-1]!==$endNodeName)
        {
            return null;
        }
        else{        
            return $shortestPath;
        }
        
    }
    
}

?>