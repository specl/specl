<?php 
function saveDataToCSV($rows,$filename,$headerRow=null){
    if(is_null($headerRow)){
        $headerRow=array_keys($rows[0]);
    }
    
    $file=fopen($filename,'w');
    fputcsv($file,$headerRow);
    
    foreach($rows as $row){
        fputcsv($file,$row);
    }
    
    fclose($file);
}

function openDownloadFileDialog($filePath,$fileName=null){
    if(is_null($fileName)){
        $fileName=basename($filePath);
    }
    
    if(file_exists($filePath)){
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$fileName);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filePath));
        ob_clean();
        flush();
        readfile($filePath);
        exit;
    }
}

?>