<?php
/*
 *  Given an objDef and to objects this function returns the difference between two objects. This
 *  function only tests top level properties it does not test nested objects or collections.
 * 
 *  Only properties that are defined in the objDef are compared, the others are ignored.
 * 
 *  The differences between the two objects are returned as a key/value pair array, where the
 *  the name of the property that is different is the key and the value is a boolean true which
 *  acts as a place holder. It does not return the actual value that is different.
 */
function compareAToB($objDef, $objA, $objB) {
    $different=array();

    foreach($objDef->getProperties() as $propDef) {
        $valueA = null;
        $valueB = null;

        // skip collections....

        if (($propDef->isCollection() == true)  || !$propDef->isScalar()) {
            continue;
        }

        if (isset($objA[$propDef->name])) {
            $valueA=$objA[$propDef->name];
        }

        if (isset($objB[$propDef->name])) {
            $valueB=$objB[$propDef->name];
        }

        if (is_blank($valueA)==true) {
            $valueA=null;
        }

        if (is_blank($valueB)==true) {
            $valueB=null;
        }

        if (!($valueA===$valueB)) {
            // only add to different are if the values actually are different
            $different[$propDef->name] = true;
        } 
    }

    return $different;
}

?>