<?php
/**
 * Description of FilePathManager
 *
 * @author Tony
 */
class FilePathManager {

    private $paths;
        
    public function __construct() {
        $this->paths = array();       
    }
    
    public function addPath($path, $location=ADDTOP) {
        global $classPaths;

        $path = trim($path);

        $l = strlen($path)-1;

        if (($path[$l]!="\\")&&($path[$l]!='/')) {
            $path .=  DIRECTORY_SEPARATOR;
        } 

        $tmp = array();
        $tmp[]=$path;
        if ($location==ADDTOP) {
            $this->paths = array_merge($tmp,$this->paths);        
        } else {
            $this->paths = array_merge($this->paths,$tmp);        
        }
    } 
    
    function search($filename) {
    
        foreach($this->paths as $path) {
            $searcName = $path . $filename;
            if (file_exists($searcName)) {
                return true;
            }
        }
        return false;
    }
    
    function getFullPath($filename) {
    
        foreach($this->paths as $path) {
            $searchName = $path . $filename;
            if (file_exists($searchName)) {
                return $searchName;
            }
        }
        return Null;
    }

    
    /* return a list of all files that match a pattern, in all paths */    
    function findFilesMatchingPattern($pattern) {    
        $files = array();        
        foreach($this->paths as $path) {            
            
            foreach (glob($path . $pattern) as $filename) {
                $files[] = realpath($filename);
            }
        }
        return $files;
    }
}
?>
