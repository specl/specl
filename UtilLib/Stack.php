<?php

class Stack {

    protected $_stack;
    protected $_top;

    public function __construct() {
        $this->_stack = array();        
    }

    public function depth() {
        return count($this->_stack);
    }
    
    public function top() {
        $cnt = count($this->_stack);

        if ($cnt > 0) {
            return $this->_stack[$cnt - 1];
        } else {
            return null;
        }
    }

    public function get() {
        return  $this->_stack;
    }
    
    public function oneBackFromTop() {
        $cnt = count($this->_stack);

        if ($cnt > 1) {
            return $this->_stack[$cnt - 2];
        } else {
            return null;
        }
    }

    public function push($obj) {
        array_push($this->_stack, $obj);        
    }

    public function pop() {
        array_pop($this->_stack);
    }

    public function non_destructive_pop() {
        array_pop($this->_stack);
    }

    
    public function clear() {
        unset($this->_stack);        
        $this->_stack = array();
    }    
    
    public function getReverse() {
        $cnt = count($this->_stack);
        
        $objs = array();
        
        $i = $cnt-1;
        while($i > -1) {
            $objs[] = $this->_stack[$i];
            $i--;
        }
                
        return $objs;
    }
}
?>
