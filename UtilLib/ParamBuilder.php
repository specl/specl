<?php
/**
 * Description of ParamBuilder
 *
 * @author Tony
 */
class ParamBuilder {
    public $params;
    
    public function __construct() {
        $this->params = array();
    }
    
    public function add($key, $value) {
        $this->params[$key] = $value;
    }
    
    public function getParams() {
        return $this->params;
    }    
}
?>
