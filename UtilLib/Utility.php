<?php

function deepCopy($object) {
    return unserialize(serialize($object));
}

function BoolToStr($in) {
    $out = "false";
    if ($in != null) {
        if (is_bool($in)) {
            if ($in == true)
                return 'true';
            else
                return 'false';
        }
    }
    return $out;
}

function StrToBool($in) {
    $in = strtolower($in);
    if ($in == "true") {
        return true;
    } else {
        return false;
    }
}

function varDumpToString($var) {
    ob_start();
    var_dump($var);
    $result = ob_get_clean();
    return $result;
}

function generateUID() {
    return uniqid();
}

// return current date and time as a string
function getCurrentDateTime() {
    //$today = date("Y-m-d h:i:s.u");
    $today = date("Y-m-d h:i:s");
    return $today;
}

function mysql2php($propDef, $input) {

    $dataType = $propDef->objectType;

    if($input==='SNULL'){
        $input=null;
    }
                    
    if (is_null($input)) {
        $value = null;
    } else {
        if ($dataType == "string") {
            $value = $input;
        } else if ($dataType == "boolean") {
            if ($input == "1") {
                $value = true;
            } else {
                $value = false;
            }
        } else if ($dataType == "date") {
            $ts = strtotime($input);

            if ($propDef->hasAttribute("valueFormat")) {
                $valueFormat = $propDef->getAttribute("valueFormat", null);
            } else {
                $valueFormat = 'Y-m-d';
            }

            if ($ts === false) {
                //invalid date
                $value = $input;
            } else {
                $value = date($valueFormat, strtotime($input));
            }
        } else if ($dataType == "datetime") {

            $ts = strtotime($input);

            if ($propDef->hasAttribute("valueFormat")) {
                $valueFormat = $propDef->getAttribute("valueFormat", null);
            } else {
                $valueFormat = 'Y-m-d G:i:s';
            }

            if ($ts === false) {
                //invalid date
                $value = $input;
            } else {
                $value = date($valueFormat, strtotime($input));
            }
        } else if ($dataType == "integer") {
            
            $value = intval($input);
            
        } else if ($dataType == "float") {
            $value = floatval($input);
        } else if ($dataType == "double") {
            $value = floatval($input);
        } else {
            $value = $input;
        }
    }
    return $value;
}

function javascript2php($propDef, $input) {

    $dataType = $propDef->objectType;
    if (is_string($input)) {
        $input = trim($input);
    }

    if (isnull($input) || $input === "SNULL" || $input === "") {
        if ($dataType != "boolean") {
            $value = null;
        } else {
            $value = false;
        }
    } else {
        if ($dataType == "string") {
            $value = $input;
        } else if ($dataType == "boolean") {
            if (is_numeric($input)) {
                if ($input == 0) {
                    $value = false;
                } else if ($input == 1) {
                    $value = true;
                } else {  // default is false!
                    $value = false;
                }
            } else if (is_bool($input)) {
                $value = $input;
            } else if (is_string($input)) {
                if (($input == "1") || ($input == "true")) {
                    $value = true;
                } else {
                    $value = false;
                }
            } else {
                $value = false;
            }
        } else if ($dataType == "date") {
            if ($propDef->hasAttribute("valueFormat")) {
                $valueFormat = $propDef->getAttribute("valueFormat", null);
            } else {
                $valueFormat = 'Y-m-d';
            }
            $value = date($valueFormat, strtotime($input));
        } else if ($dataType == "datetime") {
            if ($propDef->hasAttribute("valueFormat")) {
                $valueFormat = $propDef->getAttribute("valueFormat", null);
            } else {
                $valueFormat = 'Y-m-d G:i:s';
            }
            $value = date($valueFormat, strtotime($input));
        } else if ($dataType == "integer" && is_numeric($input)) {
            $value = intval($input);
        } else if ($dataType == "float" && is_numeric($input)) {
            $value = floatval($input);
        } else if ($dataType == "double" && is_numeric($input)) {
            $value = floatval($input);
        } else {
            $value = $input;
        }
    }
    return $value;
}

function value2php($propDef, $input) {

    $dataType = $propDef->objectType;
    if (is_string($input)) {
        $input = trim($input);
    }

    if (isnull($input) || $input === "SNULL" || $input === "") {
        if ($dataType != "boolean") {
            $value = null;
        } else {
            $value = false;
        }
    } else {
        if ($dataType == "string") {
            $value = $input;
        } else if ($dataType == "boolean") {
            if (($input == "1") || ($input == "true") || ($input === true)) {
                $value = "true";
            } else {
                $value = "false";
            }
        } else if ($dataType == "date") {
            if ($propDef->hasAttribute("valueFormat")) {
                $valueFormat = $propDef->getAttribute("valueFormat", null);
            } else {
                $valueFormat = 'Y-m-d';
            }
            $value = date($valueFormat, strtotime($input));
        } else if ($dataType == "datetime") {
            if ($propDef->hasAttribute("valueFormat")) {
                $valueFormat = $propDef->getAttribute("valueFormat", null);
            } else {
                $valueFormat = 'Y-m-d G:i:s';
            }
            $value = date($valueFormat, strtotime($input));
        } else if ($dataType == "integer" && is_numeric($input)) {
            $value = intval($input);
        } else if ($dataType == "float" && is_numeric($input)) {
            $value = floatval($input);
        } else if ($dataType == "double" && is_numeric($input)) {
            $value = floatval($input);
        } else {
            $value = $input;
        }
    }
    return $value;
}

function is_blank($value) {
    return empty($value) && !is_numeric($value) && !is_bool($value);
}

function isnull($value) {
    if (is_null($value)) {
        return true;
    }

    if (is_string($value)) {
        if (strlen(trim($value)) == 0) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function mailto($from, $to, $subject, $message, $headers = '') {
    //get_settings('charset')
    $charset = 'utf-8';
    if ($headers == '') {
        $headers = "MIME-Version: 1.0\n" .
                "From: " . $from . "\n" .
                "Content-Type: text/html; charset=\"" . $charset . "\"\n";
    }

    return @mail($to, $subject, $message, $headers);
}

//http://www.webcheatsheet.com/PHP/get_current_page_url.php
function curBaseURL() {

    $pageURL = 'http';

    if (isset($_SERVER["HTTPS"])) {
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
    }

    $pageURL .= "://";

    $folder = substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], "/") + 1);

    if ($_SERVER["SERVER_PORT"] != "80") {
        //$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $folder;
    } else {
        //$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        $pageURL .= $_SERVER["SERVER_NAME"] . $folder;
    }

    return $pageURL;
}

//function is_assoc($arr) {
//    return (is_array($arr) && count(array_filter(array_keys($arr), 'is_string')) == count($arr));
//}

function is_assoc($a){
   $a = array_keys($a);
   return ($a != array_keys($a));
}

// perpares a string for inclusion in JavaScript
function cleanforJS($str) {
    $str = json_encode($str);
    //$str = str_replace("\n", "\\n", $str);           
    return $str;
}

/// locate or create element by $path and set its value to $value
/// $path is either an array of keys, or a delimited string
function array_set(&$a, $path, $value) {
    if (!is_array($path))
        $path = explode($path[0], substr($path, 1));
    $subarray = &$a;
    // do not create an array from the last path
    $lastKey = array_pop($path);
    foreach ($path as $key) {
        if (!isset($subarray[$key])) {
            $subarray[$key] = array();
        }
        $subarray = &$subarray[$key];
    }

    $subarray[$lastKey] = $value;
}

function strStartsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function joinPaths() {
    $args = func_get_args();
    $paths = array();
    foreach ($args as $arg)
        $paths = array_merge($paths, (array) $arg);

    $paths2 = array();
    foreach ($paths as $i => $path) {
        $path = trim($path, DIRECTORY_SEPARATOR);
        if (strlen($path))
            $paths2[] = $path;
    }
    $result = join(DIRECTORY_SEPARATOR, $paths2); // If first element of old path was absolute, make this one absolute also
    if (strlen($paths[0]) && substr($paths[0], 0, 1) == DIRECTORY_SEPARATOR)
        return DIRECTORY_SEPARATOR . $result;
    return $result;
}

/**
 * Takes one or more file names and combines them, using the correct path separator for the 
 *         current platform and then return the result.
 * Example: joinPath('/var','www/html/','try.php'); // returns '/var/www/html/try.php'
 * Link: http://www.bin-co.com/php//scripts/filesystem/join_path/
 */
function joinPath() {
    $path = '';
    $arguments = func_get_args();
    $args = array();
    foreach ($arguments as $a)
        if ($a !== '')
            $args[] = $a; //Removes the empty elements

        $arg_count = count($args);
    for ($i = 0; $i < $arg_count; $i++) {
        $folder = $args[$i];

        if ($i != 0 and $folder[0] == DIRECTORY_SEPARATOR)
            $folder = substr($folder, 1); //Remove the first char if it is a '/' - and its not in the first argument
        if ($i != $arg_count - 1 and substr($folder, -1) == DIRECTORY_SEPARATOR)
            $folder = substr($folder, 0, -1); //Remove the last char - if its not in the last argument

        $path .= $folder;
        if ($i != $arg_count - 1)
            $path .= DIRECTORY_SEPARATOR; //Add the '/' if its not the last element.
    }
    return $path;
}

/* only accepts numeric, bool, string,
 * everything else returns default value
 * 
 * only acceptable numeric balues are 1, 0
 * 
 * only acceptable string values are TRUE and FALSE, in any case
 * 
 */

function ConvertToBool($value, $defaultValue) {

    if (isnull($value)) {
        return $defaultValue;
    }

    if (is_bool($value)) {
        return $value;
    }

    if (is_numeric($value)) {
        if ($value == 1) {
            return true;
        } else if ($value == 0) {
            return false;
        } else {
            return $defaultValue;
        }
    }

    if (is_string($value)) {
        $in = strtolower($in);

        if ($in == "true") {
            return true;
        } else if ($in == "false") {
            return false;
        } else {
            return $defaultValue;
        }
    }

    return $defaultValue;
}

/*
 * Takes a PHP value and converts it to a MySQL string.
 */
function ConvertValueToMySQL($paramValue,$paramType) {        
        $subValue=null;

        if (is_blank($paramValue))  {
            $paramValue=null;
        }                

        if ($paramType=="string") {
            if (is_null($paramValue)) {
                $subValue="NULL";
            } else if (is_a($paramValue, "EmptyValue")) {
                $subValue="NULL";
            } else {
                $value = mysql_real_escape_string($paramValue);
                //echo "value={$value}<br/>";
                $subValue="'{$value}'";
            }
        } else if ($paramType=="function") {
            $subValue=$paramValue;
        } else if (($paramType=="date") || ($paramType=="datetime")) {
            if (is_null($paramValue)) {
                $subValue="NULL";
            } else {                    
                if ($paramValue=="now()") {
                    $subValue="now()";
                } else {                        
                    if (is_array($paramValue)) {
                        //print 'here';                            
                    } else {
                        $value = mysql_real_escape_string($paramValue);
                        //echo "value={$value}<br/>";
                        if($paramType=="date")
                        {
                            // make sure the date is in the right format.
                            $value=date('Y-m-d',strtotime($value));
                            $subValue="'{$value}'";
                        }
                        else if ($paramType=="datetime"){
                            // make sure the datetime is in the right format
                            $value=date('Y-m-d G:i:s',strtotime($value));
                            $subValue="'{$value}'";
                        }
                    }
                }
            }
        } else if ($paramType=="integer") {
            if (is_null($paramValue)) {
               $subValue="NULL";
            } else {
               $value= filter_var($paramValue, FILTER_SANITIZE_NUMBER_INT);
               $subValue="{$value}";
            }
        } else if ($paramType=="boolean") {
            if (is_null($paramValue)) {
                $subValue="NULL";
            } else {
                $value= filter_var($paramValue, FILTER_VALIDATE_BOOLEAN);
                if ($value)
                    $value = 1;
                else
                    $value = 0;
                $subValue="{$value}";
            }
        } else if ($paramType=="float") {
            if (is_null($paramValue)) {
                $subValue="NULL";
            } else {
                $value= filter_var($paramValue, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
                $subValue="{$value}";
            }
        } else if ($paramType=="double") {
            if (is_null($paramValue)) {
                $subValue="NULL";
            } else {
                $value= filter_var($paramValue, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
                $subValue="{$paramValue}";
            }
        } else if ($paramType=="null") {
            $subValue="NULL";
        } else if($paramType=="column") {
             $value = mysql_real_escape_string($paramValue);
             $subValue="`{$value}`";
        } else if($paramType=="SQLColumn") {
             $subValue="`" . $paramValue->tableAlias . "`.`" . $paramValue->columnName ."`";
        } else {
            $msg="{$paramType} is Invalid Parameter Type";
            throw(new Exception($msg));
        }
        return $subValue;                    
    }

define("BASENAME","basename");
define("FULLPATH","fullpath");

/*
 * Author: Anthony L. Leotta
 * Date: 2013-08-14
 * 
 * directoryToArray
 * 
 * Searches paths recirsively for files or directories that match any one of a include pattern 
 * and rejects those that any one of a exclude pattern. Can return unique names, which is 
 * only usefule if the fileNameType of BASENAME is chosen.
 * 
 * directory string or array
 * recursive boolean
 * fileTypes string ('f','d','a)
 * includeRegex string or array
 * excludeRegex string or array
 * fileNameType (BASENAME,FULLPATH)
 * returnUniqueNames boolean
 * 
*/

function directoryToArray($directory, 
                          $recursive, 
                          $fileTypes, 
                          $includeRegex, 
                          $excludeRegex, 
                          $fileNameType,
                          $returnUniqueNames) {
    $array_items = array();
    
    if (!is_array($directory)) {
        $paths=Array($directory);
    } else {
        $paths = $directory;
    }
    
    $filterFileTypes=Array();
    if (is_array($fileTypes)) {
        foreach($fileTypes as $fileType) {            
            if (strtolower($fileType)=="d") {
                $filterFileTypes[]='d';
            } else if (strtolower($fileType)=="f") {
                $filterFileTypes[]='f';
            } else if (strtolower($fileType)=="a") {
                $filterFileTypes[]='d';
                $filterFileTypes[]='f';
            } 
        }
    } else if (is_null($fileTypes)) {
        $filterFileTypes[]='f';
    } else {
        if (strtolower($fileTypes)=="d") {
            $filterFileTypes[]='d';
        } else if (strtolower($fileTypes)=="f") {
            $filterFileTypes[]='f';
        } else if (strtolower($fileTypes)=="a") {
            $filterFileTypes[]='d';
            $filterFileTypes[]='f';
        }        
    }
            
    $includeRegexPatterns=Array();
    if (is_array($includeRegex)) {
        $includeRegexPatterns=$includeRegex;
    } else if (is_string($includeRegex)) {
        $includeRegexPatterns=Array($includeRegex);
    }    

    $excludeRegexPatterns=Array();
    if (is_array($excludeRegex)) {
        $excludeRegexPatterns=$excludeRegex;
    } else if (is_string($excludeRegex)) {
        $excludeRegexPatterns=Array($excludeRegex);
    }
            
    if (is_string($fileNameType)) {
        if (!(($fileNameType == BASENAME) || ($fileNameType == FULLPATH))) {           
            $fileNameType = FULLPATH;
        }
    } else {
        $fileNameType = FULLPATH;
    }
    
    foreach($paths as $path) {        
        $realpath = realpath($path);

        if ($realpath===False) {
            continue;
        }

        if ($handle = opendir($realpath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {                
                    $dir = joinPaths($realpath,$file);                 
                    if (is_dir($dir)) {
                        if($recursive) {
                            $array_items = array_merge($array_items, directoryToArray($dir, $recursive));
                        }
                    }
                    
                    if ((is_dir($dir) && in_array('d',$filterFileTypes)) ||
                        (!is_dir($dir) && in_array('f',$filterFileTypes))) {
                        
                        if ($fileNameType == FULLPATH) {
                            $filename = joinPaths($realpath,$file); 
                        } else {
                            $filename = $file; 
                        }
                        
                        $add=False;
                        if (count($includeRegexPatterns)>0) {
                            foreach($includeRegexPatterns as $regexPattern) {
                                preg_match($regexPattern,$filename,$matches);    
                                if (count($matches)>0) { 
                                    $add=True;                                    
                                    continue;
                                }
                            }                            
                        } else {                        
                            $add=True;
                        }

                        if (count($excludeRegexPatterns)>0) {
                            foreach($excludeRegexPatterns as $regexPattern) {
                                preg_match($regexPattern,$filename,$matches);    
                                if (count($matches)>0) { 
                                    $add=False;                                    
                                    continue;
                                }
                            }                            
                        } else {                        
                            $add=True;
                        }
                        
                        if ($add) {
                            if ($returnUniqueNames) {
                                if (!in_array($filename,$array_items)) {
                                    $array_items[]=$filename;    
                                }
                            } else {
                                $array_items[]=$filename;
                            }
                        }
                    }
                }
            }
            closedir($handle);
        }
    }
    
    return $array_items;
}
    
function outputCSV($data) {
    $output = fopen("php://output", "w");
    foreach ($data as $row) {
        fputcsv($output, $row);
    }
    fclose($output);
}
    
?>