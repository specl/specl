<?php 

// TODO: make sort array work with objects.

class sortColumn {
	public $name;
	public $type;
	public $direction;
	
	public function __construct($name,$type,$direction='asc'){
		$this->name=$name;
		$this->type=$type;
		$this->direction=$direction;
	}
}

function numericCmp($a, $b)
{
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

// takes an array of associative arrays and an array of sortColumns
// and returns the rows sorted.
function sortRows($data, $sortColumns) { 
	if(!is_array($sortColumns)){ 
		$sortColumns = array($sortColumns); 
	}
	
	
	
	usort($data,function($a, $b) use($sortColumns) 
				{	 	
					$retval = 0; 
					foreach($sortColumns as $sortColumn) { 
						if($retval == 0){ 
							if($sortColumn->type=="string"){
								if($sortColumn->direction=='asc'){
									$retval = strnatcasecmp ($a[$sortColumn->name],$b[$sortColumn->name]); 
								}
								else if ($sortColumn->direction=='desc'){
									$retval = -strnatcasecmp ($a[$sortColumn->name],$b[$sortColumn->name]); 
								}
							}
							else if($sortColumn->type=="integer" || $sortColumn->type=="double" || $sortColumn->type=="float" ){
								if($sortColumn->direction=='asc'){
									$retval=numericCmp($a[$sortColumn->name], $b[$sortColumn->name]);
								}
								else if ($sortColumn->direction=='desc'){
									$retval=-numericCmp($a[$sortColumn->name], $b[$sortColumn->name]);
								}
							}
                            else if($sortColumn->type=="boolean" ){
                                $a=(int)($a[$sortColumn->name]);
                                $b=(int)($b[$sortColumn->name]);
								if($sortColumn->direction=='asc'){
									$retval=numericCmp($a[$sortColumn->name], $b[$sortColumn->name]);
								}
								else if ($sortColumn->direction=='desc'){
									$retval=-numericCmp($a[$sortColumn->name], $b[$sortColumn->name]);
								}
							}
                            else if($sortColumn->type=="date" || $sortColumn->type=="datetime"){
                                $a=strtotime($a[$sortColumn->name]);
                                $b=strtotime($b[$sortColumn->name]);
                                
                                if($sortColumn->direction=='asc'){
									$retval=numericCmp($a, $b);
								}
								else if ($sortColumn->direction=='desc'){
									$retval=-numericCmp($a, $b);
								}
                            }
							else{
								throw (new Exception("The arraySort function does not handle the type {$sortColumn->type}"));
							}
						}	
						else{
							break;
						}
					} 
					return $retval; 
				}
		  ); 

	return $data; 
}
?>