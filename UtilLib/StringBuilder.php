<?php
// TODO: Make HTML and Javascript builder that extends string
// builder.
class StringBuilder
{
    public $string;
    public $indentLevel;
    public $indentString;

    public function __construct() {     
        $this->string = "";
        $this->indentLevel = 0;
        $this->tabsize=4;
        $this->indentString = " ";
    }

    public function setIndentLevel($indentLevel) {
        $this->indentLevel = $indentLevel;
    }
    
    public function addLine($lineContent, $addNewLine=true,$addLeadingIndents=true) {
        if (!isset($indentLevel)) {
            $indentLevel = $this->indentLevel;
        }
        
        if ($addLeadingIndents==true) {
            $tabInSpaces=str_repeat($this->indentString,$this->tabsize);
            $leadingSpaces = str_repeat($tabInSpaces, $indentLevel);
        } else {
            $leadingSpaces="";
        }
                
        if($addNewLine) {
            $endLine = "\n";
        } else {
            $endLine="";
        }
        
        $completeLine=$leadingSpaces.$lineContent.$endLine;
        $this->string.=$completeLine;

    }
    
    public function ln($lineContent, $addNewLine=true,$addLeadingIndents=true) {
        $this->addLine($lineContent, $addNewLine,$addLeadingIndents);
    }
            
    public function addList($contentArray,$sep=",") {
        $count=0;
        foreach($contentArray as $content) {
            $newLine=$content;
            $count+=1;
            
            if($count<count($contentArray))
            {
                $newLine.=$sep;
            }
            $this->addLines($newLine);
        }
    }

    
             
    public function addLines($content) {
        
        $content=trim($content,"\n");

        $tabInSpaces=str_repeat($this->indentString,$this->tabsize);
        $leadingSpaces = str_repeat($tabInSpaces, $this->indentLevel);
        $endLine = "\n";

        $replacementString="\n".$leadingSpaces;
        $stringToAdd=$leadingSpaces.str_replace("\n",$replacementString,$content).$endLine;

        $this->string.=$stringToAdd;

    }

    public function printToFile($outFilename) {
        file_put_contents($outFilename,$this->string);
    }

    public function getString() {
        return $this->string;
    }

    public function increaseIndentLevel($numTimes=1) {
        for($i=0;$i<$numTimes;$i++) {
            $this->indentLevel+=1;
        }
    }

    public function inc($numTimes=1) {
        $this->increaseIndentLevel($numTimes);
    }
    
    public function getIndentLevel() {
        return $this->indentLevel;
    }

    public function clearIndentLevel() {
        return $this->indentLevel=0;
    }

    public function clr() {
        $this->clearIndentLevel();
    }
        
    public function decreaseIndentLevel($numTimes=1) {
        for($i=0;$i<$numTimes;$i++) {
            if($this->indentLevel==0) {
                break;
            } else {
                $this->indentLevel-=1;
            }
        }
    }
    
    public function dec($numTimes=1) {
        $this->decreaseIndentLevel($numTimes);
    }        
    
}

?>