<?php
/**
 * Description of SQLFuncs
 *
 * @author Tony
 */

function FkExistsMultiColumn($objDef, $connectionName, $fkTable, $fkColumns, $objData) {
    global $app;
    
    $sqlBuilder = new QueryBuilder();
    
    $dbConn = $app->getConnection($connectionName);

    if (is_null($dbConn)) {
        // connection name not found or some other problem
        return;
    }
              
    $whereClause="";
    $sep = "";
    foreach($fkColumns as $columnName) {
        
        $propDef = $objDef->getProperty($columnName);
        
        // TODO check for failure.
        
        $whereClause .= $sep."`".$propDef->name."`=%".$propDef->name."%";
        if ($sep=="") $sep=" AND ";
    }    
    
    
    $sql = "select exists(select * from `".$fkTable ."` where ".$whereClause ." limit 1) as Success;";

    $sqlBuilder->setStatement($sql);
    
    foreach($fkColumns as $columnName) {
        
        $propDef = $objDef->getProperty($columnName);
         
        if (isset($objData[$propDef->name])) {            
            $value = $objData[$propDef->name];
            $sqlBuilder->createParam($columnName, $propDef->objectType, $value);
        } else {
            // ??? this is bad.
            $value = NULL;
            $sqlBuilder->createParam($columnName, $propDef->objectType, null);
        }
    }
    
    $sql = $sqlBuilder->bindParams();

    $results = $dbConn->select($sql);

    if ($results!=0)
    {
        $row = $dbConn->getRowAssoc($results);

        $success = $row['Success'];

        if ($success==0) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function FkExists($connectionName, $fkTable, $fkColumn, $value) {
    global $app;
    
    $sqlBuilder = new QueryBuilder();
    
    $dbConn = $app->getConnection($connectionName);

    if (is_null($dbConn)) {
        // connection name not found or some other problem
        return;
    }
                
    $sql = "select exists(select * from `".$fkTable ."` where `".$fkColumn ."`=%value% limit 1) as Success;";

    $sqlBuilder->setStatement($sql);
    $sqlBuilder->createParam("value", "string", $value);
    $sql = $sqlBuilder->bindParams();

    $results = $dbConn->select($sql);

    if ($results!=0)
    {
        //$row = $dbConn->getRowAssoc($results);

        $success = $results[0]['Success'];

        if ($success==0) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function FkExistsWhere($connectionName, $fkTable, $fkColumn, $value, $whereClause) {
    global $app;
    
    $sqlBuilder = new QueryBuilder();
    
    $dbConn = $app->getConnection($connectionName);

    if (is_null($dbConn)) {
        // connection name not found or some other problem
        return;
    }

    if ($whereClause) {
        $whereClause = " AND " . $app->fillInGlobals($whereClause);    
    }    
        
    $sql = "select exists(select * from `".$fkTable ."` where `".$fkColumn ."`=%value% {$whereClause} limit 1) as Success;";

    $sqlBuilder->setStatement($sql);
    $sqlBuilder->createParam("value", "string", $value);
    $sql = $sqlBuilder->bindParams();

    $results = $dbConn->select($sql);

    if ($results!=0)
    {
        $row = $dbConn->getRowAssoc($results);

        $success = $row['Success'];

        if ($success==0) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function SQLLookup($connectionName, $table, $searchColumn, $lookupColumn, $searchValue) {
    global $app;
    
    $sqlBuilder = new QueryBuilder();
    
    $dbConn = $app->getConnection($connectionName);

    if (is_null($dbConn)) {
        // connection name not found or some other problem
        return;
    }
    
    $sql = "select `".$lookupColumn."` from `".$table ."` where `".$searchColumn ."`=%value% limit 1";

    $sqlBuilder->setStatement($sql);
    $sqlBuilder->createParam("value", "string", $searchValue);
    $sql = $sqlBuilder->bindParams();

    $results = $dbConn->select($sql);

    if ($results!=0)
    {
        $row = $dbConn->getRowEnum($results);

        if ($row===False) {
            return $searchValue;
        } else {
            return $row[0];
        }               
    } else {
        return $searchValue;
    }
}


?>
