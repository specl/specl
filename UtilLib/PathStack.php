<?php
/**
 * Description of PathStack
 *
 * @author tony
 */
class PathStack extends Stack {
    public $delim;
    public function __construct($delim=null) {
        
        if ($delim==Null) {
            $this->delim = PATH_SEPARATOR;
        } else {
            $this->delim = $delim;
        }
        parent::__construct();
    }

    public function add($tag) {
        array_push($this->_stack, $tag);
        $path = $this->getPath();
        return $path;
    }
    
    public function getPath() {
        $path = implode(App::ZONESSEP, $this->_stack);
        return $path;
    }
    
    public function getPath2(){
        $path=implode($this->delim,$this->_stack);
        if(count($this->_stack)===0){
            return $path;
        }
        else{
            return $this->delim.$path;
        }
    }

    public function getParentPath() {
        
        $tmp = array();
        $tmp = array_merge($tmp, $this->_stack);
        if (count($tmp)>1) {
            array_pop($tmp);
        }
        
        $path = implode(App::ZONESSEP, $tmp);
        return $path;
    }
        
}

?>
