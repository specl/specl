<?php
/**
 * Description of SubString
 * @author Tony
 */
// We created this class because we found the bind parameters
// did not keep track of the variables that need to be subsititited.
// this class does

class SubString {

    public $statement;
    public $parameters; // has a name, and a data type BindParameter Class

    public function  __construct() {
        $this->statement = null;
        $this->parameters = array();
    }

    public function addParameter($paramObj) {
        $this->parameters[$paramObj->key] = $paramObj;
    }
}
?>
